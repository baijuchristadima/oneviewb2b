Vue.use(CKEditor);

Vue.component("hotel-confirmation-component", {
  data: function () {
    return {
      commonStore: vueCommonStore.state,
      header: {
        bookingDate: null,
        bookingStatus: "Please wait while we retrieve your book request".toUpperCase(),
        bookingStatusNote: '',
        bookingCancellationMessage: null,
        callToActionButtons: [{
          id: "cancelBtn",
          show: true,
          label: "Cancel",
          isModal: false,
          dataTarget: "",
          action: this.cancelBooking,
          showSpinnerHtml: false
        },
        {
          id: "requestCancellationBtn",
          show: true,
          label: "Request Cancellation",
          isModal: false,
          dataTarget: "",
          action: this.requestOfflineCancellation,
          showSpinnerHtml: false
        },
        {
          id: "downloadVoucherBtn",
          show: true,
          label: "Download Voucher",
          isModal: false,
          dataTarget: "",
          action: this.getPdf,
          showSpinnerHtml: false
        },
        {
          id: "emailVoucherBtn",
          show: true,
          label: "Email Voucher",
          isModal: true,
          dataTarget: "#modalEmailVoucher",
          action: function (button) { },
          showSpinnerHtml: false
        },
        {
          id: "editVoucherBtn",
          show: true,
          label: "Edit Voucher",
          isModal: true,
          dataTarget: "#modalEditVoucher",
          action: this.editVoucher,
          showSpinnerHtml: false
        }
        ]
      },
      bookingInformation: {
        referenceNumber: null,
        bookingDate: null,
        checkIn: null,
        cancellationDeadline: null,
        bookingStatus: null,
        numberOfNights: null,
        checkOut: null,
        totalAmount: null,
        couponDiscount: null,
        amountPaid: null,
        paymentMethod: null,
        paymentStatus: null,
        bookingStatusList: null,
        bookingRoomPriceBreakUp: null
      },
      supplierInformation: {
        hotelName: null,
        hotelAddress: null,
        supplierName: null,
        supplierReferenceNumber: null,
        starRating: null
      },
      roomDetails: {
        room: [],
        showCotsNotes: false,
        cancellationDetails: [],
        priceBreakUp: []
      },
      supplierCode: null,
      isBookingCompleted: false,
      statusHistoryHtml: "",
      hotelPriceBreakUpHtml: "",
      roomPriceBreakUpHtml: {
        table: "",
        totalCost: ""
      },
      paxListPerRoomHtml: "",
      spinnerHtml: 'Processing<span class="loader__dot">.</span><span class="loader__dot">.</span><span class="loader__dot">.</span>',
      isRetrieveBooking: false,
      isBookingSuccess: false,
      hotelDataFromPax: {},
      reponseData: {},
      // editor: DecoupledEditor,
      // editorData: "",
      // editorConfig: {
      //   // The configuration of the editor.
      // },
      supportDetails: {
        address: null,
        phone: null,
        email: null
      },
      roomDetailsTable: [],
      emailToVoucher: "",
      emailToEditVoucher: "",
      additionalSupplierInformation: []
    }
  },
  created: function () {
    var vm = this;

    var hotelDataFromPax = window.sessionStorage.getItem("hotelInfo");
    if (hotelDataFromPax) {
      vm.supplierCode ??= hotelDataFromPax.supplierCode;
    }
    console.log(window.localStorage.getItem("accessToken"));


    if (hotelDataFromPax) {
      hotelDataFromPax = JSON.parse(decodeURIComponent(escape(atob(hotelDataFromPax))));
      vm.hotelDataFromPax = hotelDataFromPax;
      vm.header.bookingStatus = "Please wait while we process your book request".toUpperCase();


      var continueBooking = false;
      if (hotelDataFromPax.paymentMethod == "7" || hotelDataFromPax.paymentMethod == "6") {

        if (hotelDataFromPax.paymentMethod == "6" && vm.getParameterByName("status") == 101) {
          continueBooking = true;
        } else if (hotelDataFromPax.paymentMethod == "7") {
          continueBooking = true;
        }
      }

      if (continueBooking) {

        var paxDetails = [];
        var paxPerRoom = [];
        var paxId = 1;
        for (var roomIndex = 0; roomIndex < hotelDataFromPax.searchCriteria.searchedRooms.length; roomIndex++) {
          var guests = hotelDataFromPax.searchCriteria.searchedRooms[roomIndex].guests;
          var paxes = [];
          for (var guestIndex = 0; guestIndex < guests.adult; guestIndex++) {
            paxes.push(paxId);
            paxId++;

          }
          if (guests.children != undefined && guests.children.length > 0) {

            for (var guestIndex = 0; guestIndex < guests.children.length; guestIndex++) {
              paxes.push(paxId);
              paxId++;

            }
            paxDetails.push({
              adult: guests.adult,
              child: guests.children.length
            });
          } else {
            paxDetails.push({
              adult: guests.adult,
              child: 0
            });

          }
          paxPerRoom.push(paxes);
        }

        var rooms = [];

        var totalAmount = 0;
        var sealCode = "";
        for (var index = 0; index < hotelDataFromPax.selectedRooms.data.length; index++) {
          var room = hotelDataFromPax.selectedRooms.data[index].room;
          rooms.push({
            name: room.roomName,
            roomId: room.roomId,
            paxes: paxPerRoom[index],
            guests: room.guests,
            rate: {
              bookable: true,
              cancellationPolicies: room.rates[0].cancellationPolicies,
              rateSpecific: room.rates[0].roomRateSpecific,
              pricing: {
                perNightPrice: room.rates[0].pricing.perNightPrice,
                totalPrice: room.rates[0].pricing.totalPrice
              },
              roomRateReferences: room.rates[0].roomRateSpecific ? room.rates[0].roomRateSpecific.roomRateReferences : undefined
            },
            roomReferences: room.roomSpecific ? room.roomSpecific.roomReferences : undefined,
            roomPrebookReference: hotelDataFromPax.prebookResponse.hotel.rooms[index].roomSpecific ? hotelDataFromPax.prebookResponse.hotel.rooms[index].roomSpecific.roomPrebookReference : undefined
          });
          totalAmount += room.rates[0].pricing.totalPrice.price;
          sealCode = room.rates[0].pricing.totalPrice.sealCode;
        }

        var paxes = [];
        for (var paxIndex = 0; paxIndex < hotelDataFromPax.paxDetails.pax.length; paxIndex++) {
          for (var roomIndex = 0; roomIndex < hotelDataFromPax.paxDetails.pax[paxIndex].paxPerRoom.length; roomIndex++) {
            var pax = __.cloneDeep(hotelDataFromPax.paxDetails.pax[paxIndex].paxPerRoom[roomIndex]);
            pax.email = vm.hotelDataFromPax.paxDetails.contactDetails.emailID;
            pax.phoneNumber = vm.hotelDataFromPax.paxDetails.contactDetails.phoneNumber;
            delete pax.label;
            paxes.push(pax);
          }
        }

        var BookRQ = {
          service: "HotelRQ",
          node: hotelDataFromPax.searchCriteria.node,
          token: hotelDataFromPax.searchCriteria.token,
          hubUuid: hotelDataFromPax.searchCriteria.hubUuid,
          supplierCodes: [hotelDataFromPax.supplierCode],
          credentials: [],
          content: {
            command: "BookRQ",
            hotel: {
              cityCode: hotelDataFromPax.location.cityCode,
              hotelCode: hotelDataFromPax.supplierCode == 64 ? hotelDataFromPax.hotelDetailsRS.hotelCode : hotelDataFromPax.hotelCode,
              totalAmount: totalAmount,
              sealCode: hotelDataFromPax.prebookResponse.sealCode,
              nationality: hotelDataFromPax.searchCriteria.nationality,
              residentOf: hotelDataFromPax.searchCriteria.countryOfResidence,
              rooms: rooms,
              hotelCancellationPolicies: hotelDataFromPax.prebookResponse.hotel.hotelCancellationPolicies,
              hotelSpecific: hotelDataFromPax.hotelSpecific
            },
            paxes: paxes,
            stay: hotelDataFromPax.searchCriteria.stay,
            ovBookingReference: hotelDataFromPax.bookingId,
            supplierSpecific: {
              uuid: hotelDataFromPax.searchCriteria.uuid,
              searchReferences: hotelDataFromPax.searchReferences,
              searchHotelReferences: hotelDataFromPax.hotelSpecific ? hotelDataFromPax.hotelSpecific.searchHotelReferences : undefined,
              roomsReferences: hotelDataFromPax.roomSpecific ? hotelDataFromPax.roomSpecific.roomsReferences : undefined,
              prebookReferences: hotelDataFromPax.prebookResponse.supplierSpecific ? hotelDataFromPax.prebookResponse.supplierSpecific.prebookReferences : undefined,
              roomResponse: hotelDataFromPax.roomSupplierSpecific ? hotelDataFromPax.roomSupplierSpecific.roomResponse : undefined,
              hotelName: hotelDataFromPax.supplierCode == 64 ? hotelDataFromPax.hotelDetailsRS.name : hotelDataFromPax.name,
              hotelIndex: hotelDataFromPax.index,
            }
          }
        };

        var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;

        var config = {
          axiosConfig: {
            method: "POST",
            url: hubUrl + "/hotelBook",
            data: {
              request: BookRQ
            }
          },
          successCallback: function (response) {
            console.log(response.data);

            vm.isRetrieveBooking = false;
            // if (response.data.response.content.bookResponse.hotel.statusCode == "HK") {

            //Update Status
            vm.bookingStatusUpdate(response.data.response.content.bookResponse.bookingReferences.oneviewReference, response.data.response.content.bookResponse.hotel.statusCode);

            // } else {
            //Show booking failed
            // vm.getHotelBookingInformation(response.data.response.content.bookResponse.bookingReferences.oneviewReference);
            // }
            //Clear booking identifier
            window.sessionStorage.removeItem("hotelInfo");
            window.sessionStorage.setItem("bookingId", hotelDataFromPax.bookingId);


          },
          errorCallback: function (error) {
            //Clear booking identifier
            vm.isRetrieveBooking = true;
            window.sessionStorage.removeItem("hotelInfo");
            window.sessionStorage.setItem("bookingId", hotelDataFromPax.bookingId);
            vm.bookingStatusUpdate(hotelDataFromPax.bookingId, "X");
            // vm.getHotelBookingInformation(hotelDataFromPax.bookingId);
            // alertify.alert(i18n.t('Hotel_Confirmation.AlertifyHeader1_Label'), i18n.t('Hotel_Confirmation.BookingError_Label'));

          },
          showAlert: true
        };

        mainAxiosRequest(config);
      } else {
        vm.isRetrieveBooking = true;
        window.sessionStorage.setItem("bookingId", hotelDataFromPax.bookingId);
        if (hotelDataFromPax.paymentMethod && vm.getParameterByName("status") == 102) {
          vm.bookingStatusUpdate(hotelDataFromPax.bookingId, "X");
        } else {
          vm.getHotelBookingInformation(hotelDataFromPax.bookingId);
        }
      }
    } else {
      vm.isRetrieveBooking = true;
      var bookingId = window.sessionStorage.getItem("bookingId");
      var supplierCode = window.sessionStorage.getItem("supplierId");
      vm.supplierCode ??= supplierCode;
      vm.getHotelBookingInformation(bookingId);
    }

  },
  mounted: function () { },
  methods: {
    // onReady: function (editor) {
    //   // Insert the toolbar before the editable area.
    //   editor.ui.getEditableElement().parentElement.insertBefore(
    //     editor.ui.view.toolbar.element,
    //     editor.ui.getEditableElement()
    //   );
    // },
    emailBookingDetails: function (to) {
      var agencyNode = window.localStorage.getItem("agencyNode");
      if (agencyNode) {
        agencyNode = JSON.parse(atob(agencyNode));
      }
      var vm = this;
      var totalRooms = vm.roomDetails.room.length;
      var totalChildren = vm.roomDetails.room.reduce(function (a, c) {
        return a + c.childCount
      }, 0);
      var totalAdults = vm.roomDetails.room.reduce(function (a, c) {
        return a + c.adultCount
      }, 0);
      var postData = {
        agencyPhone: agencyNode.contactNumber || "",
        agencyEmail: agencyNode.loginNode.email || "",
        logoUrl: vm.commonStore.agencyNode.loginNode.logo,
        fullName: vm.bookingInformation.bookingRoomPriceBreakUp[0].roomQuotes[0].guests[0].titleName + ' ' + vm.bookingInformation.bookingRoomPriceBreakUp[0].roomQuotes[0].guests[0].firstName + ' ' + vm.bookingInformation.bookingRoomPriceBreakUp[0].roomQuotes[0].guests[0].surName,
        bookingRefId: vm.bookingInformation.referenceNumber,
        hotelRef: vm.supplierInformation.supplierReferenceNumber || "Not Available",
        hotelName: vm.supplierInformation.hotelName,
        hotelAddress: decodeURI(vm.supplierInformation.hotelAddress),
        nights: vm.bookingInformation.numberOfNights + (vm.bookingInformation.numberOfNights > 1 ? ' Nights' : ' Night'),
        adults: totalAdults + (totalAdults > 1 ? ' Adults' : ' Adult'),
        child: totalChildren + (totalChildren > 1 ? ' Children' : ' Child'),
        room: totalRooms + (totalRooms > 1 ? ' Rooms' : ' Room'),
        checkIn: vm.bookingInformation.checkIn,
        checkOut: vm.bookingInformation.checkOut,
        roomDetails: vm.roomDetails.room.map(function (e, i) {
          return {
            name: e.room,
            pax: e.allNames
          }
        }),
      }
      vm.getTemplate("HotelEmailTemplate").then(function (templateResponse) {
        var data = templateResponse.data.data;
        var emailTemplate = "";
        if (data.length > 0) {
          for (var x = 0; x < data.length; x++) {
            if (data[x].enabled == true && data[x].type == "HotelEmailTemplate") {
              emailTemplate = data[x].content;
              break;
            }
          }
        };
        var htmlGenerate = vm.commonStore.hubUrls.emailServices.htmlGenerate
        var emailData = {
          template: emailTemplate,
          content: postData
        };
        //for supplier dropdown
        var logoUrl = "";
        var fromEmail = [{
          emailId: ""
        }];
        var ccEmails = null;
        try {
          var agencyNodeTmp = JSON.parse(atob(window.localStorage.getItem("agencyNode")));
          logoUrl = agencyNodeTmp.loginNode.logo;
          // fromEmail = agencyNodeTmp.loginNode.parentEmailId;
          fromEmail = _.filter(agencyNodeTmp.loginNode.emailList,
            function (o) {
              return o.emailTypeId == 3 && o.emailType.toLowerCase() == "support";
            });
          ccEmails = _.filter(agencyNodeTmp.loginNode.emailList,
            function (o) {
              return o.emailId != undefined && o.emailTypeId == 8 && o.emailType.toLowerCase() == 'hotel confirmation';
            });

        } catch (err) { }

        ccEmails = _.map(ccEmails, function (o) {
          return o.emailId;
        });

        var toEmails = "";
        if (to) {
          toEmails = to;
        } else {
          toEmails = Array.isArray(vm.hotelDataFromPax.paxDetails.contactDetails.emailID) ? vm.hotelDataFromPax.paxDetails.contactDetails.emailID : [vm.hotelDataFromPax.paxDetails.contactDetails.emailID];
        }
        axios.post(htmlGenerate, emailData)
          .then(function (htmlResponse) {
            vm.getPdf().then(function (response) {
              var emailPostData = {
                type: "AttachmentRequest",
                toEmails: toEmails,
                fromEmail: fromEmail[0].emailId || self.commonStore.fallBackEmail,
                ccEmails: null,
                bccEmails: null,
                subject: "Hotel Confirmation - " + vm.bookingInformation.referenceNumber,
                attachmentPath: response.data,
                html: htmlResponse.data.data
              };
              var mailUrl = vm.commonStore.hubUrls.emailServices.emailApiWithAttachment;
              sendMailService(mailUrl, emailPostData);
              if (to) {
                vm.header.callToActionButtons[__.findIndex(vm.header.callToActionButtons, function (b) {
                  return b.id === "emailVoucherBtn"
                })].showSpinnerHtml = false;
                alertify.alert(i18n.t('Hotel_Confirmation.AlertifyHeader2_Label'), i18n.t('Hotel_Confirmation.EmailSent_Label'));
              }
            });

          }).catch(function (error) {
            return false;
          })
      });
    },
    bookingStatusUpdate: function (ovBookingReference, statusCode) {
      var vm = this;
      var statusToBeSent = "";
      switch (statusCode) {
        case "HK":
          statusToBeSent = "RR";
          break;
        case "CP":
          statusToBeSent = "CP";
          break;
        case "XP":
          statusToBeSent = "XP";
          break;
        case "X":
        case "ER":
        case "RJ":
          statusToBeSent = "RF";
          break;
        default:
          statusToBeSent = "RF";
          break;
      }

      var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;

      var config = {
        axiosConfig: {
          method: "POST",
          url: hubUrl + "/hotelBook/booking/changeStatus",
          data: {
            referenceNumber: ovBookingReference,
            code: statusToBeSent
          }
        },
        successCallback: function (response) {
          console.log("Status update", response.data);
          if (response.data) {
            //bind booking details
            vm.isBookingSuccess = true;
            vm.getHotelBookingInformation(ovBookingReference);
          } else {
            //show update status error
          }

        },
        errorCallback: function (error) { },
        showAlert: true
      };

      mainAxiosRequest(config);

    },
    getHotelBookingInformation: function (ovBookingReference) {
      var vm = this;
      window.sessionStorage.removeItem("hotelInfo");
      var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;
      var bookDetails = this.commonStore.hubUrls.hubConnection.hubServices.hotels.bookDetails;
      if (vm.supplierCode == '82') {
        var bookDetailsRQ = {
          service: "HotelRQ",
          node: {
            currency: vm.commonStore.agencyNode.loginNode.currency,
            agencyCode: vm.commonStore.agencyNode.loginNode.code
          },
          token: window.localStorage.getItem("accessToken"),
          supplierCodes: [vm.supplierCode],
          credentials: [],
          content: {
            command: "BookDetailsRQ",
            bookDetails: {
              bookingRef: ovBookingReference
            }
          }
        };
        var axiosConfig = {
          method: "POST",
          url: hubUrl + bookDetails,
          data: {
            request: bookDetailsRQ
          }
        };
      } else {
        var axiosConfig = {
          method: "GET",
          url: hubUrl + "/hotelBook/bookingbyref/" + ovBookingReference
        }
      }
      var config = {
        axiosConfig: axiosConfig,
        successCallback: function (responseData) {
          var localeMoment = vm.commonStore.selectedLanguage ? vm.commonStore.selectedLanguage.code : 'en';
          vm.isBookingCompleted = true;
          console.log(responseData.data);
          var response = {};
          if (vm.supplierCode == '82') {
            response = responseData.data.response.content.hotelBookingUIData;
          } else {
            response = responseData.data;
          }
          vm.header.callToActionButtons[0].show = vueCommonStore.state.commonRoles.hasHotelCancelBooking;
          vm.header.callToActionButtons[1].show = vueCommonStore.state.commonRoles.hasHotelCancellationRequest;
          vm.header.callToActionButtons[3].show = vueCommonStore.state.commonRoles.hasHotelVoucher;
          vm.header.callToActionButtons[4].show = vueCommonStore.state.commonRoles.hasHotelEditVoucher;

          var roomQuotes = response.booking.hotels[0].rooms[0].roomQuotes[0];

          vm.header.bookingDate = moment(response.booking.bookDate).locale(localeMoment).format("DD MMMM YYYY");
          // vm.header.bookingStatus = i18n.t('Hotel_Confirmation.BookingStatusHeader1_Label') + " " +  (roomQuotes.statusCode == "RF" ? "HAS FAILED" : i18n.t('Hotel_Confirmation.BookingStatusHeader2_Label') + " " + roomQuotes.status.toUpperCase());
          vm.header.bookingStatus = "YOUR BOOKING " + (roomQuotes.statusCode == "RF" ? "HAS FAILED" : (roomQuotes.statusCode == "PD" ? "is Pending Confirmation" : "is " + roomQuotes.status)).toUpperCase();
          if (roomQuotes.statusCode == "CP" || roomQuotes.statusCode == "PD") {
            vm.header.bookingStatusNote = "This booking is pending action from the supplier, Please check later."
          }

          var currentDate = moment(new Date()).format("DD/MM/YYYY");
          var cancellationBuffer = 1;
          var cDate = moment(new Date(roomQuotes.checkInDate)).subtract(cancellationBuffer, 'day').format("DD/MM/YYYY");

          if (moment(currentDate, "DD/MM/YYYY").isSameOrAfter(moment(moment(response.booking.deadlineDate).format("DD/MM/YYYY"), "DD/MM/YYYY")) &&
            moment(currentDate, "DD/MM/YYYY").isSameOrBefore(moment(cDate, "DD/MM/YYYY"))) {
            if (roomQuotes.statusCode == "RR" || roomQuotes.statusCode == "HK" || roomQuotes.statusCode == "KK") {
              vm.header.bookingCancellationMessage = "Please note: Booking is under cancellation period";
            }
          }

          vm.bookingInformation.referenceNumber = response.booking.refId;
          vm.bookingInformation.bookingDate = moment(response.booking.bookDate).locale(localeMoment).format("DD MMMM YYYY");
          vm.bookingInformation.checkIn = moment(roomQuotes.checkInDate).locale(localeMoment).format("DD MMMM YYYY");
          vm.bookingInformation.cancellationDeadline = moment(response.booking.deadlineDate).locale(localeMoment).format("DD MMMM YYYY");
          vm.bookingInformation.bookingStatus = roomQuotes.status;
          if (roomQuotes.statusCode == 'XX' ||
            roomQuotes.statusCode == 'RF' ||
            roomQuotes.statusCode == 'XP' ||
            roomQuotes.statusCode == 'XN') {
            vm.header.callToActionButtons[0].show = false;
            vm.header.callToActionButtons[1].show = false;
            vm.header.callToActionButtons[2].show = false;
            vm.header.callToActionButtons[3].show = false;
            vm.header.callToActionButtons[4].show = false;
          }
          vm.bookingInformation.numberOfNights = moment.duration(new Date(moment(roomQuotes.checkOutDate).format("DD MMM YYYY")) - new Date(moment(roomQuotes.checkInDate).format("DD MMM YYYY"))).days();
          vm.bookingInformation.checkOut = moment(roomQuotes.checkOutDate).locale(localeMoment).format("DD MMMM YYYY");

          vm.bookingInformation.bookingStatusList = response.booking.bookingStatusList;

          var roomDetails = response.booking.hotels[0].rooms;
          vm.bookingInformation.bookingRoomPriceBreakUp = response.booking.hotels[0].rooms;

          var totalCouponDiscount = parseFloat(0);
          var totalSellAmount = parseFloat(0);
          for (var ifb = 0; ifb < roomDetails.length; ifb++) {
            for (var ix = 0; ix < roomDetails[ifb].roomQuotes.length; ix++) {
              totalCouponDiscount += parseFloat(roomDetails[ifb].roomQuotes[ix].debitDetails.totalCouponDiscount);
              totalSellAmount += parseFloat(roomDetails[ifb].roomQuotes[ix].debitDetails.sellAmount);
            }
          }
          vm.bookingInformation.totalAmount = (totalSellAmount + totalCouponDiscount).toFixed(2);
          if (totalCouponDiscount != 0) {
            //show coupon field
            vm.bookingInformation.couponDiscount = totalCouponDiscount.toFixed(2);
            vm.bookingInformation.amountPaid = totalSellAmount.toFixed(2);
          }

          var paymentMode = response.booking.paymentMode;
          var paymentStatus = response.booking.paymentStatus;
          if (paymentMode) {
            vm.bookingInformation.paymentMethod = paymentMode;
          }

          if (paymentStatus && (paymentMode && paymentMode != "Credit")) {
            vm.bookingInformation.paymentStatus = paymentStatus ? "Success" : "Fail";
          }

          vm.supplierInformation.hotelName = response.booking.hotels[0].name;
          vm.supplierInformation.hotelAddress = response.booking.hotels[0].address;
          vm.supplierInformation.supplierName = response.booking.supplierName;
          vm.supplierInformation.supplierReferenceNumber = roomQuotes.supplierReferenceNumber || "Not Available";
          vm.supplierInformation.starRating = response.booking.hotels[0].starCategory;

          vm.bookingNotes = response.booking.supplierRemarks || "";

          for (var i = 0; i < roomDetails.length; i++) {
            if (roomDetails[i].isBabyCot != 0) {
              vm.roomDetails.showCotsNotes = true;
            }
          }

          var roomDetailsTable = [];
          for (var iPax = 0; iPax < roomDetails.length; iPax++) {
            for (var iQ = 0; iQ < roomDetails[iPax].roomQuotes.length; iQ++) {
              var paxName = roomDetails[iPax].roomQuotes[iQ].guests[0].firstName + " " + roomDetails[iPax].roomQuotes[iQ].guests[0].surName;
              var room = roomDetails[iPax].category + " - " + (roomDetails[iPax].meal ? roomDetails[iPax].meal : '');
              var childCount = 0;

              for (var j = 0; j < roomDetails[iPax].roomQuotes[iQ].guests.length; j++) {
                var paxCode = roomDetails[iPax].roomQuotes[iQ].guests[j].paxTypeCode;
                if (paxCode == "CHD") {
                  childCount++;
                }
              }
              var allNames = roomDetails[iPax].roomQuotes[iQ].guests.map(function (e) {
                return e.firstName + " " + e.surName
              });

              var adultCount = roomDetails[iPax].roomQuotes[iQ].numberOfAdults;

              roomDetailsTable.push({
                room: room,
                adultCount: adultCount,
                childCount: childCount,
                paxName: paxName,
                arrayIndex: iPax + "," + iQ,
                allNames: allNames.join(", ")
              });
            }
          }

          vm.roomDetails.room = roomDetailsTable;

          vm.roomDetails.cancellationDetails = vm.getCancellationPolicy(response);
          vm.reponseData = response;

          vm.supplierCode = response.booking.supplierId;

          var roomDetailsTable = [];
          for (var iPax = 0; iPax < response.booking.hotels[0].rooms.length; iPax++) {

            for (var iQ = 0; iQ < response.booking.hotels[0].rooms[iPax].roomQuotes.length; iQ++) {
              var details = {};
              var paxData = response.booking.hotels[0].rooms[iPax].roomQuotes[iQ].guests;
              details.roomType = __.unescape(response.booking.hotels[0].rooms[iPax].category + " - " + (response.booking.hotels[0].rooms[iPax].meal ? response.booking.hotels[0].rooms[iPax].meal : ''));
              var countChild = 0;
              var countAdult = 0;
              var childAge = [];
              for (var ifb = 0; ifb < paxData.length; ifb++) {
                if (paxData[ifb].isLead == true) {
                  details.guestName = paxData[ifb].firstName + " " + paxData[ifb].surName;
                }

                if (paxData[ifb].paxTypeCode == "CHD") {
                  countChild++;
                  childAge.push(paxData[ifb].age ? (paxData[ifb].age == 1 ? paxData[ifb].age + " yr" : paxData[ifb].age + " yrs") : "");
                }
                if (paxData[ifb].paxTypeCode == "ADT") {
                  countAdult++;
                }
                details.roomNumber = iPax + 1;
              }
              details.adult = countAdult;
              details.children = countChild;
              details.childAge = childAge.length > 0 ? childAge.join(" & ") : "0";
              details.traveller = countAdult + (countAdult == 1 ? " Adult" : " Adults") + (countChild != 0 ? (", " + countChild + (countChild == 1 ? " Child" : " Children")) : "");

              details.inclusions = (response.booking.hotels[0].rooms[iPax].meal ? response.booking.hotels[0].rooms[iPax].meal : '');

              roomDetailsTable.push(details);
            }
          }

          vm.roomDetailsTable = roomDetailsTable;

          var agencyNode = window.localStorage.getItem("agencyNode");
          if (agencyNode) {
            agencyNode = JSON.parse(atob(agencyNode));
            var fromEmail = [{
              emailId: ""
            }];
            try {
              fromEmail = _.filter(agencyNode.loginNode.emailList,
                function (o) {
                  return o.emailTypeId == 3 && o.emailType.toLowerCase() == "support";
                });
            } catch (error) { }

            vm.supportDetails = {
              address: agencyNode.loginNode.address || "",
              phone: agencyNode.loginNode.phoneList.filter((x) => {
                return x.type == "Telephone";
              })[0].number || "",
              email: fromEmail[0].emailId || ""
            };
          }


          if (roomQuotes.statusCode == "HK" || roomQuotes.statusCode == "RR") {
            vm.isBookingSuccess = true;
          } else {
            vm.isBookingSuccess = false;
          }

          if (!vm.isRetrieveBooking) {
            if (vm.isBookingSuccess) {
              setTimeout(function () {
                vm.emailBookingDetails(); // generate booking email
              }, 100);
            }
          }

          var addtionalInfo = __.filter(globalConfigs.services.hotels.additionalSupplierInformation, function (e) {
            return e.supplierId == response.booking.supplierId;
          })[0];

          if (addtionalInfo && addtionalInfo.specialInstructions.show) {
            vm.getHotelStaticInformation(response.booking.supplierId, response.booking.otherDetails);
          }
        },
        errorCallback: function (error) {
          console.log(error)
        },
        showAlert: true
      };

      mainAxiosRequest(config);
    },
    getHotelStaticInformation: function (supplierId, hotelCode) {
      var vm = this;
      var query = {
        query: {
          bool: {
            should: [{
              match_phrase: {
                supplier_Hotel_Code: hotelCode
              }
            },
            {
              match_phrase: {
                supplier_code: supplierId
              }
            }
            ],
            minimum_should_match: 1
          }
        },
        size: 1
      };

      var client = new elasticsearch.Client({
        host: [{
          host: HubServiceUrls.elasticSearch.elasticsearchHost,
          auth: HubServiceUrls.elasticSearch.auth,
          protocol: HubServiceUrls.elasticSearch.protocol,
          port: HubServiceUrls.elasticSearch.port
        }],
        log: 'trace'
      });

      client.search({
        index: 'supplier_hotel_data_test',
        body: query
      }).then(function (resp) {
        try {
          vm.additionalSupplierInformation = resp.hits.hits[0]._source.content[0].instructions || [];
        } catch (error) {
          vm.additionalSupplierInformation = [];
        }
        console.log(resp.hits.hits);
      });

    },
    getBookingHistory: function () {
      var htmlTemp = "";
      if (this.bookingInformation.bookingStatusList.length != undefined) {
        for (var iH = 0; iH < this.bookingInformation.bookingStatusList.length; iH++) {
          htmlTemp += '<li class="list-group-item"> <span class="badge new_lable">' +
            moment(this.bookingInformation.bookingStatusList[iH].entryDate).
              locale(this.commonStore.selectedLanguage ? this.commonStore.selectedLanguage.code : 'en').
              format("ddd, Do MMM YYYY - hh:mm:ss a") +
            '</span><b>' + this.bookingInformation.bookingStatusList[iH].bookingStatus + '</b> - ' +
            this.bookingInformation.bookingStatusList[iH].userName + '</li>';
        }
      } else {
        htmlTemp = '<tr><td>' + "No history available. " + '</td></tr>';
      }
      this.statusHistoryHtml = htmlTemp;
    },
    getPriceBreakUp: function () {
      var fareBreakUp = this.bookingInformation.bookingRoomPriceBreakUp;
      var htmlBreakups = "";
      var netCurrency = "";
      var baseCurrency = "";
      var rateofChange = "";
      var fareCurrency = "";
      var sellAmtBreakUp = "";
      var netAmtBreakUp = "";
      var baseAmtBreakUp = "";
      var netAmount = parseFloat(0);
      var mValue = parseFloat(0);
      var sellAmount = parseFloat(0);
      var baseamount = parseFloat(0);
      var couponDiscount = parseFloat(0);
      var amtBreakUp = '<span class="tooltiptext tooltip-bottom"><table class="table table-hover"><thead> <tr> <th class="tooltip_table">' + i18n.t('Hotel_Confirmation.RoomsHeader_Label') + '</th> <th class="tooltip_table">' + i18n.t('Hotel_Confirmation.Amount_Label') + '</th> </tr> </thead> <tbody>';
      var rateofExchange = this.commonStore.selectedCurrency == "USD" ? 1 : this.commonStore.selectedCurrency == "AED" ? JSON.parse(window.localStorage.getItem("currencyI18n")).b : this.commonStore.currencyMultiplier;
      for (var ifb = 0; ifb < fareBreakUp.length; ifb++) {
        for (var ix = 0; ix < fareBreakUp[ifb].roomQuotes.length; ix++) {

          var debitDetails = fareBreakUp[ifb].roomQuotes[ix].debitDetails;
          netAmount += parseFloat(debitDetails.netAmount);
          netCurrency = debitDetails.sellCurrency;
          baseCurrency = debitDetails.netCurrency;
          rateofChange = debitDetails.conversionRate;
          mValue += parseFloat(debitDetails.totalMarkup);
          fareCurrency = debitDetails.sellCurrency;
          sellAmount += parseFloat(debitDetails.sellAmount);
          couponDiscount += parseFloat(debitDetails.totalCouponDiscount);
          baseamount += parseFloat(debitDetails.baseAmount);

          sellAmtBreakUp += '<tr><td>' + (ix + 1) + ". " + (fareBreakUp[ifb].category + " - " + (fareBreakUp[ifb].meal ? fareBreakUp[ifb].meal : '')) + '</td> <td>' + i18n.n(debitDetails.sellAmount / this.commonStore.currencyMultiplier, 'currency', this.commonStore.selectedCurrency) + '</td></tr>';
          netAmtBreakUp += '<tr><td>' + (ix + 1) + ". " + (fareBreakUp[ifb].category + " - " + (fareBreakUp[ifb].meal ? fareBreakUp[ifb].meal : '')) + '</td> <td>' + i18n.n(debitDetails.netAmount / this.commonStore.currencyMultiplier, 'currency', this.commonStore.selectedCurrency) + '</td></tr>';
          baseAmtBreakUp += '<tr><td>' + (ix + 1) + ". " + (fareBreakUp[ifb].category + " - " + (fareBreakUp[ifb].meal ? fareBreakUp[ifb].meal : '')) + '</td> <td>' + Math.ceil(debitDetails.baseAmount * 100) / 100 + " " + debitDetails.netCurrency + '</td></tr>';
        }
      }

      htmlBreakups += '<tr> <td class="fare_head_area">' + i18n.t('Hotel_Confirmation.SellAmount_Label') + amtBreakUp + sellAmtBreakUp + '</tbody></table></span></td> <td class="fare_head_area">' + i18n.n(sellAmount / this.commonStore.currencyMultiplier, 'currency', this.commonStore.selectedCurrency).match(/\d+(\.\d+)?/g).join('') + '</td></tr>';
      if (couponDiscount != 0) {
        htmlBreakups += '<tr> <td class="fare_head_area">' + i18n.t('Hotel_Confirmation.DiscountAmount_Label') + '</td>  <td class="fare_head_area">' +
          couponDiscount + '</td></tr>' +
          '<tr> <td class="fare_head_area">' + i18n.t('Hotel_Confirmation.TotalAmount_Label') + '</td>  <td class="fare_head_area">' +
          parseFloat(couponDiscount + sellAmount).toFixed(2) + '</td></tr>';
      }
      htmlBreakups += '<tr> <td class="fare_head_area">' + i18n.t('Hotel_Confirmation.SellCurrency_Label') + '</td>  <td class="fare_head_area">' + this.commonStore.selectedCurrency + '</td></tr>' +
        '<tr> <td class="fare_head_area">' + i18n.t('Hotel_Confirmation.NetAmount_Label') + amtBreakUp + netAmtBreakUp + '</tbody></table></span></td>  <td class="fare_head_area">' + i18n.n(netAmount / this.commonStore.currencyMultiplier, 'currency', this.commonStore.selectedCurrency).match(/\d+(\.\d+)?/g).join('') + '</td></tr>' +
        '<tr> <td class="fare_head_area">' + i18n.t('Hotel_Confirmation.NetCurrency_Label') + '</td>  <td class="fare_head_area">' + this.commonStore.selectedCurrency + '</td></tr>' +
        '<tr> <td class="fare_head_area">' + i18n.t('Hotel_Confirmation.BaseAmount_Label') + amtBreakUp + baseAmtBreakUp + '</tbody></table></span></td> <td class="fare_head_area">' + (baseamount % 1 == 0 ? baseamount : parseFloat(baseamount).toFixed(2)) + '</td></tr>' +
        '<tr> <td class="fare_head_area">' + i18n.t('Hotel_Confirmation.BaseCurrency_Label') + '</td> <td class="fare_head_area">' + baseCurrency + '</td></tr>' +
        '<tr> <td class="fare_head_area">' + i18n.t('Hotel_Confirmation.RateOfExchange_Label') + '</td>  <td class="fare_head_area">' + rateofExchange + '</td></tr>' +
        '<tr> <td class="fare_head_area">' + i18n.t('Hotel_Confirmation.TotalMarkup_Label') + '</td>  <td class="fare_head_area">' + i18n.n(mValue / this.commonStore.currencyMultiplier, 'currency', this.commonStore.selectedCurrency).match(/\d+(\.\d+)?/g).join('') + '</td></tr>';

      this.hotelPriceBreakUpHtml = htmlBreakups;
    },
    getPaxList: function (item) {
      var htmlTemp = "";
      var paxData = this.bookingInformation.bookingRoomPriceBreakUp[item.split(",")[0]].roomQuotes[item.split(",")[1]].guests;
      for (var i = 0; i < paxData.length; i++) {
        htmlTemp += '<tr><td>' + (i + 1) + ". " + paxData[i].titleName + " " + paxData[i].firstName + " " + paxData[i].surName + '</tr></td>';
      }
      this.paxListPerRoomHtml = htmlTemp;
    },
    getFareBreakUpPerRoom: function () {
      var total = parseFloat(0);
      var htmlTemp = '<thead> <tr> <th class="sp_table">' + i18n.t('Hotel_Confirmation.RoomsHeader_Label') + '</th> <th class="sp_table">' + i18n.t('Hotel_Confirmation.RoomCostHeader_Label') + '</th> </tr> </thead> <tbody>';
      var priceBreakup = this.bookingInformation.bookingRoomPriceBreakUp;

      for (var i = 0; i < priceBreakup.length; i++) {
        for (var j = 0; j < priceBreakup[i].roomQuotes.length; j++) {

          var fare = priceBreakup[i].roomQuotes[j].debitDetails.sellAmount;

          var amount = i18n.n(fare / this.commonStore.currencyMultiplier, 'currency', this.commonStore.selectedCurrency);

          var category = priceBreakup[i].category + " - " + (priceBreakup[i].meal ? priceBreakup[i].meal : '');
          total += parseFloat(fare);
          htmlTemp += '<tr><td style="max-width: 170px;">' + (i + 1) + '. ' + category + '</td><td>' + amount + '</td></tr>';
        }
      }

      total = i18n.n(total / this.commonStore.currencyMultiplier, 'currency', this.commonStore.selectedCurrency);
      htmlTemp += '</tbody>';
      var totalCost = i18n.t('Hotel_Confirmation.TotalCost_Label') + ' : ' + total;

      this.roomPriceBreakUpHtml.table = htmlTemp;
      this.roomPriceBreakUpHtml.totalCost = totalCost;

    },
    getCancellationPolicyModal: function () {

      this.roomDetails.cancellationDetails = this.getCancellationPolicy(this.reponseData);
      this.$nextTick(function () {
        var acc = document.getElementsByClassName("accordion");
        for (var j = 0; j < acc.length; j++) {
          acc[j].onclick = function () {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
              panel.style.maxHeight = null;
            } else {
              panel.style.maxHeight = panel.scrollHeight + "px";
            }
          };
        }
      })

    },
    cancelBooking: function (button) {
      var vm = this;
      alertify.confirm(i18n.t('Hotel_Confirmation.CancelBtn_Label'), i18n.t('Hotel_Confirmation.CancelBookingPopUp_Label'),
        function () {
          //Precancel
          button.showSpinnerHtml = true;
          vm.preCancel(button);
        },
        function () {
          button.showSpinnerHtml = false;
        }).set('labels', {
          ok: 'Yes',
          cancel: 'No'
        });
    },
    preCancel: function (button) {
      var vm = this;
      // Make a list of supplier that has no precancel request.
      var noPrecancelSuppliers = globalConfigs.services.hotels.suppliersWithPrecancel;

      var preCancelRoomReferences = {
        code: null,
        preCancelToken: null
      };

      var preCancelReferences = {
        preCancelReferenceId: null,
        oneviewReference: null,
        preCancelToken: null
      };

      if (noPrecancelSuppliers.indexOf(this.supplierCode) != -1) {

        var roomsResponse = this.reponseData.booking.hotels[0].rooms;
        var rooms = [];

        for (var i = 0; i < roomsResponse.length; i++) {
          var roomQuotes = roomsResponse[i].roomQuotes;

          for (var j = 0; j < roomQuotes.length; j++) {
            var room = roomQuotes[j];
            rooms.push({
              roomId: j + 1,
              roomBookReferences: {
                code: null,
                supplierReference: room.supplierReferenceNumber,
              },
              supplierSpecific: {
                cancelRefId: room.cancelRefId
              }
            });
          }
        }

        var preCancelRQ = {
          service: "HotelRQ",
          node: {
            currency: this.commonStore.agencyNode.loginNode.currency,
            agencyCode: this.commonStore.agencyNode.loginNode.code
          },
          token: window.localStorage.getItem("accessToken"),
          hubUuid: null,
          supplierCodes: [this.supplierCode],
          content: {
            command: "PreCancelRQ",
            hotel: {
              totalCost: this.bookingInformation.totalAmount,
              sealCode: null,
              rooms: rooms
            },
            supplierSpecific: {
              uuid: this.reponseData.booking.reference,
              bookingReferences: {
                oneviewReference: this.reponseData.booking.refId,
                bookingReferenceId: this.reponseData.booking.hotels[0].supplierReferenceNumber,
                cancelRefId: this.reponseData.booking.hotels[0].cancelRefId
              }
            }
          }
        }

        var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;

        var config = {
          axiosConfig: {
            method: "POST",
            url: hubUrl + "/preCancel",
            data: {
              request: preCancelRQ
            }
          },
          successCallback: function (response) {
            if (response.data) {
              var a = response.data.response.content.preCancelResponse.hotel.rooms;
              var b = response.data.response.content.supplierSpecific.preCancelReferences;

              vm.cancelRequest(a, b, button);

            } else {
              alertify.alert(i18n.t('Hotel_Confirmation.CancelBtn_Label'), i18n.t('Hotel_Confirmation.CancelBookingError_Label'));
              button.showSpinnerHtml = false;
            }
          },
          errorCallback: function (error) {
            // alertify.alert(i18n.t('Hotel_Confirmation.CancelBtn_Label'), i18n.t('Hotel_Confirmation.CancelBookingError_Label'));
            button.showSpinnerHtml = false;
          },
          showAlert: true
        }

        mainAxiosRequest(config);

      } else {
        this.cancelRequest(preCancelRoomReferences, preCancelReferences, button);
      }
    },
    cancelRequest: function (preCancelRoomReferences, preCancelReferences, button) {
      var vm = this;
      var roomsResponse = this.reponseData.booking.hotels[0].rooms;
      var rooms = [];

      for (var i = 0; i < roomsResponse.length; i++) {
        var roomQuotes = roomsResponse[i].roomQuotes;

        for (var j = 0; j < roomQuotes.length; j++) {
          var room = roomQuotes[j];
          rooms.push({
            roomId: i + 1,
            roomCost: room.debitDetails.sellAmount,
            sealCode: null,
            roomBookReferences: {
              code: null,
              supplierReference: room.supplierReferenceNumber
            },
            supplierSpecific: {
              cancelRefId: room.cancelRefId
            },
            preCancelRoomReferences: preCancelRoomReferences[i]
          });
        }
      }

      var cancelRQ = {
        service: "HotelRQ",
        node: {
          currency: this.commonStore.agencyNode.loginNode.currency,
          agencyCode: this.commonStore.agencyNode.loginNode.code
        },
        token: window.localStorage.getItem("accessToken"),
        hubUuid: null,
        supplierCodes: [this.supplierCode],
        content: {
          command: "CancelRQ",
          oneviewReference: this.reponseData.booking.refId,
          hotel: {
            totalCost: this.bookingInformation.totalAmount,
            sealCode: null,
            rooms: rooms
          },
          supplierSpecific: {
            uuid: this.reponseData.booking.reference,
            bookingReferences: {
              oneviewReference: this.reponseData.booking.refId,
              bookingReferenceId: this.reponseData.booking.hotels[0].supplierReferenceNumber,
              cancelRefId: this.reponseData.booking.hotels[0].cancelRefId
            },
            preCancelReferences: preCancelReferences
          }
        }
      };

      var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;

      var config = {
        axiosConfig: {
          method: "POST",
          url: hubUrl + "/cancel",
          data: {
            request: cancelRQ
          }
        },
        successCallback: function (response) {
          if (response.data) {
            var bookingStatus = response.data.response.content.cancelResponse.hotel.status;
            var bookingPrice = null;
            var responseReference = response.data.response.content.supplierSpecific.cancelReferences.bookingReferenceId;
            if (bookingPrice == null) {
              bookingPrice = 0;
            }
            if (responseReference == null) {
              responseReference = "xxx";
            }
            var bookingId = window.sessionStorage.getItem("bookingId");
            var supplierCode = window.sessionStorage.getItem("supplierId");
            vm.supplierCode ??= supplierCode;
            if (bookingStatus == "CANCELLED") {
              alertify.alert(i18n.t('Hotel_Confirmation.CancelBtn_Label'), i18n.t('Hotel_Confirmation.CancelBookingSuccess_Label'));
              vm.getHotelBookingInformation(bookingId);
            } else if (bookingStatus == "CANCELLATION PENDING") {
              alertify.alert(i18n.t('Hotel_Confirmation.CancelBtn_Label'), i18n.t("Hotel_Confirmation.CancellationPending_Label"));
              vm.getHotelBookingInformation(bookingId);
            } else {
              alertify.alert(i18n.t('Hotel_Confirmation.CancelBtn_Label'), i18n.t('Hotel_Confirmation.CancelBookingError_Label'));
            }
            button.showSpinnerHtml = false;
          } else {
            vm.cancelRequest(0, "xxx");
          }
        },
        errorCallback: function (error) {
          // alertify.alert(i18n.t('Hotel_Confirmation.CancelBtn_Label'), i18n.t('Hotel_Confirmation.CancelBookingError_Label'));
          button.showSpinnerHtml = false;
        },
        showAlert: true
      }

      mainAxiosRequest(config);

    },
    getParameterByName: function (name, url) {
      if (!url) url = window.location.href;
      name = name.replace(/[\[\]]/g, '\\$&');
      var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return '';
      return decodeURIComponent(results[2].replace(/\+/g, ' '));
    },
    getCancellationPolicy: function (hotelBookingInformation) {
      if (hotelBookingInformation) {

        var hotelCancellationPolicy = hotelBookingInformation.booking.hotels[0].cancellationPolicyList;
        var cancellationOutput = [];
        var rooms = hotelBookingInformation.booking.hotels[0].rooms;
        var vm = this;
        if (hotelCancellationPolicy.length > 0) {
          var cancellationOutputHotel = [];
          for (var i = 0; i < hotelCancellationPolicy.length; i++) {

            var localLocaleTo = moment(hotelCancellationPolicy[i].toDate);
            var localLocaleFrom = moment(hotelCancellationPolicy[i].fromDate);

            localLocaleTo.locale(this.commonStore.selectedLanguage ? this.commonStore.selectedLanguage.code : 'en');
            localLocaleFrom.locale(this.commonStore.selectedLanguage ? this.commonStore.selectedLanguage.code : 'en');

            var from = localLocaleFrom.format("MMM DD YYYY, dddd");
            var to = localLocaleTo.format("MMM DD YYYY, dddd");
            var amount = hotelCancellationPolicy[i].totalCost;

            if (parseInt(hotelCancellationPolicy[i].totalCost) == 0) {
              cancellationOutputHotel.push(i18n.t('Hotel_Confirmation.CancellationTextC1_Label') + ' ' + to);
            } else if (vm.bookingInformation.totalAmount == amount && hotelCancellationPolicy.length == 1) {
              cancellationOutputHotel.push(i18n.t('Hotel_Confirmation.CancellationTextB1_Label') + ' ' +
                i18n.n(amount / this.commonStore.currencyMultiplier, 'currency', this.commonStore.selectedCurrency));
            } else if (to != "" && from != "") {
              cancellationOutputHotel.push(i18n.t('Hotel_Confirmation.CancellationTextA1_Label') + ' ' + from + ' ' + i18n.t('Hotel_Confirmation.CancellationTextA2_Label') + ' ' + to + ' ' + i18n.t('Hotel_Confirmation.CancellationTextA3_Label') + ' ' +
                i18n.n(amount / this.commonStore.currencyMultiplier, 'currency', this.commonStore.selectedCurrency));
            }
          }

          cancellationOutput.push({
            roomName: rooms[0].category + " - " + (rooms[0].meal ? rooms[0].meal : ''),
            cancellation: cancellationOutputHotel
          });
        } else {
          for (var iP = 0; iP < rooms.length; iP++) {
            var xPolicy = rooms[iP].roomQuotes;
            for (var iarr = 0; iarr < xPolicy.length; iarr++) {
              var cancellationOutputPerRoom = [];
              for (var i = 0; i < xPolicy[iarr].cancellationPolicyList.length; i++) {
                var localLocaleTo = moment(xPolicy[iarr].cancellationPolicyList[i].toDate);
                var localLocaleFrom = moment(xPolicy[iarr].cancellationPolicyList[i].fromDate);

                localLocaleTo.locale(this.commonStore.selectedLanguage ? this.commonStore.selectedLanguage.code : 'en');
                localLocaleFrom.locale(this.commonStore.selectedLanguage ? this.commonStore.selectedLanguage.code : 'en');

                var from = localLocaleFrom.format("MMM DD YYYY, dddd");
                var to = localLocaleTo.format("MMM DD YYYY, dddd");
                var amount = xPolicy[iarr].cancellationPolicyList[i].totalCost;
                // var roomPrice = xPolicy[iarr].debitDetails.sellAmount;
                if (parseInt(xPolicy[iarr].cancellationPolicyList[i].totalCost) == 0) {
                  cancellationOutputPerRoom.push(i18n.t('Hotel_Confirmation.CancellationTextC1_Label') + ' ' + to);
                } else if (vm.bookingInformation.totalAmount == amount && xPolicy[iarr].cancellationPolicyList.length == 1) {
                  cancellationOutputPerRoom.push(i18n.t('Hotel_Confirmation.CancellationTextB1_Label') + ' ' +
                    i18n.n(amount / this.commonStore.currencyMultiplier, 'currency', this.commonStore.selectedCurrency));
                } else if (to != "" && from != "") {
                  cancellationOutputPerRoom.push(i18n.t('Hotel_Confirmation.CancellationTextA1_Label') + ' ' + from + ' ' + i18n.t('Hotel_Confirmation.CancellationTextA2_Label') + ' ' + to + ' ' + i18n.t('Hotel_Confirmation.CancellationTextA3_Label') + ' ' +
                    i18n.n(amount / this.commonStore.currencyMultiplier, 'currency', this.commonStore.selectedCurrency));
                }
              }
              cancellationOutput.push({
                roomName: rooms[iP].category + " - " + (rooms[iP].meal ? rooms[iP].meal : ''),
                cancellation: cancellationOutputPerRoom
              });
            }
          }
        }
      }
      return cancellationOutput;
    },
    getTemplate(template) {
      var url = this.commonStore.hubUrls.emailServices.getTemplate + "AGY75/" + template;
      // var url = this.commonStore.hubUrls.emailServices.getTemplate + vueCommonStore.state.agencyNode.loginNode.code + "/" + template;
      return axios.get(url);
    },
    getPdf(button) {
      if (button) {
        button.showSpinnerHtml = true;
      }
      var agencyNode = window.localStorage.getItem("agencyNode");
      if (agencyNode) {
        agencyNode = JSON.parse(atob(agencyNode));
      }
      var vm = this;
      var totalRooms = vm.roomDetails.room.length;
      var totalChildren = vm.roomDetails.room.reduce(function (a, c) {
        return a + c.childCount
      }, 0);
      var totalAdults = vm.roomDetails.room.reduce(function (a, c) {
        return a + c.adultCount
      }, 0);
      var postData = {
        logoUrl: vm.commonStore.agencyNode.loginNode.logo,
        agencyPhone: agencyNode.contactNumber || "",
        agencyAddress: agencyNode.loginNode.address || "",
        agencyEmail: agencyNode.loginNode.email || "",
        bookStatusImg: window.location.origin + (vm.isBookingSuccess ? "/assets/images/con-hotel.png" : "/assets/images/con-hote-errorl.png"),
        leadPaxName: vm.bookingInformation.bookingRoomPriceBreakUp[0].roomQuotes[0].guests[0].titleName + ' ' + vm.bookingInformation.bookingRoomPriceBreakUp[0].roomQuotes[0].guests[0].firstName + ' ' + vm.bookingInformation.bookingRoomPriceBreakUp[0].roomQuotes[0].guests[0].surName,
        statusText: vm.isBookingSuccess ? "Congratulations! Your hotel booking is confirmed" : "Sorry! Your hotel booking failed",
        bookRef: vm.bookingInformation.referenceNumber,
        hotelRef: vm.supplierInformation.supplierReferenceNumber || "Not Available",
        hotelName: vm.supplierInformation.hotelName,
        star: Array(vm.supplierInformation.starRating).fill(0),
        noStar: Array(5 - vm.supplierInformation.starRating).fill(0),
        hotelAddress: decodeURI(vm.supplierInformation.hotelAddress),
        checkInDay: moment(vm.bookingInformation.checkIn, "DD MMMM YYYY").format("dddd"),
        checkIndate: vm.bookingInformation.checkIn,
        checkOutDay: moment(vm.bookingInformation.checkOut, "DD MMMM YYYY").format("dddd"),
        checkOutDate: vm.bookingInformation.checkOut,
        nights: vm.bookingInformation.numberOfNights + (vm.bookingInformation.numberOfNights > 1 ? ' Nights' : ' Night'),
        totalAdults: totalAdults + (totalAdults > 1 ? ' Adults' : ' Adult'),
        totalChildren: totalChildren + (totalChildren > 1 ? ' Children' : ' Child'),
        totalRooms: totalRooms + (totalRooms > 1 ? ' Rooms' : ' Room'),
        rooms: vm.roomDetails.room.map(function (e, i) {
          return {
            id: "Room " + (i + 1),
            name: e.room,
            pax: e.allNames
          }
        }),
        roomCharge: i18n.n(vm.bookingInformation.totalAmount / vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency),
        tax: i18n.n(0 / vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency),
        totalPrice: i18n.n(vm.bookingInformation.totalAmount / vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency),
        cancellationPolicy: vm.roomDetails.cancellationDetails.map(function (e, i) {
          return {
            id: vm.roomDetails.cancellationDetails.length > 1 ? "Room " + (i + 1) : undefined,
            cancellation: e.cancellation
          }
        }),
        supplierRemarks: vm.bookingNotes
      }
      return vm.getTemplate("HotelPDFTemplate").then(function (response) {
        var data = response.data.data;
        var pdfTemplateId = "";
        var pdfFileName = "";
        if (data.length > 0) {
          for (var x = 0; x < data.length; x++) {
            if (data[x].enabled == true && data[x].type == "HotelPDFTemplate") {
              pdfTemplateId = data[x].id;
              pdfFileName = data[x].nodeCode;
            }
          }
        };
        var generatePdf = vm.commonStore.hubUrls.emailServices.generatePdf
        var pdfData = {
          templateID: pdfTemplateId,
          filename: pdfFileName,
          content: postData
        };
        return axios.post(generatePdf, pdfData)
          .then(function (response) {
            if (button) {
              button.showSpinnerHtml = false;
              vm.downloadPdf(response.data)
            } else {
              return response.data;
            }
          }).catch(function (error) {
            if (button) {
              button.showSpinnerHtml = false;
            } else {
              return false;
            }
          })
      }).catch(function (error) {
        if (button) {
          button.showSpinnerHtml = false;
        } else {
          return false;
        }
      });
    },
    downloadPdf: function (response) {
      try {
        var link = document.createElement('a');
        link.href = response.data;
        link.download = response.data.split('/').pop();
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
        this.processingPdf = false;
      } catch (error) {
        alertify.alert(this.Alert_Warning_Label, "Error in generating pdf file.").set('label', this.Ok_Label);
        this.processingPdf = false;
      }
    },
    requestOfflineCancellation() {
      var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;

      axios.get(hubUrl + "/api/values")
        .then(response => {
          console.log(response.data);
        })
        .catch(error => {
          if (error.response) {
            // The request was made and the server responded with a status code
            // that falls out of the range of 2xx
            alertify.alert("Error", error.response.data.message)
            console.log("Server error: ", error.response.data);
          } else if (error.request) {
            // The request was made but no response was received
            // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
            // http.ClientRequest in node.js
            console.log("No response received: ", error.request);
          } else {
            // Something happened in setting up the request that triggered an Error
            console.log("Error", error.message);
          }
        })
        .finally(() => {
          console.log("Finally");
        });
    },
    emailVoucher: function (btnId) {
      var vm = this;
      if (vm.emailToVoucher) {
        vm.header.callToActionButtons[__.findIndex(vm.header.callToActionButtons, function (b) {
          return b.id === btnId
        })].showSpinnerHtml = true;
        var toEmails = Array.isArray(vm.emailToVoucher) ? vm.emailToVoucher : [vm.emailToVoucher];

        vm.emailBookingDetails(toEmails);

      } else {
        alertify.alert(i18n.t('Hotel_Confirmation.AlertifyHeader1_Label'), i18n.t('Hotel_Confirmation.NoEmail_Label'));
      }
    },
    editVoucher: function () {
      var vm = this;
      var URL = "/Hotels/EmailTemplates/booking/editVoucher.html";
      axios.get(URL)
        .then(response => {
          var printBody = response.data;
          var tableDetails = "";
          for (var i = 0; i < vm.roomDetailsTable.length; i++) {
            tableDetails += '<tr>';
            tableDetails += '<td style="color: #555555;padding: 8px;line-height: 1.42857143;vertical-align: top;">' + (i + 1) + '</td>';
            tableDetails += '<td style="color: #555555;max-width: 220px;padding: 8px;line-height: 1.42857143;vertical-align: top;">' + vm.roomDetailsTable[i].roomType + '</td>';
            tableDetails += '<td style="color: #555555;padding: 8px;line-height: 1.42857143;vertical-align: top;">' + vm.roomDetailsTable[i].traveller + '</td>';
            tableDetails += '<td style="color: #555555;padding: 8px;line-height: 1.42857143;vertical-align: top;">' + vm.roomDetailsTable[i].guestName + '</td>';
            tableDetails += '</tr>';
          }

          var cancellationPolicy = "";

          for (var j = 0; j < vm.roomDetails.cancellationDetails.length; j++) {
            cancellationPolicy += '<div>';
            if (vm.roomDetails.cancellationDetails.length > 1) {
              cancellationPolicy += '<p style="display: block;font-size: 13px;color: #444;font-weight: 300;padding: 3px 0 0;line-height: 18px;margin:0;"><b>Room ' + (j + 1) + '</b></p>';
            }
            for (var k = 0; k < vm.roomDetails.cancellationDetails[j].cancellation.length; k++) {
              cancellationPolicy += '<p style="display: block;font-size: 13px;color: #444;font-weight: 300;padding: 3px 0 0;line-height: 18px;margin:0;">' + vm.roomDetails.cancellationDetails[j].cancellation[k]; + '</p>';
            }
            cancellationPolicy += '</div>';
          }

          cancellationPolicy += "<div>*Cancellation policies are based on date and time of destination.</div>";

          var bookingNotes = "";
          if (vm.bookingNotes != '') {
            bookingNotes = '<div style="width:100%;float:left;border:1px solid #ddd;margin-bottom:10px;border-bottom:none">' +
              '<div style="width:100%;float:left;background:#efefef;border-bottom:1px solid #ddd;display:flex">' +
              '<div style="width:100%;float:left;padding:1%;font-family:Gill Sans,Gill Sans MT,Calibri,Trebuchet MS,sans-serif;font-size:14px;color:#333;text-align:left;font-weight:600;border-right:1px solid #ddd">Booking Notes</div>' +
              '</div>' +
              '<div style="width:100%;float:left;border-bottom:1px solid #ddd;display:flex">' +
              '<div style="width:100%;float:left;padding:1%;font-family:Gill Sans,Gill Sans MT,Calibri,Trebuchet MS,sans-serif;font-size:14px;color:#333;text-align:left;font-weight:300;border-right:1px solid #ddd">' +
              (vm.bookingNotes || '').replace('<br>', '') + '</div>' +
              '</div>' +
              '</div>';
          }

          var addionalInformation = "";
          if (vm.additionalSupplierInformation.length > 0) {
            addionalInformation +=
              '<div style="width:100%;float:left;border:1px solid #ddd;margin-bottom:10px;">' +
              '<div style="width:100%;float:left;background:#efefef;border-bottom:1px solid #ddd;display:flex">' +
              '<div style="width:100%;float:left;padding:1%;font-family:Gill Sans,Gill Sans MT,Calibri,Trebuchet MS,sans-serif;font-size:14px;color:#333;text-align:left;font-weight:600;">Additional Information</div></div>';
            for (var i = 0; i < vm.additionalSupplierInformation.length; i++) {
              addionalInformation +=
                '<div style="width:100%;float:left;padding:0 10px;font-family:Gill Sans,Gill Sans MT,Calibri,Trebuchet MS,sans-serif;font-size:14px;color:#333;text-align:left;font-weight:300;"><h4>' + vm.additionalSupplierInformation[i].name.replace(/_/g, ' ') + '</h4></div>';
              for (var j = 0; j < vm.additionalSupplierInformation[i].data.length; j++) {
                addionalInformation += '<div style="width:100%;float:left;padding:0 10px;font-family:Gill Sans,Gill Sans MT,Calibri,Trebuchet MS,sans-serif;font-size:14px;color:#333;text-align:left;font-weight:300;">' + vm.additionalSupplierInformation[i].data[j] + '</div>';
              }
            }
            addionalInformation += '</div></div></div>';
          }

          var agencyNode = window.localStorage.getItem("agencyNode");
          if (agencyNode) {
            agencyNode = JSON.parse(atob(agencyNode));
          }

          var logoUrl = '';
          try {
            logoUrl = agencyNode.loginNode.logo
          } catch (err) { }
          printBody = printBody.replace('#LOGO#', logoUrl);
          printBody = printBody.replace('#PHONE#', vm.supportDetails.phone);
          printBody = printBody.replace('#STATUSICON#', vm.isBookingSuccess ? "../assets/images/con-hotel.png" : "../assets/images/con-hote-errorl.png");
          printBody = printBody.replace(/#LEADPAXNAME#/g, vm.bookingInformation.bookingRoomPriceBreakUp[0].roomQuotes[0].guests[0].firstName + ' ' + vm.bookingInformation.bookingRoomPriceBreakUp[0].roomQuotes[0].guests[0].surName);
          printBody = printBody.replace('#BOOKINGSTATUSMESSAGE#', (vm.isBookingSuccess ? "Congratulations! Your hotel booking is " : "Sorry! Your hotel booking ") + vm.bookingInformation.bookingStatus);
          printBody = printBody.replace('#BOOKINGREFNUMBER#', vm.bookingInformation.referenceNumber);
          printBody = printBody.replace('#SUPPLIERREFNUMBER#', vm.supplierInformation.supplierReferenceNumber || "Not Available");
          printBody = printBody.replace('#HOTELNAME#', vm.supplierInformation.hotelName);
          printBody = printBody.replace('#HOTELADDRESS#', decodeURI(vm.supplierInformation.hotelAddress));
          printBody = printBody.replace('#CHECKINDAY#', moment(vm.bookingInformation.checkIn, "DD MMMM YYYY").format("dddd"));
          printBody = printBody.replace('#CHECKINDATE#', vm.bookingInformation.checkIn);
          printBody = printBody.replace('#CHECKOUTDAY#', moment(vm.bookingInformation.checkOut, "DD MMMM YYYY").format("dddd"));
          printBody = printBody.replace('#CHECKOUTDATE#', vm.bookingInformation.checkOut);
          printBody = printBody.replace('#TABLECONTENTS#', tableDetails);
          printBody = printBody.replace('#ROOMCHARGES#', i18n.n(vm.bookingInformation.totalAmount / vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency));
          printBody = printBody.replace('#TAX#', i18n.n(0 / vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency));
          printBody = printBody.replace('#TOTAL#', i18n.n(vm.bookingInformation.totalAmount / vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency));
          printBody = printBody.replace('#CANCELLATIONPOLICY#', cancellationPolicy);
          printBody = printBody.replace('#BOOKINGNOTES#', bookingNotes);
          printBody = printBody.replace('#ADDITIONALINFORMATION#', addionalInformation);
          printBody = printBody.replace('#ADDRESS#', vm.supportDetails.address);
          printBody = printBody.replace('#SUPORTPHONE#', vm.supportDetails.phone);
          printBody = printBody.replace('#EMAIL#', vm.supportDetails.email);

          $('#voucherHtmlEdit').summernote({
            toolbar: [
              ['style', ['bold', 'italic', 'underline', 'clear']],
              ['font', ['strikethrough', 'superscript', 'subscript']],
              ['fontstyle', ['fontsize', 'fontname', 'color']],
              ['para', ['ul', 'ol', 'paragraph', 'height']],
              ['Edit', ['undo', 'redo']],
              ['insert', ['table', 'hr']],
              ['help', ['codeview', 'help']]
            ]
          });
          $('#voucherHtmlEdit').summernote('code', printBody);
        }).catch(function (error) {
          console.log('Error');
          this.content = [];
        });

    },
    sendEditedVoucher: function (btnId) {
      if (this.emailToEditVoucher) {
        vm.header.callToActionButtons[__.findIndex(vm.header.callToActionButtons, function (b) {
          return b.id === btnId
        })].showSpinnerHtml = true;
        $('#voucherHtmlEdit').summernote('code');
      } else {
        alertify.alert(i18n.t('Hotel_Confirmation.AlertifyHeader1_Label'), i18n.t('Hotel_Confirmation.NoEmail_Label'));
      }
    }
  },
  watch: {
    "commonStore.selectedCurrency": function () {
      this.roomDetails.cancellationDetails = this.getCancellationPolicy(this.reponseData);
    },
    "commonStore.selectedLanguage": function (newVal, oldVal) {
      if (oldVal.code != newVal.code) {

        var roomQuotes = this.reponseData.booking.hotels[0].rooms[0].roomQuotes[0];
        // this.header.bookingStatus = i18n.t('Hotel_Confirmation.BookingStatusHeader1_Label') + " " + this.reponseData.booking.hotels[0].name.toUpperCase() + " " + i18n.t('Hotel_Confirmation.BookingStatusHeader2_Label') + " " + roomQuotes.status.toUpperCase();

        var localeMoment = this.commonStore.selectedLanguage ? this.commonStore.selectedLanguage.code : 'en';

        var roomQuotes = this.reponseData.booking.hotels[0].rooms[0].roomQuotes[0];

        this.bookingInformation.referenceNumber = this.reponseData.booking.refId;
        this.bookingInformation.bookingDate = moment(this.reponseData.booking.bookDate).locale(localeMoment).format("DD MMMM YYYY");
        this.bookingInformation.checkIn = moment(roomQuotes.checkInDate).locale(localeMoment).format("DD MMMM YYYY");
        this.bookingInformation.cancellationDeadline = moment(this.reponseData.booking.deadlineDate).locale(localeMoment).format("DD MMMM YYYY");
        this.bookingInformation.bookingStatus = roomQuotes.status;
        this.bookingInformation.numberOfNights = moment.duration(new Date(moment(roomQuotes.checkOutDate).format("DD MMM YYYY")) - new Date(moment(roomQuotes.checkInDate).format("DD MMM YYYY"))).days();
        this.bookingInformation.checkOut = moment(roomQuotes.checkOutDate).locale(localeMoment).format("DD MMMM YYYY");
        this.header.bookingDate = moment(this.reponseData.booking.bookDate).locale(localeMoment).format("DD MMMM YYYY");

      }

      if (this.commonStore.selectedLanguage && this.commonStore.selectedLanguage.code == 'ar') {
        this.spinnerHtml = '<span class="loader__dot">.</span><span class="loader__dot">.</span><span class="loader__dot">.</span>معالجة';
      }


    }
  },
  computed: {}
});