Vue.prototype.$eventHub = new Vue({
    data: {
        selectedHotel: {}
    }
});

var autoCompleteMap = Vue.component('map-autocomplete', {
    props: {
        latitude: Number,
        longitude: Number,
        directive: String,
        locationName: String
    },

    data() {
        return {
            search: "",
            results: [],
            isOpen: false,
            isLoading: false
        };
    },

    methods: {
        onChange: __.debounce(function () {
            this.isLoading = true;
            this.isOpen = true;
            var rqUrl = HubServiceUrls.hubConnection.proxyUrl + "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=" + this.search + "&location=" + this.latitude + "," + this.longitude + "&radius=50000&strictbounds&key=AIzaSyBoOq8WuZ48t8uO4_XK_ACLJyIsOLX0F58";
            axios.get(rqUrl, {
                headers: {
                    'content-type': 'application/json'
                }
            }).then(response => {
                console.log(response.data);
                this.isLoading = false;

                this.results = __.map(response.data.predictions, function (results) {
                    return {
                        name: results.structured_formatting.main_text,
                        id: results.place_id
                    }
                });
            })

        }, 500),
        setResult: function (result) {
            var vm = this;
            vm.search = result.name;
            vm.isOpen = false;
            var rqUrl = HubServiceUrls.hubConnection.proxyUrl + "https://maps.googleapis.com/maps/api/place/details/json?placeid=" + result.id + "&fields=geometry&key=AIzaSyBoOq8WuZ48t8uO4_XK_ACLJyIsOLX0F58";

            axios.get(rqUrl, {
                headers: {
                    'content-type': 'application/json'
                }
            }).then(response => {
                console.log(response.data);
                var directions = {
                    location: {
                        latitude: response.data.result.geometry.location.lat,
                        longitude: response.data.result.geometry.location.lng
                    },
                    locationName: result.name,
                    directive: vm.directive
                }
                vm.$eventHub.$emit('select-directions', directions);


            })

        },
        handleClickOutside: function (evt) {
            if (!this.$el.contains(evt.target)) {
                this.isOpen = false;
            }
        }
    },
    watch: {
        locationName: function () {
            this.search = this.locationName;
        }
    },
    mounted() {
        document.addEventListener('click', this.handleClickOutside)
    },
    destroyed() {
        document.removeEventListener('click', this.handleClickOutside)
    },
    template: `
    <div class="autocomplete">
      <input type="text" class="txt_direction1 desti_place" :placeholder="directive=='to'?$t('Hotel_Results.MapDirectionsTo_Label'):$t('Hotel_Results.MapDirectionsFom_Label')" 
      @input="onChange" v-model="search"/>
      <ul id="autocomplete-results" v-show="isOpen" class="autocomplete-results">
        <li class="loading" v-if="isLoading">Loading results...</li>
        <li v-else v-for="(result, i) in results" :key="i" @click="setResult(result)" 
          class="autocomplete-result">
          {{ result.name }}
        </li>
      </ul>
    </div>
  `
});

var infoWindowButton = Vue.extend({
    i18n,
    methods: {
        infoWindowButtonClicked: function () {
            this.$eventHub.$emit('select-hotel-map', this.hotelCode, this.supplierId);
        },
        imgUrlAlt: function (event) {
            event.target.src = "/Hotels/HotelComponents/assets/images/noimage.gif";
        },
        directionsFrom: function (location, locationName) {
            var directions = {
                location: location,
                locationName: locationName,
                directive: "from"
            }
            this.$eventHub.$emit('select-directions', directions);
        },
        directionsTo: function (location, locationName) {
            var directions = {
                location: location,
                locationName: locationName,
                directive: "to"
            }
            this.$eventHub.$emit('select-directions', directions);
        }
    },
    props: {
        imageUrl: String,
        hotelName: String,
        // currency: String,
        rate: String,
        starRating: String,
        hotelCode: String,
        location: Object,
        hideDirectioBtn: Boolean,
        supplierId: Number,
        commonStore: Object
    },
    template: `
    <div class="map_details">
      <div class="hotel_img"><img :src="imageUrl||'/Hotels/HotelComponents/assets/images/noimage.gif'" alt="noimage" @error="imgUrlAlt"></div>
        <div class="hotel_dis">
        <h3>{{hotelName}}
            <span class="float-star">
              <i class="fa" :class="parseInt(starRating)>itemStar?'fa-star':'fa-star-o'" aria-hidden="true" v-for="(itemStar, index) in 5" :key="index"></i>
            </span>
        </h3>
        <div class="hotel_location"><i class="fa fa-map-marker sp01" aria-hidden="true"></i>
        <span>{{location.address}}</span></div>
        <div class="direction_btn">
           <button v-show="hideDirectioBtn" class="direction_btn1" @click="directionsFrom(location, hotelName)">{{$t('Hotel_Results.MapDirectionsFom_Label')}}</button>
           <button v-show="hideDirectioBtn" class="direction_btn2" @click="directionsTo(location, hotelName)">{{$t('Hotel_Results.MapDirectionsTo_Label')}}</button>
        </div>
        <div class="clearfix"></div>
        <div>
          <div class="map_rate"><span>{{ $n(rate / commonStore.currencyMultiplier, 'currency', commonStore.selectedCurrency)}}</span></div>
          <button class="map_hotel_select_btn" @click="infoWindowButtonClicked">{{$t('Hotel_Results.MapSelectBtn_Label')}}</button>
        </div>
      </div>
    </div>
  `
});

var hotelListMap = Vue.extend({
    i18n,
    methods: {
        hotelMapDetailsEnter: function (marker) {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        },
        hotelMapDetailLeave: function (markers, index) {
            markers[index].marker.setAnimation(null);
        },
        hotelMapDetailsButton: function (hotel, latitude, longitude) {
            var vm = this;

            var marker;
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 16,
                center: new google.maps.LatLng(latitude, longitude),
                mapTypeControl: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            var infowindow = new google.maps.InfoWindow();
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(latitude, longitude),
                icon: "/Hotels/HotelComponents/assets/images/marker_icon.png",
                map: map
            });

            var mapInfoHtml = new infoWindowButton({
                propsData: {
                    imageUrl: hotel.imageUrl,
                    hotelName: hotel.name,
                    // currency: hotel.currency,
                    rate: hotel.rate,
                    starRating: hotel.starRating,
                    hotelCode: hotel.hotelCode,
                    location: hotel.location,
                    hideDirectioBtn: true,
                    supplierId: parseInt(hotel.supplierCode),
                    commonStore: vm.commonStore
                }
            }).$mount().$el

            google.maps.event.addListener(marker, 'click', (function (marker) {
                return function () {
                    infowindow.setContent(mapInfoHtml);
                    infowindow.open(map, marker);
                };
            })(marker));


            google.maps.event.addListener(marker, 'mouseover', (function (marker) {
                return function () {
                    infowindow.setContent(mapInfoHtml);
                    infowindow.open(map, marker);
                };
            })(marker));

            // assuming you also want to hide the infowindow when user mouses-out

            // google.maps.event.addListener(marker, 'mouseout', (function () {
            //   return function () {
            //     infowindow.close();
            //   };
            // })(marker));

            vm.$eventHub.$emit('select-poi', {
                hotel: hotel.name,
                latitude: latitude,
                longitude: longitude
            });

            var directions = {
                location: hotel.location,
                locationName: hotel.name,
                directive: "from"
            }
            vm.$eventHub.$emit('select-directions', directions);
        },
        imgUrlAlt: function (event) {
            event.target.src = "/Hotels/HotelComponents/assets/images/noimage.gif";
        }
    },
    props: {
        hotels: Array,
        markers: Array,
        commonStore: Object
    },
    template: `
  <div id="map_list_parent">
    <div id="map_list"> 
      <ul class="content">
        <li class="hotel_view_area_map" v-for="(item, index) in hotels" :key="index" @mouseleave="hotelMapDetailLeave(markers, index)" @mouseenter="hotelMapDetailsEnter(markers[index].marker)" @click="hotelMapDetailsButton(item, item.location.latitude, item.location.longitude)">
          <div class="list_htlImg"><img :src="item.imageUrl||'/Hotels/HotelComponents/assets/images/noimage.gif'" alt="hotels" @error="imgUrlAlt"></div>
          <h2>{{item.name}}</h2><h3><span>{{ $n(item.rate / commonStore.currencyMultiplier, 'currency', commonStore.selectedCurrency)}}</span></h3>
          <div style="color:#FEB810;">
            <h6>
              <span class="float-star">
                <i class="fa" :class="parseInt(item.starRating)>itemStar?'fa-star':'fa-star-o'" aria-hidden="true" v-for="(itemStar, index) in 5" :key="index"></i>
              </span>
            </h6>
          </div>
        </li>
      </ul>
    </div>
  </div>
  `
});


var hotelsMap = Vue.component("hotels-map", {
    components: {
        hotelListMap
    },
    props: {
        hotels: Array,
        hideMap: Function,
        searchCriteriaLocation: Object,
        commonStore: Object
    },
    mounted() {
        this.getMap();
    },
    methods: {
        getMap: function () {
            var lat = "90";
            var lon = "-90";
            try {
                lat = this.citySearchLocation.lat;
                lon = this.citySearchLocation.lon;
            } catch (error) {}

            var element = document.getElementById("map");
            var options = {
                zoom: 11,
                center: new google.maps.LatLng(lat, lon),
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false
            };
            var map = new google.maps.Map(element, options);

            var infowindow = new google.maps.InfoWindow();
            var markers = [];
            var mapInfoHtml = [];
            for (var i = 0; i < this.filteredHotel.length; i++) {

                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(this.filteredHotel[i].location.latitude, this.filteredHotel[i].location.longitude),
                    icon: "/Hotels/HotelComponents/assets/images/marker_icon.png",
                    map: map
                });
                markers.push({
                    marker: marker,
                    index: i
                });
                mapInfoHtml[i] = new infoWindowButton({
                    propsData: {
                        imageUrl: this.filteredHotel[i].imageUrl,
                        hotelName: this.filteredHotel[i].name,
                        // currency: this.filteredHotel[i].currency,
                        rate: this.filteredHotel[i].rate,
                        starRating: this.filteredHotel[i].starRating,
                        hotelCode: this.filteredHotel[i].hotelCode,
                        location: this.filteredHotel[i].location,
                        hideDirectioBtn: true,
                        supplierId: parseInt(this.filteredHotel[i].supplierCode),
                        commonStore: this.commonStore
                    }
                }).$mount().$el;

                // google.maps.event.addListener(marker, 'mouseover', (function (marker, i) {
                //   return function () {
                //     infowindow.setContent(mapInfoHtml[i]);
                //     infowindow.open(map, marker);
                //   };
                // })(marker, i));

                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(mapInfoHtml[i]);
                        infowindow.open(map, marker);
                    };
                })(marker, i));

            }

            this.gmapMarkers = markers;
            this.isSingleHotelOnMap = false;
            this.showPOI = false;
            this.showPrintMap = false;
            this.showDirections = false;
            this.showFilter = true;
            this.showDirectionsList = false;

        },
        selectDirections: function (directions) {
            if (directions.directive == "to") {
                this.directionTo.locationName = directions.locationName;
                this.directionTo.latitude = parseFloat(directions.location.latitude);
                this.directionTo.longitude = parseFloat(directions.location.longitude);
            } else {
                this.directionFrom.locationName = directions.locationName;
                this.directionFrom.latitude = parseFloat(directions.location.latitude);
                this.directionFrom.longitude = parseFloat(directions.location.longitude);
            }
            this.showDirections = true;
        },
        getDirections: function () {
            var vm = this;
            this.$nextTick(function () {
                // Code that will run only after the
                // entire view has been rendered
                var directionsDisplay = new google.maps.DirectionsRenderer({
                    preserveViewport: true
                });
                var directionsService = new google.maps.DirectionsService;
                var start = new google.maps.LatLng(this.directionFrom.latitude, this.directionFrom.longitude);
                var end = new google.maps.LatLng(this.directionTo.latitude, this.directionTo.longitude);

                var mapOptions = {
                    mapTypeControl: false,
                    zoom: 14,
                    center: new google.maps.LatLng(this.directionFrom.latitude, this.directionFrom.longitude)
                };

                var map = new google.maps.Map(document.getElementById('map'), mapOptions);
                directionsDisplay.setMap(map);

                var tempComponent = Vue.extend({
                    mounted: function () {
                        this.$nextTick(function () {
                            directionsDisplay.setPanel(document.getElementById('tempHtml'));
                        })
                    },
                    template: '<div id="tempHtml"></div>'
                })

                var htmlTemp = new tempComponent().$mount();

                var node = document.getElementById('directions_list');
                while (node.hasChildNodes()) {
                    node.removeChild(node.firstChild);
                }
                node.appendChild(htmlTemp.$el);

                this.showPrintMap = true;
                this.showDirectionsList = true;

                var request = {
                    origin: start,
                    destination: end,
                    // Note that Javascript allows us to access the constant
                    // using square brackets and a string value as its
                    // "property."
                    travelMode: google.maps.TravelMode[this.selectedMode]
                };
                directionsService.route(request, function (response, status) {

                    if (status == 'OK') {
                        directionsDisplay.setDirections(response);
                        vm.showFilter = false;
                    }
                });

            });
        },
        printMap: function () {

            const $body = $('body');
            const $mapContainer = $('.map-container');
            var divToPrint = document.getElementById('directions_list');
            const $mapContainerParent = $mapContainer.parent();
            const $printContainer = $('<div style="position:relative;">' + divToPrint.innerHTML);

            $printContainer.height($mapContainer.height()).prepend($mapContainer).prependTo($body);

            const $content = $body.children().not($printContainer).not('script').detach();

            /**
             * Needed for those who use Bootstrap 3.x, because some of
             * its `@media print` styles ain't play nicely when printing.
             */
            const $patchedStyle = $('<style media="print">')
                .text('img { max-width: none !important; } a[href]:after { content: ""; }')
                .appendTo('head');

            window.print();

            $body.prepend($content);
            $mapContainerParent.prepend($mapContainer);

            $printContainer.remove();
            $patchedStyle.remove();
        },
        selectPoi: function (data) {
            this.isSingleHotelOnMap = true;
            this.poiData = data;
        },
        getMapPOI: function () {
            vm = this;
            vm.showFilter = false;
            vm.showPOI = true;
            vm.showDirectionsList = false;

            var locations = ['airport', 'restaurant', 'shopping_mall', 'amusement_park', 'department_store'];
            for (var j = 0; j < locations.length; j++) {

                var center = new google.maps.LatLng(vm.poiData.latitude, vm.poiData.longitude);

                var map = new google.maps.Map(document.getElementById('mapdummy'), {
                    center: center,
                    mapTypeControl: false,
                    zoom: 15
                });

                var request = {
                    location: center,
                    radius: 500,
                    type: [locations[j]]
                };

                var service = new google.maps.places.PlacesService(map);

                service.nearbySearch(request, function (results, status, pagination) {
                    if (status == google.maps.places.PlacesServiceStatus.OK) {

                        var tempComponent = Vue.extend({
                            data: function () {
                                return {
                                    poiService: null,
                                    airport: results.filter(function (e) {
                                        return e.types[0] == 'airport'
                                    }),
                                    restaurant: results.filter(function (e) {
                                        return e.types[0] == 'restaurant'
                                    }),
                                    establishment: results.filter(function (e) {
                                        return e.types[0] == 'establishment' || e.types[0] == 'department_store'
                                    }),
                                    amusementPark: results.filter(function (e) {
                                        return e.types[0] == 'amusement_park'
                                    }),
                                    shoppingMall: results.filter(function (e) {
                                        return e.types[0] == 'shopping_mall'
                                    })
                                }
                            },
                            methods: {
                                showCertainPOI: function (service) {
                                    if (this.poiService == service) {
                                        this.poiService = null;
                                    } else {
                                        this.poiService = service;
                                    }
                                },
                                selectPoi: function (place) {
                                    var directions = {
                                        location: {
                                            latitude: place.geometry.location.lat(),
                                            longitude: place.geometry.location.lng()
                                        },
                                        locationName: place.name,
                                        directive: "to"
                                    }
                                    this.$eventHub.$emit('select-directions', directions);
                                }
                            },
                            template: `
              <ul id="nav">
                <li v-if="airport.length>0">
                  <a href="#" @click.prevent="showCertainPOI('airport')">Airports<i class="fa fa-angle-down"></i></a>
                  <transition name="slide">
                    <ul v-show="poiService=='airport'">
                      <li v-for="(place, index) in airport" :key="index"><a href="#" @click.prevent="selectPoi(place)">{{place.name}}</a></li>
                    </ul>
                  </transition>
                </li>
                <li v-if="restaurant.length>0">
                  <a href="#" @click.prevent="showCertainPOI('locality')">Restaurant<i class="fa fa-angle-down"></i></a>
                  <transition name="slide">
                    <ul v-show="poiService=='locality'">
                      <li v-for="(place, index) in restaurant" :key="index"><a href="#" @click.prevent="selectPoi(place)">{{place.name}}</a></li>
                    </ul>
                    </transition>
                </li>
                <li v-if="establishment.length>0">
                  <a href="#" @click.prevent="showCertainPOI('premise')">Establishment<i class="fa fa-angle-down"></i></a>
                  <transition name="slide">
                    <ul v-show="poiService=='premise'">
                      <li v-for="(place, index) in establishment" :key="index"><a href="#" @click.prevent="selectPoi(place)">{{place.name}}</a></li>
                    </ul>
                  </transition>
                </li>
                <li v-if="amusementPark.length>0">
                  <a href="#" @click.prevent="showCertainPOI('islands')">Attractions<i class="fa fa-angle-down"></i></a>
                  <transition name="slide">
                    <ul v-show="poiService=='islands'">
                      <li v-for="(place, index) in amusementPark" :key="index"><a href="#" @click.prevent="selectPoi(place)">{{place.name}}</a></li>
                    </ul>
                  </transition>
                </li>
                <li v-if="shoppingMall.length>0">
                  <a href="#" @click.prevent="showCertainPOI('shoppingMalls')">Shopping Malls<i class="fa fa-angle-down"></i></a>
                  <transition name="slide">
                    <ul v-show="poiService=='shoppingMalls'">
                      <li v-for="(place, index) in shoppingMall" :key="index"><a href="#" @click.prevent="selectPoi(place)">{{place.name}}</a></li>
                    </ul>
                  </transition>
                </li>
              </ul>
              `
                        })

                        var htmlTemp = new tempComponent().$mount();
                        try {
                            var poi_mapNode = document.getElementById('poi_map');
                            while (poi_mapNode.hasChildNodes()) {
                                poi_mapNode.removeChild(poi_mapNode.firstChild);
                            }
                            poi_mapNode.appendChild(htmlTemp.$el);
                        } catch (error) {
                            // no poi
                        }
                    }
                });
            }


        }
    },
    data: function () {
        return {
            searchHotel: null,
            filteredHotel: this.hotels.filter(function (e) {
                return e.location.latitude != undefined;
            }),
            showList: true,
            showFilter: true,
            showPOI: false,
            showPrintMap: false,
            poiData: {
                hotel: null,
                latitude: this.searchCriteriaLocation.lat,
                longitude: this.searchCriteriaLocation.lon
            },
            showDirections: false,
            showDirectionsList: false,
            isSingleHotelOnMap: false,
            directionTo: {
                locationName: null,
                latitude: null,
                longitude: null
            },
            directionFrom: {
                locationName: null,
                latitude: null,
                longitude: null
            },
            selectedMode: "DRIVING",
            citySearchLocation: this.searchCriteriaLocation,
            gmapMarkers: null
        };
    },
    watch: {
        searchHotel: __.debounce(function (newVal, old) {
            this.filteredHotel = this.hotels.filter(hotel => {
                return hotel.name.toLowerCase().includes((newVal || "").toLowerCase())
            });

            this.filteredHotel = this.filteredHotel.filter(function (e) {
                return e.location.latitude != undefined;
            });

            this.getMap();

        }, 200)
    },
    created: function () {
        this.$eventHub.$on('select-directions', this.selectDirections);
        this.$eventHub.$on('select-poi', this.selectPoi);
    },
    beforeDestroy() {
        this.$eventHub.$off('select-directions');
        this.$eventHub.$off('select-poi');
    },
    template: `
  <!--Map view-->
  <div class="container-fluid">
    <div class="row">
      <div id="fullviewmap" class="view_map">
        <div class="htl_list">
          <div class="htl_view" @click="showList=false" v-if="showList"><i id="sp_icon" class="fa fa-caret-left"></i></div>
          <div class="htl_view" @click="showList=true" v-if="!showList"><i id="sp_icon" class="fa fa-caret-right"></i></div>
          <div class="web_view" @click="getMap" title="Map"><i id="sp_icon" class="fa fa-bed"></i></div>
          <div class="globe_view" @click="getMapPOI" title="POIs"><i id="sp_icon3" class="fa fa-globe"></i></div>
          <div class="print_view" @click="printMap" v-if="showPrintMap" title="Print"><i id="sp_icon2" class="fa fa-print"></i></div>
          <transition name="hide_list01">
            <div class="htl_list01" v-show="showList">
                <div class="map_filter">
                  <input :disabled="!showFilter" type="text" class="srch_filter" v-model="searchHotel" :placeholder="showFilter?$t('Hotel_Results.MapFilter_Placeholder'):''" />
                  <transition name="get_direction">
                  <div class="get_direction">
                    <div class="get_direction_navs">
                      <div class="mapbtn">
                        <input type="radio" id="radio-btn-1" value="DRIVING" checked="checked" name="radio-btns-1" v-model="selectedMode">
                        <label for="radio-btn-1" class="btn"><i class="fa fa-car"></i></label>
                      </div>
                      <div class="mapbtn">
                        <input type="radio" id="radio-btn-2" value="TRANSIT" name="radio-btns-2" v-model="selectedMode">
                        <label for="radio-btn-2" class="btn"><i class="fa fa-bus"></i></label>
                      </div>
                      <div class="mapbtn">
                        <input type="radio" id="radio-btn-3" value="WALKING" name="radio-btns-3" v-model="selectedMode">
                        <label for="radio-btn-3" class="btn"><i class="fa fa-male"></i></label>
                      </div>
                      <!--<a class="hide_direction" @click="showDirections=false"><i class="fa fa-close"></i></a>-->
                      <a @click="getDirections">{{$t('Hotel_Results.MapGetDirections_Label')}}</a>
                    </div>
                    <div class="get_direction_txt">
                      <div class="fm_direction">
                        <map-autocomplete :latitude="citySearchLocation.lat" :longitude="citySearchLocation.lon" :directive="'from'" :locationName="directionFrom.locationName"></map-autocomplete>
                        <!--<input :value="directionFrom.locationName" type="text" class="txt_direction1 desti_place" placeholder="Directions From" />-->
                        <i class="fa fa-street-view"></i>
                      </div>
                      <div class="line_direction">
                        <i class="fa fa-ellipsis-v"></i>
                      </div>
                      <div class="to_direction">
                        <map-autocomplete :latitude="citySearchLocation.lat" :longitude="citySearchLocation.lon" :directive="'to'" :locationName="directionTo.locationName"></map-autocomplete>
                        <!--<input type="text" :value="directionTo.locationName" class="txt_direction2 desti_place" placeholder="Directions To" />-->
                        <i class="fa fa-map-marker"></i>
                      </div>
                    </div>
                  </div>
                </transition>
                <div class="mode_change" title="Hotels" @click="$emit('hide-map')"><i class="fa fa-list"></i></div>
                <div class="hotel_map_list hotel_map_list_shrink" id="vbar_list" v-if="!showPOI&&!showDirectionsList">
                  <hotel-list-map :hotels="filteredHotel" :markers="gmapMarkers" :common-store="commonStore"></hotel-list-map>
                </div>
                <div id="directions_list" class="point_int hotel_map_list_shrink" v-show="showDirectionsList"></div>
                <div class="point_int hotel_map_list_shrink" id="poi_map" v-show="showPOI&&!showDirectionsList"></div>
              </div>
            </div>
          </transition>
        </div>
        <div class="htl_map_view">
          <div id="map" class="map-container"></div>
          <div id="mapdummy" style="display:none"></div>
        </div>
      </div>
    </div>
  </div>
  <!--Map view close-->
  `
});

Vue.component("hotel-rooms-results-page", {
    data: function () {
        return {
            adults: [{
                    'value': 1,
                    'text': '1'
                },
                {
                    'value': 2,
                    'text': '2'
                },
                {
                    'value': 3,
                    'text': '3'
                },
                {
                    'value': 4,
                    'text': '4'
                },
                {
                    'value': 5,
                    'text': '5'
                },
                {
                    'value': 6,
                    'text': '6'
                },
                {
                    'value': 7,
                    'text': '7'
                }
            ],
            children: [{
                    'value': 0,
                    'text': '0'
                },
                {
                    'value': 1,
                    'text': '1'
                },
                {
                    'value': 2,
                    'text': '2'
                },

            ],
            childrenAges: [{
                    'value': 1,
                    'text': '1'
                },
                {
                    'value': 2,
                    'text': '2'
                },
                {
                    'value': 3,
                    'text': '3'
                },
                {
                    'value': 4,
                    'text': '4'
                },
                {
                    'value': 5,
                    'text': '5'
                },
                {
                    'value': 6,
                    'text': '6'
                },
                {
                    'value': 7,
                    'text': '7'
                },
                {
                    'value': 8,
                    'text': '8'
                },
                {
                    'value': 9,
                    'text': '9'
                },
                {
                    'value': 10,
                    'text': '10'
                },
                {
                    'value': 11,
                    'text': '11'
                },
                {
                    'value': 12,
                    'text': '12'
                },
                // { 'value': 13, 'text': '13' },
                // { 'value': 14, 'text': '14' },
                // { 'value': 15, 'text': '15' },
                // { 'value': 16, 'text': '16' },
                // { 'value': 17, 'text': '17' }
            ],
            selectedAdults: 1,
            selectedChildren: 0,
            selectedChildrenAges: [{
                    child1: 1
                },
                {
                    child2: 1
                }
            ]
        };
    },
    props: {
        roomId: Number,
        getRoomDetails: Function,
        room: Object
    },
    created: function () {
        this.selectedAdults = this.room.adult;
        this.selectedChildren = this.room.children;
        if (this.room.children > 0) {
            if (this.selectedChildren == 1) {
                this.selectedChildrenAges = [this.room.childrenAges[0], {
                    child2: 1
                }];
            } else {
                this.selectedChildrenAges = this.room.childrenAges;
            }
        }
    },
    mounted: function () {
        this.$watch(function (vm) {
                return vm.selectedAdults, vm.selectedChildren, vm.selectedChildrenAges, Date.now();
            },
            function () {
                // Executes if data above have changed.
                this.$emit("get-room-details", {
                    roomId: this.roomId,
                    selectedAdults: this.selectedAdults,
                    selectedChildren: this.selectedChildren,
                    selectedChildrenAges: this.selectedChildrenAges
                })
            }
        )
    },
    template: `
    <div class="roomsAreasec">
      <h5>{{$t('Hotel_Search.RoomText_Label')}} {{roomId+1}}</h5>
      <div class="select_adult5">
        <label>{{$t('Hotel_Search.Adults_Label')}}</label>
        <select class="select_adult" v-model.number="selectedAdults">
            <option v-for="(adult, index) in adults">{{adult.text}}</option>
          </select>
      </div>
        <div class="select_adult6">
        <label>{{$t('Hotel_Search.Child_Label')}}</label>
        <select class="select_adult" v-model.number="selectedChildren">
            <option v-for="(child, index) in children">{{child.text}}</option>
          </select>
      </div>
      <div v-for="(selectedChild, index) in parseInt(selectedChildren)" :key="index" class="chdage" :class="index==0?'select_adult5':'select_adult6'">
        <div class="form_sec">
          <label>{{$t('Hotel_Search.Child_Label')}} {{index+1}} {{$t('Hotel_Search.ChildAge_Label')}}</label>
          <select class="select_adult" v-model.number="selectedChildrenAges[index]['child'+(index+1)]">
            <option v-for="(childAge, index) in childrenAges">{{childAge.text}}</option>
          </select>
        </div>
      </div>
    </div>
  `
});


Vue.component("hotels-component", {
    mixins: [websocketMixin],
    data: function () {
        return {
            commonStore: vueCommonStore.state,
            //url search params
            nationalityUrlParam: "",
            countryOfResidenceUrlParam: "",
            cityCodeUrlParam: "",
            cityUrlParam: "",
            checkInUrlParam: "",
            checkOutUrlParam: "",
            nightsUrlParam: 1,
            adtCount: 1,
            chdCount: 0,
            guestCount: 1,
            numberOfRoomsUrlParam: 1,
            rangeValueUrlParam: 5,
            supplierListUrlParam: [],
            uuid: "",
            isFromUrlParams: true,
            searchCriteriaLocation: {},
            //hotel results
            hotelResults: [],
            filteredHotelResults: [],
            uiHotelList: [],
            noResults: false,
            currentScrollLimit: 20,
            // currency: null,
            searchResponse: {},
            //city search
            isCompleted: false,
            keywordSearch: "",
            autoCompleteResult: [],
            cityCode: "",
            cityName: "",
            //supplier select
            showSupplierList: false,
            supplierList: [],
            selectedSuppliers: [],
            supplierSearchLabel: "All Suppliers",
            //coutries
            countries: [{
                    code: "AF",
                    name: "Afghanistan"
                },
                {
                    code: "AL",
                    name: "Albania"
                },
                {
                    code: "DZ",
                    name: "Algeria"
                },
                {
                    code: "AS",
                    name: "American Samoa"
                },
                {
                    code: "AD",
                    name: "Andorra"
                },
                {
                    code: "AO",
                    name: "Angola"
                },
                {
                    code: "AI",
                    name: "Anguilla"
                },
                {
                    code: "AQ",
                    name: "Antartica"
                },
                {
                    code: "AG",
                    name: "Antigua And Barbuda"
                },
                {
                    code: "AR",
                    name: "Argentina"
                },
                {
                    code: "AM",
                    name: "Armenia"
                },
                {
                    code: "AW",
                    name: "Aruba"
                },
                {
                    code: "AU",
                    name: "Australia"
                },
                {
                    code: "AT",
                    name: "Austria"
                },
                {
                    code: "AZ",
                    name: "Azerbaijan"
                },
                {
                    code: "BS",
                    name: "Bahamas"
                },
                {
                    code: "BH",
                    name: "Bahrain"
                },
                {
                    code: "BD",
                    name: "Bangladesh"
                },
                {
                    code: "BB",
                    name: "Barbados"
                },
                {
                    code: "BY",
                    name: "Belarus"
                },
                {
                    code: "BE",
                    name: "Belgium"
                },
                {
                    code: "BZ",
                    name: "Belize"
                },
                {
                    code: "BJ",
                    name: "Benin"
                },
                {
                    code: "BM",
                    name: "Bermuda"
                },
                {
                    code: "BT",
                    name: "Bhutan"
                },
                {
                    code: "BO",
                    name: "Bolivia"
                },
                {
                    code: "BQ",
                    name: "Bonaire St Eustatius And Saba "
                },
                {
                    code: "BA",
                    name: "Bosnia-Herzegovina"
                },
                {
                    code: "BW",
                    name: "Botswana"
                },
                {
                    code: "BR",
                    name: "Brazil"
                },
                {
                    code: "IO",
                    name: "British Indian Ocean Territory"
                },
                {
                    code: "BN",
                    name: "Brunei Darussalam"
                },
                {
                    code: "BG",
                    name: "Bulgaria"
                },
                {
                    code: "BF",
                    name: "Burkina Faso"
                },
                {
                    code: "BI",
                    name: "Burundi"
                },
                {
                    code: "KH",
                    name: "Cambodia"
                },
                {
                    code: "CM",
                    name: "Cameroon-Republic Of"
                },
                {
                    code: "CB",
                    name: "Canada Buffer"
                },
                {
                    code: "CA",
                    name: "Canada"
                },
                {
                    code: "CV",
                    name: "Cape Verde-Republic Of"
                },
                {
                    code: "KY",
                    name: "Cayman Islands"
                },
                {
                    code: "CF",
                    name: "Central African Republic"
                },
                {
                    code: "TD",
                    name: "Chad"
                },
                {
                    code: "CL",
                    name: "Chile"
                },
                {
                    code: "CN",
                    name: "China"
                },
                {
                    code: "CX",
                    name: "Christmas Island"
                },
                {
                    code: "CC",
                    name: "Cocos Islands"
                },
                {
                    code: "CO",
                    name: "Colombia"
                },
                {
                    code: "KM",
                    name: "Comoros"
                },
                {
                    code: "CG",
                    name: "Congo Brazzaville"
                },
                {
                    code: "CD",
                    name: "Congo The Democratic Rep Of"
                },
                {
                    code: "CK",
                    name: "Cook Islands"
                },
                {
                    code: "CR",
                    name: "Costa Rica"
                },
                {
                    code: "CI",
                    name: "Cote D Ivoire"
                },
                {
                    code: "HR",
                    name: "Croatia"
                },
                {
                    code: "CU",
                    name: "Cuba"
                },
                {
                    code: "CW",
                    name: "Curacao"
                },
                {
                    code: "CY",
                    name: "Cyprus"
                },
                {
                    code: "CZ",
                    name: "Czech Republic"
                },
                {
                    code: "DK",
                    name: "Denmark"
                },
                {
                    code: "DJ",
                    name: "Djibouti"
                },
                {
                    code: "DM",
                    name: "Dominica"
                },
                {
                    code: "DO",
                    name: "Dominican Republic"
                },
                {
                    code: "TP",
                    name: "East Timor Former Code)"
                },
                {
                    code: "EC",
                    name: "Ecuador"
                },
                {
                    code: "EG",
                    name: "Egypt"
                },
                {
                    code: "SV",
                    name: "El Salvador"
                },
                {
                    code: "EU",
                    name: "Emu European Monetary Union"
                },
                {
                    code: "GQ",
                    name: "Equatorial Guinea"
                },
                {
                    code: "ER",
                    name: "Eritrea"
                },
                {
                    code: "EE",
                    name: "Estonia"
                },
                {
                    code: "ET",
                    name: "Ethiopia"
                },
                {
                    code: "FK",
                    name: "Falkland Islands"
                },
                {
                    code: "FO",
                    name: "Faroe Islands"
                },
                {
                    code: "ZZ",
                    name: "Fictitious Points"
                },
                {
                    code: "FJ",
                    name: "Fiji"
                },
                {
                    code: "FI",
                    name: "Finland"
                },
                {
                    code: "FR",
                    name: "France"
                },
                {
                    code: "GF",
                    name: "French Guiana"
                },
                {
                    code: "PF",
                    name: "French Polynesia"
                },
                {
                    code: "GA",
                    name: "Gabon"
                },
                {
                    code: "GM",
                    name: "Gambia"
                },
                {
                    code: "GE",
                    name: "Georgia"
                },
                {
                    code: "DE",
                    name: "Germany"
                },
                {
                    code: "GH",
                    name: "Ghana"
                },
                {
                    code: "GI",
                    name: "Gibraltar"
                },
                {
                    code: "GR",
                    name: "Greece"
                },
                {
                    code: "GL",
                    name: "Greenland"
                },
                {
                    code: "GD",
                    name: "Grenada"
                },
                {
                    code: "GP",
                    name: "Guadeloupe"
                },
                {
                    code: "GU",
                    name: "Guam"
                },
                {
                    code: "GT",
                    name: "Guatemala"
                },
                {
                    code: "GW",
                    name: "Guinea Bissau"
                },
                {
                    code: "GN",
                    name: "Guinea"
                },
                {
                    code: "GY",
                    name: "Guyana"
                },
                {
                    code: "HT",
                    name: "Haiti"
                },
                {
                    code: "HN",
                    name: "Honduras"
                },
                {
                    code: "HK",
                    name: "Hong Kong"
                },
                {
                    code: "HU",
                    name: "Hungary"
                },
                {
                    code: "IS",
                    name: "Iceland"
                },
                {
                    code: "IN",
                    name: "India"
                },
                {
                    code: "ID",
                    name: "Indonesia"
                },
                {
                    code: "IR",
                    name: "Iran"
                },
                {
                    code: "IQ",
                    name: "Iraq"
                },
                {
                    code: "IE",
                    name: "Ireland-Republic Of"
                },
                {
                    code: "IL",
                    name: "Israel"
                },
                {
                    code: "IT",
                    name: "Italy"
                },
                {
                    code: "JM",
                    name: "Jamaica"
                },
                {
                    code: "JP",
                    name: "Japan"
                },
                {
                    code: "JO",
                    name: "Jordan"
                },
                {
                    code: "KZ",
                    name: "Kazakhstan"
                },
                {
                    code: "KE",
                    name: "Kenya"
                },
                {
                    code: "KI",
                    name: "Kiribati"
                },
                {
                    code: "KP",
                    name: "Korea Dem Peoples Rep Of"
                },
                {
                    code: "KR",
                    name: "Korea Republic Of"
                },
                {
                    code: "KW",
                    name: "Kuwait"
                },
                {
                    code: "KG",
                    name: "Kyrgyzstan"
                },
                {
                    code: "LA",
                    name: "Lao Peoples Dem Republic"
                },
                {
                    code: "LV",
                    name: "Latvia"
                },
                {
                    code: "LB",
                    name: "Lebanon"
                },
                {
                    code: "LS",
                    name: "Lesotho"
                },
                {
                    code: "LR",
                    name: "Liberia"
                },
                {
                    code: "LY",
                    name: "Libya"
                },
                {
                    code: "LI",
                    name: "Liechtenstein"
                },
                {
                    code: "LT",
                    name: "Lithuania"
                },
                {
                    code: "LU",
                    name: "Luxembourg"
                },
                {
                    code: "MO",
                    name: "Macao -Sar Of China-"
                },
                {
                    code: "MK",
                    name: "Macedonia -Fyrom-"
                },
                {
                    code: "MG",
                    name: "Madagascar"
                },
                {
                    code: "MW",
                    name: "Malawi"
                },
                {
                    code: "MY",
                    name: "Malaysia"
                },
                {
                    code: "MV",
                    name: "Maldives Island"
                },
                {
                    code: "ML",
                    name: "Mali"
                },
                {
                    code: "MT",
                    name: "Malta"
                },
                {
                    code: "MH",
                    name: "Marshall Islands"
                },
                {
                    code: "MQ",
                    name: "Martinique"
                },
                {
                    code: "MR",
                    name: "Mauritania"
                },
                {
                    code: "MU",
                    name: "Mauritius Island"
                },
                {
                    code: "YT",
                    name: "Mayotte"
                },
                {
                    code: "MB",
                    name: "Mexico Buffer"
                },
                {
                    code: "MX",
                    name: "Mexico"
                },
                {
                    code: "FM",
                    name: "Micronesia"
                },
                {
                    code: "MD",
                    name: "Moldova"
                },
                {
                    code: "MC",
                    name: "Monaco"
                },
                {
                    code: "MN",
                    name: "Mongolia"
                },
                {
                    code: "ME",
                    name: "Montenegro"
                },
                {
                    code: "MS",
                    name: "Montserrat"
                },
                {
                    code: "MA",
                    name: "Morocco"
                },
                {
                    code: "MZ",
                    name: "Mozambique"
                },
                {
                    code: "MM",
                    name: "Myanmar"
                },
                {
                    code: "NA",
                    name: "Namibia"
                },
                {
                    code: "NR",
                    name: "Nauru"
                },
                {
                    code: "NP",
                    name: "Nepal"
                },
                {
                    code: "AN",
                    name: "Netherlands Antilles"
                },
                {
                    code: "NL",
                    name: "Netherlands"
                },
                {
                    code: "NC",
                    name: "New Caledonia"
                },
                {
                    code: "NZ",
                    name: "New Zealand"
                },
                {
                    code: "NI",
                    name: "Nicaragua"
                },
                {
                    code: "NE",
                    name: "Niger"
                },
                {
                    code: "NG",
                    name: "Nigeria"
                },
                {
                    code: "NU",
                    name: "Niue"
                },
                {
                    code: "NF",
                    name: "Norfolk Island"
                },
                {
                    code: "MP",
                    name: "Northern Mariana Islands"
                },
                {
                    code: "NO",
                    name: "Norway"
                },
                {
                    code: "OM",
                    name: "Oman"
                },
                {
                    code: "PK",
                    name: "Pakistan"
                },
                {
                    code: "PW",
                    name: "Palau Islands"
                },
                {
                    code: "PS",
                    name: "Palestine - State Of"
                },
                {
                    code: "PA",
                    name: "Panama"
                },
                {
                    code: "PG",
                    name: "Papua New Guinea"
                },
                {
                    code: "PY",
                    name: "Paraguay"
                },
                {
                    code: "PE",
                    name: "Peru"
                },
                {
                    code: "PH",
                    name: "Philippines"
                },
                {
                    code: "PL",
                    name: "Poland"
                },
                {
                    code: "PT",
                    name: "Portugal"
                },
                {
                    code: "PR",
                    name: "Puerto Rico"
                },
                {
                    code: "QA",
                    name: "Qatar"
                },
                {
                    code: "RE",
                    name: "Reunion Island"
                },
                {
                    code: "RO",
                    name: "Romania"
                },
                {
                    code: "RU",
                    name: "Russia"
                },
                {
                    code: "XU",
                    name: "Russia"
                },
                {
                    code: "RW",
                    name: "Rwanda"
                },
                {
                    code: "WS",
                    name: "Samoa-Independent State Of"
                },
                {
                    code: "SM",
                    name: "San Marino"
                },
                {
                    code: "ST",
                    name: "Sao Tome And Principe Islands "
                },
                {
                    code: "SA",
                    name: "Saudi Arabia"
                },
                {
                    code: "SN",
                    name: "Senegal"
                },
                {
                    code: "RS",
                    name: "Serbia"
                },
                {
                    code: "SC",
                    name: "Seychelles Islands"
                },
                {
                    code: "SL",
                    name: "Sierra Leone"
                },
                {
                    code: "SG",
                    name: "Singapore"
                },
                {
                    code: "SX",
                    name: "Sint Maarten"
                },
                {
                    code: "SK",
                    name: "Slovakia"
                },
                {
                    code: "SI",
                    name: "Slovenia"
                },
                {
                    code: "SB",
                    name: "Solomon Islands"
                },
                {
                    code: "SO",
                    name: "Somalia"
                },
                {
                    code: "ZA",
                    name: "South Africa"
                },
                {
                    code: "SS",
                    name: "South Sudan"
                },
                {
                    code: "ES",
                    name: "Spain"
                },
                {
                    code: "LK",
                    name: "Sri Lanka"
                },
                {
                    code: "SH",
                    name: "St. Helena Island"
                },
                {
                    code: "KN",
                    name: "St. Kitts"
                },
                {
                    code: "LC",
                    name: "St. Lucia"
                },
                {
                    code: "PM",
                    name: "St. Pierre And Miquelon"
                },
                {
                    code: "VC",
                    name: "St. Vincent"
                },
                {
                    code: "SD",
                    name: "Sudan"
                },
                {
                    code: "SR",
                    name: "Suriname"
                },
                {
                    code: "SZ",
                    name: "Swaziland"
                },
                {
                    code: "SE",
                    name: "Sweden"
                },
                {
                    code: "CH",
                    name: "Switzerland"
                },
                {
                    code: "SY",
                    name: "Syrian Arab Republic"
                },
                {
                    code: "TW",
                    name: "Taiwan"
                },
                {
                    code: "TJ",
                    name: "Tajikistan"
                },
                {
                    code: "TZ",
                    name: "Tanzania-United Republic"
                },
                {
                    code: "TH",
                    name: "Thailand"
                },
                {
                    code: "TL",
                    name: "Timor Leste"
                },
                {
                    code: "TG",
                    name: "Togo"
                },
                {
                    code: "TK",
                    name: "Tokelau"
                },
                {
                    code: "TO",
                    name: "Tonga"
                },
                {
                    code: "TT",
                    name: "Trinidad And Tobago"
                },
                {
                    code: "TN",
                    name: "Tunisia"
                },
                {
                    code: "TR",
                    name: "Turkey"
                },
                {
                    code: "TM",
                    name: "Turkmenistan"
                },
                {
                    code: "TC",
                    name: "Turks And Caicos Islands"
                },
                {
                    code: "TV",
                    name: "Tuvalu"
                },
                {
                    code: "UM",
                    name: "U.S. Minor Outlying Islands"
                },
                {
                    code: "UG",
                    name: "Uganda"
                },
                {
                    code: "UA",
                    name: "Ukraine"
                },
                {
                    code: "AE",
                    name: "United Arab Emirates"
                },
                {
                    code: "GB",
                    name: "United Kingdom"
                },
                {
                    code: "US",
                    name: "United States Of America"
                },
                {
                    code: "UY",
                    name: "Uruguay"
                },
                {
                    code: "UZ",
                    name: "Uzbekistan"
                },
                {
                    code: "VU",
                    name: "Vanuatu"
                },
                {
                    code: "VA",
                    name: "Vatican"
                },
                {
                    code: "VE",
                    name: "Venezuela"
                },
                {
                    code: "VN",
                    name: "Vietnam"
                },
                {
                    code: "VG",
                    name: "Virgin Islands-British"
                },
                {
                    code: "VI",
                    name: "Virgin Islands-United States"
                },
                {
                    code: "WF",
                    name: "Wallis And Futuna Islands"
                },
                {
                    code: "EH",
                    name: "Western Sahara"
                },
                {
                    code: "YE",
                    name: "Yemen Republic"
                },
                {
                    code: "ZM",
                    name: "Zambia"
                },
                {
                    code: "ZW",
                    name: "Zimbabwe"
                },
            ],
            countryOfResidence: "AE",
            nationality: "AE",
            countryCode: "",
            // datepicker
            checkIn: "",
            checkOut: "",
            numberOfNights: 1,
            numberOfRooms: 1,
            roomSelectionDetails: {},
            KMrange: 5,
            showMap: false,
            //filters
            lowestPrice: 0,
            highestPrice: 100,
            hotelNameFilter: "",
            hotelLocationFilter: "",
            hotelSupplierFilter: [],
            hotelStarFilter: [0, 1, 2, 3, 4, 5],
            fromStarFilter: true,
            showAllSupplierLink: false,
            showAllStarLink: false,
            sortValues: [{
                    isActive: true,
                    sortBy: "Price",
                    isAscending: false
                },
                {
                    isActive: false,
                    sortBy: "Name",
                    isAscending: false
                },
                {
                    isActive: false,
                    sortBy: "Star",
                    isAscending: false
                }
            ],
            hideAccordionPriceFilter: true,
            hideAccordionNameFilter: true,
            hideAccordionLocationFilter: true,
            hideAccordionSupplierFilter: true,
            hideAccordionStarFilter: true,
            //quotations
            quotationsList: [],
            searchResponseDetails: {},
            currentSelectedHotel: "",
            loaderBarStyleObject: {
                width: 0 + "%"
            },
            countValidSupplierResponse: 0,
            requestCount: 1,
            forCurrencyUpdatesOnly: false,
            searchDates: {
                cin: "",
                cout: ""
            },
            highlightIndex: 0,
            searchRequest: "",
            showRecommendedSorting: false,
            mobileSort: {
                isAscending: true,
                name: "Price"
            },
            currentQuotationRoom: null
        };
    },
    created: function () {
        this.nationalityUrlParam = this.getParameterValues("nationality");
        this.countryOfResidenceUrlParam = this.getParameterValues("country_of_residence");
        this.cityCodeUrlParam = this.getParameterValues("city_code");
        this.cityUrlParam = this.getParameterValues("city");
        this.checkInUrlParam = this.getParameterValues("cin");
        this.checkOutUrlParam = this.getParameterValues("cout");
        this.nightsUrlParam = parseInt(this.getParameterValues("night"));
        this.adtCount = parseInt(this.getParameterValues("adtCount"));
        this.chdCount = parseInt(this.getParameterValues("chdCount"));
        this.guestCount = parseInt(this.getParameterValues("guest"));
        this.numberOfRoomsUrlParam = parseInt(this.getParameterValues("num_room"));
        this.rangeValueUrlParam = parseInt(this.getParameterValues("KMrange"));
        this.supplierListUrlParam = this.getParameterValues("supplier").split(",");
        this.uuid = this.getParameterValues("uuid");

        //for modify search
        this.nationality = this.nationalityUrlParam;
        this.countryOfResidence = this.countryOfResidenceUrlParam;
        this.cityCode = this.cityCodeUrlParam;
        this.cityName = this.cityUrlParam;
        this.numberOfNights = this.nightsUrlParam;
        this.KMrange = this.rangeValueUrlParam;
        this.numberOfRooms = this.numberOfRoomsUrlParam;
        this.selectedSuppliers = this.supplierListUrlParam.map(function (supplier) {
            return parseInt(supplier);
        });
        this.keywordSearch = this.cityUrlParam;

        this.searchCriteriaLocation = JSON.parse(window.localStorage.getItem("cityLocation"));

        var refreshReference = window.sessionStorage.getItem("uuID");

        if (refreshReference == null || refreshReference == undefined) {
            window.sessionStorage.setItem("uuID", this.uuid);
        } else {
            var uuIDtmp = uuidv4();
            if (refreshReference == this.uuid) {
                if (window.history.pushState) {
                    var q = decodeURIComponent(location.href.split("?")[1]).split("&");
                    q[q.length - 1] = "uuid=" + uuIDtmp;
                    var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + "?" + encodeURIComponent(q.join("&"));
                    window.history.pushState({
                        path: newurl
                    }, '', newurl);
                    window.sessionStorage.setItem("uuID", uuIDtmp);
                    this.uuid = uuIDtmp;
                }
            } else {
                window.sessionStorage.setItem("uuID", this.uuid);
            }
        }

        //for supplier dropdown
        var agencyNode = window.localStorage.getItem("agencyNode");
        if (agencyNode) {
            agencyNode = JSON.parse(atob(agencyNode));
            var servicesList = agencyNode.loginNode.servicesList;
            for (var i = 0; i < servicesList.length; i++) {
                if (servicesList[i].name == "Hotel" && servicesList[i].provider.length > 0) {
                    var supplierListSorted = servicesList[i].provider.filter(supplier => supplier.supplierType == 'Test' || supplier.supplierType == 'Production').
                    sort(function (a, b) {
                        if (a.name.toLowerCase() < b.name.toLowerCase()) {
                            return -1;
                        }
                        if (a.name.toLowerCase() > b.name.toLowerCase()) {
                            return 1;
                        }
                        return 0;
                    });
                    for (var j = 0; j < supplierListSorted.length; j++) {
                        this.supplierList.push({
                            id: supplierListSorted[j].id,
                            name: supplierListSorted[j].name.toUpperCase(),
                            supplierType: supplierListSorted[j].supplierType
                        });
                    }
                    break;
                } else if (servicesList[i].name == "Hotel" && servicesList[i].provider.length == 0) {
                    setLoginPage("login");
                }
            }
        }
        this.hotelSupplierFilter = this.supplierList.map(function (supplier) {
            return supplier.id;
        });

        //Parse rooms
        var roomList = [];
        for (var irt = 1; irt <= parseInt(this.numberOfRoomsUrlParam); irt++) {

            var roomItem = [],
                roomItemSearchParams = [];
            var roomsPax = this.getParameterValues("room" + irt);
            var paxDetails = roomsPax.split(',');
            var adultCountPerRoom = paxDetails[0].split('_')[1];

            if (paxDetails.length > 1) {
                var childArray = paxDetails[1].split('_');
                for (var index = 2; index < childArray.length; index++) {
                    roomItem.push({
                        "age": childArray[index],
                        "sequence": index
                    });
                    var tempObj = {};
                    tempObj["child" + (index - 1)] = parseInt(childArray[index]);
                    roomItemSearchParams.push(tempObj);
                }
                roomList.push({
                    "roomId": irt,
                    "guests": {
                        "adult": adultCountPerRoom,
                        "children": roomItem
                    }
                });
                this.roomSelectionDetails["room" + irt] = {
                    adult: parseInt(adultCountPerRoom),
                    children: parseInt(childArray[1]),
                    childrenAges: roomItemSearchParams
                };
            }
        }
        var searchRequest = {
            "request": {
                "service": "HotelRQ",
                "content": {
                    "command": "HotelSearchRQ",
                    "criteria": {
                        "criteriaType": "Hotel",
                        "location": {
                            "cityCode": this.cityCodeUrlParam,
                            "countryCode": window.sessionStorage.getItem("countryCode"),
                            "radius": this.rangeValueUrlParam
                        },
                        "stay": {
                            "checkIn": this.checkInUrlParam,
                            "checkOut": this.checkOutUrlParam
                        },
                        "nationality": this.nationality,
                        "residentOf": this.countryOfResidence,
                        "rooms": roomList,
                        "advanceSearch": {
                            "hotelId": 0,
                            "hotelName": "hotelname",
                            "maxAmount": 0,
                            "minAmount": 0,
                            "hotelChain": "hotelchain"
                        }
                    },
                    "sort": [{
                        "order": "Asc",
                        "sequence": 1
                    }],
                    "filter": {
                        "noOfResults": "100",
                        "from": 1,
                        "to": 100
                    },
                    "supplierSpecific": {
                        "uuid": this.uuid
                    }
                },
                "token": window.localStorage.getItem("accessToken"),
                "supplierCodes": this.supplierListUrlParam

            }
        }

        this.searchRequest = JSON.stringify(searchRequest);
        // console.log(JSON.stringify(searchRequest));

        this.connect(); //open websocket connection           

        this.sendMessage(JSON.stringify(searchRequest)); //send request to web socket server
        var vm = this;
        setTimeout(function () {
            vm.disconnect();
            if (vm.hotelResults.length == 0) {
                vm.noResults = true;
            }
            console.log(vm.webSocketStatus)
            vm.countValidSupplierResponse = vm.requestCount;
        }, 180000);

        // var data = {};
        // var axiosConfig = {
        //   headers: {
        //     "Content-Type": "application/json",
        //     Authorization: "Bearer " + accessToken
        //   }
        // };

        this.$eventHub.$on('select-hotel-map', this.selectHotelMap);

    },
    mounted: function () {
        var vm = this;
        this.$nextTick(function () {
            var generalInformation = {
                systemSettings: {
                    systemDateFormat: 'dd M y,D',
                    calendarDisplay: 1,
                    calendarDisplayInMobile: 1
                },
            }

            var dateFormat = generalInformation.systemSettings.systemDateFormat;

            var tempnumberofmonths = 1;
            if (parseInt($(window).width()) > 500 && parseInt($(window).width()) <= 999) {
                tempnumberofmonths = 2;
            } else if (parseInt($(window).width()) > 999) {
                tempnumberofmonths = 3;
            }

            var sDate = new Date(moment(vm.checkInUrlParam, "DD/MM/YYYY")).getTime();
            var eDate = new Date(moment(vm.checkOutUrlParam, "DD/MM/YYYY")).getTime();

            //Checkin Date
            $("#txtCheckInDate").datepicker({
                dateFormat: dateFormat,
                minDate: 0, // 0 for search
                maxDate: "360d",
                numberOfMonths: parseInt(tempnumberofmonths),
                onSelect: function (date) {

                    var dt2 = $('#txtCheckOutDate');
                    var startDate = $(this).datepicker('getDate');
                    var minDate = $(this).datepicker('getDate');
                    sDate = startDate.getTime();
                    startDate.setDate(startDate.getDate() + 1);
                    dt2.datepicker('option', 'minDate', startDate);
                    dt2.datepicker('setDate', minDate);
                    var a = $("#txtCheckInDate").datepicker('getDate').getTime();
                    var b = $("#txtCheckOutDate").datepicker('getDate').getTime();
                    var c = 24 * 60 * 60 * 1000;
                    var diffDays = Math.round(Math.abs((a - b) / c));
                    vm.numberOfNights = diffDays;
                    vm.checkIn = $("#txtCheckInDate").val();
                    $('.ui-datepicker-current-day').click();
                },
                beforeShowDay: function (date) {

                    var container = $(".multi_dropBox");
                    container.hide();

                    if (date.getTime() == eDate) {
                        return [true, 'event event-selected event-selected-right', ''];
                    } else if (date.getTime() <= eDate && date.getTime() >= sDate) {
                        if (date.getTime() == sDate) {
                            return [true, 'event-selected event-selected-left', ''];
                        } else {
                            return [true, 'event-selected', ''];
                        }
                    } else {
                        return [true, '', ''];
                    }
                }
            });
            $("#txtCheckInDate").datepicker({
                dateFormat: dateFormat
            }).datepicker("setDate", new Date(sDate));
            vm.checkIn = $("#txtCheckInDate").val();

            //Checkout Date
            $('#txtCheckOutDate').datepicker({
                dateFormat: dateFormat,
                minDate: new Date(moment(vm.checkInUrlParam, "DD/MM/YYYY").add(1, 'days')),
                maxDate: "360d",
                numberOfMonths: parseInt(tempnumberofmonths),
                onSelect: function (date) {
                    var a = $("#txtCheckInDate").datepicker('getDate').getTime();
                    var b = $("#txtCheckOutDate").datepicker('getDate').getTime();
                    var c = 24 * 60 * 60 * 1000;
                    var diffDays = Math.round(Math.abs((a - b) / c));
                    if (diffDays > 90) {
                        alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.NightsValidation4_Label'));
                        vm.setNumberOfNights();
                    } else {
                        vm.numberOfNights = diffDays;
                        eDate = $(this).datepicker('getDate').getTime();
                    }
                    vm.checkOut = $("#txtCheckOutDate").val();
                },
                beforeShowDay: function (date) {
                    if (date.getTime() == sDate) {
                        return [true, 'event event-selected event-selected-left', ''];
                    } else if (date.getTime() >= sDate && date.getTime() <= eDate) {
                        if (date.getTime() == eDate) {
                            return [true, 'event-selected event-selected-right', ''];
                        } else {
                            return [true, 'event-selected', ''];
                        }
                    } else {
                        return [true, '', ''];
                    }
                }
            });
            $("#txtCheckOutDate").datepicker({
                dateFormat: dateFormat
            }).datepicker("setDate", new Date(eDate));
            vm.checkOut = $("#txtCheckOutDate").val();

            var stepSlider = document.getElementById('rangeSlider');

            noUiSlider.create(stepSlider, {
                start: [parseInt(vm.KMrange)],
                step: 1,
                range: {
                    'min': [1],
                    'max': [10]
                }
            });

            stepSlider.noUiSlider.on('update', function (values, handle) {
                vm.KMrange = parseInt(values[handle]);
            });

            vm.isFromUrlParams = false;
            vm.scroll();

            // //Don't remove scrollbar when showing alertify pop up.
            // alertify.defaults.preventBodyShift = true;
            // //Don't focus on top.
            // alertify.defaults.maintainFocus = false;


            var percentWidth1 = 0.02;
            var loader = setInterval(() => {
                this.loaderBarStyleObject.width = (parseInt(this.loaderBarStyleObject.width.replace(/%/g, "")) + percentWidth1 * 100) + "%";
                if (this.loaderBarStyleObject.width.replace(/%/g, "") >= 100) {
                    clearInterval(loader);
                }
            }, 3600);

        });

        this.$watch(
            function (vmWatch) {
                return vmWatch.lowestPrice, vmWatch.highestPrice, Date.now();
            },
            function () {
                if (!vm.forCurrencyUpdatesOnly) {

                    vm.currentScrollLimit = 20;

                    var newHotelList = vm.hotelResults.filter(function (hotel) {

                        return (hotel.name || "").toLowerCase().indexOf(vm.hotelNameFilter.toLowerCase()) != -1 &&
                            (hotel.location.address || "").toLowerCase().indexOf(vm.hotelLocationFilter.toLowerCase()) != -1 &&
                            Math.ceil(hotel.rate) >= Math.floor(i18n.n(vm.lowestPrice * vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency).match(/\d+(\.\d+)?/g).join('')) &&
                            Math.floor(hotel.rate) <= Math.ceil(i18n.n(vm.highestPrice * vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency).match(/\d+(\.\d+)?/g).join('')) &&
                            vm.hotelSupplierFilter.indexOf(parseInt(hotel.supplierCode)) > -1 &&
                            (vm.hotelStarFilter.indexOf(parseInt(hotel.starRating)) > -1);
                    });

                    vm.filteredHotelResults = newHotelList;
                    vm.uiHotelList = vm.filteredHotelResults.slice(0, vm.currentScrollLimit);
                }
                vm.forCurrencyUpdatesOnly = false;
                //With recommended hotel - start
                // for (var index = 0; index < newHotelList.length; index++) {
                //   if (newHotelList[index].hotelSpecific.hotelPreference || newHotelList[index].supplierPreference != -100) {
                //     vm.showRecommendedSorting = true;
                //     if (vm.sortValues.length == 3) {
                //       vm.sortValues.push({ isActive: false, sortBy: "Recommended", isAscending: true });
                //     }
                //     break
                //   } else {
                //     vm.showRecommendedSorting = false;
                //     if (vm.sortValues.length == 4) {
                //       vm.sortValues.pop();
                //     }
                //     break
                //   }
                // }
                //With recommended hotel - end

            }
        )

        this.$watch(function (vmWatch) {
                return vmWatch.hotelNameFilter, vmWatch.hotelLocationFilter, vmWatch.hotelSupplierFilter, vmWatch.hotelStarFilter, Date.now();
            },
            function () {
                vm.currentScrollLimit = 20;

                var newHotelList = vm.hotelResults.filter(function (hotel) {

                    return (hotel.name || "").toLowerCase().indexOf(vm.hotelNameFilter.toLowerCase()) != -1 &&
                        (hotel.location.address || "").toLowerCase().indexOf(vm.hotelLocationFilter.toLowerCase()) != -1 &&
                        vm.hotelSupplierFilter.indexOf(parseInt(hotel.supplierCode)) > -1 &&
                        vm.hotelStarFilter.indexOf(parseInt(hotel.starRating)) > -1;
                });

                vm.filteredHotelResults = newHotelList;
                vm.uiHotelList = vm.filteredHotelResults.slice(0, vm.currentScrollLimit);
                var lowestPrice = Math.min.apply(Math, vm.filteredHotelResults.map(function (o) {
                    return o.rate;
                }))
                var highestPrice = Math.max.apply(Math, vm.filteredHotelResults.map(function (o) {
                    return o.rate;
                }))

                if (highestPrice == lowestPrice) {
                    lowestPrice = parseFloat(lowestPrice) - 1;
                }

                lowestPrice = lowestPrice == Number.POSITIVE_INFINITY ? 0 : lowestPrice;
                highestPrice = highestPrice == Number.NEGATIVE_INFINITY ? 1 : highestPrice;

                lowestPrice = i18n.n(lowestPrice / vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency).match(/\d+(\.\d+)?/g).join('');
                highestPrice = i18n.n(highestPrice / vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency).match(/\d+(\.\d+)?/g).join('');

                document.getElementById("property-price-lower").innerHTML = lowestPrice;
                document.getElementById("property-price-upper").innerHTML = highestPrice;

                var slider = document.getElementById("property-price-range");
                slider.noUiSlider.updateOptions({
                    tooltips: [true, true],
                    start: [parseFloat(lowestPrice), parseFloat(highestPrice)],
                    range: {
                        'min': parseFloat(lowestPrice),
                        'max': parseFloat(highestPrice)
                    }
                });
                //With recommended hotel - start

                // for (var index = 0; index < newHotelList.length; index++) {
                //   if (newHotelList[index].hotelSpecific.hotelPreference || newHotelList[index].supplierPreference != -100) {
                //     vm.showRecommendedSorting = true;
                //     if (vm.sortValues.length == 3) {
                //       vm.sortValues.push({ isActive: false, sortBy: "Recommended", isAscending: true });
                //     }
                //     break
                //   } else {
                //     vm.showRecommendedSorting = false;
                //     if (vm.sortValues.length == 4) {
                //       vm.sortValues.pop();
                //     }
                //     break
                //   }
                // }
                //With recommended hotel - end

            }
        )
    },
    methods: {
        getParameterValues: function (param) {
            var windowLoc = decodeURIComponent(window.location.href);
            var QData = 'NODATA';
            var parameterUrl = windowLoc.slice(windowLoc.indexOf('?') + 1).split('&');

            if (!Array.prototype.find) {
                for (var i = 0; i < parameterUrl.length; i++) {
                    var urlParam = parameterUrl[i].split('=');
                    if (urlParam[0] == param) {
                        return urlParam[1];
                    }
                }
                return QData;
            } else {
                QData = parameterUrl.find(function (e) {
                    return e.split('=')[0] == param;
                });
                return QData ? QData.split('=')[1] : 'NODATA';
            }
        },
        supplierName: function (supplierCode) {
            return __.filter(this.supplierList, function (e) {
                return e.id == supplierCode;
            })[0].name;
        },
        imgUrlAlt: function (event) {
            event.target.src = "/Hotels/HotelComponents/assets/images/noimage.gif";
        },
        scroll: function () {
            window.onscroll = () => {
                var bottomOfWindow = false;
                // var bottomOfWindow = Math.ceil(document.documentElement.scrollTop) + window.innerHeight >= document.documentElement.offsetHeight - 10;
                var docElement = $(document)[0].documentElement;
                var winElement = $(window)[0];

                if ((docElement.scrollHeight - winElement.innerHeight) == winElement.pageYOffset) {
                    bottomOfWindow = true;
                }

                if (!this.showMap) {

                    if (bottomOfWindow) {
                        if (this.filteredHotelResults.length > 10) {
                            console.log("bottom");
                            for (var index = this.currentScrollLimit; index < this.currentScrollLimit + 10; index++) {
                                if (this.filteredHotelResults[index]) {
                                    this.uiHotelList.push(this.filteredHotelResults[index]);
                                }
                            }
                            this.currentScrollLimit += 10;
                        }
                    }
                }

            };
        },
        fetchHotelList: function (data) {

            var vm = this;

            if (data.requestDetails) {
                this.requestCount = parseInt(data.requestDetails.count);

            } else {

                var hotels = data.response.content.searchResponse.hotels;

                if (this.hotelResults.length == 0) {
                    this.searchResponseDetails = {
                        node: data.response.node,
                        token: data.response.token,
                        hubUuid: data.response.hubUuid,
                        searchedRooms: data.response.content.supplierSpecific.criteria.rooms,
                        stay: data.response.content.supplierSpecific.criteria.stay
                    };
                }

                this.countValidSupplierResponse = ++this.countValidSupplierResponse;
                if (hotels != null && hotels.length > 0) {
                    var percentWidth = this.countValidSupplierResponse / this.requestCount;
                    //validation prevents progress bar to move backwards
                    if (parseInt(this.loaderBarStyleObject.width.replace(/%/g, "")) < (percentWidth * 100)) {
                        this.loaderBarStyleObject.width = (percentWidth * 100) + "%";
                    }

                    for (var i = 0; i < hotels.length; i++) {
                        hotels[i].hotelDetailsClicked = false;
                        hotels[i].hotelDetailsButtonToggle = false;
                        hotels[i].hotelDetailsLoading = false;
                        hotels[i].currentRoomCancellationShown = null;
                        hotels[i].hotelDetailsRS = {
                            name: "",
                            address: "",
                            location: {},
                            hotelAmenities: [],
                            roomAmenities: [],
                            specification: [{
                                images: [],
                                description: ""
                            }],
                            contact: {}
                        };
                        hotels[i].bundledResult = [];
                        hotels[i].optionalResult = [];
                        hotels[i].selectedRooms = {
                            data: [],
                            isOptional: false
                        };
                        hotels[i].bookNowIsClicked = null;
                        hotels[i].showBundledResult = false;
                        hotels[i].prebookResponse = {};

                        hotels[i].searchReferences = data.response.content.supplierSpecific.searchReferences;

                        hotels[i].hotelAmenitiesToggleShowMore = true;
                        hotels[i].roomAmenitiesToggleShowMore = true;
                        hotels[i].updatedRate = hotels[i].rate;
                        hotels[i].supplierCode = data.response.supplierCode;
                        hotels[i].supplierPreference = data.response.content.supplierSpecific.supplierPreference == undefined ? -100 :
                            data.response.content.supplierSpecific.supplierPreference;

                        hotels[i].hotelNorms = data.response.content.supplierSpecific.roomReferences == undefined ? [] : data.response.content.supplierSpecific.roomReferences.hotelNorms;
                        hotels[i].readMore = false;

                        hotels[i].suppliersWithMealTypeInRoomName = globalConfigs.services.hotels.suppliersWithMealTypeInRoomName;

                        if (parseInt(hotels[i].starRating) < 1 || parseInt(hotels[i].starRating) > 5) {
                            hotels[i].starRating = "0";
                        }

                        //With recommended hotel - start
                        // if (!this.showRecommendedSorting && data.response.content.supplierSpecific.supplierPreference != -100) {
                        //   this.showRecommendedSorting = true;
                        // }

                        // if (!this.showRecommendedSorting && hotels[i].hotelSpecific.hotelPreference) {
                        //   this.showRecommendedSorting = true;
                        // }
                        //With recommended hotel - end

                        // hotels[i].currency = data.response.node.currency;
                        // hotels[i].cityCode = data.response.content.supplierSpecific.criteria.location.cityCode;
                        // hotels[i].cityName = data.response.content.supplierSpecific.criteria.location.cityName;
                        // hotels[i].countryCode = data.response.content.supplierSpecific.criteria.location.countryCode;
                        // hotels[i].searchReferences = data.response.content.searchResponse.searchReferences;

                        //Check if same hotel already exists
                        var found = this.hotelResults.some(function (el) {
                            return el.hotelCode === hotels[i].hotelCode && el.supplierCode === data.response.supplierCode;
                        });

                        if (!found) this.hotelResults.push(hotels[i]); //Fetch total results                     

                    }

                    this.hotelResults = this.hotelResults.sort((a, b) => a.rate - b.rate);
                    //With recommended hotel - start
                    // vm.hotelResults = __.orderBy(vm.hotelResults,
                    //   [function (h) { return  parseInt(h.hotelSpecific.hotelPreference)|| -999; }
                    //     , function (h) { return  parseInt(h.supplierPreference)|| -999; },
                    //   function (h) { return parseInt(h.rate); }
                    // ],
                    //   ['desc', 'desc', 'asc']);
                    //With recommended hotel - end

                    //check if filters are applied
                    // var newHotelList = vm.hotelResults.filter(function (hotel) {
                    //   
                    //   return (hotel.name || "").toLowerCase().indexOf(vm.hotelNameFilter.toLowerCase()) != -1
                    //     && (hotel.location.address || "").toLowerCase().indexOf(vm.hotelLocationFilter.toLowerCase()) != -1
                    //     && parseFloat(hotel.rate) >= parseInt(i18n.n(vm.lowestPrice * vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency).match(/\d+(\.\d+)?/g).join(''))
                    //     && parseInt(hotel.rate) <= parseFloat(i18n.n(vm.highestPrice * vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency).match(/\d+(\.\d+)?/g).join(''))
                    //     && vm.hotelSupplierFilter.indexOf(parseInt(hotel.supplierCode)) > -1
                    //     && vm.hotelStarFilter.indexOf(parseInt(hotel.starRating)) > -1;
                    // });

                    vm.filteredHotelResults = vm.hotelResults.slice();
                    // vm.filteredHotelResults = newHotelList;
                    vm.uiHotelList = vm.filteredHotelResults.slice(0, vm.currentScrollLimit);

                    //With recommended hotel - start
                    // if (vm.showRecommendedSorting && vm.sortValues.length == 3) {
                    //   vm.sortValues[0].isActive = false;
                    //   vm.sortValues.push(
                    //     { isActive: true, sortBy: "Recommended", isAscending: false });
                    // }
                    //With recommended hotel - end

                }

                if (this.requestCount == this.countValidSupplierResponse) {
                    this.disconnect();
                    if (this.hotelResults.length == 0) {
                        this.noResults = true;
                    }
                    console.log(this.webSocketStatus)
                    this.countValidSupplierResponse = this.requestCount;
                }

                // vm.currency = data.response.node.currency;
                this.$nextTick(function () {
                    if (vm.filteredHotelResults.length > 0) {

                        var lowestPrice = Math.min.apply(Math, vm.hotelResults.map(function (o) {
                            return o.rate;
                        }))
                        var highestPrice = Math.max.apply(Math, vm.hotelResults.map(function (o) {
                            return o.rate;
                        }))

                        if (highestPrice == lowestPrice) {
                            lowestPrice = parseFloat(lowestPrice) - 1;
                        }

                        lowestPrice = i18n.n(lowestPrice / vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency).match(/\d+(\.\d+)?/g).join('');
                        highestPrice = i18n.n(highestPrice / vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency).match(/\d+(\.\d+)?/g).join('');

                        document.getElementById("property-price-lower").innerHTML = lowestPrice;
                        document.getElementById("property-price-upper").innerHTML = highestPrice;

                        var slider = document.getElementById("property-price-range");

                        if (slider.noUiSlider) {
                            slider.noUiSlider.updateOptions({
                                tooltips: [true, true],
                                start: [parseFloat(lowestPrice), parseFloat(highestPrice)],
                                range: {
                                    'min': parseFloat(lowestPrice),
                                    'max': parseFloat(highestPrice)
                                }
                            });
                        } else {
                            noUiSlider.create(slider, {
                                start: [parseFloat(lowestPrice), parseFloat(highestPrice)],
                                connect: true,
                                tooltips: [true, true],
                                range: {
                                    'min': [parseFloat(lowestPrice)],
                                    'max': [parseFloat(highestPrice)]
                                }
                            })

                            slider.noUiSlider.on('update', function (values) {
                                // console.log(values[0], values[1]);
                                vm.lowestPrice = values[0];
                                vm.highestPrice = values[1];
                                // vm.filterByPrice(values[0], values[1], vm.filteredHotelResults.slice());
                            });

                        }
                    }
                    $('[data-toggle="tooltip"]').tooltip();

                });
            }
        },
        onClickAutoCompleteEvent: function () {
            // if (this.autoCompleteResult.length > 0) {
            this.isCompleted = true;
            // } else {
            // this.isCompleted = false;
            // }
        },
        onKeyUpAutoCompleteEvent: __.debounce(function (keywordEntered) {
            var self = this;
            if (keywordEntered.length > 2) {
                var cityName = keywordEntered;
                var cityarray = cityName.split(' ');
                var uppercaseFirstLetter = "";
                for (var k = 0; k < cityarray.length; k++) {
                    uppercaseFirstLetter += cityarray[k].charAt(0).toUpperCase() + cityarray[k].slice(1).toLowerCase()
                    if (k < cityarray.length - 1) {
                        uppercaseFirstLetter += " ";
                    }
                }
                uppercaseFirstLetter = "*" + uppercaseFirstLetter + "*";
                var lowercaseLetter = "*" + cityName.toLowerCase() + "*";
                var uppercaseLetter = "*" + cityName.toUpperCase() + "*";

                var should = [];
                var i = 0;
                var supplier_type = "";
                for (var key in self.supplierList) {
                    // check if the property/key is defined in the object itself, not in parent
                    if (self.supplierList.hasOwnProperty(key)) {
                        supplier_type = [self.supplierList[key].supplierType, "Both"];

                        var supplierIndex = self.selectedSuppliers.indexOf(self.supplierList[key].id);
                        if (supplierIndex > -1) {
                            var supplier_id_query = {
                                bool: {
                                    must: [{
                                            match_phrase: {
                                                supplier_id: self.selectedSuppliers[supplierIndex]
                                            }
                                        },
                                        {
                                            terms: {
                                                supplier_type: supplier_type
                                            }
                                        }
                                    ]
                                }
                            };

                            should.push(supplier_id_query);
                            i++;
                        }
                    }
                }

                var query = {
                    query: {
                        bool: {
                            must: [{
                                    bool: {
                                        should: [{
                                                wildcard: {
                                                    supplier_city_name: uppercaseFirstLetter
                                                }
                                            },
                                            {
                                                wildcard: {
                                                    supplier_city_name: uppercaseLetter
                                                }
                                            },
                                            {
                                                wildcard: {
                                                    supplier_city_name: lowercaseLetter
                                                }
                                            }
                                        ]
                                    }
                                },
                                {
                                    bool: {
                                        should
                                    }
                                }
                            ]
                        }
                    }
                };

                var client = new elasticsearch.Client({
                    host: [{
                        host: HubServiceUrls.elasticSearch.elasticsearchHost,
                        auth: HubServiceUrls.elasticSearch.auth,
                        protocol: HubServiceUrls.elasticSearch.protocol,
                        port: HubServiceUrls.elasticSearch.port
                    }],
                    log: 'trace'
                });
                client.search({
                    index: 'city_map',
                    size: 150,
                    body: query
                }).then(function (resp) {
                    finalResult = [];
                    var hits = resp.hits.hits;
                    var Citymap = new Map();
                    for (var i = 0; i < hits.length; i++) {
                        Citymap.set(hits[i]._source.oneview_city_id, hits[i]._source);
                    }
                    var get_values = Citymap.values();
                    var Cityvalues = [];
                    for (var ele of get_values) {
                        Cityvalues.push(ele);
                    }
                    var results = SortInputFirst(cityName, Cityvalues);
                    for (var i = 0; i < results.length; i++) {
                        finalResult.push({
                            "code": results[i].oneview_city_id,
                            "label": results[i].supplier_city_name + ", " + results[i].oneview_country_name,
                            "location": results[i].location,
                            "countryCode": results[i].oneview_country_code
                        });
                    }
                    var newData = [];
                    finalResult.forEach(function (item, index) {
                        if (item.label.toLowerCase().indexOf(keywordEntered.toLowerCase()) >= 0) {

                            newData.push(item);
                        }
                    });
                    console.log(newData);
                    self.autoCompleteResult = newData;
                    self.isCompleted = true;
                    self.highlightIndex = 0;
                });

            } else {
                this.autoCompleteResult = [];
                this.isCompleted = false;
                this.cityName = "";

            }
        }, 100),
        updateResults: function (item) {
            this.keywordSearch = item.label;
            this.autoCompleteResult = [];
            this.cityCode = item.code;
            this.cityName = item.label;
            this.countryCode = item.countryCode;
            window.localStorage.setItem("cityLocation", JSON.stringify(item.location));
            window.sessionStorage.setItem("countryCode", item.countryCode);
        },
        up: function () {
            if (this.isCompleted) {
                if (this.highlightIndex > 0) {
                    this.highlightIndex--
                }
            } else {
                this.isCompleted = true;
            }
            this.fixScrolling();
        },
        down: function () {
            if (this.isCompleted) {
                if (this.highlightIndex < this.autoCompleteResult.length - 1) {
                    this.highlightIndex++
                } else if (this.highlightIndex == this.autoCompleteResult.length - 1) {
                    this.highlightIndex = 0;
                }
            } else {
                this.isCompleted = true;
            }
            this.fixScrolling();
        },
        fixScrolling: function () {
            try {
                var liH = this.$refs.options[this.highlightIndex].clientHeight;
                if (liH == 50) {
                    liH = 32;
                }
                if (liH * this.highlightIndex >= 32 || this.highlightIndex == 0) {
                    this.$refs.searchautocomplete.scrollTop = liH * this.highlightIndex;
                }
            } catch (error) {}
        },
        hideSupplier: function () {
            this.showSupplierList = false;
        },
        hideCity: function () {
            this.isCompleted = false;
        },
        showSupplierListDropdown: function () {
            if (this.supplierList.length != 1) {
                this.showSupplierList = !this.showSupplierList;
            }
            this.isCompleted = false;
        },
        getRoomDetails: function (data) {
            var selectedRoom = this.roomSelectionDetails['room' + (data.roomId + 1)];
            selectedRoom.adult = data.selectedAdults;
            selectedRoom.children = data.selectedChildren;
            if (data.selectedChildren == 0) {
                selectedRoom.childrenAges = [];
            } else if (data.selectedChildren == 1) {
                selectedRoom.childrenAges = [data.selectedChildrenAges[0]];
            } else if (data.selectedChildren == 2) {
                selectedRoom.childrenAges = data.selectedChildrenAges;
            }

            this.roomSelectionDetails['room' + (data.roomId + 1)] = selectedRoom;
        },
        setNumberOfNights: function () {
            var dt2 = $('#txtCheckOutDate');
            var startDate = $('#txtCheckInDate').datepicker('getDate');
            var a = $("#txtCheckInDate").datepicker('getDate').getTime();
            var b = $("#txtCheckOutDate").datepicker('getDate').getTime();
            var c = 24 * 60 * 60 * 1000;
            var diffDates = Math.round(Math.abs((a - b) / c));

            if (event.target.value.match(/[^0-9\.]/gi)) {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.NightsValidation1_Label'));
                this.numberOfNights = diffDates;
            } else if (event.target.value.match(/[d*\.]/gi)) {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.NightsValidation2_Label'));
                this.numberOfNights = diffDates;
            } else if (event.target.value > 90) {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.NightsValidation4_Label'));
                this.numberOfNights = diffDates;
            } else {
                startDate.setDate(startDate.getDate() + this.numberOfNights);
                dt2.datepicker('setDate', startDate);
                eDate = dt2.datepicker('getDate').getTime();
                this.checkOut = $("#txtCheckOutDate").val();
            }
        },
        searchHotels: function () {
            var rooms = "";
            var adultCount = 0;
            var childCount = 0;

            for (var roomIndex = 1; roomIndex <= parseInt(this.numberOfRooms); roomIndex++) {
                var roomSelectionDetails = this.roomSelectionDetails['room' + roomIndex];
                adultCount += roomSelectionDetails.adult;
                childCount += roomSelectionDetails.children;
                rooms += "&room" + roomIndex + "=ADT_" + roomSelectionDetails.adult + ",CHD_" + roomSelectionDetails.children;
                for (var childIndex = 0; childIndex < roomSelectionDetails.childrenAges.length; childIndex++) {
                    rooms += "_" + roomSelectionDetails.childrenAges[childIndex]['child' + (childIndex + 1)];
                }
            }

            if (this.cityName.trim() == "" || this.cityCode == "") {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.CityValidation_Label'));
            } else if (this.selectedSuppliers.length == 0) {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.SupplierValidation_Label'));
            } else if (this.checkIn == "") {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.CheckinValidation_Label'));
            } else if (this.checkOut == "") {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.CheckoutValidation_Label'));
            } else if (this.countryOfResidence == "") {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.CountryValidation_Label'));
            } else if (this.nationality == "") {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.NationalityValidation_Label'));
            } else if (this.numberOfNights <= 0) {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.NightsValidation3_Label'));
            } else if (this.numberOfNights == "") {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.NightsValidation5_Label'));
            } else if (this.numberOfRooms == "") {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.RoomsValidation1_Label'));
            } else if (this.numberOfNights > 90) {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.NightsValidation4_Label'));
            } else if (parseInt(adultCount + childCount) > 9) {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.RoomsValidation2_Label'));
            } else {

                var checkIn = $('#txtCheckInDate').val() == "" ? "" : $('#txtCheckInDate').datepicker('getDate');
                var checkOut = $('#txtCheckOutDate').val() == "" ? "" : $('#txtCheckOutDate').datepicker('getDate');

                window.localStorage.setItem("selectedSuppliers", this.selectedSuppliers.join(","));

                var searchCriteria = "nationality=" + this.nationality +
                    "&country_of_residence=" + this.countryOfResidence +
                    "&city_code=" + this.cityCode +
                    "&city=" + this.cityName +
                    "&cin=" + moment(checkIn).format('DD/MM/YYYY') +
                    "&cout=" + moment(checkOut).format('DD/MM/YYYY') +
                    "&night=" + this.numberOfNights +
                    "&adtCount=" + adultCount +
                    "&chdCount=" + childCount +
                    "&guest=" + (adultCount + childCount) +
                    "&num_room=" + this.numberOfRooms + rooms +
                    "&KMrange=" + this.KMrange +
                    "&supplier=" + this.selectedSuppliers.join(",") +
                    // "&sort=" + "price-a" +
                    "&uuid=" + uuidv4();
                var uri = "/Hotels/hotelresults.html?" + encodeURIComponent(searchCriteria);

                window.location.href = uri;
            }

        },
        supplierCount: function (id) {
            return this.hotelResults.filter(function (hotel) {
                return parseInt(hotel.supplierCode) == parseInt(id);
            }).length;
        },
        addSupplierFilter: function (event) {
            var index = this.hotelSupplierFilter.indexOf(parseInt(event.target.value));
            this.fromStarFilter = false;
            this.hotelStarFilter = [0, 1, 2, 3, 4, 5];
            if (event.target.checked) {
                if (index == -1) {
                    this.hotelSupplierFilter.push(parseInt(event.target.value));
                }
            } else {
                if (index > -1) {
                    this.hotelSupplierFilter.splice(index, 1);
                }
            }
        },
        supplierShowOnly: function (supplierId) {
            this.hotelStarFilter = [0, 1, 2, 3, 4, 5];
            this.fromStarFilter = false;
            this.hotelSupplierFilter = [];
            this.hotelSupplierFilter.push(parseInt(supplierId));
            this.showAllSupplierLink = true;
            this.showAllStarLink = false;
        },
        supplierShowAll: function () {
            this.hotelStarFilter = [0, 1, 2, 3, 4, 5];
            this.showAllSupplierLink = false;
            this.showAllStarLink = false;
            this.hotelSupplierFilter = this.supplierList.map(function (supplier) {
                return supplier.id;
            });
        },
        starCount: function (star) {
            var vm = this;
            if (this.fromStarFilter) {
                return this.hotelResults.filter(function (hotel) {
                    return parseInt(hotel.starRating) == parseInt(star) &&
                        vm.hotelSupplierFilter.indexOf(parseInt(hotel.supplierCode)) != -1
                }).length;
            } else {
                return this.hotelResults.filter(function (hotel) {
                    return parseInt(hotel.starRating) == parseInt(star) &&
                        vm.hotelSupplierFilter.indexOf(parseInt(hotel.supplierCode)) != -1
                }).length;
            }

        },
        addStarFilter: function (event) {
            var index = this.hotelStarFilter.indexOf(parseInt(event.target.value));
            this.fromStarFilter = true;
            if (event.target.checked) {
                if (index == -1) {
                    this.hotelStarFilter.push(parseInt(event.target.value));
                }
            } else {
                if (index > -1) {
                    this.hotelStarFilter.splice(index, 1);
                }
            }
            this.showAllStarLink = true;
        },
        starShowOnly: function (star) {
            this.fromStarFilter = true;
            this.hotelStarFilter = [];
            this.hotelStarFilter.push(parseInt(star));
            this.showAllStarLink = true;
        },
        starShowAll: function () {
            this.hotelStarFilter = [0, 1, 2, 3, 4, 5];
            this.showAllStarLink = false;

        },
        sortResults: function (event, isMobile, sortType) {
            var currentId = isMobile ? sortType.name : event.currentTarget.id;
            var index = __.findIndex(this.sortValues, function (e) {
                return e.sortBy == currentId;
            })
            var item = this.sortValues[index];
            this.sortValues = [{
                    isActive: false,
                    sortBy: "Price",
                    isAscending: true
                },
                {
                    isActive: false,
                    sortBy: "Name",
                    isAscending: true
                },
                {
                    isActive: false,
                    sortBy: "Star",
                    isAscending: true
                }
            ];
            if (this.showRecommendedSorting && this.sortValues.length == 3) {
                this.sortValues.push({
                    isActive: false,
                    sortBy: "Recommended",
                    isAscending: true
                });
            }
            this.$set(this.sortValues, index, {
                isActive: true,
                sortBy: item.sortBy,
                isAscending: !item.isAscending
            });
            if (currentId == "Price") {
                this.filteredHotelResults = this.filteredHotelResults.sort(function (a, b) {
                    return item.isAscending ? parseFloat(a.rate) - parseFloat(b.rate) : parseFloat(b.rate) - parseFloat(a.rate);
                });
                //With recommended hotel - start
                // var isAscending = item.isAscending ? 'asc' : 'desc';
                // this.filteredHotelResults = __.orderBy(this.filteredHotelResults,
                //   [function (h) { return h.hotelSpecific.alwaysKeepOnTop || -999; },
                //     function (h) { return parseInt(h.rate); }], ['desc', isAscending]);
                //With recommended hotel - end
            } else if (currentId == "Name") {
                this.filteredHotelResults = this.filteredHotelResults.sort(function (a, b) {
                    var nameA = a.name.toUpperCase(); // ignore upper and lowercase
                    var nameB = b.name.toUpperCase(); // ignore upper and lowercase
                    if (item.isAscending) {
                        if (nameA < nameB) {
                            return -1;
                        }
                        if (nameA > nameB) {
                            return 1;
                        }
                        return 0; // names must be equal
                    } else {
                        if (nameA < nameB) {
                            return 1;
                        }
                        if (nameA > nameB) {
                            return -1;
                        }
                        return 0; // names must be equal
                    }
                });

                //With recommended hotel - start
                // var isAscending = item.isAscending ? 'asc' : 'desc';
                // this.filteredHotelResults = __.orderBy(this.filteredHotelResults,
                //   [function (h) { return h.hotelSpecific.alwaysKeepOnTop || -999; },
                //     function (h) { return h.name.toUpperCase(); }], ['desc', isAscending]);
                //With recommended hotel - end


            } else if (currentId == "Star") {
                this.filteredHotelResults = this.filteredHotelResults.sort(function (a, b) {
                    return item.isAscending ? parseFloat(a.starRating) - parseFloat(b.starRating) : parseFloat(b.starRating) - parseFloat(a.starRating);
                });

                //With recommended hotel - start
                // var isAscending = item.isAscending ? 'asc' : 'desc';
                // this.filteredHotelResults = __.orderBy(this.filteredHotelResults,
                //   [function (h) { return h.hotelSpecific.alwaysKeepOnTop || -999; },
                //     function (h) { return parseInt(h.starRating); }], ['desc', isAscending]);
                //With recommended hotel - end

            } else if (currentId == "Recommended") {
                this.filteredHotelResults = this.filteredHotelResults.sort(function (a, b) {
                    return item.isAscending ? parseFloat(a.starRating) - parseFloat(b.starRating) : parseFloat(b.starRating) - parseFloat(a.starRating);
                });

                //With recommended hotel - start
                // var isAscending = item.isAscending ? 'asc' : 'desc';
                // this.filteredHotelResults = __.orderBy(this.filteredHotelResults,
                //   [function (h) { return h.hotelSpecific.alwaysKeepOnTop || -999; },
                //     function (h) { return parseInt(h.hotelSpecific.hotelPreference) || -999;  },
                //     function (h) { return parseInt(h.supplierPreference)|| -999; }], ['desc', isAscending, isAscending]);
                //With recommended hotel - end

            }

            this.uiHotelList = this.filteredHotelResults.slice(0, this.currentScrollLimit);

        },
        removeQuotation: function (item, id) {
            var data = {};
            data[item] = id;
        },
        addQuotationToCurrentCart: function (hotel, room) {
            var vm = this;
            vm.currentQuotationRoom = room.data[0].room.roomId;
            var uuid = hotel.hotelCode + "|" + hotel.supplierCode + "|hotels";
            var hotelQuotation = this.commonStore.selectedCart.services.hotels;

            var options = [];

            for (var x = 0; x < room.data.length; x++) {
                var roomUuid = vm.uuid + "|" + room.data[x].room.rates[0].pricing.totalPrice.sealCode + "|" + room.data[x].room.roomId + "|" + hotel.supplierCode + "|hotels";
                options.push({
                    serialNumber: roomUuid,
                    roomName: room.data[x].room.roomName,
                    rates: room.data[x].room.rates,
                    guests: room.data[x].room.guests,
                    status: "Available",
                    inOutDate: moment(vm.searchDates.cin, "DD MMM YYYY").format("DD MMM") + " - " + vm.searchDates.cout,
                    numOfNights: moment(vm.searchDates.cout, "DD MMM YYYY").diff(moment(vm.searchDates.cin, "DD MMM YYYY"), 'days')
                });
            }

            var isExisting = false;
            var hotelIndex = -1;
            // for (var m = 0; m < hotelQuotation.length; m++) {
            //   isExisting = hotelQuotation[m].rooms.findIndex(function (x) { return x.serialNumber == roomUuid; }) != -1;
            //   if (isExisting) {
            //     break;
            //   }
            // }

            if (!isExisting) {
                hotelIndex = hotelQuotation.findIndex(function (x) {
                    return x.serialNumber == uuid;
                });

                if (hotelIndex != -1) {
                    var optionName = room.data.length + " room | " +
                        room.data.reduce(function (a, c) {
                            return a + (c.room.guests.adult + (c.room.guests.children || []).length);
                        }, 0) +
                        " Guest";

                    vm.commonStore.selectedCart.services.hotels[hotelIndex].options.push({
                        serialNumber: uuidv4() + "|" +
                            room.data[0].room.rates[0].pricing.totalPrice.sealCode + "|" +
                            room.data[0].room.roomId + "|" + hotel.supplierCode + "|hotels",
                        optionName: optionName,
                        rooms: options,
                    });
                } else {
                    var imageUrl = hotel.imageUrl;
                    try {
                        imageUrl = hotel.hotelDetailsRS.specification ?
                            hotel.hotelDetailsRS.specification[0].images ?
                            hotel.hotelDetailsRS.specification[0].images[0].url : "" : ""
                    } catch (error) {
                        imageUrl = hotel.imageUrl ? hotel.imageUrl : "";
                    }
                    var optionName = room.data.length + " room | " +
                        room.data.reduce(function (a, c) {
                            return a + (c.room.guests.adult + (c.room.guests.children || []).length);
                        }, 0) +
                        " Guest";

                    vm.commonStore.selectedCart.services.hotels.push({
                        serialNumber: uuid,
                        hotelName: hotel.name,
                        options: [{
                            serialNumber: uuidv4() + "|" +
                                room.data[0].room.rates[0].pricing.totalPrice.sealCode + "|" +
                                room.data[0].room.roomId + "|" + hotel.supplierCode + "|hotels",
                            optionName: optionName,
                            rooms: options
                        }],
                        supplierCode: room.supplierCode,
                        imageUrl: imageUrl,
                        description: hotel.hotelDetailsRS.specification ?
                            hotel.hotelDetailsRS.specification[0].description ?
                            hotel.hotelDetailsRS.specification[0].description : "" : "",
                        address: hotel.hotelDetailsRS.address,
                        starRating: hotel.starRating,
                        rate: hotel.rate,
                        city: vm.cityName
                    });
                }


                for (var k = 0; k < vm.commonStore.carts.length; k++) {
                    if (vm.commonStore.carts[k].id == vm.commonStore.selectedCart.id) {
                        vm.commonStore.carts[k].services.hotels = vm.commonStore.selectedCart.services.hotels;
                        break;
                    }
                }
                var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;
                var serviceUrl = this.commonStore.hubUrls.hubConnection.hubServices.addQuote;

                var config = {
                    axiosConfig: {
                        method: "post",
                        url: hubUrl + serviceUrl,
                        data: {
                            content: JSON.stringify(vm.commonStore.carts)
                        }
                    },
                    successCallback: function (response) {
                        if (response) {
                            vm.getAllquotationsOnServer();
                            // alertify.alert("Success", "Added to quotaion " + vm.commonStore.selectedCart.name);
                        }
                    },
                    errorCallback: function (error) {
                        alertify.alert("Error", "Error unable to add quotation.");
                        $(".lds-css").hide();
                        $(".bckgrnd").hide();
                    },
                    showAlert: true
                };

                mainAxiosRequest(config);
            } else {
                alertify.alert("Please note", "Quotation already added.");
            }
        },
        getAllquotationsOnServer: function () {
            var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = this.commonStore.hubUrls.hubConnection.hubServices.pendingQuoteDetails;
            var vm = this;

            var config = {
                axiosConfig: {
                    method: "get",
                    url: hubUrl + serviceUrl
                },
                successCallback: function (response) {
                    if (response && response.data.data[0].content) {
                        vm.commonStore.carts = JSON.parse(response.data.data[0].content);
                    } else {
                        vm.commonStore.carts = [{
                            id: uuidv4(),
                            name: "Default",
                            services: {
                                flights: [],
                                hotels: [],
                                insuranceTP: [],
                                insuranceWIS: []
                            }
                        }];
                    }
                    $(".lds-css").hide();
                    $(".bckgrnd").hide();
                },
                errorCallback: function (error) {
                    $(".lds-css").hide();
                    $(".bckgrnd").hide();
                    vm.commonStore.carts = [{
                        id: uuidv4(),
                        name: "Default",
                        services: {
                            flights: [],
                            hotels: [],
                            insuranceTP: [],
                            insuranceWIS: []
                        }
                    }];
                },
                showAlert: false
            };

            mainAxiosRequest(config);
        },
        setJqueryFunctions: function () {
            //tabs start
            // Standard
            jQuery('.tabs.standard .tab-links a').on('click', function (e) {
                var currentAttrValue = jQuery(this).attr('href');

                // Show/Hide Tabs
                jQuery('.tabs ' + currentAttrValue).show().siblings().hide();

                // Change/remove current tab to active
                jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

                e.preventDefault();
            });

            // Animated Fade
            jQuery('.tabs.animated-fade .tab-links a').on('click', function (e) {
                var currentAttrValue = jQuery(this).attr('href');

                // Show/Hide Tabs
                jQuery('.tabs ' + currentAttrValue).fadeIn(400).siblings().hide();

                // Change/remove current tab to active
                jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

                e.preventDefault();
            });

            // Animated Slide 1
            jQuery('.tabs.animated-slide-1 .tab-links a').on('click', function (e) {
                var currentAttrValue = jQuery(this).attr('href');

                // Show/Hide Tabs
                jQuery('.tabs ' + currentAttrValue).siblings().slideUp(400);
                jQuery('.tabs ' + currentAttrValue).delay(400).slideDown(400);

                // Change/remove current tab to active
                jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

                e.preventDefault();
            });
            // Animated Slide 2
            jQuery('.tabs.animated-slide-2 .tab-links a').on('click', function (e) {
                var currentAttrValue = jQuery(this).attr('href');

                // Show/Hide Tabs
                jQuery('.tabs ' + currentAttrValue).slideDown(400).siblings().slideUp(400);

                // Change/remove current tab to active
                jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

                e.preventDefault();
            });
            //tabs close

            $('[data-fancybox]').fancybox({
                afterLoad: function (instance, current) {
                    var pixelRatio = window.devicePixelRatio || 1;
                    if (current.width < 500 || current.height < 500) {
                        if (current.width < 500 || current.height < 500) {
                            current.width = current.width * 2;
                            current.height = current.height * 2;
                        }
                        if (pixelRatio > 1.5) {
                            current.width = current.width / pixelRatio;
                            current.height = current.height / pixelRatio;
                        }
                    }
                },
                buttons: [
                    "thumbs",
                    "close"
                ]
            });
        },
        showRoomDetails: function (hotelDetails) {

            var vm = this;
            this.uiHotelList.forEach(function (item, index) {
                if ((hotelDetails.hotelCode + hotelDetails.supplierCode) != (item.hotelCode + item.supplierCode)) {
                    this[index].hotelDetailsButtonToggle = false;
                }
            }, this.uiHotelList);

            // var index = __.findIndex(this.hotelResults, function (hotel) { return hotel.hotelCode == hotelCode && hotel.supplierCode == supplierCode; })
            // var hotelDetails = this.hotelResults[index];
            this.currentSelectedHotel = hotelDetails.hotelCode + hotelDetails.supplierCode;
            if (!hotelDetails.hotelDetailsClicked) {
                hotelDetails.hotelDetailsLoading = true;
                // this.selectedHotel.response.content.searchResponse.hotels = [];
                // this.selectedHotel.response.content.searchResponse.searchReferences = [];
                // this.selectedHotel.response.supplierCode = hotelDetails.supplierCode;
                // this.selectedHotel.response.node.currency = hotelDetails.currency;
                // this.selectedHotel.response.content.searchResponse.searchReferences = hotelDetails.searchReferences;
                // this.selectedHotel.response.content.supplierSpecific.criteria.location.cityCode = hotelDetails.cityCode;
                // this.selectedHotel.response.content.supplierSpecific.criteria.location.cityName = hotelDetails.cityName;
                // this.selectedHotel.response.content.supplierSpecific.criteria.location.countryCode = hotelDetails.countryCode;
                // this.selectedHotel.response.content.searchResponse.hotels.push(hotelDetails);
                // var hotelSearchResponse = this.selectedHotel.response.content.searchResponse;

                var roomRQ = {
                    service: "HotelRQ",
                    node: this.searchResponseDetails.node,
                    token: this.searchResponseDetails.token,
                    hubUuid: this.searchResponseDetails.hubUuid,
                    supplierCodes: [hotelDetails.supplierCode],
                    credentials: [],
                    content: {
                        command: "HotelRoomRQ",
                        criteria: {
                            criteriaType: "HotelRoom",
                            hotel: {
                                hotelName: hotelDetails.name,
                                rooms: this.searchResponseDetails.searchedRooms,
                                location: hotelDetails.location,
                                searchHotelReferences: hotelDetails.hotelSpecific ? hotelDetails.hotelSpecific.searchHotelReferences : undefined
                            },
                            stay: this.searchResponseDetails.stay,
                            nationality: this.nationality,
                            residentOf: this.countryOfResidence
                        },
                        supplierSpecific: {
                            hotelIndex: hotelDetails.index,
                            uuid: this.uuid,
                            searchReferences: hotelDetails.searchReferences
                        }
                    }
                }

                if (globalConfigs.services.hotels.suppliersWithHotelAndRoomDetailsAsync.indexOf(hotelDetails.supplierCode) == -1) {

                    var hotelRQ = {
                        service: "HotelRQ",
                        node: this.searchResponseDetails.node,
                        token: this.searchResponseDetails.token,
                        hubUuid: this.searchResponseDetails.hubUuid,
                        supplierCodes: [hotelDetails.supplierCode],
                        credentials: [],
                        content: {
                            command: "HotelDetailsRQ",
                            hotel: {
                                oneviewId: hotelDetails.oneviewId,
                                hotelCode: hotelDetails.hotelCode,
                                nationality: this.nationality,
                                residentOf: this.countryOfResidence,
                                location: hotelDetails.location,
                                rooms: this.searchResponseDetails.searchedRooms,
                                stay: this.searchResponseDetails.stay,
                                hotelSpecific: {
                                    "test": "test"
                                }
                            },
                            supplierSpecific: {
                                uuid: this.uuid,
                                searchReferences: hotelDetails.searchReferences,
                                searchHotelReferences: hotelDetails.hotelSpecific ? hotelDetails.hotelSpecific.searchHotelReferences : undefined,
                                roomsReferences: hotelDetails.roomSpecific ? hotelDetails.roomSpecific.roomsReferences : undefined,
                                hotelIndex: hotelDetails.index,
                            }
                        }
                    };

                    var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;

                    axios.all([axios({
                            method: "post",
                            url: hubUrl + "/rooms",
                            data: {
                                request: roomRQ
                            },
                            headers: {
                                "Content-Type": "application/json",
                                "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
                            }
                        }),
                        axios({
                            method: "post",
                            url: hubUrl + "/hotelDetails",
                            data: {
                                request: hotelRQ
                            },
                            headers: {
                                "Content-Type": "application/json",
                                "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
                            }
                        })
                    ]).then(axios.spread(function (rooms, hotel) {
                        // Both requests are now complete

                        console.log(rooms.data);
                        if (rooms.data) {
                            try {
                                //rooms binding
                                var roomsArray = rooms.data.response.content.roomResponse.rooms;
                                var bundleList = rooms.data.response.content.roomResponse.bundleList;
                                var optionalList = rooms.data.response.content.roomResponse.optionalList

                                var bundledResult = [];
                                var optionalResult = [];
                                var optionalRoomId = 0;
                                var bundledRoomId = 0;
                                for (var roomIndex = 0; roomIndex < roomsArray.length; roomIndex++) {

                                    var roomDetailsArray = roomsArray[roomIndex].roomsDetails.sort((a, b) => a.rates[0].pricing.totalPrice.price - b.rates[0].pricing.totalPrice.price);
                                    var optionalRoom = [];

                                    for (var roomDetailsIndex = 0; roomDetailsIndex < roomDetailsArray.length; roomDetailsIndex++) {

                                        var roomRatesArray = roomDetailsArray[roomDetailsIndex].rates;

                                        for (var rateIndex = 0; rateIndex < roomRatesArray.length; rateIndex++) {

                                            var rates = roomRatesArray[rateIndex];
                                            var roomDetails = __.cloneDeep(roomDetailsArray[roomDetailsIndex]);
                                            roomDetails.rates = [rates];
                                            roomDetails.isPrebookSent = false;
                                            roomDetails.rates[0].cancellationPoliciesParsed = [];

                                            if (__.includes(bundleList, rates.bundleId)) {

                                                var bundleIndex = __.findIndex(bundledResult, {
                                                    bundleId: rates.bundleId
                                                });
                                                roomDetails.bundledRoomId = bundledRoomId++;
                                                if (bundleIndex == -1) {
                                                    bundledResult.push({
                                                        bundleId: rates.bundleId,
                                                        roomsToggleShowMore: true,
                                                        details: [{
                                                            searchRoomIndex: roomsArray[roomIndex].searchRoomIndex,
                                                            room: roomDetails
                                                        }]
                                                    });
                                                } else {

                                                    bundledResult[bundleIndex].details.push({
                                                        searchRoomIndex: roomsArray[roomIndex].searchRoomIndex,
                                                        room: roomDetails
                                                    });
                                                }

                                            } else if (__.includes(optionalList, rates.bundleId)) {
                                                roomDetails.isSelected = false;
                                                roomDetails.optionalRoomId = optionalRoomId++;
                                                optionalRoom.push(roomDetails);
                                            }

                                        }

                                    }

                                    if (optionalRoom.length > 0) {
                                        optionalResult.push({
                                            searchRoomIndex: roomsArray[roomIndex].searchRoomIndex,
                                            roomsToggleShowMore: true,
                                            rooms: optionalRoom
                                        });
                                    }
                                }

                                if (optionalResult.length > 0 && optionalResult.length != 1) {
                                    for (var roomIndex = 0; roomIndex < optionalResult.length; roomIndex++) {
                                        var index = __.indexOf(optionalResult[roomIndex].rooms,
                                            __.minBy(optionalResult[roomIndex].rooms, function (room) {
                                                return room.rates[0].pricing.totalPrice.price;
                                            }))
                                        optionalResult[roomIndex].rooms[index].isSelected = true;
                                    }
                                }

                                console.log(bundledResult);
                                console.log(optionalResult);


                                hotelDetails.bundledResult = bundledResult;
                                hotelDetails.optionalResult = optionalResult;

                                // this.$set(this.hotelResults, hotelIndex, hotelDetails);

                                // var hotelSearchResponse = this.selectedHotel.response.content;

                                hotelDetails.roomsReferences = rooms.data.response.content.roomResponse.roomsReferences;
                                hotelDetails.roomSupplierSpecific = rooms.data.response.content.supplierSpecific;
                            } catch (error) {
                                hotelDetails.hotelDetailsClicked = false;
                                hotelDetails.hotelDetailsButtonToggle = false;
                                hotelDetails.hotelDetailsLoading = false;
                                alertify.alert(i18n.t('Hotel_Results.AlertifyHeader1_Label'), i18n.t('Hotel_Results.RoomSelectionError_Label'));

                            }
                        }

                        console.log(hotel.data);

                        if (hotel.data) {
                            //hotel binding

                            hotelDetails.hotelDetailsClicked = true;
                            hotelDetails.hotelDetailsButtonToggle = true;
                            hotelDetails.hotelDetailsLoading = false;
                            var hotelRsDetail = hotel.data.response.content.hotelDetailsResponse.hotel;
                            hotelDetails.hotelDetailsRS = {
                                name: hotelRsDetail.name,
                                hotelCode: hotelRsDetail.hotelCode,
                                address: hotelRsDetail.address,
                                location: hotelRsDetail.location || {},
                                hotelAmenities: hotelRsDetail.hotelAmenities || [],
                                roomAmenities: hotelRsDetail.roomAmenities || [],
                                specification: hotelRsDetail.specification || [{
                                    images: [],
                                    description: ""
                                }],
                                contact: hotelRsDetail.contact || {}
                            };
                            hotel.hotelSpecific = hotelRsDetail.hotelSpecific;
                            if (!hotelDetails.imageUrl) {
                                hotelDetails.imageUrl = hotelRsDetail.specification ? hotelRsDetail.specification[0].images ? hotelRsDetail.specification[0].images[0].url : "" : "";
                            }
                            // this.$set(this.hotelResults, index, hotelDetails);

                            vm.$nextTick(function () {
                                setTimeout(this.setJqueryFunctions, 1000);
                            });

                            if (hotelDetails.bundledResult.length > 0) {
                                vm.showHotelPriceUpdated(hotelDetails, hotelDetails.bundledResult, true);
                            }
                            if (hotelDetails.optionalResult.length > 0) {
                                vm.showHotelPriceUpdated(hotelDetails, hotelDetails.optionalResult, false);
                            }
                        } else {
                            alertify.alert(i18n.t('Hotel_Results.AlertifyHeader1_Label'), i18n.t('Hotel_Results.RoomSelectionError_Label'));

                        }
                    })).catch(function (error) {
                        hotelDetails.hotelDetailsClicked = false;
                        hotelDetails.hotelDetailsButtonToggle = false;
                        hotelDetails.hotelDetailsLoading = false;

                        if (error.response) {
                            // The request was made and the server responded with a status code
                            // that falls out of the range of 2xx
                            // console.log(error.response.data);
                            // console.log(error.response.status);
                            // console.log(error.response.headers);

                            var status = error.response.status;
                            var data = error.response.data;

                            switch (status) {
                                case 400:
                                case 408:
                                case 500:
                                    alertify.alert(data.code, data.message);
                                    break;
                                default:
                                    alertify.alert("Error", "We have found some technical difficulties. Please contact admin.");
                            }
                            console.log(error);

                        } else if (error.request) {
                            // The request was made but no response was received
                            // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                            // http.ClientRequest in node.js
                            alertify.alert("Error", "The request was made but no response was received.");
                            console.log(error.request);
                        } else {
                            // Something happened in setting up the request that triggered an Error
                            alertify.alert("Error", error.message);
                            console.log('Error', error);
                        }

                    });
                } else {
                    this.sendRoomsRQ(roomRQ, hotelDetails);
                }
                setTimeout(function () {
                    $('html, body').animate({
                        scrollTop: $("#hotel_" + hotelDetails.hotelCode + hotelDetails.supplierCode).offset().top
                    }, 300, 'linear');
                }, 500);
            } else {
                hotelDetails.hotelDetailsButtonToggle = !hotelDetails.hotelDetailsButtonToggle;
                // this.$set(this.hotelResults, index, hotelDetails);
            }
            this.$nextTick(function () {
                setTimeout(this.setJqueryFunctions, 1500);
            });
        },
        sendRoomsRQ: function (roomRQ, hotelDetails) {
            var vm = this;
            var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;

            var config = {
                axiosConfig: {
                    method: "POST",
                    url: hubUrl + "/rooms",
                    data: {
                        request: roomRQ
                    }
                },
                successCallback: function (rooms) {

                    console.log(rooms.data);
                    if (rooms.data) {
                        try {

                            //rooms binding
                            var roomsArray = rooms.data.response.content.roomResponse.rooms;
                            var bundleList = rooms.data.response.content.roomResponse.bundleList;
                            var optionalList = rooms.data.response.content.roomResponse.optionalList

                            var bundledResult = [];
                            var optionalResult = [];
                            var optionalRoomId = 0;
                            var bundledRoomId = 0;
                            for (var roomIndex = 0; roomIndex < roomsArray.length; roomIndex++) {

                                var roomDetailsArray = roomsArray[roomIndex].roomsDetails.sort((a, b) => a.rates[0].pricing.totalPrice.price - b.rates[0].pricing.totalPrice.price);
                                var optionalRoom = [];

                                for (var roomDetailsIndex = 0; roomDetailsIndex < roomDetailsArray.length; roomDetailsIndex++) {

                                    var roomRatesArray = roomDetailsArray[roomDetailsIndex].rates;

                                    for (var rateIndex = 0; rateIndex < roomRatesArray.length; rateIndex++) {

                                        var rates = roomRatesArray[rateIndex];
                                        var roomDetails = __.cloneDeep(roomDetailsArray[roomDetailsIndex]);
                                        roomDetails.rates = [rates];
                                        roomDetails.isPrebookSent = false;
                                        roomDetails.rates[0].cancellationPoliciesParsed = [];

                                        if (__.includes(bundleList, rates.bundleId)) {

                                            var bundleIndex = __.findIndex(bundledResult, {
                                                bundleId: rates.bundleId
                                            });
                                            roomDetails.bundledRoomId = bundledRoomId++;
                                            if (bundleIndex == -1) {
                                                bundledResult.push({
                                                    bundleId: rates.bundleId,
                                                    roomsToggleShowMore: true,
                                                    details: [{
                                                        searchRoomIndex: roomsArray[roomIndex].searchRoomIndex,
                                                        room: roomDetails
                                                    }]
                                                });
                                            } else {

                                                bundledResult[bundleIndex].details.push({
                                                    searchRoomIndex: roomsArray[roomIndex].searchRoomIndex,
                                                    room: roomDetails
                                                });
                                            }

                                        } else if (__.includes(optionalList, rates.bundleId)) {
                                            roomDetails.isSelected = false;
                                            roomDetails.optionalRoomId = optionalRoomId++;
                                            optionalRoom.push(roomDetails);
                                        }

                                    }

                                }

                                if (optionalRoom.length > 0) {
                                    optionalResult.push({
                                        searchRoomIndex: roomsArray[roomIndex].searchRoomIndex,
                                        roomsToggleShowMore: true,
                                        rooms: optionalRoom
                                    });
                                }
                            }

                            if (optionalResult.length > 0 && optionalResult.length != 1) {
                                for (var roomIndex = 0; roomIndex < optionalResult.length; roomIndex++) {
                                    var index = __.indexOf(optionalResult[roomIndex].rooms,
                                        __.minBy(optionalResult[roomIndex].rooms, function (room) {
                                            return room.rates[0].pricing.totalPrice.price;
                                        }))
                                    optionalResult[roomIndex].rooms[index].isSelected = true;
                                }
                            }

                            console.log(bundledResult);
                            console.log(optionalResult);


                            hotelDetails.bundledResult = bundledResult;
                            hotelDetails.optionalResult = optionalResult;

                            // this.$set(this.hotelResults, hotelIndex, hotelDetails);

                            // var hotelSearchResponse = this.selectedHotel.response.content;

                            hotelDetails.roomsReferences = rooms.data.response.content.roomResponse.roomsReferences;

                            var hotelRQ = {
                                service: "HotelRQ",
                                node: vm.searchResponseDetails.node,
                                token: vm.searchResponseDetails.token,
                                hubUuid: vm.searchResponseDetails.hubUuid,
                                supplierCodes: [hotelDetails.supplierCode],
                                credentials: [],
                                content: {
                                    command: "HotelDetailsRQ",
                                    hotel: {
                                        oneviewId: hotelDetails.oneviewId,
                                        hotelCode: hotelDetails.hotelCode,
                                        nationality: vm.nationality,
                                        residentOf: vm.countryOfResidence,
                                        location: hotelDetails.location,
                                        rooms: vm.searchResponseDetails.searchedRooms,
                                        stay: vm.searchResponseDetails.stay,
                                        hotelSpecific: {
                                            "test": "test"
                                        }
                                    },
                                    supplierSpecific: {
                                        uuid: vm.uuid,
                                        searchReferences: hotelDetails.searchReferences,
                                        searchHotelReferences: hotelDetails.hotelSpecific ? hotelDetails.hotelSpecific.searchHotelReferences : undefined,
                                        roomsReferences: hotelDetails.roomSpecific ? hotelDetails.roomSpecific.roomsReferences : undefined
                                    }
                                }
                            };

                            vm.sendHDetailsRQ(hotelRQ, hotelDetails);

                        } catch (error) {
                            hotelDetails.hotelDetailsClicked = false;
                            hotelDetails.hotelDetailsButtonToggle = false;
                            hotelDetails.hotelDetailsLoading = false;
                            alertify.alert(i18n.t('Hotel_Results.AlertifyHeader1_Label'), i18n.t('Hotel_Results.RoomSelectionError_Label'));

                        }
                    }
                },
                errorCallback: function (error) {
                    hotelDetails.hotelDetailsClicked = false;
                    hotelDetails.hotelDetailsButtonToggle = false;
                    hotelDetails.hotelDetailsLoading = false;
                    // alertify.alert(i18n.t('Hotel_Results.AlertifyHeader1_Label'), i18n.t('Hotel_Results.RoomSelectionError_Label'));
                },
                showAlert: true
            };

            mainAxiosRequest(config);

        },
        sendHDetailsRQ: function (hotelRQ, hotelDetails) {
            var vm = this;
            var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;

            var config = {
                axiosConfig: {
                    method: "POST",
                    url: hubUrl + "/hotelDetails",
                    data: {
                        request: hotelRQ
                    }
                },
                successCallback: function (hotel) {
                    console.log(hotel.data);

                    if (hotel.data) {
                        //hotel binding

                        hotelDetails.hotelDetailsClicked = true;
                        hotelDetails.hotelDetailsButtonToggle = true;
                        hotelDetails.hotelDetailsLoading = false;
                        var hotelRsDetail = hotel.data.response.content.hotelDetailsResponse.hotel;
                        hotelDetails.hotelDetailsRS = {
                            name: hotelRsDetail.name,
                            hotelCode: hotelRsDetail.hotelCode,
                            address: hotelRsDetail.address,
                            location: hotelRsDetail.location || {},
                            hotelAmenities: hotelRsDetail.hotelAmenities || [],
                            roomAmenities: hotelRsDetail.roomAmenities || [],
                            specification: hotelRsDetail.specification || [{
                                images: [],
                                description: ""
                            }],
                            contact: hotelRsDetail.contact || {}
                        };
                        hotel.hotelSpecific = hotelRsDetail.hotelSpecific;
                        if (!hotelDetails.imageUrl) {
                            hotelDetails.imageUrl = hotelRsDetail.specification ? hotelRsDetail.specification[0].images ? hotelRsDetail.specification[0].images[0].url : "" : "";
                        }
                        // this.$set(this.hotelResults, index, hotelDetails);

                        vm.$nextTick(function () {
                            setTimeout(this.setJqueryFunctions, 1000);
                        });

                        if (hotelDetails.bundledResult.length > 0) {
                            vm.showHotelPriceUpdated(hotelDetails, hotelDetails.bundledResult, true);
                        }
                        if (hotelDetails.optionalResult.length > 0) {
                            vm.showHotelPriceUpdated(hotelDetails, hotelDetails.optionalResult, false);
                        }
                    } else {
                        alertify.alert(i18n.t('Hotel_Results.AlertifyHeader1_Label'), i18n.t('Hotel_Results.RoomSelectionError_Label'));

                    }
                },
                errorCallback: function (error) {
                    hotelDetails.hotelDetailsClicked = false;
                    hotelDetails.hotelDetailsButtonToggle = false;
                    hotelDetails.hotelDetailsLoading = false;
                    // alertify.alert(i18n.t('Hotel_Results.AlertifyHeader1_Label'), i18n.t('Hotel_Results.RoomSelectionError_Label'));
                },
                showAlert: true
            }

            mainAxiosRequest(config);

        },
        toggleShowMoreRoom: function (item, hotelCode, supplierCode) {

            item.roomsToggleShowMore = !item.roomsToggleShowMore;
            setTimeout(function () {
                $('html, body').animate({
                    scrollTop: $("#hotel_" + hotelCode + supplierCode).offset().top
                }, 300, 'linear');
            }, 50);

        },
        selectRoomOptional: function (index, roomDetail, rooms) {

            console.log(roomDetail)

            for (var j = 0; j < rooms[index].rooms.length; j++) {
                rooms[index].rooms[j].isSelected = false;
            }
            roomDetail.isSelected = !roomDetail.isSelected;

        },
        selectRoomBundled: function () {},
        processHotelDetails: function (hotel, roomDetail, roomDetailIndex, isOptional, type) {

            $(".lds-css").show();
            $(".bckgrnd").show();
            if (type == "booking") {
                hotel.bookNowIsClicked = roomDetailIndex;
            }

            try {
                if (isOptional) {
                    if (roomDetail) {
                        //For single optional room only since no room is selected at first.
                        roomDetail.isSelected = true;
                    }
                    var selectedRooms = [];
                    for (var searchRoomIndex = 0; searchRoomIndex < hotel.optionalResult.length; searchRoomIndex++) {
                        var rooms = __.cloneDeep(hotel.optionalResult[searchRoomIndex].rooms);
                        for (var roomIndex = 0; roomIndex < rooms.length; roomIndex++) {
                            if (rooms[roomIndex].isSelected) {
                                rooms[roomIndex].roomId = hotel.optionalResult[searchRoomIndex].searchRoomIndex;
                                rooms[roomIndex].guests = this.searchResponseDetails.searchedRooms[hotel.optionalResult[searchRoomIndex].searchRoomIndex - 1].guests;
                                selectedRooms.push({
                                    searchRoomIndex: hotel.optionalResult[searchRoomIndex].searchRoomIndex,
                                    room: rooms[roomIndex]
                                });
                                if (type == "booking") {
                                    if (hotel.updatedRate != rooms[roomIndex].rates[0].pricing.totalPrice.price) {
                                        hotel.updatedRate = rooms[roomIndex].rates[0].pricing.totalPrice.price;
                                    }
                                }
                                break;
                            }
                        }
                    }
                    hotel.selectedRooms.data = selectedRooms;
                    hotel.selectedRooms.isOptional = true;
                    if (roomDetail) {
                        roomDetail.isSelected = false;
                    }
                } else {
                    var selectedRooms = [];
                    for (var searchRoomIndex = 0; searchRoomIndex < hotel.bundledResult.length; searchRoomIndex++) {
                        var rooms = __.cloneDeep(hotel.bundledResult[searchRoomIndex].details);
                        if (hotel.bundledResult[searchRoomIndex].bundleId == roomDetail) {
                            var totalPrice = 0;
                            for (var roomIndex = 0; roomIndex < rooms.length; roomIndex++) {
                                rooms[roomIndex].room.roomId = rooms[roomIndex].searchRoomIndex;
                                rooms[roomIndex].room.guests = this.searchResponseDetails.searchedRooms[rooms[roomIndex].searchRoomIndex - 1].guests;
                                selectedRooms.push(__.cloneDeep(rooms[roomIndex]));
                                totalPrice += parseFloat(rooms[roomIndex].room.rates[0].pricing.totalPrice.price);
                            }
                            if (type == "booking") {
                                if (hotel.updatedRate != totalPrice) {
                                    hotel.updatedRate = totalPrice;
                                }
                            }
                            break;
                        }
                    }
                    hotel.selectedRooms.data = selectedRooms;
                    hotel.selectedRooms.isOptional = false;

                }

                var totalAmount = 0;
                var sealCode = "";
                var rooms = [];
                for (var index = 0; index < hotel.selectedRooms.data.length; index++) {

                    var room = hotel.selectedRooms.data[index].room || hotel.selectedRooms.data[index];
                    rooms.push({
                        name: room.roomName,
                        roomId: room.roomId,
                        paxes: [],
                        guests: room.guests,
                        rate: {
                            bookable: true,
                            cancellationPolicies: room.rates[0].cancellationPolicies,
                            rateSpecific: room.rates[0].roomRateSpecific,
                            pricing: {
                                perNightPrice: room.rates[0].pricing.perNightPrice,
                                totalPrice: room.rates[0].pricing.totalPrice
                            },
                            roomRateReferences: room.rates[0].roomRateSpecific ? room.rates[0].roomRateSpecific.roomRateReferences : undefined
                        },
                        roomReferences: room.roomSpecific ? room.roomSpecific.roomReferences : undefined,
                        // roomSpecific: room.roomSpecific ? room.roomSpecific : null,
                    });
                    // above roomSpecific tag : for DWT
                    totalAmount += room.rates[0].pricing.totalPrice.price;
                    sealCode = room.rates[0].pricing.totalPrice.sealCode;
                }

                var PrebookRQ = {
                    service: "HotelRQ",
                    node: this.searchResponseDetails.node,
                    token: this.searchResponseDetails.token,
                    hubUuid: this.searchResponseDetails.hubUuid,
                    supplierCodes: [hotel.supplierCode],
                    credentials: [],
                    content: {
                        command: "PrebookRQ",
                        hotel: {
                            cityCode: hotel.location.cityCode,
                            hotelCode: hotel.hotelCode,
                            totalAmount: totalAmount,
                            sealCode: sealCode,
                            nationality: this.nationality,
                            residentOf: this.countryOfResidence,
                            rooms: rooms,
                            hotelSpecific: hotel.hotelSpecific
                        },
                        paxes: [],
                        stay: this.searchResponseDetails.stay,
                        supplierSpecific: {
                            uuid: this.uuid,
                            requestType: "cancellationPolicy",
                            searchReferences: hotel.searchReferences,
                            searchHotelReferences: hotel.hotelSpecific ? hotel.hotelSpecific.searchHotelReferences : undefined,
                            roomsReferences: hotel.roomSpecific ? hotel.roomSpecific.roomsReferences : undefined,
                            hotelIndex: hotel.index,
                        }
                    }
                }


                var withCancellationPolicy = false;
                for (var p = 0; p < hotel.selectedRooms.data.length; p++) {
                    if (hotel.selectedRooms.data[p].room.rates[0].cancellationPoliciesParsed.length != 0) {
                        withCancellationPolicy = true;
                        break;
                    }

                }
                if (withCancellationPolicy && type != "booking") {
                    this.addQuotationToCurrentCart(hotel, hotel.selectedRooms);
                } else if (type == "booking" || (!withCancellationPolicy && type != "booking")) {
                    this.sendPrebook({
                        hotel: hotel,
                        request: PrebookRQ,
                        fromBooking: true,
                        type: type
                    });
                }

                console.log(rooms);
            } catch (error) {
                if (roomDetail) {
                    roomDetail.isSelected = false;
                }
                hotel.bookNowIsClicked = null;
                $(".lds-css").hide();
                $(".bckgrnd").hide();
            }

        },
        showRoomPriceBundled: function (bundled) {
            var totalPrice = 0;
            for (var index = 0; index < bundled.length; index++) {
                totalPrice += parseFloat(bundled[index].room.rates[0].pricing.totalPrice.price);
            }
            return i18n.n(totalPrice / this.commonStore.currencyMultiplier, 'currency', this.commonStore.selectedCurrency);
        },
        showHotelPriceUpdated: function (hotel, rooms, isBundled) {

            var totalPrice = 0;

            if (isBundled) {
                var lowestBundle = __.minBy(rooms, function (room) {
                    var tempPrice = 0;
                    for (var index = 0; index < room.details.length; index++) {
                        tempPrice += parseFloat(room.details[index].room.rates[0].pricing.totalPrice.price);
                    }
                    return tempPrice;
                })

                for (var index = 0; index < lowestBundle.details.length; index++) {
                    totalPrice += parseFloat(lowestBundle.details[index].room.rates[0].pricing.totalPrice.price);
                }
            } else {
                for (var i = 0; i < rooms.length; i++) {
                    totalPrice += __.minBy(rooms[i].rooms, function (room) {
                        return room.rates[0].pricing.totalPrice.price;
                    }).rates[0].pricing.totalPrice.price;
                }
            }

            if (parseFloat(totalPrice) != parseFloat(hotel.updatedRate)) {
                hotel.updatedRate = totalPrice.toString();
            }
        },
        getCancellationPolicy: function (hotel, room, roomIndex, prebookRoomId, UiRoomId, isOptional) {

            var isPrebookSent = false;
            if (isOptional) {
                isPrebookSent = room.rates[0].cancellationPoliciesParsed.length == 0;
            } else {
                for (var index = 0; index < room.details.length; index++) {
                    isPrebookSent = room.details[index].room.rates[0].cancellationPoliciesParsed.length == 0;
                    if (isPrebookSent) {
                        break;
                    }
                }
            }
            if (isPrebookSent) {
                hotel.currentRoomCancellationShown = roomIndex;

                var roomCancellationCheck = [];
                if (isOptional) {
                    roomCancellationCheck = room;
                } else {
                    roomCancellationCheck = room.details[index].room;
                }

                //Check if supplier needs to send prebook request
                if (roomCancellationCheck.rates[0].cancellationPolicies == undefined) {
                    var rooms = [];

                    var totalAmount = 0;
                    var sealCode = "";

                    if (isOptional) {
                        rooms.push({
                            name: room.roomName,
                            roomId: prebookRoomId,
                            paxes: [],
                            guests: this.searchResponseDetails.searchedRooms[prebookRoomId - 1].guests,
                            rate: {
                                bookable: true,
                                cancellationPolicies: room.rates[0].cancellationPolicies,
                                rateSpecific: room.rates[0].roomRateSpecific,
                                pricing: {
                                    perNightPrice: room.rates[0].pricing.perNightPrice,
                                    totalPrice: room.rates[0].pricing.totalPrice
                                },
                                roomRateReferences: room.rates[0].roomRateSpecific ? room.rates[0].roomRateSpecific.roomRateReferences : undefined
                            },
                            roomReferences: room.roomSpecific.roomReferences ? room.roomSpecific.roomReferences : undefined
                        });
                        totalAmount = room.rates[0].pricing.totalPrice.price;
                        sealCode = room.rates[0].pricing.totalPrice.sealCode;
                    } else {
                        // (item, bundled, null, bundled.bundleId, null, false)
                        for (var index = 0; index < room.details.length; index++) {

                            var currentRoom = room.details[index].room;
                            rooms.push({
                                name: currentRoom.roomName,
                                roomId: room.details[index].searchRoomIndex,
                                paxes: [],
                                guests: this.searchResponseDetails.searchedRooms[room.details[index].searchRoomIndex - 1].guests,
                                rate: {
                                    bookable: true,
                                    cancellationPolicies: currentRoom.rates[0].cancellationPolicies,
                                    rateSpecific: currentRoom.rates[0].roomRateSpecific,
                                    pricing: {
                                        perNightPrice: currentRoom.rates[0].pricing.perNightPrice,
                                        totalPrice: currentRoom.rates[0].pricing.totalPrice
                                    },
                                    roomRateReferences: currentRoom.rates[0].roomRateSpecific ? currentRoom.rates[0].roomRateSpecific.roomRateReferences : undefined
                                },
                                roomReferences: currentRoom.roomSpecific ? currentRoom.roomSpecific.roomReferences : undefined,

                            });

                            totalAmount += currentRoom.rates[0].pricing.totalPrice.price;
                            sealCode = currentRoom.rates[0].pricing.totalPrice.sealCode;
                        }
                    }

                    var PrebookRQ = {
                        service: "HotelRQ",
                        node: this.searchResponseDetails.node,
                        token: this.searchResponseDetails.token,
                        hubUuid: this.searchResponseDetails.hubUuid,
                        supplierCodes: [hotel.supplierCode],
                        credentials: [],
                        content: {
                            command: "PrebookRQ",
                            hotel: {
                                cityCode: hotel.location.cityCode,
                                hotelCode: hotel.hotelCode,
                                totalAmount: totalAmount,
                                sealCode: sealCode,
                                nationality: this.nationality,
                                residentOf: this.countryOfResidence,
                                rooms: rooms,
                                hotelSpecific: hotel.hotelSpecific
                            },
                            paxes: [],
                            stay: this.searchResponseDetails.stay,
                            supplierSpecific: {
                                uuid: this.uuid,
                                requestType: "cancellationPolicy",
                                searchReferences: hotel.searchReferences,
                                searchHotelReferences: hotel.hotelSpecific ? hotel.hotelSpecific.searchHotelReferences : undefined,
                                roomsReferences: hotel.roomSpecific ? hotel.roomSpecific.roomsReferences : undefined,
                                hotelIndex: hotel.index
                            }
                        }
                    }

                    this.sendPrebook({
                        hotel: hotel,
                        modalRoomIndex: roomIndex,
                        request: PrebookRQ,
                        roomId: UiRoomId,
                        isOptional: isOptional,
                        fromBooking: false
                    });

                } else {
                    // if (room.rates[0].cancellationPoliciesParsed.length == 0) {
                    if (isOptional) {
                        roomCancellationCheck.rates[0].cancellationPoliciesParsed = this.parseCancellationPolicy(roomCancellationCheck.rates[0].cancellationPolicies, roomCancellationCheck.rates[0].pricing.totalPrice.price).cancellation;
                    } else {
                        for (var index = 0; index < room.details.length; index++) {
                            room.details[index].room.rates[0].cancellationPoliciesParsed = this.parseCancellationPolicy(room.details[index].room.rates[0].cancellationPolicies, room.details[index].room.rates[0].pricing.totalPrice.price).cancellation;
                        }
                    }
                    // }
                    $("#cmod_" + roomIndex).modal('show');
                    hotel.currentRoomCancellationShown = null;
                }
            } else {
                $("#cmod_" + roomIndex).modal('show');
                hotel.currentRoomCancellationShown = null;
                if (isOptional) {
                    room.rates[0].cancellationPoliciesParsed = this.parseCancellationPolicy(room.rates[0].cancellationPolicies, room.rates[0].pricing.totalPrice.price).cancellation;
                } else {
                    room.details[0].room.rates[0].cancellationPoliciesParsed = this.parseCancellationPolicy(room.details[0].room.rates[0].cancellationPolicies, room.details[0].room.rates[0].pricing.totalPrice.price).cancellation;
                }
            }
        },
        sendPrebook: function (preBookData) {
            var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;
            var vm = this;

            var config = {
                axiosConfig: {
                    method: "POST",
                    url: hubUrl + "/preBook",
                    data: {
                        request: preBookData.request
                    }
                },
                successCallback: function (response) {
                    console.log(response.data);
                    var roomIsBookable = true;

                    if (response.data.response.content.prebookResponse.bookable) {
                        var hotel = response.data.response.content.prebookResponse.hotel;
                        //Check each room here

                        for (var roomIndex = 0; roomIndex < hotel.rooms.length; roomIndex++) {
                            var room = hotel.rooms[roomIndex];
                            if (typeof room.rate.bookable != "undefined" && !room.rate.bookable) {
                                roomIsBookable = false;
                            }
                        }
                        if (roomIsBookable) {
                            var hotelPolicies = response.data.response.content.prebookResponse.policies;
                            var policyBindHtml = "";
                            if (hotelPolicies) {
                                for (var index = 0; index < hotelPolicies.length; index++) {
                                    if (hotelPolicies[index].description != undefined) {
                                        policyBindHtml += "<h4>" + hotelPolicies[index].description + "</h4>";
                                    } else {
                                        policyBindHtml += "";
                                    }
                                }
                            }

                            preBookData.hotel.hotelPolicies = policyBindHtml;
                            if (preBookData.fromBooking) {

                                var isCharged = false;
                                for (var roomIndex = 0; roomIndex < preBookData.hotel.selectedRooms.data.length; roomIndex++) {
                                    var room = preBookData.hotel.selectedRooms.data[roomIndex].room || hotel.selectedRooms.data[roomIndex];

                                    // Check for room wise or hotel wise cancellation
                                    if (hotel.hotelCancellationPolicies != undefined && hotel.hotelCancellationPolicies.length > 0) {

                                        var getCancellation = vm.parseCancellationPolicy(hotel.hotelCancellationPolicies, preBookData.hotel.rate);

                                        room.rates[0].cancellationPolicies = hotel.hotelCancellationPolicies;
                                        room.rates[0].isCharged = getCancellation.isCharged;
                                        if (preBookData.type != "booking") {
                                            room.rates[0].cancellationPoliciesParsed = getCancellation.cancellation;
                                        }

                                        room.isPrebookSent = true;

                                    } else {
                                        var prebookRooms = hotel.rooms;
                                        for (var prebookRoomIndex = 0; prebookRoomIndex < prebookRooms.length; prebookRoomIndex++) {
                                            var prebookRoom = prebookRooms[prebookRoomIndex];
                                            if (room.roomId == prebookRoom.roomId) {

                                                var getCancellation = vm.parseCancellationPolicy(prebookRoom.rate.cancellationPolicies, prebookRoom.rate.pricing.totalPrice.price);

                                                room.rates[0].cancellationPolicies = prebookRoom.rate.cancellationPolicies;
                                                room.rates[0].isCharged = getCancellation.isCharged;
                                                if (preBookData.type != "booking") {
                                                    room.rates[0].cancellationPoliciesParsed = getCancellation.cancellation;
                                                }
                                                room.isPrebookSent = true;
                                                break;
                                            }
                                        }
                                    }
                                    isCharged = room.rates[0].isCharged;
                                }

                                if (preBookData.type == "booking") {
                                    if (isCharged) {
                                        alertify.confirm(i18n.t('Hotel_Results.AlertifyHeader2_Label'), "* " + i18n.t('Hotel_Results.BookingNotesPopUp1_Label') + "</br></br>" + i18n.t('Hotel_Results.BookingNotesPopUp2_Label'),
                                            function () {
                                                vm.redirectToPaxPage(preBookData, response);
                                            },
                                            function () {
                                                preBookData.hotel.bookNowIsClicked = null;
                                                $(".lds-css").hide();
                                                $(".bckgrnd").hide();
                                            });
                                    } else {
                                        vm.redirectToPaxPage(preBookData, response);
                                    }
                                } else {
                                    vm.addQuotationToCurrentCart(preBookData.hotel, preBookData.hotel.selectedRooms);
                                }
                            } else {

                                if (preBookData.isOptional) {

                                    for (var resultIndex = 0; resultIndex < preBookData.hotel.optionalResult.length; resultIndex++) {
                                        for (var roomIndex = 0; roomIndex < preBookData.hotel.optionalResult[resultIndex].rooms.length; roomIndex++) {
                                            var room = preBookData.hotel.optionalResult[resultIndex].rooms[roomIndex];

                                            if (room.optionalRoomId == preBookData.roomId) {

                                                // Check for room wise or hotel wise cancellation
                                                if (hotel.hotelCancellationPolicies != undefined && hotel.hotelCancellationPolicies.length > 0) {

                                                    var getCancellation = vm.parseCancellationPolicy(hotel.hotelCancellationPolicies, preBookData.hotel.rate);

                                                    room.rates[0].cancellationPolicies = hotel.hotelCancellationPolicies;
                                                    room.rates[0].isCharged = getCancellation.isCharged;
                                                    room.rates[0].cancellationPoliciesParsed = getCancellation.cancellation;

                                                } else {

                                                    var getCancellation = vm.parseCancellationPolicy(hotel.rooms[0].rate.cancellationPolicies, hotel.rooms[0].rate.pricing.totalPrice.price);

                                                    room.rates[0].cancellationPolicies = hotel.rooms[0].rate.cancellationPolicies;
                                                    room.rates[0].isCharged = getCancellation.isCharged;
                                                    room.rates[0].cancellationPoliciesParsed = getCancellation.cancellation;

                                                }
                                                room.isPrebookSent = true;
                                                break;
                                            }
                                        }
                                    }
                                } else {
                                    for (var roomIndex = 0; roomIndex < preBookData.hotel.bundledResult[preBookData.roomId].details.length; roomIndex++) {

                                        var room = preBookData.hotel.bundledResult[preBookData.roomId].details[roomIndex].room;

                                        if (hotel.hotelCancellationPolicies != undefined && hotel.hotelCancellationPolicies.length > 0) {

                                            var getCancellation = vm.parseCancellationPolicy(hotel.hotelCancellationPolicies, preBookData.hotel.rate);

                                            room.rates[0].cancellationPolicies = hotel.hotelCancellationPolicies;
                                            room.rates[0].isCharged = getCancellation.isCharged;
                                            room.rates[0].cancellationPoliciesParsed = getCancellation.cancellation;

                                        } else {

                                            var getCancellation = vm.parseCancellationPolicy(hotel.rooms[0].rate.cancellationPolicies, hotel.rooms[0].rate.pricing.totalPrice.price);

                                            room.rates[0].cancellationPolicies = hotel.rooms[0].rate.cancellationPolicies;
                                            room.rates[0].isCharged = getCancellation.isCharged;
                                            room.rates[0].cancellationPoliciesParsed = getCancellation.cancellation;
                                        }
                                        room.isPrebookSent = true;
                                    }
                                }
                                if (hotel.rooms[0].roomSpecific && hotel.rooms[0].roomSpecific.roomPrebookReference) {
                                    preBookData.hotel.hotelNorms = hotel.rooms[0].roomSpecific.roomPrebookReference.hotelNorms || [];
                                } else {
                                    preBookData.hotel.hotelNorms = [];
                                }
                                $("#cmod_" + preBookData.modalRoomIndex).modal('show');
                                preBookData.hotel.currentRoomCancellationShown = null;
                            }
                        } else {
                            preBookData.hotel.currentRoomCancellationShown = null;
                            preBookData.hotel.bookNowIsClicked = null;
                            $(".lds-css").hide();
                            $(".bckgrnd").hide();
                            if (preBookData.fromBooking) {
                                alertify.alert(i18n.t('Hotel_Results.AlertifyHeader1_Label'), i18n.t('Hotel_Results.UnableToProceedError_Label'));
                            } else {
                                alertify.alert(i18n.t('Hotel_Results.AlertifyHeader1_Label'), i18n.t('Hotel_Results.CancellationPolicy_Error'));
                            }
                        }
                    } else {
                        preBookData.hotel.currentRoomCancellationShown = null;
                        preBookData.hotel.bookNowIsClicked = null;
                        $(".lds-css").hide();
                        $(".bckgrnd").hide();
                        if (preBookData.fromBooking) {
                            alertify.alert(i18n.t('Hotel_Results.AlertifyHeader1_Label'), i18n.t('Hotel_Results.UnableToProceedError_Label'));
                        } else {
                            alertify.alert(i18n.t('Hotel_Results.AlertifyHeader1_Label'), i18n.t('Hotel_Results.CancellationPolicy_Error'));
                        }
                    }
                },
                errorCallback: function (error) {
                    preBookData.hotel.currentRoomCancellationShown = null;
                    preBookData.hotel.bookNowIsClicked = null;
                    $(".lds-css").hide();
                    $(".bckgrnd").hide();
                    // if (preBookData.fromBooking) {
                    //   alertify.alert(i18n.t('Hotel_Results.AlertifyHeader1_Label'), i18n.t('Hotel_Results.UnableToProceedError_Label'));
                    // } else {
                    //   alertify.alert(i18n.t('Hotel_Results.AlertifyHeader1_Label'), i18n.t('Hotel_Results.CancellationPolicy_Error'));
                    // }
                },
                showAlert: true
            }

            mainAxiosRequest(config);

        },
        redirectToPaxPage: function (preBookData, response) {
            delete preBookData.hotel.hotelDetailsClicked;
            delete preBookData.hotel.hotelDetailsButtonToggle;
            delete preBookData.hotel.hotelDetailsLoading;
            delete preBookData.hotel.currentRoomCancellationShown;
            delete preBookData.hotel.hotelDetailsRS.specification;
            delete preBookData.hotel.bundledResult;
            delete preBookData.hotel.optionalResult;
            delete preBookData.hotel.bookNowIsClicked;
            delete preBookData.hotel.showBundledResult;
            delete preBookData.hotel.hotelAmenitiesToggleShowMore;
            delete preBookData.hotel.roomAmenitiesToggleShowMore;

            preBookData.hotel.searchCriteria = this.searchResponseDetails;
            preBookData.hotel.searchCriteria.numberOfNights = this.numberOfNights;
            preBookData.hotel.searchCriteria.nationality = this.nationality;
            preBookData.hotel.searchCriteria.countryOfResidence = this.countryOfResidence;
            preBookData.hotel.searchCriteria.uuid = this.uuid;
            preBookData.hotel.searchCriteria.cityName = this.cityName;


            preBookData.hotel.prebookResponse = response.data.response.content.prebookResponse;
            console.log(__.cloneDeep(preBookData.hotel));
            window.sessionStorage.setItem("hotelInfo", btoa(unescape(encodeURIComponent(JSON.stringify(preBookData.hotel)))));


            //redirect
            window.location.href = "/Hotels/hotelpax.html";
        },
        showOptionalTotalSelectedPrice: function (hotel) {

            totalPrice = 0;
            for (var roomIndex = 0; roomIndex < hotel.optionalResult.length; roomIndex++) {
                var rooms = hotel.optionalResult[roomIndex].rooms
                for (var rateIindex = 0; rateIindex < rooms.length; rateIindex++) {
                    var rates = rooms[rateIindex];
                    if (rates.isSelected) {
                        totalPrice += parseFloat(rates.rates[0].pricing.totalPrice.price);
                    }
                }
            }
            hotel.updatedRate = totalPrice.toFixed(3);
            return totalPrice.toFixed(3);
        },
        selectHotelMap: function (hotelCode, supplierCode) {
            var hotel = this.hotelResults.filter(function (el) {
                return el.hotelCode == hotelCode && el.supplierCode == supplierCode;
            })[0];
            if (hotel) {
                this.hotelNameFilter = hotel.name;
                this.showMap = false;
                this.showRoomDetails(hotel);

                setTimeout(function () {
                    $('html, body').animate({
                        scrollTop: $("#hotel_" + hotelCode + supplierCode).offset().top
                    }, 300, 'linear');
                }, 500);
            }
        },
        parseCancellationPolicy: function (cancellationPolicy, roomPrice) {
            var parsedCancellationPolicies = [];
            var isCharged = false;
            for (var i = 0; i < cancellationPolicy.length; i++) {
                var localLocaleTo = moment(cancellationPolicy[i].to, "DD/MM/YYYY");
                var localLocaleFrom = moment(cancellationPolicy[i].from, "DD/MM/YYYY");

                localLocaleTo.locale(this.commonStore.selectedLanguage ? this.commonStore.selectedLanguage.code : 'en');
                localLocaleFrom.locale(this.commonStore.selectedLanguage ? this.commonStore.selectedLanguage.code : 'en');

                var to = localLocaleTo.format("MMM DD YYYY, dddd");
                var from = localLocaleFrom.format("MMM DD YYYY, dddd");
                var amount = cancellationPolicy[i].amount;
                if (parseInt(amount) == 0) {
                    parsedCancellationPolicies.push('<h4 style="color:green;">' + i18n.t('Hotel_Results.CancellationTextC1_Label') + ' <span>' + to + '</span></h4>');
                } else if (roomPrice == cancellationPolicy[i].amount && cancellationPolicy.length == 1) {
                    isCharged = true;
                    parsedCancellationPolicies.push('<h4 style="color:red;">' + i18n.t('Hotel_Results.CancellationTextB1_Label') + ' <span>' +
                        i18n.n(amount / this.commonStore.currencyMultiplier, 'currency', this.commonStore.selectedCurrency) + '</span></h4>');
                } else if (to != "" && from != "") {
                    parsedCancellationPolicies.push('<h4>' + i18n.t('Hotel_Results.CancellationTextA1_Label') + ' <span>' + from + '</span> ' + i18n.t('Hotel_Results.CancellationTextA2_Label') + ' <span>' + to + '</span> ' + i18n.t('Hotel_Results.CancellationTextA3_Label') + ' <span>' +
                        i18n.n(amount / this.commonStore.currencyMultiplier, 'currency', this.commonStore.selectedCurrency) + "</span></h4>");
                    if (moment(from, 'MMM DD YYYY, dddd').isSameOrBefore(moment().toDate(), "day")) {
                        isCharged = true;
                    }
                }

            }
            return {
                cancellation: parsedCancellationPolicies,
                isCharged: isCharged
            };
        }
    },
    watch: {
        // currency: function (newVal, oldVal) {
        //   this.hotelResults.forEach(function (t) { t.currency = newVal; });
        // },
        numberOfRooms: function (newVal, oldVal) {
            if (!this.isFromUrlParams) {
                if (newVal > oldVal) {
                    if (oldVal + 1 == newVal) {
                        this.roomSelectionDetails["room" + newVal] = {
                            adult: 1,
                            children: 0,
                            childrenAges: []
                        };
                    } else {
                        for (index = oldVal + 1; index <= newVal; index++) {
                            this.roomSelectionDetails["room" + index] = {
                                adult: 1,
                                children: 0,
                                childrenAges: []
                            };
                        }
                    }
                } else {
                    for (index = newVal + 1; index <= oldVal; index++) {
                        delete this.roomSelectionDetails["room" + index];
                    }
                }
            }
        },
        selectedSuppliers: function () {
            if (this.selectedSuppliers.length == this.supplierList.length && this.supplierList.length != 1) {
                this.supplierSearchLabel = 'All Suppliers';
            } else if (this.selectedSuppliers.length == 0) {
                this.supplierSearchLabel = 'No supplier selected';
            } else if (this.selectedSuppliers.length > 4) {
                this.supplierSearchLabel = this.selectedSuppliers.length + ' supplier selected'
            } else {
                var vm = this;
                this.supplierSearchLabel = this.selectedSuppliers.map(function (id) {
                    return vm.supplierList.filter(function (supplier) {
                        return id == supplier.id;
                    })[0].name;
                }).join(",");
            }
            if (!this.isFromUrlParams) {
                this.autoCompleteResult = [];
                this.keywordSearch = "";
            }
        },
        "commonStore.currencyMultiplier": function (newVal, oldVal) {
            var vm = this;
            vm.forCurrencyUpdatesOnly = true;

            var slider = document.getElementById("property-price-range");

            var currentRngeValues = slider.noUiSlider.get();

            var newHotelList = vm.hotelResults.filter(function (hotel) {

                return (hotel.name || "").toLowerCase().indexOf(vm.hotelNameFilter.toLowerCase()) != -1 &&
                    (hotel.location.address || "").toLowerCase().indexOf(vm.hotelLocationFilter.toLowerCase()) != -1 &&
                    vm.hotelSupplierFilter.indexOf(parseInt(hotel.supplierCode)) > -1 &&
                    vm.hotelStarFilter.indexOf(parseInt(hotel.starRating)) > -1;
            });

            var lowestPrice = Math.min.apply(Math, newHotelList.map(function (o) {
                return o.rate;
            }))
            var highestPrice = Math.max.apply(Math, newHotelList.map(function (o) {
                return o.rate;
            }))

            lowestPrice = i18n.n(lowestPrice / this.commonStore.currencyMultiplier, 'currency', this.commonStore.selectedCurrency).match(/\d+(\.\d+)?/g).join('');
            highestPrice = i18n.n(highestPrice / this.commonStore.currencyMultiplier, 'currency', this.commonStore.selectedCurrency).match(/\d+(\.\d+)?/g).join('');

            // var rateofExchange = this.commonStore.selectedCurrency == "AED" ? 1 : newVal;
            // var lowerStartValue = currentRngeValues[0] / rateofExchange;
            // var upperStartValue = currentRngeValues[1] / rateofExchange;

            vm.filteredHotelResults = vm.hotelResults;
            vm.uiHotelList = vm.filteredHotelResults;

            document.getElementById("property-price-lower").innerHTML = lowestPrice;
            document.getElementById("property-price-upper").innerHTML = highestPrice;

            slider.noUiSlider.updateOptions({
                tooltips: [true, true],
                start: [parseFloat(lowestPrice), parseFloat(highestPrice)],
                range: {
                    'min': parseFloat(lowestPrice),
                    'max': parseFloat(highestPrice)
                }
            });
        },
        "commonStore.selectedLanguage": {
            handler: function () {
                var localLocaleCin = moment(this.checkInUrlParam, "DD/MM/YYYY");
                var localLocaleCout = moment(this.checkOutUrlParam, "DD/MM/YYYY");

                localLocaleCin.locale(this.commonStore.selectedLanguage ? this.commonStore.selectedLanguage.code : 'en');
                localLocaleCout.locale(this.commonStore.selectedLanguage ? this.commonStore.selectedLanguage.code : 'en');

                this.searchDates.cin = localLocaleCin.format("DD MMM YYYY");
                this.searchDates.cout = localLocaleCout.format("DD MMM YYYY");

                // if (this.commonStore.selectedLanguage && this.commonStore.selectedLanguage.code == 'ar') {
                //   $("#txtCheckInDate").datepicker("option", $.datepicker.regional['ar']);
                //   $("#txtCheckOutDate").datepicker("option", $.datepicker.regional['ar']);
                // } else {
                //   $("#txtCheckInDate").datepicker("option", $.datepicker.regional['en-GB']);
                //   $("#txtCheckOutDate").datepicker("option", $.datepicker.regional['en-GB']);
                // }
            },
            deep: true
        }
    },
    computed: {
        // searchDates: function () {
        //   var localLocale = moment;
        //   localLocale.locale(this.commonStore.selectedLanguage ? this.commonStore.selectedLanguage.code : 'en');

        //   return {
        //     cin: localLocale(this.checkInUrlParam, "DD/MM/YYYY").format("DD MMM YYYY"),
        //     cout: localLocale(this.checkOutUrlParam, "DD/MM/YYYY").format("DD MMM YYYY")
        //   }
        // },
        selectAll: {
            get: function () {
                return this.supplierList ? this.selectedSuppliers.length == this.supplierList.length : false;
            },
            set: function (value) {
                var selectedSuppliers = [];

                if (value) {
                    this.supplierList.forEach(function (supplier) {
                        selectedSuppliers.push(supplier.id);
                    });
                }

                this.selectedSuppliers = selectedSuppliers;
            }
        }
    },
    beforeDestroy() {
        this.$eventHub.$off('select-hotel-map');
    },
});