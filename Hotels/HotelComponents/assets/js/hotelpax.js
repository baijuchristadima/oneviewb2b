Vue.component("hotel-pax-component", {
  data: function () {
    return {
      commonStore: vueCommonStore.state,
      hotelData: {},
      isHotelWiseCancellation: false,
      updatedCancellationPolicy: [],
      paxDetails: {
        contactDetails: {
          title: vueCommonStore.state.agencyNode.title.name,
          firstName: vueCommonStore.state.agencyNode.firstName,
          lastName: vueCommonStore.state.agencyNode.lastName,
          emailID: vueCommonStore.state.agencyNode.emailId,
          phoneNumber: vueCommonStore.state.agencyNode.contactNumber,
        },
        pax: [],
        paxPerRoomId: []
      },
      roomSummaryDetails: [],
      privacyPolicy: "No information available.",
      termsAndConditions: "No information available.",
      ppAndTcAccepted: false,
      paymentMethod: "",
      bookNowIsClicked: false,
      currentTotalCost: null,
      priceHasChanged: false,
      hasHotelPaymentGatewayCredential: false,
      searchDates: {
        cin: "",
        cout: ""
      },
      proceedBookingIsClicked: false,
      showPaymentGatewayButton: false,
    };
  },
  created: function () {
    var vm = this;
    vm.veeValidateLocaleUpdate();
    var hotelDataFromResults = window.sessionStorage.getItem("hotelInfo");
    if (hotelDataFromResults) {
      this.getTermsnConditions();

      console.log(window.localStorage.getItem("accessToken"));
      var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;

      var sessionURL = hubUrl + '/payment';

      axios.get(sessionURL, {
        headers: {
          "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
        }
      }).then((response) => {
        if (response.data) {
          vm.hasHotelPaymentGatewayCredential = true;
        }
      }).catch((error) => {
        vm.hasHotelPaymentGatewayCredential = false;
      });

      this.hotelData = JSON.parse(decodeURIComponent(escape(atob(hotelDataFromResults))));

      this.currentTotalCost = this.hotelData.updatedRate;

      var paxDetails = [];
      var paxPerRoomId = [];
      var paxId = 1;
      for (var roomIndex = 0; roomIndex < this.hotelData.searchCriteria.searchedRooms.length; roomIndex++) {
        var perRoomDetails = [];
        var guests = this.hotelData.searchCriteria.searchedRooms[roomIndex].guests;
        var paxesId = [];
        for (var guestIndex = 0; guestIndex < guests.adult; guestIndex++) {
          paxesId.push(paxId);
          perRoomDetails.push({
            paxId: paxId++,
            leadPax: guestIndex == 0 ? true : false,
            age: null,
            name: "",
            surName: "",
            title: "Mr",
            type: "Adult",
            label: guestIndex == 0 ? "LeadPax_Label" : "Pax_Label"
          });
        }
        if (guests.children != undefined && guests.children.length > 0) {
          for (var guestIndex = 0; guestIndex < guests.children.length; guestIndex++) {
            paxesId.push(paxId);
            perRoomDetails.push({
              paxId: paxId++,
              leadPax: false,
              age: guests.children[guestIndex].age,
              name: "",
              surName: "",
              title: "Master",
              type: "Child",
              label: guests.children[guestIndex].age < 2 ? "Infant_Label" : "Child1_Label"
            });
          }
          this.roomSummaryDetails.push({
            traveller: guests.adult + (guests.adult > 1 ? ' Adults' : ' Adult') + ' ' + guests.children.length + (guests.children.length > 1 ? ' Children' : ' Child'),
            roomType: null
          });
          paxDetails.push({
            adult: guests.adult,
            child: guests.children.length,
            paxPerRoom: perRoomDetails
          });
        } else {
          this.roomSummaryDetails.push({
            traveller: guests.adult + (guests.adult > 1 ? ' Adults' : ' Adult'),
            roomType: null
          });
          paxDetails.push({
            adult: guests.adult,
            child: 0,
            paxPerRoom: perRoomDetails
          });
        }
        paxPerRoomId.push(paxesId);
      }

      this.paxDetails.pax = paxDetails;
      this.paxDetails.paxPerRoomId = paxPerRoomId;


      for (var travellerIndex = 0; travellerIndex < this.roomSummaryDetails.length; travellerIndex++) {
        this.roomSummaryDetails[travellerIndex].roomPrice = this.hotelData.selectedRooms.data[travellerIndex].room.rates[0].pricing.totalPrice.price;
        this.roomSummaryDetails[travellerIndex].roomId = this.hotelData.selectedRooms.data[travellerIndex].room.roomId;
        this.roomSummaryDetails[travellerIndex].roomType = "Room " + (travellerIndex + 1) + ": " + this.hotelData.selectedRooms.data[travellerIndex].room.roomName + ' ' + '(' + this.hotelData.selectedRooms.data[travellerIndex].room.rates[0].boards[0].value.charAt(0).toUpperCase() + __.unescape(this.hotelData.selectedRooms.data[travellerIndex].room.rates[0].boards[0].value.slice(1).toLowerCase()) + ')';
      }

    } else {
      alertify.alert(i18n.t('Hotel_Pax.AlertifyHeader1_Label'), i18n.t('Hotel_Pax.RoomDetailsNotFound_Label'),
        function () {
          window.location.href = "/search.html";
        })
    }
  },
  mounted: function () {
    var vm = this;
    this.$nextTick(function () {
      var agencyNode = window.localStorage.getItem("agencyNode");
      if (agencyNode) {
        agencyNode = JSON.parse(atob(agencyNode));

        $("#phone").intlTelInput({
          initialCountry: agencyNode.loginNode.country.code || "AE",
          geoIpLookup: function (callback) {
            $.get('https://ipinfo.io', function () { }, "jsonp").always(function (resp) {
              var countryCode = (resp && resp.country) ? resp.country : "";
              (countryCode);
            });
          },
          separateDialCode: true,
          autoPlaceholder: "off",
          preferredCountries: ['ae', 'sa', 'om', 'qa'],
          utilsScript: "/assets/plugins/IntlInput/intlTelInput.js?v=201905200000" // just for formatting/placeholders etc
        });

      }

      // //Don't remove scrollbar when showing alertify pop up.
      // alertify.defaults.preventBodyShift = true;
      // //Don't focus on top.
      // alertify.defaults.maintainFocus = false;

      $("#bookInfoModal").on("hidden.bs.modal", function () {
        vm.bookNowIsClicked = false;
      });
    })
    setTimeout(() => {
      vm.paymentGatewayButton();
    }, 1000);

  },
  methods: {
    veeValidateLocaleUpdate: function () {
      var vm = this;
      if (this.commonStore.selectedLanguage && this.commonStore.selectedLanguage.code) {
        this.$validator.localize(this.commonStore.selectedLanguage.code, {
          messages: {
            required: (field) => '* ' + field + ' ' + i18n.t('Hotel_Pax.Required_Message'),
            alpha_spaces: () => '* ' + i18n.t('Hotel_Pax.AlphaSpaces_Message'),
            email: () => '* ' + i18n.t('Hotel_Pax.Email_Message'),
            numeric: () => '* ' + i18n.t('Hotel_Pax.Numeric_Message'),
            max: () => '* ' + i18n.t('Hotel_Pax.MaximumLength_Message'),
            regex: () => '* ' + i18n.t('Hotel_Pax.Regex_Message'),
            min: () => '* Minimum of 2 characters required',
            max: () => '* Maximum of 25 characters required',
            alpha: () => '* Please enter alphabetic characters without white spaces'
          },
          attributes: {
            terms: i18n.t('Hotel_Pax.TermsAndPolicy_Message')
          }
        })
      }
      this.$validator.reset();

      this.$validator.extend('ppandtc', {
        getMessage: () => i18n.t('Hotel_Pax.TermsAndPolicy_Message'),
        validate: () => vm.ppAndTcAccepted ? true : false
      });

    },
    imgUrlAlt: function (event) {
      event.target.src = "/Hotels/HotelComponents/assets/images/noimage.gif";
    },
    bookNow: function () {
      var vm = this;
      vm.proceedBookingIsClicked = false;

      this.$validator.validateAll().then((result) => {
        if (result) {

          vm.bookNowIsClicked = true;

          var totalAmount = 0;
          var sealCode = "";
          var rooms = [];
          for (var index = 0; index < this.hotelData.selectedRooms.data.length; index++) {
            var room = this.hotelData.selectedRooms.data[index].room || this.hotelData.selectedRooms.data[index];
            rooms.push({
              name: room.roomName,
              roomId: room.roomId,
              paxes: this.paxDetails.paxPerRoomId[index],
              guests: room.guests,
              rate: {
                bookable: true,
                cancellationPolicies: room.rates[0].cancellationPolicies,
                rateSpecific: room.rates[0].roomRateSpecific,
                pricing: {
                  perNightPrice: room.rates[0].pricing.perNightPrice,
                  totalPrice: room.rates[0].pricing.totalPrice
                },
                roomRateReferences: room.rates[0].roomRateSpecific ? room.rates[0].roomRateSpecific.roomRateReferences : undefined

              },
              roomReferences: room.roomSpecific ? room.roomSpecific.roomReferences : undefined,
            });
            totalAmount += room.rates[0].pricing.totalPrice.price;
            sealCode = room.rates[0].pricing.totalPrice.sealCode;
          }

          var paxes = []
          for (var paxIndex = 0; paxIndex < this.paxDetails.pax.length; paxIndex++) {
            for (var roomIndex = 0; roomIndex < this.paxDetails.pax[paxIndex].paxPerRoom.length; roomIndex++) {
              var pax = __.cloneDeep(this.paxDetails.pax[paxIndex].paxPerRoom[roomIndex]);
              delete pax.label;
              paxes.push(pax);
            }
          }

          var PrebookRQ = {
            service: "HotelRQ",
            node: this.hotelData.searchCriteria.node,
            token: this.hotelData.searchCriteria.token,
            hubUuid: this.hotelData.searchCriteria.hubUuid,
            supplierCodes: [this.hotelData.supplierCode],
            credentials: [],
            content: {
              command: "PrebookRQ",
              hotel: {
                cityCode: this.hotelData.location.cityCode,
                hotelCode: this.hotelData.supplierCode == 64 ? this.hotelData.hotelDetailsRS.hotelCode : this.hotelData.hotelCode,
                totalAmount: totalAmount,
                sealCode: sealCode,
                nationality: this.hotelData.searchCriteria.nationality,
                residentOf: this.hotelData.searchCriteria.countryOfResidence,
                rooms: rooms,
                hotelSpecific: this.hotelData.hotelSpecific
              },
              paxes: paxes,
              stay: this.hotelData.searchCriteria.stay,
              supplierSpecific: {
                uuid: this.hotelData.searchCriteria.uuid,
                requestType: "priceRecheck",
                searchReferences: this.hotelData.searchReferences,
                searchHotelReferences: this.hotelData.hotelSpecific ? this.hotelData.hotelSpecific.searchHotelReferences : undefined,
                roomsReferences: this.hotelData.roomSpecific ? this.hotelData.roomSpecific.roomsReferences : undefined,
                hotelName: this.hotelData.supplierCode == 64 ? this.hotelData.hotelDetailsRS.name : this.hotelData.name,
                hotelIndex: this.hotelData.index,
                roomResponse: this.hotelData.roomSupplierSpecific ? this.hotelData.roomSupplierSpecific.roomResponse : undefined
              }
            }
          }


          this.sendPrebook({
            request: PrebookRQ,
            roomId: null,
            isOptional: false,
            fromBooking: true
          });

        } else {
          if(!this.paymentMethod){
            alertify.alert(i18n.t('Hotel_Pax.AlertifyHeader2_Label'), "Please choose the payment mode.");
          }
          else if (this.$validator.errors.collect('terms').length == 1 && this.$validator.errors.all().length == 1) {
            alertify.alert(i18n.t('Hotel_Pax.AlertifyHeader2_Label'), this.$validator.errors.collect('terms')[0]);
          } else {
            alertify.alert(i18n.t('Hotel_Pax.AlertifyHeader2_Label'), i18n.t('Hotel_Pax.RequiredFields_Label'));

          }
        }
      });
    },
    sendPrebook: function (data) {
      var vm = this;

      var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;

      var config = {
        axiosConfig: {
          method: "POST",
          url: hubUrl + "/preBook",
          data: {
            request: data.request
          }
        },
        successCallback: function (response) {
          var hotel = response.data.response.content.prebookResponse.hotel;
          // this.currentTotalCost = this.hotelData.updatedRate;

          var roomIsBookable = true;
          if (response.data.response.content.prebookResponse.bookable) {

            for (var roomIndex = 0; roomIndex < hotel.rooms.length; roomIndex++) {
              var room = hotel.rooms[roomIndex];
              if (typeof room.rate.bookable != "undefined" && !room.rate.bookable) {
                roomIsBookable = false;
              }
            }
            if (roomIsBookable) {

              // Check updated price
              if (vm.currentTotalCost != response.data.response.content.prebookResponse.currentPrice) {

                vm.hotelData.updatedRate = response.data.response.content.prebookResponse.currentPrice;
                var prebookRooms = response.data.response.content.prebookResponse.hotel.rooms;

                for (var travellerIndex = 0; travellerIndex < vm.roomSummaryDetails.length; travellerIndex++) {
                  for (var prebookRoomIndex = 0; prebookRoomIndex < prebookRooms.length; prebookRoomIndex++) {
                    var prebookRoom = prebookRooms[prebookRoomIndex];
                    if (vm.roomSummaryDetails[travellerIndex].roomId == prebookRoom.roomId) {
                      vm.roomSummaryDetails[travellerIndex].roomPrice = prebookRoom.rate.pricing.totalPrice.price;
                      break;
                    }
                  }
                }

                for (var roomIndex = 0; roomIndex < vm.hotelData.selectedRooms.data.length; roomIndex++) {
                  var room = vm.hotelData.selectedRooms.data[roomIndex].room;
                  // Check for room wise or hotel wise cancellation

                  if (hotel.hotelCancellationPolicies != undefined && hotel.hotelCancellationPolicies.length > 0) {

                    room.rates[0].cancellationPolicies = hotel.hotelCancellationPolicies;
                    room.rates[0].cancellationPoliciesParsed = vm.parseCancellationPolicy(hotel.hotelCancellationPolicies, response.data.response.content.prebookResponse.currentPrice);

                    room.isPrebookSent = true;
                  } else {
                    for (var prebookRoomIndex = 0; prebookRoomIndex < prebookRooms.length; prebookRoomIndex++) {
                      var prebookRoom = prebookRooms[prebookRoomIndex];
                      if (room.roomId == prebookRoom.roomId) {

                        room.isPrebookSent = true;
                        room.rates[0].cancellationPolicies = prebookRoom.rate.cancellationPolicies;

                        room.rates[0].cancellationPoliciesParsed = vm.parseCancellationPolicy(prebookRoom.rate.cancellationPolicies, prebookRoom.rate.pricing.totalPrice.price);
                        break;
                      }
                    }
                  }
                  for (var prebookRoomIndex = 0; prebookRoomIndex < prebookRooms.length; prebookRoomIndex++) {
                    var prebookRoom = prebookRooms[prebookRoomIndex];
                    if (room.roomId == prebookRoom.roomId) {
                      room.rates[0].pricing = prebookRooms[prebookRoomIndex].rate.pricing;
                      break;
                    }
                  }
                }
                vm.priceHasChanged = true;

                var cancellationParsed = [];
                if (vm.hotelData.prebookResponse.hotel.hotelCancellationPolicies) {
                  vm.isHotelWiseCancellation = vm.hotelData.prebookResponse.hotel.hotelCancellationPolicies.length > 0;
                }

                if (vm.isHotelWiseCancellation) {
                  cancellationParsed.push(vm.parseCancellationPolicy(vm.hotelData.selectedRooms.data[0].room.rates[0].cancellationPolicies, vm.hotelData.updatedRate));
                } else {
                  for (var index = 0; index < vm.hotelData.selectedRooms.data.length; index++) {
                    cancellationParsed.push(vm.parseCancellationPolicy(vm.hotelData.selectedRooms.data[index].room.rates[0].cancellationPolicies, vm.hotelData.selectedRooms.data[index].room.rates[0].pricing.totalPrice.price));
                  }
                }

                vm.updatedCancellationPolicy = cancellationParsed;
                $(".pax-confirm").css("cssText", "width:750px;margin-top: 100px;");
              }
            }
          } else {
            roomIsBookable = false;
          }

          if (roomIsBookable) {
            vm.hotelData.prebookResponse = response.data.response.content.prebookResponse;
            vm.hotelData.prebookResponse.supplierSpecific = response.data.response.content.supplierSpecific;

            $("#bookInfoModal").modal({
              backdrop: 'static',
              keyboard: false,
              show: true
            });
          } else {
            alertify.alert(i18n.t('Hotel_Pax.AlertifyHeader2_Label'), i18n.t('Hotel_Pax.RoomNotBookable_Label'));

            vm.bookNowIsClicked = true;
          }
        },
        errorCallback: function (error) {
          vm.bookNowIsClicked = true;
          // alertify.alert(i18n.t('Hotel_Pax.AlertifyHeader1_Label'), i18n.t('Hotel_Pax.ProblemInBooking_Label'));

        },
        showAlert: true
      };

      mainAxiosRequest(config);

    },
    backToSearchPage: function () {
      window.location.href = "/search.html";
    },
    proceedBooking: function () {
      var vm = this;

      // vm.paxDetails.contactDetails.phoneNumber = $(".selected-dial-code").html() + "-" + vm.paxDetails.contactDetails.phoneNumber;
      if (!vm.proceedBookingIsClicked) {

        vm.proceedBookingIsClicked = true;
        var paxId = 1;
        var paxesPerRoom = [];

        for (var index = 0; index < this.paxDetails.pax.length; index++) {

          var paxes = __.map(this.paxDetails.pax[index].paxPerRoom, function (e) {
            return {
              serialNumber: paxId++,
              firstName: e.name,
              surName: e.surName,
              age: e.age,
              isLead: e.leadPax,
              titleId: e.title == "Mr" ? 1 : e.title == "Mrs" ? 3 : e.title == "Ms" ? 4 : e.title == "Master" ? 5 : 2,
              paxTypeCode: e.type == "Adult" ? "ADT" : "CHD",
              email: vm.paxDetails.contactDetails.emailID,
              phone: $(".selected-dial-code").html() + vm.paxDetails.contactDetails.phoneNumber,
              nationalityCode: vm.hotelData.searchCriteria.nationality,
              countryOfResidenceCode: vm.hotelData.searchCriteria.countryOfResidence
            }
          })
          paxesPerRoom.push(paxes);
        }

        var cancellationDeadline = "";
        var rooms = [];
        for (var index = 0; index < this.hotelData.selectedRooms.data.length; index++) {
          var room = this.hotelData.selectedRooms.data[index].room;
          var cancellation = [];
          if (!vm.isHotelWiseCancellation) {
            var sortedCancellation = JSON.parse(JSON.stringify(room.rates[0].cancellationPolicies));

            sortedCancellation.sort(function (a, b) {
              return new Date(b.date) - new Date(a.date);
            });

            for (var c = 0; c < sortedCancellation.length; c++) {
              if (parseInt(sortedCancellation[c].amount) != 0) {
                cancellationDeadline = moment(sortedCancellation[c].from, 'DD/MM/YYYY').format("YYYY-MM-DDTHH:mm:ss.000+0000");
                break;
              }
            }
        // remarks return the same value we get or return true
            cancellation = __.map(room.rates[0].cancellationPolicies, function (e, i) {
              return {
                fromDate: moment(e.from, 'DD/MM/YYYY').format("YYYY-MM-DDTHH:mm:ss.000+0000"),
                toDate: moment(e.to, 'DD/MM/YYYY').format("YYYY-MM-DDTHH:mm:ss.000+0000"),
                totalCost: e.amount,
                remarks:  e.remarks ? e.remarks : true
              };
            });
          }

          rooms.push({
            roomQuotes: [{
              cancellationPolicy: cancellation,
              guests: paxesPerRoom[index],
              serialNumber: index + 1,
              date: moment(new Date()).format("YYYY-MM-DDTHH:mm:ss.000+0000"),
              numberOfAdults: vm.paxDetails.pax[index].adult,
              status: "pending",
              checkInDate: moment(vm.hotelData.searchCriteria.stay.checkIn, 'DD/MM/YYYY').format("YYYY-MM-DDTHH:mm:ss.000+0000"),
              checkOutDate: moment(vm.hotelData.searchCriteria.stay.checkOut, 'DD/MM/YYYY').format("YYYY-MM-DDTHH:mm:ss.000+0000"),
              nights: vm.hotelData.searchCriteria.numberOfNights,
              costPerNight: 0.0,
              totalCost: 0.0,
              supplierRemarks: vm.hotelData.prebookResponse.hotel.rooms[index].rate.rateSpecific ?
                vm.hotelData.prebookResponse.hotel.rooms[index].rate.rateSpecific.rateComments : undefined
            }],
            pricing: {
              perNightPrice: room.rates[0].pricing.perNightPrice,
              totalPrice: room.rates[0].pricing.totalPrice
            },
            roomTypeId: 1,
            category: room.roomName,
            meal: room.rates[0].boards[0].value.charAt(0).toUpperCase() + __.unescape(room.rates[0].boards[0].value.slice(1).toLowerCase()),
            isExtraBed: false,
            isBabyCot: false,
            supplierRoomType: room.roomName,
            roomDetail: null
          });
        }

        if (vm.isHotelWiseCancellation) {
          var sortedCancellation = JSON.parse(JSON.stringify(vm.hotelData.prebookResponse.hotel.hotelCancellationPolicies));

          sortedCancellation.sort(function (a, b) {
            return new Date(b.date) - new Date(a.date);
          });

          for (var c = 0; c < sortedCancellation.length; c++) {
            if (parseInt(sortedCancellation[c].amount) != 0) {
              cancellationDeadline = moment(sortedCancellation[c].from, 'DD/MM/YYYY').format("YYYY-MM-DDTHH:mm:ss.000+0000");
              break;
            }
          }
          // remarks return the same value we get or return true
          var hotelWiseCancellation = __.map(vm.hotelData.prebookResponse.hotel.hotelCancellationPolicies, function (e, i) {
            return {
              fromDate: moment(e.from, 'DD/MM/YYYY').format("YYYY-MM-DDTHH:mm:ss.000+0000"),
              toDate: moment(e.to, 'DD/MM/YYYY').format("YYYY-MM-DDTHH:mm:ss.000+0000"),
              totalCost: e.amount,
              remarks: e.remarks ? e.remarks : true
            }
          });
        };

        var hubBookRQ = {
          booking: {
            hotels: [{
              rooms: rooms,
              cancellationPolicy: hotelWiseCancellation,
              serialNumber: 1,
              supplierCode: vm.hotelData.supplierCode,
              name: vm.hotelData.name,
              address: vm.hotelData.location.address,
              cityCode: vm.hotelData.location.cityCode,
              cityName: vm.hotelData.searchCriteria.cityName,
              telephone: "556646545",
              starCategory: parseFloat(vm.hotelData.starRating),
              payableBy: "GTA",
              emergencyNumber: "556646545",
              minimumNights: 1,
              tariffRemarks: "www"
            }],
            serviceId: 2,
            supplierId: vm.hotelData.supplierCode,
            reference: vm.hotelData.searchCriteria.uuid,
            comments: "comments",
            bookPosition: 1,
            supplierRemarks: vm.hotelData.hotelPolicies,
            otherDetails: vm.hotelData.hotelCode,
            bookDate: moment(new Date()).format("YYYY-MM-DDTHH:mm:ss.000+0000"),
            deadlineDate: cancellationDeadline
          },
          sealedData: vm.hotelData.prebookResponse.sealedData,
          paymentMode: this.paymentMethod,
          couponUsedIds: []
        }

        var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;

        var config = {
          axiosConfig: {
            method: "POST",
            url: hubUrl + "/hotelBook/booking",
            data: hubBookRQ
          },
          successCallback: function (response) {
            console.log(response.data);

            if (response.data) {

              vm.hotelData.bookingId = response.data.referenceNumber;
              vm.hotelData.cartID = response.data.cartId;
              vm.hotelData.paymentMethod = vm.paymentMethod;

              vm.hotelData.paxDetails = vm.paxDetails;
              window.sessionStorage.setItem("hotelInfo", btoa(unescape(encodeURIComponent(JSON.stringify(vm.hotelData)))));

              console.log(__.cloneDeep(vm.hotelData));

              if (vm.paymentMethod == 7) {
                window.location.href = "hotelconfirmation.html"
              } else {
                //pg 
                var paymentDetails = {
                  bookingReference: response.data.referenceNumber,
                  totalAmount: vm.hotelData.updatedRate.toString().indexOf(".") > 0 ? vm.hotelData.updatedRate : parseFloat(vm.hotelData.updatedRate).toFixed(2),
                  customerEmail: vm.paxDetails.contactDetails.emailID,
                  cartID: response.data.cartId,
                  supplier: vm.hotelData.supplierCode, // for hotel logging
                  uuid: vm.hotelData.searchCriteria.uuid, // for hotel logging
                  currentPayGateways:vm.commonStore.agencyNode.loginNode.paymentGateways,
                }
                console.log("before pg", window.localStorage.getItem("accessToken"));
                vm.paymentForm = paymentManager(paymentDetails, window.location.origin + "/Hotels/hotelconfirmation.html");
                console.log("before pg form", window.localStorage.getItem("accessToken"));

              }

            }
          },
          errorCallback: function (error) {
            vm.bookNowIsClicked = true;
            vm.proceedBookingIsClicked = false;
            // alertify.alert(i18n.t('Hotel_Pax.AlertifyHeader1_Label'), i18n.t('Hotel_Pax.ProblemInBooking_Label'));

          },
          showAlert: true
        };

        mainAxiosRequest(config);
      }

    },
    parseCancellationPolicy: function (cancellationPolicy, roomPrice) {

      var parsedCancellationPolicies = [];
      for (var i = 0; i < cancellationPolicy.length; i++) {

        var localLocaleTo = moment(cancellationPolicy[i].to, "DD/MM/YYYY");
        var localLocaleFrom = moment(cancellationPolicy[i].from, "DD/MM/YYYY");

        localLocaleTo.locale(this.commonStore.selectedLanguage ? this.commonStore.selectedLanguage.code : 'en');
        localLocaleFrom.locale(this.commonStore.selectedLanguage ? this.commonStore.selectedLanguage.code : 'en');

        var to = localLocaleTo.format("MMM DD YYYY, dddd");
        var from = localLocaleFrom.format("MMM DD YYYY, dddd");
        var amount = cancellationPolicy[i].amount;
        if (parseInt(amount) == 0) {
          parsedCancellationPolicies.push('<h4 style="color:green;">' + i18n.t('Hotel_Pax.CancellationTextC1_Label') + ' <span>' + to + '</span></h4>');
        } else if (roomPrice == cancellationPolicy[i].amount && cancellationPolicy.length == 1) {
          parsedCancellationPolicies.push('<h4 style="color:red;">' + i18n.t('Hotel_Pax.CancellationTextB1_Label') + ' <span>' +
            i18n.n(amount / this.commonStore.currencyMultiplier, 'currency', this.commonStore.selectedCurrency) + '</span></h4>');
        } else if (to != "" && from != "") {
          parsedCancellationPolicies.push('<h4>' + i18n.t('Hotel_Pax.CancellationTextA1_Label') + ' <span>' + from + '</span> ' + i18n.t('Hotel_Pax.CancellationTextA2_Label') + ' <span>' + to + '</span> ' + i18n.t('Hotel_Pax.CancellationTextA3_Label') + ' <span>' +
            i18n.n(amount / this.commonStore.currencyMultiplier, 'currency', this.commonStore.selectedCurrency) + "</span></h4>");
          if (moment(from, 'MMM DD YYYY, dddd').isSameOrBefore(moment().toDate(), "day")) { }
        }

      }
      return parsedCancellationPolicies;
    },
    getTermsnConditions: function () {
      try {
        var vm = this;
        var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
        var serviceUrl = vm.commonStore.hubUrls.hubConnection.hubServices.contact;
        axios({
          method: "get",
          url: hubUrl + serviceUrl,
          headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
          }
        }).then(function (response) {
          var termsNconditions = vm.getContentTypeFromKey('termsnconditions', 'HotelTermsNConditions', response.data.siteList);

          if (stringIsNullorEmpty(termsNconditions)) {
            termsNconditions = vm.getContentTypeFromKey('termsnconditions', 'CommonTermsNConditions', response.data.siteList);
            if (stringIsNullorEmpty(termsNconditions)) {
              termsNconditions = i18n.t('Hotel_Pax.NoInfoAvailable_Label');
            }
          }

          var privacyPolicy = vm.getContentTypeFromKey('privacypolicy', 'HotelPrivacyPolicy', response.data.siteList);
          if (stringIsNullorEmpty(privacyPolicy)) {
            privacyPolicy = vm.getContentTypeFromKey('privacypolicy', 'CommonPrivacyPolicy', response.data.siteList);
            if (stringIsNullorEmpty(privacyPolicy)) {
              privacyPolicy = i18n.t('Hotel_Pax.NoInfoAvailable_Label');
            }
          }

          vm.privacyPolicy = privacyPolicy;
          vm.termsAndConditions = termsNconditions;

        }).catch(function (error) {
          console.log('Error: Terms n Conditions');
        });
      } catch (err) {
        console.log('Error: Terms n Conditions');
      }
    },
    getContentTypeFromKey: function (contentType, keyValue, siteList) {
      var value = '';
      var siteContents = siteList;
      if (hasArrayData(siteContents)) {
        var contentTypes = siteContents[0].contentType.filter(function (type) {
          return type.description.toLowerCase().split(' ').join('') == contentType.toLowerCase() &&
            type.name.toLowerCase().split(' ').join('') == keyValue.toLowerCase()
        });
        if (hasArrayData(contentTypes)) {
          value = contentTypes[0].value;
        }
      }
      return value;
    },
    paymentGatewayButton: function(){
      var self = this;
      if (self.commonStore.agencyNode.loginNode.nodetype !="HQ"){
          self.getHQPaymentGateway();
      } else {
          self.creditCardButton(self.commonStore.agencyNode.loginNode.paymentGateways);
      }
    },
    creditCardButton:function(paymetGateways){
        var self = this;
        if(paymetGateways){
        var paymentMethods = paymetGateways.filter(function (item) { return item.id == 2; });
        if (self.commonStore.commonRoles.hasHotelPaymentGateway && paymentMethods) {
            self.showPaymentGatewayButton = true;
        }
      }
      else{
        self.showPaymentGatewayButton = false;
        self.paymentMethod = 7;
    }
    },

    // get HQ node payment gateway privilege because payment gateway not getting in subnodes
    getHQPaymentGateway: function () {
      var vm = this;
      var hqNodeId = vm.commonStore.agencyNode.loginNode.solutionId;
      if (hqNodeId){

          var huburl = vm.commonStore.hubUrls.hubConnection.baseUrl;
          var portno = vm.commonStore.hubUrls.hubConnection.ipAddress;
          var serviceUrl = vm.commonStore.hubUrls.hubConnection.hubServices.paymentGatewayHQ;

          var config = {
              axiosConfig: {
                  method: "GET",
                  url: huburl + portno + serviceUrl + hqNodeId
              },
              successCallback: function (response) {
                  try {
                      
                      if (response.data){
                          console.log("HQ payment gateways : ", response.data);
                          vm.creditCardButton(response.data);
                      }
                     
                  } catch (err) {
                      console.log('no payment gateway found in hqnode');
                  }
              },
              errorCallback: function (error) { },
              showAlert: false
          };

          mainAxiosRequest(config);
      }
  }

  },
  watch: {
    "commonStore.selectedLanguage": {
      handler: function () {
        var localLocaleCin = moment(this.hotelData.searchCriteria.stay.checkIn, "DD/MM/YYYY");
        var localLocaleCout = moment(this.hotelData.searchCriteria.stay.checkOut, "DD/MM/YYYY");

        localLocaleCin.locale(this.commonStore.selectedLanguage ? this.commonStore.selectedLanguage.code : 'en');
        localLocaleCout.locale(this.commonStore.selectedLanguage ? this.commonStore.selectedLanguage.code : 'en');

        this.searchDates.cin = localLocaleCin.format("DD MMM YYYY");
        this.searchDates.cout = localLocaleCout.format("DD MMM YYYY");

        // if (this.commonStore.selectedLanguage && this.commonStore.selectedLanguage.code == 'ar') {
        //   $("#txtCheckInDate").datepicker("option", $.datepicker.regional['ar']);
        //   $("#txtCheckOutDate").datepicker("option", $.datepicker.regional['ar']);
        // } else {
        //   $("#txtCheckInDate").datepicker("option", $.datepicker.regional['en-GB']);
        //   $("#txtCheckOutDate").datepicker("option", $.datepicker.regional['en-GB']);
        // }

        this.veeValidateLocaleUpdate();
      },
      deep: true
    }
  },
  computed: {
    // searchDates: function () {
    //   return {
    //     cin: moment(this.hotelData.searchCriteria.stay.checkIn, "DD/MM/YYYY").format("DD MMM YYYY"),
    //     cout: moment(this.hotelData.searchCriteria.stay.checkOut, "DD/MM/YYYY").format("DD MMM YYYY")
    //   }
    // },
    hotelNormsCompare: function () {
      var vm = this;
      try {
        return vm.hotelData.selectedRooms.data.every(function (e) {
          return vm.hotelData.selectedRooms.data[0].room.rates[0].roomRateSpecific.roomRateReferences.hotelNorms[0].localeCompare(e.room.rates[0].roomRateSpecific.roomRateReferences.hotelNorms[0]) == 0 ? true : false;
        })
      } catch (error) {
        return false;
      }

    }
  }
});