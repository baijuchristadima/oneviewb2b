var dateFormat = generalInformation.systemSettings.systemDateFormat;
var statusCodeList = [];
var searchParams = {};

Vue.component("my-quotation-details", {
  components: {
  },
  data: function () {
    return {
      commonStore: vueCommonStore.state,
      quoteDetails: {},
      quoteEmailTo: "",
      quoteEmailCC: "",
      quoteEmailSubject: "",
    };
  },
  mounted: function () {
    this.getQuotationDetailsOnServer();
  },
  methods: {
    getQuotationDetailsOnServer: function () {
      var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;
      var serviceUrl = this.commonStore.hubUrls.hubConnection.hubServices.quoteDetails;
      var vm = this;
      var quotationId = window.sessionStorage.getItem("quotationId");

      var config = {
        axiosConfig: {
          method: "post",
          url: hubUrl + serviceUrl + "/" + quotationId
        },
        successCallback: function (response) {
          vm.quoteDetails = response.data.data;
          vm.viewQuotationContent();
        },
        errorCallback: function (error) {
          
        },
        showAlert: false
      };

      mainAxiosRequest(config);
    },
    viewQuotationContent: function () {
        var vm = this;
      setTimeout(function () {
        $('#summernote').summernote({
          disableGrammar: true,
          spellCheck: false,
          toolbar: []
          // toolbar: [
          //     // [groupName, [list of button]]
          //     ['style', ['bold', 'italic', 'underline', 'clear']],
          //     ['font', ['strikethrough', 'superscript', 'subscript']],
          //     ['fontsize', ['fontsize']],
          //     ['color', ['color']],
          //     ['para', ['ul', 'ol', 'paragraph']],
          //     ['height', ['height']]
          // ]
        });
        var agencyFolderName = "";
        axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
          for (var index = 0; index < response.data.length; index++) {
            var domain = response.data[index].domain;
            if (domain.indexOf(window.location.hostname) != -1) {
              agencyFolderName = response.data[index].agencyFolderName;
            }
          }
          var styles = "font-family:Verdana, Geneva, sans-serif;font-size:13px;color:#000;";

          var summaryTable = "";
          var summaryDetails = "";

          var selectedQuotations = JSON.parse(vm.quoteDetails.body);

          var selectedQuotationsFlights = selectedQuotations.services.flights;
          for (var k = 0; k < selectedQuotationsFlights.length; k++) {
            var currentItem = selectedQuotationsFlights[k];
            var firstSegment = currentItem.groupOfFlights[0].flightDetails[0].fInfo;
            var locationFromF = vm.getCityNameUTCOnlyWithCityCode(firstSegment.location.locationFrom);
            var departureDateF = vm.getdatetimeformated(firstSegment.dateTime.depDate, 'DDMMYY', 'DD MMM YYYY') + " " + vm.gettimeformated(firstSegment.dateTime.depTime, 'HHmm', 'HH', ': ', 'mm', '');

            var lastSegment = currentItem.groupOfFlights[0].flightDetails[currentItem.groupOfFlights[0].flightDetails.length - 1].fInfo;
            var locationToL = vm.getCityNameUTCOnlyWithCityCode(lastSegment.location.locationTo);
            var departureDateL = vm.getdatetimeformated(lastSegment.dateTime.arrDate, 'DDMMYY', 'DD MMM YYYY') + " " + vm.gettimeformated(lastSegment.dateTime.arrTime, 'HHmm', 'HH', ': ', 'mm', '');

            summaryTable += `<tr id="` + (k + 1) + `">
                            <td style="text-align:center;">`+ (k + 1) + `</td>
                            <td style="text-align:center;">`+ locationFromF + `<br />` + departureDateF + `</td>
                            <td style="text-align:center;">`+ locationToL + `<br />` + departureDateL + `</td>
                            <td style="text-align:center;">`+ currentItem.adt + `</td>
                            <td style="text-align:center;">`+ currentItem.chd + `</td>
                            <td style="text-align:center;">`+ currentItem.inf + `</td>
                            <td style="text-align:center;"><img class="img_itinerary" src="`+ window.location.origin + `/Flights/FlightComponents/EmailTemplates/downloadvoucher/Airlines/` + firstSegment.companyId.mCarrier + `.gif" /><br>` + firstSegment.companyId.mCarrier + '-' + firstSegment.flightNo + `</td>
                            <td style="text-align:center;">` + i18n.n(currentItem.fare.amount / vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency) + `</td>
                            <td style="text-align:center;">` + (currentItem.groupOfFlights[0].flightDetails.length - 1) + `</td>
                            <td style="text-align:center;">` + vm.tripTypeSelected(currentItem.tripType) + `</td>
                        </tr>`

            summaryDetails += `<div><div style="width:100%; float:left;margin-top:10px;"><div style="font-family:Verdana, Geneva, sans-serif;font-size:17px;color:#2e475d;font-weight:700; padding:0px;margin:0px;margin-bottom:8px;padding-bottom:5px;float:left;">` + locationFromF + ` to ` + locationToL + ` <span style="font-size:13px">` + vm.getdatetimeformated(firstSegment.dateTime.depDate, 'DDMMYY', 'DD MMM YYYY') + `</span></div>
                                <div style="font-family:Verdana, Geneva, sans-serif;font-size:12px;font-weight:700;padding:7px;margin-bottom:8px;float:right;background-color:#2e475d;color:#fff;min-width:23px;text-align:center;border-radius:4px;">Option ` + (k + 1) + `</div>
                                <div style="font-family: Verdana, Geneva, sans-serif; font-size: 13px; color: #af0000; font-weight: bold; margin: 0px; text-align: right; float: right; padding: 5px; border: 1px solid #c1c0c0; border-radius: 3px; margin-right: 3px;">` + i18n.n(currentItem.fare.amount / vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency) + `</div>
                                <div style="font-family: Verdana, Geneva, sans-serif; font-size: 13px; color: #af0000; font-weight: bold; margin: 0px; text-align: right; float: right; padding: 5px; border: 1px solid #c1c0c0; border-radius: 3px; margin-right: 3px;">` + vm.tripTypeSelected(currentItem.tripType) + `</div>
                                </div>`;
            currentItem.groupOfFlights.forEach(function (fInfo) {
              summaryDetails += `<div style="width:100%;float:left;margin-bottom:10px;border: 1px solid #d4d4d4;padding: 2px;">`;
              fInfo.flightDetails.forEach(function (segment, segindex) {
                summaryDetails += `<div style="width:20%;float:left;">
                                        <div style="text-align:left;margin:0px;margin-bottom:5px;text-align:left;"><img src="`+ window.location.origin + `/Flights/FlightComponents/EmailTemplates/downloadvoucher/Airlines/` + segment.fInfo.companyId.mCarrier + `.gif" /></div>
                                        <div style="`+ styles + `font-weight:600;margin:0px;text-align:left;">` + vm.getAirLineName(segment.fInfo.companyId.mCarrier) + `</div>
                                        <div style="font-family:Verdana, Geneva, sans-serif;font-size:12px;color:#000;font-weight:normal;margin:0px;text-align:left;">` + segment.fInfo.companyId.mCarrier + `-` + segment.fInfo.flightNo + `</div>
                                    </div>
                                    <div style="width:20%;float:left;">
                                        <div style="`+ styles + `font-weight:600;margin:0px;text-align:right;">` + vm.momCommonFun(segment.fInfo.dateTime.depTime, 'HHmm', 'HH:mm') + `</div>
                                        <div style="`+ styles + `font-weight:normal;margin:0px;text-align:right;">` + vm.getCityNameUTCOnlyWithCityCode(segment.fInfo.location.locationFrom) + `</div>
                                        <div style="`+ styles + `font-weight:normal;margin:0px;text-align:right;">` + vm.momCommonFun(segment.fInfo.dateTime.depDate, 'DDMMYY', 'DD MMM YY') + `</div>
                                        <div style="`+ styles + `font-weight:normal;margin:0px;text-align:right;">` + (segment.fInfo.location.fromTerminal == null ? 'Terminal: N/A' : 'Terminal:' + segment.fInfo.location.fromTerminal) + `</div>
                                    </div>
                                    <div style="width:15%;float:left;">
                                        <div style="`+ styles + `font-weight:normal;margin:0px;padding-top:26px;text-align:center;">` + vm.getElapseTime(segment.fInfo.elapsedTime) + `</div>
                                    </div>
                                    <div style="width:20%;float:left;">
                                        <div style="`+ styles + `font-weight:600;margin:0px;text-align:left;">` + vm.momCommonFun(segment.fInfo.dateTime.arrTime, 'HHmm', 'HH:mm') + `</div>
                                        <div style="`+ styles + `font-weight:normal;margin:0px;text-align:left;">` + vm.getCityNameUTCOnlyWithCityCode(segment.fInfo.location.locationTo) + `</div>
                                        <div style="`+ styles + `font-weight:normal;margin:0px;text-align:left;">` + vm.momCommonFun(segment.fInfo.dateTime.arrDate, 'DDMMYY', 'DD MMM YY') + `</div>
                                        <div style="`+ styles + `font-weight:normal;margin:0px;text-align:left;">` + (segment.fInfo.location.toTerminal == null ? 'Terminal: N/A' : 'Terminal:' + segment.fInfo.location.toTerminal) + `</div>
                                    </div>
                                    <div style="width:25%;float:left;">
                                        <div style="`+ styles + `font-weight:normal;margin:0px;text-align:left;">Aircraft Type: ` + vm.getAirLineName(segment.fInfo.companyId.mCarrier) + `</div>
                                        <div style="`+ styles + `font-weight:normal;margin:0px;text-align:left;">Cabin: ` + getCabinClassObject(segment.fInfo.bookingClass).BasicClass + `</div>
                                        ` + (segment.fInfo.rbd ? `<div style="`+ styles + `font-weight:normal;margin:0px;text-align:left;">RBD: ` + segment.fInfo.rbd + '</div>' :'') + `
                                    </div>`
                if (fInfo.flightDetails.length > 1 && segindex != fInfo.flightDetails.length - 1) {
                  summaryDetails += `<div style="width:100%;float:left;font-family:Verdana, Geneva, sans-serif;font-size:13px;color:#2f38a6;line-height:18px;text-align:center;font-weight:bold;margin:10px 0;position:relative;">
                                        <div style="width:100%;height:1px;position:absolute;margin-top:9px;border-bottom:1px dashed #adacac;">&nbsp;</div>
                                        <div style="margin:auto;width:250px;">
                                            <div style="width:100%;float:left;background:#FFF;text-align:center;color:#464442;position:relative;">Layover at ` + segment.fInfo.location.locationTo + ' | ' + calcLayoverTime(segment.fInfo.dateTime.arrDate, segment.fInfo.dateTime.arrTime, fInfo.flightDetails[segindex + 1].fInfo.dateTime.depDate, fInfo.flightDetails[segindex + 1].fInfo.dateTime.depTime) + `</div>
                                        </div>
                                    </div>`
                }
              });
              summaryDetails += `</div>`
            });
            summaryDetails += `</div>`
          }

          var hotelDetails = "";
          var summaryTableHotel = "";
          var selectedQuotationsHotels = JSON.parse(JSON.stringify(selectedQuotations.services.hotels));

          for (var h = 0; h < selectedQuotationsHotels.length; h++) {
            var currentItem = selectedQuotationsHotels[h];
            var roomOptions = "<div>";
            var totalPrice = 0;
            for (var j = 0; j < currentItem.options.length; j++) {
              var roomDetails = "";

              var currentOptions = currentItem.options[j];
              roomDetails += "<b>Option " + (j + 1) + " | " + currentOptions.optionName + "</b>" + `
                                <table border="1" border-color="#00739c;page-break-inside: avoid;" cellpadding="10" cellspacing="0" style="width:100%; border-collapse:collapse;font-family:Verdana, Geneva, sans-serif;margin-bottom:15px;">
                                    <tbody>
                                        <tr style="page-break-inside: avoid;">
                                            <th style="text-align:center;font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #ffffff; font-weight: normal; background: #2c96d1;">Room Type</th>
                                            <th style="text-align:center;font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #ffffff;  font-weight: normal; background: #2c96d1;">Room Status</th>
                                            <th style="text-align:center;font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #ffffff;  font-weight: normal; background: #2c96d1;width:120px;">Check in / Check out</th>
                                            <th style="text-align:center;font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #ffffff; font-weight: normal; background: #2c96d1;">Cost</th>
                                            <th style="text-align:center;font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #ffffff;  font-weight: normal; background: #2c96d1;">Pax</th>
                                            <th style="text-align:center;font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #ffffff;  font-weight: normal; background: #2c96d1;">Meals Type</th>
                                            <th style="text-align:center;font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #ffffff;  font-weight: normal; background: #2c96d1;width:400px;">Cancellation Policy</th>
                                        </tr>`
              for (var k = 0; k < currentOptions.rooms.length; k++) {
                var cancellationPolicy = "";
                totalPrice += parseFloat(currentOptions.rooms[k].rates[0].pricing.totalPrice.price);
                var price = i18n.n(currentOptions.rooms[k].rates[0].pricing.totalPrice.price / vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency);
                var pricePerNight = i18n.n((currentOptions.rooms[k].rates[0].pricing.totalPrice.price / currentOptions.rooms[k].numOfNights) / vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency);
                var boards = currentOptions.rooms[k].rates[0].boards ? currentOptions.rooms[k].rates[0].boards[0].value : "Information not available";
                var children = currentOptions.rooms[k].guests.children ? currentOptions.rooms[k].guests.children : [];
                for (var c = 0; c < currentOptions.rooms[k].rates[0].cancellationPoliciesParsed.length; c++) {
                  cancellationPolicy += currentOptions.rooms[k].rates[0].cancellationPoliciesParsed[c].replace(/h4/, "p");
                }
                roomDetails += `<tr style="page-break-inside: avoid">
                                    <td style="font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #717171; padding: 6px; font-weight: normal;">`+ currentOptions.rooms[k].roomName + `</td>
                                    <td style="font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #717171; padding: 6px;  font-weight: normal;">`+ currentOptions.rooms[k].status + `</td>
                                    <td style="font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #717171; padding: 6px;  font-weight: normal;width:120px;">`+ currentOptions.rooms[k].inOutDate + " (" + currentOptions.rooms[k].numOfNights + (currentOptions.rooms[k].numOfNights == 1 ? ' Night' : ' Nights') + `)</td>
                                    <td style="font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #717171; padding: 6px;  font-weight: 500;">`+ price + (currentOptions.rooms[k].numOfNights == 1 ? "" : " (" + pricePerNight + "/Night)") + `</td>
                                    <td style="font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #717171;  padding: 6px; font-weight: normal;">Adults:`+ currentOptions.rooms[k].guests.adult + (children.length ? ", Child: " + children.length : "") + `</td>
                                    <td style="font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #717171;  padding: 6px; font-weight: normal;">`+ boards + `</td>
                                    <td style="font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #717171;  padding: 6px; font-weight: normal;width:400px;">`+ cancellationPolicy + `</td>
                                </tr>`;

              }
              roomOptions += roomDetails + '</tbody></table >';
            }
            roomOptions += '</div>';
            summaryTableHotel += `<tr id="` + (h + 1) + `">
                            <td style="text-align:center;">`+ (h + 1) + `</td>
                            <td style="text-align:center;">`+ currentItem.city + `</td>
                            <td style="text-align:center;">`+ currentItem.hotelName + `</td>
                            <td style="text-align:center;">`+ currentItem.starRating + `</td>
                            <td style="text-align:center;">`+ currentItem.options.length + `</td>
                            <td style="text-align:center;">` + i18n.n(totalPrice / vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency) + `</td>
                        </tr>`
            hotelDetails += `<div style="width:100%;background:#FFF;float:left;display:block;page-break-inside: auto;margin-bottom:15px;">
                            <div style="width:222px;float:left;margin-bottom: 9px;min-height:130px;"><img alt="hotel" height="226" src="`+ currentItem.imageUrl + `" style="max-height: 180px;max-width: 200px;width:100%; border: 1px solid #E8E8E8;border-radius:3px;page-break-inside: avoid;" width="226" /></div>
                            <div style="font-family: Verdana, Geneva, sans-serif; font-size: 17px; color: #565454; font-weight: 700;">`+ currentItem.hotelName + `</div>
                            <div style="font-family: Verdana, Geneva, sans-serif; font-size: 15px; color: #565454;margin-bottom: 4px;">Hotel rating:
                                <div style="color:#2c96d1;display: -webkit-inline-box;">`+ currentItem.starRating + ` Star</div>
                            </div>
                            <div style="font-family: Verdana, Geneva, sans-serif; font-size: 15px; color: #565454;">Address:
                                <div style="color:#2c96d1;display: -webkit-inline-box;">`+ currentItem.address + `</div>
                            </div>
                            <div style="width:100%;float:left;page-break-inside: inherit;">
                                <div style="font-family: Verdana, Geneva, sans-serif; font-size: 16px; color: #2c96d1; font-weight: 700;border-bottom:1px dashed #d2d2d2;padding: 4px 0px 4px 0px;margin-bottom:7px;">Description</div>
                                <div style="font-family: Verdana, Geneva, sans-serif; font-size: 13px; color: #000000; text-align: justify; line-height: 18px;padding: 5px 15px 5px 5px;">
                                    `+ currentItem.description + `
                                </div>
                            </div>
                            <div style="width:100%;float:left;padding-top:20px;">`+ roomOptions + `</div>
                        </div>`
          }

          var phone = vm.commonStore.agencyNode.loginNode.phoneList.filter((x) => {
            return x.type == "Telephone";
          })[0].number || "";
          
          var insuranceDetails = "";
          var selectedQuotationItemsInsurance = selectedQuotations.services.insuranceTP;
          selectedQuotationItemsInsurance.forEach(element => {
            insuranceDetails = insuranceDetails + '<div style=" width: 100%;float: left;border-radius: 3px;overflow: hidden;background: #FFF;box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.05);border: 1px solid #eaeaea;margin-bottom: 10px;"><div><div style="padding: 10px;"><h4 style="float:left">' +
              element.planTitle + '</h4><h4 style="float:right">' +
              vm.commonStore.agencyNode.loginNode.currency + ' ' +
              element.totalPremiumAmount + '</h4></div></div><div style="width: 100%;float: left;padding: 16px;font-size: 13px;"><div><span>' +
              element.planContent + '</span></div></div></div><br>';
          });

          var content =
            `<div style="width:100%;padding:12px 0px 0px 0px; float:left;">
                        <div style="width:100%; margin:0 auto;">
                            <div style="width:100%; float:left; background:#FFF;">
                                <div style="float: left;border:1px solid #d4d4d4;">
                                    <div style="float: left;border-bottom: 1px solid #d4d4d4;margin-bottom: 10px;width:100%">
                                        <div style="padding:1% 0;width:50%; float:left; text-align:left;"><img src="`+ vm.commonStore.agencyNode.loginNode.logo + `" style="margin:0 5%;" /></div>
                                        <div style="padding:1% 0;width:50%; float:left; font-family:Verdana, Geneva, sans-serif; font-size:13px; color:#454e68; font-weight:700; text-align: right;">
                                            <div class="logo-right-column" style="float: right; width: 50%;box-sizing: border-box; padding: 10px; position: relative;">
                                                <p style="text-align: right;color: #454E68;">`+ vm.commonStore.agencyNode.loginNode.name + `</p>
                                                <p style="text-align: right;color: #454E68;">+`+ vm.commonStore.agencyNode.loginNode.country.telephonecode + phone + `</p>
                                                <p style="text-align: right;color: #454E68;">`+ vm.commonStore.agencyNode.emailId + `</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="padding: 1%;float: left;`+ (summaryTable == "" ? 'display:none;' : '') + `">
                                        <div style="text-align:center;font-family: Verdana, Geneva, sans-serif; font-size: 20px; color: #454e68; font-weight: bold; margin-bottom: 10px; border-bottom: 1px solid #ebebeb; padding-bottom: 5px;">`+ selectedQuotations.name + `</div>
                                        <div style="font-family: Verdana, Geneva, sans-serif; font-size: 19px; color: #454e68; font-weight: bold;  margin-top: 5px; margin-bottom: 5px; border-bottom: 1px solid #ebebeb; padding-bottom: 5px;">Flights</div>
                                        <div style="font-family: Verdana, Geneva, sans-serif; font-size: 15px; color: #454e68; font-weight: bold; margin-bottom: 10px; border-bottom: 1px solid #ebebeb; padding-bottom: 5px;">Quote Summary</div>
                                        <table border="1" bordercolor="#00739c" cellpadding="0" cellspacing="0" style="margin-bottom:10px;border-collapse:collapse;width:100%;font-family:Verdana,Geneva,sans-serif;">
                                            <thead>
                                                <tr>
                                                    <th style="text-align:center;">Options</th>
                                                    <th style="text-align:center;">Departure</th>
                                                    <th style="text-align:center;">Arrival</th>
                                                    <th style="text-align:center;">Adults</th>
                                                    <th style="text-align:center;">Children</th>
                                                    <th style="text-align:center;">Infants</th>
                                                    <th style="text-align:center;">Airline</th>
                                                    <th style="text-align:center;">Fare</th>
                                                    <th style="text-align:center;">Stops</th>
                                                    <th style="text-align:center;">Type</th>
                                                </tr>
                                            </thead>
                                            <tbody>` + summaryTable + `</tbody>
                                        </table>
                                        <div style="font-family: Verdana, Geneva, sans-serif; font-size: 15px; color: #454e68; font-weight: bold; margin-bottom: 10px; border-bottom: 1px solid #ebebeb; padding-bottom: 5px;">Quote Details</div>
                                        <div class="flightdetails5" style = "float: left;border: 1px solid #d4d4d4;padding: 1%;margin-bottom: 10px;">` + summaryDetails + `</div>
                                    </div>
                                    <div style="padding: 1%;float: left;`+ (hotelDetails == "" ? 'display:none;' : '') + `">
                                        <div style="font-family: Verdana, Geneva, sans-serif; font-size: 19px; color: #454e68; font-weight: bold;  margin-top: 5px; margin-bottom: 5px; border-bottom: 1px solid #ebebeb; padding-bottom: 5px;">Hotels</div>
                                        <div style="font-family: Verdana, Geneva, sans-serif; font-size: 15px; color: #454e68; font-weight: bold; margin-bottom: 10px; border-bottom: 1px solid #ebebeb; padding-bottom: 5px;">Quote Summary</div>
                                        <table border="1" bordercolor="#00739c" cellpadding="0" cellspacing="0" style="`+ (summaryTableHotel == "" ? 'display:none;' : '') + ` margin-bottom:10px;border-collapse:collapse;width:100%;font-family:Verdana,Geneva,sans-serif;">
                                            <thead>
                                                <tr>
                                                    <th style="text-align:center;">No.</th>
                                                    <th style="text-align:center;">City</th>
                                                    <th style="text-align:center;">Hotel Name</th>
                                                    <th style="text-align:center;">Star Rating</th>
                                                    <th style="text-align:center;">No. of Options</th>
                                                    <th style="text-align:center;">Price</th>
                                                </tr>
                                            </thead>
                                            <tbody>` + summaryTableHotel + `</tbody>
                                        </table>
                                        <div style="font-family: Verdana, Geneva, sans-serif; font-size: 15px; color: #454e68; font-weight: bold; margin-bottom: 10px; border-bottom: 1px solid #ebebeb; padding-bottom: 5px;">Quote Details</div>
                                        <div style="width:100%; margin: auto;">` + hotelDetails + `</div>
                                    </div>
                                    <div style="width:100%;padding: 1%;float: left;`+ (insuranceDetails == "" ? 'display:none;' : '') + `">
                                        <div style="font-family: Verdana, Geneva, sans-serif; font-size: 19px; color: #454e68; font-weight: bold;  margin-top: 5px; margin-bottom: 5px; border-bottom: 1px solid #ebebeb; padding-bottom: 5px;">Insurance</div>
                                        <div style="font-family: Verdana, Geneva, sans-serif; font-size: 15px; color: #454e68; font-weight: bold; margin-bottom: 10px; border-bottom: 1px solid #ebebeb; padding-bottom: 5px;">Quote Details</div>
                                       
                                        ` + insuranceDetails + `
                                    </div>
                                    <div style="padding: 1%;width:100%;border:1px solid #d4d4d4;float: left;` + ((hotelDetails != "" || summaryTable != "" || insuranceDetails != "") ? '' : 'display:none;') + `">
                                        <h2 style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:13px; color:#454e68; font-weight:normal;margin:0px; margin-bottom:15px;line-height: 17px;"><strong style="font-size:14px;">Please find above Terms and Conditions</strong></h2>
                                        <h2 style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:13px; color:#525659; font-weight:normal;margin:0px; margin-bottom:15px;line-height: 17px;">1.The above is a quotation only and any reservations made are strictly subject to change and availability from the supplier/s.</h2>
                                        <h2 style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:13px; color:#525659; font-weight:normal;margin:0px; margin-bottom:15px;line-height: 17px;">2.Any taxes are subject to change without prior notice and the price of your itinerary is guaranteed only at the time of purchase.</h2>
                                        <h2 style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:13px; color:#525659; font-weight:normal;margin:0px; margin-bottom:15px;line-height: 17px;">3.Applicable fare rules and penalties from the supplier/s will apply along with agency service fees once the ticket has been issued. </h2>
                                        <h2 style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:13px; color:#525659; font-weight:normal;margin:0px; margin-bottom:15px;line-height: 17px;">4.It shall solely be the responsibility of the buyer to understand all applicable fare rules/penalties at the time of purchase.</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>`
          $('#summernote').summernote('code', content);
          $('#summernote').summernote('disable');
        });

      }, 10);

    },
    getdatetimeformated: function (datetime, inputFormat, outputFormat) {
      return moment(datetime, inputFormat).format(outputFormat);
    },
    gettimeformated: function (time, inputFormat, houroutputFormat, houraddl, minoutputFormat, minaddl) {
      var hour = moment(time, inputFormat).format(houroutputFormat).toString();
      var min = moment(time, inputFormat).format(minoutputFormat).toString();
      var formattedtime = hour + houraddl + min + minaddl;
      return formattedtime;
    },
    airportLocationFromAirportCode: function (airlinecode) {
      return airportLocationFromAirportCode(airlinecode);
    },
    getElapseTime: function (elapStr) {
      if (elapStr) {
        return elapStr.substring(0, 2) + 'h ' + elapStr.substring(2, 4) + 'm ';
      }
      return '';
    },
    momCommonFun: function (date, frmFormat, toFormat) {
      return moment(date, frmFormat).format(toFormat);
    },
    getAirLineName: function (airlinecode) {
      return getAirLineName(airlinecode);
    },
    getAirCraftName: function (aireqpcode) {
      return getAirCraftName(aireqpcode);
    },
    airportFromAirportCode: function (airport) {
      return airportFromAirportCode(airport);
    },
    getCityNameUTCOnlyWithCityCode: function (airportcode) {
      var CityName = _.where(AirlinesTimezone, { I: airportcode });
      if (CityName == "") {
        CityName = airportcode;
      }
      else {
        CityName = CityName[0].C;
      }
      return CityName + ' (' + airportcode + ')';
    },
    tripTypeSelected: function (id) {
      return (id == "o") ? 'One Way' : (id == "r") ? 'Roundtrip' : (id == "m") ? 'Multicity' : '';
    },
    fieldValidation: function(){
      if (this.quoteEmailTo == "" || this.quoteEmailSubject == "") {
        alertify.alert("Warning", "Please enter required fields.");
      }else {
        this.emailQuotation();
      }
    },
    emailQuotation: function () {
      var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;
      var serviceUrl = this.commonStore.hubUrls.hubConnection.hubServices.sendQuote;
      var vm = this;

      var logoUrl = '';
        var fromEmail = [{ emailId: "" }];

      try {
        logoUrl = JSON.parse(atob(window.localStorage.getItem("agencyNode"))).loginNode.logo;
        // fromEmail = JSON.parse(atob(window.localStorage.getItem("agencyNode"))).loginNode.parentEmailId;
        fromEmail = _.filter(JSON.parse(atob(window.localStorage.getItem("agencyNode"))).loginNode.emailList,
          function (o) { return o.emailTypeId == 3 && o.emailType.toLowerCase() == "support"; });
      } catch (err) { }

      var data = {
        quoteId: vm.quoteDetails.id,
        mailFrom: fromEmail[0].emailId || vm.commonStore.fallBackEmail,
        mailTo: vm.quoteEmailTo,
        mailCC: vm.quoteEmailCC,
        subject: vm.quoteEmailSubject,
        body: vm.quoteDetails.body,//$('#summernote').summernote('code'),
        sendDate: moment(new Date()).format('YYYY-MM-DDThh:mm:ss'),
        content: JSON.stringify(vm.quoteDetails.content)
      };
      var dataEmail = {
        type: "QuotationRequest",
        fromEmail: fromEmail[0].emailId || vm.commonStore.fallBackEmail,
        toEmails: Array.isArray(vm.quoteEmailTo) ? vm.quoteEmailTo : [vm.quoteEmailTo],
        ccEmails: Array.isArray(vm.quoteEmailCC) ? vm.quoteEmailCC : vm.quoteEmailCC ? [vm.quoteEmailCC] : null,
        bccEmails: null,
        primaryColor: null,
        secondaryColor: null,
        logoUrl: "",
        subject: vm.quoteEmailSubject,
        quoteSummary: $('#summernote').summernote('code'),
        quoteDetails: "",
        year: new Date().getFullYear(),
        websiteUrl: vm.commonStore.agencyNode.loginNode.url || window.location.origin,
        agencyName: vm.commonStore.agencyNode.loginNode.name || "",
        agencyPhone: "+" + vm.commonStore.agencyNode.loginNode.country.telephonecode + vm.commonStore.agencyNode.loginNode.phoneList.filter((x) => {
          return x.type == "Telephone";
        })[0].number || "",
        agencyMail: fromEmail[0].emailId || ""
      }

      var mailUrl = vm.commonStore.hubUrls.emailServices.emailApi;
      sendMailService(mailUrl, dataEmail);
      var config = {
        axiosConfig: {
          method: "post",
          url: hubUrl + serviceUrl,
          data: data
        },
        successCallback: function (response) {
          vm.quoteEmailTo = "";
          vm.quoteEmailCC = "";
          vm.quoteEmailSubject = "";
          alertify.alert("Suucess", "Quotation sent to your email.");
        },
        errorCallback: function (error) {
        },
        showAlert: false
      };

      mainAxiosRequest(config);
    }
  }
});