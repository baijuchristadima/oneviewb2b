Vue.component('flight-autocomplete', {
  inject: ['$validator'],
  data() {
    return {
      KeywordSearch: "",
      resultItemsarr: [],
      autoCompleteProgress: false,
      loading: false,
      highlightIndex: 0
    }
  },
  props: {
    airportValue: String,
    itemIndex: Number,
    itemText: String,
    itemId: String,
    placeHolderText: String,
    id: {
      type: String,
      default: '',
      required: false
    },
    disabledStatus: {
      type: Boolean,
      default: false,
      required: false
    },

  }, watch: {
    airportValue: function () {
      this.KeywordSearch = this.airportValue ? this.airportValue : "";
    },
    KeywordSearch: function () {
      if (this.KeywordSearch != this.airportValue) {
        this.$emit('air-search-completed', undefined, undefined, this.itemIndex, this.itemText);

      }
    }
  },
  mounted: function () {
    this.KeywordSearch = this.airportValue ? this.airportValue : "";
    window.sessionStorage.setItem("offlineReqTab", "flights");
  },
  template: `<div class="autocomplete flightauto"  v-click-outside="hideautocompelte">
      <input :name="itemId+itemIndex" data-vv-as=" " v-validate="'required'"
      type="text" :placeholder="placeHolderText" :disabled="disabledStatus" :id="id" :data-aircode="itemId" autocomplete="off" v-model="KeywordSearch" class="from_area_txt from_area_txt5" :class="{ 'loading-circle' : (KeywordSearch && KeywordSearch.length > 2), 'hide-loading-circle': resultItemsarr.length > 0 || resultItemsarr.length == 0 && !loading  }" @keyup="onSelectedAutoCompleteEvent(KeywordSearch,$event)"  @keydown.down="down"
      @keydown.up="up"      
      @keydown.esc="autoCompleteProgress=false"
      @keydown.enter="onSelected(resultItemsarr[highlightIndex].code,resultItemsarr[highlightIndex].label)"
      @click="autocomplete"
      @keydown.tab="tabclick(resultItemsarr[highlightIndex])"/>
      <ul ref="searchautocomplete" class="autocomplete-results" v-if="autoCompleteProgress&&resultItemsarr.length > 0">
          <li ref="options" :class="{'autocomplete-result-active' : i == highlightIndex}" class="autocomplete-result" v-for="(item,i) in resultItemsarr" :key="i" @click="onSelected(item.code, item.label)">
              {{ item.label }}
          </li>
      </ul>
      <p class="text-danger">{{ errors.first(itemId+itemIndex) }}</p>
  </div>`,
  methods: {
    up: function () {
      if (this.autoCompleteProgress) {
        if (this.highlightIndex > 0) {
          this.highlightIndex--
        }
      } else {
        this.autoCompleteProgress = true;
      }
      this.fixScrolling();
    },

    down: function () {
      if (this.autoCompleteProgress) {
        if (this.highlightIndex < this.resultItemsarr.length - 1) {
          this.highlightIndex++
        } else if (this.highlightIndex == this.resultItemsarr.length - 1) {
          this.highlightIndex = 0;
        }
      } else {
        this.autoCompleteProgress = true;
      }
      this.fixScrolling();
    },
    fixScrolling: function () {
      if (this.$refs.options[this.highlightIndex]) {
        var liH = this.$refs.options[this.highlightIndex].clientHeight;
        if (liH == 50) {
          liH = 32;
        }
        if (liH * this.highlightIndex >= 32 || this.highlightIndex == 0) {
          this.$refs.searchautocomplete.scrollTop = liH * this.highlightIndex;
        }
      }

    },
    onSelectedAutoCompleteEvent: _.debounce(function (keywordEntered, event) {
      var self = this;
      if (event.code != 'ArrowDown' && event.code != 'ArrowUp') {
        this.highlightIndex = 0;
      }
      if (keywordEntered.length > 2) {
        this.autoCompleteProgress = true;
        this.loading = true;
        var cityName = keywordEntered;
        var cityarray = cityName.split(' ');
        var uppercaseFirstLetter = "";
        for (var k = 0; k < cityarray.length; k++) {
          uppercaseFirstLetter += cityarray[k].charAt(0).toUpperCase() + cityarray[k].slice(1).toLowerCase()
          if (k < cityarray.length - 1) {
            uppercaseFirstLetter += " ";
          }
        }
        uppercaseFirstLetter = "*" + uppercaseFirstLetter + "*";
        var lowercaseLetter = "*" + cityName.toLowerCase() + "*";
        var uppercaseLetter = "*" + cityName.toUpperCase() + "*";
        var query = {
          query: {
            bool: {
              should: [{
                bool: {
                  should: [{
                    wildcard: {
                      iata_code: { value: uppercaseFirstLetter, boost: 3.0 }
                    }
                  }, {
                    wildcard: {
                      iata_code: { value: uppercaseLetter, boost: 3.0 }
                    }
                  }, {
                    wildcard: {
                      iata_code: { value: lowercaseLetter, boost: 3.0 }
                    }
                  }]
                }
              }, {
                bool: {
                  should: [{
                    wildcard: {
                      municipality: uppercaseFirstLetter
                    }
                  }, {
                    wildcard: {
                      municipality: uppercaseLetter
                    }
                  }, {
                    wildcard: {
                      municipality: lowercaseLetter
                    }
                  }]
                }
              }, {
                bool: {
                  should: [{
                    wildcard: {
                      name: uppercaseFirstLetter
                    }
                  }, {
                    wildcard: {
                      name: uppercaseLetter
                    }
                  }, {
                    wildcard: {
                      name: lowercaseLetter
                    }
                  }]
                }
              }],
              must: [{
                "exists": {
                  "field": "iata_code"
                }
              }]
            }
          }
        };
        var config = {
          axiosConfig: {
              headers: {
                  "Content-Type": "application/json"
              },
              method: "post",
              url: HubServiceUrls.elasticSearch.url,
              data: query
          },
          successCallback: function (resp) {
            finalResult = [];
            var hits = resp.data.hits.hits;
            var Citymap = new Map();
            for (var i = 0; i < hits.length; i++) {
              Citymap.set(hits[i]._source.iata_code, hits[i]._source);
            }
            var get_values = Citymap.values();
            var Cityvalues = [];
            for (var ele of get_values) {
              Cityvalues.push(ele);
            }
            var results = SortInputFirstFlight(cityName, Cityvalues);
            for (var i = 0; i < results.length; i++) {
              finalResult.push({
                "code": results[i].iata_code,
                "label": results[i].name + ", " + results[i].iso_country + '(' + results[i].iata_code + ')',
                "municipality": results[i].municipality
              });
            }
            var newData = [];
            finalResult.forEach(function (item, index) {
              if (item.label.toLowerCase().indexOf(keywordEntered.toLowerCase()) >= 0 || item.code.toLowerCase().includes(keywordEntered.toLowerCase()) || item.municipality.toLowerCase().indexOf(keywordEntered.toLowerCase()) >= 0) {
                newData.push(item);
              }
            });
            self.resultItemsarr = newData;
            self.autoCompleteProgress = true;
            self.loading = false;
          },
          errorCallback: function (error) { },
          showAlert: false
      };
      mainAxiosRequest(config);

        // var client = new elasticsearch.Client({
        //   host: [{
        //     host: HubServiceUrls.elasticSearch.elasticsearchHost,
        //     auth: HubServiceUrls.elasticSearch.auth,
        //     protocol: HubServiceUrls.elasticSearch.protocol,
        //     port: HubServiceUrls.elasticSearch.port,
        //     requestTimeout: 60000
        //   }],
        //   log: 'trace'
        // });
        // client.search({
        //   index: 'airport_info',
        //   size: 150,
        //   timeout: "3000ms",
        //   body: query
        // }).then(function (resp) {
          
        // })
      } else {
        this.autoCompleteProgress = false;
        this.resultItemsarr = [];
        this.loading = false;
      }
    }, 500),
    onSelected: function (code, label, type) {
      this.autoCompleteProgress = true;
      this.resultItemsarr = [];
      this.KeywordSearch = label;
      this.$emit('air-search-completed', code, label, this.itemIndex, this.itemText);
      var tempid = this.id;
      if (tempid.indexOf('txtFrom') != -1) {
        var newid = tempid.replace("From", "To");
        if (type == undefined) {
          $("#" + newid).focus();
        }
      }
      else {
        if (type == undefined) {
          $("#" + tempid).parent().closest('td').next().children('input').focus();
        }

      }

    },
    hideautocompelte: function () {
      this.autoCompleteProgress = false;
    },
    autocomplete: function () {
      this.autoCompleteProgress = true;
    },
    tabclick: function (item) {
      if (!item) {

      }
      else {
        this.onSelected(item.code, item.label, 'Tab');
      }
    }

  }

});

Vue.component("offline-request", {
  data: function () {
    return {
      commonStore: vueCommonStore.state,
      countries: [
        { code: "AF", name: "Afghanistan" },
        { code: "AL", name: "Albania" },
        { code: "DZ", name: "Algeria" },
        { code: "AS", name: "American Samoa" },
        { code: "AD", name: "Andorra" },
        { code: "AO", name: "Angola" },
        { code: "AI", name: "Anguilla" },
        { code: "AQ", name: "Antartica" },
        { code: "AG", name: "Antigua And Barbuda" },
        { code: "AR", name: "Argentina" },
        { code: "AM", name: "Armenia" },
        { code: "AW", name: "Aruba" },
        { code: "AU", name: "Australia" },
        { code: "AT", name: "Austria" },
        { code: "AZ", name: "Azerbaijan" },
        { code: "BS", name: "Bahamas" },
        { code: "BH", name: "Bahrain" },
        { code: "BD", name: "Bangladesh" },
        { code: "BB", name: "Barbados" },
        { code: "BY", name: "Belarus" },
        { code: "BE", name: "Belgium" },
        { code: "BZ", name: "Belize" },
        { code: "BJ", name: "Benin" },
        { code: "BM", name: "Bermuda" },
        { code: "BT", name: "Bhutan" },
        { code: "BO", name: "Bolivia" },
        { code: "BQ", name: "Bonaire St Eustatius And Saba " },
        { code: "BA", name: "Bosnia-Herzegovina" },
        { code: "BW", name: "Botswana" },
        { code: "BR", name: "Brazil" },
        { code: "IO", name: "British Indian Ocean Territory" },
        { code: "BN", name: "Brunei Darussalam" },
        { code: "BG", name: "Bulgaria" },
        { code: "BF", name: "Burkina Faso" },
        { code: "BI", name: "Burundi" },
        { code: "KH", name: "Cambodia" },
        { code: "CM", name: "Cameroon-Republic Of" },
        { code: "CB", name: "Canada Buffer" },
        { code: "CA", name: "Canada" },
        { code: "CV", name: "Cape Verde-Republic Of" },
        { code: "KY", name: "Cayman Islands" },
        { code: "CF", name: "Central African Republic" },
        { code: "TD", name: "Chad" },
        { code: "CL", name: "Chile" },
        { code: "CN", name: "China" },
        { code: "CX", name: "Christmas Island" },
        { code: "CC", name: "Cocos Islands" },
        { code: "CO", name: "Colombia" },
        { code: "KM", name: "Comoros" },
        { code: "CG", name: "Congo Brazzaville" },
        { code: "CD", name: "Congo The Democratic Rep Of" },
        { code: "CK", name: "Cook Islands" },
        { code: "CR", name: "Costa Rica" },
        { code: "CI", name: "Cote D Ivoire" },
        { code: "HR", name: "Croatia" },
        { code: "CU", name: "Cuba" },
        { code: "CW", name: "Curacao" },
        { code: "CY", name: "Cyprus" },
        { code: "CZ", name: "Czech Republic" },
        { code: "DK", name: "Denmark" },
        { code: "DJ", name: "Djibouti" },
        { code: "DM", name: "Dominica" },
        { code: "DO", name: "Dominican Republic" },
        { code: "TP", name: "East Timor Former Code)" },
        { code: "EC", name: "Ecuador" },
        { code: "EG", name: "Egypt" },
        { code: "SV", name: "El Salvador" },
        { code: "EU", name: "Emu European Monetary Union" },
        { code: "GQ", name: "Equatorial Guinea" },
        { code: "ER", name: "Eritrea" },
        { code: "EE", name: "Estonia" },
        { code: "ET", name: "Ethiopia" },
        { code: "FK", name: "Falkland Islands" },
        { code: "FO", name: "Faroe Islands" },
        { code: "ZZ", name: "Fictitious Points" },
        { code: "FJ", name: "Fiji" },
        { code: "FI", name: "Finland" },
        { code: "FR", name: "France" },
        { code: "GF", name: "French Guiana" },
        { code: "PF", name: "French Polynesia" },
        { code: "GA", name: "Gabon" },
        { code: "GM", name: "Gambia" },
        { code: "GE", name: "Georgia" },
        { code: "DE", name: "Germany" },
        { code: "GH", name: "Ghana" },
        { code: "GI", name: "Gibraltar" },
        { code: "GR", name: "Greece" },
        { code: "GL", name: "Greenland" },
        { code: "GD", name: "Grenada" },
        { code: "GP", name: "Guadeloupe" },
        { code: "GU", name: "Guam" },
        { code: "GT", name: "Guatemala" },
        { code: "GW", name: "Guinea Bissau" },
        { code: "GN", name: "Guinea" },
        { code: "GY", name: "Guyana" },
        { code: "HT", name: "Haiti" },
        { code: "HN", name: "Honduras" },
        { code: "HK", name: "Hong Kong" },
        { code: "HU", name: "Hungary" },
        { code: "IS", name: "Iceland" },
        { code: "IN", name: "India" },
        { code: "ID", name: "Indonesia" },
        { code: "IR", name: "Iran" },
        { code: "IQ", name: "Iraq" },
        { code: "IE", name: "Ireland-Republic Of" },
        { code: "IL", name: "Israel" },
        { code: "IT", name: "Italy" },
        { code: "JM", name: "Jamaica" },
        { code: "JP", name: "Japan" },
        { code: "JO", name: "Jordan" },
        { code: "KZ", name: "Kazakhstan" },
        { code: "KE", name: "Kenya" },
        { code: "KI", name: "Kiribati" },
        { code: "KP", name: "Korea Dem Peoples Rep Of" },
        { code: "KR", name: "Korea Republic Of" },
        { code: "KW", name: "Kuwait" },
        { code: "KG", name: "Kyrgyzstan" },
        { code: "LA", name: "Lao Peoples Dem Republic" },
        { code: "LV", name: "Latvia" },
        { code: "LB", name: "Lebanon" },
        { code: "LS", name: "Lesotho" },
        { code: "LR", name: "Liberia" },
        { code: "LY", name: "Libya" },
        { code: "LI", name: "Liechtenstein" },
        { code: "LT", name: "Lithuania" },
        { code: "LU", name: "Luxembourg" },
        { code: "MO", name: "Macao -Sar Of China-" },
        { code: "MK", name: "Macedonia -Fyrom-" },
        { code: "MG", name: "Madagascar" },
        { code: "MW", name: "Malawi" },
        { code: "MY", name: "Malaysia" },
        { code: "MV", name: "Maldives Island" },
        { code: "ML", name: "Mali" },
        { code: "MT", name: "Malta" },
        { code: "MH", name: "Marshall Islands" },
        { code: "MQ", name: "Martinique" },
        { code: "MR", name: "Mauritania" },
        { code: "MU", name: "Mauritius Island" },
        { code: "YT", name: "Mayotte" },
        { code: "MB", name: "Mexico Buffer" },
        { code: "MX", name: "Mexico" },
        { code: "FM", name: "Micronesia" },
        { code: "MD", name: "Moldova" },
        { code: "MC", name: "Monaco" },
        { code: "MN", name: "Mongolia" },
        { code: "ME", name: "Montenegro" },
        { code: "MS", name: "Montserrat" },
        { code: "MA", name: "Morocco" },
        { code: "MZ", name: "Mozambique" },
        { code: "MM", name: "Myanmar" },
        { code: "NA", name: "Namibia" },
        { code: "NR", name: "Nauru" },
        { code: "NP", name: "Nepal" },
        { code: "AN", name: "Netherlands Antilles" },
        { code: "NL", name: "Netherlands" },
        { code: "NC", name: "New Caledonia" },
        { code: "NZ", name: "New Zealand" },
        { code: "NI", name: "Nicaragua" },
        { code: "NE", name: "Niger" },
        { code: "NG", name: "Nigeria" },
        { code: "NU", name: "Niue" },
        { code: "NF", name: "Norfolk Island" },
        { code: "MP", name: "Northern Mariana Islands" },
        { code: "NO", name: "Norway" },
        { code: "OM", name: "Oman" },
        { code: "PK", name: "Pakistan" },
        { code: "PW", name: "Palau Islands" },
        { code: "PS", name: "Palestine - State Of" },
        { code: "PA", name: "Panama" },
        { code: "PG", name: "Papua New Guinea" },
        { code: "PY", name: "Paraguay" },
        { code: "PE", name: "Peru" },
        { code: "PH", name: "Philippines" },
        { code: "PL", name: "Poland" },
        { code: "PT", name: "Portugal" },
        { code: "PR", name: "Puerto Rico" },
        { code: "QA", name: "Qatar" },
        { code: "RE", name: "Reunion Island" },
        { code: "RO", name: "Romania" },
        { code: "RU", name: "Russia" },
        { code: "XU", name: "Russia" },
        { code: "RW", name: "Rwanda" },
        { code: "WS", name: "Samoa-Independent State Of" },
        { code: "SM", name: "San Marino" },
        { code: "ST", name: "Sao Tome And Principe Islands " },
        { code: "SA", name: "Saudi Arabia" },
        { code: "SN", name: "Senegal" },
        { code: "RS", name: "Serbia" },
        { code: "SC", name: "Seychelles Islands" },
        { code: "SL", name: "Sierra Leone" },
        { code: "SG", name: "Singapore" },
        { code: "SX", name: "Sint Maarten" },
        { code: "SK", name: "Slovakia" },
        { code: "SI", name: "Slovenia" },
        { code: "SB", name: "Solomon Islands" },
        { code: "SO", name: "Somalia" },
        { code: "ZA", name: "South Africa" },
        { code: "SS", name: "South Sudan" },
        { code: "ES", name: "Spain" },
        { code: "LK", name: "Sri Lanka" },
        { code: "SH", name: "St. Helena Island" },
        { code: "KN", name: "St. Kitts" },
        { code: "LC", name: "St. Lucia" },
        { code: "PM", name: "St. Pierre And Miquelon" },
        { code: "VC", name: "St. Vincent" },
        { code: "SD", name: "Sudan" },
        { code: "SR", name: "Suriname" },
        { code: "SZ", name: "Swaziland" },
        { code: "SE", name: "Sweden" },
        { code: "CH", name: "Switzerland" },
        { code: "SY", name: "Syrian Arab Republic" },
        { code: "TW", name: "Taiwan" },
        { code: "TJ", name: "Tajikistan" },
        { code: "TZ", name: "Tanzania-United Republic" },
        { code: "TH", name: "Thailand" },
        { code: "TL", name: "Timor Leste" },
        { code: "TG", name: "Togo" },
        { code: "TK", name: "Tokelau" },
        { code: "TO", name: "Tonga" },
        { code: "TT", name: "Trinidad And Tobago" },
        { code: "TN", name: "Tunisia" },
        { code: "TR", name: "Turkey" },
        { code: "TM", name: "Turkmenistan" },
        { code: "TC", name: "Turks And Caicos Islands" },
        { code: "TV", name: "Tuvalu" },
        { code: "UM", name: "U.S. Minor Outlying Islands" },
        { code: "UG", name: "Uganda" },
        { code: "UA", name: "Ukraine" },
        { code: "AE", name: "United Arab Emirates" },
        { code: "GB", name: "United Kingdom" },
        { code: "US", name: "United States Of America" },
        { code: "UY", name: "Uruguay" },
        { code: "UZ", name: "Uzbekistan" },
        { code: "VU", name: "Vanuatu" },
        { code: "VA", name: "Vatican" },
        { code: "VE", name: "Venezuela" },
        { code: "VN", name: "Vietnam" },
        { code: "VG", name: "Virgin Islands-British" },
        { code: "VI", name: "Virgin Islands-United States" },
        { code: "WF", name: "Wallis And Futuna Islands" },
        { code: "EH", name: "Western Sahara" },
        { code: "YE", name: "Yemen Republic" },
        { code: "ZM", name: "Zambia" },
        { code: "ZW", name: "Zimbabwe" },
      ],
      airlineautoCompleteProgress: false,
      pnrCode: undefined,
      airlineserch: undefined,
      airlineserchCode: undefined,
      Airlineresults: [],
      airlineloading: false,
      airlineList: AirlinesDatas,
      supplierList: [],
      selectedSuppliers: "ANY",
      tripDetails: [],
      passengerDetails: [],
      journeyType: "1",
      isForUpdate: false,
      isApplied: false,
      totalAmount: 0,
      dataResponse: {},
      isUpdated: false,
      isReadOnly: false,
      isCompleted: false,
      isDisable: false,
      requestAttachments: [],
      remark: null,
      remarks: null,
      highlightIndex: 0,
      roundTripFlag: false,
      selectedSrc: {},
      temptripDetails: [],
      isRequestSending: false
    };
  },
  created: function () {
    var agencyNode = window.localStorage.getItem("agencyNode");
    if (agencyNode) {
      agencyNode = JSON.parse(atob(agencyNode));
      var servicesList = agencyNode.loginNode.servicesList;
      for (var i = 0; i < servicesList.length; i++) {
        var provider = servicesList[i].provider;
        if (servicesList[i].name == "Air" && provider.length > 0) {
          provider = provider.sort(function (a, b) {
            if (a.name.toLowerCase() < b.name.toLowerCase()) { return -1; }
            if (a.name.toLowerCase() > b.name.toLowerCase()) { return 1; }
            return 0;
          });
          for (var j = 0; j < provider.length; j++) {
            this.supplierList.push({ id: provider[j].id, name: provider[j].name.toUpperCase(), supplierType: provider[j].supplierType });
          }
          // this.selectedSuppliers = this.supplierList[0].id;
          break;
        } else if (servicesList[i].name == "Air" && provider.length == 0) {
          setLoginPage("login");
        }
      }
    }

    var vm = this;

    var offlineBookingId = window.sessionStorage.getItem("offlineBookingId");
    if (offlineBookingId) {
      this.isForUpdate = true;
      var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrlOffline;
      var port = vm.commonStore.hubUrls.hubConnection.ipAddress;
      var offlineRequest = vm.commonStore.hubUrls.hubConnection.hubServices.flights.offlineRequest;

      axios({
        method: "GET",
        url: hubUrl + port + offlineRequest + "/" + offlineBookingId,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
        }
      }).then((response) => {

        console.log("RESPONSE RECEIVED: ", response);
        this.dataResponse = response.data;
        if (response.data.data.id) {
          vm.tripDetails = response.data.data.requestAirBook.leg.map(function (e) {
            var cabinClassCode = "Y";
            if (e.cabinClassCode != undefined) {
              cabinClassCode = e.cabinClassCode == "M" ? "Y" : (["Y", "C", "F"].indexOf(e.cabinClassCode) != -1 ? e.cabinClassCode : "Y");
            }
            return {
              id: e.id,
              airportCodeSrc: e.airportCodeSrc || { code: undefined, name: undefined },
              airportCodeDst: e.airportCodeDst || { code: undefined, name: undefined },
              departureDate: moment(e.departureDate, "YYYY-MM-DD").format('DD MMM YY, ddd') || undefined,
              cabinClassCode: cabinClassCode,
              rbd: (e.rbd || "").toUpperCase() || "Y",
              flightNum: e.flightNum || undefined,
            }
          });

          vm.passengerDetails = response.data.data.requestAirBook.requestAirBookPax.map(function (e) {
            return {
              pax: {
                id: e.pax.id,
                firstName: e.pax.firstName || undefined,
                surName: e.pax.surName || undefined,
                age: e.pax.age || undefined,
                gender: e.pax.gender || undefined,
                nationality: e.pax.nationality || { code: undefined },
                residence: e.pax.residence || { code: undefined },
                lead: e.pax.lead || false,
                paxType: e.pax.paxType || { code: undefined, name: undefined },
                title: e.pax.title || { id: undefined },
                flightPax: {
                  passportNumber: e.pax.flightPax ? e.pax.flightPax.passportNumber : undefined,
                  dob: e.pax.flightPax && e.pax.flightPax.dob ? moment(e.pax.flightPax.dob, "YYYY-MM-DD").format('DD MMM YY, ddd') : undefined
                }
              },
              prm_paxid: e.prm_paxid,
              requestAirBookRequestId: e.requestAirBookRequestId

            }
          });
          vm.airlineserch = response.data.data.requestAirBook.airLine ? response.data.data.requestAirBook.airLine.name : "";
          vm.airlineserchCode = response.data.data.requestAirBook.airLine ? response.data.data.requestAirBook.airLine.code : "";
          vm.journeyType = response.data.data.requestAirBook.journeyType.id;
          vm.pnrCode = response.data.data.requestAirBook.pnrNumber;
          try {
            if (response.data.data.requestAirBook.requestAirBookServiceList[0].requestedprovider.id != null && response.data.data.requestAirBook.requestAirBookServiceList[0].requestedprovider.id != undefined) {
              vm.selectedSuppliers = response.data.data.requestAirBook.requestAirBookServiceList[0].requestedprovider.id;
            } else if (response.data.data.requestAirBook.provider.id != null && response.data.data.requestAirBook.provider.id != undefined) {
              vm.selectedSuppliers = response.data.data.requestAirBook.provider.id
            }
            else {
              vm.selectedSuppliers = "ANY";
            }
          } catch (supp) {
            vm.selectedSuppliers = "ANY";
          }

          vm.totalAmount = response.data.data.requestAirBook.requestAirBookPax.reduce(function (sum, curr) {
            return parseFloat(sum + parseFloat(curr.debit ? curr.debit.value : 0));
          }, 0)

          // vm.remarks = response.data.data.requestHST[0].comments;
          for (var ind = 0; ind < response.data.data.requestHST.length; ind++) {

            if (response.data.data.requestHST[ind].comments) {
              var tempRemarks;
              vm.remark = [];
              tempRemarks = _.filter(response.data.data.requestHST, function (remark) { return remark.comments; });
              tempRemarks.forEach(remark => {
                vm.remark.push({ name: remark.user.firstName, comments: remark.comments });
              });
              break;
            }

          }
          vm.requestAttachments = response.data.data.requestAttachments || [];
          var rqStatus = response.data.data.requestTypeStatus.requestStatus.name.toLowerCase();

          if (rqStatus == "applied") {
            vm.isApplied = true;
            vm.isForUpdate = true;
            vm.isDisable = true;
            vm.isDisable = false;
          }

          if (rqStatus == "completed") {
            vm.isForUpdate = true;
            vm.isCompleted = true;
            vm.isDisable = true;
          }
          if (rqStatus == "rejected") {
            vm.isForUpdate = true;
            vm.isCompleted = true;
            vm.isDisable = true;
          }
          if (rqStatus == "cancelled") {
            vm.isForUpdate = true;
            vm.isCompleted = true;
            vm.isDisable = true;
          } if (rqStatus == "unclaimed") {
            vm.isDisable = true;
          }

          if (rqStatus == "completed" || rqStatus == "on progress") {
            vm.isReadOnly = true;
            vm.isDisable = true;
          } else {
            vm.isReadOnly = false;
          }
          setTimeout(() => {
            vm.fireJQueryEvents();
          }, 1000);

        } else {

          alertify.alert('Error', 'We have found some technical difficulties. Please contact admin!');
        }


      }).catch(function (e) {
        console.log("ERR");
        alertify.alert('Error', 'We have found some technical difficulties. Please contact admin!');
      })

    } else {
      var self = this;
      setTimeout(() => {
        self.tripChange({ target: { value: self.journeyType } });
        setTimeout(() => {
          if (self.isReadOnly == false) {
            self.addPassengerDetails();
          }

        }, 1000);


      }, 1000);
      this.$nextTick(function () {


      });

    }
  },
  mounted: function () {
  },
  methods: {
    allowNumber: function () {
      if ((event.keyCode >= 48 && event.keyCode <= 57) ||
        (event.keyCode >= 96 && event.keyCode <= 105) ||
        event.keyCode == 08 ||
        event.keyCode == 46 &&
        event.keyCode != 32) {
      } else {
        event.preventDefault();
      }
    },
    allowText: function () {
      if (((event.keyCode == 8) || (event.keyCode == 32) ||
        (event.keyCode == 96) || (event.keyCode == 9) ||
        (event.keyCode >= 65 && event.keyCode <= 90) ||
        (event.keyCode >= 97 && event.keyCode <= 122))) {
      } else {
        event.preventDefault();
      }
    },
    hideautocompelteair: function () {
      this.airlineautoCompleteProgress = false;
    },
    onSelectedAirlineAutoComplete: _.debounce(function (keywordEntered, event) {
      // console.log(event.code);
      if (event.code != 'ArrowDown' && event.code != 'ArrowUp') {
        this.highlightIndex = 0;
      }
      if (keywordEntered.length > 1) {
        this.airlineautoCompleteProgress = true;
        this.airlineloading = true;
        var newData = [];
        this.airlineList.filter(function (el) {
          if (el.C.toLowerCase().indexOf(keywordEntered.toLowerCase()) >= 0) {
            newData.push({
              "code": el.C,
              "label": el.A
            });
          }
        });

        this.airlineList.filter(function (el) {
          if (el.A.toLowerCase().indexOf(keywordEntered.toLowerCase()) >= 0) {
            newData.push({
              "code": el.C,
              "label": el.A
            });
          }
        });
        if (newData.length > 0) {
          this.Airlineresults = newData;
          this.airlineautoCompleteProgress = true;
          this.airlineloading = false;
          this.selectedSrc = newData[0];
        }
        else {
          this.Airlineresults = [];
          this.airlineautoCompleteProgress = false;
          this.airlineloading = false;
          this.selectedSrc = {}
        }
        event.target.focus();
      }
      else {
        this.Airlineresults = [];
        this.airlineautoCompleteProgress = false;
        this.airlineloading = false;
      }
    }, 100),
    onSelectedAirline: function (code, label, type) {
      this.airlineautoCompleteProgress = true;
      this.airlineloading = false;
      this.airlineserch = label;
      this.airlineserchCode = code;
      this.Airlineresults = [];
      this.preferAirline = code;

      if (type == undefined) {
        $("#book-source").focus();
      }
    }, tripChange: function (event) {
      var id = event.target.value;
      if (id == 1 && this.tripDetails.length == 0) {
        this.addTripDetails();
        this.roundTripFlag = false;
      } else if (id == 1 && this.tripDetails.length > 1) {
        this.temptripDetails = this.tripDetails;
        this.tripDetails = this.tripDetails.slice(0, 1);
        this.roundTripFlag = false;
      } else if (id == 2 && this.tripDetails.length == 0) {
        this.addTripDetails();
        this.addTripDetails();
        this.roundTripFlag = true;
      } else if (id == 2 && this.tripDetails.length > 2) {
        this.tripDetails = this.tripDetails.slice(0, 2);
        if (this.tripDetails[0].airportCodeSrc.code) {
          this.tripDetails[1].airportCodeDst = this.tripDetails[0].airportCodeSrc;
        }
        if (this.tripDetails[0].airportCodeDst.code) {
          this.tripDetails[1].airportCodeSrc = this.tripDetails[0].airportCodeDst;
        }
        this.roundTripFlag = true;
      } else if (id == 2 && this.tripDetails.length == 1) {
        if (this.temptripDetails.length > 0) {
          this.tripDetails = this.temptripDetails.slice();
          this.fireJQueryEvents("t");
        } else {
          this.addTripDetails();
        }
        if (this.tripDetails[0].airportCodeSrc.code) {
          this.tripDetails[1].airportCodeDst = this.tripDetails[0].airportCodeSrc;
        }
        if (this.tripDetails[0].airportCodeDst.code) {
          this.tripDetails[1].airportCodeSrc = this.tripDetails[0].airportCodeDst;
        }
        this.roundTripFlag = true;
      } else if (id == 3) {
        this.roundTripFlag = false;
      } else if (id == 2) {
        this.roundTripFlag = true;
      }
      this.tripDetails = JSON.parse(JSON.stringify(this.tripDetails));

    },
    addTripDetails: function () {
      var newTrip = {
        airportCodeSrc: {
          code: undefined,
          name: undefined
        },
        airportCodeDst: {
          code: undefined,
          name: undefined
        },
        departureDate: undefined,
        cabinClassCode: "Y",
        rbd: "Y",
        flightNum: undefined,
      };

      this.tripDetails.push(newTrip);
      this.fireJQueryEvents("t");
    },
    fireJQueryEvents: function (tableDetails) {
      var vm = this;

      this.$nextTick(function () {
        var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
        var numberofmonths = generalInformation.systemSettings.calendarDisplay;
        $(".dateInput").each(function (index) {
          var beforeID = this.id;
          var maxDate = "180d";
          var mindate = "-100y";
          var yearRange = "-80:+0";
          if (beforeID.includes('dob')) {
            maxDate = new Date();

            if (vm.passengerDetails[$(this).attr("data-id")].pax.flightPax.dob != undefined && !vm.passengerDetails[$(this).attr("data-id")].pax.flightPax.dob.includes('-')) {
              //newPax.pax.flightPax.dob=moment(newPax.pax.flightPax.dob, "DD MMM YY").format('YYYY-MM-DD');
            }

          } else {
            mindate = "0d";
            yearRange = "-80:+1";
            maxDate = "1y";
            if (vm.journeyType == '2' || vm.journeyType == '3') {
              var oldDatePicker = $('#deptDate01' + (Number(index) - 1));

              if (oldDatePicker != undefined && oldDatePicker.datepicker("getDate") != undefined) {
                mindate = oldDatePicker.datepicker("getDate");
              }


            }
          }
          $(this).datepicker({
            minDate: mindate,
            maxDate: maxDate,
            numberOfMonths: numberofmonths,
            changeYear: true,
            changeMonth: true,
            showButtonPanel: false,
            yearRange: yearRange,
            dateFormat: systemDateFormat,
            onSelect: function (selectedDate, inst) {
              if (beforeID.includes('dob')) {
                var startDate = new Date(inst.selectedYear, inst.selectedMonth, inst.selectedDay);
                var age = new Date().getFullYear() - startDate.getFullYear();
                vm.passengerDetails[$(this).attr("data-id")].pax.flightPax.dob = selectedDate;
                vm.passengerDetails[$(this).attr("data-id")].pax.age = age;
                var paxType = undefined;
                if (age <= 2) {
                  paxType = "INF";
                } else if (age <= 12) {
                  paxType = "CHD";
                } else if (12 < age) {
                  paxType = "ADT";
                }
                vm.passengerDetails[$(this).attr("data-id")].pax.paxType.code = paxType;
              } else {
                if (beforeID.includes('deptDate')) {
                  vm.tripDetails[$(this).attr("data-id")].departureDate = moment(selectedDate, "DD MMM YY").format('DD MMM YY, ddd');
                  $(this).parent().find("p").text('');
                  if (vm.journeyType == '2' || vm.journeyType == '3') {
                    $(".dateInput").each(function (nextindex) {
                      if (this.id.includes('deptDate01' + (Number(index) + 1))) {
                        $(this).datepicker('option', 'minDate', selectedDate);
                        vm.tripDetails[$(this).attr("data-id")].departureDate = '';
                      }
                    });
                  }
                }


              }
              $(this).parent().closest('td').next().children('input').focus();
              // $("#retDate").datepicker("option", "minDate", selectedDate);
            }
          });
        });

      });
    },
    removeTripDetails: function (index) {
      var vm = this;
      alertify.confirm("Warning", "Do you want to delete this trip details?",
        function () {
          vm.tripDetails.splice(index, 1);
        },
        function () { });
    },
    flightSearchcomplete: function (code, label, index, key) {
      if (code != undefined && label != undefined) {
        this.tripDetails[index][key]["code"] = code;
        this.tripDetails[index][key]["name"] = label;
        var nextNumber = (Number(index) + 1);
        if (this.journeyType == 2 && key == 'airportCodeSrc' && this.tripDetails.length > nextNumber) {
          this.tripDetails[nextNumber]["airportCodeDst"]["code"] = code;
          this.tripDetails[nextNumber]["airportCodeDst"]["name"] = label;
          $(".dateInput").each(function (indexDate) {
            if (indexDate == nextNumber) {

            }
          });
        } else if (this.journeyType == 2 && key == 'airportCodeDst' && this.tripDetails.length > nextNumber) {
          this.tripDetails[nextNumber]["airportCodeSrc"]["code"] = code;
          this.tripDetails[nextNumber]["airportCodeSrc"]["name"] = label;
        }
      }



    },
    addPassengerDetails: function () {

      var newTrip = {
        firstName: undefined,
        surName: undefined,
        age: undefined,
        gender: "M",
        nationality: {
          code: this.commonStore.agencyNode.loginNode.country.code
        },
        residence: {
          code: undefined
        },
        lead: true,
        paxType: {
          code: "ADT"
        },
        title: {
          id: 1
        },
        flightPax: {
          passportNumber: undefined,
          dob: undefined
        }
      };

      this.passengerDetails.push({ pax: newTrip });
      this.fireJQueryEvents("p");
    },
    removePassengerDetails: function (index) {
      var vm = this;
      alertify.confirm("Warning", "Do you want to delete this passenger details?",
        function () {
          vm.passengerDetails.splice(index, 1);
        },
        function () { });
    },
    insertOfflineRequest: function () {
      var vm = this;
      this.$validator.validate().then(function (result) {
        vm.isRequestSending = true;
        var alertMessage = "";
        var isRequiredFieldValidation = false;
        var isSameAirport = false;
        var isEmptyAirportSrcDst = false;
        var isEmptyAirportSrc = false;
        var isEmptyAirportDst = false;

        for (var index = 0; index < vm.tripDetails.length; index++) {
          if ((vm.tripDetails[index].airportCodeSrc.code != undefined || vm.tripDetails[index].airportCodeDst.code != undefined) &&
            vm.tripDetails[index].airportCodeSrc.code == vm.tripDetails[index].airportCodeDst.code) {
            isSameAirport = true;
            break;
          }
        }

        for (var index = 0; index < vm.tripDetails.length; index++) {
          if (!vm.tripDetails[index].airportCodeSrc.code && !vm.tripDetails[index].airportCodeDst.code) {
            isEmptyAirportSrcDst = true;
            break;
          }
        }

        for (var index = 0; index < vm.tripDetails.length; index++) {
          if (!vm.tripDetails[index].airportCodeSrc.code) {
            isEmptyAirportSrc = true;
            break;
          }
        }

        for (var index = 0; index < vm.tripDetails.length; index++) {
          if (!vm.tripDetails[index].airportCodeDst.code) {
            isEmptyAirportDst = true;
            break;
          }
        }

        if (!result) {
          alertMessage = "Please enter required fields.";
        } else if (vm.tripDetails.length == 0) {
          alertMessage = "Please add trip details.";
        } else if (vm.passengerDetails.length == 0) {
          alertMessage = "Please add passenger details.";
        } else if (vm.passengerDetails.length == 0 && vm.tripDetails.length == 0) {
          alertMessage = "Please add trip and passenger details.";
        } else if (isSameAirport) {
          alertMessage = "Departure and arrival airports should not be same.";
        } else if (isEmptyAirportSrcDst) {
          alertMessage = "Please provide valid departure and destination airport.";
        } else if (isEmptyAirportSrc) {
          alertMessage = "Please provide valid departure airport.";
        } else if (isEmptyAirportDst) {
          alertMessage = "Please provide valid destination airport.";
        }
        else {
          isRequiredFieldValidation = true;
        }
        var adultCount = 0;
        var childCount = 0;
        var infantCount = 0;
        vm.passengerDetails.forEach(function (e) {
          if (e.pax.paxType.code == 'ADT') {
            adultCount = Number(adultCount) + 1;
          } else if (e.pax.paxType.code == 'CHD') {
            childCount = Number(childCount) + 1;
          } else if (e.pax.paxType.code == 'INF') {
            infantCount = Number(infantCount) + 1;
          }
        });
        if (adultCount == 0 && alertMessage == '') {
          alertMessage = "Please add one adult in passenger details.";
          isRequiredFieldValidation = false;
        } else if (adultCount < infantCount && alertMessage == '') {
          alertMessage = "Infant is greater than adult in passenger details.";
          isRequiredFieldValidation = false;
        }

        var providername = _.where(vm.supplierList, { id: vm.selectedSuppliers })[0].name;
        var provider = { id: vm.selectedSuppliers, name: providername };

        if (vm.selectedSuppliers == 'ANY') {
          provider = null;
        }
        if (result && isRequiredFieldValidation) {
          var request = {
            requestTypeStatus: {
              requestType: {
                id: 1
              },
              requestStatus: {
                id: 1
              }
            },
            requestHST: [{
              comments: vm.remarks
            }],
            requestAirBook: {
              pnrNumber: vm.pnrCode,
              airLine: {
                name: vm.airlineserch,
                code: vm.airlineserchCode
              },
              journeyType: {
                id: parseInt(vm.journeyType)
              },
              leg: vm.tripDetails.map(function (e) {
                return {
                  airportCodeSrc: {
                    code: e.airportCodeSrc.code,
                    name: e.airportCodeSrc.name
                  },
                  airportCodeDst: {
                    code: e.airportCodeDst.code,
                    name: e.airportCodeDst.name
                  },
                  departureDate: moment(e.departureDate, "DD MMM YY").format('YYYY-MM-DD'),
                  cabinClassCode: e.cabinClassCode == "ANY" ? null : e.cabinClassCode,
                  rbd: e.rbd == "ANY" ? null : e.rbd,
                  flightNum: e.flightNum,
                }
              }),
              node: {
                id: vm.commonStore.agencyNode.loginNode.code.replace(/^\D+/g, '')
              },
              provider: provider,
              service: {
                id: 1 // flights
              },
              requestAirBookPax: vm.passengerDetails.map(function (e) {
                var paxDetails = JSON.parse(JSON.stringify(e));
                if (paxDetails.pax.flightPax.dob != undefined) {
                  paxDetails.pax.flightPax.dob = moment(paxDetails.pax.flightPax.dob, 'DD MMM YY, ddd').format('YYYY-MM-DD');
                }
                return paxDetails;
              })
            }
          }

          var axiosConfig = {
            headers: {
              "Content-Type": "application/json",
              Authorization: "Bearer " + window.localStorage.getItem("accessToken")
            }
          };

          var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrlOffline;
          var port = vm.commonStore.hubUrls.hubConnection.ipAddress;
          var offlineRequest = vm.commonStore.hubUrls.hubConnection.hubServices.flights.offlineRequest;
          console.log(request);
          axios.post(hubUrl + port + offlineRequest, request, axiosConfig).then(function (response) {
            if (response.data.message = "Created Successfully" && response.data.code == 201) {
              alertify.alert("Success", "Offline Booking Request Submitted Successfully.", function () {
                window.location.href = "/searchofflinebookings.html";
              });
              // vm.tripDetails = [];
              // vm.passengerDetails = [];
              // vm.airlineserch = undefined;
              // vm.selectedSuppliers = "";
              // vm.journeyType = "";
            } else {
              alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");
            }
            vm.isRequestSending = false;
            window.sessionStorage.setItem("offlineReqTab", "flights");
          }).catch(function (e) {
            try {
              if (e.response.data.code && e.response.data.message) {
                alertify.alert("Warning", e.response.data.message);
              }
              else {
                alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");
              }
            } catch (error) {
              alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");

            }
            vm.isRequestSending = false;

          });
        } else {
          alertify.alert("Warning", alertMessage);
          vm.isRequestSending = false;

        }
      });
    },
    updateOfflineRequest: function () {
      var vm = this;
      this.$validator.validate().then(function (result) {
        var alertMessage = "";
        var isRequiredFieldValidation = false;

        if (!result) {
          alertMessage = "Please enter required fields.";
        } else if (vm.tripDetails.length == 0) {
          alertMessage = "Please add trip details.";
        } else if (vm.passengerDetails.length == 0) {
          alertMessage = "Please add passenger details.";
        } else if (vm.passengerDetails.length == 0 && vm.tripDetails.length == 0) {
          alertMessage = "Please add trip and passenger details.";
        } else {
          isRequiredFieldValidation = true;
        }
        var provider = { id: vm.selectedSuppliers };

        if (vm.selectedSuppliers == 'ANY') {
          provider = null;
        }
        var adultCount = 0;
        var childCount = 0;
        var infantCount = 0;
        var consPassenger = [];
        for (var index = 0; index < vm.passengerDetails.length; index++) {
          const element = vm.passengerDetails[index];
          const newPax = JSON.parse(JSON.stringify(element));
          if (newPax.pax.flightPax.dob != undefined && !newPax.pax.flightPax.dob.includes('-')) {
            newPax.pax.flightPax.dob = moment(newPax.pax.flightPax.dob, 'DD MMM YY, ddd').format('YYYY-MM-DD');
          }
          if (element.pax.paxType.code == 'ADT') {
            adultCount = Number(adultCount) + 1;
          } else if (element.pax.paxType.code == 'CHD') {
            childCount = Number(childCount) + 1;
          } else if (element.pax.paxType.code == 'INF') {
            infantCount = Number(infantCount) + 1;
          }
          consPassenger.push(newPax);
        }
        if (adultCount == 0 && alertMessage == '') {
          alertMessage = "Please add one adult in passenger details.";
          isRequiredFieldValidation = false;
        } else if (adultCount < infantCount && alertMessage == '') {
          alertMessage = "Infant is greater than adult in passenger details.";
          isRequiredFieldValidation = false;
        }
        var loginNode = vm.commonStore.agencyNode.loginNode.code.replace(/^\D+/g, '');
        if (vm.dataResponse.data.requestAirBook.node != undefined && vm.dataResponse.data.requestAirBook.node.id != undefined) {
          loginNode = vm.dataResponse.data.requestAirBook.node.id;
        }
        if (result && isRequiredFieldValidation) {
          var request = {
            requestTypeStatus: vm.dataResponse.data.requestTypeStatus,
            requestAirBook: {
              pnrNumber: vm.pnrCode,
              airLine: {
                code: vm.airlineserchCode,
                name: vm.airlineserch
              },
              journeyType: {
                id: vm.journeyType,
                name: vm.journeyType == 1 ? "Oneway" : vm.journeyType == 2 ? "Roundtrip" : "Multi City"
              },
              leg: vm.tripDetails.map(function (e) {
                return {
                  id: e.id,
                  airportCodeSrc: e.airportCodeSrc,
                  airportCodeDst: e.airportCodeDst,
                  departureDate: moment(e.departureDate, "DD MMM YY").format('YYYY-MM-DD'),
                  cabinClassCode: e.cabinClassCode == "ANY" ? null : e.cabinClassCode,
                  rbd: e.rbd == "ANY" ? null : e.rbd,
                  flightNum: e.flightNum,
                }
              }),
              node: {
                id: loginNode
              },
              provider: provider,
              service: {
                id: 1
              },
              requestAirBookPax: consPassenger
            },
            requestHST: [{
              comments: vm.remarks
            }],
            requestAttachments: vm.dataResponse.data.requestAttachments,
            id: vm.dataResponse.data.id
          }

          var axiosConfig = {
            headers: {
              "Content-Type": "application/json",
              Authorization: "Bearer " + window.localStorage.getItem("accessToken")
            }
          };


          var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrlOffline;
          var port = vm.commonStore.hubUrls.hubConnection.ipAddress;
          var offlineRequest = vm.commonStore.hubUrls.hubConnection.hubServices.flights.offlineRequest;

          alertify.confirm("Warning", "Do you want to save changes?",
            function () {
              axios.put(hubUrl + port + offlineRequest, request, axiosConfig).then(function (response) {
                if (response.data.message = "Details Updated Successfully" && response.data.code == 202) {
                  alertify.alert("Success", "Offline Booking Request Updated Successfully.", function () {
                    window.location.href = "/searchofflinebookings.html";
                  });
                } else if (response.data.code == 400) {
                  var warning = response.data.message;
                  alertify.alert("Warning", warning);
                }

                else {
                  alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");
                }
                window.sessionStorage.setItem("offlineReqTab", "flights");
              }).catch(function (e) {
                try {
                  if (e.response.data.code && e.response.data.message) {
                    alertify.alert("Warning", e.response.data.message);
                  }
                  else {
                    alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");
                  }
                } catch (error) {
                  alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");

                }
              });
            },
            function () { });
        } else {
          alertify.alert("Warning", alertMessage);
        }
      });
    },
    // passengerChange:function(pax){
    //   pax.flightPax.dob="";
    // },
    cancelUpdate: function () {
      var vm = this;
      var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrlOffline;
      var port = vm.commonStore.hubUrls.hubConnection.ipAddress;
      var offlineRequestUpdate = vm.commonStore.hubUrls.hubConnection.hubServices.flights.offlineRequestUpdate;
      var url = hubUrl + port + offlineRequestUpdate;
      var axiosConfig = {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + window.localStorage.getItem("accessToken")
        }
      };
      var request = vm.dataResponse.data;
      alertify.confirm("Warning", "Do you want to cancel Request?",
        function () {
          if (request.requestTypeStatus.requestStatus.id == 1) {
            axios.put(url, request, axiosConfig).then(function (response) {
              if (response.data.message = "Details Updated Successfully" && response.data.code == 202) {
                alertify.alert("Success", "Cancel Successful.", function () {
                  window.location.href = "/searchofflinebookings.html";
                });
              }
              else {
                alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");
              }
            }).catch(function (e) {
              console.log(e);
              try {
                if (e.response.data.code && e.response.data.message) {
                  alertify.alert("Warning", e.response.data.message);
                }
                else {
                  alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");
                }
              } catch (error) {
                alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");

              }
            });

          }
        },
        function () { });
    },
    updateGender: function (passenger) {

      passenger.pax.gender = event.target.value == "1" || event.target.value == "4" ? "M" : "F";
    },
    up: function () {
      if (this.airlineautoCompleteProgress) {
        if (this.highlightIndex > 0) {
          this.highlightIndex--
        }
      } else {
        this.airlineautoCompleteProgress = true;
      }
      this.fixScrolling();
    },
    down: function () {
      if (this.airlineautoCompleteProgress) {
        if (this.highlightIndex < this.Airlineresults.length - 1) {
          this.highlightIndex++
        } else if (this.highlightIndex == this.Airlineresults.length - 1) {
          this.highlightIndex = 0;
        }
      } else {
        this.airlineautoCompleteProgress = true;
      }
      this.fixScrolling();
    },
    fixScrolling: function () {
      if (this.$refs.options[this.highlightIndex]) {
        var liH = this.$refs.options[this.highlightIndex].clientHeight;
        if (liH == 50) {
          liH = 32;
        }
        if (liH * this.highlightIndex >= 32 || this.highlightIndex == 0) {
          this.$refs.searchautocomplete.scrollTop = liH * this.highlightIndex;
        }
      }
    },
    autocomplete: function () {
      this.airlineautoCompleteProgress = true;
    },
    tabclick: function (item) {
      if (!item) {
      }
      else {
        this.onSelectedAirline(item.code, item.label, 'Tab');
      }
    }
  },
  watch: {
    $data: {
      handler: function () {
        this.isUpdated = true;
      },
      deep: true
    },
    airlineserch: function () {

      if (this.airlineserch == "") {
        this.airlineserchCode = "";
      }
    }
  },
  computed: {}
});