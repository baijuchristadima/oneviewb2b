﻿function stringIsNullorEmpty(value) {
    var status = false;
    if (value == undefined || value == null || value == '') { status = true; }
    return status;
}

function isValidEmail(emailID) {
    var status = false;
    var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    var matchArray = emailID.match(emailPat);
    if (matchArray != null) { status = true; }
    return status;
}

function isNullorUndefind(value) {
    var status = false;
    if (value == null || value == undefined || value == "undefined") { status = true; }
    return status;
}

function isNumber(value) {
    var status = false;
    if (Math.floor(value) == value && $.isNumeric(value)) { status = true }
    return status;
}

function hasArrayData(value) {
    var status = false;
    if (value == undefined || value == null || value == '') {
        status = false;
    } else {
        if (value.length > 0) {
            status = true;
        }
    }
    return status;
}

$(".numbersOnly").keydown(function (e) {
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        (e.keyCode >= 35 && e.keyCode <= 40)) {
        return;
    }
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});

function isNumber(input) {
    var isNum = false;
    var intRegex = /^\d+$/;
    var floatRegex = /^((\d+(\.\d *)?)|((\d*\.)?\d+))$/;
    if (intRegex.test(input) || floatRegex.test(input)) { var isNum = true; }
}

function isValidPassport(passportNumber) {
    var status = false;
    var passportPattern = /^[a-zA-Z0-9]+$/;
    var matchArray = passportNumber.match(passportPattern);
    if (matchArray != null) { status = true; }
    return status;
}


function isAjaxSuccess(response) {
    var isSuccess = response.d != null && response.d != undefined && response.d != "" ? true : false;
    return isSuccess;
}

function isAjaxSuccessAsync(response) {

    var isSuccess = response.d != null && response.d != undefined && response.d != "" ? true : false;
    if (isSuccess) { isSuccess = response.d.Result != null && response.d.Result != undefined && response.d.Result != "" ? true : false; }
    return isSuccess;
}

// updates by shyam - start \\
function isNullorEmpty(value) {
    var status = false;
    if (value == null || value == undefined || value == "undefined") { status = true; }
    if (!status && $.trim(value) == '') { status = true; }
    return status;
}

function isNullorEmptyToBlank(value, optval) {
    return isNullorEmpty(value) ? (isNullorEmpty(optval) ? '' : optval) : value;
}

//function isNullorEmptyToBlank(value, optval) {
//    return isNullorEmpty(value) ? (isNullorEmpty(optval) ? '' : optval) : value;
//}

Number.prototype.pad = function (size) {
    if (isNullorUndefind(size)) size = 2;
    var sign = Math.sign(this) === -1 ? '-' : '';
    return sign + new Array(size).concat([Math.abs(this)]).join('0').slice(-size);
}

String.prototype.pad = function (size) {
    if (isNullorUndefind(size)) size = 2;
    var sign = Math.sign(this) === -1 ? '-' : '';
    return sign + new Array(size).concat([Math.abs(this)]).join('0').slice(-size);
}

$(document).ready(function () {
    var messageDiv =
        '<div id="message" style="position:fixed;top:0px;left:0;width:100%;z-index:1000;display:none;z-index:999999999">' +
        '<div style="padding: 5px;">' +
        '<div id="inner-message" style="margin:0 auto;" class="">' +
        '<button type="button" class="close hidden" data-dismiss="alert">&times;</button>' +
        '<div id="inner-message-content">hello</div>' +
        '</div>' +
        '</div>' +
        '</div>';
    $('body').append(messageDiv);
});

function showNotification(notifyBody, notifyType, callBack, closeTime) {
    if (isNullorUndefind(notifyBody))
        notifyBody = 'sample message';
    if (isNullorUndefind(notifyType))
        notifyType = 'success';
    var alertType = 'info';
    switch (notifyType) {
        case 'success': alertType = 'success'; break;
        case 'info': alertType = 'info'; break;
        case 'warning': alertType = 'warning'; break;
        default: alertType = 'danger'; break;
    }
    alertType = 'alert alert-' + alertType;
    if (isNullorUndefind(closeTime))
        closeTime = 2500;
    if (isNullorUndefind(callBack))
        callBack = function (data) { console.log('notification done'); }
    $('#inner-message').addClass(alertType);
    $('#inner-message-content').html(notifyBody);
    $('#message').show().fadeOut(closeTime, function () {
        $('#inner-message-content').html('');
        $('#inner-message').removeClass(alertType);
        callBack();
    });
}

function ParseInteger(value) {
    var retval = 0;
    try { retval = parseInt(value); } catch (err) { }
    if (isNullorEmpty(retval)) retval = 0;
    return retval;
}
// updates by shyam - end \\

function isMobile() {
    var isMobile = false;
    if (parseInt($(window).width()) > 500 && parseInt($(window).width()) <= 999) { isMobile = true }
    else if (parseInt($(window).width()) > 999) { }
    return isMobile;
}
function ParseDecimal(value) {
    var retval = 0;
    try { retval = parseFloat(value); } catch (err) { }
    if (isNullorEmpty(retval)) retval = 0;
    return retval;
}

function ajaxCall(requestUrl, dataRequest, type, dataType, contentType, async, callBack, showNotice, notifyBodySuccess, closeTime) {
    var notifyType = 'error';
    var notifyBody = 'Process failed.';
    if (isNullorUndefind(async))
        async = true;
    if (isNullorUndefind(showNotice))
        showNotice = false;
    if (isNullorUndefind(callBack))
        callBack = function () { console.log('ajaxCall done'); }
    if (isNullorUndefind(closeTime))
        closeTime = 10000;
    if (isNullorUndefind(type))
        type = 'POST';
    if (isNullorUndefind(dataType))
        dataType = 'json';
    var lastResponse;
    var ajaxOptions = {
        type: type,
        url: requestUrl,
        dataType: dataType,
        async: async,
        success: function (response) {
            if (!isNullorEmpty(response)) {
                notifyType = 'success';
                notifyBody = notifyBodySuccess;
            }
            lastResponse = response;
        },
        failure: function (response) {
            lastResponse = response;
        },
        statusCode: {
            500: function () {
            }
        }
    }
    if (!isNullorUndefind(dataRequest))
        ajaxOptions.data = JSON.stringify(dataRequest);
    if (!isNullorUndefind(contentType))
        ajaxOptions.contentType = 'application/json; charset=utf-8';

    $.ajax(ajaxOptions).done(function () {
        if (showNotice) {
            showNotification(notifyBody, notifyType, callBack(lastResponse), closeTime);
        } else {
            callBack(lastResponse);
        }
    });
}

// $(document).ready(function () {
//     var url = window.location.href;
//     if (url.indexOf('/login') >= 0 && url.indexOf('localhost:') < 0) {
//         $.ajax({
//             type: "POST",
//             url: "/CommonFuntions/WebServices.aspx/GetAgencyFolder",
//             contentType: "application/json; charset=utf-8",
//             dataType: "json",
//             success: function (data) {
//                 console.log(data.d);
//                 if (url.indexOf(data.d) < 0 || data.d == '') {
//                     window.location = '/' + data.d;
//                 }
//             },
//             async: false,
//             failure: function (response) { }
//         });
//     }
//     showwebpage();
// });

function showwebpage() {
    setTimeout(function () {
        $('#pageLoader').hide();
    }, 1000);
}
function roundAmount(amount) {
    return parseFloat(Math.round(amount * 100) / 100).toFixed(2);
}