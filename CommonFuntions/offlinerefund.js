Vue.component("offline-refund", {
  data: function () {
    return {
      commonStore: vueCommonStore.state,
      passengerDetails: [],
      pnrCode: null,
      noShow: false,
      fullRefund: false,
      route: null,
      airlineautoCompleteProgress: false,
      pnrCode: undefined,
      airlineserch: undefined,
      airlineserchCode: undefined,
      Airlineresults: [],
      airlineloading: false,
      airlineList: AirlinesDatas,
      highlightIndex: 0,
      remarks: null,
      remark: null,
      totalAmount: null,
      Request_TypeId: 0,
      isReadOnly: true,
      refunded: false,
      isRequestSending: false,
      isCancel: false,
      isVoidRequest: false,
      isRefundRequest: false,
      departureDate: null,
      forOfflinerequestFromTicketing: false,
      checkedNotes: []
    };
  },
  created: function () {
    var vm = this;
    var offlineRefundId = window.sessionStorage.getItem("offlineRefundId");
    var offlineRequestData = window.sessionStorage.getItem("offlineRequestData");
    var paxId = window.sessionStorage.getItem("paxId");
    if (offlineRefundId) {
      var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrlOffline;
      var port = vm.commonStore.hubUrls.hubConnection.ipAddress;
      var offlineRefundRequest = vm.commonStore.hubUrls.hubConnection.hubServices.flights.offlineRefund;
      axios({
        method: "GET",
        url: hubUrl + port + offlineRefundRequest + "/" + offlineRefundId + "/" + paxId,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
        }
      }).then((response) => {

        console.log("RESPONSE RECEIVED: ", response);
        this.dataResponse = response.data;

        if (response.data.data.id) {
          var rqType = response.data.data.requestTypeStatus.requestType.id;
          if (rqType == 3) {
            vm.isRefundRequest = true;
          } else if (rqType == 4) {
            vm.isVoidRequest = true;
          }


          vm.passengerDetails = response.data.data.requestCancel.passengers.map(function (e) {
            return {
              pax: {
                id: e.pax.id || undefined,
                firstName: e.pax.firstName || undefined,
              },
              ticketNumber: e.ticketNumber ? e.ticketNumber : undefined,
              fare: e.fare ? vm.isRefundRequest ? e.fare.find(function (curr) {
                return curr.key ? curr.key == "refundAmount" : 0;
              }) : vm.isVoidRequest ? e.fare.find(function (curr) {
                return curr.key ? curr.key == "serviceFee" : 0;
              }) : undefined : undefined

            }
          });
          vm.Request_TypeId = response.data.data.requestTypeStatus.requestType.id;
          vm.airlineserch = response.data.data.requestCancel.airLine ? response.data.data.requestCancel.airLine.name : "";
          vm.airlineserchCode = response.data.data.requestCancel.airLine ? response.data.data.requestCancel.airLine.code : "";
          vm.pnrCode = response.data.data.requestCancel.pnrNumber;
          vm.noShow = response.data.data.requestCancel.noShow;
          vm.fullRefund = response.data.data.requestCancel.fullRefund;
          vm.route = response.data.data.requestCancel.route;
          vm.departureDate = moment(new Date(response.data.data.requestCancel.travelDate)).format('DD/MM/YYYY');
          vm.totalAmount = response.data.data.requestCancel.passengers.reduce(function (sum, curr) {
            return parseFloat(sum + parseFloat(curr.fare ? curr.fare.value : 0));
          }, 0)

          // vm.remarks = response.data.data.requestHST[0].comments;


          for (var ind = 0; ind < response.data.data.requestHST.length; ind++) {

            if (response.data.data.requestHST[ind].comments) {
              var tempRemarks;
              vm.remark = [];
              tempRemarks = _.filter(response.data.data.requestHST, function (remark) { return remark.comments; });
              tempRemarks.forEach(remark => {
                vm.remark.push({ name: remark.user.firstName+' '+remark.user.lastName, comments: remark.comments });
              });
              break;
            }

          }
          var rqStatus = response.data.data.requestTypeStatus.requestStatus.name.toLowerCase();
          if (rqStatus == "posted") {
            vm.isReadOnly = true;
            vm.refunded = true;
          } else if (rqStatus == "in progress") {
            vm.isReadOnly = true;
          } else if (rqStatus == "unclaimed") {
            vm.isReadOnly = true;
            vm.isCancel = true;
          } else if (rqStatus == "rejected") {
            vm.isReadOnly = true;
          } else if (rqStatus == "applied") {
            vm.isReadOnly = true;
            vm.isCancel = true;
          } else if (rqStatus == "cancelled") {
            vm.isReadOnly = true;
          } else if (rqStatus == "voided") {
            vm.isReadOnly = true;
            vm.refunded = true;
          } else if (rqStatus == "pending approval") {
            vm.isReadOnly = true;
          } else if (rqStatus == "pending credit note") {
            vm.refunded = true;
            vm.isReadOnly = true;
          } else if (rqStatus == "refund posted") {
            vm.isReadOnly = true;
          } else {
            vm.isReadOnly = false;
          }
        } else {
          alertify.alert('Error', 'We have found some technical difficulties. Please contact admin!');
        }


      }).catch(function (e) {
        alertify.alert('Error', 'We have found some technical difficulties. Please contact admin!');
      })

    } else if (offlineRequestData) {
      var vm = this;
      vm.forOfflinerequestFromTicketing = true;
      offlineRequestData = JSON.parse(offlineRequestData);
      if (offlineRequestData.offlineReqTab == "refund") {
        vm.isRefundRequest = true;
      } else if (offlineRequestData.offlineReqTab == "void") {
        vm.isVoidRequest = true;
      }

      vm.passengerDetails = offlineRequestData.passengerDetails.map(function (e, i) {
        return {
          pax: {
            id: (i+1) || undefined,
            firstName: e.pax.firstName + " " + e.pax.surName || undefined,
          },
          ticketNumber: e.ticketNumber ? e.ticketNumber : undefined,
          fare: e.fare ? vm.isRefundRequest ? e.fare.find(function (curr) {
            return curr.key ? curr.key == "refundAmount" : 0;
          }) : vm.isVoidRequest ? e.fare.find(function (curr) {
            return curr.key ? curr.key == "serviceFee" : 0;
          }) : undefined : undefined

        }
      });
      vm.Request_TypeId = vm.isRefundRequest ? 3 : 4;
      vm.airlineserch = offlineRequestData.airline.name || "";
      vm.airlineserchCode = offlineRequestData.airline.code || "";
      vm.pnrCode = offlineRequestData.pnrCode;
      if (offlineRequestData.journeyType == 1) {
        vm.route = offlineRequestData.tripDetails[0].airportCodeSrc.code+ "-" + 
        offlineRequestData.tripDetails[offlineRequestData.tripDetails.length -1].airportCodeDst.code;
      } else if (offlineRequestData.journeyType == 2) {
        vm.route = offlineRequestData.tripDetails[0].airportCodeSrc.code+ "-" + 
        offlineRequestData.tripDetails[offlineRequestData.tripDetails.length -1].airportCodeSrc.code + "-" +
        offlineRequestData.tripDetails[offlineRequestData.tripDetails.length -1].airportCodeDst.code;
      } else if (offlineRequestData.journeyType == 3) {
        let codes = [];
        for (let index = 0; index < offlineRequestData.tripDetails.length; index++) {
          codes.push(offlineRequestData.tripDetails[index].airportCodeSrc.code + "-" + offlineRequestData.tripDetails[index].airportCodeDst.code);
        }
        vm.route = codes.join("|");
      }
      vm.departureDate = moment(offlineRequestData.tripDetails[0].departureDate, "DD MMM YY, ddd").format('DD/MM/YYYY');
    } else {
      var self = this;
      self.addPassengerDetails();
      self.isReadOnly = false;
    }
  },
  mounted: function () {
    var vm = this;
    window.sessionStorage.setItem("offlineReqTab", "refund");
    var maxDate = "10y";
        var mindate = "-10y";
        var dateFormat = 'dd/mm/yy';
        var noOfMonths = 1;
        $("#departureDate").datepicker({
          numberOfMonths: parseInt(noOfMonths),
          changeMonth: true,
          changeYear: true,
          minDate: mindate,
          maxDate: maxDate,
          showButtonPanel: false,
          dateFormat: dateFormat,
          onSelect: function (selectedDate) {
            vm.departureDate = selectedDate;
        }
        });
  },
  methods: {
    insertOfflineRequest: function () {
      var vm = this;
      var airlineValidate = false;
      var defaultRequest = false;
      var notesRequired = false;
      var tempAirLineCheck = null;
      tempAirLineCheck = vm.airlineList.filter(e => e.A.includes(vm.airlineserch) && e.C.includes(vm.airlineserchCode));
      if (tempAirLineCheck.length == 1 && tempAirLineCheck[0].A == vm.airlineserch) {
        airlineValidate = true;
      }
      if (this.Request_TypeId == 0) {
        defaultRequest = true;
      }
      if (this.checkedNotes.length != 1) {
        notesRequired = true;
      }
      this.$validator.validate().then(function (result) {
        vm.isRequestSending = true;
        var alertMessage = "";
        var isRequiredFieldValidation = false;
        if (!result) {
          alertMessage = "Please enter required fields.";
        } else if (vm.passengerDetails.length == 0) {
          alertMessage = "Please add at least one passenger.";
        } else if (!airlineValidate) {
          alertMessage = "Please enter valid Airline.";
        } else if (defaultRequest) {
          alertMessage = "Please select request type.";
        } else if (notesRequired) {
          alertMessage = "Please accept the Disclaimer Notice as a token of compliance.";
        } else {
          isRequiredFieldValidation = true;
        }
        if (result && isRequiredFieldValidation) {
          var request = {
            requestTypeStatus: {
              requestType: {
                id: vm.Request_TypeId,
                name: vm.Request_TypeId == 3 ? "Refund" : vm.Request_TypeId == 4 ? "Void" : undefined,
              },
              requestStatus: {
                id: 1,
                name: "Applied"
              }
            },
            requestHST: [
              {
                comments: vm.remarks
              }
            ],
            requestCancel: {
              airLine: {
                code: vm.airlineserchCode,
                name: vm.airlineserch
              },
              service: {
                id: 1
              },
              pnrNumber: vm.pnrCode,
              route: vm.route,
              travelDate: moment(vm.departureDate, "DD/MM/YYYY").format('YYYY-MM-DD'),
              noShow: vm.noShow,
              fullRefund: vm.fullRefund,
              passengers: vm.passengerDetails
            }
          }
          var axiosConfig = {
            headers: {
              "Content-Type": "application/json",
              Authorization: "Bearer " + window.localStorage.getItem("accessToken")
            }
          };
          var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrlOffline;
          var port = vm.commonStore.hubUrls.hubConnection.ipAddress;
          var offlineRefund = vm.commonStore.hubUrls.hubConnection.hubServices.flights.offlineRefund;
          var offlineRequestRefund = vm.commonStore.hubUrls.hubConnection.hubServices.flights.offlineRequestRefund;
          console.log(JSON.stringify(request));
          axios.post(hubUrl + port + offlineRefund + offlineRequestRefund, request, axiosConfig).then(function (response) {
            if (response.data.code == 201 && response.data.message != undefined && response.data.message != '') {
              alertify.alert("Success", "Offline Refund/Void Request has submitted successfully.", function () {
                window.location.href = "/searchofflinebookings.html";
              });
            } else {
              alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");
            }
            vm.isRequestSending = false;
            window.sessionStorage.setItem("offlineReqTab", "refund");
          }).catch(function (e) {
            if (e.response.data.errors.length > 0) {
              alertify.alert("Warning", e.response.data.errors[0].split(" = ")[1]);
            } else {
              alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");
            }
            vm.isRequestSending = false;
          });

        } else {
          alertify.alert("Warning", alertMessage);
          vm.isRequestSending = false;
        }
      });
    },
    cancelUpdate: function () {
      var vm = this;
      var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrlOffline;
      var port = vm.commonStore.hubUrls.hubConnection.ipAddress;
      var offlineRequestRefund = vm.commonStore.hubUrls.hubConnection.hubServices.flights.offlineRefund;
      var offlineRequestCancel = vm.commonStore.hubUrls.hubConnection.hubServices.flights.offlineRefundCancel;
      var url = hubUrl + port + offlineRequestRefund + offlineRequestCancel;
      var axiosConfig = {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + window.localStorage.getItem("accessToken")
        }
      };
      var request = vm.dataResponse.data;
      alertify.confirm("Warning", "Do you want to cancel Request?",
        function () {
          vm.isRequestSending = true;
          if (request.requestTypeStatus.requestStatus.id == 1 || request.requestTypeStatus.requestStatus.id == 6) {
            request.requestTypeStatus.requestStatus.id = 5;
            request.requestTypeStatus.requestStatus.name = "Cancelled";
            axios.put(url, request, axiosConfig).then(function (response) {
              if (response.data.message = "Details Updated Successfully" && response.data.code == 202) {
                alertify.alert("Success", "Cancel Successful.", function () {
                  vm.commonStore.currentSearchBookingpage = "refund";
                  window.location.href = "/searchofflinebookings.html";
                });
              }
              else if (response.data.code == 400) {
                var warning = response.data.message;
                alertify.alert("Warning", warning);
                vm.isRequestSending = false;
              }
              else {
                alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");
                vm.isRequestSending = false;
              }
              window.sessionStorage.setItem("offlineReqTab", "refund");
            }).catch(function (e) {
              try {
                if (e.response.data.code && e.response.data.message) {
                  alertify.alert("Warning", e.response.data.message);
                  vm.isRequestSending = false;
                }
                else {
                  alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");
                  vm.isRequestSending = false;
                }
              } catch (error) {
                alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");
                vm.isRequestSending = false;
              }
            });

          }
        },
        function () { });
    },
    addPassengerDetails: function () {
      var newTrip = {
        id: this.passengerDetails.length + 1,
        firstName: undefined,
      };
      this.passengerDetails.push({ pax: newTrip });
    },
    removePassengerDetails: function (index) {
      var vm = this;
      alertify.confirm("Warning", "Do you want to delete this passenger details?",
        function () {
          vm.passengerDetails.splice(index, 1);
          vm.passengerDetails.forEach(function (passenger, index) {
            passenger.pax.id = index + 1;
          });
        },
        function () { });
    },
    allowNumber: function () {
      if ((event.keyCode >= 48 && event.keyCode <= 57) ||
        (event.keyCode >= 96 && event.keyCode <= 105) ||
        event.keyCode == 08 ||
        event.keyCode == 46 &&
        event.keyCode != 32) {
      } else {
        event.preventDefault();
      }
    },
    allowText: function () {
      if (((event.keyCode == 8) || (event.keyCode == 32) ||
        (event.keyCode == 96) || (event.keyCode == 9) ||
        (event.keyCode >= 65 && event.keyCode <= 90) ||
        (event.keyCode >= 97 && event.keyCode <= 122))) {
      } else {
        event.preventDefault();
      }
    },
    ticketNumber: function (tNumber) {
      var ticketPat = /^([0-9a-zA-Z]{3,})$/;
      var matchArray = tNumber.match(ticketPat);
      if (matchArray != null) {
        return true;
      } else {
        return false;
      }
    },
    autocomplete: function () {
      this.airlineautoCompleteProgress = true;
    },
    up: function () {
      if (this.airlineautoCompleteProgress) {
        if (this.highlightIndex > 0) {
          this.highlightIndex--
        }
      } else {
        this.airlineautoCompleteProgress = true;
      }
      this.fixScrolling();
    },
    down: function () {
      if (this.airlineautoCompleteProgress) {
        if (this.highlightIndex < this.Airlineresults.length - 1) {
          this.highlightIndex++
        } else if (this.highlightIndex == this.Airlineresults.length - 1) {
          this.highlightIndex = 0;
        }
      } else {
        this.airlineautoCompleteProgress = true;
      }
      this.fixScrolling();
    },
    fixScrolling: function () {
      if (this.$refs.options[this.highlightIndex]) {
        var liH = this.$refs.options[this.highlightIndex].clientHeight;
        if (liH == 50) {
          liH = 32;
        }
        if (liH * this.highlightIndex >= 32 || this.highlightIndex == 0) {
          this.$refs.searchautocomplete.scrollTop = liH * this.highlightIndex;
        }
      }

    },
    autocomplete: function () {
      this.airlineautoCompleteProgress = true;
    },
    tabclick: function (item) {
      if (!item) {
      }
      else {
        this.onSelectedAirline(item.code, item.label, 'Tab');
      }
    },
    hideautocompelteair: function () {
      this.airlineautoCompleteProgress = false;
    },
    onSelectedAirlineAutoComplete: _.debounce(function (keywordEntered, event) {
      // console.log(event.code);
      if (event.code != 'ArrowDown' && event.code != 'ArrowUp') {
        this.highlightIndex = 0;
      }
      if (keywordEntered.length > 1) {
        this.airlineautoCompleteProgress = true;
        this.airlineloading = true;
        var newData = [];
        this.airlineList.filter(function (el) {
          if (el.C.toLowerCase().indexOf(keywordEntered.toLowerCase()) >= 0) {
            newData.push({
              "code": el.C,
              "label": el.A
            });
          }
        });

        this.airlineList.filter(function (el) {
          if (el.A.toLowerCase().indexOf(keywordEntered.toLowerCase()) >= 0) {
            newData.push({
              "code": el.C,
              "label": el.A
            });
          }
        });
        if (newData.length > 0) {
          this.Airlineresults = newData;
          this.airlineautoCompleteProgress = true;
          this.airlineloading = false;
          this.selectedSrc = newData[0];
        }
        else {
          this.Airlineresults = [];
          this.airlineautoCompleteProgress = false;
          this.airlineloading = false;
          this.selectedSrc = {}
        }
        event.target.focus();
      }
      else {
        this.Airlineresults = [];
        this.airlineautoCompleteProgress = false;
        this.airlineloading = false;
      }
    }, 100),
    onSelectedAirline: function (code, label, type) {
      this.airlineautoCompleteProgress = true;
      this.airlineloading = false;
      this.airlineserch = label;
      this.airlineserchCode = code;
      this.Airlineresults = [];
      this.preferAirline = code;
      this.fareIsCalculated = false;

      if (type == undefined) {
        $("#booking-pnr").focus();
      }
    },
  },
  watch: {
    $data: {
      handler: function () {
        this.isUpdated = true;
      },
      deep: true
    },
    airlineserch: function () {
      if (this.airlineserch == "") {
        this.airlineserchCode = "";
      }
    }

  },
  computed: {}
});