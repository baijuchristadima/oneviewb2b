function paymentManager(paymentDetails, returnURL) {

  var hubUrl = vueCommonStore.state.hubUrls.hubConnection.baseUrl + vueCommonStore.state.hubUrls.hubConnection.ipAddress;
  var reNewTokenUrl = vueCommonStore.state.hubUrls.hubConnection.hubServices.reNewToken;
  axios.get(hubUrl + reNewTokenUrl, {
    headers: {
      "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
    }
  }).then(function (response) {
    var newtoken = response.headers.access_token;

    if (newtoken != null) {
      console.log(window.localStorage.getItem("accessToken"));
      var sessionURL = hubUrl + '/payment/';

      axios.get(sessionURL, {
        headers: {
          "Authorization": "Bearer " + newtoken
        }
      }).then((response) => {
        var paymentCredentials = response.data;
        var cartId = paymentDetails.cartID;

        if (cartId == "") {

          axios.get(sessionURL + paymentDetails.bookingReference, {
            headers: {
              Authorization: "Bearer " + newtoken
            }
          }).then((response) => {
            cartId = response.data.cartId;
            pgWay(newtoken, paymentDetails, cartId, paymentCredentials, returnURL);
          }).catch((error) => {
            alertify.alert("Error on getting cart ID");
            console.error(error);
          });

        } else {
          pgWay(newtoken, paymentDetails, cartId, paymentCredentials, returnURL);
        }
      }).catch((error) => {
        alertify.alert("Error on getting credentails");
        console.error("Error on getting credentails");
        console.error(error);
      });
    }
  }).catch(function (error) {
    console.log('Error on Authentication');
  });

};

function pgWay(accessToken, paymentDetails, cartId, paymentCredentials, returnURL) {

  var hubUrl = vueCommonStore.state.hubUrls.hubConnection.baseUrl + vueCommonStore.state.hubUrls.hubConnection.ipAddress;
  var paymentGatwayReturnURL = hubUrl + '/paymenturi';
  var pymentGatwayID = 2; //payfort

  if (accessToken) {
    $('#BookingLoader').hide();
    if (paymentDetails.totalAmount) {
      if (paymentCredentials.length > 0) {
        switch (paymentDetails.currentPayGateways[0].id) {
          case 2: //PayFort
            var CredentiallistTop = paymentCredentials[0].credentialList;
            var payfortcommand = CredentiallistTop.filter((x) => {
              return x.key == "payfortcommand";
            })[0].value;
            var payfortactlink = CredentiallistTop.filter((x) => {
              return x.key == "payfortactlink";
            })[0].value;
            var payfortaccesscode = CredentiallistTop.filter((x) => {
              return x.key == "payfortaccesscode";
            })[0].value;
            var payfortlanguage = CredentiallistTop.filter((x) => {
              return x.key == "payfortlanguage";
            })[0].value;
            var payfortmerchantidentifier = CredentiallistTop.filter((x) => {
              return x.key == "payfortmerchantidentifier";
            })[0].value;
            var payfortcurrency = CredentiallistTop.filter((x) => {
              return x.key == "payfortcurrency";
            })[0].value;
            var PayfortSHARequestPhrase = CredentiallistTop.filter((x) => {
              return x.key == "PayfortSHARequestPhrase";
            })[0].value;
            var payfortCurrencyMultiplier = CredentiallistTop.filter((x) => {
              return x.key == "payfortCurrencyMultiplier";
            })[0].value;
            // var payfortcurrency = localStorage.selectedCurrency;
            // var PayfortSHAResponsePhrase = CredentiallistTop.filter((x) => { return x.key == "PayfortSHAResponsePhrase"; })[0].value;
            // var integrationtype = CredentiallistTop.filter((x) => { return x.key == "integrationtype"; })[0].value;
            // var MerchantPageUrl = CredentiallistTop.filter((x) => { return x.key == "MerchantPageUrl"; })[0].value;
            var payfortreturl = paymentGatwayReturnURL + '?paymentGwayId=' + pymentGatwayID + '&bookingAmount=' + paymentDetails.totalAmount + '&returnUrl=' + returnURL + '&cartId=' + cartId + '&token=' + accessToken;

            var totalpayAmount = parseFloat(paymentDetails.totalAmount);
            totalpayAmount = totalpayAmount.toFixed(2);
            totalpayAmount = totalpayAmount.toString().replace('.', '');

            var signature = signatureGenerator(payfortaccesscode, payfortlanguage, payfortmerchantidentifier, cartId, payfortcommand, totalpayAmount, payfortcurrency, paymentDetails.customerEmail, payfortreturl, PayfortSHARequestPhrase);
            var formString = outformGenerator(payfortactlink, payfortcommand, payfortaccesscode, payfortmerchantidentifier, totalpayAmount, payfortcurrency, payfortlanguage, paymentDetails.customerEmail, signature, cartId, payfortreturl);
            try {
              $('#payformbind').html(formString);
            } catch (err) {}
            setTimeout(function () {

              $("#btnGoToPG").click();
              console.log(window.localStorage.getItem("accessToken"));

            }, 10);
            break;
          case 13: // EPG
            var request = {
              amount: paymentDetails.totalAmount,
              currency: paymentDetails.currency?paymentDetails.currency:vueCommonStore.state.agencyNode.loginNode.currency || "USD",
              cartId: cartId,
              orderName: "PayBill",
              redirectUrl: returnURL
            }
            var pgFAB = vueCommonStore.state.hubUrls.hubConnection.hubServices.pgFAB;
            axios.post(hubUrl + pgFAB, request, {
              headers: {
                'Authorization': 'Bearer ' + window.localStorage.getItem("accessToken")
              }
            }).then(function (response) {
              console.log("FB REQ:", response)
              if (response.data.paymentUrl) {
                  var formString = '<form action="' + response.data.paymentUrl + '" method="post">' +
                    '<input type="hidden" name="TransactionID" value="' + response.data.transactionID + '"/>' +
                    '<input id="btnGoToPG" type="submit" value="Submit"></form>';

                  try {
                    $('#payformbind').html(formString);
                  } catch (err) {}
                  setTimeout(function () {

                    $("#btnGoToPG").click();
                  }, 10);
              } else {
                console.log('Error on Payment Gateway Hub - ' + response.data.message);
              }
              console.log(response);
              return response.data;
            }).catch(function (error) {
              console.log('Error on Payment Gateway Hub - ' + error);
              return error;
            });
            break;
          default:
            break;
        }

      }

    }
  }
}

function signatureGenerator(payfortaccesscode, payfortlanguage, payfortmerchantidentifier, bookrefnum, payfortcommand, totpayamount, payfortcurrency, customeremail, payfortreturl, SHARequestPhrase) {
  var text = SHARequestPhrase + "access_code=" + payfortaccesscode + "amount=" + totpayamount + "command=" + payfortcommand + "currency=" + payfortcurrency + "customer_email=" + customeremail + "language=" + payfortlanguage + "merchant_identifier=" + payfortmerchantidentifier + "merchant_reference=" + bookrefnum + "return_url=" + payfortreturl + SHARequestPhrase;
  return hash = a(text);
};

function outformGenerator(payfortactlink, payfortcommand, payfortaccesscode, payfortmerchantidentifier, totpayamount, payfortcurrency, payfortlanguage, customeremail, signature, bookrefnum, payfortreturl) {

  return "<form method=\"post\" action=" + payfortactlink +
    " id=\"form1\" name=\"form1\">" +
    "<input type=\"hidden\" name=\"command\" value='" + payfortcommand + "' id=\"command\" />" +
    "<input type=\"hidden\" name=\"access_code\" value='" + payfortaccesscode + "' id=\"access_code\" />" +
    "<input type=\"hidden\" name=\"merchant_identifier\" value='" + payfortmerchantidentifier + "' id=\"merchant_identifier\" />" +
    "<input type=\"hidden\" name=\"amount\" value='" + totpayamount + "' id=\"amount\" />" +
    "<input type=\"hidden\" name=\"currency\" value='" + payfortcurrency + "' id=\"currency\" />" +
    "<input type=\"hidden\" name=\"language\" value='" + payfortlanguage + "' id=\"language\" />" +
    "<input type=\"hidden\" name=\"customer_email\" value='" + customeremail + "' id=\"customeremail\" />" +
    "<input type=\"hidden\" name=\"signature\" value='" + signature + "' id=\"signature\" />" +
    "<input type=\"hidden\" name=\"merchant_reference\" value='" + bookrefnum + "' id=\"merchant_reference\" />" +
    "<input type=\"hidden\" name=\"return_url\" value='" + payfortreturl + "' id=\"return_url\" />" +
    "<input type=\"submit\" value=\"PAY\" id=\"btnGoToPG\"  ref=\"payBtn\"  style='display:none;' /> </form>";
};

function a(b) {
  function c(a, b) {
    return a >>> b | a << 32 - b
  }
  for (var d, e, f = Math.pow, g = f(2, 32), h = "length", i = "", j = [], k = 8 * b[h], l = a.h = a.h || [], m = a.k = a.k || [], n = m[h], o = {}, p = 2; 64 > n; p++)
    if (!o[p]) {
      for (d = 0; 313 > d; d += p) o[d] = p;
      l[n] = f(p, .5) * g | 0, m[n++] = f(p, 1 / 3) * g | 0
    } for (b += "\x80"; b[h] % 64 - 56;) b += "\x00";
  for (d = 0; d < b[h]; d++) {
    if (e = b.charCodeAt(d), e >> 8) return;
    j[d >> 2] |= e << (3 - d) % 4 * 8
  }
  for (j[j[h]] = k / g | 0, j[j[h]] = k, e = 0; e < j[h];) {
    var q = j.slice(e, e += 16),
      r = l;
    for (l = l.slice(0, 8), d = 0; 64 > d; d++) {
      var s = q[d - 15],
        t = q[d - 2],
        u = l[0],
        v = l[4],
        w = l[7] + (c(v, 6) ^ c(v, 11) ^ c(v, 25)) + (v & l[5] ^ ~v & l[6]) + m[d] + (q[d] = 16 > d ? q[d] : q[d - 16] + (c(s, 7) ^ c(s, 18) ^ s >>> 3) + q[d - 7] + (c(t, 17) ^ c(t, 19) ^ t >>> 10) | 0),
        x = (c(u, 2) ^ c(u, 13) ^ c(u, 22)) + (u & l[1] ^ u & l[2] ^ l[1] & l[2]);
      l = [w + x | 0].concat(l), l[4] = l[4] + w | 0
    }
    for (d = 0; 8 > d; d++) l[d] = l[d] + r[d] | 0
  }
  for (d = 0; 8 > d; d++)
    for (e = 3; e + 1; e--) {
      var y = l[d] >> 8 * e & 255;
      i += (16 > y ? 0 : "") + y.toString(16)
    }
  return i
};

function PayVThree(pgId, bkRefNo, returnUrl) {
  var huburl = vueCommonStore.state.hubUrls.hubConnection.baseUrl;
  var portno = vueCommonStore.state.hubUrls.hubConnection.ipAddress;
  var pgGetPostData = vueCommonStore.state.hubUrls.hubConnection.hubServices.pgGetPostData;
  var currenttoken = localStorage.accessToken;
  var rq = {
    "bookingId": bkRefNo,
    "paymentGatwayId": pgId,
    "returnUrl": returnUrl
  }

  return axios.post(huburl + portno + pgGetPostData, rq, {
    headers: {
      'Authorization': 'Bearer ' + currenttoken
    }
  }).then(function (response) {
    localStorage.accessToken = response.headers.access_token;
    console.log(response);
    return response.data;
  }).catch(function (error) {
    console.log('Error on Payment Gateway Hub - ' + error);
    return error;
  });
}

function GenSignature(plainStr, sigType, paymentGatwayId) {
  var huburl = vueCommonStore.state.hubUrls.hubConnection.baseUrl;
  var portno = vueCommonStore.state.hubUrls.hubConnection.ipAddress;
  var pgGetSignature = vueCommonStore.state.hubUrls.hubConnection.hubServices.pgGetSignature;
  var currenttoken = localStorage.accessToken;
  var rq = {
    "signature": plainStr,
    "type": sigType,
    "paymentGatwayId": paymentGatwayId
  }

  return axios.post(huburl + portno + pgGetSignature, rq, {
    headers: {
      'Authorization': 'Bearer ' + currenttoken
    }
  }).then(function (response) {
    localStorage.accessToken = response.headers.access_token;
    console.log(response);
    return response.data;
  }).catch(function (error) {
    console.log('Error on Payment Gateway Hub - ' + error);
    return error;
  });
}