Vue.component('language-i18n', {
    template: `
  <div class="lang lag" style="padding:20px 2px;" v-if="languages.length>0">
    <ul>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="dropdown09" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></span>{{selectedLanguage.name}}</a>
        <div class="nav-link dropdown-toggle dropdown-menu" aria-labelledby="dropdown09">
          <a href="#" class="dropdown-item"  v-for="item in languages" @click.prevent="onSelected(item)">{{item.name}}</a>
        </div>
      </li>
    </ul>
  </div>`,
    data: function () {
        return {
            commonStore: vueCommonStore.state,
            languages: [],
            selectedLanguage: {},
            languageLoaded: []
        }
    },
    methods: {
        setLocalLanguage: function (agencyCode, languageCode) {
            var vm = this;
            agencyCode = "AGY75";
            var hubUrls = HubServiceUrls;
            var hubUrl = hubUrls.hubConnection.cmsUrl;
            var port = hubUrls.hubConnection.ipAddress;
            var baseUrl = hubUrl + port + '/persons/source?path=/B2B/AdminPanel/CMS/' + agencyCode;
            var headers = { headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': languageCode } };
            var cmsUrls = [];

            cmsUrls.push(baseUrl + '/Template/Master Page/Master Page/Master Page.ftl');

            i18n.locale = languageCode;

            if (this.languageLoaded.indexOf(languageCode) == -1) {
                this.languageLoaded.push(languageCode)
                var pagePath = window.location.pathname.toString();
                var translationsData = {};

                //Common CMS
                switch (pagePath) {
                    case "/search.html":
                        cmsUrls.push(baseUrl + '/Template/Search Page/Search Page/Search Page.ftl');
                        break;
                    case "/profile.html":
                        cmsUrls.push(baseUrl + '/Template/My Profile/My Profile/My Profile.ftl');
                        break;
                    case "/searchbookings.html":
                    case "/consolidatedbookings.html":
                    case "/searchofflinebookings.html":
                        cmsUrls.push(baseUrl + '/Template/Search Booking Page/Search Booking Page/Search Booking Page.ftl');
                        break;
                    case "/Hotels/hotelresults.html":
                        cmsUrls.push(baseUrl + '/Template/Search Page/Search Page/Search Page.ftl');
                        cmsUrls.push(baseUrl + '/Template/HotelResults Page/HotelResults Page/HotelResults Page.ftl');
                        break;
                    case "/Hotels/hotelpax.html":
                        cmsUrls.push(baseUrl + '/Template/HotelPax Page/HotelPax Page/HotelPax Page.ftl');
                        break;
                    case "/Hotels/hotelconfirmation.html":
                        cmsUrls.push(baseUrl + '/Template/HotelConfirmation Page/HotelConfirmation Page/HotelConfirmation Page.ftl');
                        break;
                    default:
                        break;

                }

                axios.all(cmsUrls.map(function (url) { return axios.get(url, headers); }))
                    .then(axios.spread(function () {
                        for (var k = 0; k < arguments.length; k++) {
                            console.log(arguments[k]);
                            var area_List = arguments[k].data.area_List
                            if (area_List.length > 0) {
                                var areaTemp = {};
                                for (var i = 0; i < area_List.length; i++) {
                                    var objKey = Object.keys(area_List[i])[0].split(' ').join('_');
                                    areaTemp[objKey] = {};
                                    var area = pluck(objKey, area_List);
                                    if (area) {
                                        areaTemp[objKey] = {};
                                        for (var j = 0; j < area[0].component.length; j++) {
                                            var name = area[0].component[j].name.split(' ').join('_');
                                            areaTemp[objKey][name] = pluckcom(name, area[0].component);
                                        }
                                    }
                                }
                            }
                            Object.assign(translationsData, areaTemp);
                        }

                        //Agency Specific CMS
                        var pagePath = window.location.pathname.toString();

                        var baseUrlLogined = hubUrl + port + '/persons/source?path=/B2B/AdminPanel/CMS/' + vm.commonStore.agencyNode.loginNode.code;

                        var newsAndNoticeUrl = '/Template/News and Notice/News and Notice/News and Notice.ftl';
                        var baseUrlNewsEvent
                        var homeNode;
                        try {
                            axios.get(baseUrlLogined + newsAndNoticeUrl, headers).then(function (response) {
                                var respo = response.data.area_List;
                                var data = pluckcom("Agency_Info", respo);
                                var data2 = getAllMapData(data.component);
                                homeNode = data2.Home_Node;
                                if (homeNode) {
                                    baseUrlNewsEvent = hubUrl + port + '/persons/source?path=/B2B/AdminPanel/CMS/' + homeNode;
                                }
                                else {
                                    baseUrlNewsEvent = hubUrl + port + '/persons/source?path=/B2B/AdminPanel/CMS/AGY' + vm.commonStore.agencyNode.loginNode.solutionId;
                                }
                                // var newsAndNoticeUrl = '/Template/News and Notice/News and Notice/News and Notice.ftl';
                                axios.get(baseUrlNewsEvent + newsAndNoticeUrl, headers).then(function (response) {
                                    var area_List = response.data.area_List;
                                    Object.assign(translationsData, vm.processCmsData(area_List));
                                    vm.$nextTick(function () {
                                        $(document).ready(function () {
                                            if (vm.commonStore.commonRoles.hasNoticeBoard && window.localStorage.getItem('popState') != 'shown') {
                                                window.localStorage.setItem('popState', 'shown')
                                                $("#notice").modal('show');
                                            }
                                            if (vm.commonStore.commonRoles.hasNewsLetter) {
                                                $('#newsTicker1').breakingNews();
                                            }
                                        });
                                    });
                                    i18n.setLocaleMessage(languageCode, translationsData);

                                }).catch(function (error) {
                                    console.log('Error on getting CMS');
                                })

                            }).catch(function (error) {
                                console.log('Error getting CMS')
                                baseUrlNewsEvent = hubUrl + port + '/persons/source?path=/B2B/AdminPanel/CMS/AGY' + vm.commonStore.agencyNode.loginNode.solutionId;
                                axios.get(baseUrlNewsEvent + newsAndNoticeUrl, headers).then(function (response) {
                                    var area_List = response.data.area_List;
                                    Object.assign(translationsData, vm.processCmsData(area_List));
                                    vm.$nextTick(function () {
                                        $(document).ready(function () {
                                            if (vm.commonStore.commonRoles.hasNoticeBoard && window.localStorage.getItem('popState') != 'shown') {
                                                window.localStorage.setItem('popState', 'shown')
                                                $("#notice").modal('show');
                                            }
                                            if (vm.commonStore.commonRoles.hasNewsLetter) {
                                                $('#newsTicker1').breakingNews();
                                            }
                                        });
                                    });
                                    i18n.setLocaleMessage(languageCode, translationsData);

                                })
                            })

                        } catch (err) {
                            console.log(err)
                        }


                        var baseUrlPerAgency = hubUrl + port + '/persons/source?path=/B2B/AdminPanel/CMS/AGY' + vm.commonStore.agencyNode.loginNode.solutionId;

                        if (pagePath == "/Hotels/hotelconfirmation.html") {
                            var bookingVoucherCms = '/Template/Hotel Booking Voucher/Hotel Booking Voucher/Hotel Booking Voucher.ftl';
                            axios.get(baseUrlPerAgency + bookingVoucherCms, headers).then(function (response) {
                                var area_List = response.data.area_List;
                                Object.assign(translationsData, vm.processCmsData(area_List));

                            }).catch(function (error) {
                                console.log('Error on getting CMS');
                            }).finally(function () {
                                // all requests are now complete
                                // i18n.setLocaleMessage(languageCode, translationsData);
                            });
                        }

                    }));

            }

            try {
                if (languageCode == 'ar') {
                    //Alertify global settings
                    alertify.confirm().set('labels', { ok: 'موافق', cancel: 'إلغاء' });
                    alertify.alert().set('label', 'موافق');

                    //Hotels specific settings

                    if (window.location.pathname.indexOf("hotelresults.html") == -1 &&
                        window.location.pathname.indexOf("search.html") == -1) {
                        $("#txtCheckInDate").datepicker("option", $.datepicker.regional['ar']);
                        $("#txtCheckOutDate").datepicker("option", $.datepicker.regional['ar']);
                    }

                } else {
                    //Alertify global settings
                    alertify.confirm().set('labels', { ok: 'Ok', cancel: 'Cancel' });
                    alertify.alert().set('label', 'Ok');

                    //Hotels specific settings
                    if (window.location.pathname.indexOf("hotelresults.html") == -1 &&
                        window.location.pathname.indexOf("search.html") == -1) {
                        $("#txtCheckInDate").datepicker("option", $.datepicker.regional['en-GB']);
                        $("#txtCheckOutDate").datepicker("option", $.datepicker.regional['en-GB']);
                    }
                }

            } catch (error) { }
        },
        onSelected: function (item) {
            var vm = this;
            vm.selectedLanguage = item;
            vm.commonStore.selectedLanguage = item;
            window.localStorage.setItem("languageI18n", JSON.stringify(item));
            var agencyNode = window.localStorage.getItem("agencyNode");
            if (agencyNode) {
                agencyCode = JSON.parse(atob(agencyNode)).loginNode.code;
                vm.setLocalLanguage(agencyCode, vm.selectedLanguage.code);
            }
        },
        processCmsData: function (data) {
            var areaTemp = {};
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    var objKey = Object.keys(data[i])[0].split(' ').join('_');
                    areaTemp[objKey] = {};
                    var area = pluck(objKey, data);
                    if (area) {
                        areaTemp[objKey] = {};
                        for (var j = 0; j < area[0].component.length; j++) {
                            var name = area[0].component[j].name.split(' ').join('_');
                            areaTemp[objKey][name] = pluckcom(name, area[0].component);
                        }
                    }
                }
            }
            return areaTemp;
        }
    },
    created: function () {
        var vm = this;
        var hubUrls = HubServiceUrls;
        var hubUrl = hubUrls.hubConnection.cmsUrl;
        var port = hubUrls.hubConnection.ipAddress;
        var agencyCode = "";
        var agencyNode = window.localStorage.getItem("agencyNode");
        if (agencyNode) {
            agencyCode = JSON.parse(atob(agencyNode)).loginNode.code;
            var requrl = "/templates?path=/B2B/AdminPanel/CMS/" + agencyCode + "/Template/Home/Home";
            axios.get(hubUrl + port + requrl, {
                headers: {
                    "Content-Type": "application/json;charset=UTF-8"
                }
            }).then((response) => {
                var languages = response.data;
                if (languages) {
                    languages = languages.filter(function (item) {
                        return item.indexOf("B2B/AdminPanel") !== 0;
                    });

                    languages.map(function (value, key) {
                        var langauge = value.split('.')[0].split('_')[1];
                        $.getJSON('/Resources/language/languages.json', function (json) {
                            var lang = json.Languages.find(o => o.code.toLowerCase() === langauge.toLowerCase());
                            if (typeof lang !== "undefined") {
                                vm.languages.push({ 'code': lang.code, 'name': lang.nativeName });
                            }
                        })
                    })

                    var b, d = "";

                    d = JSON.parse(window.localStorage.getItem("languageI18n"));
                    if (d === null) {
                        window.localStorage.setItem("languageI18n", JSON.stringify({
                            code: "en",
                            name: "English"
                        }));
                        vm.selectedLanguage = b = { code: "en", name: "English" };
                    } else {
                        b = vm.selectedLanguage = d;
                    }
                    vm.commonStore.selectedLanguage = b;

                    vm.setLocalLanguage(agencyCode, vm.selectedLanguage.code);
                }
            }).catch((err) => {
                console.log("There is no configured language.");
            });
        }
    }
});

Vue.component("curreny-i18n", {
    template: `
  <div class="currency lag" style="padding:20px 2px;"  v-if="currenciesParsed.length>0">
    <ul>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" id="dropdown09" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-html="selectedCurrency.dataContent"></a>
        <div class="nav-link dropdown-toggle dropdown-menu" aria-labelledby="dropdown09">
          <a href="#" class="dropdown-item"  v-for="item in currenciesParsed" @click.prevent="onSelected(item)" v-html="item.dataContent"></a>
        </div>
      </li>
    </ul>
  </div>
  `,
    data: function () {
        return {
            commonStore: vueCommonStore.state,
            currencies: [
                { text: 'AED', desc: 'UAE Dirham', value: 'AED', dataContent: '<span class="flag-icon flag-icon-ae"></span> AED' },
                { text: 'AFN', desc: 'Afghanistan Afghani ', value: 'AFN', dataContent: '<span class="flag-icon flag-icon-af"></span> AFN' },
                { text: 'ALL', desc: 'Albania Lek ', value: 'ALL', dataContent: '<span class="flag-icon flag-icon-al"></span> ALL' },
                { text: 'AMD', desc: 'Armenia Dram ', value: 'AMD', dataContent: '<span class="flag-icon flag-icon-am"></span> AMD' },
                { text: 'ANG', desc: 'Netherlands Antilles ', value: 'ANG', dataContent: '<span class="flag-icon flag-icon-nl"></span> ANG' },
                { text: 'AOA', desc: 'Angola Kwanza ', value: 'AOA', dataContent: '<span class="flag-icon flag-icon-ao"></span> AOA' },
                { text: 'ARS', desc: 'Argentina Peso ', value: 'ARS', dataContent: '<span class="flag-icon flag-icon-ar"></span> ARS' },
                { text: 'AUD', desc: 'Australia Dollar ', value: 'AUD', dataContent: '<span class="flag-icon flag-icon-au"></span> AUD' },
                { text: 'AWG', desc: 'Aruba Guilder ', value: 'AWG', dataContent: '<span class="flag-icon flag-icon-aw"></span> AWG' },
                { text: 'AZN', desc: 'Azerbaijan New Manat ', value: 'AZN', dataContent: '<span class="flag-icon flag-icon-az"></span> AZN' },
                { text: 'BAM', desc: 'Bosnia and Herzegovi', value: 'BAM', dataContent: '<span class="flag-icon flag-icon-ba"></span> BAM' },
                { text: 'BBD', desc: 'Barbados Dollar', value: 'BBD', dataContent: '<span class="flag-icon flag-icon-bb"></span> BBD' },
                { text: 'BDT', desc: 'Bangladesh Taka', value: 'BDT', dataContent: '<span class="flag-icon flag-icon-bd"></span> BDT' },
                { text: 'BGN', desc: 'Bulgaria Lev', value: 'BGN', dataContent: '<span class="flag-icon flag-icon-bg"></span> BGN' },
                { text: 'BHD', desc: 'Bahrain Dinar', value: 'BHD', dataContent: '<span class="flag-icon flag-icon-bh"></span> BHD' },
                { text: 'BIF', desc: 'Burundi Franc', value: 'BIF', dataContent: '<span class="flag-icon flag-icon-bi"></span> BIF' },
                { text: 'BMD', desc: 'Bermuda Dollar', value: 'BMD', dataContent: '<span class="flag-icon flag-icon-bm"></span> BMD' },
                { text: 'BND', desc: 'Brunei Darussalam Do', value: 'BND', dataContent: '<span class="flag-icon flag-icon-bn"></span> BND' },
                { text: 'BOB', desc: 'Bolivian boliviano', value: 'BOB', dataContent: '<span class="flag-icon flag-icon-b0"></span> BOB' },
                { text: 'BRL', desc: 'Brazil Real', value: 'BRL', dataContent: '<span class="flag-icon flag-icon-br"></span> BRL' },
                { text: 'BSD', desc: 'Bahamas Dollar', value: 'BSD', dataContent: '<span class="flag-icon flag-icon-bs"></span> BSD' },
                { text: 'BTN', desc: 'Bhutan Ngultrum', value: 'BTN', dataContent: '<span class="flag-icon flag-icon-bt"></span> BTN' },
                { text: 'BWP', desc: 'Botswana Pula', value: 'BWP', dataContent: '<span class="flag-icon flag-icon-bw"></span> BWP' },
                { text: 'BYN', desc: 'Belarus Ruble', value: 'BYN', dataContent: '<span class="flag-icon flag-icon-by"></span> BYN' },
                { text: 'BZD', desc: 'Belize Dollar', value: 'BZD', dataContent: '<span class="flag-icon flag-icon-bz"></span> BZD' },
                { text: 'CAD', desc: 'Canada Dollar', value: 'CAD', dataContent: '<span class="flag-icon flag-icon-ca"></span> CAD' },
                { text: 'CDF', desc: 'Congo/Kinshasa Franc', value: 'CDF', dataContent: '<span class="flag-icon flag-icon-ca"></span> CDF' },
                { text: 'CHF', desc: 'Switzerland Franc', value: 'CHF', dataContent: '<span class="flag-icon flag-icon-ch"></span> CHF' },
                { text: 'CLP', desc: 'Chile Peso', value: 'CLP', dataContent: '<span class="flag-icon flag-icon-cl"></span> CLP' },
                { text: 'CNY', desc: 'China Yuan Renminbi', value: 'CNY', dataContent: '<span class="flag-icon flag-icon-cn"></span> CNY' },
                { text: 'COP', desc: 'Colombia Peso', value: 'COP', dataContent: '<span class="flag-icon flag-icon-co"></span> COP' },
                { text: 'CRC', desc: 'Costa Rica Colon', value: 'CRC', dataContent: '<span class="flag-icon flag-icon-cr"></span> CRC' },
                { text: 'CUC', desc: 'Cuba Convertible Pes', value: 'CUC', dataContent: '<span class="flag-icon flag-icon-cu"></span> CUC' },
                { text: 'CUP', desc: 'Cuba Peso', value: 'CUP', dataContent: '<span class="flag-icon flag-icon-cu"></span> CUP' },
                { text: 'CVE', desc: 'Cape Verde Escudo', value: 'CVE', dataContent: '<span class="flag-icon flag-icon-cv"></span> CVE' },
                { text: 'CZK', desc: 'Czech Republic Korun', value: 'CZK', dataContent: '<span class="flag-icon flag-icon-cz"></span> CZK' },
                { text: 'DJF', desc: 'Djibouti Franc', value: 'DJF', dataContent: '<span class="flag-icon flag-icon-dj"></span> DJF' },
                { text: 'DKK', desc: 'Denmark Krone', value: 'DKK', dataContent: '<span class="flag-icon flag-icon-dk"></span> DKK' },
                { text: 'DOP', desc: 'Dominican Republic P', value: 'DOP', dataContent: '<span class="flag-icon flag-icon-do"></span> DOP' },
                { text: 'DZD', desc: 'Algeria Dinar', value: 'DZD', dataContent: '<span class="flag-icon flag-icon-dz"></span> DZD' },
                { text: 'EGP', desc: 'Egypt Pound', value: 'EGP', dataContent: '<span class="flag-icon flag-icon-eg"></span> EGP' },
                { text: 'ERN', desc: 'Eritrea Nakfa', value: 'ERN', dataContent: '<span class="flag-icon flag-icon-er"></span> ERN' },
                { text: 'ETB', desc: 'Ethiopia Birr', value: 'ETB', dataContent: '<span class="flag-icon flag-icon-et"></span> ETB' },
                { text: 'EUR', desc: 'Euro Member Countrie', value: 'EUR', dataContent: '<span class="flag-icon flag-icon-fr"></span> EUR' },
                { text: 'FJD', desc: 'Fiji Dollar', value: 'FJD', dataContent: '<span class="flag-icon flag-icon-fj"></span> FJD' },
                { text: 'FKP', desc: 'Falkland Islands (Ma', value: 'FKP', dataContent: '<span class="flag-icon flag-icon-fk"></span> FKP' },
                { text: 'GBP', desc: 'United Kingdom Pound', value: 'GBP', dataContent: '<span class="flag-icon flag-icon-gb"></span> GBP' },
                { text: 'GEL', desc: 'Georgia Lari', value: 'GEL', dataContent: '<span class="flag-icon flag-icon-ge"></span> GEL' },
                { text: 'GGP', desc: 'Guernsey Pound', value: 'GGP', dataContent: '<span class="flag-icon flag-icon-gg"></span> GGP' },
                { text: 'GHS', desc: 'Ghana Cedi', value: 'GHS', dataContent: '<span class="flag-icon flag-icon-gh"></span> GHS' },
                { text: 'GIP', desc: 'Gibraltar Pound', value: 'GIP', dataContent: '<span class="flag-icon flag-icon-gi"></span> GIP' },
                { text: 'GMD', desc: 'Gambia Dalasi', value: 'GMD', dataContent: '<span class="flag-icon flag-icon-gm"></span> GMD' },
                { text: 'GNF', desc: 'Guinea Franc', value: 'GNF', dataContent: '<span class="flag-icon flag-icon-gn"></span> GNF' },
                { text: 'GTQ', desc: 'Guatemala Quetzal', value: 'GTQ', dataContent: '<span class="flag-icon flag-icon-gt"></span> GTQ' },
                { text: 'GYD', desc: 'Guyana Dollar', value: 'GYD', dataContent: '<span class="flag-icon flag-icon-gy"></span> GYD' },
                { text: 'HKD', desc: 'Hong Kong Dollar', value: 'HKD', dataContent: '<span class="flag-icon flag-icon-hk"></span> HKD' },
                { text: 'HNL', desc: 'Honduras Lempira', value: 'HNL', dataContent: '<span class="flag-icon flag-icon-hn"></span> HNL' },
                { text: 'HRK', desc: 'Croatia Kuna', value: 'HRK', dataContent: '<span class="flag-icon flag-icon-hr"></span> HRK' },
                { text: 'HTG', desc: 'Haiti Gourde', value: 'HTG', dataContent: '<span class="flag-icon flag-icon-ht"></span> HTG' },
                { text: 'HUF', desc: 'Hungary Forint', value: 'HUF', dataContent: '<span class="flag-icon flag-icon-hu"></span> HUF' },
                { text: 'IDR', desc: 'Indonesia Rupiah', value: 'IDR', dataContent: '<span class="flag-icon flag-icon-id"></span> IDR' },
                { text: 'ILS', desc: 'Israel Shekel', value: 'ILS', dataContent: '<span class="flag-icon flag-icon-il"></span> ILS' },
                { text: 'IMP', desc: 'Isle of Man Pound', value: 'IMP', dataContent: '<span class="flag-icon flag-icon-im"></span> IMP' },
                { text: 'INR', desc: 'India Rupee', value: 'INR', dataContent: '<span class="flag-icon flag-icon-in"></span> INR' },
                { text: 'IQD', desc: 'Iraq Dinar', value: 'IQD', dataContent: '<span class="flag-icon flag-icon-iq"></span> IQD' },
                { text: 'IRR', desc: 'Iran Rial', value: 'IRR', dataContent: '<span class="flag-icon flag-icon-ir"></span> IRR' },
                { text: 'ISK', desc: 'Iceland Krona', value: 'ISK', dataContent: '<span class="flag-icon flag-icon-is"></span> ISK' },
                { text: 'JEP', desc: 'Jersey Pound', value: 'JEP', dataContent: '<span class="flag-icon flag-icon-je"></span> JEP' },
                { text: 'JMD', desc: 'Jamaica Dollar', value: 'JMD', dataContent: '<span class="flag-icon flag-icon-jm"></span> JMD' },
                { text: 'JOD', desc: 'Jordan Dinar', value: 'JOD', dataContent: '<span class="flag-icon flag-icon-jo"></span> JOD' },
                { text: 'JPY', desc: 'Japan Yen', value: 'JPY', dataContent: '<span class="flag-icon flag-icon-jp"></span> JPY' },
                { text: 'KES', desc: 'Kenya Shilling', value: 'KES', dataContent: '<span class="flag-icon flag-icon-ke"></span> KES' },
                { text: 'KGS', desc: 'Kyrgyzstan Som', value: 'KGS', dataContent: '<span class="flag-icon flag-icon-kg"></span> KGS' },
                { text: 'KHR', desc: 'Cambodia Riel', value: 'KHR', dataContent: '<span class="flag-icon flag-icon-kh"></span> KHR' },
                { text: 'KMF', desc: 'Comoros Franc', value: 'KMF', dataContent: '<span class="flag-icon flag-icon-km"></span> KMF' },
                { text: 'KPW', desc: 'Korea (North) Won', value: 'KPW', dataContent: '<span class="flag-icon flag-icon-kp"></span> KPW' },
                { text: 'KRW', desc: 'Korea (South) Won', value: 'KRW', dataContent: '<span class="flag-icon flag-icon-kr"></span> KRW' },
                { text: 'KWD', desc: 'Kuwait Dinar', value: 'KWD', dataContent: '<span class="flag-icon flag-icon-kw"></span> KWD' },
                { text: 'KYD', desc: 'Cayman Islands Dolla', value: 'KYD', dataContent: '<span class="flag-icon flag-icon-ky"></span> KYD' },
                { text: 'KZT', desc: 'Kazakhstan Tenge', value: 'KZT', dataContent: '<span class="flag-icon flag-icon-kz"></span> KZT' },
                { text: 'LAK', desc: 'Laos Kip', value: 'LAK', dataContent: '<span class="flag-icon flag-icon-la"></span> LAK' },
                { text: 'LBP', desc: 'Lebanon Pound', value: 'LBP', dataContent: '<span class="flag-icon flag-icon-lb"></span> LBP' },
                { text: 'LKR', desc: 'Sri Lanka Rupee', value: 'LKR', dataContent: '<span class="flag-icon flag-icon-lk"></span> LKR' },
                { text: 'LRD', desc: 'Liberia Dollar', value: 'LRD', dataContent: '<span class="flag-icon flag-icon-lr"></span> LRD' },
                { text: 'LSL', desc: 'Lesotho Loti', value: 'LSL', dataContent: '<span class="flag-icon flag-icon-ls"></span> LSL' },
                { text: 'LYD', desc: 'Libya Dinar', value: 'LYD', dataContent: '<span class="flag-icon flag-icon-ly"></span> LYD' },
                { text: 'MAD', desc: 'Morocco Dirham', value: 'MAD', dataContent: '<span class="flag-icon flag-icon-ma"></span> MAD' },
                { text: 'MDL', desc: 'Moldova Leu', value: 'MDL', dataContent: '<span class="flag-icon flag-icon-md"></span> MDL' },
                { text: 'MGA', desc: 'Madagascar Ariary', value: 'MGA', dataContent: '<span class="flag-icon flag-icon-mg"></span> MGA' },
                { text: 'MKD', desc: 'Macedonia Denar', value: 'MKD', dataContent: '<span class="flag-icon flag-icon-mk"></span> MKD' },
                { text: 'MMK', desc: 'Myanmar (Burma) Kyat', value: 'MMK', dataContent: '<span class="flag-icon flag-icon-mm"></span> MMK' },
                { text: 'MNT', desc: 'Mongolia Tughrik', value: 'MNT', dataContent: '<span class="flag-icon flag-icon-mn"></span> MNT' },
                { text: 'MOP', desc: 'Macau Pataca', value: 'MOP', dataContent: '<span class="flag-icon flag-icon-mo"></span> MOP' },
                { text: 'MRO', desc: 'Mauritania Ouguiya', value: 'MRO', dataContent: '<span class="flag-icon flag-icon-mr"></span> MRO' },
                { text: 'MUR', desc: 'Mauritius Rupee', value: 'MUR', dataContent: '<span class="flag-icon flag-icon-mu"></span> MUR' },
                { text: 'MVR', desc: 'Maldives (Maldive Is', value: 'MVR', dataContent: '<span class="flag-icon flag-icon-mv"></span> MVR' },
                { text: 'MWK', desc: 'Malawi Kwacha', value: 'MWK', dataContent: '<span class="flag-icon flag-icon-mw"></span> MWK' },
                { text: 'MXN', desc: 'Mexico Peso', value: 'MXN', dataContent: '<span class="flag-icon flag-icon-mx"></span> MXN' },
                { text: 'MYR', desc: 'Malaysia Ringgit', value: 'MYR', dataContent: '<span class="flag-icon flag-icon-my"></span> MYR' },
                { text: 'MZN', desc: 'Mozambique Metical', value: 'MZN', dataContent: '<span class="flag-icon flag-icon-mz"></span> MZN' },
                { text: 'NAD', desc: 'Namibia Dollar', value: 'NAD', dataContent: '<span class="flag-icon flag-icon-na"></span> NAD' },
                { text: 'NGN', desc: 'Nigeria Naira', value: 'NGN', dataContent: '<span class="flag-icon flag-icon-ng"></span> NGN' },
                { text: 'NIO', desc: 'Nicaragua Cordoba', value: 'NIO', dataContent: '<span class="flag-icon flag-icon-ni"></span> NIO' },
                { text: 'NOK', desc: 'Norway Krone', value: 'NOK', dataContent: '<span class="flag-icon flag-icon-no"></span> NOK' },
                { text: 'NPR', desc: 'Nepal Rupee', value: 'NPR', dataContent: '<span class="flag-icon flag-icon-np"></span> NPR' },
                { text: 'NZD', desc: 'New Zealand Dollar', value: 'NZD', dataContent: '<span class="flag-icon flag-icon-nz"></span> NZD' },
                { text: 'OMR', desc: 'Oman Rial', value: 'OMR', dataContent: '<span class="flag-icon flag-icon-om"></span> OMR' },
                { text: 'PAB', desc: 'Panama Balboa', value: 'PAB', dataContent: '<span class="flag-icon flag-icon-pa"></span> PAB' },
                { text: 'PEN', desc: 'Peru Sol', value: 'PEN', dataContent: '<span class="flag-icon flag-icon-pe"></span> PEN' },
                { text: 'PGK', desc: 'Papua New Guinea Kin', value: 'PGK', dataContent: '<span class="flag-icon flag-icon-pg"></span> PGK' },
                { text: 'PHP', desc: 'Philippines Peso', value: 'PHP', dataContent: '<span class="flag-icon flag-icon-ph"></span> PHP' },
                { text: 'PKR', desc: 'Pakistan Rupee', value: 'PKR', dataContent: '<span class="flag-icon flag-icon-pk"></span> PKR' },
                { text: 'PLN', desc: 'Poland Zloty', value: 'PLN', dataContent: '<span class="flag-icon flag-icon-pl"></span> PLN' },
                { text: 'PYG', desc: 'Paraguay Guarani', value: 'PYG', dataContent: '<span class="flag-icon flag-icon-py"></span> PYG' },
                { text: 'QAR', desc: 'Qatar Riyal', value: 'QAR', dataContent: '<span class="flag-icon flag-icon-qa"></span> QAR' },
                { text: 'RON', desc: 'Romania New Leu', value: 'RON', dataContent: '<span class="flag-icon flag-icon-ro"></span> RON' },
                { text: 'RSD', desc: 'Serbia Dinar', value: 'RSD', dataContent: '<span class="flag-icon flag-icon-rs"></span> RSD' },
                { text: 'RUB', desc: 'Russia Ruble', value: 'RUB', dataContent: '<span class="flag-icon flag-icon-ru"></span> RUB' },
                { text: 'RWF', desc: 'Rwanda Franc', value: 'RWF', dataContent: '<span class="flag-icon flag-icon-rw"></span> RWF' },
                { text: 'SAR', desc: 'Saudi Arabia Riyal', value: 'SAR', dataContent: '<span class="flag-icon flag-icon-sa"></span> SAR' },
                { text: 'SBD', desc: 'Solomon Islands Doll', value: 'SBD', dataContent: '<span class="flag-icon flag-icon-sb"></span> SBD' },
                { text: 'SCR', desc: 'Seychelles Rupee', value: 'SCR', dataContent: '<span class="flag-icon flag-icon-sc"></span> SCR' },
                { text: 'SDG', desc: 'Sudan Pound', value: 'SDG', dataContent: '<span class="flag-icon flag-icon-sd"></span> SDG' },
                { text: 'SEK', desc: 'Sweden Krona', value: 'SEK', dataContent: '<span class="flag-icon flag-icon-se"></span> SEK' },
                { text: 'SGD', desc: 'Singapore Dollar', value: 'SGD', dataContent: '<span class="flag-icon flag-icon-sg"></span> SGD' },
                { text: 'SHP', desc: 'Saint Helena Pound', value: 'SHP', dataContent: '<span class="flag-icon flag-icon-sh"></span> SHP' },
                { text: 'SLL', desc: 'Sierra Leone Leone', value: 'SLL', dataContent: '<span class="flag-icon flag-icon-sl"></span> SLL' },
                { text: 'SOS', desc: 'Somalia Shilling', value: 'SOS', dataContent: '<span class="flag-icon flag-icon-so"></span> SOS' },
                { text: 'SPL', desc: 'Seborga Luigino', value: 'SPL', dataContent: '<span class="flag-icon flag-icon-"></span> SPL' },
                { text: 'SRD', desc: 'Suriname Dollar', value: 'SRD', dataContent: '<span class="flag-icon flag-icon-sr"></span> SRD' },
                { text: 'SSP', desc: 'South Sudanese Pound', value: 'SSP', dataContent: '<span class="flag-icon flag-icon-ss"></span> SSP' },
                { text: 'STD', desc: 'Sao Tome and Princip', value: 'STD', dataContent: '<span class="flag-icon flag-icon-st"></span> STD' },
                { text: 'SVC', desc: 'El Salvador Colon', value: 'SVC', dataContent: '<span class="flag-icon flag-icon-sv"></span> SVC' },
                { text: 'SYP', desc: 'Syria Pound', value: 'SYP', dataContent: '<span class="flag-icon flag-icon-sy"></span> SYP' },
                { text: 'SZL', desc: 'Swaziland Lilangeni', value: 'SZL', dataContent: '<span class="flag-icon flag-icon-sz"></span> SZL' },
                { text: 'THB', desc: 'Thailand Baht', value: 'THB', dataContent: '<span class="flag-icon flag-icon-th"></span> THB' },
                { text: 'TJS', desc: 'Tajikistan Somoni', value: 'TJS', dataContent: '<span class="flag-icon flag-icon-tj"></span> TJS' },
                { text: 'TMT', desc: 'Turkmenistan Manat', value: 'TMT', dataContent: '<span class="flag-icon flag-icon-tm"></span> TMT' },
                { text: 'TND', desc: 'Tunisia Dinar', value: 'TND', dataContent: '<span class="flag-icon flag-icon-tn"></span> TND' },
                { text: 'TOP', desc: 'Tonga Paanga', value: 'TOP ', dataContent: '<span class="flag-icon flag-icon-to"></span> TOP' },
                { text: 'TRY', desc: 'Turkey Lira', value: 'TRY', dataContent: '<span class="flag-icon flag-icon-tr"></span> TRY' },
                { text: 'TTD', desc: 'Trinidad and Tobago', value: 'TTD', dataContent: '<span class="flag-icon flag-icon-tt"></span> TTD' },
                { text: 'TVD', desc: 'Tuvalu Dollar', value: 'TVD', dataContent: '<span class="flag-icon flag-icon-tv"></span> TVD' },
                { text: 'TWD', desc: 'Taiwan New Dollar', value: 'TWD', dataContent: '<span class="flag-icon flag-icon-tw"></span> TWD' },
                { text: 'TZS', desc: 'Tanzania Shilling', value: 'TZS', dataContent: '<span class="flag-icon flag-icon-tz"></span> TZS' },
                { text: 'UAH', desc: 'Ukraine Hryvnia', value: 'UAH', dataContent: '<span class="flag-icon flag-icon-ua"></span> UAH' },
                { text: 'UGX', desc: 'Uganda Shilling', value: 'UGX', dataContent: '<span class="flag-icon flag-icon-ug"></span> UGX' },
                { text: 'USD', desc: 'United States Dollar', value: 'USD', dataContent: '<span class="flag-icon flag-icon-us"></span> USD' },
                { text: 'UYU', desc: 'Uruguay Peso', value: 'UYU', dataContent: '<span class="flag-icon flag-icon-uy"></span> UYU' },
                { text: 'UZS', desc: 'Uzbekistan Som', value: 'UZS', dataContent: '<span class="flag-icon flag-icon-uz"></span> UZS' },
                { text: 'VEF', desc: 'Venezuela Bolivar', value: 'VEF', dataContent: '<span class="flag-icon flag-icon-ve"></span> VEF' },
                { text: 'VND', desc: 'Viet Nam Dong', value: 'VND', dataContent: '<span class="flag-icon flag-icon-vn"></span> VND' },
                { text: 'VUV', desc: 'Vanuatu Vatu', value: 'VUV', dataContent: '<span class="flag-icon flag-icon-vu"></span> VUV' },
                { text: 'WST', desc: 'Samoa Tala', value: 'WST', dataContent: '<span class="flag-icon flag-icon-ws"></span> WST' },
                { text: 'XAF', desc: 'Central Africa CFA fra', value: 'XAF', dataContent: '<span class="flag-icon flag-icon-"></span> XAF' },
                { text: 'XCD', desc: 'East Caribbean Dolla', value: 'XCD', dataContent: '<span class="flag-icon flag-icon-"></span> XCD' },
                { text: 'XOF', desc: 'West African CFA fra', value: 'XOF', dataContent: '<span class="flag-icon flag-icon-cf"></span> XOF' },
                { text: 'XPF', desc: 'CFP Franc', value: 'XPF', dataContent: '<span class="flag-icon flag-icon-pf"></span> XPF' },
                { text: 'YER', desc: 'Yemen Rial', value: 'YER', dataContent: '<span class="flag-icon flag-icon-ye"></span> YER' },
                { text: 'ZAR', desc: 'South Africa Rand', value: 'ZAR', dataContent: '<span class="flag-icon flag-icon-za"></span> ZAR' },
                { text: 'ZMW', desc: 'Zambia Kwacha', value: 'ZMW', dataContent: '<span class="flag-icon flag-icon-zm"></span> ZMW' },
                { text: 'ZWD', desc: 'Zimbabwe Dollar', value: 'ZWD', dataContent: '<span class="flag-icon flag-icon-zw"></span> ZWD' },
                { text: 'ZWL', desc: 'Zimbabwe Dollar', value: 'ZWL', dataContent: '<span class="flag-icon flag-icon-zw"></span> ZWL' },
                { text: 'SUP', desc: 'Supplier Currency', value: 'SUP', dataContent: 'Supplier Currency' }
            ],
            currenciesParsed: [],
            selectedCurrency: [],
        }
    },
    created: function () {
        this.getCurrencyRate();
    },
    methods: {
        onSelected: function (item) {
            var vm = this;
            vm.selectedCurrency = item;
            vm.commonStore.selectedCurrency = item.value;
            vm.commonStore.currencyMultiplier = item.rate;
            window.localStorage.setItem("currencyI18n", JSON.stringify({
                c: item.value,
                m: item.rate,
                b: item.base
            }));
        },
        getCurrencyRate: function () {
            var vm = this;
            var hubUrls = HubServiceUrls;
            var hubUrl = hubUrls.hubConnection.baseUrl;
            var port = hubUrls.hubConnection.ipAddress;
            var agencyCode = "";
            var agencyNode = window.localStorage.getItem("agencyNode");
            if (agencyNode) {
                agencyCode = JSON.parse(atob(agencyNode)).loginNode.code.slice(3);
                var reqUrl = "/currencyrate/" + agencyCode;
                axios.get(hubUrl + port + reqUrl, {
                    headers: {
                        "Content-Type": "application/json"
                    }
                }).then((response) => {
                    if (isNullorUndefined(response) || isNullorUndefined(response.data) || response.data.length <= 0) {
                        var agencyNode = JSON.parse(atob(window.localStorage.getItem("agencyNode")));
                        if (agencyNode) {
                            response = {
                                data: [{
                                    "id": 0,
                                    "fromCurrencyCode": agencyNode.loginNode.currency,
                                    "toCurrencyCode": agencyNode.loginNode.currency,
                                    "buyRate": 1,
                                    "sellRate": 1,
                                    "nodeId": agencyCode,
                                    "edit": false
                                }]
                            };
                        }
                    }
                    var currency = response.data;
                    vm.commonStore.currencyRates = response.data;
                    if (response.data.length > 0) {
                        if (currency) {

                            var b, c, d = "";

                            var tempArr = vm.currencies.find(x => x.value.toUpperCase() === currency[0].toCurrencyCode.toUpperCase());
                            if (vm.currenciesParsed.length == 0) {
                                vm.currenciesParsed.push({ text: tempArr.text, value: tempArr.value, rate: 1, dataContent: tempArr.dataContent, base: currency[0].sellRate });
                            }

                            d = JSON.parse(window.localStorage.getItem("currencyI18n"));
                            if (d === null) {
                                window.localStorage.setItem("currencyI18n", JSON.stringify({
                                    c: currency[0].toCurrencyCode.toUpperCase(),
                                    m: 1,
                                    b: currency[0].sellRate
                                }));
                                b = currency[0].toCurrencyCode.toUpperCase();
                                c = 1;
                                // vm.selectedCurrency = vm.currencies.find(x => x.value.toUpperCase() === currency[0].toCurrencyCode.toUpperCase());
                            } else {
                                b = d.c;
                                c = d.m;
                                // vm.selectedCurrency = vm.currencies.find(x => x.value.toUpperCase() === d.c.toUpperCase());
                                // if (!vm.selectedCurrency) {
                                //     vm.selectedCurrency = vm.currencies.find(x => x.value.toUpperCase() === "SUP");
                                // }
                            }

                            currency.map(function (value) {
                                var temp = vm.currencies.find(x => x.value.toUpperCase() === value.fromCurrencyCode.toUpperCase());
                                if (temp) {
                                    var hasCurrencyColl = vm.currenciesParsed.find(x => x.value.toUpperCase() === temp.value.toUpperCase());
                                    if (isNullorUndefined(hasCurrencyColl) || hasCurrencyColl.length <= 0) {
                                        vm.currenciesParsed.push({ text: temp.text, value: temp.value, rate: value.sellRate, dataContent: temp.dataContent, base: value.sellRate });
                                    }
                                }
                            })

                            if (window.location.pathname.indexOf("bookinginfo.html") != -1 ||
                                window.location.pathname.indexOf("flightresults.html") != -1 ||
                                window.location.pathname.indexOf("travellerdetails.html") != -1 ||
                                window.location.pathname.indexOf("search.html") != -1 ||
                                window.location.pathname.indexOf("searchbookings.html") != -1
                            ) {
                                if (vm.commonStore.commonRoles.hasSupplierCurrency) {
                                    var hasCurrencyColl = vm.currenciesParsed.find(x => x.value.toUpperCase() == "");
                                    if (isNullorUndefined(hasCurrencyColl) || hasCurrencyColl.length <= 0) {
                                        vm.currenciesParsed.push({ text: "SUPPLIER", value: "", rate: 100, dataContent: "Supplier Currency", base: 0 });
                                    }
                                }
                            }

                            //check roles for SUP curr
                            if (d.c == "" && _.find(vm.currenciesParsed, function (e) { return e.text == "SUPPLIER"; }) == undefined) {
                                vm.onSelected(vm.currenciesParsed[0]);
                                //Use of popStateCurrency below indicates that the user was doing initial login.
                            } else if (window.localStorage.getItem('popStateCurrency') != 'shown' && vm.commonStore.commonRoles.hasSupplierCurrency) {
                                var suppCurr = _.find(vm.currenciesParsed, function (e) { return e.text == "SUPPLIER"; })
                                vm.onSelected(suppCurr);
                                window.localStorage.setItem('popStateCurrency', 'shown');
                            } else {
                                vm.selectedCurrency = vm.currencies.find(x => x.value.toUpperCase() === d.c.toUpperCase());
                                if (!vm.selectedCurrency) {
                                    vm.selectedCurrency = vm.currencies.find(x => x.value.toUpperCase() === "SUP");
                                }
                                vm.commonStore.selectedCurrency = b;
                                vm.commonStore.currencyMultiplier = c;
                            }
                            bindCreditLimit();
                        }
                    } else {
                        alertify.alert("Error", "There is no configured currency.");
                    }
                }).catch((err) => {
                    this.getCurrencyRate();
                    //alertify.alert("Error", "There is no configured currency.");
                });
            }
        }
    },
});

Vue.component("quotation-module", {
    data: function () {
        return {
            commonStore: vueCommonStore.state,
            showQuotationListModal: false,
            showQuotationViewModal: false,
            showQuotationDetailsModal: false,
            showQuotationSnippetModal: false,
            viewQuotationItems: {
                services: {
                    flights: [],
                    hotels: [],
                    insuranceTP: [],
                    insuranceWIS: []
                }
            },
            selectedQuotationItems: [],
            allFlightsChecked: false,
            allHotelsChecked: false,
            allInsuranceChecked: false,
            allWISInsuranceChecked: false,
            allQuotationsChecked: false,
            flightsTab: true,
            hotelsTab: true,
            insuranceTab: true,
            WISinsuranceTab: true,
            quoteSnippet: "",
            enableSaveButton: false,
            pendingQuoteId: null,
            quoteEmailTo: "",
            quoteEmailCC: "",
            quoteEmailSubject: ""
        };
    },
    created: function () {

    },
    mounted: function () {
        this.getAllquotationsOnServer();
    },
    methods: {
        onSelected: function (item) {
            var vm = this;
            vm.commonStore.selectedCart = item;
            window.localStorage.setItem("currentCart", item.id);
        },
        addQuotation: function (index) {
            this.commonStore.carts.push({
                    id: uuidv4(),
                    name: "Quotation " + (this.commonStore.carts.length + 1),
                    services: {
                        flights: [],
                        hotels: [],
                        insuranceTP: [],
                        insuranceWIS: []
                    }
                }
            );
            this.enableSaveButton = true;
        },
        removeQuotationInList: function (Qid) {
            var index = this.commonStore.carts.findIndex(function (e) {
                return e.id == Qid;
            });
            if (index > -1) {
                this.commonStore.carts.splice(index, 1);
            }
            this.enableSaveButton = true;
            if (this.commonStore.selectedCart.id == Qid) {
                this.commonStore.selectedCart = this.commonStore.carts[0];
            }
        },
        removeFlightQuotation: function (Fid) {
            var index = this.viewQuotationItems.services.flights.findIndex(function (e) {
                return e.serialNumber == Fid;
            });
            if (index > -1) {
                this.viewQuotationItems.services.flights.splice(index, 1);
            }
            this.updateQuotationOnServer(true);
        },
        removeHotelQuotation: function (Hid) {
            var index = this.viewQuotationItems.services.hotels.findIndex(function (e) {
                return e.serialNumber == Hid;
            });
            if (index > -1) {
                this.viewQuotationItems.services.hotels.splice(index, 1);
            }
            this.updateQuotationOnServer(true);
        },
        removeRoomQuotation: function (Hid, Rid) {
            var Hindex = this.viewQuotationItems.services.hotels.findIndex(function (e) {
                return e.serialNumber == Hid;
            });

            if (Hindex > -1) {
                var Rindex = this.viewQuotationItems.services.hotels[Hindex].options.findIndex(function (e) {
                    return e.serialNumber == Rid;
                });
                if (Rindex > -1) {
                    this.viewQuotationItems.services.hotels[Hindex].options.splice(Rindex, 1);

                    if (this.viewQuotationItems.services.hotels[Hindex].options.length == 0) {
                        this.viewQuotationItems.services.hotels.splice(Hindex, 1);
                    }
                }
            }
            this.updateQuotationOnServer(false);
        },
        removeAllQuotation: function () {
            var vm = this;
            if (vm.allQuotationsChecked) {
                vm.viewQuotationItems.services.flights = [];
                vm.viewQuotationItems.services.hotels = [];
                vm.viewQuotationItems.services.insuranceTP = [];
                vm.viewQuotationItems.services.insuranceWIS = [];
                vm.selectedQuotationItems = [];
            } else {
                vm.selectedQuotationItems.forEach(function (e, k) {
                    var Findex = vm.viewQuotationItems.services.flights.findIndex(function (FQ) {
                        return e == FQ.serialNumber;
                    });
                    var Iindex = vm.viewQuotationItems.services.insuranceTP.findIndex(function (IQ) {
                        return e == IQ.serialNumber;
                    });
                    var WISindex = vm.viewQuotationItems.services.insuranceWIS.findIndex(function (IQ) {
                        return e == IQ.serialNumber;
                    });
                    if (Findex > -1) {
                        vm.viewQuotationItems.services.flights.splice(Findex, 1);
                    } else if (Iindex > -1) {
                        vm.viewQuotationItems.services.insuranceTP.splice(Iindex, 1);
                    } else if (WISindex > -1) {
                        vm.viewQuotationItems.services.insuranceWIS.splice(Iindex, 1);
                    } else{
                        var Hindex = vm.viewQuotationItems.services.hotels.findIndex(function (HQ) {
                            return e == HQ.serialNumber;
                        });
                        if (Hindex > -1) {
                            vm.viewQuotationItems.services.hotels.splice(Hindex, 1);
                        } else {
                            vm.viewQuotationItems.services.hotels.forEach(function (h, i) {
                                var Rindex = h.options.findIndex(function (RQ) {
                                    return e == RQ.serialNumber;
                                });
                                if (Rindex > -1) {
                                    h.options.splice(Rindex, 1);

                                    if (h.options.length == 0) {
                                        vm.viewQuotationItems.services.hotels.splice(i, 1);
                                    }
                                }
                            });

                        }
                    }
                });
                vm.selectedQuotationItems = [];
            }
            this.updateQuotationOnServer(true);
        },
        checkAllFlights: function () {
            var vm = this;
            if (vm.allFlightsChecked) {
                vm.viewQuotationItems.services.flights.forEach(function (allQ) {
                    var index = vm.selectedQuotationItems.findIndex(function (selQ) {
                        return selQ == allQ.serialNumber;
                    });
                    if (index > -1) {
                        vm.selectedQuotationItems.splice(index, 1);
                    }
                });
            } else {
                vm.viewQuotationItems.services.flights.forEach(function (e) {
                    if (vm.selectedQuotationItems.indexOf(e.serialNumber) == -1) {
                        vm.selectedQuotationItems.push(e.serialNumber);
                    }
                });
            }
            vm.allFlightsChecked = !vm.allFlightsChecked;
            vm.allQuotationsChecked = !vm.allQuotationsChecked;
        },
        checkAllHotels: function () {
            var vm = this;
            if (vm.allHotelsChecked) {
                vm.viewQuotationItems.services.hotels.forEach(function (allQ) {
                    allQ.options.forEach(function (room) {
                        var index = vm.selectedQuotationItems.findIndex(function (selQ) {
                            return selQ == room.serialNumber;
                        });
                        if (index > -1) {
                            vm.selectedQuotationItems.splice(index, 1);
                        }
                    });
                });
            } else {
                vm.viewQuotationItems.services.hotels.forEach(function (e) {
                    e.options.forEach(function (room) {
                        if (vm.selectedQuotationItems.indexOf(room.serialNumber) == -1) {
                            vm.selectedQuotationItems.push(room.serialNumber);
                        }
                    });
                });
            }
            vm.allHotelsChecked = !vm.allHotelsChecked;
            vm.allQuotationsChecked = !vm.allQuotationsChecked;
        },
        checkAllInsurance: function () {
            var vm = this;
            if (vm.allInsuranceChecked) {
                vm.viewQuotationItems.services.insuranceTP.forEach(function (allQ) {
                    var index = vm.selectedQuotationItems.findIndex(function (selQ) {
                        return selQ == allQ.serialNumber;
                    });
                    if (index > -1) {
                        vm.selectedQuotationItems.splice(index, 1);
                    }
                });
            } else {
                vm.viewQuotationItems.services.insuranceTP.forEach(function (e) {
                    if (vm.selectedQuotationItems.indexOf(e.serialNumber) == -1) {
                        vm.selectedQuotationItems.push(e.serialNumber);
                    }
                });
            }
            vm.allInsuranceChecked = !vm.allInsuranceChecked;
            vm.allQuotationsChecked = !vm.allQuotationsChecked;
        },
        removeInsuranceQuotation: function (Iid) {
            var index = this.viewQuotationItems.services.insuranceTP.findIndex(function (e) {
                return e.serialNumber == Iid;
            });
            if (index > -1) {
                this.viewQuotationItems.services.insuranceTP.splice(index, 1);
            }
            this.updateQuotationOnServer(true);
        },
        checkAllWISInsurance: function () {
            var vm = this;
            if (vm.allWISInsuranceChecked) {
                vm.viewQuotationItems.services.insuranceWIS.forEach(function (allQ) {
                    var index = vm.selectedQuotationItems.findIndex(function (selQ) {
                        return selQ == allQ.serialNumber;
                    });
                    if (index > -1) {
                        vm.selectedQuotationItems.splice(index, 1);
                    }
                });
            } else {
                vm.viewQuotationItems.services.insuranceWIS.forEach(function (e) {
                    if (vm.selectedQuotationItems.indexOf(e.serialNumber) == -1) {
                        vm.selectedQuotationItems.push(e.serialNumber);
                    }
                });
            }
            vm.allWISInsuranceChecked = !vm.allWISInsuranceChecked;
            vm.allQuotationsChecked = !vm.allQuotationsChecked;
        },
        removeWISInsuranceQuotation: function (Iid) {
            var index = this.viewQuotationItems.services.insuranceWIS.findIndex(function (e) {
                return e.serialNumber == Iid;
            });
            if (index > -1) {
                this.viewQuotationItems.services.insuranceWIS.splice(index, 1);
            }
            this.updateQuotationOnServer(true);
        },
        checkAllQuotationList: function () {
            var vm = this;

            if (vm.allQuotationsChecked) {
                vm.viewQuotationItems.services.flights.forEach(function (allQ) {
                    var index = vm.selectedQuotationItems.findIndex(function (selQ) {
                        return selQ == allQ.serialNumber;
                    });
                    if (index > -1) {
                        vm.selectedQuotationItems.splice(index, 1);
                    }
                });
                vm.viewQuotationItems.services.hotels.forEach(function (allQ) {
                    allQ.options.forEach(function (room) {
                        var index = vm.selectedQuotationItems.findIndex(function (selQ) {
                            return selQ == room.serialNumber;
                        });
                        if (index > -1) {
                            vm.selectedQuotationItems.splice(index, 1);
                        }
                    });
                });
                vm.viewQuotationItems.services.insuranceTP.forEach(function (allQ) {
                    var index = vm.selectedQuotationItems.findIndex(function (selQ) {
                        return selQ == allQ.serialNumber;
                    });
                    if (index > -1) {
                        vm.selectedQuotationItems.splice(index, 1);
                    }
                });
                vm.viewQuotationItems.services.insuranceWIS.forEach(function (allQ) {
                    var index = vm.selectedQuotationItems.findIndex(function (selQ) {
                        return selQ == allQ.serialNumber;
                    });
                    if (index > -1) {
                        vm.selectedQuotationItems.splice(index, 1);
                    }
                });
            } else {
                vm.viewQuotationItems.services.flights.forEach(function (e) {
                    if (vm.selectedQuotationItems.indexOf(e.serialNumber) == -1) {
                        vm.selectedQuotationItems.push(e.serialNumber);
                    }
                });
                vm.viewQuotationItems.services.hotels.forEach(function (e) {
                    e.options.forEach(function (room) {
                        if (vm.selectedQuotationItems.indexOf(room.serialNumber) == -1) {
                            vm.selectedQuotationItems.push(room.serialNumber);
                        }
                    });
                });
                vm.viewQuotationItems.services.insuranceTP.forEach(function (e) { 
                    if (vm.selectedQuotationItems.indexOf(e.serialNumber)==-1) {
                        vm.selectedQuotationItems.push(e.serialNumber); 
                    }
                });
                vm.viewQuotationItems.services.insuranceWIS.forEach(function (e) { 
                    if (vm.selectedQuotationItems.indexOf(e.serialNumber)==-1) {
                        vm.selectedQuotationItems.push(e.serialNumber); 
                    }
                });
            }

            vm.allQuotationsChecked = !vm.allQuotationsChecked;
            vm.allFlightsChecked = vm.allHotelsChecked = vm.allQuotationsChecked;
        },
        getdatetimeformated: function (datetime, inputFormat, outputFormat) {
            return moment(datetime, inputFormat).format(outputFormat);
        },
        gettimeformated: function (time, inputFormat, houroutputFormat, houraddl, minoutputFormat, minaddl) {
            var hour = moment(time, inputFormat).format(houroutputFormat).toString();
            var min = moment(time, inputFormat).format(minoutputFormat).toString();
            var formattedtime = hour + houraddl + min + minaddl;
            return formattedtime;
        },
        openViewQuotationContent: function () {
            var vm = this;
            this.showQuotationDetailsModal = true;
            setTimeout(function () {
                $('#summernote').summernote({
                    disableGrammar: true,
                    spellCheck: false,
                    toolbar: []
                    // toolbar: [
                    //     // [groupName, [list of button]]
                    //     ['style', ['bold', 'italic', 'underline', 'clear']],
                    //     ['font', ['strikethrough', 'superscript', 'subscript']],
                    //     ['fontsize', ['fontsize']],
                    //     ['color', ['color']],
                    //     ['para', ['ul', 'ol', 'paragraph']],
                    //     ['height', ['height']]
                    // ]
                });
                var agencyFolderName = "";
                axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                    for (var index = 0; index < response.data.length; index++) {
                        var domain = response.data[index].domain;
                        if (domain.indexOf(window.location.hostname) != -1) {
                            agencyFolderName = response.data[index].agencyFolderName;
                        }
                    }
                    var styles = "font-family:Verdana, Geneva, sans-serif;font-size:13px;color:#000;";

                    var summaryTable = "";
                    var summaryDetails = "";

                    var selectedQuotationsFlights = [];
                    var selectedQuotationItemsFlights = vm.selectedQuotationItems.filter(function (item) { return item.indexOf("flights") != -1; });
                    if (selectedQuotationItemsFlights.length > 0) {
                        selectedQuotationsFlights = vm.viewQuotationItems.services.flights.filter(function (item) { return selectedQuotationItemsFlights.includes(item.serialNumber); })
                    }
                    for (var k = 0; k < selectedQuotationsFlights.length; k++) {
                        var currentItem = selectedQuotationsFlights[k];
                        var firstSegment = currentItem.groupOfFlights[0].flightDetails[0].fInfo;
                        var locationFromF = vm.getCityNameUTCOnlyWithCityCode(firstSegment.location.locationFrom);
                        var departureDateF = vm.getdatetimeformated(firstSegment.dateTime.depDate, 'DDMMYY', 'DD MMM YYYY') + " " + vm.gettimeformated(firstSegment.dateTime.depTime, 'HHmm', 'HH', ': ', 'mm', '');

                        var lastSegment = currentItem.groupOfFlights[0].flightDetails[currentItem.groupOfFlights[0].flightDetails.length - 1].fInfo;
                        var locationToL = vm.getCityNameUTCOnlyWithCityCode(lastSegment.location.locationTo);
                        var departureDateL = vm.getdatetimeformated(lastSegment.dateTime.arrDate, 'DDMMYY', 'DD MMM YYYY') + " " + vm.gettimeformated(lastSegment.dateTime.arrTime, 'HHmm', 'HH', ': ', 'mm', '');

                        summaryTable += `<tr id="` + (k + 1) + `">
                            <td style="text-align:center;">` + (k + 1) + `</td>
                            <td style="text-align:center;">` + locationFromF + `<br />` + departureDateF + `</td>
                            <td style="text-align:center;">` + locationToL + `<br />` + departureDateL + `</td>
                            <td style="text-align:center;">` + currentItem.adt + `</td>
                            <td style="text-align:center;">` + currentItem.chd + `</td>
                            <td style="text-align:center;">` + currentItem.inf + `</td>
                            <td style="text-align:center;"><img class="img_itinerary" src="` + window.location.origin + `/Flights/FlightComponents/EmailTemplates/downloadvoucher/Airlines/` + firstSegment.companyId.mCarrier + `.gif" /><br>` + firstSegment.companyId.mCarrier + '-' + firstSegment.flightNo + `</td>
                            <td style="text-align:center;">` + i18n.n(currentItem.fare.amount / vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency) + `</td>
                            <td style="text-align:center;">` + (currentItem.groupOfFlights[0].flightDetails.length - 1) + `</td>
                            <td style="text-align:center;">` + vm.tripTypeSelected(currentItem.tripType.toLowerCase()) + `</td>
                        </tr>`

                        summaryDetails += `<div><div style="width:100%; float:left;margin-top:10px;"><div style="font-family:Verdana, Geneva, sans-serif;font-size:17px;color:#2e475d;font-weight:700; padding:0px;margin:0px;margin-bottom:8px;padding-bottom:5px;float:left;">` + locationFromF + ` to ` + locationToL + ` <span style="font-size:13px">` + vm.getdatetimeformated(firstSegment.dateTime.depDate, 'DDMMYY', 'DD MMM YYYY') + `</span></div>
                                <div style="font-family:Verdana, Geneva, sans-serif;font-size:12px;font-weight:700;padding:7px;margin-bottom:8px;float:right;background-color:#2e475d;color:#fff;min-width:23px;text-align:center;border-radius:4px;">Option ` + (k + 1) + `</div>
                                <div style="font-family: Verdana, Geneva, sans-serif; font-size: 13px; color: #af0000; font-weight: bold; margin: 0px; text-align: right; float: right; padding: 5px; border: 1px solid #c1c0c0; border-radius: 3px; margin-right: 3px;">` + i18n.n(currentItem.fare.amount / vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency) + `</div>
                                <div style="font-family: Verdana, Geneva, sans-serif; font-size: 13px; color: #af0000; font-weight: bold; margin: 0px; text-align: right; float: right; padding: 5px; border: 1px solid #c1c0c0; border-radius: 3px; margin-right: 3px;">` + vm.tripTypeSelected(currentItem.tripType.toLowerCase()) + `</div>
                                </div>`;

                        var weight = '';
                        if (currentItem.bagDetails != undefined) {
                            if (currentItem.bagDetails.qCode != undefined) {
                                weight = currentItem.bagDetails.qCode;
                            } else {
                                weight = currentItem.bagDetails.unit;
                            }
                        }
                        currentItem.groupOfFlights.forEach(function (fInfo) {
                            summaryDetails += `<div style="width:100%;float:left;margin-bottom:10px;border: 1px solid #d4d4d4;padding: 2px;">`;
                            fInfo.flightDetails.forEach(function (segment, segindex) {

                                var bagunitDesc = '';
                                var baggage = '';
                                var bagunitVal = '';
                                var bagUnit = '';
                                try {
                                    segment.fInfo.bags.forEach(function (bag, index) {
                                        try { baggage = bag.baggage; } catch (err) { /* empty */ }

                                        try { bagunitDesc = getBaggageUnitString(bag.unit).bagunitDesc; } catch (err) { /* empty */ }
                                        try { bagUnit = getBaggageUnitString(bag.unit).bagunitVal; } catch (err) { /* empty */ }
                                        if (baggage == "" || baggage == "NA" || baggage == undefined) { } else {
                                            if (weight == "N" || weight == "P" || bagUnit == "N" || bagUnit == "P" || bagUnit == "Piece") {
                                                bagunitVal = "Piece";
                                            } else {
                                                bagunitVal = "kg";
                                            }
                                        }

                                    });
                                } catch (err) { }
                                var checkInBaggage = baggage != undefined && baggage != "" ? '<p><span data-toggle="tooltip" data-placement="top" title="' + bagunitDesc + '"><span>Check-in baggage: </span>' + baggage + ' ' + bagunitVal + '</span></p>' : '';

                                summaryDetails += `<div style="width:20%;float:left;">
                                        <div style="text-align:left;margin:0px;margin-bottom:5px;text-align:left;"><img src="` + window.location.origin + `/Flights/FlightComponents/EmailTemplates/downloadvoucher/Airlines/` + segment.fInfo.companyId.mCarrier + `.gif" /></div>
                                        <div style="` + styles + `font-weight:600;margin:0px;text-align:left;">` + vm.getAirLineName(segment.fInfo.companyId.mCarrier) + `</div>
                                        <div style="font-family:Verdana, Geneva, sans-serif;font-size:12px;color:#000;font-weight:normal;margin:0px;text-align:left;">` + segment.fInfo.companyId.mCarrier + `-` + segment.fInfo.flightNo + `</div>
                                    </div>
                                    <div style="width:20%;float:left;">
                                        <div style="` + styles + `font-weight:600;margin:0px;text-align:right;">` + vm.momCommonFun(segment.fInfo.dateTime.depTime, 'HHmm', 'HH:mm') + `</div>
                                        <div style="` + styles + `font-weight:normal;margin:0px;text-align:right;">` + vm.getCityNameUTCOnlyWithCityCode(segment.fInfo.location.locationFrom) + `</div>
                                        <div style="` + styles + `font-weight:normal;margin:0px;text-align:right;">` + vm.momCommonFun(segment.fInfo.dateTime.depDate, 'DDMMYY', 'DD MMM YY') + `</div>
                                        <div style="` + styles + `font-weight:normal;margin:0px;text-align:right;">` + (segment.fInfo.location.fromTerminal == null ? 'Terminal: N/A' : 'Terminal:' + segment.fInfo.location.fromTerminal) + `</div>
                                    </div>
                                    <div style="width:15%;float:left;">
                                        <div style="` + styles + `font-weight:normal;margin:0px;padding-top:26px;text-align:center;">` + vm.getElapseTime(segment.fInfo.elapsedTime) + `</div>
                                    </div>
                                    <div style="width:20%;float:left;">
                                        <div style="` + styles + `font-weight:600;margin:0px;text-align:left;">` + vm.momCommonFun(segment.fInfo.dateTime.arrTime, 'HHmm', 'HH:mm') + `</div>
                                        <div style="` + styles + `font-weight:normal;margin:0px;text-align:left;">` + vm.getCityNameUTCOnlyWithCityCode(segment.fInfo.location.locationTo) + `</div>
                                        <div style="` + styles + `font-weight:normal;margin:0px;text-align:left;">` + vm.momCommonFun(segment.fInfo.dateTime.arrDate, 'DDMMYY', 'DD MMM YY') + `</div>
                                        <div style="` + styles + `font-weight:normal;margin:0px;text-align:left;">` + (segment.fInfo.location.toTerminal == null ? 'Terminal: N/A' : 'Terminal:' + segment.fInfo.location.toTerminal) + `</div>
                                    </div>
                                    <div style="width:25%;float:left;">
                                        <div style="` + styles + `font-weight:normal;margin:0px;text-align:left;">Aircraft Type: ` + vm.getAirCraftName(segment.fInfo.eqpType) + `</div>
                                        <div style="` + styles + `font-weight:normal;margin:0px;text-align:left;">Class: ` + (getCabinClassObject(segment.fInfo.bookingClass) ? getCabinClassObject(segment.fInfo.bookingClass).BasicClass : '') + ' (' + segment.fInfo.rbd + `)</div>
                                        ` + checkInBaggage + `<p><span>Cabin baggage:</span> 7 Kg</p></div>`
                                if (fInfo.flightDetails.length > 1 && segindex != fInfo.flightDetails.length - 1) {
                                    summaryDetails += `<div style="width:100%;float:left;font-family:Verdana, Geneva, sans-serif;font-size:13px;color:#2f38a6;line-height:18px;text-align:center;font-weight:bold;margin:10px 0;position:relative;">
                                        <div style="width:100%;height:1px;position:absolute;margin-top:9px;border-bottom:1px dashed #adacac;">&nbsp;</div>
                                        <div style="margin:auto;width:250px;">
                                            <div style="width:100%;float:left;background:#FFF;text-align:center;color:#464442;position:relative;">Layover at ` + segment.fInfo.location.locationTo + ' | ' + calcLayoverTime(segment.fInfo.dateTime.arrDate, segment.fInfo.dateTime.arrTime, fInfo.flightDetails[segindex + 1].fInfo.dateTime.depDate, fInfo.flightDetails[segindex + 1].fInfo.dateTime.depTime) + `</div>
                                        </div>
                                    </div>`
                                }
                            });
                            summaryDetails += `</div>`
                        });
                        summaryDetails += `</div>`
                    }

                    var hotelDetails = "";
                    var summaryTableHotel = "";
                    var selectedQuotationsHotels = [];
                    var selectedQuotationItemsHotels = vm.selectedQuotationItems.filter(function (item) { return item.indexOf("hotels") != -1; });
                    if (selectedQuotationItemsHotels.length > 0) {
                        selectedQuotationsHotels = JSON.parse(JSON.stringify(vm.viewQuotationItems.services.hotels));
                        for (var x = 0; x < vm.viewQuotationItems.services.hotels.length; x++) {
                            selectedQuotationsHotels[x].options = vm.viewQuotationItems.services.hotels[x].options.filter(function (item) { return selectedQuotationItemsHotels.includes(item.serialNumber); })
                        }
                    }
                    for (var h = 0; h < selectedQuotationsHotels.length; h++) {
                        var currentItem = selectedQuotationsHotels[h];
                        var roomOptions = "<div>";

                        var totalPrice = 0;

                        for (var j = 0; j < currentItem.options.length; j++) {
                            var roomDetails = "";

                            var currentOptions = currentItem.options[j];
                            roomDetails += "<b>Option " + (j + 1) + " | " + currentOptions.optionName + "</b>" + `
                                <table border="1" border-color="#00739c;page-break-inside: avoid;" cellpadding="10" cellspacing="0" style="width:100%; border-collapse:collapse;font-family:Verdana, Geneva, sans-serif;margin-bottom:15px;">
                                    <tbody>
                                        <tr style="page-break-inside: avoid;">
                                            <th style="text-align:center;font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #ffffff; font-weight: normal; background: #2c96d1;">Room Type</th>
                                            <th style="text-align:center;font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #ffffff;  font-weight: normal; background: #2c96d1;">Room Status</th>
                                            <th style="text-align:center;font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #ffffff;  font-weight: normal; background: #2c96d1;width:120px;">Check in / Check out</th>
                                            <th style="text-align:center;font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #ffffff; font-weight: normal; background: #2c96d1;">Cost</th>
                                            <th style="text-align:center;font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #ffffff;  font-weight: normal; background: #2c96d1;">Pax</th>
                                            <th style="text-align:center;font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #ffffff;  font-weight: normal; background: #2c96d1;">Meals Type</th>
                                            <th style="text-align:center;font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #ffffff;  font-weight: normal; background: #2c96d1;width:400px;">Cancellation Policy</th>
                                        </tr>`
                            for (var k = 0; k < currentOptions.rooms.length; k++) {
                                var cancellationPolicy = "";
                                totalPrice += parseFloat(currentOptions.rooms[k].rates[0].pricing.totalPrice.price);
                                var price = i18n.n(currentOptions.rooms[k].rates[0].pricing.totalPrice.price / vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency);
                                var pricePerNight = i18n.n((currentOptions.rooms[k].rates[0].pricing.totalPrice.price / currentOptions.rooms[k].numOfNights) / vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency);
                                var boards = currentOptions.rooms[k].rates[0].boards ? currentOptions.rooms[k].rates[0].boards[0].value : "Information not available";
                                var children = currentOptions.rooms[k].guests.children ? currentOptions.rooms[k].guests.children : [];
                                for (var c = 0; c < currentOptions.rooms[k].rates[0].cancellationPoliciesParsed.length; c++) {
                                    cancellationPolicy += currentOptions.rooms[k].rates[0].cancellationPoliciesParsed[c].replace(/h4/, "p");
                                }
                                roomDetails += `<tr style="page-break-inside: avoid">
                                    <td style="font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #717171; padding: 6px; font-weight: normal;">` + currentOptions.rooms[k].roomName + `</td>
                                    <td style="font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #717171; padding: 6px;  font-weight: normal;">` + currentOptions.rooms[k].status + `</td>
                                    <td style="font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #717171; padding: 6px;  font-weight: normal;width:120px;">` + currentOptions.rooms[k].inOutDate + " (" + currentOptions.rooms[k].numOfNights + (currentOptions.rooms[k].numOfNights == 1 ? ' Night' : ' Nights') + `)</td>
                                    <td style="font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #717171; padding: 6px;  font-weight: 500;">` + price + (currentOptions.rooms[k].numOfNights == 1 ? "" : " (" + pricePerNight + "/Night)") + `</td>
                                    <td style="font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #717171;  padding: 6px; font-weight: normal;">Adults:` + currentOptions.rooms[k].guests.adult + (children.length ? ", Child: " + children.length : "") + `</td>
                                    <td style="font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #717171;  padding: 6px; font-weight: normal;">` + boards + `</td>
                                    <td style="font-family: Verdana, Geneva, sans-serif; font-size: 12px; color: #717171;  padding: 6px; font-weight: normal;width:400px;">` + cancellationPolicy + `</td>
                                </tr>`;

                            }
                            roomOptions += roomDetails + '</tbody></table >';
                        }
                        roomOptions += '</div>';
                        summaryTableHotel += `<tr id="` + (h + 1) + `">
                            <td style="text-align:center;">` + (h + 1) + `</td>
                            <td style="text-align:center;">` + currentItem.city + `</td>
                            <td style="text-align:center;">` + currentItem.hotelName + `</td>
                            <td style="text-align:center;">` + currentItem.starRating + `</td>
                            <td style="text-align:center;">` + currentItem.options.length + `</td>
                            <td style="text-align:center;">` + i18n.n(totalPrice / vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency) + `</td>
                        </tr>`
                        hotelDetails += `<div style="width:100%;background:#FFF;float:left;display:block;page-break-inside: auto;margin-top:15px;">
                            <div style="width:222px;float:left;margin-bottom: 9px;min-height:130px;"><img alt="hotel" height="226" src="` + currentItem.imageUrl + `" style="max-height: 180px;max-width: 200px;width:100%; border: 1px solid #E8E8E8;border-radius:3px;page-break-inside: avoid;" width="226" /></div>
                            <div style="font-family: Verdana, Geneva, sans-serif; font-size: 17px; color: #565454; font-weight: 700;">` + currentItem.hotelName + `</div>
                            <div style="font-family: Verdana, Geneva, sans-serif; font-size: 15px; color: #565454;margin-bottom: 4px;">Hotel rating:
                                <div style="color:#2c96d1;display: -webkit-inline-box;">` + currentItem.starRating + ` Star</div>
                            </div>
                            <div style="font-family: Verdana, Geneva, sans-serif; font-size: 15px; color: #565454;">Address:
                                <div style="color:#2c96d1;display: -webkit-inline-box;">` + currentItem.address + `</div>
                            </div>
                            <div style="width:100%;float:left;page-break-inside: inherit;">
                                <div style="font-family: Verdana, Geneva, sans-serif; font-size: 16px; color: #2c96d1; font-weight: 700;border-bottom:1px dashed #d2d2d2;padding: 4px 0px 4px 0px;margin-bottom:7px;">Description</div>
                                <div style="font-family: Verdana, Geneva, sans-serif; font-size: 13px; color: #000000; text-align: justify; line-height: 18px;padding: 5px 15px 5px 5px;">
                                    ` + currentItem.description + `
                                </div>
                            </div>
                            <div style="width:100%;float:left;padding-top:20px;">` + roomOptions + `</div>
                        </div>`
                    }

                    var phone = vm.commonStore.agencyNode.loginNode.phoneList.filter((x) => {
                        return x.type == "Telephone";
                    })[0].number || "";

                    var TPInsuranceDetails = "";
                    var selectedQuotationsInsurance = [];
                    var selectedQuotationItemsInsurance = vm.selectedQuotationItems.filter(function (item) { return item.indexOf("insurance") != -1; });
                    if (selectedQuotationItemsInsurance.length > 0) {
                        selectedQuotationsInsurance = vm.viewQuotationItems.services.insuranceTP.filter(function (item) { return selectedQuotationItemsInsurance.includes(item.serialNumber); })
                    }
                    selectedQuotationsInsurance.forEach(element => {
                       TPInsuranceDetails = TPInsuranceDetails + '<div style=" width: 100%;float: left;border-radius: 3px;overflow: hidden;background: #FFF;box-shadow: 0px 1px 5px rgba(0, 0, 0, 0.05);border: 1px solid #eaeaea;margin-bottom: 10px;"><div><div style="padding: 10px;"><h4 style="float:left">' +
                            element.planTitle + '</h4><h4 style="float:right">' +
                            vm.commonStore.agencyNode.loginNode.currency + ' ' +
                            element.totalPremiumAmount + '</h4></div></div><div style="width: 100%;float: left;padding: 16px;font-size: 13px;"><div><span>' +
                            element.planContent + '</span></div></div></div><br>';
                    });

                    var WISInsuranceDetails = "";
                    var selectedQuotationsWISInsurance = [];
                    var selectedQuotationItemsWISInsurance = vm.selectedQuotationItems.filter(function (item) { return item.indexOf("insurance") != -1; });
                    if (selectedQuotationItemsWISInsurance.length > 0) {
                        selectedQuotationsWISInsurance = vm.viewQuotationItems.services.insuranceWIS.filter(function (item) { return selectedQuotationItemsWISInsurance.includes(item.serialNumber); })
                    }
                    selectedQuotationsWISInsurance.forEach(element => {

                        var fullBenefits= benefits= options="";
                        var cancellation, medicalExpense, personalLiability, tranportation, lossOfPassport, emergencyFamily;
                        element.planContent.benefits.forEach(el => {
                            var cover = isNullorEmptyToBlank(el.cover);
                            var coverageAmount = isNullorEmptyToBlank(el.amount);
                            var excess = isNullorEmptyToBlank(el.excess);

                            if (cover.toLowerCase().indexOf('cancellation') >= 0) {
                                cancellation = coverageAmount;
                            } else if (cover.toLowerCase().indexOf('medical expense') >= 0 || (cover.toLowerCase().indexOf('medical') >= 0 && cover.toLowerCase().indexOf('expenses') >= 0)) {
                                medicalExpense = coverageAmount;
                            } else if (cover.toLowerCase().indexOf('personal liability') >= 0) {
                                personalLiability = coverageAmount;
                            } else if (cover.toLowerCase().indexOf('transportation') >= 0) {
                                tranportation = coverageAmount;
                            } else if (cover.toLowerCase().indexOf('loss of passport') >= 0) {
                                lossOfPassport = coverageAmount;
                            } else if (cover.toLowerCase().indexOf('emergency family travel') >= 0) {
                                emergencyFamily = coverageAmount;
                            }

                            fullBenefits += '<tr><td>' + cover + '</td><td>' + coverageAmount + '</td><td>' + excess + '</td></tr>'; 
                        });
                        benefits = '<tr> <td>Cacellation</td><td>'+ cancellation + '</td></tr><tr> <td>Medical Expenses</td><td>' + medicalExpense + '</td></tr><tr> <td>Personal Liability</td><td>' + personalLiability + '</td></tr><tr> <td>Transportation</td><td>' + tranportation + '</td></tr><tr> <td>Loss of Passport</td><td>' + lossOfPassport + '</td></tr><tr> <td>Emergency Family Travel</td><td>' + emergencyFamily +'</td></tr>';

                        for (var property in element.planContent.options) {
                            var name = isNullorEmptyToBlank(element.planContent.options[property].name);
                            var amount = isNullorEmptyToBlank(element.planContent.options[property].price);
                           
                            options += '<tr><td>' + name + '</td><td>' + vm.commonStore.agencyNode.loginNode.currency + " " + amount + '</td></tr>';
                        }

                        WISInsuranceDetails = `
                    <div style="width:98%;padding:1%;border:1px solid #e5e5e5;">
                            <div style="float:left;width:98%;padding:1%;border:1px solid #e5e5e5;margin-bottom:6px;">
                                <div style="float:left;">
                                    <div style="font-family: Verdana, Geneva, sans-serif;font-size:19px;color:#000;width:100%;text-align:right;">`+element.planTitle+`</div>
                                </div>
                                <div style="float:right;">
                                    <div style="font-family: Verdana, Geneva, sans-serif;font-size:19px;color:#000;width:100%;text-align:right;">`+ vm.commonStore.agencyNode.loginNode.currency + ' ' + element.totalPremiumAmount +`</div>
                                </div>
                            </div>
                            <div style="width:98%;padding:1%;">
                                <div style="font-family: Verdana, Geneva, sans-serif;font-size:14px;color:#000;width:100%;font-weight:bold;border-bottom: 1px dashed #e5e5e5; padding-bottom: 6px;margin-bottom:6px;">Trip Details</div>
                                <table width="100%" cellpadding="6" cellspacing="0" border="1" style="border-collapse:collapse;font-family: Verdana, Geneva, sans-serif;font-size:12px;color:#000;margin-bottom:24px;" bordercolor="#e5e5e5">
                                    <tr>
                                        <td width="20%">Trip Type</td>
                                        <td colspan="7">`+ element.tripDetails.tripType +`</td>
                                    </tr>
                                    <tr>
                                        <td>Start Date</td>
                                        <td width="20%">`+ element.tripDetails.startDate +`</td>
                                        <td width="15%">End Date</td>
                                        <td colspan="5">`+ element.tripDetails.endDate +`</td>
                                    </tr>
                                    <tr>
                                        <td>Family Group</td>
                                        <td colspan="7">`+ element.tripDetails.familyGroup +`</td>
                                    </tr>
                                    <tr>
                                        <td>Destinations</td>
                                        <td colspan="7">`+ element.tripDetails.destinationId +`</td>
                                    </tr>
                                    <tr>
                                        <td>Adults</td>
                                        <td>`+ element.pax.adults +`</td>
                                        <td>Children</td>
                                        <td width="5%">`+ element.pax.children +`</td>
                                        <td width="13%">Seniors</td>
                                        <td width="5%">`+ element.pax.seniors +`</td>
                                        <td width="13%">Super Seniors</td>
                                        <td width="9%">`+ element.pax.seniors2 +`</td>
                                    </tr>
                                </table>
                                <div style="font-family: Verdana, Geneva, sans-serif;font-size:14px;color:#000;width:100%;font-weight:bold;border-bottom: 1px dashed #e5e5e5; padding-bottom: 6px;margin-bottom:6px;">Insurance Cover</div>
                                <table width="100%" cellpadding="6" cellspacing="0" border="1" style="border-collapse:collapse;font-family: Verdana, Geneva, sans-serif;font-size:12px;color:#000;margin-bottom:24px;" bordercolor="#e5e5e5">
                                    `+benefits+`
                                </table>
                                <div style="display:none;font-family: Verdana, Geneva, sans-serif;font-size:14px;color:#000;width:100%;font-weight:bold;border-bottom: 1px dashed #e5e5e5; padding-bottom: 6px;margin-bottom:6px;">Full Benefits</div>
                                <table id="divInsuranceBenefits" width="100%" cellpadding="6" cellspacing="0" border="1" style="display:none;border-collapse:collapse;font-family: Verdana, Geneva, sans-serif;font-size:12px;color:#000;margin-bottom:24px;text-align:left;" bordercolor="#e5e5e5">
                                    <tr>
                                        <th>Section Cover</th>
                                        <th>Sum Insured</th>
                                        <th>Excess</th>
                                    </tr>`
                                    +fullBenefits+
                                `</table>
                                <div style="font-family: Verdana, Geneva, sans-serif;font-size:14px;color:#000;width:100%;font-weight:bold;border-bottom: 1px dashed #e5e5e5; padding-bottom: 6px;margin-bottom:6px;">Additional Cover</div>
                                    <table id="divInsuranceOptions" width="100%" cellpadding="6" cellspacing="0" border="1" style="border-collapse:collapse;font-family: Verdana, Geneva, sans-serif;font-size:12px;color:#000;margin-bottom:24px;" bordercolor="#e5e5e5">
                                `+ options+`
                                    </table>
                                </div>
                        </div>`;

                    });

                    var content = 
                    `<div style="box-sizing:border-box;width:100%;padding:20px;float:left;">
                        <div style="width:100%; margin:0 auto;float:left;">
                            <div style="width:100%; float:left; background:#FFF;">
                                <div style="float: left;border:1px solid #d4d4d4;">
                                    <div style="float: left;border-bottom: 1px solid #d4d4d4;margin-bottom: 10px;width:100%">
                                        <div style="padding:1% 0;width:50%; float:left; text-align:left;"><img src="` + vm.commonStore.agencyNode.loginNode.logo + `" style="margin:0 5%;" /></div>
                                        <div style="padding:1% 0;width:50%; float:left; font-family:Verdana, Geneva, sans-serif; font-size:13px; color:#454e68; font-weight:700; text-align: right;">
                                            <div class="logo-right-column" style="float: right; width: 50%;box-sizing: border-box; padding: 10px 40px 10px 0; position: relative;">
                                                <p style="text-align: right;color: #454E68;">`+ vm.commonStore.agencyNode.loginNode.name + `</p>
                                                <p style="text-align: right;color: #454E68;">+`+ vm.commonStore.agencyNode.loginNode.country.telephonecode + phone + `</p>
                                                <p style="text-align: right;color: #454E68;">`+ vm.commonStore.agencyNode.emailId + `</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="padding: 1%;float: left;` + (summaryTable == "" ? 'display:none;' : '') + `">
                                        <div style="text-align:center;font-family: Verdana, Geneva, sans-serif; font-size: 20px; color: #454e68; font-weight: bold; margin-bottom: 10px; border-bottom: 1px solid #ebebeb; padding-bottom: 5px;">` + vm.viewQuotationItems.name + `</div>
                                        <div style="font-family: Verdana, Geneva, sans-serif; font-size: 19px; color: #454e68; font-weight: bold;  margin-top: 5px; margin-bottom: 5px; border-bottom: 1px solid #ebebeb; padding-bottom: 5px;">Flights</div>
                                        <div style="font-family: Verdana, Geneva, sans-serif; font-size: 15px; color: #454e68; font-weight: bold; margin-bottom: 10px; border-bottom: 1px solid #ebebeb; padding-bottom: 5px;">Quote Summary</div>
                                        <table border="1" bordercolor="#00739c" cellpadding="0" cellspacing="0" style="margin-bottom:10px;border-collapse:collapse;width:100%;font-family:Verdana,Geneva,sans-serif;">
                                            <thead>
                                                <tr>
                                                    <th style="text-align:center;">Options</th>
                                                    <th style="text-align:center;">Departure</th>
                                                    <th style="text-align:center;">Arrival</th>
                                                    <th style="text-align:center;">Adults</th>
                                                    <th style="text-align:center;">Children</th>
                                                    <th style="text-align:center;">Infants</th>
                                                    <th style="text-align:center;">Airline</th>
                                                    <th style="text-align:center;">Fare</th>
                                                    <th style="text-align:center;">Stops</th>
                                                    <th style="text-align:center;">Type</th>
                                                </tr>
                                            </thead>
                                            <tbody>` + summaryTable + `</tbody>
                                        </table>
                                        <div style="font-family: Verdana, Geneva, sans-serif; font-size: 15px; color: #454e68; font-weight: bold; margin-bottom: 10px; border-bottom: 1px solid #ebebeb; padding-bottom: 5px;">Quote Details</div>
                                        <div class="flightdetails5" style = "float: left;border: 1px solid #d4d4d4;padding: 1%;margin-bottom: 10px;">` + summaryDetails + `</div>
                                    </div>
                                    <div style="padding: 1%;float: left;` + (hotelDetails == "" ? 'display:none;' : '') + `">
                                        <div style="font-family: Verdana, Geneva, sans-serif; font-size: 19px; color: #454e68; font-weight: bold;  margin-top: 5px; margin-bottom: 5px; border-bottom: 1px solid #ebebeb; padding-bottom: 5px;">Hotels</div>
                                        <div style="font-family: Verdana, Geneva, sans-serif; font-size: 15px; color: #454e68; font-weight: bold; margin-bottom: 10px; border-bottom: 1px solid #ebebeb; padding-bottom: 5px;">Quote Summary</div>
                                        <table border="1" bordercolor="#00739c" cellpadding="0" cellspacing="0" style="` + (summaryTableHotel == "" ? 'display:none;' : '') + ` margin-bottom:10px;border-collapse:collapse;width:100%;font-family:Verdana,Geneva,sans-serif;">
                                            <thead>
                                                <tr>
                                                    <th style="text-align:center;">No.</th>
                                                    <th style="text-align:center;">City</th>
                                                    <th style="text-align:center;">Hotel Name</th>
                                                    <th style="text-align:center;">Star Rating</th>
                                                    <th style="text-align:center;">No. of Options</th>
                                                    <th style="text-align:center;">Price</th>
                                                </tr>
                                            </thead>
                                            <tbody>` + summaryTableHotel + `</tbody>
                                        </table>
                                        <div style="font-family: Verdana, Geneva, sans-serif; font-size: 15px; color: #454e68; font-weight: bold; margin-bottom: 10px; border-bottom: 1px solid #ebebeb; padding-bottom: 5px;">Quote Details</div>
                                        <div style="width:100%; margin: auto;">` + hotelDetails + `</div>
                                    </div>
                                    <div style="width:98%;padding: 1%;float: left;`+ (TPInsuranceDetails == "" ? 'display:none;' : '') + `">
                                        <div style="font-family: Verdana, Geneva, sans-serif; font-size: 19px; color: #454e68; font-weight: bold;  margin-top: 5px; margin-bottom: 5px; border-bottom: 1px solid #ebebeb; padding-bottom: 5px;">Insurance (Tune Protect)</div>
                                        <div style="font-family: Verdana, Geneva, sans-serif; font-size: 15px; color: #454e68; font-weight: bold; margin-bottom: 10px; border-bottom: 1px solid #ebebeb; padding-bottom: 5px;">Quote Details</div>
                                        ` + TPInsuranceDetails + `
                                    </div><div style="box-sizing:border-box;width:98%;padding: 1%;float: left;`+ (WISInsuranceDetails == "" ? 'display:none;' : '') + `">
                                        <div style="font-family: Verdana, Geneva, sans-serif; font-size: 19px; color: #454e68; font-weight: bold;  margin-top: 5px; margin-bottom: 5px; border-bottom: 1px solid #ebebeb; padding-bottom: 5px;">Insurance (WIS)</div>
                                        <div style="font-family: Verdana, Geneva, sans-serif; font-size: 15px; color: #454e68; font-weight: bold; margin-bottom: 10px; border-bottom: 1px solid #ebebeb; padding-bottom: 5px;">Quote Details</div>
                                        ` + WISInsuranceDetails + 
                                    `</div><div style="padding: 1%;width:98%;border:1px solid #d4d4d4;float: left;` + ((hotelDetails != "" || summaryTable != "" || TPInsuranceDetails != "" || WISInsuranceDetails != "") ? '' : 'display:none;') + `">
                                        <h2 style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:13px; color:#454e68; font-weight:normal;margin:0px; margin-bottom:15px;line-height: 17px;"><strong style="font-size:14px;">Please find above Terms and Conditions</strong></h2>
                                        <h2 style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:13px; color:#525659; font-weight:normal;margin:0px; margin-bottom:15px;line-height: 17px;">1.The above is a quotation only and any reservations made are strictly subject to change and availability from the supplier/s.</h2>
                                        <h2 style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:13px; color:#525659; font-weight:normal;margin:0px; margin-bottom:15px;line-height: 17px;">2.Any taxes are subject to change without prior notice and the price of your itinerary is guaranteed only at the time of purchase.</h2>
                                        <h2 style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:13px; color:#525659; font-weight:normal;margin:0px; margin-bottom:15px;line-height: 17px;">3.Applicable fare rules and penalties from the supplier/s will apply along with agency service fees once the ticket has been issued. </h2>
                                        <h2 style="font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; font-size:13px; color:#525659; font-weight:normal;margin:0px; margin-bottom:15px;line-height: 17px;">4.It shall solely be the responsibility of the buyer to understand all applicable fare rules/penalties at the time of purchase.</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>`
                    $('#summernote').summernote('code', content);
                    $('#summernote').summernote('disable');
                });

            }, 10);
        },
        airportLocationFromAirportCode: function (airlinecode) {
            return airportLocationFromAirportCode(airlinecode);
        },
        getElapseTime: function (elapStr) {
            if (elapStr) {
                return elapStr.substring(0, 2) + 'h ' + elapStr.substring(2, 4) + 'm ';
            }
            return '';
        },
        momCommonFun: function (date, frmFormat, toFormat) {
            return moment(date, frmFormat).format(toFormat);
        },
        getAirLineName: function (airlinecode) {
            return getAirLineName(airlinecode);
        },
        getAirCraftName: function (aireqpcode) {
            return getAirCraftName(aireqpcode);
        },
        airportFromAirportCode: function (airport) {
            return airportFromAirportCode(airport);
        },
        openViewQuotationSnippet: function (flRes) {
            this.showQuotationSnippetModal = true;
            var vm = this;
            var self = this;
            var itinerary = `<div class="itinerary_details">`;

            var weight = '';
            if (flRes.bagDetails != undefined) {
                if (flRes.bagDetails.qCode != undefined) {
                    weight = flRes.bagDetails.qCode;
                } else {
                    weight = flRes.bagDetails.unit;
                }
            }
            flRes.groupOfFlights.forEach(function (flleg2) {
                itinerary += `<div class="itinerary_details">
                <div class="itinerary_head">
                    <h2><i class="fa fa-plane" aria-hidden="true"></i>` + vm.airportLocationFromAirportCode(flleg2.flightDetails[0].fInfo.location.locationFrom) + ' to ' + vm.airportLocationFromAirportCode(flleg2.flightDetails[flleg2.flightDetails.length - 1].fInfo.location.locationTo) + `</h2>
                    <h6><i class="fa fa-clock-o" aria-hidden="true"></i>` + vm.getElapseTime(flleg2.flightProposal.elapse) + `</h6>
                    <h6><i class="fa fa-calendar" aria-hidden="true"></i>` + vm.momCommonFun(flleg2.flightDetails[0].fInfo.dateTime.depDate, 'DDMMYY', 'ddd, DD MMM YY') + `</h6>
                </div>`;

                flleg2.flightDetails.forEach(function (segment, segindex) {
                    var oprater = (segment.fInfo.companyId.mCarrier == segment.fInfo.companyId.oCarrier) ? '' : '<p>Operted by ' + vm.getAirLineName(segment.fInfo.companyId.oCarrier) + '</p>';
                    var bagunitDesc = '';
                    var baggage = '';
                    var bagunitVal = '';
                    var bagUnit = '';
                    try {
                        segment.fInfo.bags.forEach(function (bag, index) {
                            try { baggage = bag.baggage; } catch (err) { /* empty */ }

                            try { bagunitDesc = getBaggageUnitString(bag.unit).bagunitDesc; } catch (err) { /* empty */ }
                            try { bagUnit = getBaggageUnitString(bag.unit).bagunitVal; } catch (err) { /* empty */ }
                            if (baggage == "" || baggage == "NA" || baggage == undefined) { } else {
                                if (weight == "N" || weight == "P" || bagUnit == "N" || bagUnit == "P" || bagUnit == "Piece") {
                                    bagunitVal = "Piece";
                                } else {
                                    bagunitVal = "kg";
                                }
                            }

                        });
                    } catch (err) { }
                    var checkInBaggage = baggage != undefined && baggage != "" ? '<p><span data-toggle="tooltip" data-placement="top" title="' + bagunitDesc + '"><span>Check-in baggage:</span>' + baggage + ' ' + bagunitVal + '</span></p>' : '';
                    itinerary += `<div class="flight_itinerary">
                        <div class="col-sm-2 col-xs-12 text-left no_padding_left itinerary01">` +
                        `<img :src="'/Flights/assets/images/AirLines/' + segment.fInfo.companyId.mCarrier + '.gif'" class="img_itinerary">
                            <h1>` + vm.getAirLineName(segment.fInfo.companyId.mCarrier) + `</h1>
                            <h1>` + segment.fInfo.companyId.mCarrier + `-` + segment.fInfo.flightNo + `</h1>
                            ` + oprater + `</div>

                        <div class="col-xs-12 col-sm-7 no_padding_left mrg_btm">
                        <div class="flight_from left_txt">
                            <p><span data-toggle="tooltip" data-placement="top" title="` + vm.airportFromAirportCode(segment.fInfo.location.locationFrom) + `">` + segment.fInfo.location.locationFrom + `</span></p>
                            <p>` + vm.momCommonFun(segment.fInfo.dateTime.depTime, 'HHmm', 'HH:mm') + `</p>
                            <h4>` + vm.momCommonFun(segment.fInfo.dateTime.depDate, 'DDMMYY', 'DD MMM YY') + `</h4>
                            <h5>` + (segment.fInfo.location.fromTerminal == null ? 'Terminal: N/A' : 'Terminal:' + segment.fInfo.location.fromTerminal) + `</h5>            
                        </div>

                        <div class="flight_from sp08">
                            <div class="filghtImg02 split">
                            <p>` + vm.airportFromAirportCode(segment.fInfo.location.locationFrom) + `</p>
                            <div class="line_flight01">` + line_flight01 + `</div>
                            <p>` + vm.getAirCraftName(segment.fInfo.eqpType) + `</p>
                            </div>
                        </div>

                        <div class="flight_from txt_right01">
                            <p><span data-toggle="tooltip" data-placement="top" title="` + vm.airportFromAirportCode(segment.fInfo.location.locationTo) + `">` + segment.fInfo.location.locationTo + `</span></p>            
                            <p>` + vm.momCommonFun(segment.fInfo.dateTime.arrTime, 'HHmm', 'HH:mm') + `</p>            
                            <h4>` + vm.momCommonFun(segment.fInfo.dateTime.arrDate, 'DDMMYY', 'DD MMM YY') + `</h4>            
                            <h5>` + (segment.fInfo.location.toTerminal == null ? 'Terminal: N/A' : 'Terminal:' + segment.fInfo.location.toTerminal) + `</h5>
                        </div>

                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 left_padding01">
                        <div class="itinerary_flight_details">
                            <p><span>Cabin:</span>` + (getCabinClassObject(segment.fInfo.bookingClass) ? getCabinClassObject(segment.fInfo.bookingClass).BasicClass : '') + `</p>
                            <p><span>RBD:</span>` + (segment.fInfo.rbd ? segment.fInfo.rbd : 'N/A') + `</p>` + checkInBaggage +
                        `<p><span>Cabin baggage:</span> 7 Kg</p>
                        </div>
                        </div>
                        
                    </div>
                    </div>`;

                    if (flleg2.flightDetails.length > 1 && segindex != flleg2.flightDetails.length - 1) {
                        itinerary += '<div class="new_layover"> <h2 data-toggle="tooltip" data-placement="top" title="' + vm.airportFromAirportCode(segment.fInfo.location.locationTo) + '">Layover at' + ' ' + segment.fInfo.location.locationTo + ' | ' + calcLayoverTime(segment.fInfo.dateTime.arrDate, segment.fInfo.dateTime.arrTime, flleg2.flightDetails[segindex + 1].fInfo.dateTime.depDate, flleg2.flightDetails[segindex + 1].fInfo.dateTime.depTime) + '</h2> </div>';
                    }
                });
            });
            this.quoteSnippet = itinerary;
        },
        downloadViewQuotationContent: function () {
            var iframe = document.createElement('iframe');
            $('body').append($(iframe));
            var iframedoc = iframe.contentDocument || iframe.contentWindow.document;

            iframe.style.width = "1049px";
            iframe.style.height = "100%";

            $('body', $(iframedoc)).html($('#summernote').summernote('code'));

            html2canvas(iframedoc.body, { scale: 1, allowTaint: false }).then(function (canvas) {
                var imgData = canvas.toDataURL('image/png', 1.0);

                /*
                Here are the numbers (paper width and height) that I found to work. 
                It still creates a little overlap part between the pages, but good enough for me.
                if you can find an official number from jsPDF, use them.
                */
                var doc = jsPDF('p', 'mm', 'a4');

                var imgWidth = doc.internal.pageSize.getWidth();
                var pageHeight = doc.internal.pageSize.getHeight();
                var imgHeight = canvas.height * imgWidth / canvas.width;
                var heightLeft = imgHeight;

                var position = 0;

                doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight, '', 'FAST');
                heightLeft -= pageHeight;

                while (heightLeft >= 0) {
                    position = -doc.internal.pageSize.getHeight() + 2;
                    doc.addPage();
                    doc.addImage(imgData, 'PNG', 0, position, imgWidth, imgHeight, '', 'FAST');
                    heightLeft -= pageHeight;
                }
                doc.save('Quotation_' + new Date().getTime() + '.pdf');
                $('iframe').remove();
            })

        },
        printViewQuotationContent: function () {
            var id = (new Date()).getTime();
            var myWindow = window.open(window.location.href + '?printerFriendly=true', id, "toolbar=1,scrollbars=1,location=0,statusbar=0,menubar=1,resizable=1,width=800,height=600,left = 240,top = 212");
            myWindow.document.write('<div style="padding:20px 2px;display:flex">' + $('#summernote').summernote('code') + '</div>');
            myWindow.focus();
            setTimeout(
                function () {
                    myWindow.print();
                }, 500);
        },
        clickViewQuotationItems: function (cart) {
            this.viewQuotationItems = cart;
            this.showQuotationViewModal = true;
        },
        getCityNameUTCOnlyWithCityCode: function (airportcode) {
            var CityName = _.where(AirlinesTimezone, { I: airportcode });
            if (CityName == "") {
                CityName = airportcode;
            } else {
                CityName = CityName[0].C;
            }
            return CityName + ' (' + airportcode + ')';
        },
        tripTypeSelected: function (id) {
            return (id == "o") ? 'One Way' : (id == "r") ? 'Roundtrip' : (id == "m") ? 'Multicity' : '';
        },
        updateQuotationOnServer: function (update) {
            var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = this.commonStore.hubUrls.hubConnection.hubServices.addQuote;
            var vm = this;

            var config = {
                axiosConfig: {
                    method: "post",
                    url: hubUrl + serviceUrl,
                    data: {
                        content: JSON.stringify(vm.commonStore.carts)
                    }
                },
                successCallback: function (response) {
                    if (update) {
                        vm.getAllquotationsOnServer();
                    }
                },
                errorCallback: function (error) { },
                showAlert: false
            };

            mainAxiosRequest(config);
        },
        getAllquotationsOnServer: function () {
            var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = this.commonStore.hubUrls.hubConnection.hubServices.pendingQuoteDetails;
            var vm = this;

            var config = {
                axiosConfig: {
                    method: "get",
                    url: hubUrl + serviceUrl
                },
                successCallback: function (response) {
                    if (response.data.data && response.data.data[0].content) {
                        vm.commonStore.carts = JSON.parse(response.data.data[0].content);
                        vm.pendingQuoteId = response.data.data[0].id;
                        var currentCart = window.localStorage.getItem("currentCart");
                        if (currentCart) {
                            for (var k = 0; k < vm.commonStore.carts.length; k++) {
                                if (currentCart == vm.commonStore.carts[k].id) {
                                    vm.commonStore.selectedCart = vm.commonStore.carts[k];
                                } else {
                                    vm.commonStore.selectedCart = vm.commonStore.carts[0];
                                }
                            }
                        } else {
                            vm.commonStore.selectedCart = vm.commonStore.carts[0];
                        }

                    } else {
                        vm.commonStore.carts = [
                            {
                                id: uuidv4(),
                                name: "Default",
                                services: {
                                    flights: [],
                                    hotels: [],
                                    insuranceTP: [],
                                    insuranceWIS: []
                                }
                            }
                        ];
                        vm.commonStore.selectedCart = vm.commonStore.carts[0];
                    }
                },
                errorCallback: function (error) {
                    vm.commonStore.carts = [
                        {
                            id: uuidv4(),
                            name: "Default",
                            services: {
                                flights: [],
                                hotels: [],
                                insuranceTP: [],
                                insuranceWIS: []
                            }
                        }
                    ];
                    vm.commonStore.selectedCart = vm.commonStore.carts[0];
                },
                showAlert: false
            };

            mainAxiosRequest(config);
        },
        saveQuotationList: function () {
            var vm = this;
            var checkEmpty = vm.commonStore.carts.some(function (e) {
                return e.name == "";
            });

            if (checkEmpty) {
                alertify.alert("Warning", "Quotation name cannot be blank.");
            } else {
                vm.updateQuotationOnServer(false);
                vm.showQuotationListModal = false;
                this.enableSaveButton = false;
            }
        },
        nameUpdated: function () {
            if (!this.enableSaveButton) {
                this.enableSaveButton = true;
            }
        },
        fieldValidation: function () {
            if (this.quoteEmailTo == "" || this.quoteEmailSubject == "") {
                alertify.alert("Warning", "Please enter required fields.");
            } else {
                this.emailQuotation();
            }
        },
        emailQuotation: function () {
            var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = this.commonStore.hubUrls.hubConnection.hubServices.sendQuote;
            var vm = this;

            var logoUrl = '';
            var fromEmail = [{ emailId: "" }];

            try {
                logoUrl = JSON.parse(atob(window.localStorage.getItem("agencyNode"))).loginNode.logo;
                // fromEmail = JSON.parse(atob(window.localStorage.getItem("agencyNode"))).loginNode.parentEmailId;
                fromEmail = _.filter(JSON.parse(atob(window.localStorage.getItem("agencyNode"))).loginNode.emailList,
                    function (o) { return o.emailTypeId == 3 && o.emailType.toLowerCase() == "support"; });
            } catch (err) { }

            var sentQuotationItems = {};

            var selectedHotelItems = JSON.parse(JSON.stringify(vm.viewQuotationItems.services.hotels));
            for (var s = 0; s < vm.viewQuotationItems.services.hotels.length; s++) {
                selectedHotelItems[s].options = vm.viewQuotationItems.services.hotels[s].options.filter(function (item) { return vm.selectedQuotationItems.includes(item.serialNumber); })
            }

            var unSelectedHotelItems = JSON.parse(JSON.stringify(vm.viewQuotationItems.services.hotels));
            for (var s = 0; s < vm.viewQuotationItems.services.hotels.length; s++) {
                unSelectedHotelItems[s].options = vm.viewQuotationItems.services.hotels[s].options.filter(function (item) { return !vm.selectedQuotationItems.includes(item.serialNumber); })
            }

            unSelectedHotelItems = unSelectedHotelItems.filter(function (e) { return e.options.length != 0; });

            if (vm.selectedQuotationItems.length > 0) {
                sentQuotationItems = {
                    id: vm.viewQuotationItems.id,
                    name: vm.viewQuotationItems.name,
                    services: {
                        flights: vm.viewQuotationItems.services.flights.filter(function (item) { return vm.selectedQuotationItems.includes(item.serialNumber); }),
                        hotels: selectedHotelItems,
                        insuranceTP: vm.viewQuotationItems.services.insuranceTP.filter(function (item) { return vm.selectedQuotationItems.includes(item.serialNumber); }),
                        insuranceWIS: vm.viewQuotationItems.services.insuranceWIS.filter(function (item) { return vm.selectedQuotationItems.includes(item.serialNumber); })
                    }
                }

                vm.viewQuotationItems.services.flights = vm.viewQuotationItems.services.flights.filter(function (item) { return !vm.selectedQuotationItems.includes(item.serialNumber); })
                vm.viewQuotationItems.services.insuranceTP = vm.viewQuotationItems.services.insuranceTP.filter(function (item) { return !vm.selectedQuotationItems.includes(item.serialNumber); })
                vm.viewQuotationItems.services.insuranceWIS = vm.viewQuotationItems.services.insuranceWIS.filter(function (item) { return !vm.selectedQuotationItems.includes(item.serialNumber); })
                vm.viewQuotationItems.services.hotels = unSelectedHotelItems;
            } else {
                sentQuotationItems = {
                    id: vm.viewQuotationItems.id,
                    name: vm.viewQuotationItems.name,
                    services: {
                        flights: vm.viewQuotationItems.services.flights,
                        hotels: vm.viewQuotationItems.services.hotels,
                        insuranceTP: vm.viewQuotationItems.services.insuranceTP,
                        insuranceWIS: vm.viewQuotationItems.services.insuranceWIS
                    }
                }

                vm.viewQuotationItems.services.flights = [];
                vm.viewQuotationItems.services.hotels = [];
                vm.viewQuotationItems.services.insuranceTP = [];
                vm.viewQuotationItems.services.insuranceWIS = [];
            }
            vm.selectedQuotationItems = [];
            var data = {
                quoteId: vm.pendingQuoteId,
                mailFrom: fromEmail[0].emailId || vm.commonStore.fallBackEmail,
                mailTo: vm.quoteEmailTo,
                mailCC: vm.quoteEmailCC,
                subject: vm.quoteEmailSubject,
                body: JSON.stringify(sentQuotationItems), //$('#summernote').summernote('code'),
                sendDate: moment(new Date()).format('YYYY-MM-DDThh:mm:ss'),
                content: JSON.stringify(vm.commonStore.carts)
            };

            var dataEmail = {
                type: "QuotationRequest",
                fromEmail: fromEmail[0].emailId || vm.commonStore.fallBackEmail,
                toEmails: Array.isArray(vm.quoteEmailTo) ? vm.quoteEmailTo : [vm.quoteEmailTo],
                ccEmails: Array.isArray(vm.quoteEmailCC) ? vm.quoteEmailCC : vm.quoteEmailCC ? [vm.quoteEmailCC] : null,
                bccEmails: null,
                primaryColor: null,
                secondaryColor: null,
                logoUrl: "",
                subject: vm.quoteEmailSubject,
                quoteSummary: $('#summernote').summernote('code'),
                quoteDetails: "",
                year: new Date().getFullYear(),
                websiteUrl: vm.commonStore.agencyNode.loginNode.url || window.location.origin,
                agencyName: vm.commonStore.agencyNode.loginNode.name || "",
                agencyPhone: "+" + vm.commonStore.agencyNode.loginNode.country.telephonecode + vm.commonStore.agencyNode.loginNode.phoneList.filter((x) => {
                    return x.type == "Telephone";
                })[0].number || "",
                agencyMail: fromEmail[0].emailId || ""
            }

            var mailUrl = vm.commonStore.hubUrls.emailServices.emailApi;
            sendMailService(mailUrl, dataEmail);

            var index = vm.commonStore.carts.findIndex(function (item) { return item.id == vm.viewQuotationItems.id })

            vm.commonStore.carts[index] = vm.viewQuotationItems;

            var config = {
                axiosConfig: {
                    method: "post",
                    url: hubUrl + serviceUrl,
                    data: data
                },
                successCallback: function (response) {
                    vm.updateQuotationOnServer(true);
                    vm.showQuotationDetailsModal = false;
                    vm.quoteEmailTo = "";
                    vm.quoteEmailCC = "";
                    vm.quoteEmailSubject = "";
                },
                errorCallback: function (error) { },
                showAlert: false
            };

            mainAxiosRequest(config);
        }

    },
    components: {
        'p-dialog': dialog,
        'p-button': button,
        'p-toolbar': toolbar,
        'p-inputtext': inputtext,
        'p-accordion': accordion,
        'p-accordion-tab': accordiontab,
        'p-checkbox': checkbox
    },
    watch: {

    },
    template: `
    <div>
        <div class="lang lag" style="padding:20px 2px;display:flex">
            <span @click="showQuotationListModal=true" style="padding-right: 10px;font-size: 20px;color: var(--primary-color);cursor:pointer"><i class="fa fa-shopping-cart" aria-hidden="true"></i></span>
            <ul v-if="commonStore.carts.length>0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="dropdown09" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></span>{{commonStore.selectedCart.name}}</a>
                    <div class="nav-link dropdown-toggle dropdown-menu" aria-labelledby="dropdown09">
                    <a href="#" class="dropdown-item" @click.prevent="onSelected(item)" v-for="(item, index) in commonStore.carts" :key="index">{{item.name}}</a>
                    </div>
                </li>
            </ul>
        </div>
        <p-dialog :visible.sync="showQuotationListModal" :base-z-index="9999999" :modal="true" :showHeader="true">
            <p-toolbar>
                <template slot="left">
                    <h4 style="margin: 7px 0;">Manage Quotations</h4>
                </template>
                <template slot="right">
                    <p-button icon="pi pi-plus" class="p-button-primary" @click="addQuotation"/>
                </template>
            </p-toolbar>
            <div v-if="commonStore.carts.length>0" class="relative-position" style="margin-top: 10px;">
                <div class="list-group">
                    <div class="form-inline" v-for="(item, index) in commonStore.carts" style="padding: 5px" :key="index">
                        <span>{{index+1}}.</span>
                        <p-inputtext type="text" @input="nameUpdated" style="width:250px" placeholder="Quotation name" :value="item.name" v-model="item.name"/>
                        <p-button :disabled="commonStore.carts.length==1" icon="pi pi-minus" @click="removeQuotationInList(item.id)"/>
                        <p-button label="View" @click="clickViewQuotationItems(item)"/>
                    </div>
                </div>
            </div>
            <div v-else class="relative-position" style="margin-top: 20px;">
                <div class="list-group">    
                    <h5 style="padding: 10px;">No quotations added. Click plus button to add quotation.</h5>
                </div>
            </div>
            <template #footer>
                <p-button label="Save" icon="pi pi-check" @click="saveQuotationList" :disabled="!enableSaveButton"/>
            </template>
        </p-dialog>
        <p-dialog :visible.sync="showQuotationViewModal" :style="{width: '30vw'}" :base-z-index="99999999" :modal="true">
            <p-toolbar>
                <template slot="left">
                    <div class="header-quotation"><i class="fa fa-file-text"></i>&nbsp;Quotations</div>
                </template>
                <template slot="right">
                    <p-button v-show="(viewQuotationItems.services.flights.length!=0||viewQuotationItems.services.hotels.length!=0||viewQuotationItems.services.insuranceTP.length!=0||viewQuotationItems.services.insuranceWIS.length!=0)&&selectedQuotationItems.length>0" label="Remove |" class="header-btn" @click="removeAllQuotation"></p-button>
                    <p-button v-show="(viewQuotationItems.services.flights.length!=0||viewQuotationItems.services.hotels.length!=0||viewQuotationItems.services.insuranceTP.length!=0||viewQuotationItems.services.insuranceWIS.length!=0)&&selectedQuotationItems.length>0" label="Send |" class="header-btn" @click="openViewQuotationContent"/>
                    <p-button v-show="viewQuotationItems.services.flights.length!=0||viewQuotationItems.services.hotels.length!=0||viewQuotationItems.services.insuranceTP.length!=0||viewQuotationItems.services.insuranceWIS.length!=0" :label="(allQuotationsChecked?'Uncheck':'Check')+' All'" class="header-btn" @click="checkAllQuotationList"/>
                </template>
            </p-toolbar>
            <div class="search_area tabSearch" style="width:100%;margin-top: 10px;" :key="Math.random()">
                <p-accordion class="accordion-custom" :multiple="true">
                    <div class="list-group" v-if="viewQuotationItems.services.flights.length==0&&viewQuotationItems.services.hotels.length==0&&viewQuotationItems.services.insuranceTP.length==0&&viewQuotationItems.services.insuranceWIS.length==0">    
                        <h5 style="padding: 10px 0;">No quotations found.</h5>
                    </div>
                    <p-accordion-tab :active.sync="flightsTab" v-if="viewQuotationItems.services.flights.length!=0">
                        <template slot="header">
                            <span>Flights</span>
                        </template>
                        <div class="q_list_area">
                            <div v-show="viewQuotationItems.services.flights.length!=0" class="checkall-accordion" @click="checkAllFlights">{{allFlightsChecked? 'Uncheck':'Check'}} All</div>
                            <ul>
                                <li v-for="(item, index) in viewQuotationItems.services.flights" :key="index">
                                    <p-checkbox :id="item.serialNumber" name="flights" :value="item.serialNumber" v-model="selectedQuotationItems"/>
                                    <label @click="openViewQuotationSnippet(item)" :for="item.serialNumber" class="p-checkbox-label quotation-snippet">{{getAirLineName(item.groupOfFlights[0].flightDetails[0].fInfo.companyId.mCarrier)}}</label>
                                    <ul>
                                        <li><label>{{getCityNameUTCOnlyWithCityCode(item.groupOfFlights[0].flightDetails[0].fInfo.location.locationFrom)}} - {{getCityNameUTCOnlyWithCityCode(item.groupOfFlights[0].flightDetails[item.groupOfFlights[0].flightDetails.length-1].fInfo.location.locationTo)}}</label></li>
                                        <li><label>{{(item.groupOfFlights[0].flightDetails[0].fInfo.companyId.mCarrier + '-' + item.groupOfFlights[0].flightDetails[0].fInfo.flightNo)}} {{tripTypeSelected(item.tripType.toLowerCase())}}</label></li>
                                    </ul>
                                    <i class="fa fa-times-circle" @click="removeFlightQuotation(item.serialNumber)"></i>
                                </li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                    </p-accordion-tab>
                    <p-accordion-tab :active.sync="hotelsTab" v-if="viewQuotationItems.services.hotels.length!=0">
                        <template slot="header">
                            <span>Hotels</span>
                        </template>
                        <div class="q_list_area">
                            <span v-show="viewQuotationItems.services.hotels.length!=0" class="checkall-accordion" @click="checkAllHotels">{{allHotelsChecked? 'Uncheck':'Check'}} All</span>
                            <ul>
                                <li v-for="(item, index) in viewQuotationItems.services.hotels" :key="index">
                                    <span>{{item.hotelName}}</span>
                                    <ul>
                                        <li v-for="(option, index) in item.options">
                                            <p-checkbox :id="option.serialNumber" name="hotels" :value="option.serialNumber" v-model="selectedQuotationItems"/>
                                            <label :for="option.serialNumber" class="p-checkbox-label">{{'Option ' + (index+1) + ' | ' + option.optionName}}</label>
                                            <i class="fa fa-times-circle" @click="removeRoomQuotation(item.serialNumber, option.serialNumber)"></i>
                                        </li>
                                    </ul>
                                    <i class="fa fa-times-circle" @click="removeHotelQuotation(item.serialNumber)"></i>
                                </li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                    </p-accordion-tab>
                    <p-accordion-tab :active.sync="insuranceTab" v-if="viewQuotationItems.services.insuranceTP.length!=0">
                        <template slot="header">
                            <span>Insurance (Tune Protect)</span>
                        </template>
                        <div class="q_list_area">
                            <span v-show="viewQuotationItems.services.insuranceTP.length!=0" class="checkall-accordion" @click="checkAllInsurance">{{allInsuranceChecked? 'Uncheck':'Check'}} All</span>
                            <ul>
                                <li v-for="(item, index) in viewQuotationItems.services.insuranceTP" :key="index">
                                    <p-checkbox :id="item.serialNumber" name="insurance" :value="item.serialNumber" v-model="selectedQuotationItems"/>
                                    <label style="overflow-wrap: break-word;max-width: 290px;" :for="item.serialNumber" class="p-checkbox-label quotation-snippet">{{item.planTitle}}</label>
                                    <i class="fa fa-times-circle" @click="removeInsuranceQuotation(item.serialNumber)"></i>
                                    <ul></ul>
                                </li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                    </p-accordion-tab>
                    <p-accordion-tab :active.sync="WISinsuranceTab" v-if="viewQuotationItems.services.insuranceWIS.length!=0">
                        <template slot="header">
                            <span>Insurance (WIS)</span>
                        </template>
                        <div class="q_list_area">
                            <span v-show="viewQuotationItems.services.insuranceWIS.length!=0" class="checkall-accordion" @click="checkAllWISInsurance">{{allWISInsuranceChecked? 'Uncheck':'Check'}} All</span>
                            <ul>
                                <li v-for="(item, index) in viewQuotationItems.services.insuranceWIS" :key="index">
                                    <p-checkbox :id="item.serialNumber" name="insurance" :value="item.serialNumber" v-model="selectedQuotationItems"/>
                                    <label style="overflow-wrap: break-word;max-width: 290px;" :for="item.serialNumber" class="p-checkbox-label quotation-snippet">{{item.planTitle}}</label>
                                    <i class="fa fa-times-circle" @click="removeWISInsuranceQuotation(item.serialNumber)"></i>
                                    <ul></ul>
                                </li>
                            </ul>
                        </div>
                        <div class="clearfix"></div>
                    </p-accordion-tab>
                </p-accordion>
            </div>
        </p-dialog>
        <p-dialog :visible.sync="showQuotationDetailsModal" :style="{width: '70vw'}" :base-z-index="999999999" :modal="true" :showHeader="true">
            <div style="text-align:center;margin-bottom:10px;">
                <span class="red">*</span><p-inputtext type="text" style="width:25%" placeholder="To" v-model="quoteEmailTo"/>
                <p-inputtext type="text" style="width:25%" placeholder="CC" v-model="quoteEmailCC"/>
                <span class="red">*</span><p-inputtext type="text" style="width:45%;" placeholder="Subject" v-model="quoteEmailSubject"/>
            </div>
            <div id="summernote"></div>
            <template #footer>
                <p-button label="Print" icon="pi pi-print" @click="printViewQuotationContent" />
                <p-button label="Download" icon="pi pi-download" @click="downloadViewQuotationContent"/>
                <p-button label="Email" icon="pi pi-envelope" @click="fieldValidation"/>
            </template>
        </p-dialog>
        <p-dialog :visible.sync="showQuotationSnippetModal" :style="{width: '50vw'}" :base-z-index="999999999" :modal="true" :showHeader="true">
            <div v-html="quoteSnippet"></div>
        </p-dialog>
        
    </div>
    `
});

//header, footer, sidebar
Vue.component("master-header", {
    data: function () {
        return {
            commonStore: vueCommonStore.state,
            showProfileLinks: false,
            isIndexPage: false,
            nodeList: []
        }
    },
    methods: {
        logout: function () {
            // window.localStorage.clear();
            // window.sessionStorage.clear();
            // window.location.href = "/";
            setLoginPage("login");
        },
        setServiceHeader: function (service) {
            if (service) {
                this.commonStore.currentSearchpage = service;
                window.localStorage.setItem("service", service);
            } else {
                this.setActiveLinks();
                window.localStorage.setItem("service", this.currentSelectedService);
            }
        },
        setActiveLinks: function () {
            //set active links in header
            if (this.commonStore.commonRoles.hasAir) {
                this.currentSelectedService = "flights";
            } else if (this.commonStore.commonRoles.hasHotel) {
                this.currentSelectedService = "hotels";
            } else if (this.commonStore.commonRoles.hasSightseeing) {
                this.currentSelectedService = "sightseeing";
            } else if (this.commonStore.commonRoles.hasInsurance || this.commonStore.commonRoles.hasInsuranceTP) {
                this.currentSelectedService = "insurance";
            } else if (this.commonStore.commonRoles.hasTransfer) {
                this.currentSelectedService = "transfer";
            }

            if (window.location.pathname.indexOf("search.html") != -1) {
                this.isIndexPage = true;
            }
        },
        hideMyProfileLink: function () {
            this.showProfileLinks = false;
        },
        logToAdminPanel: function () {
            var vm = this;
            var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
            var reNewTokenUrl = vm.commonStore.hubUrls.hubConnection.hubServices.reNewToken;
            axios.get(hubUrl + reNewTokenUrl, {
                headers: { "Authorization": "Bearer " + window.localStorage.getItem("accessToken") }
            }).then(function (response) {
                var homaPage = vm.commonStore.agencyNode.loginNode.apUrl;
                if (response && homaPage) {
                    window.open(homaPage + "?token=" + response.headers.access_token, '_blank');
                } else {
                    alertify.alert("Failed!", "You are not authorized to log to admin panel.");
                }
            }).catch(function (error) {
                console.log('Error on Authentication');
            });
        },
        getNodeList: function () {
            var vm = this;
            var roles = vm.commonStore.agencyNode.roleList;
            if (!isNullorUndefind(roles)) {
                if (hasArrayData(roles)) {
                    var agentRoles = roles.filter(function (role) { return role.name.toLowerCase().split(' ').join('') == 'travelcoordinator'; });

                    if (agentRoles.length > 0) {
                        var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
                        var nodeList = vm.commonStore.hubUrls.hubConnection.hubServices.nodeList;
                        axios.get(hubUrl + nodeList, {
                            headers: { "Authorization": "Bearer " + window.localStorage.getItem("accessToken") }
                        }).then(function (response) {
                            if (response) {
                                // vm.commonStore.selectedNode = vueCommonStore.state.agencyNode.loginNode.name;
                                vm.commonStore.selectedNode = vueCommonStore.state.agencyNode.loginNode.code;
                                vm.nodeList = response.data;
                                // vm.nodeList = [
                                //     {
                                //         "code": "AGY389",
                                //         "name": "Oneview B2B"
                                //     },
                                // ]
                                window.localStorage.setItem("agencyNodeListTmp", JSON.stringify(response.data));
                            }
                        }).catch(function (error) {
                            console.log('Error on getting node list.');
                            refreshToken();
                        });
                    }
                }
            }
        },
        onSelected: function (item) {
            var vm = this;
            var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
            var switchNode = vm.commonStore.hubUrls.hubConnection.hubServices.switchNode;
            axios.get(hubUrl + switchNode + "/" + item.code, {
                headers: { "Authorization": "Bearer " + window.localStorage.getItem("accessToken") }
            }).then(function (response) {
                if (response) {
                    vm.commonStore.selectedNode = response.data.user.loginNode.code;
                    vm.commonStore.agencyNode = response.data.user;
                    window.localStorage.setItem("agencyNode", btoa(JSON.stringify(response.data.user)));
                    window.localStorage.setItem("sessionTime", new Date());
                    window.localStorage.setItem("accessToken", response.headers.access_token);
                    window.localStorage.removeItem("currencyI18n");
                    window.location.href = "/search.html";

                }
            }).catch(function (error) {
                console.log('Error on getting node details.');
            });
        },
        showAccountManagerPopUp(){
            this.getAccountManagerDetails();
        },
        getAccountManagerDetails() {
            var vm = this;
            var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
            var accountManager = vm.commonStore.hubUrls.hubConnection.hubServices.accountManager;
            axios.get(hubUrl + accountManager + "/" + vm.commonStore.agencyNode.loginNode.code.slice(3), {
                headers: { "Authorization": "Bearer " + window.localStorage.getItem("accessToken") }
            }).then(function (response) {
                if (response && !_.isEmpty(response.data.node.accountManager[0])) {
                    vm.commonStore.accountManagerDetails = response.data.node;
                }
                $("#accountManagerPopUp").modal('show');
            }).catch(function (error) {
                console.log('Error on getting account details.');
            });
        }
    },
    mounted: function () {
        this.setActiveLinks();
        this.getNodeList();
    },
    // <img class="img-logo" :src="'http://staginghub.oneviewitsolutions.com:8080/premier-v3/javax.faces.resource/'+commonStore.agencyNode.loginNode.logo+'.xhtml?ln=logo&ver='+Date.now()" alt="">
    template: `
    <div>
  <div class="container">

    <div class="main_header">
      <div class="logo_area">
        <a href="/search.html" @click="setServiceHeader('')">
          <img v-if="" class="img-logo" :src="commonStore.agencyNode.loginNode.logo" alt="">
        </a>
      </div>
      <div class="header" :style="commonStore.showSideMenu&&(commonStore.agencyNode.loginNode.logo==''||commonStore.agencyNode.loginNode.logo==undefined)?'margin-left:125px;':''">
        <div id="nav-icon" :class="{'open':commonStore.showSideMenu}" @click="commonStore.showSideMenu=!commonStore.showSideMenu">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>
      <div class="user-menu pull-right profile_view">
        <div v-click-outside="hideMyProfileLink" class="user-info" :class="{'user-info-open':showProfileLinks}" @click="showProfileLinks=!showProfileLinks">
          <div class="profile-img" style="background-image:url(/assets/images/profile.png)"></div>
          <div class="top_info_padding">
            <h3 class="name">{{$t('Header.UserGreeting_Label')}} <span class="username">{{commonStore.agencyNode.firstName}}</span>
            </h3>
            <h4 class="name">{{$t('Header.ViewAccount_Label')}}</h4>
          </div>
        </div>
        <transition name="slide">
          <div class="profile_view_toggle" v-if="showProfileLinks">
            <div class="list_first">
              <ul>
                <li>
                  <a href="/profile.html"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;{{$t('Header.MyProfile_Label')}}</a>
                </li>
                <li>
                  <a href="#" @click="showAccountManagerPopUp"><i class="fa fa-user-circle-o" aria-hidden="true"></i>&nbsp;Your account manager</a>
                </li>
                <li v-if="commonStore.commonRoles.hasAirLogintoAdminPanel">
                  <a href="#" @click="logToAdminPanel"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;Log to Admin Panel</a>
                </li>
                <li v-if="commonStore.commonRoles.hasSupport">
                  <a href="https://helpdesk.oneviewitsolutions.com/" target="_blank"><i class="fa fa-phone" aria-hidden="true"></i>&nbsp;{{$t('Header.Support_Label')}}
                    <img src="/assets/images/new_gif.gif" alt="gif">
                  </a>
                </li>
                <li>
                  <a href="#" @click.prevent="logout"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;{{$t('Header.Logout_Label')}}</a>
                </li>
              </ul>
            </div>
          </div>
        </transition>
      </div>
      
      <div class="pull-right">
      <div class="currency lag" style="padding:20px 2px;" v-if="nodeList.length>0">
          <ul>
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" id="dropdown09" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{commonStore.selectedNode}}</a>
              <div class="nav-link dropdown-toggle dropdown-menu" aria-labelledby="dropdown09">
              <a href="#" class="dropdown-item" v-for="item in nodeList" @click.prevent="onSelected(item)" >{{item.code}}</a>
              </div>
          </li>
          </ul>
      </div>
      </div>

      <div class="pull-right" v-if="commonStore.showCreditLimit">
        <div class="availcredit_limit">
            <div class="credit_sec">
                <p>Available Credit Limit</p>
                <h6><span>{{commonStore.selectedCurrency}}</span> {{commonStore.creditAvailableLimit}}</h6>
            </div>
        </div>
      </div>
      <div class="pull-right">
        <curreny-i18n></curreny-i18n>
      </div>
      <div class="pull-right">
        <language-i18n></language-i18n>
      </div>
      
      
      <div class="pull-right" v-if="commonStore.commonRoles.hasHotelQuotation||commonStore.commonRoles.hasAirQuotation">
        <quotation-module></quotation-module>
      </div>

      <div class="pull-right" v-if="commonStore.commonRoles.hasNoticeBoard&&$t('Notice.Notice_Content').length!=0">
        <div class="notice_sec">
            <a href="#" data-toggle="modal" data-target="#notice"><i class="fa fa-file-text-o"></i>Notice</a>
        </div>  
      </div>


      <div class="header_menu" v-if="!isIndexPage" style="display:none;">
        <ul id="subheaderlinks">
          <li v-if="commonStore.commonRoles.hasAir" class="serviceAir" :class="{active:commonStore.currentSearchpage=='flights'}">
            <a href="/search.html" @click="setServiceHeader('flights')"><i class="fa fa-plane" aria-hidden="true"></i>{{$t('Header.Flights_Label')}}</a>
          </li>
          <li v-if="commonStore.commonRoles.hasHotel" class="serviceHotel" :class="{active:commonStore.currentSearchpage=='hotels'}">
            <a href="/search.html" @click="setServiceHeader('hotels')"><i class="fa fa-hotel" aria-hidden="true"></i>{{$t('Header.Hotels_Label')}}</a>
          </li>
          <li v-if="commonStore.commonRoles.hasSightseeing" class="serviceSightseeing" :class="{active:commonStore.currentSearchpage=='sightseeing'}">
            <a href="/search.html" @click="setServiceHeader('sightseeing')"><i class="fa fa-binoculars" aria-hidden="true"></i>{{$t('Header.Sightseeing_Label')}}</a>
          </li>
          <li v-if="commonStore.commonRoles.hasInsurance" class="serviceInsurance" :class="{active:commonStore.currentSearchpage=='tp-insurance'||commonStore.currentSearchpage=='wis-insurance'}">
            <a><i class="fa fa-money" aria-hidden="true"></i>{{$t('Header.Insurance_Label')}}</a>
            <ul class="srvInsHdr">
              <li v-if="commonStore.commonRoles.hasInsuranceBook">
                <a href="/search.html" @click="setServiceHeader('wis-insurance')"><i class="fa fa-money" aria-hidden="true"></i>{{$t('Header.WISInsurance_Label')}}</a>
              </li>
              <li v-if="commonStore.commonRoles.hasInsuranceTPBook">
                <a href="/search.html" @click="setServiceHeader('tp-insurance')"><i class="fa fa-money" aria-hidden="true"></i>{{$t('Header.TPInsurance_Label')}}</a>
              </li>
            </ul>
          </li>
          <li v-if="commonStore.commonRoles.hasTransfer" class="serviceTransfer" :class="{active:commonStore.currentSearchpage=='transfer'}">
            <a href="/search.html" @click="setServiceHeader('transfer')"><i class="fa fa-car" aria-hidden="true"></i>{{$t('Header.Transfer_Label')}}</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <div class="news" v-if="commonStore.commonRoles.hasNewsLetter">
        <div class="container">
            <div class="bn-breaking-news" id="newsTicker1">
            <div class="bn-label">News Alert</div>
                <div class="bn-news">
                    <ul><li v-for="(item, index) in $t('News.NewsAlert_Content')" :key="index">{{item}}</li></ul>
                </div>
                <div class="bn-controls">
                    <button><span class="bn-arrow bn-prev"></span></button>
                    <button><span class="bn-action"></span></button>
                    <button><span class="bn-arrow bn-next"></span></button>
                </div>
            </div>
        </div>
   </div>
    <div class="supplier-header-information" v-if="commonStore.hotels.expediaAdditionaInfo&&commonStore.hotels.expediaAdditionaInfo.length==1">
        <div class="container">
            <div class="contact">
                <div class="data-field" v-if="commonStore.hotels.expediaAdditionaInfo&&commonStore.hotels.expediaAdditionaInfo[0].customerSupportNumber.show">Customer Support: {{commonStore.hotels.expediaAdditionaInfo[0].customerSupportNumber.data}}</div>
            </div>
        </div>
    </div>
</div>`
});

Vue.component("master-sidebar", {
    data: function () {
        return {
            commonStore: vueCommonStore.state,
            showInsuranceBookList: false,
            showInsuranceSearchList: false,
            nodeList: []
        };
    },
    methods: {
        logout: function () {
            window.localStorage.clear();
            window.sessionStorage.clear();
            window.location.href = "/";
        },
        setServiceSideBar: function (service) {
            window.localStorage.setItem("service", service);
        },
        gotoMyBookings: function () {
            window.localStorage.setItem("searchBookingsFlag", true);
            window.location.href = "/searchbookings.html";
        },
        gotoConsolidatedBookings: function () {
            window.localStorage.setItem("mybookingFlag", true);
            window.location.href = "/consolidatedbookings.html";
        },
        logToAdminPanel: function () {
            var vm = this;
            var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
            var reNewTokenUrl = vm.commonStore.hubUrls.hubConnection.hubServices.reNewToken;
            axios.get(hubUrl + reNewTokenUrl, {
                headers: { "Authorization": "Bearer " + window.localStorage.getItem("accessToken") }
            }).then(function (response) {
                var homaPage = vm.commonStore.agencyNode.loginNode.apUrl;
                if (response && homaPage) {
                    window.open(homaPage + "?token=" + response.headers.access_token, '_blank');
                } else {
                    alertify.alert("Failed!", "You are not authorized to log to admin panel.");
                }
            }).catch(function (error) {
                console.log('Error on Authentication');
            });
        },
        goToOfflineRequest: function () {
            window.sessionStorage.removeItem('offlineBookingId');
            window.location.href = "/offlineBooking.html";
        },
        goToOfflineTicketing: function () {
            window.sessionStorage.removeItem('offlineBookingId');
            window.location.href = "/offlineTicketing.html";
        },
        goToOfflineRefund: function () {
            window.sessionStorage.removeItem('offlineRefundId');
            window.sessionStorage.removeItem('offlineRequestData');
            window.location.href = "/offlinerefund.html";
        },
        goToOfflineVisa: function () {
            window.sessionStorage.removeItem('offlineVisaId');
            window.location.href = "/OfflineRequest.html";
        },
        getNodeList: function () {
            var vm = this;
            var roles = vm.commonStore.agencyNode.roleList;
            if (!isNullorUndefind(roles)) {
                if (hasArrayData(roles)) {
                    var agentRoles = roles.filter(function (role) { return role.name.toLowerCase().split(' ').join('') == 'travelcoordinator'; });

                    if (agentRoles.length > 0) {
                        var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
                        var nodeList = vm.commonStore.hubUrls.hubConnection.hubServices.nodeList;
                        axios.get(hubUrl + nodeList, {
                            headers: { "Authorization": "Bearer " + window.localStorage.getItem("accessToken") }
                        }).then(function (response) {
                            if (response) {
                                // vm.commonStore.selectedNode = vueCommonStore.state.agencyNode.loginNode.name;
                                vm.commonStore.selectedNode = vueCommonStore.state.agencyNode.loginNode.code;
                                vm.nodeList = response.data;
                                // vm.nodeList = [
                                //     {
                                //         "code": "AGY389",
                                //         "name": "Oneview B2B"
                                //     },
                                // ]
                                window.localStorage.setItem("agencyNodeListTmp", JSON.stringify(response.data));
                            }
                        }).catch(function (error) {
                            console.log('Error on getting node list.');
                        });
                    }
                }
            }
        },
        onSelected: function (item) {
            var vm = this;
            var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
            var switchNode = vm.commonStore.hubUrls.hubConnection.hubServices.switchNode;
            axios.get(hubUrl + switchNode + "/" + item.code, {
                headers: { "Authorization": "Bearer " + window.localStorage.getItem("accessToken") }
            }).then(function (response) {
                if (response) {
                    vm.commonStore.selectedNode = response.data.user.loginNode.code;
                    vm.commonStore.agencyNode = response.data.user;
                    window.localStorage.setItem("agencyNode", btoa(JSON.stringify(response.data.user)));
                    window.localStorage.setItem("sessionTime", new Date());
                    window.localStorage.setItem("accessToken", response.headers.access_token);
                    window.localStorage.removeItem("currencyI18n");
                    window.location.href = "/search.html";

                }
            }).catch(function (error) {
                console.log('Error on getting node details.');
            });
        }

    },
    mounted: function () {
        this.getNodeList();
    },
    created: function () {
        // bindCreditLimit();
    },
    template: `
  <div class="left_menu_sec">
    <!--<nav class="st-menu" v-show="commonStore.showSideMenu" @mouseleave="commonStore.showSideMenu=!commonStore.showSideMenu">-->
    <nav class="st-menu" v-if="commonStore.showSideMenu">
	  	<div class="users_info">
        <h2 class="username">{{commonStore.agencyNode.firstName}}</h2>
        <h3 class="AgencyName">{{commonStore.agencyNode.loginNode.name}}</h3>
        <h3 v-if="commonStore.showCreditLimit&&false" class="CreditLimitAmount">{{$n(commonStore.creditAvailableLimit/commonStore.currencyMultiplier, 'currency', commonStore.selectedCurrency)}}</h3>
      </div>
          <ul id="sidemenubar" class="mm-listview">
          <li class="flxmenu"><language-i18n></language-i18n><curreny-i18n></curreny-i18n>
          <div class="multi-agency">
          <div class="currency lag" style="padding:20px 2px;" v-if="nodeList.length>0">
              <ul>
              <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" id="dropdown09" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{commonStore.selectedNode}}</a>
                  <div class="nav-link dropdown-toggle dropdown-menu" aria-labelledby="dropdown09">
                  <a href="#" class="dropdown-item" v-for="item in nodeList" @click.prevent="onSelected(item)" >{{item.code}}</a>
                  </div>
              </li>
              </ul>
          </div>
          </div></li>
          <li class="noticemb"><a href="#" data-toggle="modal" data-target="#notice"><i class="fa fa-file-text-o"></i>Notice</a></li>
        <li>
          <a href="/profile.html" tabindex="0"><i class="fa fa-user" aria-hidden="true"></i>{{$t('Sidebar.MyProfile_Label')}}</a>
        </li>
        <li class="serviceAir" v-if="commonStore.commonRoles.hasAir">
          <a href="/search.html" @click="setServiceSideBar('flights')" tabindex="0"><i class="fa fa-plane" aria-hidden="true"></i>{{$t('Sidebar.Flights_Label')}}</a>
        </li>
        <li class="serviceHotel" v-if="commonStore.commonRoles.hasHotel">
          <a href="/search.html" @click="setServiceSideBar('hotels')" tabindex="0"><i class="fa fa-bed" aria-hidden="true"></i>{{$t('Sidebar.Hotels_Label')}}</a>
        </li>
        <li class="serviceSightseeing" v-if="commonStore.commonRoles.hasSightseeing">
          <a href="/search.html" @click="setServiceSideBar('sightseeing')" tabindex="0"><i class="fa fa-binoculars" aria-hidden="true"></i>{{$t('Sidebar.Sightseeing_Label')}}</a>
        </li>
        <li class="serviceIns" v-if="commonStore.commonRoles.hasInsurance">
          <a @click="showInsuranceBookList=!showInsuranceBookList"><i class="fa fa-money" aria-hidden="true"></i>{{$t('Sidebar.Insurance_Label')}}</a>
          <transition name="slide">
            <ul class="srvIns" v-if="showInsuranceBookList">
                <li v-if="commonStore.commonRoles.hasInsuranceBook">
                  <a href="/search.html" @click="setServiceSideBar('wis-insurance')" tabindex="0"><i class="fa fa-money" aria-hidden="true"></i>{{$t('Sidebar.WISInsurance_Label')}}</a>
                </li>
                <li v-if="commonStore.commonRoles.hasInsuranceTPBook">
                  <a href="/search.html" @click="setServiceSideBar('tp-insurance')" tabindex="0"><i class="fa fa-money" aria-hidden="true"></i>{{$t('Sidebar.TPInsurance_Label')}}</a>
                </li>
                <li v-if="commonStore.commonRoles.hasInsurance">
                  <a href="/search.html" @click="setServiceSideBar('unitrust-insurance')" tabindex="0"><i class="fa fa-money" aria-hidden="true"></i>Unitrust</a>
                </li>
            </ul>
          </transition>
        </li>
        <li class="serviceTransfer" v-if="commonStore.commonRoles.hasTransfer">
          <a href="/search.html" @click="setServiceSideBar('transfer')" tabindex="0"><i class="fa fa-car" aria-hidden="true"></i>{{$t('Sidebar.Transfer_Label')}}</a>
        </li>
        <li class="serviceAirsearch" v-if="commonStore.commonRoles.hasAir&&false">
          <a href="/Flights/SearchBookings.html" tabindex="0"><i class="fa fa-search" aria-hidden="true"></i>{{$t('Sidebar.SearchFlights_Label')}}</a>
        </li>
        <li  class="serviceHotelBook" v-if="commonStore.commonRoles.hasHotel&&false">
          <a href="/Hotels/hotelsearchbooking.html" tabindex="0"><i class="fa fa-search" aria-hidden="true"></i>{{$t('Sidebar.SearchHotels_Label')}}</a>
        </li>
        <li  class="serviceSightBooking" v-if="commonStore.commonRoles.hasSightseeing&&false">
          <a href="/sightseeing/SearchSightBooking.html" tabindex="0"><i class="fa fa-search" aria-hidden="true"></i>{{$t('Sidebar.SearchSightseeing_Label')}}</a>
        </li>
        <li  class="serviceInsBkng" v-if="commonStore.commonRoles.hasInsurance&&false">
          <a @click="showInsuranceSearchList=!showInsuranceSearchList"><i class="fa fa-search" aria-hidden="true"></i>{{$t('Sidebar.SearchInsurance_Label')}}</a>
          <transition name="slide">
            <ul class="srvInsBkng" v-if="showInsuranceSearchList">
                <li v-if="commonStore.commonRoles.hasInsuranceBook">
                  <a href="/Insurance/wis/SearchInsBooking.html" class="serviceInsBkngwis" tabindex="0"><i class="fa fa-search" aria-hidden="true"></i>{{$t('Sidebar.SearchWISInsurance_Label')}}</a>
                </li>
                <li v-if="commonStore.commonRoles.hasInsuranceTPBook">
                  <a href="/Insurance/Tuneprotect/SearchInsBooking.html" class="serviceInsBkngtuneprotect" tabindex="0"><i class="fa fa-search" aria-hidden="true"></i>{{$t('Sidebar.SearchTPInsurance_Label')}}</a>
                </li>
            </ul>
          </transition>
        </li>
        <li class="serviceTns" v-if="commonStore.commonRoles.hasTransfer&&false">
          <a href="/transfer/SearchBooking.html" tabindex="0"><i class="fa fa-search" aria-hidden="true"></i>{{$t('Sidebar.SearchTransfer_Label')}}</a>
        </li>
        <li class="ServicePnrImport" v-if="commonStore.commonRoles.hasAirPnrImport">
            <a href="/Flights/PnrImport.html" tabindex="0"><i class="fa fa-download" aria-hidden="true"></i>Import PNR</a>
        </li>
        <li v-if="commonStore.commonRoles.hasAirSearchBookings
          ||commonStore.commonRoles.hasHotelSearchBookings
          ||commonStore.commonRoles.hasSightseingSearchBookings
          ||(commonStore.commonRoles.hasInsuranceSearchBookings || commonStore.commonRoles.hasInsuranceTPSearchBookings)
          ||commonStore.commonRoles.hastranferSearchBookings">
          <a href="#" @click="gotoMyBookings" tabindex="0"><i class="fa fa-search" aria-hidden="true"></i>{{$t('Sidebar.MyBookings_Label')}}</a>
        </li>
        <li v-if="commonStore.commonRoles.hasAirConsolidatedBookings
          ||commonStore.commonRoles.hasHotelConsolidatedBookings
          ||commonStore.commonRoles.hasSightseingConsolidatedBookings
          ||(commonStore.commonRoles.hasInsuranceConsolidatedBookings || commonStore.commonRoles.hasInsuranceTPConsolidatedBookings)
          ||commonStore.commonRoles.hastranferConsolidatedBookings">
          <a href="#" @click="gotoConsolidatedBookings" tabindex="0"><i class="fa fa-search" aria-hidden="true"></i>{{$t('Sidebar.SearchBookings_Label')}}</a>
        </li>
        <li class="offlineRequest" v-if="commonStore.commonRoles.hasAirOfflineRequestList">
            <a href="/searchofflinebookings.html" tabindex="0"><i class="fa fa-search" aria-hidden="true"></i>Search Offline Requests</a>
        </li>
        <li class="offlineRequest" v-if="commonStore.commonRoles.hasAirOfflineBooking">
            <a href="#" @click.prevent="goToOfflineRequest" tabindex="0"><i class="fa fa-external-link" aria-hidden="true"></i>Offline Booking</a>
        </li>
        <li class="offlineRequest" v-if="commonStore.commonRoles.hasAirOfflineTicketing">
            <a href="#" @click.prevent="goToOfflineTicketing" tabindex="0"><i class="fa fa-external-link" aria-hidden="true"></i>Offline Ticketing</a>
        </li>

        <li class="offlineRequest" v-if="commonStore.commonRoles.hasAirOfflineRefund">
            <a href="#" @click.prevent="goToOfflineRefund" tabindex="0"><i class="fa fa-external-link" aria-hidden="true"></i>Offline Refund/Void</a>
        </li>

        <li class="offlineRequest" v-if="commonStore.commonRoles.hasAirOfflineVisa">
            <a href="#" @click.prevent="goToOfflineVisa" tabindex="0"><i class="fa fa-external-link" aria-hidden="true"></i>Offline Service Request</a>
        </li>

        <li>
        <li v-if="commonStore.commonRoles.hasAirLogintoAdminPanel">
          <a href="#" @click="logToAdminPanel" tabindex="0"><i class="fa fa-sign-in" aria-hidden="true"></i>Log to Admin Panel</a>
        </li>
        <li class="serviceMyQuotes" v-if="commonStore.commonRoles.hasHotelQuotation||commonStore.commonRoles.hasAirQuotation">
          <a href="/myquotations.html" tabindex="0"> <i class="fa fa-shopping-cart" aria-hidden="true"></i>{{$t('Sidebar.MyQuotations_Label')}}</a>
        </li>
        <li v-if="commonStore.commonRoles.hasSupport">
          <a href="https://helpdesk.oneviewitsolutions.com/" target="_blank" tabindex="0"><i class="fa fa-support" aria-hidden="true"></i>
            {{$t('Sidebar.Support_Label')}} <img src="/assets/images/new_gif.gif" alt="gif"></a>
        </li>
        <li class="cliklogout"><a href="#" @click.prevent="logout"><i class="fa fa-power-off" aria-hidden="true"></i>{{$t('Sidebar.Logout_Label')}}</a>
        </li>
      </ul>
    </nav>
  </div>
  `
});

Vue.component("master-footer", {
    data: function () {
        return {
            commonStore: vueCommonStore.state,
            currentYear: new Date().getFullYear(),
            accountManager: null
        };
    },
    watch: {
        "commonStore.accountManagerDetails" (val) {
            this.accountManager = val;
        }
    },
    template: `
    <div>
    <!-- Notice Board -->
    <div class="modal fade" id="notice" role="dialog" v-if="$t('Notice.Notice_Content').length!=0">
        <div class="modal-dialog fix_width">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Notice Board</h4>
            </div>
            <div class="modal-body">
                <div class="noticeSec" v-html="$t('Notice.Notice_Content')">
                </div>
            </div>
        </div>
        </div>
    </div>
    <!--Notice Board close-->
    <!-- Account Manager Board -->
    <div class="modal fade" id="accountManagerPopUp" role="dialog">
        <div class="modal-dialog fix_width">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Account Manager</h4>
            </div>
            <div class="modal-body text-center">
                <div v-if="accountManager">
                    <p><strong>Account Manager Name: &nbsp; {{accountManager.accountManager[0].name}}</strong></p>
                    <p><strong>Account Manager Email: &nbsp; {{accountManager.accountManager[0].accountManagerContact.emailList[0].emailId}}</strong></p>
                    <p><strong>Account Manager Phone: &nbsp; {{accountManager.accountManager[0].accountManagerContact.phone[0].number}}</strong></p>
                </div>
                <div v-else>
                    <p>Not Available</p>
                </div>
            </div>
        </div>
        </div>
    </div>
    <!--Account Manager close-->
      <section class="thirdbelt">
        <div class="container">
          <div class="row">
            <div class="copyrightmain">
              <p class="al_cp footer-content">© {{currentYear}}. {{commonStore.agencyNode.loginNode.name}} - {{$t('Footer.Copyright_Label')}}</p>
             <!-- <p class="ar_cp semibold">Powered by <a href="http://www.oneviewit.com/" target="_blank">
              <img src="/assets/images/powered.png" style="width: 85px;" alt="Oneview"></a></p> -->
            </div>
          </div>
        </div>
      </section><a href="#" class="scrollToTop" style="display: inline;"><i class="fa fa-angle-double-up"></i></a>
  </div>
  `
});

var master_vue_element = new Vue({
    i18n,
    el: "#master_vue_element",
    data: {
        commonStore: vueCommonStore.state
    },
    created: function () {

        var agencyNode = window.localStorage.getItem("agencyNode");
        if (agencyNode) {
            this.commonStore.agencyNode = JSON.parse(atob(agencyNode));
            // window.localStorage.removeItem("agencyNode");
            this.getRoleLoginNode(this.commonStore.agencyNode);
            var favIcon;
            var pageTitle;
            pageTitle = this.commonStore.agencyNode.loginNode.site.title;
            favIcon = this.commonStore.agencyNode.loginNode.site.fevIcon;
            if (favIcon === 'NA') {
                axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                    for (var index = 0; index < response.data.length; index++) {
                        var domain = response.data[index].domain;
                        if (domain.indexOf(window.location.hostname) != -1) {
                            agencyFolderName = response.data[index].agencyFolderName;
                            favIcon = agencyFolderName + '/website-informations/favicon/favi.png';
                            pageTitle = agencyFolderName.split("Login/")[1];
                            document.title = pageTitle;
                            var link = document.querySelector("link[rel*='icon']") || document.createElement('link');
                            link.type = 'image/x-icon';
                            link.rel = 'shortcut icon';
                            link.href = favIcon;
                            document.getElementsByTagName('head')[0].appendChild(link);
                        }
                    }
                });
            } else {
                document.title = pageTitle;
                var link = document.querySelector("link[rel*='icon']") || document.createElement('link');
                link.type = 'image/x-icon';
                link.rel = 'shortcut icon';
                link.href = favIcon;
                document.getElementsByTagName('head')[0].appendChild(link);
            }




            //Hotels

            var pagePath = window.location.pathname.toString();

            if (pagePath != "/Hotels/hotelconfirmation.html" && pagePath != "/Hotels/hotelpax.html") {
                window.sessionStorage.removeItem("hotelInfo");
            }
        } else {
            setLoginPage("login")
        }
    },
    beforeDestroy: function () {
        clearInterval(this.roleTimer);
    },
    watch: {
        "commonStore.commonRoles": {
            handler: function () {
                this.setRoles();
                if (this.commonStore.commonRoles.hasAir) {
                    this.commonStore.currentSearchpage = "flights";
                    this.commonStore.currentSearchBookingpage = "flights";
                } else if (this.commonStore.commonRoles.hasHotel) {
                    this.commonStore.currentSearchpage = "hotels";
                    this.commonStore.currentSearchBookingpage = "hotels";
                } else if (this.commonStore.commonRoles.hasSightseeing) {
                    this.commonStore.currentSearchpage = "sightseeing";
                    this.commonStore.currentSearchBookingpage = "sightseeing";
                } else if (this.commonStore.commonRoles.hasInsurance) {
                    this.commonStore.currentSearchpage = "wis-insurance";
                    this.commonStore.currentSearchBookingpage = "wis-insurance";
                } else if (this.commonStore.commonRoles.hasInsuranceTP) {
                    this.commonStore.currentSearchpage = "tp-insurance";
                    this.commonStore.currentSearchBookingpage = "tp-insurance";
                } else if (this.commonStore.commonRoles.hasTransfer) {
                    this.commonStore.currentSearchpage = "transfer";
                    this.commonStore.currentSearchBookingpage = "transfer";
                }

                var service = window.localStorage.getItem("service")

                if (window.location.pathname.indexOf("searchofflinebookings.html") != -1) {
                    var tab = window.sessionStorage.getItem("offlineReqTab");
                    service = tab || "flights";
                }

                if (!isNullorUndefind(service)) {
                    this.commonStore.currentSearchpage = service;
                    var pagePath = window.location.pathname.toString();
                    if (pagePath == "/searchbookings.html" || pagePath == "/consolidatedbookings.html") {
                        var sfbws = window.localStorage.getItem('sfbws');
                        if (sfbws != "null") {
                            this.commonStore.currentSearchBookingpage = this.commonStore.currentSearchpage = window.localStorage.getItem("service") || "";
                        } else {
                            if (window.localStorage.getItem("mybookingFlag") != "true" && window.localStorage.getItem("searchBookingsFlag") != "true") {
                                this.commonStore.currentSearchBookingpage = this.commonStore.currentSearchpage;
                            } else {
                                this.commonStore.currentSearchpage = this.commonStore.currentSearchBookingpage;

                            }

                        }
                    } else if (window.location.pathname.indexOf("searchofflinebookings.html") != -1) {
                        this.setServiceSearchBookingPage(service);
                    }
                }
            },
            deep: true
        },
        "commonStore.selectedNode": {
            handler: function () {
                this.configureRoles(this.commonStore.agencyNode);
            },
            deep: true
        }
    },
    methods: {
        setServiceSearchPage: function (service) {
            this.commonStore.currentSearchpage = service;
            window.localStorage.setItem("service", service);
        },
        setServiceSearchBookingPage: function (service) {
            this.commonStore.currentSearchpage = service;
            this.commonStore.currentSearchBookingpage = service;
        },
        setRoles: function () {
            var vm = this;
            axios.get("/Resources/Roles/Roles.json").then(function (response) {
                vm.configureRoleServices(response.data);

                var pagePath = window.location.pathname.toString();

                var hasSearchBookingsRoles = vm.commonStore.commonRoles.hasAirSearchBookings ||
                    vm.commonStore.commonRoles.hasHotelSearchBookings ||
                    vm.commonStore.commonRoles.hasSightseingSearchBookings ||
                    vm.commonStore.commonRoles.hasInsuranceSearchBookings ||
                    vm.commonStore.commonRoles.hasInsuranceTPSearchBookings ||
                    vm.commonStore.commonRoles.hastranferSearchBookings;

                var hasConsolidatedSearchBookingsRoles = vm.commonStore.commonRoles.hasAirConsolidatedBookings ||
                    vm.commonStore.commonRoles.hasHotelConsolidatedBookings ||
                    vm.commonStore.commonRoles.hasSightseingConsolidatedBookings ||
                    vm.commonStore.commonRoles.hasInsuranceConsolidatedBookings ||
                    vm.commonStore.commonRoles.hasInsuranceTPConsolidatedBookings ||
                    vm.commonStore.commonRoles.hastranferConsolidatedBookings;
                //  remove pagePath.toLowerCase() from below if and else if
                if (!hasSearchBookingsRoles && pagePath == "/searchbookings.html") {
                    window.location.href = "/search.html";
                } else if (!hasConsolidatedSearchBookingsRoles && pagePath == "/consolidatedbookings.html") {
                    window.location.href = "/search.html";
                } else if (!vm.commonStore.commonRoles.hasAirOfflineRequestList &&
                    (pagePath == "/offlinebooking.html" || pagePath == "/searchofflinebookings.html")) {
                    window.location.href = "/search.html";
                }

            }).catch(function (error) {
                console.log(error);
            });
        },
        getRoleLoginNode: function (agencyNode) {
            var vm = this;
            vm.configureRoles(agencyNode);
            vm.setRoles();

            axios.get("/Resources/Roles/Roles.json").then(function (response) {
                this.roleTimer = setInterval(function () {
                    vm.configureRoleServices(response.data);
                }, 5000);
            }).catch(function (error) {
                console.log(error);
            });
        },
        hasRole: function (roleId, roles, loginRole) {
            var hasRoleType = false;
            roles = (roles || []).filter(function (role) { return role.id == roleId; });
            if (hasArrayData(roles)) {
                try {
                    roles = loginRole[0].functionalityList.filter(function (role) { return role.id == roleId; });
                } catch (e) { roles = []; }
                if (hasArrayData(roles)) {
                    hasRoleType = true;
                }
            }
            return hasRoleType;
        },
        configureRoles: function (data) {
            var vm = this;
            var roles = data.roleList;
            if (!isNullorUndefind(roles)) {
                if (hasArrayData(roles)) {
                    var agentRoles = roles.filter(function (role) { return role.name.toLowerCase().split(' ').join('') == 'agent' || role.name.toLowerCase().split(' ').join('') == 'travelcoordinator'; });
                    if (hasArrayData(agentRoles)) {
                        agentRoles = agentRoles[0].moduleList;
                        var commonRoles = this.commonStore.commonRoles;
                        try {
                            commonRoles.airRoles = agentRoles.filter(function (role) { return role.code.toLowerCase().split(' ').join('') == 'air' });
                        } catch (e) { }
                        try {
                            commonRoles.hotelRoles = agentRoles.filter(function (role) { return role.code.toLowerCase().split(' ').join('') == 'htl' });
                        } catch (e) { }
                        try {
                            commonRoles.insuranceRoles = agentRoles.filter(function (role) { return role.code.toLowerCase().split(' ').join('') == 'ins' });
                        } catch (e) { }
                        try {
                            commonRoles.insuranceTuneProtectRoles = agentRoles.filter(function (role) { return role.code.toLowerCase().split(' ').join('') == 'int' });
                        } catch (e) { }
                        try {
                            commonRoles.sightseeingRoles = agentRoles.filter(function (role) { return role.code.toLowerCase().split(' ').join('') == 'sig' });
                        } catch (e) { }
                        try {
                            commonRoles.transferRoles = agentRoles.filter(function (role) { return role.code.toLowerCase().split(' ').join('') == 'trn' });
                        } catch (e) { }
                    } else {
                        alertify.alert("Error", "No Services or roles configured. Please contact admin.", function () {
                            var agencyNodeTmp = JSON.parse(window.localStorage.getItem("agencyNodeListTmp"));
                            if (agencyNodeTmp && vm.commonStore.selectedNode != "" && data.loginNode.code != agencyNodeTmp[0].code) {
                                var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
                                var switchNode = vm.commonStore.hubUrls.hubConnection.hubServices.switchNode;
                                axios.get(hubUrl + switchNode + "/" + agencyNodeTmp[0].code, {
                                    headers: { "Authorization": "Bearer " + window.localStorage.getItem("accessToken") }
                                }).then(function (response) {
                                    if (response) {
                                        vm.commonStore.selectedNode = response.data.user.loginNode.code;
                                        vm.commonStore.agencyNode = response.data.user;
                                        window.localStorage.setItem("agencyNode", btoa(JSON.stringify(response.data.user)));
                                        window.localStorage.setItem("sessionTime", new Date());
                                        window.localStorage.setItem("accessToken", response.headers.access_token);
                                    }
                                }).catch(function (error) {
                                    console.log('Error on getting node details.');
                                });
                            } else {
                                setLoginPage("login");
                            }
                        });
                    }
                }
            }
        },
        configureRoleServices: function (data) {
            var commonRoles = this.commonStore.commonRoles;
            var vm = this;
            var agencyNode = window.localStorage.getItem("agencyNode");
            if (agencyNode) {
                agencyNode = JSON.parse(atob(agencyNode));
                var agentRoles = agencyNode.roleList.filter(function (role) { return role.name.toLowerCase().split(' ').join('') == 'agent' || role.name.toLowerCase().split(' ').join('') == 'travelcoordinator'; });
                if (agentRoles.length > 1) {
                    agentRoles = agencyNode.roleList.filter(function (role) { return role.name.toLowerCase().split(' ').join('') == 'agent'; })[0] || {};
                } else {
                    agentRoles = agentRoles[0] || {};

                    var agencyNodeTmp = JSON.parse(window.localStorage.getItem("agencyNodeListTmp"));
                    if (agencyNodeTmp && vm.commonStore.selectedNode != "" && agencyNode.loginNode.code != agencyNodeTmp[0].code) {
                        vm.$nextTick(function () {
                            $(document).ready(function () {
                                if (vm.commonStore.commonRoles.hasNoticeBoard && window.localStorage.getItem('popState') != 'shown') {
                                    window.localStorage.setItem('popState', 'shown')
                                    $("#notice").modal('show');
                                }
                                if (vm.commonStore.commonRoles.hasNewsLetter) {
                                    $('#newsTicker1').breakingNews();
                                }
                            });
                        });
                    }
                }

                var airNode = _.find(agencyNode.loginNode.servicesList, function (service) { return service.name.toLowerCase() == 'air' });
                var airRoles = _.find(agentRoles.moduleList, function (service) { return service.name.toLowerCase() == 'flight' });
                commonRoles.hasAir = !isNullorUndefind(airNode) && !isNullorUndefind(airRoles) && !isNullorUndefind(data.roles.flights);

                var hotelNode = _.find(agencyNode.loginNode.servicesList, function (service) { return service.name.toLowerCase() == 'hotel' });
                var hotelRoles = _.find(agentRoles.moduleList, function (service) { return service.name.toLowerCase() == 'hotel' });
                commonRoles.hasHotel = !isNullorUndefind(hotelNode) && !isNullorUndefind(hotelRoles) && !isNullorUndefind(data.roles.hotel);

                var sightseeingNode = _.find(agencyNode.loginNode.servicesList, function (service) { return service.name.toLowerCase() == 'sightseeing' });
                var sightseeingRoles = _.find(agentRoles.moduleList, function (service) { return service.name.toLowerCase() == 'sightseeing' });
                commonRoles.hasSightseeing = !isNullorUndefind(sightseeingNode) && !isNullorUndefind(sightseeingRoles) && !isNullorUndefind(data.roles.sightseeing);

                var insuranceNode = _.find(agencyNode.loginNode.servicesList, function (service) { return service.name.toLowerCase().includes('insurance') });
                var insuranceRoles = _.find(agentRoles.moduleList, function (service) { return service.name.toLowerCase().includes('insurance') });
                commonRoles.hasInsurance = !isNullorUndefind(insuranceNode) && !isNullorUndefind(insuranceRoles) && !isNullorUndefind(data.roles.insurance);

                var insuranceNodeTP = _.find(agencyNode.loginNode.servicesList, function (service) { return service.name.toLowerCase().includes('tuneprotect') });
                var insuranceRolesTP = _.find(agentRoles.moduleList, function (service) { return service.name.toLowerCase().includes('tuneprotect') });
                commonRoles.hasInsuranceTP = !isNullorUndefind(insuranceNodeTP) && !isNullorUndefind(insuranceRolesTP) && !isNullorUndefind(data.roles.insuranceTP);

                var transferNode = _.find(agencyNode.loginNode.servicesList, function (service) { return service.name.toLowerCase() == 'transfer' });
                var transferRoles = _.find(agentRoles.moduleList, function (service) { return service.name.toLowerCase() == 'transfer' });
                commonRoles.hasTransfer = !isNullorUndefind(transferNode) && !isNullorUndefind(transferRoles) && !isNullorUndefind(data.roles.transfer);

                var generalRoles = _.find(agentRoles.moduleList, function (service) { return service.name.toLowerCase() == 'general' });

            }

            var generalRoles = [generalRoles];
            commonRoles.hasSupplierCurrency = this.hasRole(186, data.roles.general, generalRoles); // supplier currency
            commonRoles.hasAirLogintoAdminPanel = this.hasRole(149, data.roles.general, generalRoles); // Air Login to Admin Panel

            var airRoles = [airRoles];
            commonRoles.hasAirDashboard = this.hasRole(39, data.roles.flights, airRoles); // dashboard
            commonRoles.hasAirActionRequired = this.hasRole(40, data.roles.flights, airRoles); // actionrequired
            commonRoles.hasAirQuotation = this.hasRole(41, data.roles.flights, airRoles); // quotation
            commonRoles.hasAirSupplier = this.hasRole(38, data.roles.flights, airRoles); // showsuppliers
            commonRoles.hasAirSupplierFilter = this.hasRole(84, data.roles.flights, airRoles); // supplierfilter
            commonRoles.hasAirBook = this.hasRole(42, data.roles.flights, airRoles); // book
            //commonRoles.hasAirFareCalendar = this.hasRole(92, data.roles.flights, airRoles); // farecalendar
            commonRoles.hasAirPaymentGateway = this.hasRole(76, data.roles.flights, airRoles); // paymentgateway
            commonRoles.hasAirRecentSearch = this.hasRole(96, data.roles.flights, airRoles); // recentsearch
            commonRoles.hasAirIssueTicket = this.hasRole(97, data.roles.flights, airRoles); // issueticket
            commonRoles.hasAirCancelPNR = this.hasRole(98, data.roles.flights, airRoles); // cancelpnr
            commonRoles.hasAirCancelTicket = this.hasRole(150, data.roles.flights, airRoles); // Void Ticket
            commonRoles.hasMiniRuleRole = this.hasRole(123, data.roles.flights, airRoles); // minirule
            commonRoles.hasAirPreferredAndDirectAirline = this.hasRole(99, data.roles.flights, airRoles); // preferredanddirectairline
            commonRoles.hasEditVoucher = this.hasRole(120, data.roles.flights, airRoles); // editvoucher
            commonRoles.hasAirSearchBookings = this.hasRole(37, data.roles.flights, airRoles); // searchBooking
            commonRoles.hasAirAncillaryBaggage = this.hasRole(126, data.roles.flights, airRoles); // AirAncillaryBaggage
            commonRoles.hasAirAncillarySeat = this.hasRole(127, data.roles.flights, airRoles); // AirAncillarySeat
            commonRoles.hasAirAncillaryMeals = this.hasRole(128, data.roles.flights, airRoles); // AirAncillaryMeals
            commonRoles.hasAirAncillaryInsurence = this.hasRole(129, data.roles.flights, airRoles); // AirAncillaryInsurence
            commonRoles.hasAirAncillaryAssistance = this.hasRole(130, data.roles.flights, airRoles); // AirAncillaryAssistance
            commonRoles.hasAirPnrImport = this.hasRole(148, data.roles.flights, airRoles); // Air PNR import
            commonRoles.hasPassportOptional = this.hasRole(154, data.roles.flights, airRoles); // Passport Info Optional
            commonRoles.hasAirConsolidatedBookings = this.hasRole(157, data.roles.flights, airRoles); //Consolidated Bookings
            commonRoles.hasFareRuleRole = this.hasRole(169, data.roles.flights, airRoles); // minirule
            commonRoles.hasAirOfflineRequestList = this.hasRole(170, data.roles.flights, airRoles); // Offline Booking List
            commonRoles.hasAirOfficeId = this.hasRole(171, data.roles.flights, airRoles); // Office Id
            commonRoles.hasNewsLetter = this.hasRole(172, data.roles.flights, airRoles); // News Alerts
            commonRoles.hasNoticeBoard = this.hasRole(173, data.roles.flights, airRoles); // Notices
            commonRoles.hasAirSameRBD = this.hasRole(174, data.roles.flights, airRoles); // repriceSameRBD
            commonRoles.hasAirLowestRBD = this.hasRole(175, data.roles.flights, airRoles); // repriceLowestRBD
            commonRoles.hasAirOfflineBooking = this.hasRole(176, data.roles.flights, airRoles); // Offline Booking
            commonRoles.hasAirOfflineTicketing = this.hasRole(177, data.roles.flights, airRoles); // Offline Ticketing
            commonRoles.hasAirOfflineTicketAfterBooking = this.hasRole(178, data.roles.flights, airRoles); // Offline Ticketing After Booking
            commonRoles.hasSupport = this.hasRole(179, data.roles.flights, airRoles);
            commonRoles.hasAirOfflineRefund = this.hasRole(191, data.roles.flights, airRoles); // Offline Refund
            commonRoles.hasAirOfflineVisa = this.hasRole(196, data.roles.flights, airRoles); // Offline Visa
            commonRoles.hasMoreOptions = this.hasRole(201, data.roles.flights, airRoles); // More Options
            commonRoles.hasShowMarkupComponent = this.hasRole(203, data.roles.flights, airRoles); // Service fee
            commonRoles.hasCalendarSearch = this.hasRole(204, data.roles.flights, airRoles); // Calendar search
            commonRoles.hasShowFlightDaysDuration = this.hasRole(206, data.roles.flights, airRoles); // Show days
            commonRoles.hasAirIssueContactNumber = this.hasRole(209, data.roles.flights, airRoles); // SMS Notification

            var insuranceRoles = [insuranceRoles];
            commonRoles.hasInsuranceDashboard = this.hasRole(52, data.roles.insurance, insuranceRoles); // insuranceDashboard
            commonRoles.hasInsuranceActionRequired = this.hasRole(45, data.roles.insurance, insuranceRoles); // insuranceActionRequired
            commonRoles.hasInsuranceQuotation = this.hasRole(43, data.roles.insurance, insuranceRoles); // insuranceQuotation
            commonRoles.hasInsuranceBook = this.hasRole(44, data.roles.insurance, insuranceRoles); // insuranceBook
            commonRoles.hasInsurancePaymentGateway = this.hasRole(73, data.roles.insurance, insuranceRoles); // insurancePaymentGateway
            commonRoles.hasInsuranceSearchBookings = this.hasRole(105, data.roles.insurance, insuranceRoles); // searchBooking
            commonRoles.hasInsuranceConsolidatedBookings = this.hasRole(159, data.roles.insurance, insuranceRoles); //Consolidated Bookings


            var transferRoles = [transferRoles];
            commonRoles.hasTransferDashboard = this.hasRole(68, data.roles.transfer, transferRoles); // transferDashboard
            commonRoles.hasTransferActionRequired = this.hasRole(69, data.roles.transfer, transferRoles); // transferActionRequired
            commonRoles.hasTransferQuotation = this.hasRole(70, data.roles.transfer, transferRoles); // transferQuotation
            commonRoles.hasTransferBook = this.hasRole(67, data.roles.transfer, transferRoles); // transferBook
            commonRoles.hastranferPaymentGateway = this.hasRole(72, data.roles.transfer, transferRoles); // transferPaymentGateway
            commonRoles.hastranferSearchBookings = this.hasRole(106, data.roles.transfer, transferRoles); // searchBooking
            commonRoles.hastranferConsolidatedBookings = this.hasRole(162, data.roles.transfer, transferRoles); //Consolidated Bookings

            var sightseeingRoles = [sightseeingRoles];
            commonRoles.hasSightseeingDashboard = this.hasRole(48, data.roles.sightseeing, sightseeingRoles); // sightseeingDashboard
            commonRoles.hasSightseeingActionRequired = this.hasRole(47, data.roles.sightseeing, sightseeingRoles); // sightseeingActionRequired
            commonRoles.hasSightseeingQuotation = this.hasRole(51, data.roles.sightseeing, sightseeingRoles); // sightseeingQuotation
            commonRoles.hasSightseeingBook = this.hasRole(50, data.roles.sightseeing, sightseeingRoles); // sightseeingBook
            commonRoles.hasSightseingPaymentGateway = this.hasRole(74, data.roles.sightseeing, sightseeingRoles); // sightseeingPaymentGateway
            commonRoles.hasSightseingSearchBookings = this.hasRole(107, data.roles.sightseeing, sightseeingRoles); // searchBooking
            commonRoles.hasSightseingConsolidatedBookings = this.hasRole(161, data.roles.sightseeing, sightseeingRoles); //Consolidated Bookings

            var hotelRoles = [hotelRoles];
            commonRoles.hasHotelSupplierFilter = this.hasRole(85, data.roles.hotel, hotelRoles); // Supplier Filter
            commonRoles.hasHotelSupplier = this.hasRole(86, data.roles.hotel, hotelRoles); // Suppliers
            commonRoles.hasHotelQuotation = this.hasRole(87, data.roles.hotel, hotelRoles); // Quotation
            commonRoles.hasHotelBook = this.hasRole(88, data.roles.hotel, hotelRoles); // Book
            commonRoles.hasHotelDashboard = this.hasRole(89, data.roles.hotel, hotelRoles); // Dashboard
            commonRoles.hasHotelActionRequired = this.hasRole(90, data.roles.hotel, hotelRoles); // Action Required
            commonRoles.hasHotelSearchBookings = this.hasRole(91, data.roles.hotel, hotelRoles); // Search Bookings
            commonRoles.hasHotelVoucher = this.hasRole(108, data.roles.hotel, hotelRoles); // Voucher
            commonRoles.hasHotelCancelBooking = this.hasRole(109, data.roles.hotel, hotelRoles); // Cancel Booking
            commonRoles.hasHotelStatusHistory = this.hasRole(111, data.roles.hotel, hotelRoles); // Status History
            commonRoles.hasHotelMarkupDetails = this.hasRole(110, data.roles.hotel, hotelRoles); // Markup Details
            commonRoles.hasHotelMap = this.hasRole(112, data.roles.hotel, hotelRoles); // Map
            commonRoles.hasHotelPaymentGateway = this.hasRole(117, data.roles.hotel, hotelRoles); // Payment Gateway
            commonRoles.hasHotelEditVoucher = this.hasRole(121, data.roles.hotel, hotelRoles); // Edit Voucher
            commonRoles.hasHotelCancellationRequest = this.hasRole(122, data.roles.hotel, hotelRoles); // Cancellation Request
            commonRoles.hasHotelConsolidatedBookings = this.hasRole(158, data.roles.hotel, hotelRoles); //Consolidated Bookings
            commonRoles.hasHideCancellationPolicy = this.hasRole(194, data.roles.hotel, hotelRoles); //Show cancellation in voucher
            commonRoles.hasHidePayableThrough = this.hasRole(195, data.roles.hotel, hotelRoles); //Show Supplier notes in voucher
            commonRoles.allHotelRolesLoaded = true;

            var insuranceTuneProtectRoles = [insuranceRolesTP];
            commonRoles.hasInsuranceTPDashboard = this.hasRole(82, data.roles.insuranceTP, insuranceTuneProtectRoles); // insuranceDashboard
            commonRoles.hasInsuranceTPActionRequired = this.hasRole(80, data.roles.insuranceTP, insuranceTuneProtectRoles); // insuranceActionRequired
            commonRoles.hasInsuranceTPQuotation = this.hasRole(78, data.roles.insuranceTP, insuranceTuneProtectRoles); // insuranceQuotation
            commonRoles.hasInsuranceTPBook = this.hasRole(77, data.roles.insuranceTP, insuranceTuneProtectRoles); // insuranceBook
            commonRoles.hasInsuranceTPPaymentGateway = this.hasRole(81, data.roles.insuranceTP, insuranceTuneProtectRoles); // insurancePaymentGateway
            commonRoles.hasInsuranceTPSearchBookings = this.hasRole(104, data.roles.insuranceTP, insuranceTuneProtectRoles); // searchBooking
            commonRoles.hasInsuranceTPConsolidatedBookings = this.hasRole(160, data.roles.insuranceTP, insuranceTuneProtectRoles); //Consolidated Bookings

            commonRoles.hasQuotation = commonRoles.hasAirQuotation || commonRoles.hasHotelQuotation || commonRoles.hasInsuranceQuotation || commonRoles.hasInsuranceTPQuotation || commonRoles.hasSightseeingQuotation || commonRoles.hasTransferQuotation;
            commonRoles.hasSetPrivileges = true;
        },
        getNodeList: function () {
            var vm = this;
            var roles = vm.commonStore.agencyNode.roleList;
            if (!isNullorUndefind(roles)) {
                if (hasArrayData(roles)) {
                    var agentRoles = roles.filter(function (role) { return role.name.toLowerCase().split(' ').join('') == 'travelcoordinator'; });

                    if (agentRoles.length > 0) {
                        var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
                        var nodeList = vm.commonStore.hubUrls.hubConnection.hubServices.nodeList;
                        axios.get(hubUrl + nodeList, {
                            headers: { "Authorization": "Bearer " + window.localStorage.getItem("accessToken") }
                        }).then(function (response) {
                            if (response) {
                                // vm.commonStore.selectedNode = vueCommonStore.state.agencyNode.loginNode.name;
                                vm.commonStore.selectedNode = vueCommonStore.state.agencyNode.loginNode.code;
                                vm.commonStore.nodeList = response.data;
                                // vm.commonStore.nodeList = [
                                //     {
                                //         "code": "AGY389",
                                //         "name": "Oneview B2B"
                                //     },
                                // ]
                                window.localStorage.setItem("agencyNodeListTmp", JSON.stringify(response.data));
                            }
                        }).catch(function (error) {
                            console.log('Error on getting node list.');
                        });
                    }
                }
            }
        },
        onSelected: function (item) {
            var vm = this;
            var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
            var switchNode = vm.commonStore.hubUrls.hubConnection.hubServices.switchNode;
            axios.get(hubUrl + switchNode + "/" + item.code, {
                headers: { "Authorization": "Bearer " + window.localStorage.getItem("accessToken") }
            }).then(function (response) {
                if (response) {
                    vm.commonStore.selectedNode = item.code;
                    // vm.commonStore.selectedNode = item.name;
                    vm.commonStore.agencyNode = response.data.user;
                    window.localStorage.setItem("agencyNode", btoa(JSON.stringify(response.data.user)));
                    window.localStorage.setItem("sessionTime", new Date());
                    window.localStorage.setItem("accessToken", response.headers.access_token);
                }
            }).catch(function (error) {
                console.log('Error on getting node details.');
            });
        }
    },
    computed: {
        searchPageBackgroundImage: function () {
            if (this.commonStore.currentSearchpage == 'flights' && this.commonStore.commonRoles.hasAir) {
                return "dash_form";
            } else if (this.commonStore.currentSearchpage == 'hotels' && this.commonStore.commonRoles.hasHotel) {
                return "dash_form01";
            } else if (this.commonStore.currentSearchpage == 'sightseeing' && this.commonStore.commonRoles.hasSightseeing) {
                return "dash_form02";
            } else if (this.commonStore.currentSearchpage == 'unitrust-insurance' && this.commonStore.commonRoles.hasInsurance) {
                return "dash_form03WIS";
            } else if (this.commonStore.currentSearchpage == 'tp-insurance' && this.commonStore.commonRoles.hasInsurance) {
                return "dash_form03TP";
            } else if (this.commonStore.currentSearchpage == 'wis-insurance' && this.commonStore.commonRoles.hasInsurance) {
                return "dash_form03WIS";
            } else if (this.commonStore.currentSearchpage == 'transfer' && this.commonStore.commonRoles.hasTransfer) {
                return "dash_form04";
            } else {
                return "";
            }

        }
    },
    mounted: function () {
        //Don't remove scrollbar when showing alertify pop up.
        alertify.defaults.preventBodyShift = true;
        //Don't focus on top.
        alertify.defaults.maintainFocus = false;
        this.getNodeList();
        this.$nextTick(function () {
            $(document).ready(function () {
                checkWidth();
            });
            $(document).ready(function () {

                $('.show2').click(function () {
                    $('#pop2').simplePopup();
                    $('.sort_popup5').css({ "z-index": "99999999999999", "display": "block" });
                    $('.search_result_right').css('display', 'none');
                    $('html, body').animate({ scrollTop: 0 }, "slow");
                });

            });

            function checkWidth() {
                if ($(window).width() < 990) {
                    $('#sort_popupNew').addClass('sort_popup5');
                } else {
                    $('#sort_popupNew').removeClass('sort_popup5');
                }
            }

            $(window).resize(checkWidth);
        });

        var vm = this;

        if (window.location.pathname.indexOf("hotelresults.html") == -1 &&
            window.location.pathname.indexOf("hotelconfirmation.html") == -1 && window.location.pathname.indexOf("hotelpax.html") == -1) {
            window.localStorage.removeItem("selectedSuppliers");
        }
        try {
            var hotelSupplier = window.localStorage.getItem("selectedSuppliers");
            if (globalConfigs && hotelSupplier && (hotelSupplier || []).split(",").length == 1) {
                vm.commonStore.hotels.expediaAdditionaInfo = _.filter(globalConfigs.services.hotels.additionalSupplierInformation, function (e) {
                    return e.supplierId == hotelSupplier;
                }) || [];
            }
        } catch (error) { }
    }
});