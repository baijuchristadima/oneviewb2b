Vue.component("offline-visa", {
  data: function () {
    return {
      commonStore: vueCommonStore.state,

      visaProducts: [],
      countries: [],
      visaOptions: [],
      visaProductsList: [],

      country: { name: "", code: "" },
      visaProduct: { name: "", id: "" },
      visaOption: { name: "", id: "" },
      firstName: "",
      surName: "",
      passportNumber: "",
      passportExpiry: "",
      nameOfMother: "",
      expectedTravelDate: "",
      remark: "",
      remarks: [],
      cost: "",
      paxType: "",


      isApplied: false,
      newReq: false,
      docUpdate: false,
      isCompleted: false,

      fileStorage: [],
      fileLinks: [],
      link: [],
      uploadFlag: false,
      costFlag: false,
      checkBox: false,
      isRequestSending: false,

      calcRes: [],
      dataResponse: [],
      ProductCountryList: [],

      requestAttachments: [],
      visaAttachments: [],
      requiredDocs: "",
      termsAndConditions: ""

    };
  },
  created: function () {
    var vm = this;

    var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrlOffline;
    var port = vm.commonStore.hubUrls.hubConnection.ipAddress;
    var offlineRequest = vm.commonStore.hubUrls.hubConnection.hubServices.flights.visaRequest;

    var hqNode = vm.commonStore.agencyNode.loginNode.solutionId;


    var offlineVisaId = window.sessionStorage.getItem("offlineVisaId");
    if (offlineVisaId) {
      axios({
        method: "GET",
        url: hubUrl + port + offlineRequest + "/" + offlineVisaId,
        headers: {
          "Content-Type": "application/json",
          "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
        }
      }).then((response) => {
        console.log("RESPONSE RECEIVED: ", response);
        if (response.data.data.id) {
          vm.dataResponse = response.data.data;



          vm.country.code = vm.dataResponse.visaRequest.visaProduct.visaCountry.code;
          vm.country.name = vm.dataResponse.visaRequest.visaProduct.visaCountry.name;
          vm.visaProduct.name = vm.dataResponse.visaRequest.visaProduct.name;
          vm.visaProduct.id = vm.dataResponse.visaRequest.visaProduct.id;
          vm.visaOption.id = vm.dataResponse.visaRequest.visaProduct.visaFeeList[0].visaOption.id;
          vm.visaOption.name = vm.dataResponse.visaRequest.visaProduct.visaFeeList[0].visaOption.name;
          vm.firstName = vm.dataResponse.visaRequest.passenger.pax.firstName;
          vm.surName = vm.dataResponse.visaRequest.passenger.pax.surName;
          vm.passportNumber = vm.dataResponse.visaRequest.passenger.pax.flightPax.passportNumber;
          vm.passportExpiry = moment(vm.dataResponse.visaRequest.passenger.pax.flightPax.passportExpiry).format('DD/MM/YYYY');
          vm.nameOfMother = vm.dataResponse.visaRequest.passenger.nameOfMother;
          vm.expectedTravelDate = moment(vm.dataResponse.visaRequest.expectedTravelDate).format('DD/MM/YYYY');
          vm.cost = vm.dataResponse.visaRequest.passenger.fare[0].amount;
          vm.paxType = vm.dataResponse.visaRequest.passenger.pax.paxTypeCode
          vm.requestAttachments = vm.dataResponse.requestAttachments || [];
          vm.visaAttachments = vm.dataResponse.visaRequest.attachments || [];

          vm.countries.push(vm.country);
          vm.visaProductsList.push(vm.visaProduct);
          vm.visaOptions.push(vm.visaOption);

          for (var ind = 0; ind < response.data.data.requestHST.length; ind++) {
            if (response.data.data.requestHST[ind].comments) {
              var tempRemarks;
              vm.remarks = [];
              tempRemarks = _.filter(response.data.data.requestHST, function (remark) { return remark.comments; });
              tempRemarks.forEach(remark => {
                vm.remarks.push({ name: remark.user.firstName, comments: remark.comments });
              });
              break;
            }

          }
          // vm.findProduct();
          // vm.findOption();



          var rqStatus = response.data.data.requestTypeStatus.requestStatus.name.toLowerCase();


          if (rqStatus == "applied" || rqStatus == "unclaimed") {
            vm.isApplied = true;
            vm.newReq = false;
            vm.docUpdate = false;
            vm.isCompleted = false;
          } else if (rqStatus == "docs required") {
            vm.isApplied = false;
            vm.newReq = false;
            vm.docUpdate = true;
            vm.isCompleted = false;
          } else if (rqStatus == "approved") {
            vm.isCompleted = true;
            vm.isApplied = false;
            vm.newReq = false;
            vm.docUpdate = false;
          } else {
            vm.isApplied = false;
            vm.newReq = false;
            vm.docUpdate = false;
            vm.isCompleted = false;

          }

        } else {
          alertify.alert('Error', 'We have found some technical difficulties. Please contact admin!');
        }


      }).catch(function (e) {
        console.log(e)
        alertify.alert('Error', 'We have found some technical difficulties. Please contact admin!');
      })

    } else {

      if (hqNode) {
        axios({
          method: "GET",
          url: hubUrl + port + offlineRequest + "/all/" + hqNode,
          headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
          }
        }).then((response) => {
          console.log("List: ", response);
          if (response.data.code == 200) {
            vm.ProductCountryList = response.data.data;
            var countriesTemp = [];
            // var visaProductsTemp = [];
            vm.ProductCountryList.forEach(element => {
              countriesTemp.push({ name: element.visaCountry.name, code: element.visaCountry.code });
              // visaProductsTemp.push({ name: element.name, id: element.id });
            });

            //uniqe list
            var coun = {};
            vm.countries = countriesTemp.filter(function (entry) {
              if (coun[entry.code]) {
                return false;
              }
              coun[entry.code] = true;
              return true;
            });

            // var prod = {};
            // vm.visaProducts = visaProductsTemp.filter(function (entry) {
            //   if (prod[entry.name]) {
            //     return false;
            //   }
            //   prod[entry.name] = true;
            //   return true;
            // });


          }
        });
      }

      vm.newReq = true;
    }


  },
  mounted: function () {
    this.fireJQueryEvents();
    var vm = this;
    vm.checkBox = false;
    vm.fileStorage = [];
    vm.fileLinks = [];
    vm.link = [];

    window.sessionStorage.setItem("offlineReqTab", "visa");
    this.$nextTick(function () {
      $('#deptDate').on("change", function (e) {
        vm.dropdownChange(e, 'expectdate')
      }); $('#passporExpirytDate').on("change", function (e) {
        vm.dropdownChange(e, 'passportdate')
      });
    })
  },
  methods: {
    onUpload: function (event) {
      var vm = this;
      var hqNode = "AGY" + vm.commonStore.agencyNode.loginNode.solutionId;
      var port = vm.commonStore.hubUrls.hubConnection.ipAddress;
      var uploadUrl = vm.commonStore.hubUrls.hubConnection.fileUploadUrl;
      var upload = vm.commonStore.hubUrls.hubConnection.hubServices.upload;
      var url = uploadUrl + port + upload + "/documents";
      $.getJSON('/Login/Oneview/fileupload.json', function (json) {
        var encodedString = btoa(hqNode + ":" + json.Password);
        event.xhr.open('POST', url);
        event.xhr.withCredentials = true;
        event.xhr.setRequestHeader('Authorization', 'Basic ' + encodedString);
        event.xhr.setRequestHeader('Content-Type', 'multipart/form-data');
        event.xhr.setRequestHeader('Accept', 'application/json');
      });
    },
    allowNumber: function () {
      if ((event.keyCode >= 48 && event.keyCode <= 57) ||
        (event.keyCode >= 96 && event.keyCode <= 105) ||
        event.keyCode == 08 ||
        event.keyCode == 46 &&
        event.keyCode != 32) {
      } else {
        event.preventDefault();
      }
    },
    allowText: function () {
      if (((event.keyCode == 8) || (event.keyCode == 32) ||
        (event.keyCode == 96) || (event.keyCode == 9) ||
        (event.keyCode >= 65 && event.keyCode <= 90) ||
        (event.keyCode >= 97 && event.keyCode <= 122))) {
      } else {
        event.preventDefault();
      }
    },
    fireJQueryEvents: function (tableDetails) {
      var vm = this;

      this.$nextTick(function () {

        var maxDate = "10y";
        var mindate = "0d";
        var dateFormat = 'dd/mm/yy';
        var noOfMonths = 1;
        $("#deptDate").datepicker({
          numberOfMonths: parseInt(noOfMonths),
          changeMonth: true,
          changeYear: true,
          minDate: mindate,
          maxDate: maxDate,
          showButtonPanel: false,
          dateFormat: dateFormat,
        });

        $("#passporExpirytDate").datepicker({
          numberOfMonths: parseInt(noOfMonths),
          changeMonth: true,
          changeYear: true,
          // minDate: mindate,
          // maxDate: "180d",
          maxDate: "20y",
          showButtonPanel: false,
          dateFormat: dateFormat,
          beforeShow: function (event, ui) {
            var selectedDate = $("#deptDate").val();
            var selectedDate = moment(selectedDate, "DD/MM/YYYY").add(180, 'days').format("DD/MM/YYYY");
            $("#passporExpirytDate").datepicker("option", "minDate", selectedDate);
          }
        });
      });
    },
    fileUpload: function (event) {
      this.fileStorage = [];

      console.log(event.target.files || event.dataTransfer.files);
      this.fileStorage = event.target.files || event.dataTransfer.files;
      var extention = false;
      if (this.fileStorage.length) {
        for (var i = 0; i < this.fileStorage.length; i++) {
          var name = this.fileStorage[i].name;
          var lastDot = name.lastIndexOf('.');

          // var fileName = name.substring(0, lastDot);
          var ext = name.substring(lastDot + 1);
          // console.log(fileName, ext);
          if (ext.toLowerCase() == "jpg" || ext.toLowerCase() == "jpeg" || ext.toLowerCase() == "pdf") {
            extention = true;
          } else {
            extention = false;
            break;
          }
        }
        if (extention) {
          this.uploadFile();
        } else {
          alertify.alert("Warning", "Please select a valid file");
        }
      }
    },
    deleteFile: function (item) {
      // var files = Object.values(this.fileStorage);
      var files = Object.values(this.fileLinks);
      // this.fileStorage = files.filter(function (e) { return e.name !== item.name; });
      this.fileLinks = files.filter(function (e) { return e !== item; });

    },
    uploadFile: function () {
      var self = this;
      // self.fileLinks = [];
      var hqNode = "AGY" + self.commonStore.agencyNode.loginNode.solutionId;
      var pass;
      self.commonStore.hubUrls.fileUploads.forEach(element => {
        if (hqNode == element.agency) {
          pass = element.password;
          // break;
        }
      });

      if (self.fileStorage != null && self.fileStorage != undefined) {
        for (let i = 0; i < self.fileStorage.length; i++) {
          var encodedString = btoa(hqNode + ":" + pass);
          let formData = new FormData();
          formData.append('file', self.fileStorage[i], new Date().getTime() + "_" + self.fileStorage[i].name);
          var port = self.commonStore.hubUrls.hubConnection.ipAddress;
          var uploadUrl = self.commonStore.hubUrls.hubConnection.fileUploadUrl;
          var upload = self.commonStore.hubUrls.hubConnection.hubServices.upload;
          var url = uploadUrl + port + upload + "/documents"
          axios.post(url,
            formData, {
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'multipart/form-data',
              'Authorization': 'Basic ' + encodedString
            }
          }
          ).then(function (repo) {
            console.log('SUCCESS!!', repo.data.message);
            var fileLink = repo.data.message;
            self.fileLinks.push(fileLink);
            // self.link.push({ attachmentUrl: entry });
            if ((i + 1) == self.fileStorage.length) {
              // alertify.alert("Success", "File Successfully uploaded.");

              self.uploadFlag = true;

            }
          })
            .catch(function () {
              console.log('FAILURE!!');
              self.uploadFlag = false;
            });
        }

      }
      else {
        alertify.alert("warning", "please select the file");
        self.uploadFlag = false;
      }
    },
    dropdownChange: function (event, type) {
      if (event != undefined && event.target != undefined && event.target.value != undefined) {
        if (type == 'expectdate') {
          this.expectedTravelDate = event.target.value;
          // $("#passporExpirytDate").val(" ");
          this.passportExpiry = null;
        } else if (type == 'passportdate') {
          this.passportExpiry = event.target.value;
        }
      }
    },
    submitVisa: function () {
      var vm = this;
      vm.link = [];
      vm.fileLinks.forEach(function (entry) {
        vm.link.push({ attachmentUrl: entry });
      });

      // if (!vm.link.length) {
      //   vm.uploadFile();

      // }
      vm.$validator.validate().then(function (result) {
        var validate = true;
        if (vm.country.code == null || vm.country.code == "") {
          alertify.alert("Warning", "Please select visa country.");
          validate = false;
        } else if (vm.visaProduct.id == null || vm.visaProduct.id == "") {
          alertify.alert("Warning", "Please select visa type.");
          validate = false;
        } else if (vm.visaOption.id == null || vm.visaOption.id == "") {
          alertify.alert("Warning", "Please select priority.");
          validate = false;
        } else if (!result) {
          alertify.alert("Warning", "Please enter required fields.");
          validate = false;
        } else if (!vm.costFlag) {
          alertify.alert("Warning", "Cost is calculating Please Wait.");
          validate = false;
        } else if (!vm.checkBox) {
          alertify.alert("Warning", "Please agree to the terms and conditions.");
          validate = false;
        } else if (!vm.link.length) {
          alertify.alert("Warning", "Please choose  files to upload.");
          validate = false;
        } else if (!vm.uploadFlag && !vm.fileLinks.length) {
          alertify.alert("Warning", "Upload in progress, please wait.");
        }
        // else if (!vm.link.length) {
        //   alertify.confirm("Warning", "Upload in progress, please wait.", function () {
        //     vm.submitVisa();
        //   }, function () { });
        // }
        else if (validate) {
          vm.isRequestSending = true;
          {
            var request = {

              requestHST: [
                {
                  comments: vm.remark

                }
              ],
              visaRequest: {
                visaProduct: {
                  id: vm.visaProduct.id,
                  name: vm.visaProduct.name,
                  visaCountry: vm.country,
                  visaFeeList: [
                    {
                      visaOption: vm.visaOption,
                    }
                  ],
                  visaEnabledNodeService: {
                    tripElement: {
                      provider: [
                        {
                          id: vm.calcRes.data.visaEnabledNodeService.tripElement.provider[0].id,
                          name: vm.calcRes.data.visaEnabledNodeService.tripElement.provider[0].name,
                        }
                      ]
                    }
                  },
                  sealCode: vm.calcRes.data.sealCode,
                },
                passenger: {
                  pax: {
                    firstName: vm.firstName,
                    surName: vm.surName,
                    paxTypeCode: vm.paxType,
                    flightPax: {
                      passportNumber: vm.passportNumber,
                      // passportExpiry: moment(vm.passportExpiry, "dd/mm/yy").format('YYYY-MM-DD')
                      passportExpiry: moment(vm.passportExpiry, "DD MM YYYY").format('YYYY-MM-DD')
                    }
                  },
                  fare: [
                    {
                      key: "actualCost",
                      amount: vm.calcRes.data.visaFeeList[0] ? vm.calcRes.data.visaFeeList[0].supplierBaseFare : null
                    },
                    {
                      key: "roe",
                      amount: vm.calcRes.data.visaFeeList[0] ? vm.calcRes.data.visaFeeList[0].roe : null
                    },
                    {
                      key: "localCost",
                      amount: vm.cost
                    }
                  ],
                  nameOfMother: vm.nameOfMother
                },
                // expectedTravelDate: moment(vm.expectedTravelDate, "dd/mm/yy").format('YYYY-MM-DD')
                expectedTravelDate: moment(vm.expectedTravelDate, "DD MM YYYY").format('YYYY-MM-DD')
              },
              requestAttachments: vm.link
            }

            var axiosConfig = {
              headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + window.localStorage.getItem("accessToken")
              }
            };
            var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrlOffline;
            var port = vm.commonStore.hubUrls.hubConnection.ipAddress;
            var offlineRequestVisa = "/visaRequest/insert";
            console.log(JSON.stringify(request));
            axios.post(hubUrl + port + offlineRequestVisa, request, axiosConfig).then(function (response) {
              vm.fileStorage = [];
              vm.fileLinks = [];
              vm.link = [];
              if (response.data.code == 201 && response.data.message != undefined && response.data.message != '') {
                alertify.alert("Success", "Offline Visa Request Submitted Successfully.", function () {
                  window.location.href = "/searchofflinebookings.html";
                });
              } else {
                alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");
              }
              vm.isRequestSending = false;

            }).catch(function (e) {
              if (e.response.data.errors.length>0) {
                alertify.alert("Warning", e.response.data.errors[0].split(" = ")[1]);
              }else {
                alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");
              }
              vm.isRequestSending = false;
            });
          }
        }
        else {
          alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");
          vm.isRequestSending = false;
        }
      });
    },
    cancelVisa: function () {
      var vm = this;
      var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrlOffline;
      var port = vm.commonStore.hubUrls.hubConnection.ipAddress;
      var offlineVisaCancel = vm.commonStore.hubUrls.hubConnection.hubServices.flights.offlineVisaCancel;
      var url = hubUrl + port + offlineVisaCancel;
      var axiosConfig = {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + window.localStorage.getItem("accessToken")
        }
      };
      var request = vm.dataResponse;
      //ToDo cancel status 5
      request.requestTypeStatus.requestStatus.id = 5;
      request.requestTypeStatus.requestStatus.name = "Cancelled";
      alertify.confirm("Warning", "Do you want to cancel Request?",
        function () {
          if (request.requestTypeStatus.requestStatus.id == 5) {
            vm.isRequestSending = true;
            axios.put(url, request, axiosConfig).then(function (response) {
              if (response.data.message = "Details Updated Successfully" && response.data.code == 202) {
                alertify.alert("Success", "Cancel Successful.", function () {
                  window.location.href = "/searchofflinebookings.html";
                });
              }
              else if (response.data.code == 400) {
                var warning = response.data.message;
                vm.isRequestSending = false;
                alertify.alert("Warning", warning);
              }
              else {
                vm.isRequestSending = false;
                alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");
              }
            }).catch(function (e) {
              try {
                if (e.response.data.code && e.response.data.message) {
                  alertify.alert("Warning", e.response.data.message);
                }
                else {
                  alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");
                }
                vm.isRequestSending = false;
              } catch (error) {
                alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");
                vm.isRequestSending = false;
              }
            });

          }
        },
        function () { });
    },
    calculateCost: function () {
      var vm = this;
      if (vm.paxType && vm.visaOption.id && vm.visaProduct.id && vm.country.code) {
        var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrlOffline;
        var port = vm.commonStore.hubUrls.hubConnection.ipAddress;
        var costCalculateUrl = "/visaRequest/" + vm.visaProduct.id + "/" + vm.visaOption.id + "/" + vm.paxType + "/" + vm.country.code;
        axios.get(hubUrl + port + costCalculateUrl, {
          headers: {
            "Content-Type": "application/json",
            Authorization: "Bearer " + window.localStorage.getItem("accessToken")
          }
        }).then(function (response) {
          if (response.data.code == 200) {
            vm.cost = response.data.data.totalFare ? response.data.data.totalFare : 0
            vm.calcRes = response.data;
            vm.costFlag = true;
          } else {
            vm.costFlag = false;
          }
        }).catch(function () {
          vm.cost = 0;
        });
      }
      else {
        vm.costFlag = false;
      }
    },
    updateDocs: function () {
      // For update Documents
      var vm = this;
      var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrlOffline;
      var port = vm.commonStore.hubUrls.hubConnection.ipAddress;
      var offlineVisaCancel = vm.commonStore.hubUrls.hubConnection.hubServices.flights.offlineVisaCancel;
      var url = hubUrl + port + offlineVisaCancel;

      vm.link = [];
      vm.fileLinks.forEach(function (entry) {
        vm.link.push({ attachmentUrl: entry });
      });

      var axiosConfig = {
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + window.localStorage.getItem("accessToken")
        }
      };

      // if (vm.fileStorage.length) {
      //   vm.uploadFile();
      // }
      if (!vm.fileStorage.length) {
        alertify.alert("Warning", "Please choose  files to upload.");
      } else if (!vm.uploadFlag && !vm.fileLinks.length) {
        alertify.alert("Warning", "Upload in progress, please wait.");
      } else {
        vm.isRequestSending = true;
        var request = vm.dataResponse;
        if (request.requestTypeStatus.requestStatus.id == 12) {
          request.requestTypeStatus.requestStatus.id = 15;
          request.requestTypeStatus.requestStatus.name = "Docs Updated";
          request.requestAttachments = vm.link;
          request.requestHST = [{
            comments: vm.remark
          }];
          axios.put(url, request, axiosConfig).then(function (response) {
            if (response.data.message = "Details Updated Successfully" && response.data.code == 202) {
              alertify.alert("Success", "Updated Successfully.", function () {
                window.location.href = "/searchofflinebookings.html";
              });
            }
            else if (response.data.code == 400) {
              var warning = response.data.message;
              vm.isRequestSending = false;
              alertify.alert("Warning", warning);
            }
            else {
              vm.isRequestSending = false;
              alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");
            }
          }).catch(function (e) {
            try {
              if (e.response.data.code && e.response.data.message) {
                alertify.alert("Warning", e.response.data.message);
              }
              else {
                alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");
              }
              vm.isRequestSending = false;
            } catch (error) {
              vm.isRequestSending = false;
              alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");

            }
          });

        }
      }
    },
    findProduct: function () {
      var self = this;

      var offlineVisaId = window.sessionStorage.getItem("offlineVisaId");
      if (!offlineVisaId) {
        self.visaProduct.name = null;
        self.visaProduct.id = null;
        self.visaOption.name = null;
        self.visaOption.id = null;
      }

      var productList = self.ProductCountryList.filter(product => product.visaCountry.code.toLowerCase().trim() == self.country.code.toLowerCase().trim());
      self.visaProducts = productList;


      // self.visaProducts.forEach(element => {
      //   if (element.requiredDocs) {
      //     self.requiredDocs = element.requiredDocs;

      //   }
      // });

      var visaProductsTemp = [];
      self.visaProducts.forEach(element => {
        visaProductsTemp.push({ name: element.name, id: element.id });
      });

      var prod = {};
      self.visaProductsList = visaProductsTemp.filter(function (entry) {
        if (prod[entry.name]) {
          return false;
        }
        prod[entry.name] = true;
        return true;
      });

      self.calculateCost();
    },
    findOption: function () {
      var self = this;

      var offlineVisaId = window.sessionStorage.getItem("offlineVisaId");
      if (!offlineVisaId) {
        self.visaOption.name = null;
        self.visaOption.id = null;
      }

      var list = [];
      var optionList = self.visaProducts.filter(product => product.name.toLowerCase().trim() == self.visaProduct.name.toLowerCase().trim());
      console.log(optionList);
      if (optionList.length) {
        self.requiredDocs = optionList[0].requiredDocs;
        self.termsAndConditions = optionList[0].termsAndConditions;
      }
      optionList[0].visaFeeList.forEach(function (item, index) {
        list.push({ id: item.visaOption.id, name: item.visaOption.name });
      });
      // console.log(list);
      var prod = {};
      self.visaOptions = list.filter(function (entry) {
        if (prod[entry.id]) {
          return false;
        }
        prod[entry.id] = true;
        return true;
      });
      self.visaOption = self.visaOptions[0];
      // self.$nextTick(function () {

      // });

      self.calculateCost();
    },
  },
  computed: {}
});