var dateFormat = generalInformation.systemSettings.systemDateFormat;
var statusCodeList = [];
var searchParams = {};
Vue.component('flight-autocomplete', {
    data() {
        return {
            KeywordSearch: '',
            resultItemsarr: [],
            autoCompleteProgress: false,
            loading: false,
            highlightIndex: 0
        }
    },
    props: {
        itemText: String,
        itemId: String,
        status: Boolean,
        placeHolderText: String,
        returnValue: String,
        tripType: String,
        id: {
            type: String,
            default: '',
            required: false
        },

    },
    template: `<div class="autocomplete flightauto"  v-click-outside="hideautocompelte">
      <input type="text" :placeholder="placeHolderText" :id="id" :data-aircode="itemId" autocomplete="off"  
      :value="KeywordSearch" class="txt-srch" 
      :class="{ 'loading-circle' : (KeywordSearch && KeywordSearch.length > 2), 'hide-loading-circle': resultItemsarr.length > 0 || resultItemsarr.length == 0 && !loading  }" 
      @input="function(event){onSelectedAutoCompleteEvent(event.target.value,event)}"  
      @keydown.down="down"
      @keydown.up="up"      
      @keydown.esc="autoCompleteProgress=false"
      @keydown.enter="onSelected(resultItemsarr[highlightIndex].code,resultItemsarr[highlightIndex].label)"
      @click="autocomplete"
      @keydown.tab="tabclick(resultItemsarr[highlightIndex])"/>
      <ul ref="searchautocomplete" class="autocomplete-results" v-if="autoCompleteProgress&&resultItemsarr.length > 0">
          <li ref="options" :class="{'autocomplete-result-active' : i == highlightIndex}" class="autocomplete-result" v-for="(item,i) in resultItemsarr" :key="i" @click="onSelected(item.code, item.label)">
              {{ item.label }}
          </li>
      </ul>
  </div>`,
    methods: {
        up: function () {
            if (this.autoCompleteProgress) {
                if (this.highlightIndex > 0) {
                    this.highlightIndex--
                }
            } else {
                this.autoCompleteProgress = true;
            }
            this.fixScrolling();
        },

        down: function () {
            if (this.autoCompleteProgress) {
                if (this.highlightIndex < this.resultItemsarr.length - 1) {
                    this.highlightIndex++
                } else if (this.highlightIndex == this.resultItemsarr.length - 1) {
                    this.highlightIndex = 0;
                }
            } else {
                this.autoCompleteProgress = true;
            }
            this.fixScrolling();
        },
        fixScrolling: function () {
            if (this.$refs.options[this.highlightIndex]) {
                var liH = this.$refs.options[this.highlightIndex].clientHeight;
                if (liH == 50) {
                    liH = 32;
                }
                if (liH * this.highlightIndex >= 32 || this.highlightIndex == 0) {
                    this.$refs.searchautocomplete.scrollTop = liH * this.highlightIndex;
                }
            }

        },
        onSelectedAutoCompleteEvent: _.debounce(function (keywordEntered) {
            var self = this;
            if (keywordEntered.length > 2) {
                this.autoCompleteProgress = true;
                this.loading = true;
                var cityName = keywordEntered;
                var cityarray = cityName.split(' ');
                var uppercaseFirstLetter = "";
                for (var k = 0; k < cityarray.length; k++) {
                    uppercaseFirstLetter += cityarray[k].charAt(0).toUpperCase() + cityarray[k].slice(1).toLowerCase()
                    if (k < cityarray.length - 1) {
                        uppercaseFirstLetter += " ";
                    }
                }
                uppercaseFirstLetter = "*" + uppercaseFirstLetter + "*";
                var lowercaseLetter = "*" + cityName.toLowerCase() + "*";
                var uppercaseLetter = "*" + cityName.toUpperCase() + "*";
                var query = {
                    query: {
                        bool: {
                            should: [{
                                bool: {
                                    should: [{
                                        wildcard: {
                                            iata_code: { value: uppercaseFirstLetter, boost: 3.0 }
                                        }
                                    }, {
                                        wildcard: {
                                            iata_code: { value: uppercaseLetter, boost: 3.0 }
                                        }
                                    }, {
                                        wildcard: {
                                            iata_code: { value: lowercaseLetter, boost: 3.0 }
                                        }
                                    }]
                                }
                            }, {
                                bool: {
                                    should: [{
                                        wildcard: {
                                            municipality: uppercaseFirstLetter
                                        }
                                    }, {
                                        wildcard: {
                                            municipality: uppercaseLetter
                                        }
                                    }, {
                                        wildcard: {
                                            municipality: lowercaseLetter
                                        }
                                    }]
                                }
                            }, {
                                bool: {
                                    should: [{
                                        wildcard: {
                                            name: uppercaseFirstLetter
                                        }
                                    }, {
                                        wildcard: {
                                            name: uppercaseLetter
                                        }
                                    }, {
                                        wildcard: {
                                            name: lowercaseLetter
                                        }
                                    }]
                                }
                            }],
                            must: [{
                                "exists": {
                                    "field": "iata_code"
                                }
                            }]
                        }
                    }
                };
                var config = {
                    axiosConfig: {
                        headers: {
                            "Content-Type": "application/json"
                        },
                        method: "post",
                        url: HubServiceUrls.elasticSearch.url,
                        data: query
                    },
                    successCallback: function (resp) {
                        finalResult = [];
                        var hits = resp.data.hits.hits;
                        var Citymap = new Map();
                        for (var i = 0; i < hits.length; i++) {
                            Citymap.set(hits[i]._source.iata_code, hits[i]._source);
                        }
                        var get_values = Citymap.values();
                        var Cityvalues = [];
                        for (var ele of get_values) {
                            Cityvalues.push(ele);
                        }
                        var results = SortInputFirstFlight(cityName, Cityvalues);
                        for (var i = 0; i < results.length; i++) {
                            finalResult.push({
                                "code": results[i].iata_code,
                                "label": results[i].name + ", " + results[i].iso_country + '(' + results[i].iata_code + ')',
                                "municipality": results[i].municipality
                            });
                        }
                        var newData = [];
                        finalResult.forEach(function (item, index) {
                            if (item.label.toLowerCase().indexOf(keywordEntered.toLowerCase()) >= 0 || item.code.toLowerCase().includes(keywordEntered.toLowerCase()) || item.municipality.toLowerCase().indexOf(keywordEntered.toLowerCase()) >= 0) {
                                newData.push(item);
                            }
                        });
                        self.resultItemsarr = newData;
                        self.autoCompleteProgress = true;
                        self.loading = false;
                    },
                    errorCallback: function (error) { },
                    showAlert: false
                };

                mainAxiosRequest(config);
                // var client = new elasticsearch.Client({
                //     host: [{
                //         host: HubServiceUrls.elasticSearch.elasticsearchHost,
                //         auth: HubServiceUrls.elasticSearch.auth,
                //         protocol: HubServiceUrls.elasticSearch.protocol,
                //         port: HubServiceUrls.elasticSearch.port,
                //         requestTimeout: 60000
                //     }],
                //     log: 'trace'
                // });
                // client.search({
                //     index: 'airport_info',
                //     size: 150,
                //     timeout: "3000ms",
                //     body: query
                // }).then(function (resp) {
                    
                // })
            } else {
                this.autoCompleteProgress = false;
                this.resultItemsarr = [];
                this.loading = false;
            }
            this.KeywordSearch = keywordEntered;
        }, 500),
        onSelected: function (code, label) {
            this.autoCompleteProgress = true;
            this.resultItemsarr = [];
            this.KeywordSearch = label;
            if (this.tripType != 'M') {
                if (this.id == 'txtFrom') {
                    $("#txtTo").focus();
                } else if (this.id == 'txtTo') {
                    $("#deptDate01").focus();
                }

            } else {
                if (this.id == 'txtFrom') {
                    $("#txtTo").focus();
                } else if (this.id == 'txtTo') {
                    $("#deptDate01").focus();
                } else if (this.id == 'txtLeg2From') {
                    $("#txtLeg2To").focus();
                } else if (this.id == 'txtLeg2To') {
                    $("#txtLeg2Date").focus();
                } else if (this.id == 'txtLeg3From') {
                    $("#txtLeg3To").focus();
                } else if (this.id == 'txtLeg3To') {
                    $("#txtLeg3Date").focus();
                } else if (this.id == 'txtLeg4From') {
                    $("#txtLeg4To").focus();
                } else if (this.id == 'txtLeg4To') {
                    $("#txtLeg4Date").focus();
                } else if (this.id == 'txtLeg5From') {
                    $("#txtLeg5To").focus();
                } else if (this.id == 'txtLeg5To') {
                    $("#txtLeg5Date").focus();
                } else if (this.id == 'txtLeg6From') {
                    $("#txtLeg6To").focus();
                } else if (this.id == 'txtLeg6To') {
                    $("#txtLeg6Date").focus();
                }

            }
            this.$emit('air-search-completed', code, label, this.itemId, this.itemText, this.id);

        },
        hideautocompelte: function () {
            this.autoCompleteProgress = false;
        },
        autocomplete: function () {
            this.autoCompleteProgress = true;
        },
        tabclick: function (item) {
            if (!item) {

            } else {
                this.onSelected(item.code, item.label);
            }
        }

    },
    watch: {
        status: function () {
            this.KeywordSearch = this.returnValue;
        }
    }

});
Vue.component("flight-search-bookings", {
    components: {
        'p-dropdown': dropdown,
        'p-paginator': paginator,
        'p-datatable': datatable,
        'p-column': column,
        'p-button': button,
        'p-calendar': calendar,
        'p-multiselect': multiselect,
    },
    directives: {
        'tooltip': tooltip
    },
    data: function () {
        return {
            commonStore: vueCommonStore.state,
            statusCode: null,
            depCity: null,
            arrCity: null,
            deptDateFrom: null,
            deptDateTo: null,
            arrDateFrom: null,
            arrDateTo: null,
            bkngDateFrom: null,
            bkngDateTo: null,
            journeyType: "",
            stops: "",
            status: "",
            operatingAirLine: "",
            supplier: "",
            showBookingList: false,
            sendRequest: false,
            noDataFromResponse: false,
            bookingList: null,
            supplierList: [],
            airlineList: [],
            childAgencyCodes: [],
            showMoreFields: false,
            paxName: "",
            bookRef: "",
            statusSearch: false,
            FlightItemtID: {
                cityTo: "cityTo",
                cityFrom: "cityFrom",
            },
            flightSearchCities: {
                cityTo: "",
                cityFrom: "",
            },
            flightSearchCityName: {
                cityTo: "",
                cityFrom: "",
            },
            firstIndex: 0,
            totalBookings: null,
            loading: false,
            globalSearch: null,
            rows: 10,
            selectedBooking: null,
            pageOptions: [10,20,50,100]
        };
    },
    mounted: function () {
        var vm = this;
        var hubUrls = HubServiceUrls;
        var hubUrl = hubUrls.hubConnection.baseUrl;
        var port = hubUrls.hubConnection.ipAddress;
        var childAgencyCodes = hubUrls.hubConnection.hubServices.getChildAgencyCodes;
        axios.get(hubUrl + port + childAgencyCodes, {
            headers: {
                Authorization: "Bearer " + window.localStorage.getItem("accessToken")
            }
        }).then(function (response) {
            vm.childAgencyCodes = response.data.map(function (item) {
                return {
                    disabled: false,
                    id: item.code,
                    name: item.code + ' : ' + item.name,
                    selected: false
                };
            });

            if ($('.dropdown-display').length == 0) {
                $('.dropdown-sin-2').dropdown({
                    data: vm.childAgencyCodes,
                    input: '<input type="text" maxLength="20" placeholder="Search">'
                });
            }
        }).catch(function (error) {
            console.log(error);
        });
        this.$nextTick(function () {
            $('#DepartureDateFrom').datepicker({
                defaultDate: "0d",
                changeYear: true,
                numberOfMonths: 1,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: dateFormat,
                onSelect: function (selectedDate) {
                    $("#DepartureDateTo").datepicker("option", "minDate", selectedDate);
                }
            });
            $('#DepartureDateFrom').datepicker("widget").css({ "z-index": 10000 });
            $('#DepartureDateTo').datepicker({
                defaultDate: "0d",
                changeMonth: true,
                changeYear: true,
                numberOfMonths: 1,
                showButtonPanel: false,
                dateFormat: dateFormat,
                onSelect: function (selectedDate) {
                    $("#DepartureDateFrom").datepicker("option", "maxDate", selectedDate);
                }
            });
            $('#DepartureDateTo').datepicker("widget").css({ "z-index": 10000 });

            $('#ArrivalDateFrom').datepicker({
                defaultDate: "0d",
                changeYear: true,
                numberOfMonths: 1,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: dateFormat,
                onSelect: function (selectedDate) {
                    $("#ArrivalDateTo").datepicker("option", "minDate", selectedDate);
                }
            });
            $('#ArrivalDateFrom').datepicker("widget").css({ "z-index": 10000 });
            $('#ArrivalDateTo').datepicker({
                defaultDate: "0d",
                changeMonth: true,
                changeYear: true,
                numberOfMonths: 1,
                showButtonPanel: false,
                dateFormat: dateFormat,
                onSelect: function (selectedDate) {
                    $("#ArrivalDateFrom").datepicker("option", "maxDate", selectedDate);
                }
            });
            $('#ArrivalDateTo').datepicker("widget").css({ "z-index": 10000 });

            $('#BookingDateFrom').datepicker({
                defaultDate: "0d",
                changeYear: true,
                numberOfMonths: 1,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: dateFormat,
                onSelect: function (selectedDate) {
                    $("#BookingDateTo").datepicker("option", "minDate", selectedDate);
                }
            });
            $('#BookingDateFrom').datepicker("widget").css({ "z-index": 10000 });
            var d = new Date();
            d.setDate(d.getDate() - 7);
            $("#BookingDateFrom").datepicker('setDate', new Date(d));
            this.bkngDateFrom = $('#BookingDateFrom').val();
            $('#BookingDateTo').datepicker({
                defaultDate: "0d",
                changeMonth: true,
                changeYear: true,
                numberOfMonths: 1,
                showButtonPanel: false,
                dateFormat: dateFormat,
                onSelect: function (selectedDate) {
                    $("#BookingDateFrom").datepicker("option", "maxDate", selectedDate);
                }
            });
            $('#BookingDateTo').datepicker("widget").css({ "z-index": 10000 });

            var sfbws = window.localStorage.getItem('sfbws');
            if (!isNullorUndefind(sfbws) && sfbws != 'null') {
                this.status = sfbws;
                this.getAirBookingsCriteria(0);
            }
            window.localStorage.setItem('sfbws', null);

        });
        this.updateSupplierList();
        this.configureAirports();
        this.getAirLines();
    },
    methods: {
        moment: function () {
            return moment();
        },
        airportLocationFromAirportCode: function () {
            return airportLocationFromAirportCode;
        },
        getAirLines: function () {
            var airLines = AirlinesDatas;
            this.airlineList = _.sortBy(airLines, 'A');
        },
        updateSupplierList: function () {
            var agencyNode = window.localStorage.getItem("agencyNode");
            if (agencyNode) {
                agencyNode = JSON.parse(atob(agencyNode));
                if (!isNullorUndefind(agencyNode)) {
                    try {
                        this.supplierList = agencyNode.loginNode.servicesList.find(x => x.name == 'Air').provider;
                    } catch (err) {
                        console.log('end supplier list');
                    }
                }
            }
        },
        configureAirports: function () {
            $(".airportCities").autocomplete({
                source: AirlinesTimezone,
                minLength: 3,
                position: { my: "left top", at: "left bottom" },
                change: function (event, ui) { if (!ui.item) { } },
                select: function (event, ui) {
                    if (ui.item.id != 0) {
                        if (ui.item.value.indexOf('No Match Found') < 0) {
                            this.value = ui.item.label;
                            $(this).parents('.txt_animation').addClass('focused');
                        }
                    }
                    return true;
                },
                focus: function () { return false; }
            });
        },
        getDateFromTextBox: function (dateValue) {
            var retDate = null;
            if (!stringIsNullorEmpty(dateValue)) {
                retDate = moment(dateValue, 'DD MMM YY,ddd').format('YYYY-MM-DD') + "T00:00:00.0+0000";
            }
            return retDate;
        },
        filterBookings: function (airBookings) {
            var statusCode = window.location.href.toLowerCase();
            if (statusCode.includes('status')) {
                statusCode = statusCode.split('status=')[1];
                airBookings = airBookings.filter(function (airBooking) { return airBooking.status.toLowerCase().includes(statusCode) });
            }
            airBookings = airBookings.sort(function (a, b) { return parseFloat(moment(b.bookDate).format('YYYYMMDDHHmmSSHHmm')) - parseFloat(moment(a.bookDate).format('YYYYMMDDHHmmSSHHmm')); });
            airBookings = _.sortBy(airBookings, function (airBooking) { return new moment(airBooking.bookDate); });
            return airBookings;
        },
        getBookingInfo: function (item) {
            window.sessionStorage.setItem("flightbookingId", item.referenceNumber);
            window.sessionStorage.setItem('sendbookmail', true);
            try { window.sessionStorage.setItem("selectCredential", JSON.stringify(item.selectCredential)); } catch (err) { }
            window.location.href = '/Flights/bookinginfo.html';
        },
        getAirBookingsCriteria: function () {

            var agencyCode = [];
            try {
                $.each($('li.dropdown-chose'), function (index, item) {
                    agencyCode.push($(item).data('value'));
                });
            } catch (err) { console.log('no child agency'); }
            var d = new Date();
            d.setDate(d.getDate() - 30);
            var minDate = this.getDateFromTextBox($('#BookingDateFrom').val());
            if (this.paxName || this.bookRef || this.status || this.supplier || this.journeyType || this.stops
                || this.flightSearchCities.cityFrom || this.flightSearchCities.cityTo || this.operatingAirLine
                || $('#BookingDateTo').val() || $('#DepartureDateFrom').val() || $('#DepartureDateTo').val()
                || $('#ArrivalDateFrom').val() || $('#ArrivalDateTo').val()) {
                minDate = this.getDateFromTextBox($('#BookingDateFrom').val());
            } else {
                if ($('#BookingDateFrom').val() == "") {
                    setTimeout(() => {
                        $("#BookingDateFrom").datepicker('setDate', new Date(d));
                    }, 50);
                    minDate = this.getDateFromTextBox(d)
                }
            }
            var request = {
                paxName: this.paxName || null,
                pnr: this.bookRef || null,
                statusCode: this.status || null,
                supplierId: this.supplier || null,
                journeyTypeId: this.journeyType || null,
                stopsNum: this.stops || null,
                origin: this.flightSearchCities.cityFrom || null,
                destination: this.flightSearchCities.cityTo || null,
                operatingAirLine: this.operatingAirLine || null,
                bookingFromDate: minDate,
                bookingToDate: this.getDateFromTextBox($('#BookingDateTo').val()),
                departureFromDate: this.getDateFromTextBox($('#DepartureDateFrom').val()),
                departureToDate: this.getDateFromTextBox($('#DepartureDateTo').val()),
                arrivalFromDate: this.getDateFromTextBox($('#ArrivalDateFrom').val()),
                arrivalToDate: this.getDateFromTextBox($('#ArrivalDateTo').val()),
                from:this.firstIndex,
                size:this.rows
            }

            if (agencyCode.length > 0) {
                request["agencyCodes"] = agencyCode;
            }

            // this.showBookingList = true;
            // this.sendRequest = true;
            $("#tableSearch").DataTable().destroy();
            var vm = this;
            var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = vm.commonStore.hubUrls.hubConnection.hubServices.flights.myBookingsConsolidateCriteria;

            // axios({
            //     method: "POST",
            //     url: hubUrl + serviceUrl,
            //     data: request,
            //     headers: {
            //         "Content-Type": "application/json",
            //         "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
            //     }
            // }).then((response) => {
            //     console.log("RESPONSE RECEIVED: ", response);
            //     if (response.data.length == 0) {
            //         vm.noDataFromResponse = true;
            //         vm.bookingList = null;
            //         vm.totalBookings = null;
            //         vm.loading = false;
            //     } else {
            //         vm.noDataFromResponse = false;
            //         vm.loading = false;
            //         vm.bookingList = response.data.bookingSearch;
            //         vm.totalBookings = response.data.totalBookings;
            //         if (vm.totalBookings < 50) {
            //             vm.pageOptions =  [10, 20, vm.totalBookings];
            //         } else if (vm.totalBookings >= 50 && vm.totalBookings < 100) {
            //             vm.pageOptions =  [10, 20, 50, vm.totalBookings];
            //         } else {
            //             vm.pageOptions =  [10, 20, 50, 100, vm.totalBookings];
            //         }
            //     }
            //     vm.sendRequest = false;
            //     vm.$nextTick(function () {
                var table = $("#tableSearch").DataTable({
                        dom: 'Blfrtip',
                        responsive: true,
                        processing: true,
                        serverSide: true,
                        destroy: true,
                        autoWidth: true,
                        lengthMenu: [[30, 60, 100, -1], [30, 60, 100, "All"]],
                        info: true,
                        language: {
                            "search": "Search PNR:"
                        },
                        pagingType: 'full_numbers',
                        ajax: function (data, callback, settings) {
                            request.from = settings._iDisplayStart
                            request.size = settings._iDisplayLength;
                            if(data.search.value) {
                                request.pnr = data.search.value;
                            }
                            if (request.size == -1) {
                                request.size = settings._iRecordsTotal;
                            }
                            $.ajax({
                                data: JSON.stringify(request),
                                headers: {
                                    "Content-Type": "application/json",
                                    "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
                                },
                                url: hubUrl + serviceUrl,
                                type: 'POST',
                                dataFilter: function(data){
                                    // vm.sendRequest = false;
                                    // vm.noDataFromResponse = false;
                                    // vm.loading = false
                                    var json = {
                                        data: jQuery.parseJSON( data ).bookingSearch || []
                                    }
                                    json.draw = vm.firstIndex;
                                    json.recordsTotal = jQuery.parseJSON( data ).totalBookings;
                                    json.recordsFiltered = jQuery.parseJSON( data ).totalBookings;
                                    callback( json );
                                    return JSON.stringify( json ); // return JSON string
                                },
                                // success: function (data) {
                                //     console.log(data)
                                //     vm.bookingList = data.data;
                                // }
                            });
                        },
                        "columnDefs": [
                            {
                                // The `data` parameter refers to the data for the cell (defined by the
                                // `data` option, which defaults to the column being worked with, in
                                // this case `data: 0`.
                                "render": function ( data, type, row ) {
                                    return vm.momentLocale(row.bookDate).format('DD MMM YYYY');
                                },
                                "targets": 0
                            },
                            {
                                "render": function ( data, type, row ) {
                                    if (!row.referenceNumber) {
                                        return "";
                                    }
                                    return row.referenceNumber;
                                },
                                "targets": 1
                            },
                            {
                                "render": function ( data, type, row ) {
                                    if (!row.supplierName) {
                                        return "";
                                    }
                                    return row.supplierName;
                                },
                                "targets": 2
                            },
                            {
                                "render": function ( data, type, row ) {
                                    if (!row.supplierReferenceNumber) {
                                        return "";
                                    }
                                    return row.supplierReferenceNumber;
                                },
                                "targets": 3
                            },
                            {
                                "render": function ( data, type, row ) {
                                    if (!row.trip && !row.tripId) {
                                        return "";
                                    }
                                    return row.trip + " (" + ['OW', 'RT', 'MC'][parseInt(row.tripId)-1]+")";
                                },
                                "targets": 4
                            },
                            {
                                "render": function ( data, type, row ) {
                                    if (!row.paxName && !row.paxCount) {
                                        return "";
                                    }
                                    return row.paxName + " x " + row.paxCount;
                                },
                                "targets": 5
                            },
                            {
                                "render": function ( data, type, row ) {
                                    return vm.momentLocale(row.departureDate).format('DD MMM YYYY');
                                },
                                "targets": 6
                            },
                            {
                                "render": function ( data, type, row ) {
                                    if (!row.status) {
                                        return "";
                                    }
                                    return row.status;
                                },
                                "targets": 7
                            },
                        ],
                        // columns: [
                        //     { "data": "bookDate" },
                        //     { "data": "referenceNumber" },
                        //     { "data": "supplierName" },
                        //     { "data": "supplierReferenceNumber" },
                        //     { "data": "trip" },
                        //     { "data": "paxName" },
                        //     { "data": "departureDate" },
                        //     { "data": "status" },
                        // ],
                        // columnDefs: [
                        //     { width: '200px', targets: 0 }, //column 1 out of 7
                        //     { width: '200px', targets: 1 }, //column 2 out of 7
                        //     { width: '200px', targets: 2 }, //column 3 out of 7
                        //     { width: '200px', targets: 3 }, //column 4 out of 7
                        //     { width: '200px', targets: 4 }, //column 5 out of 7
                        //     { width: '200px', targets: 5 }, //column 6 out of 7
                        //     { width: '200px', targets: 6 }, //column 6 out of 7
                        //     { width: '300px', targets: 7 }, //column 6 out of 7
                        //     // { width: '200px', targets: 6 }, //column 7 out of 7
                        // ],
                        "order": [
                            [0, "desc"],
                            [1, "desc"]
                        ],
                        buttons: [
                            {
                                extend: 'collection',
                                text: 'Export',
                                className: 'custom-html-collection',
                                buttons: [
                                    'copy',
                                    'csv',
                                    'excel',
                                    'print'
                                ]
                            }
                        ],
                        "drawCallback": function () {
                             $('[data-toggle="tooltip"]').tooltip(); 
                            }
                    });
                    $('#tableSearch tbody').on('click', 'tr', function () {
                        var data = $("#tableSearch").DataTable().row( this ).data();
                        vm.getBookingInfo(data);
                        // alert( 'You clicked on '+data[0]+'\'s row' );
                    } );
                // });

            // }).catch(function () {
            //     vm.noDataFromResponse = true;
            //     vm.bookingList = null;
            //     vm.totalBookings = null;
            //     vm.loading = false;
            //     vm.sendRequest = false;
            //     alertify.alert('Error', 'We have found some technical difficulties. Please contact admin!');
            // })
        },
        flightSearchcomplete(AirportCode, AirportName, item, direction, id) {

            var leg = item.replace(/^\D+/g, '');
            var from = 'cityFrom' + leg;
            var to = 'cityTo' + leg;
            if (direction == 'from') {
                if (this.flightSearchCities[to] == AirportCode) {
                    this.statusSearch = false;
                    this.flightSearchCityName[item] = '';
                    this.flightSearchCities[item] = '';
                    $("#" + id).focus();
                    alertify.alert(i18n.t('Flight_Search.Alert_Validation_Label'), i18n.t('Flight_Search.Dept_and_Arr_Alert_Msg'));

                } else {
                    this.flightSearchCities[item] = AirportCode;
                    this.flightSearchCityName[item] = AirportName;
                    this.statusSearch = true;
                }
            } else {
                if (this.flightSearchCities[from] == AirportCode) {
                    this.statusSearch = false;
                    this.flightSearchCityName[item] = '';
                    this.flightSearchCities[item] = '';
                    $("#" + id).focus();
                    alertify.alert(i18n.t('Flight_Search.Alert_Validation_Label'), i18n.t('Flight_Search.Dept_and_Arr_Alert_Msg'));
                } else {
                    this.flightSearchCities[item] = AirportCode;
                    this.flightSearchCityName[item] = AirportName;
                    this.statusSearch = true;
                }

            }


        },
        onSort(event) {
            this.lazyParams = event;
            this.onLazyEvent();
        },
        onPage: function (event) {
            var vm = this;
            vm.loading = true;
            vm.getAirBookingsCriteria(event.first);
        },
        searchTable: function() {
            
        },
        exportCSV() {
            this.$refs.dt.exportCSV();
        },
        momentLocale: function (date) {
            var localLocale = moment.utc(date);
            localLocale.locale(this.commonStore.selectedLanguage ? this.commonStore.selectedLanguage.code : 'en');
            return localLocale;
        }
    }
});

Vue.component("hotel-search-bookings", {
    data: function () {
        return {
            commonStore: vueCommonStore.state,
            statusCode: null,
            city: null,
            hotelName: null,
            bookingRef: null,
            supplierRef: null,
            bookingFromDate: null,
            bookingToDate: null,
            checkInFromDate: null,
            checkInToDate: null,
            checkOutFromDate: null,
            checkOutToDate: null,
            sendRequest: false,
            showBookingList: false,
            noDataFromResponse: false,
            bookingList: [],
            statusList: [
                { id: "HK", label: "Booking Success/Confirmed" },
                { id: "CP", label: "Confirmation Pending" },
                { id: "XX", label: "Cancelled" },
                { id: "RR", label: "Reconfirmed" },
                { id: "RQ", label: "Requested" },
                { id: "RF", label: "Request Failed" },
                { id: "XN", label: "Cancellation On request", }
            ],
            showStatusList: false,
            selectedStatus: [],
            statusSearchLabel: "All Status",
            dataTable: null,
            childAgencyCodes: []
        };
    },
    created: function () { },
    mounted: function () {
        var vm = this;
        var hubUrls = HubServiceUrls;
        var hubUrl = hubUrls.hubConnection.baseUrl;
        var port = hubUrls.hubConnection.ipAddress;
        var childAgencyCodes = hubUrls.hubConnection.hubServices.getChildAgencyCodes;
        axios.get(hubUrl + port + childAgencyCodes, {
            headers: {
                Authorization: "Bearer " + window.localStorage.getItem("accessToken")
            }
        }).then(function (response) {
            vm.childAgencyCodes = response.data.map(function (item) {
                return {
                    disabled: false,
                    id: item.code,
                    name: item.code + ' : ' + item.name,
                    selected: false
                };
            });

            if ($('.dropdown-display').length == 0) {
                $('.dropdown-sin-2').dropdown({
                    data: vm.childAgencyCodes,
                    input: '<input type="text" maxLength="20" placeholder="Search">'
                });
            }

        }).catch(function (error) {
            console.log(error);
        });

        this.$nextTick(function () {
            $('#BookingDateFrom').datepicker({
                defaultDate: "0d",
                changeYear: true,
                numberOfMonths: 1,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: dateFormat,
                onSelect: function (selectedDate) {
                    vm.bookingFromDate = selectedDate;
                    $("#BookingDateTo").datepicker("option", "minDate", selectedDate);
                }
            });
            $('#BookingDateFrom').datepicker("widget").css({ "z-index": 10000 });
            $('#BookingDateTo').datepicker({
                defaultDate: "0d",
                changeMonth: true,
                changeYear: true,
                numberOfMonths: 1,
                showButtonPanel: false,
                dateFormat: dateFormat,
                onSelect: function (selectedDate) {
                    vm.bookingToDate = selectedDate;
                    $("#BookingDateFrom").datepicker("option", "maxDate", selectedDate);
                }
            });
            $('#BookingDateTo').datepicker("widget").css({ "z-index": 10000 });

            $('#CheckinDateFrom').datepicker({
                defaultDate: "0d",
                changeYear: true,
                numberOfMonths: 1,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: dateFormat,
                onSelect: function (selectedDate) {
                    vm.checkInFromDate = selectedDate;
                    $("#CheckinDateTo").datepicker("option", "minDate", selectedDate);
                }
            });
            $('#CheckinDateFrom').datepicker("widget").css({ "z-index": 10000 });
            $('#CheckinDateTo').datepicker({
                defaultDate: "0d",
                changeYear: true,
                changeMonth: true,
                numberOfMonths: 1,
                showButtonPanel: false,
                dateFormat: dateFormat,
                onSelect: function (selectedDate) {
                    vm.checkInToDate = selectedDate;
                    $("#CheckinDateFrom").datepicker("option", "maxDate", selectedDate);
                }
            });
            $('#CheckoutDateTo').datepicker("widget").css({ "z-index": 10000 });

            $('#CheckoutDateFrom').datepicker({
                defaultDate: "0d",
                changeYear: true,
                numberOfMonths: 1,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: dateFormat,
                onSelect: function (selectedDate) {
                    vm.checkOutFromDate = selectedDate;
                    $("#CheckoutDateTo").datepicker("option", "minDate", selectedDate);
                }
            });
            $('#CheckoutDateFrom').datepicker("widget").css({ "z-index": 10000 });
            $('#CheckoutDateTo').datepicker({
                defaultDate: "0d",
                changeYear: true,
                changeMonth: true,
                numberOfMonths: 1,
                showButtonPanel: false,
                dateFormat: dateFormat,
                onSelect: function (selectedDate) {
                    vm.checkOutToDate = selectedDate;
                    $("#CheckoutDateFrom").datepicker("option", "maxDate", selectedDate);
                }
            });
            $('#CheckoutDateTo').datepicker("widget").css({ "z-index": 10000 });

            var sfbws = window.localStorage.getItem('sfbws');
            if (sfbws == "KK") {
                sfbws = "HK";
            }
            if (!isNullorUndefind(sfbws) && sfbws != 'null') {
                this.selectedStatus.push(sfbws);
                this.searchHotel();
            } else {
                this.selectedStatus = this.statusList.map(function (status) { return status.id; });
            }
            window.localStorage.setItem('sfbws', null);
        })
    },
    methods: {
        searchHotel: function () {
            var statusCode = this.selectedStatus || [];
            var city = $('#CityName').val();
            var hotelName = $('#hotelName').val();
            var bookingRef = $('#BookingRefNo').val();
            var supplierRef = $('#SupplierRefNo').val();
            var bookingFromDate = $('#BookingDateFrom').val() == "" ? "" : $('#BookingDateFrom').datepicker('getDate');
            var bookingToDate = $('#BookingDateTo').val() == "" ? "" : $('#BookingDateTo').datepicker('getDate');
            var checkInFromDate = $('#CheckinDateFrom').val() == "" ? "" : $('#CheckinDateFrom').datepicker('getDate');
            var checkInToDate = $('#CheckinDateTo').val() == "" ? "" : $('#CheckinDateTo').datepicker('getDate');
            var checkOutFromDate = $('#CheckoutDateFrom').val() == "" ? "" : $('#CheckoutDateFrom').datepicker('getDate');
            var checkOutToDate = $('#CheckoutDateTo').val() == "" ? "" : $('#CheckoutDateTo').datepicker('getDate');

            var agencyCode = [];
            try {
                $.each($('li.dropdown-chose'), function (index, item) {
                    agencyCode.push($(item).data('value'));
                });
            } catch (err) { console.log('no child agency'); }

            var request = {};

            if (statusCode.length > 0) {
                request["statusCodeList"] = statusCode;
            }
            if (city != "") {
                request["cityName"] = city;
            }
            if (hotelName != "") {
                request["hotelName"] = hotelName;
            }
            if (bookingRef != "") {
                request["bookingRef"] = bookingRef;

            }
            if (supplierRef != "") {
                request["supplierRef"] = supplierRef;

            }
            if (bookingFromDate != "") {
                request["bookingFromDate"] = moment(new Date(bookingFromDate)).format('YYYY-MM-DD');

            }
            if (bookingToDate != "") {
                request["bookingToDate"] = moment(new Date(bookingToDate)).add(1, 'days').format('YYYY-MM-DD');

            }
            if (checkInFromDate != "") {
                request["checkInFromDate"] = moment(new Date(checkInFromDate)).format('YYYY-MM-DD');

            }
            if (checkInToDate != "") {
                request["checkInToDate"] = moment(new Date(checkInToDate)).format('YYYY-MM-DD');
            }
            if (checkOutFromDate != "") {
                request["checkOutFromDate"] = moment(new Date(checkOutFromDate)).format('YYYY-MM-DD');

            }
            if (checkOutToDate != "") {
                request["checkOutToDate"] = moment(new Date(checkOutToDate)).format('YYYY-MM-DD');
            }

            if (agencyCode.length > 0) {
                request["agencyCodes"] = agencyCode;
            }

            this.showBookingList = true;
            this.sendRequest = true;
            $("#tableSearch").DataTable().destroy();
            var vm = this;

            var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = vm.commonStore.hubUrls.hubConnection.hubServices.hotels.myBookingsConsolidateCriteria;

            axios({
                method: "POST",
                url: hubUrl + serviceUrl,
                data: request,
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
                }
            }).then((response) => {
                console.log("RESPONSE RECEIVED: ", response);
                if (response.data.length == 0) {
                    vm.noDataFromResponse = true;
                    vm.bookingList = [];
                } else {
                    vm.noDataFromResponse = false;
                    vm.bookingList = response.data;
                }
                vm.sendRequest = false;
                vm.$nextTick(function () {
                    $("#tableSearch").DataTable({
                        responsive: true,
                        destroy: true,
                        autoWidth: false,
                        columnDefs: [
                            { width: '170px', targets: 0 }, //column 1 out of 8
                            { width: '130px', targets: 1 }, //column 2 out of 8
                            { width: '900px', targets: 2 }, //column 3 out of 8
                            { width: '200px', targets: 3 }, //column 4 out of 8
                            { width: '100px', targets: 4 }, //column 5 out of 8
                            { width: '200px', targets: 5 }, //column 6 out of 8
                            { width: '200px', targets: 6 }, //column 7 out of 8
                            { width: '600px', targets: 7 } //column 8 out of 8
                        ],
                        "order": [
                            [0, "desc"]
                        ],
                        language: dataTableLangauge[this.commonStore.selectedLanguage ? this.commonStore.selectedLanguage.code : 'en'],
                        "drawCallback": function () { $('[data-toggle="tooltip"]').tooltip(); }
                    }).draw();
                });

            }).catch(function () {
                vm.sendRequest = false;
                alertify.alert('Error', 'We have found some technical difficulties. Please contact admin!');
            })
        },
        getDateDetails: function (date) {
            var localLocale = moment(date);
            localLocale.locale(this.commonStore.selectedLanguage ? this.commonStore.selectedLanguage.code : 'en');
            return '<span data-toggle="tooltip" data-placement="top" title="" data-original-title="' +
                localLocale.format('DD MMM YYYY HH:mm') + '">' + localLocale.format('DD MMM YYYY') + '</span>';
        },
        getCityDetails: function (city) {
            var cityNameWithCountry = (city || "").split(" ").map(function (e) {
                return e.charAt(0).toUpperCase() + e.slice(1).toLowerCase();
            }).slice().join(" ");
            var cityName = cityNameWithCountry.split(",")[0];
            return '<span data-toggle="tooltip" data-placement="top" title="" data-original-title="' + cityNameWithCountry + '">' + cityName + '</span>';
        },
        getRefDetails: function (tooltip, data) {
            return '<span data-toggle="tooltip" data-placement="top" title="" data-original-title="' + tooltip + '">' + data + '</span>';
        },
        showStatusDropdown: function () {
            if (this.statusList.length != 1) {
                this.showStatusList = !this.showStatusList;
            }
        },
        getBookingInfo: function (item) {
            window.sessionStorage.setItem("bookingId", item.referenceNumber);
            window.sessionStorage.setItem("supplierId", item.supplierId);
            window.location.href = "/Hotels/hotelconfirmation.html";
        },
        hideStatus: function () {
            this.showStatusList = false;
        },
    },
    watch: {
        selectedStatus: function () {
            if (this.selectedStatus.length == this.statusList.length && this.statusList.length != 1) {
                this.statusSearchLabel = 'All Status';
            } else if (this.selectedStatus.length == 0) {
                this.statusSearchLabel = 'No status selected';
            } else if (this.selectedStatus.length > 1) {
                this.statusSearchLabel = this.selectedStatus.length + ' status selected'
            } else {
                var vm = this;
                this.statusSearchLabel = this.selectedStatus.map(function (id) {
                    return vm.statusList.filter(function (status) {
                        return id == status.id;
                    })[0].label;
                }).join(",");
            }
            this.autoCompleteResult = [];
            this.keywordSearch = "";
        },
        "commonStore.selectedLanguage": {
            handler: function () {
                $("#tableSearch").DataTable().destroy();

                $("#tableSearch").DataTable({
                    responsive: true,
                    destroy: true,
                    autoWidth: false,
                    columnDefs: [
                        { width: '170px', targets: 0 }, //column 1 out of 8
                        { width: '130px', targets: 1 }, //column 2 out of 8
                        { width: '900px', targets: 2 }, //column 3 out of 8
                        { width: '200px', targets: 3 }, //column 4 out of 8
                        { width: '100px', targets: 4 }, //column 5 out of 8
                        { width: '200px', targets: 5 }, //column 6 out of 8
                        { width: '200px', targets: 6 }, //column 7 out of 8
                        { width: '600px', targets: 7 } //column 8 out of 8
                    ],
                    "order": [
                        [0, "desc"]
                    ],
                    language: dataTableLangauge[this.commonStore.selectedLanguage ? this.commonStore.selectedLanguage.code : 'en'],
                    "drawCallback": function () { $('[data-toggle="tooltip"]').tooltip(); }
                }).draw();

                if (this.commonStore.selectedLanguage.code == 'ar') {
                    $("#BookingDateFrom").datepicker("option", $.datepicker.regional['ar']);
                    $("#BookingDateTo").datepicker("option", $.datepicker.regional['ar']);
                    $("#CheckinDateFrom").datepicker("option", $.datepicker.regional['ar']);
                    $("#CheckinDateTo").datepicker("option", $.datepicker.regional['ar']);
                    $("#CheckoutDateFrom").datepicker("option", $.datepicker.regional['ar']);
                    $("#CheckoutDateTo").datepicker("option", $.datepicker.regional['ar']);
                } else {
                    $("#BookingDateFrom").datepicker("option", $.datepicker.regional['en-GB']);
                    $("#BookingDateTo").datepicker("option", $.datepicker.regional['en-GB']);
                    $("#CheckinDateFrom").datepicker("option", $.datepicker.regional['en-GB']);
                    $("#CheckinDateTo").datepicker("option", $.datepicker.regional['en-GB']);
                    $("#CheckoutDateFrom").datepicker("option", $.datepicker.regional['en-GB']);
                    $("#CheckoutDateTo").datepicker("option", $.datepicker.regional['en-GB']);
                }
            },
            deep: true
        }

    },
    computed: {
        selectAll: {
            get: function () {
                return this.statusList ? this.selectedStatus.length == this.statusList.length : false;
            },
            set: function (value) {
                var selectedStatus = [];

                if (value) {
                    this.statusList.forEach(function (status) {
                        selectedStatus.push(status.id);
                    });
                }

                this.selectedStatus = selectedStatus;
            }
        }
    }
});

Vue.component("sightseeing-search-bookings", {
    data: function () {
        return {
            commonStore: vueCommonStore.state
        };
    }
});

Vue.component("wis-insurance-search-bookings", {
    data: function () {
        return {
            commonStore: vueCommonStore.state,
            bookingFromDate: null,
            bookingToDate: null,
            startFromDate: null,
            startToDate: null,
            bookingRef: null,
            policyNo: null,
            status: null,
            premiumName: null,
            statusLst: [{ name: "Select All", value: null }, { name: "On Request", value: "RQ" }, { name: "Issued", value: "ID" }, { name: "Cancelled", value: "XX" }],
            showBookingList: false,
            sendRequest: false,
            noDataFromResponse: false,
            bookingList: [],
            childAgencyCodes: [],
            pageContent: {
                Booking_From_Date_Label: 'Booking Date (From)',
                Booking_To_Date_Label: 'Booking Date (To)',
                Booking_Reference_Label: 'Booking Ref. No.',
                Start_From_Date_Label: 'Start Date (From)',
                Start_To_Date_Label: 'End Date (To)',
                Policy_Number_Label: 'Policy. No.',
                Status_Label: 'Status',
                Premium_Name_Label: 'Premium Name'
            }
        };
    },
    methods: {
        getBookingInfo: function (item) {
            window.sessionStorage.setItem("Insurance_Info", JSON.stringify(item));
            try { window.sessionStorage.setItem("selectCredential", JSON.stringify(item.selectCredential)); } catch (err) { }
            window.location.href = '/Insurance/wis/policyinfo.html';
        },
        getDateFromTextBox: function (dateValue) {
            var retDate = null;
            if (!stringIsNullorEmpty(dateValue)) {
                retDate = moment(dateValue, 'DD/MM/YYYY').format('YYYY-MM-DD') + "T00:00:00.0+0000";
            }
            return retDate;
        },
        getInsBookings: function () {
            var vm = this;
            var dataRequest = {
                statusCode: vm.status,
                supplierId: 23,
                journeyTypeId: null,
                bookingFromDate: vm.getDateFromTextBox($('#BookingDateFrom').val()),
                bookingToDate: vm.getDateFromTextBox($('#BookingDateTo').val()),
                startDate: vm.getDateFromTextBox($('#CheckinDateFrom').val()),
                endDate: vm.getDateFromTextBox($('#CheckinDateTo').val()),
                premiumName: (vm.premiumName == '' ? null : vm.premiumName),
                policyNo: (vm.policyNo == '' ? null : vm.policyNo),
                bookingRef: (vm.bookingRef == '' ? null : vm.bookingRef)
            }
            this.showBookingList = true;
            this.sendRequest = true;
            $("#tableSearch").DataTable().destroy();
            var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = vm.commonStore.hubUrls.hubConnection.hubServices.insurance.myBookingsCriteria;
            console.log(dataRequest);

            var agencyCode = [];
            try {
                $.each($('li.dropdown-chose'), function (index, item) {
                    agencyCode.push($(item).data('value'));
                });
            } catch (err) { console.log('no child agency'); }

            if (agencyCode.length > 0) {
                dataRequest["agencyCodes"] = agencyCode;
            }
            axios({
                method: "POST",
                url: hubUrl + serviceUrl,
                data: dataRequest,
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
                }
            }).then((response) => {
                console.log("RESPONSE RECEIVED: ", response);
                if (response.data.length == 0) {
                    vm.noDataFromResponse = false;
                    vm.bookingList = response.data;
                } else {
                    vm.noDataFromResponse = false;
                    vm.bookingList = response.data;
                }
                vm.sendRequest = false;
                vm.$nextTick(function () {
                    $("#tableSearch").DataTable({
                        responsive: true,
                        destroy: true,
                        autoWidth: false,
                        columnDefs: [
                            { width: '100px', targets: 0 }, //column 1 out of 5
                            { width: '250px', targets: 1 }, //column 2 out of 5
                            { width: '250px', targets: 2 }, //column 3 out of 5
                            { width: '200px', targets: 3 },  //column 4 out of 5
                            { width: '200px', targets: 4 }, //column 5 out of 5

                        ],
                        "order": [[0, "desc"]],
                        language: dataTableLangauge[this.commonStore.selectedLanguage ? this.commonStore.selectedLanguage.code : 'en'],
                        "drawCallback": function () { $('[data-toggle="tooltip"]').tooltip(); }
                    }).draw();
                });

            }).catch(function () {
                vm.sendRequest = false;
                alertify.alert('Error', 'We have found some technical difficulties. Please contact admin!');
            })

        }
    },
    mounted: function () {
        var vm = this;
        var hubUrls = HubServiceUrls;
        var hubUrl = hubUrls.hubConnection.baseUrl;
        var port = hubUrls.hubConnection.ipAddress;
        var childAgencyCodes = hubUrls.hubConnection.hubServices.getChildAgencyCodes;
        axios.get(hubUrl + port + childAgencyCodes, {
            headers: {
                Authorization: "Bearer " + window.localStorage.getItem("accessToken")
            }
        }).then(function (response) {
            vm.childAgencyCodes = response.data.map(function (item) {
                return {
                    disabled: false,
                    id: item.code,
                    name: item.code + ' : ' + item.name,
                    selected: false
                };
            });

            if ($('.dropdown-display').length == 0) {
                $('.dropdown-sin-2').dropdown({
                    data: vm.childAgencyCodes,
                    input: '<input type="text" maxLength="20" placeholder="Search">'
                });
            }
        }).catch(function (error) {
            console.log(error);
        });
        this.$nextTick(function () {
            $('#BookingDateFrom,#BookingDateTo,#CheckinDateFrom,#CheckinDateTo').datepicker({
                defaultDate: "0d",
                changeYear: true,
                numberOfMonths: 1,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: dateFormat,
                onSelect: function (selectedDate) {
                    var id = this.id;
                    if (id == 'BookingDateFrom') {
                        $("#BookingDateTo").datepicker("option", "minDate", selectedDate);
                        vm.bookingFromDate = selectedDate;
                        // vm.bookingToDate=selectedDate;
                    } else if (id == 'BookingDateTo') {
                        vm.bookingToDate = selectedDate;
                    } else if (id == 'CheckinDateFrom') {
                        $("#CheckinDateTo").datepicker("option", "minDate", selectedDate);
                        vm.startFromDate = selectedDate;
                        // vm.startToDate=selectedDate;
                    } else if (id == 'CheckinDateTo') {
                        vm.startToDate = selectedDate;
                    }

                }
            });
            if (vm.commonStore.selectedLanguage.code == 'ar') {
                $("#BookingDateFrom,#BookingDateTo,#CheckinDateFrom,#CheckinDateTo").datepicker("option", $.datepicker.regional['ar']);
            } else {
                $("#BookingDateFrom,#BookingDateTo,#CheckinDateFrom,#CheckinDateTo").datepicker("option", $.datepicker.regional['en-GB']);
            }
        });

    }
});

Vue.component("tp-insurance-search-bookings", {
    data: function () {
        return {
            commonStore: vueCommonStore.state,
            bookingFromDate: null,
            bookingToDate: null,
            startFromDate: null,
            startToDate: null,
            bookingRef: null,
            policyNo: null,
            status: null,
            premiumName: null,
            statusLst: [{ name: "Select All", value: null }, { name: "On Request", value: "RQ" }, { name: "Issued", value: "ID" }, { name: "Cancelled", value: "XX" }],
            showBookingList: false,
            sendRequest: false,
            noDataFromResponse: false,
            bookingList: [],
            childAgencyCodes: [],
            pageContent: {
                Booking_From_Date_Label: 'Booking Date (From)',
                Booking_To_Date_Label: 'Booking Date (To)',
                Booking_Reference_Label: 'Booking Ref. No.',
                Start_From_Date_Label: 'Start Date (From)',
                Start_To_Date_Label: 'End Date (To)',
                Policy_Number_Label: 'Policy. No.',
                Status_Label: 'Status',
                Premium_Name_Label: 'Premium Name'
            }
        };
    },
    methods: {
        getBookingInfo: function (item) {
            window.sessionStorage.setItem("Insurance_Info", JSON.stringify(item));
            try { window.sessionStorage.setItem("selectCredential", JSON.stringify(item.selectCredential)); } catch (err) { }
            window.location.href = '/Insurance/TuneProtect/policyinfo.html';
        },
        getDateFromTextBox: function (dateValue) {
            var retDate = null;
            if (!stringIsNullorEmpty(dateValue)) {
                retDate = moment(dateValue, 'DD/MM/YYYY').format('YYYY-MM-DD') + "T00:00:00.0+0000";
            }
            return retDate;
        },
        getInsBookings: function () {
            var vm = this;
            var dataRequest = {
                statusCode: vm.status,
                supplierId: 24,
                journeyTypeId: null,
                bookingFromDate: vm.getDateFromTextBox($('#BookingDateFrom').val()),
                bookingToDate: vm.getDateFromTextBox($('#BookingDateTo').val()),
                startDate: vm.getDateFromTextBox($('#CheckinDateFrom').val()),
                endDate: vm.getDateFromTextBox($('#CheckinDateTo').val()),
                premiumName: (vm.premiumName == '' ? null : vm.premiumName),
                policyNo: (vm.policyNo == '' ? null : vm.policyNo),
                bookingRef: (vm.bookingRef == '' ? null : vm.bookingRef)
            }
            this.showBookingList = true;
            this.sendRequest = true;
            $("#tableSearch").DataTable().destroy();
            var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = vm.commonStore.hubUrls.hubConnection.hubServices.insurance.myBookingsCriteria;
            console.log(dataRequest);

            var agencyCode = [];
            try {
                $.each($('li.dropdown-chose'), function (index, item) {
                    agencyCode.push($(item).data('value'));
                });
            } catch (err) { console.log('no child agency'); }

            if (agencyCode.length > 0) {
                dataRequest["agencyCodes"] = agencyCode;
            }
            axios({
                method: "POST",
                url: hubUrl + serviceUrl,
                data: dataRequest,
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
                }
            }).then((response) => {
                console.log("RESPONSE RECEIVED: ", response);
                if (response.data.length == 0) {
                    vm.noDataFromResponse = false;
                    vm.bookingList = response.data;
                } else {
                    vm.noDataFromResponse = false;
                    vm.bookingList = response.data;
                }
                vm.sendRequest = false;
                vm.$nextTick(function () {
                    $("#tableSearch").DataTable({
                        responsive: true,
                        destroy: true,
                        autoWidth: false,
                        columnDefs: [
                            { width: '100px', targets: 0 }, //column 1 out of 5
                            { width: '250px', targets: 1 }, //column 2 out of 5
                            { width: '250px', targets: 2 }, //column 3 out of 5
                            { width: '200px', targets: 3 }, //column 4 out of 5
                            { width: '200px', targets: 4 }, //column 5 out of 5

                        ],
                        "order": [
                            [0, "desc"]
                        ],
                        language: dataTableLangauge[this.commonStore.selectedLanguage ? this.commonStore.selectedLanguage.code : 'en'],
                        "drawCallback": function () { $('[data-toggle="tooltip"]').tooltip(); }
                    }).draw();
                });

            }).catch(function () {
                vm.sendRequest = false;
                alertify.alert('Error', 'We have found some technical difficulties. Please contact admin!');
            })

        }
    },
    mounted: function () {
        var vm = this;
        var hubUrls = HubServiceUrls;
        var hubUrl = hubUrls.hubConnection.baseUrl;
        var port = hubUrls.hubConnection.ipAddress;
        var childAgencyCodes = hubUrls.hubConnection.hubServices.getChildAgencyCodes;
        axios.get(hubUrl + port + childAgencyCodes, {
            headers: {
                Authorization: "Bearer " + window.localStorage.getItem("accessToken")
            }
        }).then(function (response) {
            vm.childAgencyCodes = response.data.map(function (item) {
                return {
                    disabled: false,
                    id: item.code,
                    name: item.code + ' : ' + item.name,
                    selected: false
                };
            });

            if ($('.dropdown-display').length == 0) {
                $('.dropdown-sin-2').dropdown({
                    data: vm.childAgencyCodes,
                    input: '<input type="text" maxLength="20" placeholder="Search">'
                });
            }
        }).catch(function (error) {
            console.log(error);
        });
        this.$nextTick(function () {
            $('#BookingDateFrom,#BookingDateTo,#CheckinDateFrom,#CheckinDateTo').datepicker({
                defaultDate: "0d",
                changeYear: true,
                numberOfMonths: 1,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: dateFormat,
                onSelect: function (selectedDate) {
                    var id = this.id;
                    if (id == 'BookingDateFrom') {
                        $("#BookingDateTo").datepicker("option", "minDate", selectedDate);
                        vm.bookingFromDate = selectedDate;
                        // vm.bookingToDate=selectedDate;
                    } else if (id == 'BookingDateTo') {
                        vm.bookingToDate = selectedDate;
                    } else if (id == 'CheckinDateFrom') {
                        $("#CheckinDateTo").datepicker("option", "minDate", selectedDate);
                        vm.startFromDate = selectedDate;
                        // vm.startToDate=selectedDate;
                    } else if (id == 'CheckinDateTo') {
                        vm.startToDate = selectedDate;
                    }

                }
            });
            if (vm.commonStore.selectedLanguage.code == 'ar') {
                $("#BookingDateFrom,#BookingDateTo,#CheckinDateFrom,#CheckinDateTo").datepicker("option", $.datepicker.regional['ar']);
            } else {
                $("#BookingDateFrom,#BookingDateTo,#CheckinDateFrom,#CheckinDateTo").datepicker("option", $.datepicker.regional['en-GB']);
            }
        });

    }
});

Vue.component("unitrust-insurance-search-bookings", {
    data: function () {
        return {
            commonStore: vueCommonStore.state,
            bookingFromDate: null,
            bookingToDate: null,
            startFromDate: null,
            startToDate: null,
            bookingRef: null,
            policyNo: null,
            status: null,
            premiumName: null,
            statusLst: [{ name: "Select All", value: null }, { name: "On Request", value: "RQ" }, { name: "Issued", value: "ID" }, { name: "Cancelled", value: "XX" }],
            showBookingList: false,
            sendRequest: false,
            noDataFromResponse: false,
            bookingList: [],
            childAgencyCodes: [],
            pageContent: {
                Booking_From_Date_Label: 'Booking Date (From)',
                Booking_To_Date_Label: 'Booking Date (To)',
                Booking_Reference_Label: 'Booking Ref. No.',
                Start_From_Date_Label: 'Start Date (From)',
                Start_To_Date_Label: 'End Date (To)',
                Policy_Number_Label: 'Policy. No.',
                Status_Label: 'Status',
                Premium_Name_Label: 'Premium Name'
            }
        };
    },
    methods: {
        getBookingInfo: function (item) {
            window.sessionStorage.setItem("Insurance_Info", JSON.stringify(item));
            try { window.sessionStorage.setItem("selectCredential", JSON.stringify(item.selectCredential)); } catch (err) { }
            window.location.href = '/Insurance/TuneProtect/policyinfo.html';
        },
        getDateFromTextBox: function (dateValue) {
            var retDate = null;
            if (!stringIsNullorEmpty(dateValue)) {
                retDate = moment(dateValue, 'DD/MM/YYYY').format('YYYY-MM-DD') + "T00:00:00.0+0000";
            }
            return retDate;
        },
        getInsBookings: function () {
            var vm = this;
            var dataRequest = {
                statusCode: vm.status,
                supplierId: 76,
                journeyTypeId: null,
                bookingFromDate: vm.getDateFromTextBox($('#BookingDateFrom').val()),
                bookingToDate: vm.getDateFromTextBox($('#BookingDateTo').val()),
                startDate: vm.getDateFromTextBox($('#CheckinDateFrom').val()),
                endDate: vm.getDateFromTextBox($('#CheckinDateTo').val()),
                premiumName: (vm.premiumName == '' ? null : vm.premiumName),
                policyNo: (vm.policyNo == '' ? null : vm.policyNo),
                bookingRef: (vm.bookingRef == '' ? null : vm.bookingRef)
            }
            this.showBookingList = true;
            this.sendRequest = true;
            $("#tableSearch").DataTable().destroy();
            var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = vm.commonStore.hubUrls.hubConnection.hubServices.insurance.myBookingsCriteria;
            console.log(dataRequest);

            var agencyCode = [];
            try {
                $.each($('li.dropdown-chose'), function (index, item) {
                    agencyCode.push($(item).data('value'));
                });
            } catch (err) { console.log('no child agency'); }

            if (agencyCode.length > 0) {
                dataRequest["agencyCodes"] = agencyCode;
            }
            axios({
                method: "POST",
                url: hubUrl + serviceUrl,
                data: dataRequest,
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
                }
            }).then((response) => {
                console.log("RESPONSE RECEIVED: ", response);
                if (response.data.length == 0) {
                    vm.noDataFromResponse = false;
                    vm.bookingList = response.data;
                } else {
                    vm.noDataFromResponse = false;
                    vm.bookingList = response.data;
                }
                vm.sendRequest = false;
                vm.$nextTick(function () {
                    $("#tableSearch").DataTable({
                        responsive: true,
                        destroy: true,
                        autoWidth: false,
                        columnDefs: [
                            { width: '100px', targets: 0 }, //column 1 out of 5
                            { width: '250px', targets: 1 }, //column 2 out of 5
                            { width: '250px', targets: 2 }, //column 3 out of 5
                            { width: '200px', targets: 3 }, //column 4 out of 5
                            { width: '200px', targets: 4 }, //column 5 out of 5

                        ],
                        "order": [
                            [0, "desc"]
                        ],
                        language: dataTableLangauge[this.commonStore.selectedLanguage ? this.commonStore.selectedLanguage.code : 'en'],
                        "drawCallback": function () { $('[data-toggle="tooltip"]').tooltip(); }
                    }).draw();
                });

            }).catch(function () {
                vm.sendRequest = false;
                alertify.alert('Error', 'We have found some technical difficulties. Please contact admin!');
            })

        }
    },
    mounted: function () {
        var vm = this;
        var hubUrls = HubServiceUrls;
        var hubUrl = hubUrls.hubConnection.baseUrl;
        var port = hubUrls.hubConnection.ipAddress;
        var childAgencyCodes = hubUrls.hubConnection.hubServices.getChildAgencyCodes;
        axios.get(hubUrl + port + childAgencyCodes, {
            headers: {
                Authorization: "Bearer " + window.localStorage.getItem("accessToken")
            }
        }).then(function (response) {
            vm.childAgencyCodes = response.data.map(function (item) {
                return {
                    disabled: false,
                    id: item.code,
                    name: item.code + ' : ' + item.name,
                    selected: false
                };
            });

            if ($('.dropdown-display').length == 0) {
                $('.dropdown-sin-2').dropdown({
                    data: vm.childAgencyCodes,
                    input: '<input type="text" maxLength="20" placeholder="Search">'
                });
            }
        }).catch(function (error) {
            console.log(error);
        });
        this.$nextTick(function () {
            $('#BookingDateFrom,#BookingDateTo,#CheckinDateFrom,#CheckinDateTo').datepicker({
                defaultDate: "0d",
                changeYear: true,
                numberOfMonths: 1,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: dateFormat,
                onSelect: function (selectedDate) {
                    var id = this.id;
                    if (id == 'BookingDateFrom') {
                        $("#BookingDateTo").datepicker("option", "minDate", selectedDate);
                        vm.bookingFromDate = selectedDate;
                        // vm.bookingToDate=selectedDate;
                    } else if (id == 'BookingDateTo') {
                        vm.bookingToDate = selectedDate;
                    } else if (id == 'CheckinDateFrom') {
                        $("#CheckinDateTo").datepicker("option", "minDate", selectedDate);
                        vm.startFromDate = selectedDate;
                        // vm.startToDate=selectedDate;
                    } else if (id == 'CheckinDateTo') {
                        vm.startToDate = selectedDate;
                    }

                }
            });
            if (vm.commonStore.selectedLanguage.code == 'ar') {
                $("#BookingDateFrom,#BookingDateTo,#CheckinDateFrom,#CheckinDateTo").datepicker("option", $.datepicker.regional['ar']);
            } else {
                $("#BookingDateFrom,#BookingDateTo,#CheckinDateFrom,#CheckinDateTo").datepicker("option", $.datepicker.regional['en-GB']);
            }
        });

    }
});

Vue.component("transfer-search-bookings", {
    data: function () {
        return {
            commonStore: vueCommonStore.state
        };
    }
});

Vue.component("data-table-component", {
    data: function () {
        return {
            commonStore: vueCommonStore.state
        };
    },
    props: {
        showBookingList: Boolean,
        sendRequest: Boolean,
        noDataFromResponse: Boolean,
        bookingList: Array,
        getBookingInfo: Function
    },
    methods: {
        momentLocale: function (date) {
            var localLocale = moment.utc(date);
            localLocale.locale(this.commonStore.selectedLanguage ? this.commonStore.selectedLanguage.code : 'en');
            return localLocale;
        },
        moment: function () {
            return moment();
        },
        airportLocationFromAirportCode: function (item) {
            return airportLocationFromAirportCode(item);
        },
    },
});