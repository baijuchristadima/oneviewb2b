//search page components
var countryList = [
    { code: "AF", name: "Afghanistan" },
    { code: "AL", name: "Albania" },
    { code: "DZ", name: "Algeria" },
    { code: "AS", name: "American Samoa" },
    { code: "AD", name: "Andorra" },
    { code: "AO", name: "Angola" },
    { code: "AI", name: "Anguilla" },
    { code: "AQ", name: "Antartica" },
    { code: "AG", name: "Antigua And Barbuda" },
    { code: "AR", name: "Argentina" },
    { code: "AM", name: "Armenia" },
    { code: "AW", name: "Aruba" },
    { code: "AU", name: "Australia" },
    { code: "AT", name: "Austria" },
    { code: "AZ", name: "Azerbaijan" },
    { code: "BS", name: "Bahamas" },
    { code: "BH", name: "Bahrain" },
    { code: "BD", name: "Bangladesh" },
    { code: "BB", name: "Barbados" },
    { code: "BY", name: "Belarus" },
    { code: "BE", name: "Belgium" },
    { code: "BZ", name: "Belize" },
    { code: "BJ", name: "Benin" },
    { code: "BM", name: "Bermuda" },
    { code: "BT", name: "Bhutan" },
    { code: "BO", name: "Bolivia" },
    { code: "BQ", name: "Bonaire St Eustatius And Saba " },
    { code: "BA", name: "Bosnia-Herzegovina" },
    { code: "BW", name: "Botswana" },
    { code: "BR", name: "Brazil" },
    { code: "IO", name: "British Indian Ocean Territory" },
    { code: "BN", name: "Brunei Darussalam" },
    { code: "BG", name: "Bulgaria" },
    { code: "BF", name: "Burkina Faso" },
    { code: "BI", name: "Burundi" },
    { code: "KH", name: "Cambodia" },
    { code: "CM", name: "Cameroon-Republic Of" },
    { code: "CB", name: "Canada Buffer" },
    { code: "CA", name: "Canada" },
    { code: "CV", name: "Cape Verde-Republic Of" },
    { code: "KY", name: "Cayman Islands" },
    { code: "CF", name: "Central African Republic" },
    { code: "TD", name: "Chad" },
    { code: "CL", name: "Chile" },
    { code: "CN", name: "China" },
    { code: "CX", name: "Christmas Island" },
    { code: "CC", name: "Cocos Islands" },
    { code: "CO", name: "Colombia" },
    { code: "KM", name: "Comoros" },
    { code: "CG", name: "Congo Brazzaville" },
    { code: "CD", name: "Congo The Democratic Rep Of" },
    { code: "CK", name: "Cook Islands" },
    { code: "CR", name: "Costa Rica" },
    { code: "CI", name: "Cote D Ivoire" },
    { code: "HR", name: "Croatia" },
    { code: "CU", name: "Cuba" },
    { code: "CW", name: "Curacao" },
    { code: "CY", name: "Cyprus" },
    { code: "CZ", name: "Czech Republic" },
    { code: "DK", name: "Denmark" },
    { code: "DJ", name: "Djibouti" },
    { code: "DM", name: "Dominica" },
    { code: "DO", name: "Dominican Republic" },
    { code: "TP", name: "East Timor Former Code)" },
    { code: "EC", name: "Ecuador" },
    { code: "EG", name: "Egypt" },
    { code: "SV", name: "El Salvador" },
    { code: "EU", name: "Emu European Monetary Union" },
    { code: "GQ", name: "Equatorial Guinea" },
    { code: "ER", name: "Eritrea" },
    { code: "EE", name: "Estonia" },
    { code: "ET", name: "Ethiopia" },
    { code: "FK", name: "Falkland Islands" },
    { code: "FO", name: "Faroe Islands" },
    { code: "ZZ", name: "Fictitious Points" },
    { code: "FJ", name: "Fiji" },
    { code: "FI", name: "Finland" },
    { code: "FR", name: "France" },
    { code: "GF", name: "French Guiana" },
    { code: "PF", name: "French Polynesia" },
    { code: "GA", name: "Gabon" },
    { code: "GM", name: "Gambia" },
    { code: "GE", name: "Georgia" },
    { code: "DE", name: "Germany" },
    { code: "GH", name: "Ghana" },
    { code: "GI", name: "Gibraltar" },
    { code: "GR", name: "Greece" },
    { code: "GL", name: "Greenland" },
    { code: "GD", name: "Grenada" },
    { code: "GP", name: "Guadeloupe" },
    { code: "GU", name: "Guam" },
    { code: "GT", name: "Guatemala" },
    { code: "GW", name: "Guinea Bissau" },
    { code: "GN", name: "Guinea" },
    { code: "GY", name: "Guyana" },
    { code: "HT", name: "Haiti" },
    { code: "HN", name: "Honduras" },
    { code: "HK", name: "Hong Kong" },
    { code: "HU", name: "Hungary" },
    { code: "IS", name: "Iceland" },
    { code: "IN", name: "India" },
    { code: "ID", name: "Indonesia" },
    { code: "IR", name: "Iran" },
    { code: "IQ", name: "Iraq" },
    { code: "IE", name: "Ireland-Republic Of" },
    { code: "IL", name: "Israel" },
    { code: "IT", name: "Italy" },
    { code: "JM", name: "Jamaica" },
    { code: "JP", name: "Japan" },
    { code: "JO", name: "Jordan" },
    { code: "KZ", name: "Kazakhstan" },
    { code: "KE", name: "Kenya" },
    { code: "KI", name: "Kiribati" },
    { code: "KP", name: "Korea Dem Peoples Rep Of" },
    { code: "KR", name: "Korea Republic Of" },
    { code: "KW", name: "Kuwait" },
    { code: "KG", name: "Kyrgyzstan" },
    { code: "LA", name: "Lao Peoples Dem Republic" },
    { code: "LV", name: "Latvia" },
    { code: "LB", name: "Lebanon" },
    { code: "LS", name: "Lesotho" },
    { code: "LR", name: "Liberia" },
    { code: "LY", name: "Libya" },
    { code: "LI", name: "Liechtenstein" },
    { code: "LT", name: "Lithuania" },
    { code: "LU", name: "Luxembourg" },
    { code: "MO", name: "Macao -Sar Of China-" },
    { code: "MK", name: "Macedonia -Fyrom-" },
    { code: "MG", name: "Madagascar" },
    { code: "MW", name: "Malawi" },
    { code: "MY", name: "Malaysia" },
    { code: "MV", name: "Maldives Island" },
    { code: "ML", name: "Mali" },
    { code: "MT", name: "Malta" },
    { code: "MH", name: "Marshall Islands" },
    { code: "MQ", name: "Martinique" },
    { code: "MR", name: "Mauritania" },
    { code: "MU", name: "Mauritius Island" },
    { code: "YT", name: "Mayotte" },
    { code: "MB", name: "Mexico Buffer" },
    { code: "MX", name: "Mexico" },
    { code: "FM", name: "Micronesia" },
    { code: "MD", name: "Moldova" },
    { code: "MC", name: "Monaco" },
    { code: "MN", name: "Mongolia" },
    { code: "ME", name: "Montenegro" },
    { code: "MS", name: "Montserrat" },
    { code: "MA", name: "Morocco" },
    { code: "MZ", name: "Mozambique" },
    { code: "MM", name: "Myanmar" },
    { code: "NA", name: "Namibia" },
    { code: "NR", name: "Nauru" },
    { code: "NP", name: "Nepal" },
    { code: "AN", name: "Netherlands Antilles" },
    { code: "NL", name: "Netherlands" },
    { code: "NC", name: "New Caledonia" },
    { code: "NZ", name: "New Zealand" },
    { code: "NI", name: "Nicaragua" },
    { code: "NE", name: "Niger" },
    { code: "NG", name: "Nigeria" },
    { code: "NU", name: "Niue" },
    { code: "NF", name: "Norfolk Island" },
    { code: "MP", name: "Northern Mariana Islands" },
    { code: "NO", name: "Norway" },
    { code: "OM", name: "Oman" },
    { code: "PK", name: "Pakistan" },
    { code: "PW", name: "Palau Islands" },
    { code: "PS", name: "Palestine - State Of" },
    { code: "PA", name: "Panama" },
    { code: "PG", name: "Papua New Guinea" },
    { code: "PY", name: "Paraguay" },
    { code: "PE", name: "Peru" },
    { code: "PH", name: "Philippines" },
    { code: "PL", name: "Poland" },
    { code: "PT", name: "Portugal" },
    { code: "PR", name: "Puerto Rico" },
    { code: "QA", name: "Qatar" },
    { code: "RE", name: "Reunion Island" },
    { code: "RO", name: "Romania" },
    { code: "RU", name: "Russia" },
    { code: "XU", name: "Russia" },
    { code: "RW", name: "Rwanda" },
    { code: "WS", name: "Samoa-Independent State Of" },
    { code: "SM", name: "San Marino" },
    { code: "ST", name: "Sao Tome And Principe Islands " },
    { code: "SA", name: "Saudi Arabia" },
    { code: "SN", name: "Senegal" },
    { code: "RS", name: "Serbia" },
    { code: "SC", name: "Seychelles Islands" },
    { code: "SL", name: "Sierra Leone" },
    { code: "SG", name: "Singapore" },
    { code: "SX", name: "Sint Maarten" },
    { code: "SK", name: "Slovakia" },
    { code: "SI", name: "Slovenia" },
    { code: "SB", name: "Solomon Islands" },
    { code: "SO", name: "Somalia" },
    { code: "ZA", name: "South Africa" },
    { code: "SS", name: "South Sudan" },
    { code: "ES", name: "Spain" },
    { code: "LK", name: "Sri Lanka" },
    { code: "SH", name: "St. Helena Island" },
    { code: "KN", name: "St. Kitts" },
    { code: "LC", name: "St. Lucia" },
    { code: "PM", name: "St. Pierre And Miquelon" },
    { code: "VC", name: "St. Vincent" },
    { code: "SD", name: "Sudan" },
    { code: "SR", name: "Suriname" },
    { code: "SZ", name: "Swaziland" },
    { code: "SE", name: "Sweden" },
    { code: "CH", name: "Switzerland" },
    { code: "SY", name: "Syrian Arab Republic" },
    { code: "TW", name: "Taiwan" },
    { code: "TJ", name: "Tajikistan" },
    { code: "TZ", name: "Tanzania-United Republic" },
    { code: "TH", name: "Thailand" },
    { code: "TL", name: "Timor Leste" },
    { code: "TG", name: "Togo" },
    { code: "TK", name: "Tokelau" },
    { code: "TO", name: "Tonga" },
    { code: "TT", name: "Trinidad And Tobago" },
    { code: "TN", name: "Tunisia" },
    { code: "TR", name: "Turkey" },
    { code: "TM", name: "Turkmenistan" },
    { code: "TC", name: "Turks And Caicos Islands" },
    { code: "TV", name: "Tuvalu" },
    { code: "UM", name: "U.S. Minor Outlying Islands" },
    { code: "UG", name: "Uganda" },
    { code: "UA", name: "Ukraine" },
    { code: "AE", name: "United Arab Emirates" },
    { code: "GB", name: "United Kingdom" },
    { code: "US", name: "United States Of America" },
    { code: "UY", name: "Uruguay" },
    { code: "UZ", name: "Uzbekistan" },
    { code: "VU", name: "Vanuatu" },
    { code: "VA", name: "Vatican" },
    { code: "VE", name: "Venezuela" },
    { code: "VN", name: "Vietnam" },
    { code: "VG", name: "Virgin Islands-British" },
    { code: "VI", name: "Virgin Islands-United States" },
    { code: "WF", name: "Wallis And Futuna Islands" },
    { code: "EH", name: "Western Sahara" },
    { code: "YE", name: "Yemen Republic" },
    { code: "ZM", name: "Zambia" },
    { code: "ZW", name: "Zimbabwe" },
]

var airlineListWithSupplierCodes = 
[{name: "AIRARABIA", airlineCode: "3L", supplierCode: "71", isGDS: false},
{name: "AIRARABIA", airlineCode: "3O", supplierCode: "49", isGDS: false},
{name: "AIRARABIA", airlineCode: "E5", supplierCode: "50", isGDS: false},
{name: "AIRARABIA", airlineCode: "G9", supplierCode: "44", isGDS: false},
{name: "AIR BLUE", airlineCode: "PA", supplierCode: "58", isGDS: false},
{name: "AIR SIAL", airlineCode: "PF", supplierCode: "74", isGDS: false},
{name: "ALJAZEERA", airlineCode: "J9", supplierCode: "69", isGDS: false},
{name: "AMADEUS", airlineCode: "1A", supplierCode: "1", isGDS: true},
{name: "EMIRATES", airlineCode: "EK", supplierCode: "83", isGDS: false},
{name: "FLY DUBAI", airlineCode: "FZ", supplierCode: "54", isGDS: false},
{name: "OMAN AIR", airlineCode: "WY", supplierCode: "62", isGDS: false},
{name: "PIA", airlineCode: "PK", supplierCode: "57", isGDS: false},
{name: "SABRE", airlineCode: "1S", supplierCode: "55", isGDS: true},
{name: "SABRE", airlineCode: "1W", supplierCode: "55", isGDS: true},
{name: "SALAM AIR", airlineCode: "OV", supplierCode: "18", isGDS: false},
{name: "TARCO", airlineCode: "3T", supplierCode: "73", isGDS: false}]

Vue.component('flight-autocomplete', {
    data() {
        return {
            KeywordSearch: '',
            resultItemsarr: [],
            autoCompleteProgress: false,
            loading: false,
            highlightIndex: 0
        }
    },
    props: {
        itemText: String,
        itemId: String,
        status: Boolean,
        placeHolderText: String,
        returnValue: String,
        tripType: String,
        id: {
            type: String,
            default: '',
            required: false
        },

    },
    template: `<div class="autocomplete flightauto"  v-click-outside="hideautocompelte">
      <input type="text" :placeholder="placeHolderText" :id="id" :data-aircode="itemId" autocomplete="off"  
      :value="KeywordSearch" class="from_area_txt from_area_txt5" 
      :class="{ 'loading-circle' : (KeywordSearch && KeywordSearch.length > 2), 'hide-loading-circle': resultItemsarr.length > 0 || resultItemsarr.length == 0 && !loading  }" 
      @input="function(event){onSelectedAutoCompleteEvent(event.target.value,event)}"  
      @keydown.down="down"
      @keydown.up="up"      
      @keydown.esc="autoCompleteProgress=false"
      @keydown.enter="onSelected(resultItemsarr[highlightIndex].code,resultItemsarr[highlightIndex].label,resultItemsarr[highlightIndex].country)"
      @click="autocomplete"
      @keydown.tab="tabclick(resultItemsarr[highlightIndex])"/>
      <ul ref="searchautocomplete" class="autocomplete-results" v-if="autoCompleteProgress&&resultItemsarr.length > 0">
          <li ref="options" :class="{'autocomplete-result-active' : i == highlightIndex}" class="autocomplete-result" v-for="(item,i) in resultItemsarr" :key="i" @click="onSelected(item.code, item.label, item.country)">
              {{ item.label }}
          </li>
      </ul>
  </div>`,
    methods: {
        up: function() {
            if (this.autoCompleteProgress) {
                if (this.highlightIndex > 0) {
                    this.highlightIndex--
                }
            } else {
                this.autoCompleteProgress = true;
            }
            this.fixScrolling();
        },

        down: function() {
            if (this.autoCompleteProgress) {
                if (this.highlightIndex < this.resultItemsarr.length - 1) {
                    this.highlightIndex++
                } else if (this.highlightIndex == this.resultItemsarr.length - 1) {
                    this.highlightIndex = 0;
                }
            } else {
                this.autoCompleteProgress = true;
            }
            this.fixScrolling();
        },
        fixScrolling: function() {
            if (this.$refs.options[this.highlightIndex]) {
                var liH = this.$refs.options[this.highlightIndex].clientHeight;
                if (liH == 50) {
                    liH = 32;
                }
                if (liH * this.highlightIndex >= 32 || this.highlightIndex == 0) {
                    this.$refs.searchautocomplete.scrollTop = liH * this.highlightIndex;
                }
            }

        },
        onSelectedAutoCompleteEvent: _.debounce(function(keywordEntered) {
            var self = this;
            if (keywordEntered.length > 2) {
                this.autoCompleteProgress = true;
                this.loading = true;
                var cityName = keywordEntered;
                var cityarray = cityName.split(' ');
                var uppercaseFirstLetter = "";
                for (var k = 0; k < cityarray.length; k++) {
                    uppercaseFirstLetter += cityarray[k].charAt(0).toUpperCase() + cityarray[k].slice(1).toLowerCase()
                    if (k < cityarray.length - 1) {
                        uppercaseFirstLetter += " ";
                    }
                }
                uppercaseFirstLetter = "*" + uppercaseFirstLetter + "*";
                var lowercaseLetter = "*" + cityName.toLowerCase() + "*";
                var uppercaseLetter = "*" + cityName.toUpperCase() + "*";
                var query = {
                    query: {
                        bool: {
                            should: [{
                                bool: {
                                    should: [{
                                        wildcard: {
                                            iata_code: { value: uppercaseFirstLetter, boost: 3.0 }
                                        }
                                    }, {
                                        wildcard: {
                                            iata_code: { value: uppercaseLetter, boost: 3.0 }
                                        }
                                    }, {
                                        wildcard: {
                                            iata_code: { value: lowercaseLetter, boost: 3.0 }
                                        }
                                    }]
                                }
                            }, {
                                bool: {
                                    should: [{
                                        wildcard: {
                                            municipality: uppercaseFirstLetter
                                        }
                                    }, {
                                        wildcard: {
                                            municipality: uppercaseLetter
                                        }
                                    }, {
                                        wildcard: {
                                            municipality: lowercaseLetter
                                        }
                                    }]
                                }
                            }, {
                                bool: {
                                    should: [{
                                        wildcard: {
                                            name: uppercaseFirstLetter
                                        }
                                    }, {
                                        wildcard: {
                                            name: uppercaseLetter
                                        }
                                    }, {
                                        wildcard: {
                                            name: lowercaseLetter
                                        }
                                    }]
                                }
                            }],
                            must: [{
                                "exists": {
                                    "field": "iata_code"
                                }
                            }]
                        }
                    }
                };

                var config = {
                    axiosConfig: {
                        headers: {
                            "Content-Type": "application/json"
                        },
                        method: "post",
                        url: HubServiceUrls.elasticSearch.url,
                        data: query
                    },
                    successCallback: function (resp) {
                        finalResult = [];
                        var hits = resp.data.hits.hits;
                        var Citymap = new Map();
                        for (var i = 0; i < hits.length; i++) {
                            Citymap.set(hits[i]._source.iata_code, hits[i]._source);
                        }
                        var get_values = Citymap.values();
                        var Cityvalues = [];
                        for (var ele of get_values) {
                            Cityvalues.push(ele);
                        }
                        var results = SortInputFirstFlight(cityName, Cityvalues);
                        for (var i = 0; i < results.length; i++) {
                            finalResult.push({
                                "code": results[i].iata_code,
                                "label": results[i].name + ", " + results[i].iso_country + '(' + results[i].iata_code + ')',
                                "municipality": results[i].municipality,
                                "country": results[i].iso_country
                            });
                        }
                        var newData = [];
                        finalResult.forEach(function(item, index) {
                            if (item.label.toLowerCase().indexOf(keywordEntered.toLowerCase()) >= 0 || item.code.toLowerCase().includes(keywordEntered.toLowerCase()) || item.municipality.toLowerCase().indexOf(keywordEntered.toLowerCase()) >= 0) {
                                newData.push(item);
                            }
                        });
                        self.resultItemsarr = newData;
                        self.autoCompleteProgress = true;
                        self.loading = false;
                    },
                    errorCallback: function (error) { },
                    showAlert: false
                };

                mainAxiosRequest(config);
                // var client = new elasticsearch.Client({
                //     host: [{
                //         host: HubServiceUrls.elasticSearch.elasticsearchHost,
                //         auth: HubServiceUrls.elasticSearch.auth,
                //         protocol: HubServiceUrls.elasticSearch.protocol,
                //         port: HubServiceUrls.elasticSearch.port,
                //         requestTimeout: 60000
                //     }],
                //     log: 'trace'
                // });
                // client.search({
                //     index: 'airport_info',
                //     size: 150,
                //     timeout: "3000ms",
                //     body: query
                // }).then(function(resp) {
                    
                // })
            } else {
                this.autoCompleteProgress = false;
                this.resultItemsarr = [];
                this.loading = false;
            }
            this.KeywordSearch = keywordEntered;
        }, 500),
        onSelected: function(code, label, country) {
            this.autoCompleteProgress = true;
            this.resultItemsarr = [];
            this.KeywordSearch = label;
            if (this.tripType != 'M') {
                if (this.id == 'txtFrom') {
                    $("#txtTo").focus();
                } else if (this.id == 'txtTo') {
                    $("#deptDate01").focus();
                }

            } else {
                if (this.id == 'txtFrom') {
                    $("#txtTo").focus();
                } else if (this.id == 'txtTo') {
                    $("#deptDate01").focus();
                } else if (this.id == 'txtLeg2From') {
                    $("#txtLeg2To").focus();
                } else if (this.id == 'txtLeg2To') {
                    $("#txtLeg2Date").focus();
                } else if (this.id == 'txtLeg3From') {
                    $("#txtLeg3To").focus();
                } else if (this.id == 'txtLeg3To') {
                    $("#txtLeg3Date").focus();
                } else if (this.id == 'txtLeg4From') {
                    $("#txtLeg4To").focus();
                } else if (this.id == 'txtLeg4To') {
                    $("#txtLeg4Date").focus();
                } else if (this.id == 'txtLeg5From') {
                    $("#txtLeg5To").focus();
                } else if (this.id == 'txtLeg5To') {
                    $("#txtLeg5Date").focus();
                } else if (this.id == 'txtLeg6From') {
                    $("#txtLeg6To").focus();
                } else if (this.id == 'txtLeg6To') {
                    $("#txtLeg6Date").focus();
                }

            }
            this.$emit('air-search-completed', code, label, this.itemId, this.itemText, this.id, country);

        },
        hideautocompelte: function() {
            this.autoCompleteProgress = false;
        },
        autocomplete: function() {
            this.autoCompleteProgress = true;
        },
        tabclick: function(item) {
            if (!item) {

            } else {
                this.onSelected(item.code, item.label, item.country);
            }
        }

    },
    watch: {
        status: function() {
            this.KeywordSearch = this.returnValue;
        }
    }

});
Vue.component("flight-search", {
    data: function() {
        return {
            commonStore: vueCommonStore.state,
            showSupplierList: false,
            supplierList: [],
            selectedSuppliers: [],
            supplierSearchLabel: "Select All",
            selected_adults: 1,
            selected_children: 0,
            selected_infant: 0,
            selected_cabin: 'Y',
            triptype: "O",
            totalAllowdPax: 9,
            status: true,
            Totaltravaller: "1 Passenger, Economy",
            flightSearchCities: { cityFrom0: '', cityTo0: '', cityFrom1: '', cityTo1: '', cityFrom2: '', cityTo2: '', cityFrom3: '', cityTo3: '', cityFrom4: '', cityTo4: '', cityFrom5: '', cityTo5: '' },
            flightSearchCityName: { cityFrom0: '', cityTo0: '', cityFrom1: '', cityTo1: '', cityFrom2: '', cityTo2: '', cityFrom3: '', cityTo3: '', cityFrom4: '', cityTo4: '', cityFrom5: '', cityTo5: '' },
            legcount: 1,
            placeholders: {
                From: 'Enter Departure Airport or City',
                To: 'Enter Destination Airport or City',
                MCityFrom: 'From',
                MCityTo: 'TO',
            },
            preferAirline: "",
            airlineList: AirlinesDatas,
            airlineserch: "",
            Airlineresults: [],
            airlineautoCompleteProgress: false,
            airlineloading: false,
            direct_flight: false,
            FlightItemtext: { from: 'from', to: 'to' },
            FlightItemtID: {
                cityFrom0: 'cityFrom0',
                cityTo0: 'cityTo0',
                cityFrom1: 'cityFrom1',
                cityTo1: 'cityTo1',
                cityFrom2: 'cityFrom2',
                cityTo2: 'cityTo2',
                cityFrom3: 'cityFrom3',
                cityTo3: 'cityTo3',
                cityFrom4: 'cityFrom4',
                cityTo4: 'cityTo4',
                cityFrom5: 'cityFrom5',
                cityTo5: 'cityTo5'
            },
            divHistoryHTML: "",
            oldRetDate: "",
            calendarSearch: false,
            flightDuration: 1,
            calendarSearchSearchSuppliers: globalConfigs.services.flights.calendarSearch.supplier,
            isSending:false
        };
    },

    created: function() {
        var agencyNode = window.localStorage.getItem("agencyNode");
        if (agencyNode) {
            agencyNode = JSON.parse(atob(agencyNode));
            var servicesList = agencyNode.loginNode.servicesList;
            for (var i = 0; i < servicesList.length; i++) {
                var provider = servicesList[i].provider;
                if (servicesList[i].name == "Air" && provider.length > 0) {
                    provider = provider.filter(supplier => supplier.supplierType == 'Test' || supplier.supplierType == 'Production').
                    sort(function(a, b) {
                        if (a.name.toLowerCase() < b.name.toLowerCase()) { return -1; }
                        if (a.name.toLowerCase() > b.name.toLowerCase()) { return 1; }
                        return 0;
                    });
                    for (var j = 0; j < provider.length; j++) {
                        this.supplierList.push({ id: provider[j].id, name: provider[j].name.toUpperCase(), supplierType: provider[j].supplierType });
                        this.selectedSuppliers = this.supplierList.map(function(supplier) { return supplier.id });
                        this.supplierSearchLabel = this.supplierList.map(function(supplier) { return supplier.name; }).join(",");
                    }
                    break;
                } else if (servicesList[i].name == "Air" && provider.length == 0) {
                    setLoginPage("login");
                }
            }

        }
    },
    computed: {
        selectAll: {
            get: function() {
                return this.supplierList ? this.selectedSuppliers.length == this.supplierList.length : false;
            },
            set: function(value) {
                var selectedSuppliers = [];

                if (value) {
                    this.supplierList.forEach(function(supplier) {
                        selectedSuppliers.push(supplier.id);
                    });
                }

                this.selectedSuppliers = selectedSuppliers;
            }
        },
        showCalendarSearch() {
            var suppliers = this.calendarSearchSearchSuppliers;
            var hasCalendarSearch = false;
            for (var i = 0; i < this.selectedSuppliers.length; i++) {
                if (suppliers.indexOf(parseInt(this.selectedSuppliers[i])) >= 0) {
                    hasCalendarSearch = true;
                }
            }
            return this.selectedSuppliers.length > 0 && hasCalendarSearch;
        }

    },
    watch: {
        selectedSuppliers: function() {
            if (this.selectedSuppliers.length == 0) {
                this.supplierSearchLabel = 'No supplier selected';
            } else {
                var vm = this;
                this.supplierSearchLabel = this.selectedSuppliers.map(function(id) {
                    return vm.supplierList.filter(function(supplier) {
                        return id == supplier.id;
                    })[0].name;
                }).join(",");
            }
            this.autoCompleteResult = [];
            this.keywordSearch = "";
            this.calendarSearch = false;

        },
        triptype: function(newVal, oldVal) {
            if (newVal == 'O' && oldVal == 'R') {
                $("#retDate").val("");
                this.flightDuration = 1;
            }
        },
        airlineserch: function() {
            if (this.airlineserch == "") {
                this.preferAirline = "";
            }
        }
    },
    mounted: function() {
        var vm = this;
        vm.setCalender();
        vm.$nextTick(function() {

            var vr;
            var sl;
            sl = "0";
            $(".class_view").click(function() {
                sl = "1";
            });
            $(".class_type").click(function() {
                if (sl == "0") {
                    $(".class_view").fadeToggle();
                    vr = "1";
                } else {
                    vr = "1";
                    sl = "0";
                }

            });
            $(".wrapper").click(function() {
                if (sl == "0") {
                    if (vr == "0") {

                        $(".class_view").hide();
                    } else {
                        vr = "0";
                        sl = "0";
                    }
                } else {
                    vr = "0";
                    sl = "0";
                }
            });
            // $(".multi_box").click(function (event) {
            //   event.stopPropagation();
            //   $(".multi_dropBox").slideToggle();
            // });
            // $(".multi_dropBox").on("click", function (event) {
            //   event.stopPropagation();
            // });

            $(document).mouseup(function(e) {
                var container = $(".multi_dropBox");
                if (!container.is(e.target) && container.has(e.target).length === 0) {
                    container.hide();
                }
            });

            this.getSearchHistory();
        });
    },
    methods: {
        showSupplierListDropdown: function() {
            if (this.supplierList.length != 1) {
                this.showSupplierList = !this.showSupplierList;
            }
            this.isCompleted = false;
        },
        flightSearchcomplete(AirportCode, AirportName, item, direction, id) {

            var leg = item.replace(/^\D+/g, '');
            var from = 'cityFrom' + leg;
            var to = 'cityTo' + leg;
            if (direction == 'from') {
                if (this.flightSearchCities[to] != AirportCode) {
                    this.selectedSuppliers = this.supplierList.map(function(supplier) { return supplier.id });
                }
                if (this.flightSearchCities[to] == AirportCode) {
                    this.status = false;
                    this.flightSearchCityName[item] = '';
                    this.flightSearchCities[item] = '';
                    $("#" + id).focus();
                    alertify.alert(i18n.t('Flight_Search.Alert_Validation_Label'), i18n.t('Flight_Search.Dept_and_Arr_Alert_Msg'));

                } else {
                    this.flightSearchCities[item] = AirportCode;
                    this.flightSearchCityName[item] = AirportName;
                    this.status = true;
                }
            } else {
                if (this.flightSearchCities[from] != AirportCode) {
                    this.selectedSuppliers = this.supplierList.map(function(supplier) { return supplier.id });
                }
                if (this.flightSearchCities[from] == AirportCode) {
                    this.status = false;
                    this.flightSearchCityName[item] = '';
                    this.flightSearchCities[item] = '';
                    $("#" + id).focus();
                    alertify.alert(i18n.t('Flight_Search.Alert_Validation_Label'), i18n.t('Flight_Search.Dept_and_Arr_Alert_Msg'));
                } else {
                    this.flightSearchCities[item] = AirportCode;
                    this.flightSearchCityName[item] = AirportName;
                    this.status = true;
                }

            }


        },
        AddNewLeg() {
            if (this.legcount <= 5) {
                ++this.legcount;

            }
        },
        DeleteLeg(leg) {
            var legs = this.legcount;
            if (legs > 1) {
                --this.legcount;
                var from = 'cityFrom' + leg;
                var to = 'cityTo' + leg;
                this.flightSearchCities[from] = "";
                this.flightSearchCities[to] = "";

            }
        },
        setpaxcount(item, action) {
            if (item == 'adt') {
                if (action == 'plus') {
                    if (this.selected_adults < this.totalAllowdPax) {
                        ++this.selected_adults;
                    }
                } else {
                    if (this.selected_adults > 1) {
                        --this.selected_adults;
                        if (this.selected_adults < this.selected_infant) {
                            this.selected_infant = this.selected_adults;
                        }

                    }
                }
            } else if (item == 'chd') {
                if (action == 'plus') {
                    if (this.selected_adults + this.selected_children < this.totalAllowdPax) {
                        ++this.selected_children;
                    }
                } else {
                    if (this.selected_children > 0) {
                        --this.selected_children;
                    }
                }
            } else if (item == 'inf') {
                if (action == 'plus') {
                    if (this.selected_adults + this.selected_children + this.selected_infant < this.totalAllowdPax) {
                        if (this.selected_infant < this.selected_adults) {
                            ++this.selected_infant
                        }

                    }
                } else {
                    if (this.selected_infant > 0) {
                        --this.selected_infant
                    }
                }
            } else {}
            if (this.selected_adults + this.selected_children > this.totalAllowdPax) {
                this.selected_children = 0;
            }

            if (this.selected_adults + this.selected_children + this.selected_infant > this.totalAllowdPax) {
                this.selected_infant = 0;

            }
            var cabin = getCabinName(this.selected_cabin);
            var totalpax = this.selected_adults + this.selected_children + this.selected_infant;
            if (parseInt(totalpax) > 1) {
                totalpax = totalpax + ' Passengers';
            } else {
                totalpax = totalpax + ' Passenger'
            }
            this.Totaltravaller = totalpax + ',' + cabin;
        },
        onSelectedAirlineAutoComplete: _.debounce(function(keywordEntered) {
            if (keywordEntered.length > 2) {
                this.airlineautoCompleteProgress = true;
                this.airlineloading = true;
                var newData = [];
                this.airlineList.filter(function(el) {
                    if (el.A.toLowerCase().indexOf(keywordEntered.toLowerCase()) >= 0) {
                        newData.push({
                            "code": el.C,
                            "label": el.A
                        });
                    }
                });
                if (newData.length > 0) {
                    this.Airlineresults = newData;
                    this.airlineautoCompleteProgress = true;
                    this.airlineloading = false;
                } else {
                    this.Airlineresults = [];
                    this.airlineautoCompleteProgress = false;
                    this.airlineloading = false;
                }

            } else {
                this.Airlineresults = [];
                this.airlineautoCompleteProgress = false;
                this.airlineloading = false;
            }
        }, 100),
        onSelectedAirline: function(code, label) {
            this.airlineserch = label;
            this.Airlineresults = [];
            this.preferAirline = code;
        },
        setCalender() {
            var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
            var numberofmonths = generalInformation.systemSettings.calendarDisplay;
            var vm = this;
            var tempnumberofmonths = numberofmonths;
            if (parseInt($(window).width()) > 500 && parseInt($(window).width()) <= 999) {
                tempnumberofmonths = 2;
            } else if (parseInt($(window).width()) > 999) {
                tempnumberofmonths = 3;
            }
            $("#deptDate01").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: tempnumberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                onSelect: function(selectedDate) {
                    vm.oldRetDate = selectedDate;
                    $("#retDate").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                    if ($("#retDate").datepicker('getDate')) {
                        var a = $("#deptDate01").datepicker('getDate').getTime();
                        var b = $("#retDate").datepicker('getDate').getTime();
                        var c = 24 * 60 * 60 * 1000;
                        var diffDays = Math.round(Math.abs((a - b) / c)) + 1;
                        vm.flightDuration = diffDays <= 0 ? 1 : diffDays;
                    }
                }
            });

            $("#retDate").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: tempnumberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function(event, ui) {
                    var selectedDate = $("#deptDate01").val();
                    $("#retDate").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function(selectedDate) {
                    // if ($('#deptDate01').val() != '') {
                    vm.triptype = 'R';
                    // document.getElementById("radio-btn-2").checked = true;
                    // } else {
                    // $("#retDate").val("");
                    // alertify.alert('Please fill !', 'Please fill departure date !');
                    // }
                    if ($("#deptDate01").datepicker('getDate')) {
                        var a = $("#deptDate01").datepicker('getDate').getTime();
                        var b = $("#retDate").datepicker('getDate').getTime();
                        var c = 24 * 60 * 60 * 1000;
                        var diffDays = Math.round(Math.abs((a - b) / c)) + 1;
                        vm.flightDuration = diffDays <= 0 ? 1 : diffDays;
                    }
                }
            });

            $("#txtLeg2Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: tempnumberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function(event, ui) {
                    var selectedDate = $("#deptDate01").val();
                    $("#retDate").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function(selectedDate) {
                    $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg3Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: tempnumberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function(event, ui) {
                    var selectedDate = $("#txtLeg2Date").val();
                    $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function(selectedDate) {
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg4Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: tempnumberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function(event, ui) {
                    var selectedDate = $("#txtLeg3Date").val();
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function(selectedDate) {
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg5Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: tempnumberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function(event, ui) {
                    var selectedDate = $("#txtLeg4Date").val();
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function(selectedDate) {
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg6Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: tempnumberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function(event, ui) {
                    var selectedDate = $("#txtLeg5Date").val();
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function(selectedDate) {}
            });


        },
        serchflight: function() {
            var vm = this;
            vm.isSending = true;
            var Departuredate = $('#deptDate01').val() == "" ? "" : $('#deptDate01').datepicker('getDate');
            if (!this.flightSearchCities.cityFrom0) {
                alertify.alert(i18n.t('Flight_Search.Alert_Validation_Label'), i18n.t('Flight_Search.Origin_Alert_Msg'));
                return false;
            }
            if (!this.flightSearchCities.cityTo0) {
                alertify.alert(i18n.t('Flight_Search.Alert_Validation_Label'), i18n.t('Flight_Search.Dest_Alert_Msg'));
                return false;
            }
            if (!Departuredate) {
                alertify.alert(i18n.t('Flight_Search.Alert_Validation_Label'), i18n.t('Flight_Search.Dept_Alert_Msg'));
                return false;
            }
            var sec1TravelDate = moment(Departuredate).format('DD|MM|YYYY');
            var sectors = this.flightSearchCities.cityFrom0 + '-' + this.flightSearchCities.cityTo0 + '-' + sec1TravelDate;
            if (this.triptype == 'R') {
                var ArrivalDate = $('#retDate').val() == "" ? "" : $('#retDate').datepicker('getDate');
                if (!isNullorUndefined(ArrivalDate)) {
                    ArrivalDate = moment(ArrivalDate).format('DD|MM|YYYY');
                    if (!ArrivalDate) {
                        // alertify.alert(i18n.t('Flight_Search.Alert_Validation_Label'), i18n.t('Flight_Search.Arrv_Alert_Msg'));
                        // return false;
                        this.triptype = 'O';
                    } else {
                        sectors += '/' + this.flightSearchCities.cityTo0 + '-' + this.flightSearchCities.cityFrom0 + '-' + ArrivalDate;
                    }
                } else {
                    // alertify.alert(i18n.t('Flight_Search.Alert_Validation_Label'), i18n.t('Flight_Search.Arrv_Alert_Msg'));
                    // return false;
                    this.triptype = 'O';
                }
            }
            if (this.triptype == 'M') {
                for (var legValue = 1; legValue <= this.legcount; legValue++) {
                    var temDeparturedate = $('#txtLeg' + (legValue + 1) + 'Date').val() == "" ? "" : $('#txtLeg' + (legValue + 1) + 'Date').datepicker('getDate');
                    var from = 'cityFrom' + legValue;
                    var to = 'cityTo' + legValue;
                    if (temDeparturedate != "" && this.flightSearchCities[from] != "" && this.flightSearchCities[to] != "") {
                        var departureFrom = this.flightSearchCities[from];
                        var arrivalTo = this.flightSearchCities[to];
                        var travelDate = moment(temDeparturedate).format('DD|MM|YYYY');
                        sectors += '/' + departureFrom + '-' + arrivalTo + '-' + travelDate;
                    } else {
                        alertify.alert(i18n.t('Flight_Search.Alert_Validation_Label'), i18n.t('Flight_Search.Fill_Trip_Alert_Msg1') + ' ' + (legValue + 1) + ' ' + i18n.t('Flight_Search.Fill_Trip_Alert_Msg2'));
                        return false;
                    }
                }
            }
            var directFlight = this.direct_flight ? 'DF' : 'AF';
            var calendarSearch = this.calendarSearch ? '1' : '0';
            var adult = this.selected_adults;
            var child = this.selected_children;
            var infant = this.selected_infant;
            var cabin = this.selected_cabin;
            var tripType = this.triptype;
            var preferAirline = this.preferAirline;
            var airsupplier = 'all';

            var mergedSelected = [];

            for (var l = 0; l < this.selectedSuppliers.length; l++) {
                var supplierExist = airlineListWithSupplierCodes.find(function(e){return e.supplierCode == vm.selectedSuppliers[l];});
                if (supplierExist) {
                    mergedSelected.push(supplierExist);
                } else {
                    mergedSelected.push({supplierCode: this.selectedSuppliers[l], include: true});
                }
                
            }
            var filteredAirline = mergedSelected.filter(function(e) { return (vm.preferAirline ? e.airlineCode ==  vm.preferAirline : true) || e.isGDS || e.include;});
            if (filteredAirline && filteredAirline.length > 0) {
                this.selectedSuppliers = filteredAirline.map(function(e){return parseInt(e.supplierCode.toString())});
            }
            var legDetails = [];
            if (this.triptype == 'M') {
                for (var legValue = 1; legValue <= this.legcount; legValue++) {
                    let temDeparturedate = $('#txtLeg' + (legValue + 1) + 'Date').val() == "" ? "" : $('#txtLeg' + (legValue + 1) + 'Date').datepicker('getDate');
                    let from = 'cityFrom' + legValue;
                    let to = 'cityTo' + legValue;
                    if (temDeparturedate != "" && this.flightSearchCities[from] != "" && this.flightSearchCities[to] != "") {
                        legDetails.push(this.flightSearchCities[from] + '|' + this.flightSearchCities[to])
                    }
                }
            } else {
                legDetails.push(this.flightSearchCities.cityFrom0 + '|' + this.flightSearchCities.cityTo0);
            }

            var PostData = {
                Leg: legDetails,
                SupplierCode: this.selectedSuppliers.filter(function(e) { return ([1, 45, 55, 64, 42].indexOf(e) == -1) || (["1", "45", "55", "64", "42"].indexOf(e) == -1)}).map(function(e){return e.toString()})
            }

            var config = {
                axiosConfig: {
                    method: "POST",
                    url: HubServiceUrls.elasticSearch.airFilter,
                    data: PostData
                },
                successCallback: function (response) {
                    if (response.data) {
                        var tempSupp = [];
                        for (let index = 0; index < response.data.data.length; index++) {
                            tempSupp.push(response.data.data[index].SupplierCode);
                        }
                        var tmpArr = vm.selectedSuppliers.filter(function(e) { return ([1, 45, 55, 64, 42].indexOf(e) != -1) || (["1", "45", "55", "64", "42"].indexOf(e) != -1)});
                        vm.selectedSuppliers = tempSupp.concat(tmpArr);
                    }

                    if (vm.selectedSuppliers.length == 0 && response.data.data.length == 0) {
                        alertify.alert(i18n.t('Flight_Search.Alert_Validation_Label'), "Supplier does not support route.");
                        vm.isSending = false;
                        return false;
                    }
                    
                    // var hasFlyDubai = vm.selectedSuppliers.indexOf("54") != -1 || vm.selectedSuppliers.indexOf(54) != -1;

                    // var removeFZSupplier = false;
                    // if (hasFlyDubai) {
                    //     for (var key of Object.keys(vm.flightSearchCities)) {
                    //         if (vm.flightSearchCities[key]) {
                    //             var validAirport = _.find(fzAirports, function(e){ return e.airportCode == vm.flightSearchCities[key]; });
                    //             if (validAirport==undefined) {
                    //                 removeFZSupplier = true;
                    //             }
                    //         }
                    //     }
                    //     if (removeFZSupplier) {
                    //         var index = vm.selectedSuppliers.indexOf("54");
                    //         index = index == -1 ? vm.selectedSuppliers.indexOf(54) : index;
                    //         if (index > -1) {
                    //             vm.selectedSuppliers.splice(index, 1);
                    //         }
                    //     }
                    // }
                    if (vm.selectedSuppliers.length == 0) {
                        alertify.alert(i18n.t('Flight_Search.Alert_Validation_Label'), "No supplier selected.");
                        vm.isSending = false;
                        return false;
                    }
                    airsupplier = vm.selectedSuppliers.length > 0 ? vm.selectedSuppliers.join('|') : 'all';
                    var searchUrl = '/Flights/flightresults.html?flight=/' + sectors + '/' + adult + '-' + child + '-' + infant + '-' + cabin + '-' + airsupplier + '-50-F-' + tripType + '-F-' + preferAirline + '-' + directFlight + '-' + calendarSearch;
                    //  searchUrl = searchUrl.toLocaleLowerCase();
                    window.location.href = searchUrl;
                },
                errorCallback: function (error) {
                    self.showLoader = false;
                },
                showAlert: true
            }

            mainAxiosRequest(config);
        },
        hideautocompelteair: function() {
            this.airlineautoCompleteProgress = false;
        },
        getSearchHistory: function() {
            var searchHistories = $.cookie("AirSearchHistory");
            if (!isNullorUndefind(searchHistories)) {
                searchHistories = JSON.parse(searchHistories);
                searchHistories = this.filterValidSearches(searchHistories);

                if (searchHistories.length > 0) {
                    var body = '';
                    $.each(searchHistories, function(index, history) {
                        var searchCriteria = history.searchKey.split('/');
                        var airSements = searchCriteria.slice(0, -1);
                        var travellers = searchCriteria[searchCriteria.length - 1];
                        var tripType = travellers.split('-')[7].toLowerCase();
                        if (tripType == 'o' || tripType == 'r') {

                            var departureFrom = '';
                            var arrivalTo = '';
                            var tripTypeIcon = '';
                            var travelDate = '';
                            switch (tripType.toUpperCase()) {
                                case 'O':
                                    departureFrom = airSements[0].split('-')[0];
                                    arrivalTo = airSements[0].split('-')[1];
                                    tripTypeIcon = '→';
                                    travelDate = moment(airSements[0].split('-')[2], 'DD-MM-YYYY').format('DD MMM YYYY');
                                    break;
                                case 'R':
                                    departureFrom = airSements[0].split('-')[0];
                                    arrivalTo = airSements[1].split('-')[0];
                                    tripTypeIcon = '⇄';
                                    travelDate = moment(airSements[0].split('-')[2], 'DD-MM-YYYY').format('DD MMM YYYY') + ' - ' + moment(airSements[1].split('-')[2], 'DD-MM-YYYY').format('DD MMM YYYY');
                                    break;
                                default:
                                    break;
                            }
                            body += '<li><a href="/Flights/flightresults.html?flight=/' + history.searchKey + '">' +
                                '<div class="recent_cont"><div class="f_left" > ' + departureFrom.toUpperCase() + '</div >' +
                                '<div class="single_way"> ' + tripTypeIcon + ' </div>' +
                                '<div class="t_right" > ' + arrivalTo.toUpperCase() + '</div > ' +
                                '<div class="s_date" > ' + travelDate + '</div > </div>' +
                                '</a></li >';
                        }
                    });
                    this.divHistoryHTML = body;
                    $.cookie("AirSearchHistory", JSON.stringify(searchHistories), { expires: 50 });
                    this.clearSearchHistory();
                }
            }
        },
        filterValidSearches: function(searchHistories) {
            // searchHistories = searchHistories.searchAirHistories;
            var histories = [];
            $.each(searchHistories, function(index, history) {
                var isValid = true;
                var sectors = history.searchKey.split('/').slice(0, -1);
                $.each(sectors, function(ser, sector) {
                    var travelDate = moment(sector.split('-')[2], 'DD|MM|YYYY').format('YYYY-MM-DD');
                    isValid = moment(travelDate, 'YYYY-MM-DD').isBefore(moment().format('YYYY-MM-DD'));
                });
                if (!isValid) { histories.push(history); }
            });
            //histories = histories.sort(function (a, b) {
            //    return new Date(a.searchTime) < new Date(b.searchTime);
            //});
            histories = histories.reverse();

            //histories = _.sortBy(histories, 'searchTime');
            //histories = histories.filter(function (a) { return !this[a] ? this[a] = true : false; });

            if (histories.length >= 6) { histories = histories.splice(0, 6); }
            return histories;
        },
        clearSearchHistory: function() {
            this.$nextTick(function() {
                $(document).ready(function() {
                    setTimeout(function() {
                        $('#clsSearchHistory').click(function() {
                            $('.divHistory').hide("slow");
                            $('.searchlink').show("slow");
                        });
                        $('.searchlink').click(function() {
                            $('.divHistory').show("slow");
                            $('.searchlink').hide("slow");
                        });
                        $('#clearHistory').click(function() {
                            $('.divHistory').hide("slow");
                            $('.searchlink').hide("slow");
                            setTimeout(function() { $('#divHisroty').remove(); }, 1000);
                            $.removeCookie("AirSearchHistory");
                        });
                    }, 500);
                });
            })
        },
        retDateClose: function() {
            this.triptype = 'O';
        },
        tripTypeReturn: function() {
            var vm = this;
            if (vm.oldRetDate == "") {
                $("#retDate").datepicker("setDate", 'today');
            } else {
                $("#retDate").datepicker("setDate", new Date(moment(vm.oldRetDate, "DD MMM YY, ddd")));
            }
        }
    },
    updated: function() {
        this.setCalender();
    }
});

Vue.component("hotel-rooms", {
    data: function() {
        return {
            adults: [
                { 'value': 1, 'text': '1' },
                { 'value': 2, 'text': '2' },
                { 'value': 3, 'text': '3' },
                { 'value': 4, 'text': '4' },
                { 'value': 5, 'text': '5' },
                { 'value': 6, 'text': '6' },
                { 'value': 7, 'text': '7' }
            ],
            children: [
                { 'value': 0, 'text': '0' },
                { 'value': 1, 'text': '1' },
                { 'value': 2, 'text': '2' },

            ],
            childrenAges: [
                { 'value': 1, 'text': '1' },
                { 'value': 2, 'text': '2' },
                { 'value': 3, 'text': '3' },
                { 'value': 4, 'text': '4' },
                { 'value': 5, 'text': '5' },
                { 'value': 6, 'text': '6' },
                { 'value': 7, 'text': '7' },
                { 'value': 8, 'text': '8' },
                { 'value': 9, 'text': '9' },
                { 'value': 10, 'text': '10' },
                { 'value': 11, 'text': '11' },
                { 'value': 12, 'text': '12' },
                // { 'value': 13, 'text': '13' },
                // { 'value': 14, 'text': '14' },
                // { 'value': 15, 'text': '15' },
                // { 'value': 16, 'text': '16' },
                // { 'value': 17, 'text': '17' }
            ],
            selectedAdults: 1,
            selectedChildren: 0,
            selectedChildrenAges: [
                { child1: 1 },
                { child2: 1 }
            ]
        };
    },
    props: {
        roomId: Number,
        getRoomDetails: Function
    },
    mounted: function() {
        this.$watch(function(vm) { return vm.selectedAdults, vm.selectedChildren, vm.selectedChildrenAges, Date.now(); },
            function() {
                // Executes if data above have changed.
                this.$emit("get-room-details", {
                    roomId: this.roomId,
                    selectedAdults: this.selectedAdults,
                    selectedChildren: this.selectedChildren,
                    selectedChildrenAges: this.selectedChildrenAges
                })
            }
        )

    }
});

Vue.component("hotel-search", {
    data: function() {
        return {
            commonStore: vueCommonStore.state,
            //city search
            isCompleted: false,
            keywordSearch: "",
            autoCompleteResult: [],
            cityCode: "",
            cityName: "",
            countryCode: "",
            //supplier select
            showSupplierList: false,
            supplierList: [],
            selectedSuppliers: [],
            supplierSearchLabel: "All Suppliers",
            //coutries
            countries: countryList,
            countryOfResidence: "AE",
            nationality: "AE",
            // datepicker
            checkIn: "",
            checkOut: "",
            numberOfNights: 1,
            numberOfRooms: 1,
            roomSelectionDetails: {
                room1: {
                    adult: 1,
                    children: 0,
                    childrenAges: []
                },
                room2: {
                    adult: 1,
                    children: 0,
                    childrenAges: []
                },
                room3: {
                    adult: 1,
                    children: 0,
                    childrenAges: []
                },
                room4: {
                    adult: 1,
                    children: 0,
                    childrenAges: []
                },
                room5: {
                    adult: 1,
                    children: 0,
                    childrenAges: []
                }
            },
            KMrange: 5,
            highlightIndex: 0
        }
    },
    created: function() {
        var agencyNode = window.localStorage.getItem("agencyNode");
        if (agencyNode) {
            agencyNode = JSON.parse(atob(agencyNode));
            var servicesList = agencyNode.loginNode.servicesList;
            for (var i = 0; i < servicesList.length; i++) {
                if (servicesList[i].name == "Hotel" && servicesList[i].provider.length > 0) {
                    var supplierListSorted = servicesList[i].provider.filter(supplier => supplier.supplierType == 'Test' || supplier.supplierType == 'Production')
                        .sort(function(a, b) {
                            if (a.name.toLowerCase() < b.name.toLowerCase()) { return -1; }
                            if (a.name.toLowerCase() > b.name.toLowerCase()) { return 1; }
                            return 0;
                        });
                    for (var j = 0; j < supplierListSorted.length; j++) {
                        this.supplierList.push({ id: supplierListSorted[j].id, name: supplierListSorted[j].name.toUpperCase(), supplierType: supplierListSorted[j].supplierType });
                        this.selectedSuppliers = this.supplierList.map(function(supplier) { return supplier.id });
                    }
                    break;
                } else if (servicesList[i].name == "Hotel" && servicesList[i].provider.length == 0) {
                    setLoginPage("login");
                }
            }
        }
    },
    computed: {
        selectAll: {
            get: function() {
                return this.supplierList ? this.selectedSuppliers.length == this.supplierList.length : false;
            },
            set: function(value) {
                var selectedSuppliers = [];

                if (value) {
                    this.supplierList.forEach(function(supplier) {
                        selectedSuppliers.push(supplier.id);
                    });
                }

                this.selectedSuppliers = selectedSuppliers;
            }
        }
    },
    methods: {
        onClickAutoCompleteEvent: function() {
            // if (this.autoCompleteResult.length > 0) {
            this.isCompleted = true;
            // } else {
            // this.isCompleted = false;
            // }
        },
        onKeyUpAutoCompleteEvent: _.debounce(function(keywordEntered) {
            var self = this;
            if (keywordEntered.length > 2) {
                var cityName = keywordEntered;
                var cityarray = cityName.split(' ');
                var uppercaseFirstLetter = "";
                for (var k = 0; k < cityarray.length; k++) {
                    uppercaseFirstLetter += cityarray[k].charAt(0).toUpperCase() + cityarray[k].slice(1).toLowerCase()
                    if (k < cityarray.length - 1) { uppercaseFirstLetter += " "; }
                }
                uppercaseFirstLetter = "*" + uppercaseFirstLetter + "*";
                var lowercaseLetter = "*" + cityName.toLowerCase() + "*";
                var uppercaseLetter = "*" + cityName.toUpperCase() + "*";


                var should = [];
                var i = 0;
                var supplier_type = "";
                for (var key in self.supplierList) {

                    // check if the property/key is defined in the object itself, not in parent
                    if (self.supplierList.hasOwnProperty(key)) {
                        supplier_type = [self.supplierList[key].supplierType, "Both"];

                        var supplierIndex = self.selectedSuppliers.indexOf(self.supplierList[key].id);
                        if (supplierIndex > -1) {
                            var supplier_id_query = {
                                bool: {
                                    must: [{
                                            match_phrase: { supplier_id: self.selectedSuppliers[supplierIndex] }
                                        },
                                        {
                                            terms: { supplier_type: supplier_type }
                                        }
                                    ]
                                }
                            };

                            should.push(supplier_id_query);
                            i++;
                        }
                    }
                }

                var query = {
                    query: {
                        bool: {
                            must: [{
                                    bool: {
                                        should: [
                                            { wildcard: { supplier_city_name: uppercaseFirstLetter } },
                                            { wildcard: { supplier_city_name: uppercaseLetter } },
                                            { wildcard: { supplier_city_name: lowercaseLetter } }
                                        ]
                                    }
                                },
                                { bool: { should } }
                            ]
                        }
                    }
                };

                var client = new elasticsearch.Client({
                    host: [{
                        host: HubServiceUrls.elasticSearch.elasticsearchHost,
                        auth: HubServiceUrls.elasticSearch.auth,
                        protocol: HubServiceUrls.elasticSearch.protocol,
                        port: HubServiceUrls.elasticSearch.port
                    }],
                    log: 'trace'
                });
                client.search({ index: 'city_map', size: 150, body: query }).then(function(resp) {
                    finalResult = [];
                    var hits = resp.hits.hits;
                    var Citymap = new Map();
                    for (var i = 0; i < hits.length; i++) {
                        Citymap.set(hits[i]._source.oneview_city_id, hits[i]._source);
                    }
                    var get_values = Citymap.values();
                    var Cityvalues = [];
                    for (var ele of get_values) {
                        Cityvalues.push(ele);
                    }
                    var results = SortInputFirst(cityName, Cityvalues);
                    for (var i = 0; i < results.length; i++) {
                        finalResult.push({
                            "code": results[i].oneview_city_id,
                            "label": results[i].supplier_city_name + ", " + results[i].oneview_country_name,
                            "location": results[i].location,
                            "countryCode": results[i].oneview_country_code
                        });
                    }
                    var newData = [];
                    finalResult.forEach(function(item, index) {
                        if (item.label.toLowerCase().indexOf(keywordEntered.toLowerCase()) >= 0) {
                            newData.push(item);
                        }
                    });
                    console.log(newData);
                    self.autoCompleteResult = newData;
                    self.isCompleted = true;
                    self.highlightIndex = 0;
                });

            } else {
                this.autoCompleteResult = [];
                this.isCompleted = false;
            }
        }, 100),
        updateResults: function(item) {
            this.keywordSearch = item.label;
            this.autoCompleteResult = [];
            this.cityCode = item.code;
            this.cityName = item.label;
            this.countryCode = item.countryCode;
            window.localStorage.setItem("cityLocation", JSON.stringify(item.location));
            window.sessionStorage.setItem("countryCode", item.countryCode);
        },
        showSupplierListDropdown: function() {
            if (this.supplierList.length != 1) {
                this.showSupplierList = !this.showSupplierList;
            }
            this.isCompleted = false;
        },
        getRoomDetails: function(data) {
            var selectedRoom = this.roomSelectionDetails['room' + (data.roomId + 1)];
            selectedRoom.adult = data.selectedAdults;
            selectedRoom.children = data.selectedChildren;
            if (data.selectedChildren == 0) {
                selectedRoom.childrenAges = [];
            } else if (data.selectedChildren == 1) {
                selectedRoom.childrenAges = [data.selectedChildrenAges[0]];
            } else if (data.selectedChildren == 2) {
                selectedRoom.childrenAges = data.selectedChildrenAges;
            }

            this.roomSelectionDetails['room' + (data.roomId + 1)] = selectedRoom;
        },
        setNumberOfNights: function() {
            var dt2 = $('#txtCheckOutDate');
            var startDate = $('#txtCheckInDate').datepicker('getDate');
            var a = $("#txtCheckInDate").datepicker('getDate').getTime();
            var b = $("#txtCheckOutDate").datepicker('getDate').getTime();
            var c = 24 * 60 * 60 * 1000;
            var diffDates = Math.round(Math.abs((a - b) / c));

            if ((event.target.value || "").match(/[^0-9\.]/gi)) {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.NightsValidation1_Label'));
                this.numberOfNights = diffDates;
            } else if ((event.target.value || "").match(/[d*\.]/gi)) {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.NightsValidation2_Label'));
                this.numberOfNights = diffDates;
            } else if (event.target.value > 90) {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.NightsValidation4_Label'));
                this.numberOfNights = diffDates;
            } else {
                startDate.setDate(startDate.getDate() + this.numberOfNights);
                // dt2.datepicker('setDate', startDate);
                // eDate = dt2.datepicker('getDate').getTime();
                // this.checkOut = $("#txtCheckOutDate").val();
                dt2.datepicker('setDate', startDate);
                if (this.numberOfNights != "") {
                    $('.ui-datepicker-current-day').click();
                }
                // eDate = dt2.datepicker('getDate').getTime();
            }
        },
        searchHotels: function() {
            var rooms = "";
            var adultCount = 0;
            var childCount = 0;

            for (var roomIndex = 1; roomIndex <= parseInt(this.numberOfRooms); roomIndex++) {
                var roomSelectionDetails = this.roomSelectionDetails['room' + roomIndex];
                adultCount += roomSelectionDetails.adult;
                childCount += roomSelectionDetails.children;
                rooms += "&room" + roomIndex + "=ADT_" + roomSelectionDetails.adult + ",CHD_" + roomSelectionDetails.children;
                for (var childIndex = 0; childIndex < roomSelectionDetails.childrenAges.length; childIndex++) {
                    rooms += "_" + roomSelectionDetails.childrenAges[childIndex]['child' + (childIndex + 1)];
                }
            }

            if (this.cityName.trim() == "" || this.cityCode == "") {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.CityValidation_Label'));
            } else if (this.selectedSuppliers.length == 0) {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.SupplierValidation_Label'));
            } else if (this.checkIn == "") {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.CheckinValidation_Label'));
            } else if (this.checkOut == "") {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.CheckoutValidation_Label'));
            } else if (this.countryOfResidence == "") {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.CountryValidation_Label'));
            } else if (this.nationality == "") {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.NationalityValidation_Label'));
            } else if (this.numberOfNights <= 0) {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.NightsValidation3_Label'));
            } else if (this.numberOfNights == "") {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.NightsValidation5_Label'));
            } else if (this.numberOfRooms == "") {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.RoomsValidation1_Label'));
            } else if (this.numberOfNights > 90) {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.NightsValidation4_Label'));
            } else if (parseInt(adultCount + childCount) > 9) {
                alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.RoomsValidation2_Label'));
            } else {
                var checkIn = $('#txtCheckInDate').val() == "" ? "" : $('#txtCheckInDate').datepicker('getDate');
                var checkOut = $('#txtCheckOutDate').val() == "" ? "" : $('#txtCheckOutDate').datepicker('getDate');

                window.localStorage.setItem("selectedSuppliers", this.selectedSuppliers.join(","));

                var searchCriteria = "nationality=" + this.nationality +
                    "&country_of_residence=" + this.countryOfResidence +
                    "&city_code=" + this.cityCode +
                    "&city=" + this.cityName +
                    "&cin=" + moment(checkIn).format('DD/MM/YYYY') +
                    "&cout=" + moment(checkOut).format('DD/MM/YYYY') +
                    "&night=" + this.numberOfNights +
                    "&adtCount=" + adultCount +
                    "&chdCount=" + childCount +
                    "&guest=" + (adultCount + childCount) +
                    "&num_room=" + this.numberOfRooms + rooms +
                    "&KMrange=" + this.KMrange +
                    "&supplier=" + this.selectedSuppliers.join(",") +
                    // "&sort=" + "price-a" +
                    "&uuid=" + uuidv4();
                var uri = "/Hotels/hotelresults.html?" + encodeURIComponent(searchCriteria);
                window.location.href = uri;
            }

        },
        hideSupplier: function() {
            this.showSupplierList = false;
        },
        hideCity: function() {
            this.isCompleted = false;
        },
        up: function() {
            if (this.isCompleted) {
                if (this.highlightIndex > 0) {
                    this.highlightIndex--
                }
            } else {
                this.isCompleted = true;
            }
            this.fixScrolling();
        },
        down: function() {
            if (this.isCompleted) {
                if (this.highlightIndex < this.autoCompleteResult.length - 1) {
                    this.highlightIndex++
                } else if (this.highlightIndex == this.autoCompleteResult.length - 1) {
                    this.highlightIndex = 0;
                }
            } else {
                this.isCompleted = true;
            }
            this.fixScrolling();
        },
        fixScrolling: function() {
            try {
                var liH = this.$refs.options[this.highlightIndex].clientHeight;
                if (liH == 50) {
                    liH = 32;
                }
                if (liH * this.highlightIndex >= 32 || this.highlightIndex == 0) {
                    this.$refs.searchautocomplete.scrollTop = liH * this.highlightIndex;
                }
            } catch (error) {}
        }
    },
    watch: {
        selectedSuppliers: function() {
            if (this.selectedSuppliers.length == this.supplierList.length && this.supplierList.length != 1) {
                this.supplierSearchLabel = 'All Suppliers';
            } else if (this.selectedSuppliers.length == 0) {
                this.supplierSearchLabel = 'No supplier selected';
            } else if (this.selectedSuppliers.length > 4) {
                this.supplierSearchLabel = this.selectedSuppliers.length + ' supplier selected'
            } else {
                var vm = this;
                this.supplierSearchLabel = this.selectedSuppliers.map(function(id) {
                    return vm.supplierList.filter(function(supplier) {
                        return id == supplier.id;
                    })[0].name;
                }).join(",");
            }
            this.autoCompleteResult = [];
            this.keywordSearch = "";
        }
    },
    mounted: function() {
        var vm = this;
        this.$nextTick(function() {
            // var generalInformation = {
            //   systemSettings: {
            //     systemDateFormat: 'dd M y,D',
            //     calendarDisplay: 1,
            //     calendarDisplayInMobile: 1
            //   },
            // }

            if (vm.commonStore.selectedLanguage && vm.commonStore.selectedLanguage.code == 'ar') {
                $("#txtCheckInDate").datepicker("option", $.datepicker.regional['ar']);
                $("#txtCheckOutDate").datepicker("option", $.datepicker.regional['ar']);
            } else {
                $("#txtCheckInDate").datepicker("option", $.datepicker.regional['en']);
                $("#txtCheckOutDate").datepicker("option", $.datepicker.regional['en']);
            }

            var setDate = function(days) { return moment(new Date).add(days, 'days').format("DD/MM/YYYY"); };

            var startDate = setDate(1);
            var endDate = setDate(2);

            var dateFormat = generalInformation.systemSettings.systemDateFormat;

            var tempnumberofmonths = 1;
            if (parseInt($(window).width()) > 500 && parseInt($(window).width()) <= 999) {
                tempnumberofmonths = 2;
            } else if (parseInt($(window).width()) > 999) {
                tempnumberofmonths = 3;
            }

            var sDate = new Date(moment(startDate, "DD/MM/YYYY")).getTime();
            var eDate = new Date(moment(endDate, "DD/MM/YYYY")).getTime();

            //Checkin Date
            $("#txtCheckInDate").datepicker({
                dateFormat: dateFormat,
                minDate: 0, // 0 for search
                maxDate: "360d",
                numberOfMonths: parseInt(tempnumberofmonths),
                onSelect: function(date) {

                    var dt2 = $('#txtCheckOutDate');
                    var startDate = $(this).datepicker('getDate');
                    var minDate = $(this).datepicker('getDate');
                    sDate = startDate.getTime();
                    startDate.setDate(startDate.getDate() + 1);
                    dt2.datepicker('option', 'minDate', startDate);
                    dt2.datepicker('setDate', minDate);
                    var a = $("#txtCheckInDate").datepicker('getDate').getTime();
                    var b = $("#txtCheckOutDate").datepicker('getDate').getTime();
                    var c = 24 * 60 * 60 * 1000;
                    var diffDays = Math.round(Math.abs((a - b) / c));
                    vm.numberOfNights = diffDays;
                    vm.checkIn = $("#txtCheckInDate").val();
                    $('.ui-datepicker-current-day').click();
                },
                beforeShowDay: function(date) {

                    var container = $(".multi_dropBox");
                    container.hide();

                    if (date.getTime() == eDate) {
                        return [true, 'event event-selected event-selected-right', ''];
                    } else if (date.getTime() <= eDate && date.getTime() >= sDate) {
                        if (date.getTime() == sDate) {
                            return [true, 'event-selected event-selected-left', ''];
                        } else {
                            return [true, 'event-selected', ''];
                        }
                    } else {
                        return [true, '', ''];
                    }
                }
            });
            $("#txtCheckInDate").datepicker({ dateFormat: dateFormat }).datepicker("setDate", new Date().getDay + 1);
            vm.checkIn = $("#txtCheckInDate").val();

            //Checkout Date
            $('#txtCheckOutDate').datepicker({
                dateFormat: dateFormat,
                minDate: 2, // 2 for search
                maxDate: "360d",
                numberOfMonths: parseInt(tempnumberofmonths),
                onSelect: function(date) {
                    var a = $("#txtCheckInDate").datepicker('getDate').getTime();
                    var b = $("#txtCheckOutDate").datepicker('getDate').getTime();
                    var c = 24 * 60 * 60 * 1000;
                    var diffDays = Math.round(Math.abs((a - b) / c));
                    if (diffDays > 90) {
                        alertify.alert(i18n.t('Hotel_Search.AlertifyHeader_Label'), i18n.t('Hotel_Search.NightsValidation4_Label'));
                        vm.setNumberOfNights();
                    } else {
                        vm.numberOfNights = diffDays;
                        eDate = $(this).datepicker('getDate').getTime();
                    }
                    vm.checkOut = $("#txtCheckOutDate").val();
                },
                beforeShowDay: function(date) {
                    // if ($("#txtCheckInDate").datepicker('getDate') == null) {
                    //   return [false, ''];
                    // }
                    if (date.getTime() == sDate) {
                        return [true, 'event event-selected event-selected-left', ''];
                    } else if (date.getTime() >= sDate && date.getTime() <= eDate) {
                        if (date.getTime() == eDate) {
                            return [true, 'event-selected event-selected-right', ''];
                        } else {
                            return [true, 'event-selected', ''];
                        }
                    } else {
                        return [true, '', ''];
                    }
                }
            });
            $("#txtCheckOutDate").datepicker({ dateFormat: dateFormat }).datepicker("setDate", new Date().getDay + 2);
            vm.checkOut = $("#txtCheckOutDate").val();

            var stepSlider = document.getElementById('rangeSlider');

            noUiSlider.create(stepSlider, {
                start: [5],
                step: 1,
                range: {
                    'min': [1],
                    'max': [10]
                }
            });

            stepSlider.noUiSlider.on('update', function(values, handle) {
                vm.KMrange = parseInt(values[handle]);
            });
        });
        var agencyNode = window.localStorage.getItem("agencyNode");
        if (agencyNode) {
            agencyNode = JSON.parse(atob(agencyNode));
            vm.nationality = agencyNode.loginNode.country.code || "AE";
            vm.countryOfResidence = agencyNode.loginNode.country.code || "AE";
        }

    }
});

Vue.component("sightseeing-search", {
    data: function() {
        return {
            commonStore: vueCommonStore.state
        };
    }
});

Vue.component("wis-insurance-search", {
  data: function () {
    return {
      commonStore: vueCommonStore.state,
      tripType: "",
      familyGroup: 0,
      destinationId: 0,
      adult: 0,
      children: 0,
      seniors: 0,
      superSeniors: 0,
      airport: "NA"
    };
  },
  mounted: function(){
    var vm = this;
    vm.$nextTick(function () {
      var tempnumberofmonths = 1;
      if (parseInt($(window).width()) > 500 && parseInt($(window).width()) <= 999) {
        tempnumberofmonths = 2;
      } else if (parseInt($(window).width()) > 999) {
        tempnumberofmonths = 3;
      }

      if (vm.commonStore.selectedLanguage && vm.commonStore.selectedLanguage.code == 'ar') {
        $("#txtStartInDate").datepicker("option", $.datepicker.regional['ar']);
        $("#txtEndDate").datepicker("option", $.datepicker.regional['ar']);
      } else {
        $("#txtStartInDate").datepicker("option", $.datepicker.regional['en']);
        $("#txtEndDate").datepicker("option", $.datepicker.regional['en']);
      }
      var dateFormat = generalInformation.systemSettings.systemDateFormat;
      $("#txtStartInDate").datepicker({
        minDate: "0",
        maxDate: "360",
        numberOfMonths: tempnumberofmonths,
        changeMonth: true,
        dateFormat: dateFormat,
        showButtonPanel: false,
        onSelect: function (selectedDate) {

          vm.changeCalendarDates();
          $("#txtEndDate").datepicker("option", "minDate", selectedDate);

        }
      });
      $("#txtEndDate").datepicker({
        minDate: "0",
        maxDate: "360",
        numberOfMonths: tempnumberofmonths,
        changeMonth: true,
        dateFormat: dateFormat,
        showButtonPanel: false,
        onSelect: function (selectedDate) {
          vm.endDate = $("#txtEndDate").val();
          vm.triptype = 'R';
        }
      });
      $("#txtStartInDate").datepicker("setDate", new Date().getDay + 0);
      $("#txtEndDate").datepicker("setDate", new Date().getDay + 1);

      $(".airportCities").autocomplete({
        source: AirlinesTimezone, minLength: 3, position: { my: "left top", at: "left bottom" },
        change: function (event, ui) { if (!ui.item) { $(this).val(''); } },
        select: function (event, ui) {
          if (ui.item.id != 0) {
            if (ui.item.value.indexOf('No Match Found') < 0) {
              this.value = ui.item.label;
              vm.airport = ui.item.I;
              $(this).parents('.txt_animation').addClass('focused');
            }
          } return true;
        },
        focus: function () { return false; }
      });

      vm.changeCalendarDates();
      setTimeout(() => {
        $('[data-toggle="popover"]').popover({ html: true });
      }, 100);
    });
  },
  methods: {
    changeCalendarDates: function () {
      var vm = this;
      var startDate = $("#txtStartInDate").datepicker('getDate');
      startDate.setDate(startDate.getDate() + 1);
      var dt2 = $('#txtEndDate');
      if (vm.triptype == 'R') {
        dt2.datepicker('option', 'minDate', startDate);
        dt2.datepicker('setDate', startDate);
        vm.endDate = $("#txtEndDate").val();
      } else {
        vm.endDate = "";
        dt2.datepicker('setDate', "");
      }
      vm.startDate = $("#txtStartInDate").val();
    },
    getDestination: function (destinationId, currentInstance) {
      $('#destination li').each(function () {
        $(this).children('span').removeClass('desti_active');
      });
      $(currentInstance.currentTarget).children('span').addClass('desti_active');
      destinationId != "airport" ? $('#airPortOption').css('opacity', '0.4').css('pointer-events', 'none') : $('#airPortOption').css('opacity', '1').css('pointer-events', 'unset');
      this.destinationId = destinationId;
    },
    removePassenger: function (paxType) {
      switch (paxType) {
        case "adt":
          this.adult = this.adult == 0 ? this.adult : this.adult - 1;
          break;
        case "chd":
          this.children = this.children == 0 ? this.children : this.children - 1;
          break;
        case "snr":
          this.seniors = this.seniors == 0 ? this.seniors : this.seniors - 1;
          break;
        case "ssnr":
          this.superSeniors = this.superSeniors == 0 ? this.superSeniors : this.superSeniors - 1;
          break;
        default:
          break;
      }
    },
    addPassenger: function (paxType){
      var totalPassenger = this.adult + this.children + this.seniors + this.superSeniors;

      if (totalPassenger >= 9) {
        return;
      }

      switch (paxType) {
        case "adt":
          if (this.familyGroup == 1) {
            if (this.adult < 1) {
              this.adult++;
              this.children = this.seniors = this.superSeniors = 0;
            }
          } else {
            this.adult++;
          }
          break;
        case "chd":
          if (this.familyGroup == 1) {
            if (this.children < 1) {
              this.children++;
              this.adult = this.seniors = this.superSeniors = 0;
            }
          } else {
            this.children++;
          }
          break;
        case "snr":
          if (this.familyGroup == 1) {
            if (this.seniors < 1) {
              this.seniors++;
              this.adult = this.children = this.superSeniors = 0;
            }
          } else {
            this.seniors++;
          }
          break;
        case "ssnr":
          if (this.familyGroup == 1) {
            if (this.superSeniors < 1) {
              this.superSeniors++;
              this.adult = this.children = this.seniors = 0;
            }
          } else {
            this.superSeniors++;
          }
          break;
        default:
          break;
      }
    },
    validateInsurance() {
      var isSuccess = true;
      var vm = this;

      if (stringIsNullorEmpty(vm.tripType)) {
        alertify.alert('Warning', 'Please select Trip type.');
        return false;
      }

      if (stringIsNullorEmpty($('#txtStartInDate').val())) {
        alertify.alert('Warning', 'Please select start date.');
        return false;
      }

      if (stringIsNullorEmpty($('#txtEndDate').val())) {
        alertify.alert('Warning', 'Please select end date.');
        return false;
      }

      if (vm.familyGroup == 0) {
        alertify.alert('Warning', 'Please select family group.');
        return false;
      }

      if (vm.destinationId == 0 && stringIsNullorEmpty($('#txtairport').val())) {
        alertify.alert('Warning', 'Please select airport.');
        return false
      }

      if (vm.adult== 0 && vm.children== 0 && vm.seniors== 0 && vm.superSeniors== 0) {
        alertify.alert('Warning', 'Please fill the Passengers.');
        $('#ADTCount').focus();
        return false;
      }

      var totalPassenger = vm.adult + vm.children + vm.seniors + vm.superSeniors;

      if (vm.familyGroup == 1) {
        if (totalPassenger > 1) {
          alertify.alert('Validation', 'Only a single passenger is allowed if Family Group is Individual.');
          return false;
        }
      } else if (vm.familyGroup == 2) {
        if (totalPassenger > 6) {
          alertify.alert('Validation', 'Maximum number of passengers is 6.');
          return false;
        }
        else if (vm.adult != 2) {
          alertify.alert('Validation', 'Adult passengers must be 2 for family.');
          return false;
        }
        else if (vm.children > 4 || vm.children == 0) {
          alertify.alert('Validation', 'Maximum number of child passengers is 4 and minimum number is 1.');
          return false;
        }

      } else if (vm.familyGroup ==  3) {
        if (totalPassenger == 1) {
          alertify.alert('Validation', 'Single passenger is not allowed for group travel.');
          return false;
        }
        else if (vm.adult + vm.seniors + vm.superSeniors < 1) {
          alertify.alert('Validation', 'Atleast 1 Adult or Senior Must be For Group.');
          return false;
        }
      }
      // isSuccess = supplierValidation(selectedSuppliers);
      return isSuccess;
    },
    searchInsurance: function() {
      var proceed = this.validateInsurance();
      if (this.destinationId == 0) {
        this.airport = "";
      }
      if (proceed) {
        var startDate = moment($('#txtStartInDate').val(), 'DD MMM YY,ddd').format('YYYY|MM|DD');
        var endDate = moment($('#txtEndDate').val(), 'DD MMM YY,ddd').format('YYYY|MM|DD');

        var url = this.tripType + '-' + startDate + '-' + endDate + '-' + this.familyGroup + '-' + this.adult + '|' + this.children + '|' + this.seniors + '|' + this.superSeniors + '-' + this.airport + '|' + this.destinationId;
        window.location.href = '/Insurance/wis/searchinsurance.html?insurance=' + url.toUpperCase();
      }
    }
  },
  watch: {
    familyGroup: function (current) {
      if (current == 1) {
        this.adult = 1;
        this.children = 0;
        this.seniors = 0;
        this.superSeniors = 0;
      } else if (current == 2) {
        this.adult = 2;
        this.children = 1;
        this.seniors = 0;
        this.superSeniors = 0;
      } else {
        this.adult = 2;
        this.children = 0;
        this.seniors = 0;
        this.superSeniors = 0;
      }
    }
  }
});

Vue.component("tp-insurance-search", {
    data: function() {
        return {
            commonStore: vueCommonStore.state,
            countries: countryList,
            cityList: cityList,
            departureCityLst: [],
            arrivalCityLst: [],
            departureCountry: "AE",
            departureCity: "",
            arrivalCountry: "AE",
            arrivalCity: "",
            startDate: "",
            endDate: "",
            triptype: "O",
            totalPassenagers: "9",
            totalAdult: 1,
            totalChildern: 0,
            totalInfant: 0,
            airlineautoCompleteProgress: false,
            airlineserch: "",
            Airlineresults: [],
            airlineloading: false,
            preferAirline: "",
            airlineList: AirlinesDatas,
        };
    },
    created: function() {

    },
    methods: {
        searchAction: function() {

            if (this.departureCountry == undefined || this.departureCountry == '') {
                alertify.alert(i18n.t('TPInsurance_Search.Alert_Validation_Label'), i18n.t('TPInsurance_Search.Departure_Country_Alert'));
                return false;
            } else if (this.departureCity == undefined || this.departureCity == '') {
                alertify.alert(i18n.t('TPInsurance_Search.Alert_Validation_Label'), i18n.t('TPInsurance_Search.Departure_City_Alert'));
                return false;
            } else if (this.arrivalCountry == undefined || this.arrivalCountry == '') {
                alertify.alert(i18n.t('TPInsurance_Search.Alert_Validation_Label'), i18n.t('TPInsurance_Search.Arrival_Country_Alert'));
                return false;
            } else if (this.arrivalCity == undefined || this.arrivalCity == '') {
                alertify.alert(i18n.t('TPInsurance_Search.Alert_Validation_Label'), i18n.t('TPInsurance_Search.Arrival_City_Alert'));
                return false;
            } else if (this.startDate == undefined || this.startDate == '') {
                alertify.alert(i18n.t('TPInsurance_Search.Alert_Validation_Label'), i18n.t('TPInsurance_Search.Start_Date_Alert'));
                return false;
            } else if ((this.endDate == undefined || this.endDate == '') && this.triptype == 'R') {
                alertify.alert(i18n.t('TPInsurance_Search.Alert_Validation_Label'), i18n.t('TPInsurance_Search.Return_Date_Alert'));
                return false;
            } else if (this.departureCountry != 'AE' && (this.endDate == undefined || this.endDate == '')) {
                alertify.alert(i18n.t('TPInsurance_Search.Alert_Validation_Label'), i18n.t('TPInsurance_Search.Return_Date_Alert'));
                return false;
            }
            var startIn = $('#txtStartDate').val() == "" ? "" : $('#txtStartDate').datepicker('getDate');
            var endOut = $('#txtEndDate').val() == "" ? "" : $('#txtEndDate').datepicker('getDate');

            var deptDate = moment(startIn).format('DD|MM|YYYY');
            var arrivalDate = "NA";

            if (this.triptype == 'R') {
                arrivalDate = moment(endOut).format('DD|MM|YYYY');
            }
            var parameter = this.departureCountry + "-" + this.departureCity + "-" + this.arrivalCountry + "-" + this.arrivalCity + "--" + deptDate + "-" + arrivalDate + "--" + this.totalAdult + "|" + this.totalChildern + "|" + this.totalInfant + "--" + this.preferAirline;

            //var searchUrl = '/Insurance/TuneProtect/searchinsurance.html?flight=/' + sectors + '/' + adult + '-' + child + '-' + infant + '-' + cabin + '-' + airsupplier + '-50-F-' + tripType + '-F-' + preferAirline + '-' + directFlight;
            var searchUrl = '/Insurance/TuneProtect/searchinsurance.html?insurance=' + parameter;
            window.location.href = searchUrl;

        },
        countryChange: function(type) {
            if (type == 'Departure') {
                this.departureCity = "";
            } else {
                this.arrivalCity = "";
            }
        },
        changeTrip: function(type) {
            this.triptype = type;
            console.log("Type " + this.triptype);
            this.changeCalendarDates();

        },
        addingPassenger: function(typeOfAction, type) {
            this.changeCalendarDates();
            if (typeOfAction == 'Plus') {
                var total = parseInt(this.totalAdult) + parseInt(this.totalChildern) + parseInt(this.totalInfant);
                if (parseInt(this.totalPassenagers) > parseInt(total)) {
                    if (type == 'Adult') {
                        this.totalAdult = parseInt(this.totalAdult) + 1;
                    } else if (type == 'Child') {
                        this.totalChildern = parseInt(this.totalChildern) + 1;
                    } else if (type == 'Infant') {
                        this.totalInfant = parseInt(this.totalInfant) + 1;
                    }
                }


            } else {

                if (type == 'Adult') {
                    if (parseInt(this.totalAdult) > 1) {
                        this.totalAdult = parseInt(this.totalAdult) - 1;
                    }
                } else if (type == 'Child') {
                    if (parseInt(this.totalChildern) > 0) {
                        this.totalChildern = parseInt(this.totalChildern) - 1;
                    }
                } else if (type == 'Infant') {
                    if (parseInt(this.totalInfant) > 0) {
                        this.totalInfant = parseInt(this.totalInfant) - 1;
                    }
                }
            }
        },
        changeCalendarDates: function() {
            var vm = this;
            var startDate = $("#txtStartDate").datepicker('getDate');
            startDate.setDate(startDate.getDate() + 1);
            var dt2 = $('#txtEndDate');
            if (vm.triptype == 'R') {
                dt2.datepicker('option', 'minDate', startDate);
                dt2.datepicker('setDate', startDate);
                vm.endDate = $("#txtEndDate").val();
            } else {
                vm.endDate = "";
                dt2.datepicker('setDate', "");
            }
            vm.startDate = $("#txtStartDate").val();
        },
        hideautocompelteair: function() {
            this.airlineautoCompleteProgress = false;
        },
        onSelectedAirlineAutoComplete: _.debounce(function(keywordEntered) {
            if (keywordEntered.length > 2) {
                this.airlineautoCompleteProgress = true;
                this.airlineloading = true;
                var newData = [];
                this.airlineList.filter(function(el) {
                    if (el.A.toLowerCase().indexOf(keywordEntered.toLowerCase()) >= 0) {
                        newData.push({
                            "code": el.C,
                            "label": el.A
                        });
                    }
                });
                if (newData.length > 0) {
                    this.Airlineresults = newData;
                    this.airlineautoCompleteProgress = true;
                    this.airlineloading = false;
                } else {
                    this.Airlineresults = [];
                    this.airlineautoCompleteProgress = false;
                    this.airlineloading = false;
                }

            } else {
                this.Airlineresults = [];
                this.airlineautoCompleteProgress = false;
                this.airlineloading = false;
            }
        }, 100),
        onSelectedAirline: function(code, label) {
            this.airlineserch = label;
            this.Airlineresults = [];
            this.preferAirline = code;
        },
        retDateClose: function() {
            this.triptype = 'O';
        },
    },
    mounted: function() {
        var vm = this;
        vm.$nextTick(function() {
            var tempnumberofmonths = 1;
            if (parseInt($(window).width()) > 500 && parseInt($(window).width()) <= 999) {
                tempnumberofmonths = 2;
            } else if (parseInt($(window).width()) > 999) {
                tempnumberofmonths = 3;
            }

            if (vm.commonStore.selectedLanguage && vm.commonStore.selectedLanguage.code == 'ar') {
                $("#txtStartDate").datepicker("option", $.datepicker.regional['ar']);
                $("#txtEndDate").datepicker("option", $.datepicker.regional['ar']);
            } else {
                $("#txtStartDate").datepicker("option", $.datepicker.regional['en']);
                $("#txtEndDate").datepicker("option", $.datepicker.regional['en']);
            }
            var dateFormat = generalInformation.systemSettings.systemDateFormat;
            $("#txtStartDate").datepicker({
                minDate: "0",
                maxDate: "360",
                numberOfMonths: tempnumberofmonths,
                changeMonth: true,
                dateFormat: dateFormat,
                showButtonPanel: false,
                onSelect: function(selectedDate) {

                    vm.changeCalendarDates();
                    $("#txtEndDate").datepicker("option", "minDate", selectedDate);

                }
            });
            $("#txtEndDate").datepicker({
                minDate: "0",
                maxDate: "360",
                numberOfMonths: tempnumberofmonths,
                changeMonth: true,
                dateFormat: dateFormat,
                showButtonPanel: false,
                onSelect: function(selectedDate) {
                    vm.endDate = $("#txtEndDate").val();
                    vm.triptype = 'R';
                }
            });
            $("#txtStartDate").datepicker("setDate", new Date().getDay + 0);
            $("#txtEndDate").datepicker("setDate", new Date().getDay + 1);
            vm.changeCalendarDates();
            setTimeout(() => {
                $('[data-toggle="popover"]').popover({ html: true });
              }, 100);
        });

    },
    watch: {
        airlineserch: function() {
            if (this.airlineserch == "") {
                this.preferAirline = "";
            }
        },
        triptype: function(newVal, oldVal) {
            if (newVal == 'O' && oldVal == 'R') {
                this.changeCalendarDates();
            }
        }
    }
});

Vue.component("unitrust-insurance-search", {
    data: function () {
        return {
            status: true,
            triptype: "O",
            flightSearchCities: { cityFrom0: '', cityTo0: ''},
            flightSearchCountry: { cityFrom0: '', cityTo0: ''},
            flightSearchCityName: { cityFrom0: '', cityTo0: ''},
            FlightItemtext: { from: 'from', to: 'to' },
            FlightItemtID: {
                cityFrom0: 'cityFrom0',
                cityTo0: 'cityTo0'
            },
            duration: 1,
            totalPassenagers: 1,
            travellerList: [{id: 1, age: 1}],
            country: ""
        };
    },
    mounted: function() {
        var vm = this;
        vm.setCalender();
    },
    methods: {
        flightSearchcomplete(AirportCode, AirportName, item, direction, id, country) {
            var leg = item.replace(/^\D+/g, '');
            var from = 'cityFrom' + leg;
            var to = 'cityTo' + leg;
            if (direction == 'from') {
                if (this.flightSearchCities[to] == AirportCode) {
                    this.status = false;
                    this.flightSearchCountry[item] = '';
                    this.flightSearchCityName[item] = '';
                    this.flightSearchCities[item] = '';
                    $("#" + id).focus();
                    alertify.alert(i18n.t('Flight_Search.Alert_Validation_Label'), i18n.t('Flight_Search.Dept_and_Arr_Alert_Msg'));

                } else {
                    this.flightSearchCities[item] = AirportCode;
                    this.flightSearchCityName[item] = AirportName;
                    this.flightSearchCountry[item] = country;
                    this.status = true;
                }
            } else {
                if (this.flightSearchCities[from] == AirportCode) {
                    this.status = false;
                    this.flightSearchCityName[item] = '';
                    this.flightSearchCountry[item] = '';
                    this.flightSearchCities[item] = '';
                    $("#" + id).focus();
                    alertify.alert(i18n.t('Flight_Search.Alert_Validation_Label'), i18n.t('Flight_Search.Dept_and_Arr_Alert_Msg'));
                } else {
                    this.flightSearchCities[item] = AirportCode;
                    this.flightSearchCityName[item] = AirportName;
                    this.flightSearchCountry[item] = country;
                    this.status = true;
                }
            }
        },
        setCalender() {
            var vm = this;
            var systemDateFormat = generalInformation.systemSettings.systemDateFormat;
            var numberofmonths = generalInformation.systemSettings.calendarDisplay;
            var vm = this;
            var tempnumberofmonths = numberofmonths;
            if (parseInt($(window).width()) > 500 && parseInt($(window).width()) <= 999) {
                tempnumberofmonths = 2;
            } else if (parseInt($(window).width()) > 999) {
                tempnumberofmonths = 3;
            }
            $("#deptDate01").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: tempnumberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                onSelect: function(selectedDate) {
                    vm.oldRetDate = selectedDate;
                    $("#retDate").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#retDate").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: tempnumberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function(event, ui) {
                    var selectedDate = $("#deptDate01").val();
                    $("#retDate").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function(selectedDate) {
                    if ($("#deptDate01").datepicker('getDate')) {
                        var a = $("#deptDate01").datepicker('getDate').getTime();
                        var b = $("#retDate").datepicker('getDate').getTime();
                        var c = 24 * 60 * 60 * 1000;
                        var diffDays = Math.round(Math.abs((a - b) / c));
                        vm.duration = diffDays <= 0 ? 1 : diffDays;
                    }

                }
            });
        },
        addingPassenger: function(typeOfAction, type) {
            if (typeOfAction == 'Plus') {
                var total = this.travellerList.length;
                if (parseInt(this.totalPassenagers) > parseInt(total)) {
                    if (type == 'Adult') {
                        this.travellerList.push({id: this.travellerList.length, age: 1});
                    }
                }
            } else {
                if (type == 'Adult') {
                    if (parseInt(this.travellerList.length) > 1) {
                        this.travellerList.pop();
                    }
                }
            }
        },
        validateInsurance() {
          var isSuccess = true;
          var vm = this;
    
          if (stringIsNullorEmpty($('#deptDate01').val())) {
            alertify.alert('Warning', 'Please select departure date.');
            return false;
          }
    
          if (stringIsNullorEmpty($('#retDate').val())) {
            alertify.alert('Warning', 'Please select return date.');
            return false;
          }
    
          if (!this.flightSearchCities.cityFrom0) {
            alertify.alert(i18n.t('Flight_Search.Alert_Validation_Label'), i18n.t('Flight_Search.Origin_Alert_Msg'));
            return false;
          }
          if (!this.flightSearchCities.cityTo0) {
            alertify.alert(i18n.t('Flight_Search.Alert_Validation_Label'), i18n.t('Flight_Search.Dest_Alert_Msg'));
            return false;
          }
          return isSuccess;
        },
        searchInsurance: function() {
          var proceed = this.validateInsurance();
          if (proceed) {
            var startDate = moment($('#deptDate01').val(), 'DD MMM YY,ddd').format('DD|MM|YYYY');
            var endDate = moment($('#retDate').val(), 'DD MMM YY,ddd').format('DD|MM|YYYY');
            var parameter = this.flightSearchCountry.cityFrom0 + "-" 
            + this.flightSearchCities.cityFrom0 + "-" 
            + this.flightSearchCountry.cityTo0 + "-" 
            + this.flightSearchCities.cityTo0  + "--" 
            + startDate + "-" + endDate 
            + "--" + this.travellerList.filter((e)=>e.age < 2).length + "|" 
            + this.travellerList.filter((e)=>e.age >= 2 && e.age <=16).length + "|" 
            + this.travellerList.filter((e)=>e.age > 16 && e.age <=75).length + "|" 
            + this.travellerList.filter((e)=>e.age > 75).length;
            sessionStorage.travellerList = JSON.stringify(this.travellerList);
            window.location.href = '/Insurance/unitrust/searchinsurance.html?insurance=' + parameter.toUpperCase();
          }
        }
    }
})

Vue.component("transfer-search", {
    data: function() {
        return {
            commonStore: vueCommonStore.state
        };
    }
});