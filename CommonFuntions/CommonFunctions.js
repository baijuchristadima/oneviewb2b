var generalInformation = {
        systemSettings: {
            systemDateFormat: 'dd M y, D',
            calendarDisplay: 1,
            calendarDisplayInMobile: 1
        },
    }
    /**Sorting Function for AutoCompelte Start**/
function SortInputFirst(input, data) {
    var first = [];
    var others = [];
    for (var i = 0; i < data.length; i++) {
        if (data[i].supplier_city_name.toLowerCase().startsWith(input.toLowerCase())) {
            first.push(data[i]);
        } else {

            others.push(data[i]);
        }
    }
    first.sort(function(a, b) {
        var nameA = a.supplier_city_name.toLowerCase(),
            nameB = b.supplier_city_name.toLowerCase()
        if (nameA < nameB) //sort string ascending
            return -1
        if (nameA > nameB)
            return 1
        return 0 //default return value (no sorting)
    })
    others.sort(function(a, b) {
        var nameA = a.supplier_city_name.toLowerCase(),
            nameB = b.supplier_city_name.toLowerCase()
        if (nameA < nameB) //sort string ascending
            return -1
        if (nameA > nameB)
            return 1
        return 0 //default return value (no sorting)
    })
    return (first.concat(others));
}

function SortInputFirstFlight(input, data) {
    var first = [];
    var others = [];
    for (var i = 0; i < data.length; i++) {
        data[i].municipality = isNullorEmptyToBlank(data[i].municipality);
        if (data[i].iata_code.toLowerCase().startsWith(input.toLowerCase())) {
            first.push(data[i]);
        } else if (data[i].name.toLowerCase().startsWith(input.toLowerCase())) {
            first.push(data[i]);
        } else if (data[i].municipality.toLowerCase().startsWith(input.toLowerCase())) {
            first.push(data[i]);
        } else {
            others.push(data[i]);
        }
    }
    // first.sort(function (a, b) {
    //   var nameA = a.name.toLowerCase(),
    //     nameB = b.name.toLowerCase()
    //   if (nameA < nameB)
    //     return -1
    //   if (nameA > nameB)
    //     return 1
    //   return 0
    // })
    // others.sort(function (a, b) {
    //   var nameA = a.name.toLowerCase(),
    //     nameB = b.name.toLowerCase()
    //   if (nameA < nameB)
    //     return -1
    //   if (nameA > nameB)
    //     return 1
    //   return 0
    // })
    return (first.concat(others));
}
/**UUID Generation Start**/
function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16 | 0,
            v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}
/**UUID Generation End**/

//send Mail Functions
function sendMailService(reqUrl, postData) {
    return axios
        .post(reqUrl, postData)
        .then(res => {
            // console.log("Mail Status: ", res);
            return true;
        })
        .catch(err => {
            console.log("Mail Status: ", err);
            return false;
        });
}

function mainAxiosRequest(config) {


    // var config = {
    //   axiosConfig: {
    //     method: "GET",
    //     url: "",
    //     headers: {
    //       "Content-Type": "application/json",
    //       "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
    //     }
    //   },
    //   successCallback: function (response) {},
    //   errorCallback: function (error) {},
    //   showAlert: false
    // }

    if (!config.axiosConfig) {
        alertify.alert("Error", "Please enter axios config.");
        return false;
    }

    config.axiosConfig.headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
    };
    axios(config.axiosConfig)
        .then(function(response) {
            // handle success
            var error = null;
            try {
                error = response.data.response.content.error;
            } catch (error) {}

            if (config.successCallback) {
                if (typeof error == "undefined" || error == null) {
                    config.successCallback(response);
                } else {
                    if (config.showAlert) {
                        if (config.showAdditionalAlerts) {
                            config.successCallback(response);
                        } else {
                            config.errorCallback(error);
                            alertify.alert((error.code || "Error").toString(), error.message || "We have found some technical difficulties. Please contact admin.");
                        }
                    }
                }
            } else {
                alertify.alert("Error", "Please enter success callback.");
            }
        })
        .catch(function(error) {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                // console.log(error.response.data);
                // console.log(error.response.status);
                // console.log(error.response.headers);

                //if you want to control your own error message please put an
                // error callback here
                // if (config.errorCallback) {
                config.errorCallback(error);
                // } else {
                var status = error.response.status;
                var data = error.response.data;

                switch (status) {
                    case 400:
                    case 402:
                    case 408:
                        // case 500:
                        if (config.showAlert) {
                            alertify.alert((data.code || "Error").toString(), data.message || "We have found some technical difficulties. Please contact admin.");
                        }
                        break;
                    default:
                        if (config.showAlert) {
                            alertify.alert("Error", "We have found some technical difficulties. Please contact admin.");
                        }
                }
                console.log(error);

                // }
            } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                config.errorCallback(error);
                alertify.alert("Error", "The request was made but no response was received.");
                console.log(error.request);
            } else {
                // Something happened in setting up the request that triggered an Error
                config.errorCallback(error);
                alertify.alert("Error", error.message);
                console.log('Error', error);
            }
        });
}

function ColorLuminance(hex, lum) {
    // validate hex string
    hex = String(hex).replace(/[^0-9a-f]/gi, "");
    if (hex.length < 6) {
        hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    }
    lum = lum || 0;

    // convert to decimal and change luminosity
    var rgb = "#",
        c, i;
    for (i = 0; i < 3; i++) {
        c = parseInt(hex.substr(i * 2, 2), 16);
        c = Math.round(Math.min(Math.max(0, c + c * lum), 255)).toString(16);
        rgb += ("00" + c).substr(c.length);
    }

    return rgb;
}

//dynamic theming
var lookNfeel = JSON.parse(window.localStorage.getItem("lookNfeel"));
var primary;
var secondary;
var tertiary;
try {
    primary = lookNfeel.primary;
    secondary = lookNfeel.secondary;
    tertiary = lookNfeel.tertiary;
} catch (err) {}

function setColorTheme() {
    $('head').append('<style>:root{' +
        '--primary-color:' + ColorLuminance(primary, 0) +
        ';--primary-light-color:' + ColorLuminance(primary, 0.3) +
        ';--primary-dark-color:' + ColorLuminance(primary, -0.3) +
        ';--secondary-color:' + ColorLuminance(secondary, 0) +
        ';--secondary-light-color:' + ColorLuminance(secondary, 0.3) +
        ';--secondary-dark-color:' + ColorLuminance(secondary, -0.3) +
        ';--tertiary-color:' + ColorLuminance(tertiary, 0) +
        ';--tertiary-light-color:' + ColorLuminance(tertiary, 0.3) +
        ';---tertiary-dark-color:' + ColorLuminance(tertiary, -0.3) +
        ';' + '}</style>');
}

$(document).ready(function() {

    setColorTheme();
    pageWiseBind();

    function setBanner(response, service) {
        try {
            return ((_.filter(response.data.siteList[0].contentType, function(data) { return data.name == service; })[0] || {}).value || "").replace(/(\r\n|\n|\r)/gm, "");
        } catch (error) {
            return "";
        }
    }

    if (window.location.pathname.includes("search.html") ||
        window.location.pathname.includes("flightresults.html") ||
        window.location.pathname.includes("travellerdetails.html") ||
        window.location.pathname.includes("hotelresults.html") ||
        window.location.pathname.includes("hotelpax.html") ||
        window.location.pathname.includes("searchinsurance.html") ||
        window.location.pathname.includes("IssuePolicy.html")) {

        // axios.get("/Resources/HubUrls/HubServiceUrls.json").then(function (response) {
        var hubUrls = HubServiceUrls;
        var hubUrl = hubUrls.hubConnection.baseUrl;
        var port = hubUrls.hubConnection.ipAddress;
        var contact = hubUrls.hubConnection.hubServices.contact;
        axios.get(hubUrl + port + contact, {
            headers: {
                Authorization: "Bearer " + window.localStorage.getItem("accessToken")
            }
        }).then(function(response) {
            var agencyNode = window.localStorage.getItem("agencyNode");
            var hqNode = "AGY" + JSON.parse(atob(agencyNode)).loginNode.solutionId;

            var amazonUrl = "https://site-details.s3.ap-south-1.amazonaws.com/" + hqNode + "/Banners/";
            var banners = [
                // flights
                {
                    variable: "--flight-search-banner",
                    url: setBanner(response, "flightSearchPageBanner"),
                    fallback: amazonUrl + "flights/search-banner.jpg"
                },
                {
                    variable: "--flight-result-banner",
                    url: setBanner(response, "flightResultPageBanner"),
                    fallback: amazonUrl + "flights/result-banner.jpg"
                },
                {
                    variable: "--flight-booking-banner",
                    url: setBanner(response, "flightBookingPageBanner"),
                    fallback: amazonUrl + "flights/booking-banner.jpg"
                },
                {
                    variable: "--flight-voucher-banner",
                    url: setBanner(response, "flightVoucherPageBanner"),
                    fallback: amazonUrl + "flights/voucher-banner.jpg"
                },
                // hotels
                {
                    variable: "--hotel-search-banner",
                    url: setBanner(response, "hotelSearchPageBanner"),
                    fallback: amazonUrl + "hotels/search-banner.jpg"
                },
                {
                    variable: "--hotel-result-banner",
                    url: setBanner(response, "hotelResultPageBanner"),
                    fallback: amazonUrl + "hotels/result-banner.jpg"
                },
                {
                    variable: "--hotel-booking-banner",
                    url: setBanner(response, "hotelBookingPageBanner"),
                    fallback: amazonUrl + "hotels/booking-banner.jpg"
                },
                {
                    variable: "--hotel-voucher-banner",
                    url: setBanner(response, "hotelVoucherPageBanner"),
                    fallback: amazonUrl + "hotels/voucher-banner.jpg"
                },
                // tune protect
                {
                    variable: "--insuranceTP-search-banner",
                    url: setBanner(response, "InsuranceTPSearchPageBanner"),
                    fallback: amazonUrl + "insurance/tp-search-banner.jpg"
                },
                {
                    variable: "--insuranceTP-result-banner",
                    url: setBanner(response, "InsuranceTPResultPageBanner"),
                    fallback: amazonUrl + "insurance/tp-result-banner.jpg"
                },
                {
                    variable: "--insuranceTP-booking-banner",
                    url: setBanner(response, "InsuranceTPBookingPageBanner"),
                    fallback: amazonUrl + "insurance/tp-booking-banner.jpg"
                },
                {
                    variable: "--insuranceTP-voucher-banner",
                    url: setBanner(response, "InsuranceTPVoucherPageBanner"),
                    fallback: amazonUrl + "insurance/tp-voucher-banner.jpg"
                },
                // wis
                {
                    variable: "--insuranceWIS-search-banner",
                    url: setBanner(response, "InsuranceWISSearchPageBanner"),
                    fallback: amazonUrl + "insurance/wis-search-banner.jpg"
                },
                {
                    variable: "--insuranceWIS-result-banner",
                    url: setBanner(response, "InsuranceWISResultPageBanner"),
                    fallback: amazonUrl + "insurance/wis-result-banner.jpg"
                },
                {
                    variable: "--insuranceWIS-booking-banner",
                    url: setBanner(response, "InsuranceWISBookingPageBanner"),
                    fallback: amazonUrl + "insurance/wis-booking-banner.jpg"
                },
                {
                    variable: "--insuranceWIS-voucher-banner",
                    url: setBanner(response, "InsuranceWISVoucherPageBanner"),
                    fallback: amazonUrl + "insurance/wis-voucher-banner.jpg"
                },
                // transfer
                {
                    variable: "--transfer-search-banner",
                    url: setBanner(response, "transferSearchPageBanner"),
                    fallback: amazonUrl + "insurance/search-banner.jpg"
                },
                {
                    variable: "--transfer-result-banner",
                    url: setBanner(response, "transferResultPageBanner"),
                    fallback: amazonUrl + "insurance/result-banner.jpg"
                },
                {
                    variable: "--transfer-booking-banner",
                    url: setBanner(response, "transferBookingPageBanner"),
                    fallback: amazonUrl + "insurance/booking-banner.jpg"
                },
                {
                    variable: "--transfer-voucher-banner",
                    url: setBanner(response, "transferVoucherPageBanner"),
                    fallback: amazonUrl + "insurance/voucher-banner.jpg"
                },
                // sightseeing
                {
                    variable: "--sightseeing-search-banner",
                    url: setBanner(response, "SightseeingSearchPageBanner"),
                    fallback: amazonUrl + "sightseeing/search-banner.jpg"
                },
                {
                    variable: "--sightseeing-result-banner",
                    url: setBanner(response, "SightseeingResultPageBanner"),
                    fallback: amazonUrl + "sightseeing/result-banner.jpg"
                },
                {
                    variable: "--sightseeing-booking-banner",
                    url: setBanner(response, "SightseeingBookingPageBanner"),
                    fallback: amazonUrl + "sightseeing/booking-banner.jpg"
                },
                {
                    variable: "--sightseeing-voucher-banner",
                    url: setBanner(response, "SightseeingVoucherPageBanner"),
                    fallback: amazonUrl + "sightseeing/voucher-banner.jpg"
                }
            ]
            var bannerString = "";
            for (var b = 0; b < banners.length; b++) {
                bannerString += banners[b].variable + ":url('" + (banners[b].url || banners[b].fallback) + "');";
            }
            $('head').append('<style>:root{' + bannerString + '}</style>');

            // $('head').append('<style>:root{' +
            //   '--flight-search-banner:url("' + setBanner(response, "flightSearchPageBanner") +
            //   '");--flight-result-banner:url("' + setBanner(response, "flightResultPageBanner") +
            //   '");--flight-booking-banner:url("' + setBanner(response, "flightBookingPageBanner") +
            //   '");--flight-voucher-banner:url("' + setBanner(response, "flightVoucherPageBanner") +
            //   '");--hotel-search-banner:url("' + setBanner(response, "hotelSearchPageBanner") +
            //   '");--hotel-result-banner:url("' + setBanner(response, "hotelResultPageBanner") +
            //   '");--insurance-search-banner:url("' + setBanner(response, "InsuranceSearchPageBanner") +
            //   '");--insuranceTP-search-banner:url("' + setBanner(response, "InsuranceTPSearchPageBanner") +
            //   '");--sightseeing-search-banner:url("' + setBanner(response, "SightseeingSearchPageBanner") +
            //   '");--sightseeing-result-banner:url("' + setBanner(response, "SightseeingResultPageBanner") +
            //   '");--transfer-search-banner:url("' + setBanner(response, "transferSearchPageBanner") +
            //   '");' + '}</style>');
        }).catch(function(error) {
            console.log(error);
        });
        // }).catch(function (error) {
        //   console.log(error);
        // });
    }

});

Vue.directive('click-outside', {
    bind: function(el, binding, vnode) {
        el.clickOutsideEvent = function(event) {
            // here I check that click was outside the el and his childrens
            if (!(el == event.target || el.contains(event.target))) {
                // and if it did, call method provided in attribute value
                if (vnode.context[binding.expression]) {
                    vnode.context[binding.expression](event);
                }
            }
        };
        document.body.addEventListener('click', el.clickOutsideEvent)
    },
    unbind: function(el) {
        document.body.removeEventListener('click', el.clickOutsideEvent)
    },
});

var websocketMixin = {
    data: function() {
        return {
            webSocketStatus: "Disconnected",
            socket: null
        };
    },
    methods: {
        //websocket connection
        connect: function() {
            var vm = this;
            //Connecting to websocket server
            // axios.get("/Resources/HubUrls/HubServiceUrls.json").then(function (response) {

            vm.socket = new WebSocket(vm.commonStore.hubUrls.hubConnection.webSocketUrl);

            vm.socket.onopen = () => { //get all the response here
                vm.webSocketStatus = "Connected"
                // console.log("Status: " + vm.webSocketStatus);
                vm.socket.onmessage = ({ data }) => {
                    var message = JSON.parse(data);
                    // if (message.message == "Success") {
                    //   message.requestDetails = { count: 4, service: "HotelRS" };
                    // }
                    if (message.response || message.requestDetails) {
                        var serviceId = "";
                        if (message.response) {
                            serviceId = message.response.service;
                        } else {
                            serviceId = message.requestDetails.service;
                        }
                        // console.log("Recieved message: ", serviceId, message);
                        switch (serviceId) {
                            case 'FlightRS':
                                //flight Search
                                vm.processFlightResult(message);
                                break;
                            case 'HotelRS':
                                //hotel Search
                                vm.fetchHotelList(message);
                                break;
                            case 'InsuranceRS':
                            case 'InsuranceUniTrustRS':
                            case 'InsuranceWisRS':
                                //insurance Search
                                vm.processInsuranceResult(message);
                            default:
                                break;
                        }
                    }
                };
            };
            // });
        },
        disconnect: function() { //Disconnect connection from websocket server
            this.socket.close();
            this.webSocketStatus = "Disconnected";
        },
        sendMessage: function(message) { //sending requests to websocket server
            var vm = this;
            setTimeout(function() {
                if (vm.socket && vm.socket.readyState === 1) {
                    // console.log("Connection is made...")
                    vm.socket.send(message);
                    // console.log("Sent message: " + message);
                } else {
                    // console.log("Wait for connection...")
                    vm.sendMessage(message)
                }
            }, 1); // wait 1 milisecond for the connection...
        }
    }
}

var currencyDefault = JSON.parse(window.localStorage.getItem("currencyI18n"));
var languageDefault = JSON.parse(window.localStorage.getItem("languageI18n"));

var vueCommonStore = {
  state: {
    showSideMenu: false, // updated in  'master-header'
    agencyNode: null, // read only
    currentSearchpage: "", // updated in header and master vue app
    currentSearchBookingpage: "",
    hubUrls: {},
    selectedCurrency: currencyDefault ? currencyDefault.c : "",
    currencyMultiplier: currencyDefault ? currencyDefault.m : "",
    currencyRates: [],
    selectedLanguage: languageDefault,
    creditAvailableLimit: '',
    showCreditLimit: false,
    selectedNode: "", // for node switching
    nodeList: [],
    accountManagerDetails: null,
    fallBackEmail: "itsolutionsoneview@gmail.com",
    selectedCart: {},
    carts: [
      {
        id: uuidv4(),
        name: "Default",
        services: {
          flights: [],
          hotels: [],
          insuranceTP: [],
          insuranceWIS: []
        }
      }
    ],
    commonRoles: { // updated in master intance
      roleTimer: 0,
      //services roles array
      airRoles: [],
      hotelRoles: [],
      insuranceRoles: [],
      insuranceTuneProtectRoles: [],
      sightseeingRoles: [],
      transferRoles: [],
      //services header links roles
      hasAir: false,
      hasHotel: false,
      hasSightseeing: false,
      hasInsurance: false,
      hasInsuranceTP: false,
      hasTransfer: false,

            hasAirDashboard: false,
            hasAirActionRequired: false,
            hasAirQuotation: false,
            hasAirSupplier: false,
            hasAirSupplierFilter: false,
            hasAirBook: false,
            hasAirFareCalendar: false,
            hasAirPaymentGateway: false,
            hasAirPreferredAndDirectAirline: false,
            hasAirRecentSearch: false,
            hasAirIssueTicket: false,
            hasAirCancelPNR: false,
            hasAirCancelTicket: false,
            hasMiniRuleRole: false,
            hasFareRuleRole: false,

            hasAirSearchBookings: false,
            hasAirAncillaryBaggage: false,
            hasAirAncillarySeat: false,
            hasAirAncillaryMeals: false,
            hasAirAncillaryInsurence: false,
            hasAirAncillaryAssistance: false,
            hasPassportOptional: false,
            hasAirOfficeId: false,
            hasAirSameRBD: false,
            hasAirLowestRBD: false,
            hasAirOfflineRequestList: false,
            hasAirOfflineTicketing: false,
            hasAirOfflineBooking: false,
            hasAirOfflineTicketAfterBooking: false,
            hasAirOfflineRefund: false,
            hasAirOfflineVisa: false,
            
            hasMoreOptions: false,
            hasShowMarkupComponent: false,
            hasCalendarSearch: false,
            hasShowFlightDaysDuration: false,
            hasAirIssueContactNumber: false,

            //news and notice
            hasNewsLetter: false,
            hasNoticeBoard: false,

            hasEditVoucher: false,
            hasAirConsolidatedBookings: false,

            hasInsuranceDashboard: false,
            hasInsuranceActionRequired: false,
            hasInsuranceQuotation: false,
            hasInsuranceBook: false,
            hasInsurancePaymentGateway: false,
            hasInsuranceSearchBookings: false,
            hasInsuranceConsolidatedBookings: false,

            hasSightseeingDashboard: false,
            hasSightseeingActionRequired: false,
            hasSightseeingQuotation: false,
            hasSightseeingBook: false,
            hasSightseingPaymentGateway: false,
            hasSightseingSearchBookings: false,
            hasSightseingConsolidatedBookings: false,

            hasInsuranceTPDashboard: false,
            hasInsuranceTPActionRequired: false,
            hasInsuranceTPQuotation: false,
            hasInsuranceTPBook: false,
            hasInsuranceTPPaymentGateway: false,
            hasInsuranceTPSearchBookings: false,
            hasInsuranceTPConsolidatedBookings: false,
            hasQuotation: false,

            hasSetPrivileges: false,

            hasAirPnrImport: false,

            hasAirLogintoAdminPanel: false,

            allHotelRolesLoaded: false,
            hasHotelDashboard: false,
            hasHotelActionRequired: false,
            hasHotelQuotation: false,
            hasHotelSupplier: false,
            hasHotelBook: false,
            hasHotelSupplierFilter: false,
            hasHotelSearchBookings: false,
            hasHotelVoucher: false,
            hasHotelCancelBooking: false,
            hasHotelStatusHistory: false,
            hasHotelMarkupDetails: false,
            hasHotelMap: false,
            hasHotelPaymentGateway: false,
            hasHotelEditVoucher: false,
            hasHotelCancellationRequest: false,
            hasHideCancellationPolicy: false,
            hasHidePayableThrough: false,

            hasHotelConsolidatedBookings: false,

            hasTransferDashboard: false,
            hasTransferActionRequired: false,
            hasTransferQuotation: false,
            hasTransferBook: false,
            hastranferPaymentGateway: false,
            hastranferSearchBookings: false,
            hastranferConsolidatedBookings: false,
            hasSupport: false,

            hasSupplierCurrency: false
        },
        hotels: {}
    }
};

vueCommonStore.state.hubUrls = HubServiceUrls;


var numberFormats = {
    'AED': { currency: { style: 'currency', currency: 'AED', currencyDisplay: 'code' } },
    'AFN': { currency: { style: 'currency', currency: 'AFN', currencyDisplay: 'code' } },
    'ALL': { currency: { style: 'currency', currency: 'ALL', currencyDisplay: 'code' } },
    'AMD': { currency: { style: 'currency', currency: 'AMD', currencyDisplay: 'code' } },
    'ANG': { currency: { style: 'currency', currency: 'ANG', currencyDisplay: 'code' } },
    'AOA': { currency: { style: 'currency', currency: 'AOA', currencyDisplay: 'code' } },
    'ARS': { currency: { style: 'currency', currency: 'ARS', currencyDisplay: 'code' } },
    'AUD': { currency: { style: 'currency', currency: 'AUD', currencyDisplay: 'code' } },
    'AWG': { currency: { style: 'currency', currency: 'AWG', currencyDisplay: 'code' } },
    'AZN': { currency: { style: 'currency', currency: 'AZN', currencyDisplay: 'code' } },
    'BAM': { currency: { style: 'currency', currency: 'BAM', currencyDisplay: 'code' } },
    'BBD': { currency: { style: 'currency', currency: 'BBD', currencyDisplay: 'code' } },
    'BDT': { currency: { style: 'currency', currency: 'BDT', currencyDisplay: 'code' } },
    'BGN': { currency: { style: 'currency', currency: 'BGN', currencyDisplay: 'code' } },
    'BHD': { currency: { style: 'currency', currency: 'BHD', currencyDisplay: 'code' } },
    'BIF': { currency: { style: 'currency', currency: 'BIF', currencyDisplay: 'code' } },
    'BMD': { currency: { style: 'currency', currency: 'BMD', currencyDisplay: 'code' } },
    'BND': { currency: { style: 'currency', currency: 'BND', currencyDisplay: 'code' } },
    'BOB': { currency: { style: 'currency', currency: 'BOB', currencyDisplay: 'code' } },
    'BRL': { currency: { style: 'currency', currency: 'BRL', currencyDisplay: 'code' } },
    'BSD': { currency: { style: 'currency', currency: 'BSD', currencyDisplay: 'code' } },
    'BTN': { currency: { style: 'currency', currency: 'BTN', currencyDisplay: 'code' } },
    'BWP': { currency: { style: 'currency', currency: 'BWP', currencyDisplay: 'code' } },
    'BYN': { currency: { style: 'currency', currency: 'BYN', currencyDisplay: 'code' } },
    'BZD': { currency: { style: 'currency', currency: 'BZD', currencyDisplay: 'code' } },
    'CAD': { currency: { style: 'currency', currency: 'CAD', currencyDisplay: 'code' } },
    'CDF': { currency: { style: 'currency', currency: 'CDF', currencyDisplay: 'code' } },
    'CHF': { currency: { style: 'currency', currency: 'CHF', currencyDisplay: 'code' } },
    'CLP': { currency: { style: 'currency', currency: 'CLP', currencyDisplay: 'code' } },
    'CNY': { currency: { style: 'currency', currency: 'CNY', currencyDisplay: 'code' } },
    'COP': { currency: { style: 'currency', currency: 'COP', currencyDisplay: 'code' } },
    'CRC': { currency: { style: 'currency', currency: 'CRC', currencyDisplay: 'code' } },
    'CUC': { currency: { style: 'currency', currency: 'CUC', currencyDisplay: 'code' } },
    'CUP': { currency: { style: 'currency', currency: 'CUP', currencyDisplay: 'code' } },
    'CVE': { currency: { style: 'currency', currency: 'CVE', currencyDisplay: 'code' } },
    'CZK': { currency: { style: 'currency', currency: 'CZK', currencyDisplay: 'code' } },
    'DJF': { currency: { style: 'currency', currency: 'DJF', currencyDisplay: 'code' } },
    'DKK': { currency: { style: 'currency', currency: 'DKK', currencyDisplay: 'code' } },
    'DOP': { currency: { style: 'currency', currency: 'DOP', currencyDisplay: 'code' } },
    'DZD': { currency: { style: 'currency', currency: 'DZD', currencyDisplay: 'code' } },
    'EGP': { currency: { style: 'currency', currency: 'EGP', currencyDisplay: 'code' } },
    'ERN': { currency: { style: 'currency', currency: 'ERN', currencyDisplay: 'code' } },
    'ETB': { currency: { style: 'currency', currency: 'ETB', currencyDisplay: 'code' } },
    'EUR': { currency: { style: 'currency', currency: 'EUR', currencyDisplay: 'code' } },
    'FJD': { currency: { style: 'currency', currency: 'FJD', currencyDisplay: 'code' } },
    'FKP': { currency: { style: 'currency', currency: 'FKP', currencyDisplay: 'code' } },
    'GBP': { currency: { style: 'currency', currency: 'GBP', currencyDisplay: 'code' } },
    'GEL': { currency: { style: 'currency', currency: 'GEL', currencyDisplay: 'code' } },
    'GGP': { currency: { style: 'currency', currency: 'GGP', currencyDisplay: 'code' } },
    'GHS': { currency: { style: 'currency', currency: 'GHS', currencyDisplay: 'code' } },
    'GIP': { currency: { style: 'currency', currency: 'GIP', currencyDisplay: 'code' } },
    'GMD': { currency: { style: 'currency', currency: 'GMD', currencyDisplay: 'code' } },
    'GNF': { currency: { style: 'currency', currency: 'GNF', currencyDisplay: 'code' } },
    'GTQ': { currency: { style: 'currency', currency: 'GTQ', currencyDisplay: 'code' } },
    'GYD': { currency: { style: 'currency', currency: 'GYD', currencyDisplay: 'code' } },
    'HKD': { currency: { style: 'currency', currency: 'HKD', currencyDisplay: 'code' } },
    'HNL': { currency: { style: 'currency', currency: 'HNL', currencyDisplay: 'code' } },
    'HRK': { currency: { style: 'currency', currency: 'HRK', currencyDisplay: 'code' } },
    'HTG': { currency: { style: 'currency', currency: 'HTG', currencyDisplay: 'code' } },
    'HUF': { currency: { style: 'currency', currency: 'HUF', currencyDisplay: 'code' } },
    'IDR': { currency: { style: 'currency', currency: 'IDR', currencyDisplay: 'code' } },
    'ILS': { currency: { style: 'currency', currency: 'ILS', currencyDisplay: 'code' } },
    'IMP': { currency: { style: 'currency', currency: 'IMP', currencyDisplay: 'code' } },
    'INR': { currency: { style: 'currency', currency: 'INR', currencyDisplay: 'code' } },
    'IQD': { currency: { style: 'currency', currency: 'IQD', currencyDisplay: 'code' } },
    'IRR': { currency: { style: 'currency', currency: 'IRR', currencyDisplay: 'code' } },
    'ISK': { currency: { style: 'currency', currency: 'ISK', currencyDisplay: 'code' } },
    'JEP': { currency: { style: 'currency', currency: 'JEP', currencyDisplay: 'code' } },
    'JMD': { currency: { style: 'currency', currency: 'JMD', currencyDisplay: 'code' } },
    'JOD': { currency: { style: 'currency', currency: 'JOD', currencyDisplay: 'code' } },
    'JPY': { currency: { style: 'currency', currency: 'JPY', currencyDisplay: 'code' } },
    'KES': { currency: { style: 'currency', currency: 'KES', currencyDisplay: 'code' } },
    'KGS': { currency: { style: 'currency', currency: 'KGS', currencyDisplay: 'code' } },
    'KHR': { currency: { style: 'currency', currency: 'KHR', currencyDisplay: 'code' } },
    'KMF': { currency: { style: 'currency', currency: 'KMF', currencyDisplay: 'code' } },
    'KPW': { currency: { style: 'currency', currency: 'KPW', currencyDisplay: 'code' } },
    'KRW': { currency: { style: 'currency', currency: 'KRW', currencyDisplay: 'code' } },
    'KWD': { currency: { style: 'currency', currency: 'KWD', currencyDisplay: 'code' } },
    'KYD': { currency: { style: 'currency', currency: 'KYD', currencyDisplay: 'code' } },
    'KZT': { currency: { style: 'currency', currency: 'KZT', currencyDisplay: 'code' } },
    'LAK': { currency: { style: 'currency', currency: 'LAK', currencyDisplay: 'code' } },
    'LBP': { currency: { style: 'currency', currency: 'LBP', currencyDisplay: 'code' } },
    'LKR': { currency: { style: 'currency', currency: 'LKR', currencyDisplay: 'code' } },
    'LRD': { currency: { style: 'currency', currency: 'LRD', currencyDisplay: 'code' } },
    'LSL': { currency: { style: 'currency', currency: 'LSL', currencyDisplay: 'code' } },
    'LYD': { currency: { style: 'currency', currency: 'LYD', currencyDisplay: 'code' } },
    'MAD': { currency: { style: 'currency', currency: 'MAD', currencyDisplay: 'code' } },
    'MDL': { currency: { style: 'currency', currency: 'MDL', currencyDisplay: 'code' } },
    'MGA': { currency: { style: 'currency', currency: 'MGA', currencyDisplay: 'code' } },
    'MKD': { currency: { style: 'currency', currency: 'MKD', currencyDisplay: 'code' } },
    'MMK': { currency: { style: 'currency', currency: 'MMK', currencyDisplay: 'code' } },
    'MNT': { currency: { style: 'currency', currency: 'MNT', currencyDisplay: 'code' } },
    'MOP': { currency: { style: 'currency', currency: 'MOP', currencyDisplay: 'code' } },
    'MRO': { currency: { style: 'currency', currency: 'MRO', currencyDisplay: 'code' } },
    'MUR': { currency: { style: 'currency', currency: 'MUR', currencyDisplay: 'code' } },
    'MVR': { currency: { style: 'currency', currency: 'MVR', currencyDisplay: 'code' } },
    'MWK': { currency: { style: 'currency', currency: 'MWK', currencyDisplay: 'code' } },
    'MXN': { currency: { style: 'currency', currency: 'MXN', currencyDisplay: 'code' } },
    'MYR': { currency: { style: 'currency', currency: 'MYR', currencyDisplay: 'code' } },
    'MZN': { currency: { style: 'currency', currency: 'MZN', currencyDisplay: 'code' } },
    'NAD': { currency: { style: 'currency', currency: 'NAD', currencyDisplay: 'code' } },
    'NGN': { currency: { style: 'currency', currency: 'NGN', currencyDisplay: 'code' } },
    'NIO': { currency: { style: 'currency', currency: 'NIO', currencyDisplay: 'code' } },
    'NOK': { currency: { style: 'currency', currency: 'NOK', currencyDisplay: 'code' } },
    'NPR': { currency: { style: 'currency', currency: 'NPR', currencyDisplay: 'code' } },
    'NZD': { currency: { style: 'currency', currency: 'NZD', currencyDisplay: 'code' } },
    'OMR': { currency: { style: 'currency', currency: 'OMR', currencyDisplay: 'code' } },
    'PAB': { currency: { style: 'currency', currency: 'PAB', currencyDisplay: 'code' } },
    'PEN': { currency: { style: 'currency', currency: 'PEN', currencyDisplay: 'code' } },
    'PGK': { currency: { style: 'currency', currency: 'PGK', currencyDisplay: 'code' } },
    'PHP': { currency: { style: 'currency', currency: 'PHP', currencyDisplay: 'code' } },
    'PKR': { currency: { style: 'currency', currency: 'PKR', currencyDisplay: 'code' } },
    'PLN': { currency: { style: 'currency', currency: 'PLN', currencyDisplay: 'code' } },
    'PYG': { currency: { style: 'currency', currency: 'PYG', currencyDisplay: 'code' } },
    'QAR': { currency: { style: 'currency', currency: 'QAR', currencyDisplay: 'code' } },
    'RON': { currency: { style: 'currency', currency: 'RON', currencyDisplay: 'code' } },
    'RSD': { currency: { style: 'currency', currency: 'RSD', currencyDisplay: 'code' } },
    'RUB': { currency: { style: 'currency', currency: 'RUB', currencyDisplay: 'code' } },
    'RWF': { currency: { style: 'currency', currency: 'RWF', currencyDisplay: 'code' } },
    'SAR': { currency: { style: 'currency', currency: 'SAR', currencyDisplay: 'code' } },
    'SBD': { currency: { style: 'currency', currency: 'SBD', currencyDisplay: 'code' } },
    'SCR': { currency: { style: 'currency', currency: 'SCR', currencyDisplay: 'code' } },
    'SDG': { currency: { style: 'currency', currency: 'SDG', currencyDisplay: 'code' } },
    'SEK': { currency: { style: 'currency', currency: 'SEK', currencyDisplay: 'code' } },
    'SGD': { currency: { style: 'currency', currency: 'SGD', currencyDisplay: 'code' } },
    'SHP': { currency: { style: 'currency', currency: 'SHP', currencyDisplay: 'code' } },
    'SLL': { currency: { style: 'currency', currency: 'SLL', currencyDisplay: 'code' } },
    'SOS': { currency: { style: 'currency', currency: 'SOS', currencyDisplay: 'code' } },
    'SPL': { currency: { style: 'currency', currency: 'SPL', currencyDisplay: 'code' } },
    'SRD': { currency: { style: 'currency', currency: 'SRD', currencyDisplay: 'code' } },
    'SSP': { currency: { style: 'currency', currency: 'SSP', currencyDisplay: 'code' } },
    'STD': { currency: { style: 'currency', currency: 'STD', currencyDisplay: 'code' } },
    'SVC': { currency: { style: 'currency', currency: 'SVC', currencyDisplay: 'code' } },
    'SYP': { currency: { style: 'currency', currency: 'SYP', currencyDisplay: 'code' } },
    'SZL': { currency: { style: 'currency', currency: 'SZL', currencyDisplay: 'code' } },
    'THB': { currency: { style: 'currency', currency: 'THB', currencyDisplay: 'code' } },
    'TJS': { currency: { style: 'currency', currency: 'TJS', currencyDisplay: 'code' } },
    'TMT': { currency: { style: 'currency', currency: 'TMT', currencyDisplay: 'code' } },
    'TND': { currency: { style: 'currency', currency: 'TND', currencyDisplay: 'code' } },
    'TOP': { currency: { style: 'currency', currency: 'TOP', currencyDisplay: 'code' } },
    'TRY': { currency: { style: 'currency', currency: 'TRY', currencyDisplay: 'code' } },
    'TTD': { currency: { style: 'currency', currency: 'TTD', currencyDisplay: 'code' } },
    'TVD': { currency: { style: 'currency', currency: 'TVD', currencyDisplay: 'code' } },
    'TWD': { currency: { style: 'currency', currency: 'TWD', currencyDisplay: 'code' } },
    'TZS': { currency: { style: 'currency', currency: 'TZS', currencyDisplay: 'code' } },
    'UAH': { currency: { style: 'currency', currency: 'UAH', currencyDisplay: 'code' } },
    'UGX': { currency: { style: 'currency', currency: 'UGX', currencyDisplay: 'code' } },
    'USD': { currency: { style: 'currency', currency: 'USD', currencyDisplay: 'code' } },
    'UYU': { currency: { style: 'currency', currency: 'UYU', currencyDisplay: 'code' } },
    'UZS': { currency: { style: 'currency', currency: 'UZS', currencyDisplay: 'code' } },
    'VEF': { currency: { style: 'currency', currency: 'VEF', currencyDisplay: 'code' } },
    'VND': { currency: { style: 'currency', currency: 'VND', currencyDisplay: 'code' } },
    'VUV': { currency: { style: 'currency', currency: 'VUV', currencyDisplay: 'code' } },
    'WST': { currency: { style: 'currency', currency: 'WST', currencyDisplay: 'code' } },
    'XAF': { currency: { style: 'currency', currency: 'XAF', currencyDisplay: 'code' } },
    'XCD': { currency: { style: 'currency', currency: 'XCD', currencyDisplay: 'code' } },
    'XOF': { currency: { style: 'currency', currency: 'XOF', currencyDisplay: 'code' } },
    'XPF': { currency: { style: 'currency', currency: 'XPF', currencyDisplay: 'code' } },
    'YER': { currency: { style: 'currency', currency: 'YER', currencyDisplay: 'code' } },
    'ZAR': { currency: { style: 'currency', currency: 'ZAR', currencyDisplay: 'code' } },
    'ZMW': { currency: { style: 'currency', currency: 'ZMW', currencyDisplay: 'code' } },
    'ZWD': { currency: { style: 'currency', currency: 'ZWD', currencyDisplay: 'code' } },
    'ZWL': { currency: { style: 'currency', currency: 'ZWL', currencyDisplay: 'code' } },
}

// Fallback locale messages
var translations = {
    en: {
        Header: {
            Flights_Label: "Flights",
            Hotels_Label: "Hotels",
            Sightseeing_Label: "Sightseeing",
            WISInsurance_Label: "WIS",
            TPInsurance_Label: "Tune Proctect",
            Insurance_Label: "Insurance",
            Transfer_Label: "Transfer",
            UserGreeting_Label: "Hello!",
            ViewAccount_Label: "View Account",
            MyProfile_Label: "My Profile",
            Support_Label: "Support",
            Logout_Label: "Logout",
        },
        Sidebar: {
            MyProfile_Label: "My Profile",
            Flights_Label: "Book a Flight",
            Hotels_Label: "Book a Hotel",
            Sightseeing_Label: "Book a Sightseeing",
            Insurance_Label: "Book a Insurance",
            WISInsurance_Label: "WIS",
            TPInsurance_Label: "Tune Protect",
            Transfer_Label: "Book a Transfer",
            SearchFlights_Label: "Search Flight Bookings",
            SearchHotels_Label: "Search Hotel Bookings",
            SearchSightseeing_Label: "Search Sightseeing Bookings",
            SearchInsurance_Label: "Search Insurance Bookings",
            SearchWISInsurance_Label: "WIS",
            SearchTPInsurance_Label: "Tune Protect",
            SearchTransfer_Label: "Search Transfer Bookings",
            MyQuotations_Label: "My Quotations",
            Support_Label: "Support",
            Logout_Label: "Logout",
            MyBookings_Label: "My Bookings",
            SearchBookings_Label: "Search Bookings"
        },
        Footer: {
            Copyright_Label: "All Rights Reserved"
        },
        My_Profile: {
            Header_Label: "Your Personal Information",
            EditProfileBtn_Label: "EDIT PROFILE",
            NameDetails_Label: "Your name",
            ContactDetails_Label: "Your contact info",
            PasswordDetails_Label: "Change password",
            Title_Placeholder: "Title",
            FirstName_Placeholder: "First Name",
            LastName_Placeholder: "Last Name",
            Email_Placeholder: "Email",
            Phone_Placeholder: "Contact Number",
            OldPassword_Placeholder: "Old Password",
            NewPassword_Placeholder: "New Password",
            ConfirmPassword_Placeholder: "Confirm Password",
            UpdateBtn_Label: "Update",
            UpdatePwdBtn_Label: "Update Password",
            AlertifyHeader1_Label: "Error",
            AlertifyHeader2_Label: "Warning",
            AlertifyHeader3_Label: "Success",
            ProfileUpdated1_Message: "Profile updated.",
            ProfileUpdated2_Message: "Error in updating profile.",
            Validation_Message: "Please enter required fields.",
            PasswordUpdated1_Message: "Password updated.",
            PasswordUpdated2_Message: "Error in updating password."
        },
        Flight_Search: {
            Oneway_Label: "Oneway",
            Round_Trip_Label: "Round Trip",
            Multi_City_Label: "Multi City",
            From_Label: "From",
            From_Placeholder: "Enter Departure Airport or City",
            To_Label: "To",
            To_Placeholder: "Enter Destination Airport or City",
            Departure_Date_Label: "Departure Date",
            Departure_Date_Placeholder: "Departure Date",
            Return_Date_Label: "Return Date",
            Return_Date_Placeholder: "Return Date",
            Trip_Label: "Trip",
            MulticityFrom_Placeholder: "From",
            MulticityTo_Placeholder: "To",
            Add_up_to_Label_1: "Add up to",
            Add_up_to_Label_2: "trips",
            Travellers_Label: "Travellers",
            Adults_Label: "Adults",
            Children_Label: "Children",
            Infant_Label: "Infant",
            Economy_Label: "Economy",
            Business_Label: "Business",
            First_Class_Label: "First Class",
            Suppliers_Label: "Suppliers",
            Select_all_Label: "Select all",
            Airline_Label: "Airline",
            ShowDirect_flight_options_Label: "Show only direct flight options",
            Show_Recent_Searches_Label: "Show Recent Searches",
            Airline_Placeholder: "Preferred Airline",
            Search_Button_Label: "SEARCH",
            Alert_Validation_Label: 'Validation',
            Origin_Alert_Msg: 'Please fill origin !',
            Dest_Alert_Msg: 'Please fill destination !',
            Arrv_Alert_Msg: 'Please choose arrival date !',
            Dept_Alert_Msg: 'Please choose departure date !',
            Fill_Trip_Alert_Msg1: 'Please fill the Trip',
            Fill_Trip_Alert_Msg2: 'fields !',
            Dept_and_Arr_Alert_Msg: 'Departure and arrival airports should not be same !'
        },
        Hotel_Search: {
            City_Label: "City",
            City_Placeholder: "Enter city",
            Supplier_Label: "Supplier",
            Supplier_Placeholder: "All Suppliers",
            Residence_Label: "Country of Residence",
            Nationality_Label: "Nationality",
            Checkin_Label: "Check-in",
            Checkin_Placeholder: "Check-in",
            Checkout_Label: "Check-out",
            Checkout_Placeholder: "Check-out",
            Nights_Label: "Nights",
            SearchWithin_Label: "Include cities within",
            SearchWithin_Placeholder: "KM",
            RoomsDropdown_Label: "Rooms",
            RoomText_Label: "Room",
            Adults_Label: "Adults",
            Child_Label: "Child",
            ChildAge_Label: "age",
            SearchButton_Label: "Search",
            AlertifyHeader_Label: "Validation",
            NightsValidation1_Label: "Only numbers are allowed.",
            NightsValidation2_Label: "Only whole numbers are allowed.",
            CheckoutValidation_Label: "Please select check out date.",
            NightsValidation5_Label: "Please enter number of nights.",
            NightsValidation3_Label: "Minimum nights allowed in a single booking is 1.",
            NightsValidation4_Label: "Maximum nights allowed in a single booking is 90.",
            CityValidation_Label: "Please select a city.",
            SupplierValidation_Label: "Please select a supplier.",
            CheckinValidation_Label: "Please select check in date.",
            CountryValidation_Label: "Please select country of residence.",
            NationalityValidation_Label: "Please select nationality.",
            RoomsValidation1_Label: "Please select number of rooms.",
            RoomsValidation2_Label: "Maximum occupancy in a single booking is 9."
        },
        Sightseeing_Search: {},
        WISInsurance_Search: {},
        TPInsurance_Search: {
            Alert_Validation_Label: 'Validation',
            Departure_Country_Alert: 'Please Choose Departure Country',
            Departure_City_Alert: 'Please Choose Departure City',
            Arrival_Country_Alert: 'Please Choose Departure Country',
            Arrival_City_Alert: 'Please Choose Departure City',
            Start_Date_Alert: 'Please Choose Start Date',
            Return_Date_Alert: 'Please Choose Return Date',
            Departure_Country_Label: 'Departure Country',
            Departure_City_Label: 'Departure City',
            Arrival_Country_Label: 'Arrival Country',
            Arrival_City_Label: 'Arrival City',
            Start_Date_Label: 'Start Date',
            Return_Date_Label: 'Return Date',
            Adult_Label: 'Adults',
            Children_Label: 'Children',
            Infant_Label: 'Infants',
            One_Way_Label: 'Oneway',
            Round_Trip_Label: 'Round Trip',
            Search_Button_Label: 'Search'

        },
        Transfer_Search: {},
        Booking_Reports: {
            bookingFailed_Label: "Booking Failed",
            cancelled_Label: "Cancelled",
            confirmed_Label: "Confirmed",
            fareChanged_Label: "Fare Changed",
            requested_Label: "Requested",
            ticketFailed_Label: "Ticket Failed",
            ticketed_Label: "Ticketed",
            totalBookings_Label: "Total",
            reconfirmed_Label: "Reconfirmed",
            issued_Label: "Issued",
            ticketPending_Label: "Ticket Pending",
            TableActionRequired_Label: "ACTION REQUIRED",
            TableLastFiveBookings_Label: "LAST 5 BOOKINGS",
            ReferenceNumber_Label: "Reference Number",
            Trip_Label: "Trip",
            BookedOn_Label: "Booked On",
            Status_Label: "Status",
            Deadline_Label: "Deadline",
            City_Label: "City",
            Hotel_Label: "Hotel",
            Checkin_Label: "Check-in",
            Nights_Label: "Nights",
            Timelimit_Label: "Timelimit",
            Activity_Label: "Activity",
            Insurance_Label: "Insurance",
            SupplierReference_Label: "Supplier Reference",
            Vehicle_Label: "Vehicle",
            NoRecord_Label: "No record"
        },
        Hotel_Results: {
            CheckInDateBanner_Label: "Check-in",
            CheckOutDateBanner_Label: "Check-out",
            PaxBanner1_Label: "Pax",
            PaxBanner2_Label: "Pax in",
            RoomBanner_Label: "room(s)",
            ModifySearch_Label: "MODIFY SEARCH",
            QuotationSideBar_Label: "Quotations",
            QuotationSideBarBtn_Label: "Send Quotation",
            Filters_Label: "Filters",
            FilterByPrice_Label: "FILTER BY PRICE",
            FilterByHotelName_Label: "FILTER BY HOTEL NAME",
            FilterByHotelName_Placeholder: "Enter hotel name",
            FilterByLocation_Label: "FILTER BY LOCATION",
            FilterByLocation_Placeholder: "Enter hotel location",
            FilterBySupplier_Label: "FILTER BY SUPPLIER",
            FilterBySupplierUnit1_Label: "hotel",
            FilterBySupplierUnit2_Label: "hotels",
            FilterByStar_Label: "FILTER BY STAR",
            FilterByStarUnit1_Label: "hotel",
            FilterByStarUnit2_Label: "hotels",
            OnlyFilter_Label: "Only",
            ShowAllFilter_Label: "Show all",
            SortResults_Label: "SORT RESULT BY",
            SortByPrice_Label: "Price",
            SortByName_Label: "Name",
            SortByStar_Label: "Star",
            SortByRecommended_Label: "Recommended",
            HotelCount1_Label: "hotels found",
            HotelCount2_Label: "Showing",
            HotelCount3_Label: "out of",
            HotelCount4_Label: "hotels",
            Map_Label: "Map",
            RoomDetailsBtn_Label: "Room Details",
            Avalable_Label: "AVAILABLE",
            MoreDetails_Label: "More details",
            NoInformation_Label: "No information available.",
            InformationsHeader_Label: "Informations",
            DescriptionHeader_Label: "Description",
            PhotosHeader_Label: "Photos",
            LocationHeader_Label: "Location",
            HotelAmenities_Label: "Hotel Amenities",
            RoomAmenities_Label: "Room Amenities",
            ShowMore_Label: "Show more",
            ShowLess_Label: "Show less",
            Address_Label: "Address",
            Telephone_Label: "Telephone",
            Fax_Label: "Fax",
            Email_Label: "Email",
            Website_Label: "Website",
            POI_Label: "Point of Interests",
            NoImg_Label: "No images available.",
            RoomResultsHeader_Label: "Room",
            StatusHeader_Label: "Status",
            CostHeader_Label: "Cost",
            BookNow_Label: "Book now",
            AddQuote_Label: "Add quote",
            ShowMoreRooms_Label: "Show more rooms",
            ShowLessRooms_Label: "Show top 5 rooms",
            CancellationPolicy_Label: "CANCELLATION POLICY",
            CancellationTextA1_Label: "Cancellation between",
            CancellationTextA2_Label: "and",
            CancellationTextA3_Label: "will be charged",
            CancellationTextB1_Label: "Non refundable booking with cancellation charges",
            CancellationTextC1_Label: "Free cancellation from",
            CancellationTextC2_Label: "to",
            CancellationNotes_Label: "Cancellation policies are based on date and time of destination.",
            EssentialInformation_Label: "ESSENTIAL INFORMATIONS",
            RoomType_Label: "Room Type",
            TotalCost_Label: "Total Cost",
            Night1_Label: "Night",
            Night2_Label: "Nights",
            OptionsHeader_Label: "Option",
            DateToRange_Label: "to",
            HideOptionsBtn_Label: "Hide options",
            ShowOptionsBtn_Label: "Show more options",
            SelectedBtn1_Label: "Select",
            SelectedBtn2_Label: "Selected",
            MoreDetailsTooltip1_Label: "show more details",
            MoreDetailsTooltip2_Label: "hide more details",
            NoHotels_Label: "NO HOTELS FOUND!",
            NoHotelsDescription_Label: "Please try again with different travel dates or guest details.",
            NoHotelsError_Label: "Error",
            NoHotelsBackBtn_Label: "GO BACK TO SEARCH",
            AlertifyHeader1_Label: "Error",
            AlertifyHeader2_Label: "Please note",
            UnableToProceedError_Label: "Unable to proceed booking for this room selection.Please try different room.",
            CancellationPolicy_Error: "There is a problem in showing cancellation policy.Please try again later.",
            RoomSelectionError_Label: "There is a problem in room selection.Please try again later.",
            BookingNotesPopUp1_Label: "This booking is under cancellation period and you here by agree in the cancellation policies.",
            BookingNotesPopUp2_Label: "Please click OK if you agree the above terms or click cancel to select another hotel.",
            MapFilter_Placeholder: "Filter by hotel name",
            MapGetDirections_Label: "Get Directions",
            MapDirectionsTo_Label: "Directions To",
            MapDirectionsFom_Label: "Directions From",
            MapSelectBtn_Label: "Select",
            HotelNorms_Label: "HOTEL NORMS"

        },
        Hotel_Pax: {
            HotelInformationHeader_Label: "Hotel Information",
            RoomHeader_Label: "Room",
            CancellationPolicy_Label: "Cancellation Policy",
            CancellationTextA1_Label: "Cancellation between",
            CancellationTextA2_Label: "and",
            CancellationTextA3_Label: "will be charged",
            CancellationTextB1_Label: "Non refundable booking with cancellation charges",
            CancellationTextC1_Label: "Free cancellation from",
            CancellationTextC2_Label: "to",
            CancellationNotes_Label: "Cancellation policies are based on date and time of destination.",
            GuestNotes_Label: "Please enter the name of the main guest of the room",
            LeadPax_Label: "Lead Pax",
            Pax_Label: "Pax",
            AdultsTitle_Label: ["Mr", "Mrs", "Ms"],
            ChildTitle_Label: ["Master", "Miss"],
            FirstName_Placeholder: "First Name",
            LastName_Placeholder: "Last Name",
            ContactDetailsHeader_Label: "Contact details",
            RequiredField_Label: "Required field",
            FullName_Label: "Full name",
            Email_Label: "Email",
            Email_Placeholder: "Email",
            ContactNumber_Label: "Contact No",
            ContactNumber_Placeholder: "Contact Number",
            PaymentMethodHeader_Label: "Payment Method",
            Credit_Label: "Credit",
            PaymentGateway_Label: "Payment Gateway",
            TermsAndPolicy1_Label: "By completing this booking. I acknowledge and agree to the",
            TermsAndPolicy2_Label: "privacy policy",
            TermsAndPolicy3_Label: "and the",
            TermsAndPolicy4_Label: "terms & conditions",
            TermsAndPolicy5_Label: "that are applicable to this hotel booking.",
            ConfirmBookingBtn_Label: "Confirm booking",
            ProceedToPaymentBtn_Label: "Proceed to payment",
            HotelDetailsHeader_Label: "Hotel Details",
            Address_Label: "Address",
            RoomChargesHeader_Label: "Room charges",
            StayFor_Label: "Stay for",
            Nights1_Label: "night",
            Nights2_Label: "nights",
            Adult1_Label: "adult",
            Adult2_Label: "adults",
            Child1_Label: "child",
            Child2_Label: "children",
            Infant_Label: "infant",
            Age_Label: "Age",
            TotalAmount_Label: "Total Amount",
            NoInfoAvailable_Label: "No information available.",
            PriceChange_Label: "Final booking price has changed. Updated price Is",
            FinalBookingPrice_Label: "Final booking price",
            BackToSearchBtn_Label: "Back to Search",
            ContinueBtn_Label: "Continue",
            PleaseNote_Label: "Please Note",
            AlertifyHeader1_Label: "Error",
            AlertifyHeader2_Label: "Warning",
            RequiredFields_Label: "Please enter required fields.",
            RoomNotBookable_Label: "This room is not bookable.",
            ProblemInBooking_Label: "There is a problem in booking. Please try again later.",
            RoomDetailsNotFound_Label: "Room details not found. Please make another search.",
            Required_Message: "is required",
            AlphaSpaces_Message: "Only alphabets and spaces allowed",
            Email_Message: "Please enter a valid email",
            Numeric_Message: "Please enter a valid phone number",
            MaximumLength_Message: "Please enter less than 15 characters",
            Regex_Message: "Please enter a valid phone number",
            TermsAndPolicy_Message: "Privacy policy and terms & conditions",
            HotelNormsHeader_Label: "Hotel Norms"

        },
        Hotel_Confirmation: {
            BookingInformation_Label: "BOOKING INFORMATION",
            BookedOn_Label: "BOOKED ON",
            BookiingStatusHeader_Label: "YOUR BOOKING AT HYATT REGENCY DUBAI HAS BEEN",
            CancelBtn_Label: "Cancel",
            DownloadVoucherBtn_Label: "Download Voucher",
            EmailVoucherBtn_Label: "Email Voucher",
            EditVoucherBtn_Label: "Edit Voucher",
            RequestCancellationBtn_Label: "Request Cancellation",
            AccomodationDetails_Label: "ACCOMODATION DETAILS",
            OurRefNo_Label: "Our Ref No",
            BookingDate_Label: "Booking Date",
            Checkin_Label: "Check-in",
            CancellationDeadline_Label: "Cancellation Deadline",
            Status_Label: "Status",
            Nights_Label: "Nights",
            Checkout_Label: "Check-out",
            TotalAmount_Label: "Total Amount",
            CouponDiscount_Label: "Coupon Discount",
            AmountPaid_Label: "Amount Paid",
            PaymentMode_Label: "Payment Mode",
            PaymentStatus_Label: "Payment Status",
            Accomodation_Label: "Accomodation",
            Address_Label: "Address",
            SupplierName_Label: "Supplier Name",
            BookingID_Label: "Booking ID",
            Services_Label: "SERVICES",
            RoomsHeader_Label: "Rooms",
            AdultsHeader_Label: "Adults",
            ChildrenHeader_Label: "Children",
            PaxNameHeader_Label: "Pax Name",
            Acquaintance_Label: "Acquaintance",
            CancellationPolicy_Label: "Cancellation Policy",
            PriceBreakup_Label: "Price Breakup",
            RoomCostHeader_Label: "Room Cost",
            TotalCost_Label: "Total Cost",
            BookingHistory_Label: "Booking History",
            SellAmount_Label: "Sell Amount",
            SellCurrency_Label: "Sell Currency",
            NetAmount_Label: "Net Amount",
            NetCurrency_Label: "Net Currency",
            BaseAmount_Label: "Base Amount",
            BaseCurrency_Label: "Base Currency",
            RateOfExchange_Label: "Rate Of Exchange",
            TotalMarkup_Label: "Total Markup",
            Amount_Label: "Amount",
            CancellationTextA1_Label: "Cancellation between",
            CancellationTextA2_Label: "and",
            CancellationTextA3_Label: "will be charged",
            CancellationTextB1_Label: "Non refundable booking with cancellation charges",
            CancellationTextC1_Label: "Free cancellation from",
            CancellationTextC2_Label: "to",
            CancellationNotes_Label: "Cancellation policies are based on date and time of destination.",
            CloseBtn_Label: "Close",
            SendBtn_Label: "Send",
            EmailTo_Label: "Email to",
            CotsNotes_Label: "Please note: Cot(s) will be requested at the hotel, however cots are not guaranteed and are subject to availability at check-in. Also, maximum age for cot is 2 years old.",
            AlertifyHeader1_Label: "Error",
            AlertifyHeader2_Label: "Please note",
            CancelBookingPopUp_Label: "Are you sure to cancel this booking?",
            BookingError_Label: "There is a problem in booking. Please try again later.",
            CancelBookingError_Label: "There is a problem in cancel booking. Please try again later.",
            CancelBookingSuccess_Label: "Booking cancelled successfully.",
            BookingStatusHeader1_Label: "YOUR BOOKING AT",
            BookingStatusHeader2_Label: "HAS BEEN",
            NoEmail_Label: "Please enter a valid email address.",
            EmailSent_Label: "Email sent successfully.",
            EmailNotSent_Label: "Error in sending email.",
            CancellationPending_Label: "We have initiated the cancellation process. You will be notified once it is cancelled."

        },
        Hotel_Search_Booking: {
            Header_Label: "SEARCH BOOKING",
            CityName_Label: "City Name",
            HotelName_Label: "Hotel Name",
            BookingDateFrom_Label: "Booking Date(From)",
            BookingDateTo_Label: "Booking Date(To)",
            CheckinDateFrom_Label: "Check-in(From)",
            CheckinDateTo_Label: " Check-in(To)",
            CheckoutDateFrom_Label: "Check-out(From)",
            CheckoutDateTo_Label: "Check-out(To)",
            BookingRefNo_Label: "Booking Ref.No.",
            SupplierRefNo_Label: "Supplier Ref.No.",
            Status_Label: "Status",
            SearchBtn_Label: "Search",
            ReferenceID_Label: "Reference ID",
            City_Label: "City",
            Hotel_Label: "Hotel",
            Checkin_Label: "Check-in",
            Nights_Label: "Nights",
            Timelimit_Label: "Timelimit",
            Booking_Label: "Booking",
            ChildAgencyNodes_Label: "Child Agency Nodes"
        },
        Flight_Search_Booking: {
            DepartureCity_Label: "Departure City",
            ArrivalCity_Label: "Arrival City",
            DepartureDateFrom_Label: "Departure Date(From)",
            DepartureDateTo_Label: "Departure Date(To)",
            ArrivalDateFrom_Label: "Arrival Date(From)",
            ArrivalDateTo_Label: "Arrival Date(To)",
            BookingDateFrom_Label: "Booking Date(From)",
            BookingDateTo_Label: "Booking Date(To)",
            JourneyType_Label: "Journey Type",
            Stops_Label: "Stops",
            Status_Label: "Status",
            OperatingAirline_Label: "Operating Airline",
            Supplier_Label: "Supplier",
            RefNumber_Label: "Ref.Number",
            SupplierRef_Label: "Supplier Ref.",
            Trip_Label: "Trip",
            BookedOn_Label: "Booked On",
            Pax_Label: "Pax",
            Deadline_Label: "Deadline",
            ChildAgencyNodes_Label: "Child Agency Nodes"
        },
        Common_Labels: {
            SearchBtn_Label: "SEARCH",
            Header_Label: "SEARCH BOOKING",
            Flight_Label: "Flight",
            Hotel_Label: "Hotel",
            Sightseeing_Label: "Hotel",
            Insurance_Label: "Insurance",
            WIS_Label: "Hotel",
            TuneProtect_Label: "Hotel",
            Transfer_Label: "Hotel",
            NoRecord_Label: "No record available!"
        },
        Notice: {
            Notice_Content: "",
            Notice_Label: "Notice"
        },
        News: {
            NewsAlert_Content: [],
            NewsAlert_Label: "News Alert"
        },
        Hotel_Booking_Voucher: {
            Terms_and_Conditions: ""
        }
    }
}

//Datatables translation
var dataTableLangauge = {
    ar: {
        "sProcessing": "جارٍ التحميل...",
        "sLengthMenu": "أظهر _MENU_ مدخلات",
        "sZeroRecords": "لم يعثر على أية سجلات",
        "sInfo": "إظهار _START_ إلى _END_ من أصل _TOTAL_ مدخل",
        "sInfoEmpty": "يعرض 0 إلى 0 من أصل 0 سجل",
        "sInfoFiltered": "(منتقاة من مجموع _MAX_ مُدخل)",
        "sInfoPostFix": "",
        "sSearch": "ابحث:",
        "sUrl": "",
        "oPaginate": {
            "sFirst": "الأول",
            "sPrevious": "السابق",
            "sNext": "التالي",
            "sLast": "الأخير"
        }
    },
    en: {
        "sEmptyTable": "No data available in table",
        "sInfo": "Showing _START_ to _END_ of _TOTAL_ entries",
        "sInfoEmpty": "Showing 0 to 0 of 0 entries",
        "sInfoFiltered": "(filtered from _MAX_ total entries)",
        "sInfoPostFix": "",
        "sInfoThousands": ",",
        "sLengthMenu": "Show _MENU_ entries",
        "sLoadingRecords": "Loading...",
        "sProcessing": "Processing...",
        "sSearch": "Search:",
        "sZeroRecords": "No matching records found",
        "oPaginate": {
            "sFirst": "First",
            "sLast": "Last",
            "sNext": "Next",
            "sPrevious": "Previous"
        },
        "oAria": {
            "sSortAscending": ": activate to sort column ascending",
            "sSortDescending": ": activate to sort column descending"
        }
    }
}

var i18n = new VueI18n({
    numberFormats,
    locale: 'en', // set locale
    fallbackLocale: 'en',
    messages: translations, // set locale messages
    silentFallbackWarn: true,
    silentTranslationWarn: true
})

function pluck(key, contentArray) {
    var tempArray = [];
    contentArray.map(function(item) {
        if (item[key] != undefined) {
            tempArray.push(item[key]);
        }
    });
    return tempArray;
}

 function getAllMapData(contentArry){
    var tempDataObject = {};
    if(contentArry!=undefined){
        contentArry.map(function (item) {
            let allKeys=Object.keys(item)
            for(let j=0;j<allKeys.length;j++){
                let key=allKeys[j];
                let value= item[key];
                
                if(key!='name' && key!='type'){
                    if(value==undefined||value==null){
                        value="";
                    }
                    tempDataObject[key]=value;
                }
            }
        });
    }
    return tempDataObject;
}

function pluckcom(key, contentArray) {
    var tempArray = [];
    contentArray.map(function(item) {
        if (item[key] != undefined) {
            tempArray = item[key];
        }
    });
    return tempArray;
}

function getCabinName(cabinCode) {
    var cabinClass = 'Economy';
    if (cabinCode == "F") {
        cabinClass = "First Class";
    } else if (cabinCode == "C") {
        cabinClass = "Business";
    } else {
        try { cabinClass = getCabinClassObject(cabinCode).BasicClass; } catch (err) {}
    }
    return cabinClass;
}

function isNullorUndefined(value) {
    var status = false;
    if (value == '' || value == null || value == undefined || value == "undefined" || value == [] || value == NaN) { status = true; }
    return status;
}

function convertCurrency(amount, currentObject) {
    return i18n.n(amount / currentObject.commonStore.currencyMultiplier, 'currency', currentObject.commonStore.selectedCurrency);
}

function bindCreditLimit() {
    vueCommonStore.state.showCreditLimit = false;
    $("#divCredLim .fa-refresh").addClass("fa-spin");
    var hubUrl = HubServiceUrls.hubConnection.baseUrl + HubServiceUrls.hubConnection.ipAddress;
    var serviceUrl = HubServiceUrls.hubConnection.hubServices.creditLimit;

    var config = {
        axiosConfig: {
            method: "GET",
            url: hubUrl + serviceUrl
        },
        successCallback: function(response) {
            if (!stringIsNullorEmpty(response.data)) {
                var result = response.data;
                // console.log('CreditLimit: ', result);
                if (result.availableCreditLimit !== 'infinity') {
                    var sign = Number(result.availableCreditLimit) < 0 ? '-' : '';
                    var crediValue = i18n.n(result.availableCreditLimit / vueCommonStore.state.currencyMultiplier, 'currency', vueCommonStore.state.selectedCurrency).match(/\d+(\.\d+)?/g) || [];
                    if (crediValue.length > 0) {
                        vueCommonStore.state.creditAvailableLimit = sign + crediValue.join('');
                        vueCommonStore.state.showCreditLimit = true;
                    } else {
                        vueCommonStore.state.showCreditLimit = false;
                    }
                }
            }
            $("#divCredLim .fa-refresh").removeClass("fa-spin");
        },
        errorCallback: function(error) {
            // vueCommonStore.state.showCreditLimit = true;
            // vueCommonStore.state.creditAvailableLimit = "AED 1000000.00".match(/\d+(\.\d+)?/g).join('');
            // $("#divCredLim .fa-refresh").removeClass("fa-spin");
        },
        showAlert: false
    };

    mainAxiosRequest(config);

}

function pageWiseBind() {

    //powered by
    var host = window.location.host;
    switch (host) {
        case "127.0.0.1:5500":
            $(".ar_cp").hide();
            break;
        case "stag-nct-b2b-v3.oneviewitsolutions.com":
            $(".ar_cp").hide();
            break;
        case "stag-okt-b2b-v3.oneviewitsolutions.com":
            $(".ar_cp").hide();
            break;
        case "agents.choudharytravel.com":
            $(".ar_cp").hide();
            break;
        default:

    }
}