Vue.component("my-profile", {
  data: function () {
    return {
      commonStore: vueCommonStore.state,
      fullName: "",
      titleId: "",
      title: "",
      firstName: "",
      lastName: "",
      emailId: "",
      contactNumber: "",
      isUpdateProfile: false,
      isUpdateProfileSubmitted: false,
      isUpdatePwdSubmitted: false,
      currentPwd: "",
      newPwd: "",
      confirmPwd: ""

    };
  },
  created: function () { },
  mounted: function () {
    var agencyNode = window.localStorage.getItem("agencyNode");
    if (agencyNode) {
      agencyNode = JSON.parse(atob(agencyNode));

      this.titleId = agencyNode.title.id;
      this.title = agencyNode.title.name;
      this.firstName = agencyNode.firstName;
      this.lastName = agencyNode.lastName;
      this.emailId = agencyNode.emailId;
      this.contactNumber = agencyNode.contactNumber;
      this.fullName = this.title + ' ' + this.firstName + ' ' + this.lastName;
      this.veeValidateLocaleUpdate();

    }
  },
  methods: {
    editProfile: function () {
      this.isUpdateProfile = true;
    },
    veeValidateLocaleUpdate: function () {
      
      if (this.commonStore.selectedLanguage && this.commonStore.selectedLanguage.code == 'en') {
        this.$validator.localize('en', {
          messages: {
            required: (field) => '* ' + field + ' is required',
            alpha_spaces: () => '* Only alphabets and spaces allowed',
            email: () => '* Please enter a valid email',
            numeric: () => '* Please enter a valid phone number',
            max: () => '* Please enter less than 15 characters',
            regex: () => '* Please enter a valid phone number',
          }
        })
      } else if (this.commonStore.selectedLanguage && this.commonStore.selectedLanguage.code == 'ar') {
        this.$validator.localize('ar', {
          messages: {
            required: (field) => '* ' + field + ' مطلوب',
            alpha_spaces: () => '* الحروف الهجائية والمساحات المسموح بها فقط',
            email: () => '* يرجى إدخال البريد الإلكتروني الصحيح',
            numeric: () => '* يرجى إدخال رقم هاتف صالح',
            max: () => '* الرجاء إدخال أقل من 15 حرفًا',
            regex: () => '* يرجى إدخال رقم هاتف صالح',
          }
        })
      }
      this.$validator.reset();
    },
    updateProfile: function () {

      var vm = this;
      
      vm.isUpdateProfileSubmitted = true;
      // var a = this.$validator.validate('firstName', this.firstName);
      var areValid = false;
      Promise.all([
        this.$validator.validate('title'),
        this.$validator.validate('firstName'),
        this.$validator.validate('lastName'),
        this.$validator.validate('phone')
      ]).then(function (response) {
        areValid = response.every(function (e) {
          return e;
        })
        if (areValid) {

          var editProfileRequest = {
            title: vm.titleId,
            firstName: vm.firstName,
            lastName: vm.lastName,
            contactNumber: {
              number: vm.contactNumber,
              countryCode: "AE"
            }
          };

          var hubUrls = HubServiceUrls;
          var hubUrl = hubUrls.hubConnection.baseUrl;
          var port = hubUrls.hubConnection.ipAddress;
          var updateProfile = hubUrls.hubConnection.hubServices.updateProfile;

          axios({
            method: "put",
            url: hubUrl + port + updateProfile,
            data: editProfileRequest,
            headers: {
              "Content-Type": "application/json",
              "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
            }
          }).then(function () {

            alertify.alert(i18n.t('My_Profile.AlertifyHeader3_Label'), i18n.t('My_Profile.ProfileUpdated1_Message'));
            vm.isUpdateProfileSubmitted = false;

          }).catch(function () {
            vm.isUpdateProfileSubmitted = false;
            alertify.alert(i18n.t('My_Profile.AlertifyHeader1_Label'), i18n.t('My_Profile.ProfileUpdated2_Message'));
          })
        } else {
          vm.isUpdateProfileSubmitted = false;
          alertify.alert(i18n.t('My_Profile.AlertifyHeader2_Label'), i18n.t('My_Profile.Validation_Message'));
        }
      });

    },
    updatePassword: function () {

      var vm = this;
      
      vm.isUpdatePwdSubmitted = true;
      // var a = this.$validator.validate('firstName', this.firstName);
      var areValid = false;
      Promise.all([
        this.$validator.validate('currentPwd'),
        this.$validator.validate('newPwd'),
        this.$validator.validate('confirmPwd')
      ]).then(function (response) {
        areValid = response.every(function (e) {
          return e;
        })
        if (areValid) {

          var updatePasswordRequest = {
            oldPassword: vm.currentPwd,
            newPassword: vm.newPwd,
            confirmPassword: vm.confirmPwd
          };

          var hubUrls = HubServiceUrls;
          var hubUrl = hubUrls.hubConnection.baseUrl;
          var port = hubUrls.hubConnection.ipAddress;
          var resetPassword = hubUrls.hubConnection.hubServices.changePassword;

          axios({
            method: "put",
            url: hubUrl + port + resetPassword,
            data: updatePasswordRequest,
            headers: {
              "Content-Type": "application/json",
              "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
            }
          }).then(function () {

            alertify.alert(i18n.t('My_Profile.AlertifyHeader3_Label'), i18n.t('My_Profile.PasswordUpdated1_Message'));
            vm.isUpdatePwdSubmitted = false;

          }).catch(function () {
            vm.isUpdatePwdSubmitted = false;
            alertify.alert(i18n.t('My_Profile.AlertifyHeader1_Label'), i18n.t('My_Profile.PasswordUpdated2_Message'));
          })
        } else {
          vm.isUpdatePwdSubmitted = false;
          alertify.alert(i18n.t('My_Profile.AlertifyHeader2_Label'), i18n.t('My_Profile.Validation_Message'));
        }
      });

    }
  },
  watch: {
    "commonStore.selectedLanguage": {
      handler: function () {
        this.veeValidateLocaleUpdate();
      },
      deep: true
    }
  }
});