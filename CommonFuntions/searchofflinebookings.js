var dateFormat = generalInformation.systemSettings.systemDateFormat;
var statusCodeList = [];
var searchParams = {};

// Lazy Loading Prime Vue Offline bookings
Vue.component("flight-search-bookings", {
  components: {
    'p-dropdown': dropdown,
    'p-paginator': paginator,
    'p-datatable': datatable,
    'p-column': column,
    'p-button': button,
    'p-calendar': calendar,
    'p-multiselect': multiselect,
  },
  data: function () {
    return {
      commonStore: vueCommonStore.state,
      loading: false,
      customers: null,
      firstIndex: 1,
      hits: null,
      searchKeywords: { globalSearch: null, uniqueId: null, requestType: null, airline: null, date: null, pnr: null, passengerName: null, route: null, amount: null, status: null },
      rStatus: [{ name: "Applied", code: "Applied" }, { name: "In Progress", code: "In Progress" }, { name: "Completed", code: "Completed" }, { name: "Rejected", code: "Rejected" }, { name: "Cancelled", code: "Cancelled" }],
      sortList: [],
    }
  },
  mounted: function () {
    this.loading = true;
    window.sessionStorage.setItem("offlineReqTab", "flights");
    this.getAirBookingsCriteria(0);
  },
  methods: {
    searchTable: function () {
      var vm = this;
      vm.loading = true;
      vm.getAirBookingsCriteria(0);
    },
    timeFormat: function (time) {
      return moment(time).format('DD MMM YYYY');
    },
    sortBy: function (event) {
      var sortField = event.sortField;
      var sortOrder = event.sortOrder;
      sortOrder = sortOrder == 1 ? "ASC" : sortOrder == -1 ? "DESC" : "ASC";
      var field = null;
      this.sortList = [];
      if (sortField == "id") {
        this.sortList.push({
          "field": "id",
          "order": sortOrder
        });
      } else if (sortField == "Date") {
        this.sortList.push({
          "field": "date3",
          "order": sortOrder
        });
      } else if (sortField == "requestType") {
        this.sortList.push({
          "field": "type",
          "order": sortOrder
        });
      } else if (sortField == "Airline") {
        this.sortList.push({
          "field": "text17",
          "order": sortOrder
        });
      } else if (sortField == "ticket") {
        this.sortList.push({
          "field": "keyword2",
          "order": sortOrder
        });
      } else if (sortField == "PNR") {
        this.sortList.push({
          "field": "keyword1",
          "order": sortOrder
        });
      } else if (sortField == "route") {
        this.sortList.push({
          "field": "text3",
          "order": sortOrder
        });
      } else if (sortField == "Pax Name") {
        this.sortList.push({
          "field": "text2",
          "order": sortOrder
        });
      } else if (sortField == "Total") {
        this.sortList.push({
          "field": "amount8",
          "order": sortOrder
        });
      } else if (sortField == "reqStatus") {
        this.sortList.push({
          "field": "keyword8",
          "order": sortOrder
        });
      } else {
        this.sortList = null;
      }
      // sortOrder == 1 ? "ASC" : sortOrder ? sortOrder == -1 ? "DESC" || "ASC"

      this.getAirBookingsCriteria(event.first);
    },
    onPage: function (event) {
      var vm = this;
      vm.loading = true;
      vm.getAirBookingsCriteria(event.first);
    },
    getBookingInfo: function (item) {
      window.sessionStorage.setItem("offlineBookingId", item.id);

      if (item.requestTypeStatus.requestTypeId == 2) {
        window.location.href = "/offlineTicketing.html";
      } else {
        window.location.href = "/offlineBooking.html";
      }
    },
    getAirBookingsCriteria: function (from) {
      var vm = this;
      var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrlOffline;
      var port = vm.commonStore.hubUrls.hubConnection.ipAddress;
      var offlineRequest = vm.commonStore.hubUrls.hubConnection.hubServices.flights.offlineRequest;
      var searchList = [];
      var globalSearch = [];

      switch (!isNullorUndefind(this.searchKeywords)) {
        case (!isNullorUndefind(this.searchKeywords.globalSearch)):
          if (this.searchKeywords.globalSearch) {
            globalSearch = { "searchTerm": this.searchKeywords.globalSearch };
          }
          else {
            globalSearch = null
          }
          break;
        default:
          globalSearch = null;
          break;
      }

      switch (!isNullorUndefind(this.searchKeywords)) {
        case (!isNullorUndefind(this.searchKeywords.uniqueId)):
          if (this.searchKeywords.uniqueId) {
            searchList.push({
              "field": "id",
              "term": this.searchKeywords.uniqueId
            });
          }
        case (!isNullorUndefind(this.searchKeywords.requestType)):
          if (this.searchKeywords.requestType) {
            searchList.push({
              "field": "type",
              "term": this.searchKeywords.requestType
            });
          }
        case (!isNullorUndefind(this.searchKeywords.airline)):
          if (this.searchKeywords.airline) {
            searchList.push({
              "field": "text17",
              "term": this.searchKeywords.airline
            });
          }
        case (!isNullorUndefind(this.searchKeywords.pnr)):
          if (this.searchKeywords.pnr) {
            searchList.push({
              "field": "keyword1",
              "term": this.searchKeywords.pnr
            });
          }
        case (!isNullorUndefind(this.searchKeywords.passengerName)):
          if (this.searchKeywords.passengerName) {
            searchList.push({
              "field": "text2",
              "term": this.searchKeywords.passengerName
            });
          }
        case (!isNullorUndefind(this.searchKeywords.route)):
          if (this.searchKeywords.route) {
            searchList.push({
              "field": "text3",
              "term": this.searchKeywords.route
            });
          }
        case (!isNullorUndefind(this.searchKeywords.date)):
          if (this.searchKeywords.date) {
            searchList.push({
              "field": "date3",
              "term": moment(this.searchKeywords.date).format('YYYY-MM-DD')
            });
          }
        case (!isNullorUndefind(this.searchKeywords.amount)):
          if (this.searchKeywords.amount) {
            searchList.push({
              "field": "amount8",
              "term": this.searchKeywords.amount
            });
          }
        case (!isNullorUndefind(this.searchKeywords.status)):
          if (this.searchKeywords.status && this.searchKeywords.status.length) {
            this.searchKeywords.status.forEach(element => {
              searchList.push({
                "field": "keyword8",
                "term": element
              });
            });
          }
          break;
        default:
          searchList = null
          break;
      }


      var PostData =
      {
        "from": from,
        "size": 15,
        "search": globalSearch
        ,
        "filterList": isNullorUndefind(searchList) ? null : searchList.length ? searchList : null,
        "sortList": this.sortList || null
      };


      var headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
      };
      axios({
        method: "post",
        url: hubUrl + port + offlineRequest,
        data: PostData,
        headers: headers
      }).then(function (response) {
        if (response.data.data.data == undefined || response.data.data.data == null || !response.data.data.data || response.data.data.data.length == 0) {
          vm.loading = false;
          vm.hits = null;
          vm.customers = [];
        } else {
          vm.hits = response.data.data.hits;
          vm.customers = response.data.data.data || [];
          vm.loading = false;
          console.log("OFFLINE LIST RESPONSE RECEIVED: ", response);
        }
      }).catch(function (err) {
        console.log(err);
        vm.loading = false;
        vm.hits = null;
        vm.customers = [];
      });

    }
  }
});

// Lazy Loading Prime Vue Offline refund
Vue.component("flight-search-refund", {
  components: {
    'p-dropdown': dropdown,
    'p-calendar': calendar,
    'p-paginator': paginator,
    'p-datatable': datatable,
    'p-column': column,
    'p-button': button,
    'p-multiselect': multiselect,
  },
  data: function () {
    return {
      commonStore: vueCommonStore.state,
      loading: false,
      customers: null,
      firstIndex: 1,
      hits: null,
      rStatus: [{ name: "Applied", code: "Applied" }, { name: "In Progress", code: "In Progress" }, { name: "Posted", code: "Posted" }, { name: "Rejected", code: "Rejected" }, { name: "Voided", code: "Voided" }, { name: "Processing", code: "Processing" }, { name: "Cancelled", code: "Cancelled" }],
      searchKeywords: { globalSearch: null, uniqueId: null, slNo: null, requestType: null, airline: null, ticketNo: null, pnr: null, passengerName: null, traveldate: null, route: null, amount: null, status: null },
      sortList: [],
    }
  },
  mounted: function () {
    this.loading = true;
    var vm = this;
    window.sessionStorage.setItem("offlineReqTab", "refund");
    vm.getRequest(0);
  },
  methods: {
    timeFormat: function (time) {
      if (time) {
        return moment(time).format('DD MMM YYYY');
      }
      return time;
    },
    searchTable: function () {
      this.loading = true;
      this.getRequest(0);
    },
    getBookingInfo: function (item) {
      window.sessionStorage.removeItem('offlineRequestData');
      window.sessionStorage.setItem("offlineRefundId", item.id);
      window.sessionStorage.setItem("paxId", item.requestCancel.passengers[0].pax.id);
      window.location.href = "/offlinerefund.html";
    },
    sortBy: function (event) {
      var sortField = event.sortField;
      var sortOrder = event.sortOrder;
      sortOrder = sortOrder == 1 ? "ASC" : sortOrder == -1 ? "DESC" : "ASC";
      var field = null;
      this.sortList = [];
      if (sortField == "id") {
        this.sortList.push({
          "field": "id",
          "order": sortOrder
        });
        
      } else if (sortField == "Date") {
        this.sortList.push({
          "field": "date3",
          "order": sortOrder
        });
      } else if (sortField == "traveldate") {
        this.sortList.push({
          "field": "date2",
          "order": sortOrder
        });
      } else if (sortField == "slNo") {
        this.sortList.push({
          "field": "number1",
          "order": sortOrder
        });
      } else if (sortField == "requestType") {
        this.sortList.push({
          "field": "type",
          "order": sortOrder
        });
      } else if (sortField == "airLine") {
        this.sortList.push({
          "field": "text17",
          "order": sortOrder
        });
      } else if (sortField == "ticket") {
        this.sortList.push({
          "field": "keyword2",
          "order": sortOrder
        });
      } else if (sortField == "pnr") {
        this.sortList.push({
          "field": "keyword1",
          "order": sortOrder
        });
      } else if (sortField == "name") {
        this.sortList.push({
          "field": "text2",
          "order": sortOrder
        });
      } else if (sortField == "route") {
        this.sortList.push({
          "field": "text3",
          "order": sortOrder
        });
      } else if (sortField == "amount") {
        this.sortList.push({
          "field": "amount3",
          "order": sortOrder
        });
      } else if (sortField == "reqStatus") {
        this.sortList.push({
          "field": "keyword8",
          "order": sortOrder
        });
      } else {
        this.sortList = null;
      }
      // sortOrder == 1 ? "ASC" : sortOrder ? sortOrder == -1 ? "DESC" || "ASC"

      this.getRequest(event.first);
    },
    mouseOver: function (item) {
      if (item.requestHST[0].comments) {
        return item.requestHST[0].comments;
      }
    },
    onPage: function (event) {
      var vm = this;
      vm.loading = true;
      vm.getRequest(event.first);
    },
    getRequest: function (from) {
      var vm = this;
      var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrlOffline;
      var port = vm.commonStore.hubUrls.hubConnection.ipAddress;
      var offlineRequest = vm.commonStore.hubUrls.hubConnection.hubServices.flights.offlineRefund;
      var searchList = [];
      var globalSearch = [];

      switch (!isNullorUndefind(this.searchKeywords)) {
        case (!isNullorUndefind(this.searchKeywords.globalSearch)):
          if (this.searchKeywords.globalSearch) {
            globalSearch = { "searchTerm": this.searchKeywords.globalSearch };
          }
          else {
            globalSearch = null
          }
          break;
        default:
          globalSearch = null;
          break;
      }

      switch (!isNullorUndefind(this.searchKeywords)) {
        case (!isNullorUndefind(this.searchKeywords.uniqueId)):
          if (this.searchKeywords.uniqueId) {
            searchList.push({
              "field": "id",
              "term": this.searchKeywords.uniqueId
            });
          }
        case (!isNullorUndefind(this.searchKeywords.slNo)):
          if (this.searchKeywords.slNo) {
            searchList.push({
              "field": "number1",
              "term": this.searchKeywords.slNo
            });
          }
        case (!isNullorUndefind(this.searchKeywords.requestType)):
          if (this.searchKeywords.requestType) {
            searchList.push({
              "field": "type",
              "term": this.searchKeywords.requestType
            });
          }
        case (!isNullorUndefind(this.searchKeywords.airline)):
          if (this.searchKeywords.airline) {
            searchList.push({
              "field": "text17",
              "term": this.searchKeywords.airline
            });
          }
        case (!isNullorUndefind(this.searchKeywords.ticketNo)):
          if (this.searchKeywords.ticketNo) {
            searchList.push({
              "field": "keyword2",
              "term": this.searchKeywords.ticketNo
            });
          }
        case (!isNullorUndefind(this.searchKeywords.pnr)):
          if (this.searchKeywords.pnr) {
            searchList.push({
              "field": "keyword1",
              "term": this.searchKeywords.pnr
            });
          }
        case (!isNullorUndefind(this.searchKeywords.passengerName)):
          if (this.searchKeywords.passengerName) {
            searchList.push({
              "field": "text2",
              "term": this.searchKeywords.passengerName
            });
          }
        case (!isNullorUndefind(this.searchKeywords.route)):
          if (this.searchKeywords.route) {
            searchList.push({
              "field": "text3",
              "term": this.searchKeywords.route
            });
          }
        case (!isNullorUndefind(this.searchKeywords.amount)):
          if (this.searchKeywords.amount) {
            searchList.push({
              "field": "amount3",
              "term": this.searchKeywords.amount
            });
          }
        case (!isNullorUndefind(this.searchKeywords.status)):
          if (this.searchKeywords.status && this.searchKeywords.status.length) {
            this.searchKeywords.status.forEach(element => {
              searchList.push({
                "field": "keyword8",
                "term": element
              });
            });
          }
          break;
        default:
          searchList = null
          break;
      }


      var PostData =
      {
        "from": from,
        "size": 15,
        "search": globalSearch
        ,
        "filterList": isNullorUndefind(searchList) ? null : searchList.length ? searchList : null,
        "sortList": this.sortList || null
      };

      var headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
      };
      axios({
        method: "post",
        url: hubUrl + port + offlineRequest,
        data: PostData,
        headers: headers
      }).then(function (response) {
        if (response.data.data.data == undefined || response.data.data.data == null || !response.data.data.data || response.data.data.data.length == 0) {
          vm.loading = false;
          vm.hits = null;
          vm.customers = [];
        } else {
          vm.hits = response.data.data.hits;
          vm.customers = response.data.data.data || [];
          vm.loading = false;
          console.log(response);
        }
      }).catch(function (err) {
        console.log(err);
        vm.loading = false;
        vm.hits = null;
        vm.customers = [];
      });

    },
  }
});

// Lazy Loading Prime Vue Offline Visa
Vue.component("flight-search-visa", {
  components: {
    'p-dropdown': dropdown,
    'p-paginator': paginator,
    'p-datatable': datatable,
    'p-column': column,
    'p-button': button,
    'p-multiselect': multiselect,
  },
  data: function () {
    return {
      commonStore: vueCommonStore.state,
      loading: false,
      customers: null,
      firstIndex: 1,
      hits: null,
      rStatus: [{ name: "Applied", code: "Applied" }, { name: "In Progress", code: "In Progress" }, { name: "Docs Required", code: "Docs Required" }, { name: "On Hold", code: "On Hold" }, { name: "Processing", code: "Processing" }, { name: "Cancelled", code: "Cancelled" }, { name: "Approved", code: "Approved" }, { name: "Rejected", code: "Rejected" }, { name: "Visa Refused", code: "Visa Refused" }, { name: "Docs Updated", code: "Docs Updated" }],
      searchKeywords: { globalSearch: null, uniqueId: null, country: null, visaType: null, passengerName: null, passportNo: null, bookingRef: null, amount: null, status: null },
      sortList: [],
    }
  },
  mounted: function () {
    this.loading = true;
    var vm = this;
    window.sessionStorage.setItem("offlineReqTab", "visa");
    vm.getRequest(0);
  },
  methods: {
    searchTable: function () {
      this.loading = true;
      this.getRequest(0);
    },
    getBookingInfo: function (item) {
      window.sessionStorage.setItem("offlineVisaId", item.id);
      window.location.href = "/OfflineRequest.html";
    },
    sortBy: function (event) {
      var sortField = event.sortField;
      var sortOrder = event.sortOrder;
      sortOrder = sortOrder == 1 ? "ASC" : sortOrder == -1 ? "DESC" : "ASC";
      var field = null;
      this.sortList = [];
      if (sortField == "id") {
        this.sortList.push({
          "field": "id",
          "order": sortOrder
        });
      } else if (sortField == "visaType") {
        this.sortList.push({
          "field": "text4",
          "order": sortOrder
        });
      } else if (sortField == "country") {
        this.sortList.push({
          "field": "keyword10",
          "order": sortOrder
        });
      } else if (sortField == "passportNumber") {
        this.sortList.push({
          "field": "keyword2",
          "order": sortOrder
        });
      } else if (sortField == "bookingRef") {
        this.sortList.push({
          "field": "keyword4",
          "order": sortOrder
        });
      } else if (sortField == "name") {
        this.sortList.push({
          "field": "text2",
          "order": sortOrder
        });
      } else if (sortField == "amount") {
        this.sortList.push({
          "field": "amount3",
          "order": sortOrder
        });
      } else if (sortField == "reqStatus") {
        this.sortList.push({
          "field": "keyword8",
          "order": sortOrder
        });
      } else {
        this.sortList = null;
      }
      // sortOrder == 1 ? "ASC" : sortOrder ? sortOrder == -1 ? "DESC" || "ASC"

      this.getRequest(event.first);
    },
    mouseOver: function (item) {
      if (item.requestHST[0].comments) {
        return item.requestHST[0].comments;
      }
    },
    onPage: function (event) {
      var vm = this;
      vm.loading = true;
      vm.getRequest(event.first);
    },
    getRequest: function (from) {
      var vm = this;
      var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrlOffline;
      var port = vm.commonStore.hubUrls.hubConnection.ipAddress;
      var offlineRequest = vm.commonStore.hubUrls.hubConnection.hubServices.flights.visaRequest;
      // var offlineRequest = "/visaRequest";
      var searchList = [];
      var globalSearch = [];

      switch (!isNullorUndefind(this.searchKeywords)) {
        case (!isNullorUndefind(this.searchKeywords.globalSearch)):
          if (this.searchKeywords.globalSearch) {
            globalSearch = { "searchTerm": this.searchKeywords.globalSearch };
          }
          else {
            globalSearch = null
          }
          break;
        default:
          globalSearch = null;
          break;
      }

      switch (!isNullorUndefind(this.searchKeywords)) {
        case (!isNullorUndefind(this.searchKeywords.uniqueId)):
          if (this.searchKeywords.uniqueId) {
            searchList.push({
              "field": "id",
              "term": this.searchKeywords.uniqueId
            });
          }
        case (!isNullorUndefind(this.searchKeywords.visaType)):
          if (this.searchKeywords.visaType) {
            searchList.push({
              "field": "text4",
              "term": this.searchKeywords.visaType
            });
          }
        case (!isNullorUndefind(this.searchKeywords.country)):
          if (this.searchKeywords.country) {
            searchList.push({
              "field": "keyword10",
              "term": this.searchKeywords.country
            });
          }
        case (!isNullorUndefind(this.searchKeywords.passportNo)):
          if (this.searchKeywords.passportNo) {
            searchList.push({
              "field": "keyword2",
              "term": this.searchKeywords.passportNo
            });
          }
        case (!isNullorUndefind(this.searchKeywords.passengerName)):
          if (this.searchKeywords.passengerName) {
            searchList.push({
              "field": "text2",
              "term": this.searchKeywords.passengerName
            });
          }
        case (!isNullorUndefind(this.searchKeywords.bookingRef)):
          if (this.searchKeywords.bookingRef) {
            searchList.push({
              "field": "keyword4",
              "term": this.searchKeywords.bookingRef
            });
          }
        case (!isNullorUndefind(this.searchKeywords.amount)):
          if (this.searchKeywords.amount) {
            searchList.push({
              "field": "amount1",
              "term": this.searchKeywords.amount
            });
          }
        case (!isNullorUndefind(this.searchKeywords.status)):
          if (this.searchKeywords.status && this.searchKeywords.status.length) {
            this.searchKeywords.status.forEach(element => {
              searchList.push({
                "field": "keyword8",
                "term": element
              });
            });
          }
          break;
        default:
          searchList = null
          break;
      }


      var PostData =
      {
        "from": from,
        "size": 15,
        "search": globalSearch
        ,
        "filterList": isNullorUndefind(searchList) ? null : searchList.length ? searchList : null,
        "sortList": this.sortList || null
      };

      var headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
      };
      axios({
        method: "post",
        url: hubUrl + port + offlineRequest,
        data: PostData,
        headers: headers
      }).then(function (response) {
        if (response.data.data.data == undefined || response.data.data.data == null || !response.data.data.data || response.data.data.data.length == 0) {
          vm.loading = false;
          vm.hits = null;
          vm.customers = [];
        } else {
          vm.hits = response.data.data.hits;
          vm.customers = response.data.data.data || [];
          vm.loading = false;
          console.log(response);
        }
      }).catch(function (err) {
        console.log(err);
        vm.loading = false;
        vm.hits = null;
        vm.customers = [];
      });

    },
  }
});