var dateFormat = generalInformation.systemSettings.systemDateFormat;
var statusCodeList = [];
var searchParams = {};

Vue.component("my-quotations", {
  components: {

  },
  data: function () {
    return {
      commonStore: vueCommonStore.state,
      allQuotes: []
    };
  },
  mounted: function () {
    this.getAllquotationsOnServer();
  },
  methods: {
    momentLocale: function () {
      var localLocale = moment();
      localLocale.locale(this.commonStore.selectedLanguage ? this.commonStore.selectedLanguage.code : 'en');
      return localLocale;
    },
    getAllquotationsOnServer: function () {
      var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;
      var serviceUrl = this.commonStore.hubUrls.hubConnection.hubServices.allQuotes;
      var vm = this;

      var config = {
        axiosConfig: {
          method: "get",
          url: hubUrl + serviceUrl
        },
        successCallback: function (response) {
          vm.allQuotes = response.data;
          vm.$nextTick(function () {
            $("#tableSearch").DataTable({
              responsive: true,
              destroy: true,
              autoWidth: false,
              columnDefs: [
                { width: '200px', targets: 0 }, //column 1 out of 7
                { width: '200px', targets: 1 }, //column 2 out of 7
                { width: '200px', targets: 2 }, //column 3 out of 7
                { width: '200px', targets: 3 },  //column 4 out of 7
              ],
              "order": [[0, "desc"]],
              "drawCallback": function () { $('[data-toggle="tooltip"]').tooltip(); }
            }).draw();
          });
        },
        errorCallback: function (error) {
          
        },
        showAlert: false
      };

      mainAxiosRequest(config);
    },
    getQuoteInfo: function(item){
      window.sessionStorage.setItem("quotationId", item.quoteId);
      window.location.href = "/myquotationdetail.html";
    }
  }
});