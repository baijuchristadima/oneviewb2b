Vue.component("arm-charts-div", {
  props: {
    totalBookings: Number,
    chartId: Number,
    chartData: Object,
    currentPage: String,
    language: String
  },
  mounted: function () {
    var vm = this;
    vm.$nextTick(function () {
      am4core.useTheme(am4themes_animated);

      // Create chart instance
      var chart = am4core.create(vm.$refs["serviceCharts" + vm.chartId], am4charts.PieChart);

      chart.data = [vm.chartData, { status: i18n.t('Booking_Reports.totalBookings_Label'), count: vm.totalBookings }];
      if (vm.language == "ar") {
        chart.rtl = true;
      } else {
        chart.rtl = false;
      }
      // Add label
      chart.innerRadius = 43;
      var label = chart.seriesContainer.createChild(am4core.Label);
      label.text = i18n.t('Booking_Reports.totalBookings_Label') + ": " + vm.chartData.count;
      label.horizontalCenter = "middle";
      label.verticalCenter = "middle";
      label.fontSize = 12;

      // Add and configure Series
      var pieSeries = chart.series.push(new am4charts.PieSeries());
      pieSeries.dataFields.value = "count";
      pieSeries.dataFields.category = "status";
      pieSeries.labels.template.disabled = true;

      var bodyStyles = window.getComputedStyle(document.body);
      var secondaryColor = bodyStyles.getPropertyValue('--secondary-color')
      pieSeries.colors.list = [
        new am4core.color(secondaryColor),
        new am4core.color('#CBDBE0')
      ]

    });
  },
  methods: {
    getBookingInfo: function (item) {
      window.localStorage.setItem("sfbws", item);
      window.localStorage.setItem("service", this.currentPage);
      window.localStorage.setItem("mybookingFlag", false);
      window.localStorage.setItem("searchBookingsFlag", false);
      window.location.href = "/searchbookings.html";

    }
  },
  beforeDestroy() {
    if (this.chart) {
      this.chart.dispose();
    }
  },
  template: `
  <div class="col-lg-3 col-md-3 col-sm-6 flightdetails_area">
    <div class="flightdetails" style="height: 276px;">
      <a href="#" @click.prevent="getBookingInfo(chartData.code)">
        <h3>{{chartData.status}}</h3>
      </a>
      <div class="arm-chart-margin" :ref="'serviceCharts'+chartId"></div>
    </div>
  </div>  
    `
});

Vue.component("booking-reports", {
  data: function () {
    return {
      commonStore: vueCommonStore.state,
      bookingStatus: {},
      latestBookings: [],
      chartDataParsed: [],
      allDataLoaded: false,
      dataError: false,
      bookingStatCount: ""
    };
  },
  props: {
    service: String,
    numberOfBookings: String
  },
  watch: {
    service: function () {
      this.allDataLoaded = false;
      this.getBookingDetails();
    }
  },
  mounted: function () {
    this.allDataLoaded = false;
    this.getBookingDetails();
  },
  methods: {

    findBookingStatus: function (searchKey) {
      var bookingStatuses = {
        ticketed: "OK",
        requested: "RQ",
        confirmed: "KK",
        cancelled: "XX",
        reconfirmed: "RR",
        issued: "ID"
      };

      return bookingStatuses[searchKey] ? bookingStatuses[searchKey] : "";
    },
    getKeys: function (item) {
      return 'Booking_Reports' + '.' + item + '_Label';
    },
    getBookingDetails: function () {
      var vm = this;
      var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl;
      var port = this.commonStore.hubUrls.hubConnection.ipAddress;
      var agencyNode = window.localStorage.getItem("agencyNode");
      if (agencyNode) {
        agencyNode = JSON.parse(atob(agencyNode));
        var servicesList = agencyNode.loginNode.servicesList.filter(function (serviceName) {
          var service = "";
          if (vm.service.toLowerCase() == "flights") {
            service = "air";
          } else if (vm.service.toLowerCase() == "hotels") {
            service = "hotel";
          } else if (vm.service.toLowerCase() == "unitrust-insurance") {
            service = "insurance";
          } else if (vm.service.toLowerCase() == "wis-insurance") {
            service = "insurance";
          } else if (vm.service.toLowerCase() == "tp-insurance") {
            service = "tuneprotectinsurance";
          } else {
            service = vm.service.toLowerCase();
          }
          return serviceName.name.toLowerCase() == service;
        });
      }
      if (servicesList.length > 0) {

        var bookingStatusUrl = this.commonStore.hubUrls.hubConnection.hubServices.reportUrl + "/" + servicesList[0].id;
        var latestBookingsUrl = this.commonStore.hubUrls.hubConnection.hubServices.myBookings + "/" + servicesList[0].id + "/" + vm.numberOfBookings;

        var accessToken = window.localStorage.getItem("accessToken");
        axios.all([
          axios.get(hubUrl + port + bookingStatusUrl, { headers: { Authorization: "Bearer " + accessToken } }),
          axios.get(hubUrl + port + latestBookingsUrl, { headers: { Authorization: "Bearer " + accessToken } })
        ]).then(axios.spread(function (bookingStatus, latestBookings) {
          if (bookingStatus.data) {
            vm.bookingStatus = bookingStatus.data;
            vm.bookingStatCount = bookingStatus.data.bookingStatCount;
            var bookingStatCount = bookingStatus.data.bookingStatCount;
            var chartDataParsed = [];

            for (var key in bookingStatCount) {
              if (bookingStatCount.hasOwnProperty(key)) {
                if (key != "totalBookings") {
                  chartDataParsed.push({ status: i18n.t(vm.getKeys(key)), count: bookingStatCount[key], code: vm.findBookingStatus(key.toLowerCase()) });
                }
              }
            }
            vm.chartDataParsed = chartDataParsed;

          }
          if (latestBookings.data) {
            var sortedData = latestBookings.data.sort(function (a, b) {
              return b.referenceNumber.split("-")[1] - a.referenceNumber.split("-")[1];
            });
            vm.latestBookings = sortedData;
          }

          vm.allDataLoaded = true;

          vm.$nextTick(function () {
            $('[data-toggle="tooltip"]').tooltip();
          });

        })).catch(function (error) {
          console.log(error);
          vm.dataError = true;
        });
      } else {
        vm.latestBookings = vm.chartDataParsed = [];
        vm.allDataLoaded = true;
      }
    },
    getTripDetails: function (trip) {
      return '<span data-toggle="tooltip" data-placement="top" title="" data-original-title="' +
        airportLocationFromAirportCode(trip.split(' ')[0]) + ' to ' + airportLocationFromAirportCode(trip.split(' ')[2]) + '">' + trip + '</span>';
    },
    getDateDetails: function (date) {
      var localLocale = moment(date);
      localLocale.locale(this.commonStore.selectedLanguage ? this.commonStore.selectedLanguage.code : 'en');
      return '<span data-toggle="tooltip" data-placement="top" title="" data-original-title="' +
        localLocale.format('DD MMM YYYY HH:mm') + '">' + localLocale.format('DD MMM YYYY') + '</span>';
    },
    getCityDetails: function (city) {
      var cityNameWithCountry = (city || "").split(" ").map(function (e) {
        return e.charAt(0).toUpperCase() + e.slice(1).toLowerCase();
      }).slice().join(" ");
      var cityName = cityNameWithCountry.split(",")[0];
      return '<span data-toggle="tooltip" data-placement="top" title="" data-original-title="' + cityNameWithCountry + '">' + cityName + '</span>';
    },
    getRefDetails: function (tooltip, data) {
      return '<span data-toggle="tooltip" data-placement="top" title="" data-original-title="' + tooltip + '">' + data + '</span>';
    },
    getBookingInfo: function (booking) {

      if (this.commonStore.currentSearchpage == "flights") {
        window.sessionStorage.setItem('flightbookingId', booking.referenceNumber);
        window.sessionStorage.setItem('sendbookmail', true);
        try { window.sessionStorage.setItem("selectCredential", JSON.stringify(booking.selectCredential)); } catch (err) { }
        window.location.href = "/Flights/bookinginfo.html";

      } else if (this.commonStore.currentSearchpage == "hotels") {
        window.sessionStorage.setItem('bookingId', booking.referenceNumber);
        window.sessionStorage.setItem('supplierId', booking.supplierId);
        window.location.href = "/Hotels/hotelconfirmation.html";
      }
      else if (this.commonStore.currentSearchpage == "sightseeing") {

      }
      else if (this.commonStore.currentSearchpage == "tp-insurance") {
        window.sessionStorage.setItem("Insurance_Info", JSON.stringify(booking));
        try { window.sessionStorage.setItem("selectCredential", JSON.stringify(booking.selectCredential)); } catch (err) { }
        window.location.href = '/Insurance/TuneProtect/policyinfo.html';
      }
      else if (this.commonStore.currentSearchpage == "wis-insurance") {
        window.sessionStorage.setItem("Insurance_Info", JSON.stringify(booking));
        try { window.sessionStorage.setItem("selectCredential", JSON.stringify(booking.selectCredential)); } catch (err) { }
        window.location.href = '/Insurance/wis/policyinfo.html';
      }
      else if (this.commonStore.currentSearchpage == "unitrust-insurance") {
        window.sessionStorage.setItem("Insurance_Info", JSON.stringify(booking));
        try { window.sessionStorage.setItem("selectCredential", JSON.stringify(booking.selectCredential)); } catch (err) { }
        window.location.href = '/Insurance/unitrust/policyinfo.html';
      }
      else if (this.commonStore.currentSearchpage == "transfer") {

      }
    }
  },
  computed: {
    imageLoader: function () {
      if (this.commonStore.currentSearchpage == "flights") {
        return "/assets/images/AirGenerateBooking.gif";
      } else if (this.commonStore.currentSearchpage == "hotels") {
        return "/assets/images/HotelGenerateBooking.gif";
      }
      else if (this.commonStore.currentSearchpage == "sightseeing") {
        return "/assets/images/SightseeingGenerateBooking.gif";
      }
      else if (this.commonStore.currentSearchpage == "wis-insurance" || this.commonStore.currentSearchpage == "tp-insurance" || this.commonStore.currentSearchpage == "unitrust-insurance") {
        return "/assets/images/InsuranceGenerateBooking.gif";
      }
      else if (this.commonStore.currentSearchpage == "transfer") {
        return "/assets/images/TransferGenerateBooking.gif";
      }
    },
    showDiv: function () {
      return true;
    },
    checkRole: function () {
      if (this.commonStore.currentSearchpage == "flights" && this.commonStore.commonRoles.hasAir) {
        return true;
      } else if (this.commonStore.currentSearchpage == "hotels" && this.commonStore.commonRoles.hasHotel) {
        return true;
      }
      else if (this.commonStore.currentSearchpage == "sightseeing" && this.commonStore.commonRoles.hasSightseeing) {
        return true;
      }
      else if ((this.commonStore.currentSearchpage == "unitrust-insurance" || this.commonStore.currentSearchpage == "wis-insurance" || this.commonStore.currentSearchpage == "tp-insurance") && this.commonStore.commonRoles.hasInsurance) {
        return true;
      }
      else if (this.commonStore.currentSearchpage == "transfer" && this.commonStore.commonRoles.hasTransfer) {
        return true;
      } else {
        return false;
      }
    }
  },
  template: `
  <div v-if="showDiv&&checkRole">
    <div class="container" v-if="!dataError&&!allDataLoaded">
      <div class="row">
        <div class="acvtivity_details_area">
          <div id="bookingStatusLoader">
            <div class="col-lg-12 col-md-12 col-sm-12 flightdetails_area">
              <div :class="{'chart_section':imageLoader}">
                <div class="viewmiddle">
                  <div class="reportLoading"><img :src="imageLoader"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container" v-if="!dataError&&allDataLoaded">
      <div class="row">
        <div class="acvtivity_details_area">
          <div>
            <arm-charts-div v-for="(chart, index) in chartDataParsed" :key="index" :total-bookings="bookingStatus.bookingStatCount.totalBookings" :chartId="index" :chart-data="chart" :current-page="commonStore.currentSearchpage" :language="commonStore.selectedLanguage.code"></arm-charts-div>
          </div>
          <div class="search_area fullwidth" v-if="allDataLoaded">
            <h6>{{commonStore.currentSearchpage=='flights'?$t('Booking_Reports.TableActionRequired_Label'):$t('Booking_Reports.TableLastFiveBookings_Label')}}</h6>
            <div v-if="latestBookings.length==0" class="text-center"><p class="bg-info">{{$t('Booking_Reports.NoRecord_Label')}}</p></div>
            <div v-else class="srch_table22">
              <table class="table table-responsive table-hover srch_grid_style01" v-if="commonStore.currentSearchpage == 'flights'">
                <thead>
                  <tr>
                    <th>{{$t('Booking_Reports.ReferenceNumber_Label')}}</th>
                    <th>{{$t('Booking_Reports.SupplierReference_Label')}}</th>
                    <th>{{$t('Booking_Reports.Trip_Label')}}</th>
                    <th>{{$t('Booking_Reports.BookedOn_Label')}}</th>
                    <th>{{$t('Booking_Reports.Status_Label')}}</th>
                    <!--<th>{{$t('Booking_Reports.Deadline_Label')}}</th>-->
                  </tr>
                </thead>
                <tbody>
                  <tr v-for="item in latestBookings" @click="getBookingInfo(item)" style="cursor:pointer">
                    <td v-html="getRefDetails(item.paxName,item.referenceNumber)"></td>
                    <td>{{item.supplierReferenceNumber||'N/A'}}</td>
                    <td v-html="getTripDetails(item.trip)"></td>
                    <td v-html="getDateDetails(item.bookDate)"></td>
                    <td>{{item.status}}</td>
                    <!--<<td v-html="getDateDetails(item.deadlineDate)"></td>-->
                  </tr>
                </tbody>
              </table>
              <table class="table table-responsive table-hover srch_grid_style01" v-if="commonStore.currentSearchpage == 'hotels'">
                <thead>
                  <tr>
                    <th>{{$t('Booking_Reports.ReferenceNumber_Label')}}</th>
                    <th>{{$t('Booking_Reports.City_Label')}}</th>
                    <th>{{$t('Booking_Reports.Hotel_Label')}}</th>
                    <th>{{$t('Booking_Reports.Checkin_Label')}}</th>
                    <th>{{$t('Booking_Reports.Nights_Label')}}</th>
                    <th>{{$t('Booking_Reports.Timelimit_Label')}}</th>
                    <th>{{$t('Booking_Reports.BookedOn_Label')}}</th>
                    <th>{{$t('Booking_Reports.Status_Label')}}</th>
                  </tr>
                </thead>
                <tbody>
                  <tr v-for="item in latestBookings" @click="getBookingInfo(item)" style="cursor:pointer">
                    <td>{{item.referenceNumber}}</td>
                    <td v-html="getCityDetails(item.cityName)"></td>
                    <td>{{item.hotelName}}</td>
                    <td v-html="getDateDetails(item.checkInDate)"></td>
                    <td>{{item.numOfNights}}</td>
                    <td v-html="getDateDetails(item.deadlineDate)"></td>
                    <td v-html="getDateDetails(item.bookDate)"></td>
                    <td v-html="getRefDetails(item.status,item.status)"></td>
                  </tr>
                </tbody>
              </table>
              <table class="table table-responsive table-hover srch_grid_style01" v-if="commonStore.currentSearchpage == 'sightseeing'">
                <thead>
                  <tr>
                    <th>{{$t('Booking_Reports.ReferenceNumber_Label')}}</th>
                    <th>{{$t('Booking_Reports.SupplierReference_Label')}}</th>
                    <th>{{$t('Booking_Reports.Activity_Label')}}</th>
                    <th>{{$t('Booking_Reports.BookedOn_Label')}}</th>
                    <th>{{$t('Booking_Reports.Status_Label')}}</th>
                  </tr>
                </thead>
                <tbody>
                  <tr v-for="item in latestBookings" @click="getBookingInfo(item)" style="cursor:pointer">
                    <td v-html="getRefDetails(item.paxName,item.referenceNumber)"></td>
                    <td>{{item.supplierReferenceNumber||'N/A'}}</td>
                    <td>{{item.sightseeingItemDesc}}</td>
                    <td v-html="getDateDetails(item.bookDate)"></td>
                    <td>{{item.status}}</td>
                  </tr>
                </tbody>
              </table>
              <table class="table table-responsive table-hover srch_grid_style01" v-if="commonStore.currentSearchpage == 'unitrust-insurance' || commonStore.currentSearchpage == 'wis-insurance'||commonStore.currentSearchpage == 'tp-insurance'">
                <thead>
                  <tr>
                    <th>{{$t('Booking_Reports.ReferenceNumber_Label')}}</th>
                    <th>{{$t('Booking_Reports.SupplierReference_Label')}}</th>
                    <th>{{$t('Booking_Reports.Insurance_Label')}}</th>
                    <th>{{$t('Booking_Reports.BookedOn_Label')}}</th>
                    <th>{{$t('Booking_Reports.Status_Label')}}</th>
                  </tr>
                </thead>
                <tbody>
                  <tr v-for="item in latestBookings" @click="getBookingInfo(item)" style="cursor:pointer">
                    <td v-html="getRefDetails(item.paxName,item.referenceNumber)"></td>
                    <td>{{item.supplierReferenceNumber||'N/A'}}</td>
                    <td>{{item.insurName}}</td>
                    <td v-html="getDateDetails(item.bookDate)"></td>
                    <td>{{item.status}}</td>
                  </tr>
                </tbody>
              </table>
              <table class="table table-responsive table-hover srch_grid_style01" v-if="commonStore.currentSearchpage == 'transfer'">
                <thead>
                  <tr>
                    <th>{{$t('Booking_Reports.ReferenceNumber_Label')}}</th>
                    <th>{{$t('Booking_Reports.SupplierReference_Label')}}</th>
                    <th>{{$t('Booking_Reports.Vehicle_Label')}}</th>
                    <th>{{$t('Booking_Reports.BookedOn_Label')}}</th>
                    <th>{{$t('Booking_Reports.Status_Label')}}</th>
                  </tr>
                </thead>
                <tbody>
                  <tr v-for="item in latestBookings" @click="getBookingInfo(item)" style="cursor:pointer">
                    <td v-html="getRefDetails(item.paxName,item.referenceNumber)"></td>
                    <td>{{item.supplierReferenceNumber||'N/A'}}</td>
                    <td>{{item.insurName}}</td>
                    <td v-html="getDateDetails(item.bookDate)"></td>
                    <td>{{item.status}}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  `
});

