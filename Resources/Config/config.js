﻿var globalConfigs = {
  services: {
    flights: {
      // supplpierWithoutCancelPNR: [44, 49, 50], //ariarabia
      voidAllTicket: {
        supplier: [57, 83, 62],
        status: ["OK", "PV", "XX"],
      },
      voidTicket: {
        supplier: [1, 45, 55, 72, 57],
        status: ["OK", "PV", "XX"],
      },
      cancelPNR: {
        supplier: [1, 45, 55, 57, 72, 73, 83, 62, 60],
        status: ["OK", "TP", "TF", "TV", "PV", "HK", "FC", "FE", "RQ", "RF", "PD", "NF", "HL", "UC", "UN"],
      },
      issueTicket: {
        supplier: [1, 45, 55, 44, 49, 50, 71, 57, 72, 73, 64, 54, 58, 83, 62, 60, 74,86,87],
        status: ["TP", "TF", "TV", "PV", "HK"],
      },
      offlineTicket: {
        supplier: [1, 45, 55, 44, 49, 50, 71, 57, 64, 54, 83, 62, 60, 58, 74,86,87],
        status: ["TP", "TF", "TV", "PV", "HK"],
      },
      repricePNR: {
        supplier: [1, 45, 72, 55],
        status: ["TP", "TF", "TV", "PV", "HK", "FC", "FE", "RQ", "RF", "PD", "NF"],
      },
      emailVoucher: {
        supplier: [1, 45, 55, 44, 49, 50, 71, 54, 69, 18, 57, 42, 72, 73, 64, 58, 62, 60, 74,86],
        status: ["OK", "TP", "TF", "TV", "PV", "HK", "FC", "FE", "RQ", "RF", "PD", "NF", "HL", "UC", "UN"],
      },
      editVoucher: {
        supplier: [1, 45, 55, 44, 49, 50, 71, 54, 69, 18, 57, 42, 72, 73, 64, 60, 58,86],
        status: ["OK", "TP", "TF", "TV", "PV", "HK", "FC", "FE", "RQ", "RF", "PD", "NF"],
      },
      printVoucher: {
        supplier: [1, 45, 55, 44, 49, 50, 71, 54, 69, 18, 57, 42, 72, 73, 64, 83, 62, 60, 58, 74,86,87],
        status: ["OK", "TP", "TF", "TV", "PV", "HK", "FC", "FE", "RQ", "RF", "PD", "NF", "HL", "UC", "UN"],
      },
      downloadVoucher: {
        supplier: [1, 45, 55, 44, 49, 50, 71, 54, 69, 18, 57, 42, 72, 73, 64, 58, 83, 62, 60, 58, 74,86,87],
        status: ["OK", "TP", "TF", "TV", "PV", "HK", "FC", "FE", "RQ", "RF", "PD", "NF", "HL", "UC", "UN"],
      },
      fareRules: {
        supplier: [1, 45, 55, 54, 69, 18, 57, 42, 83, 62],
        status: ["OK", "TP", "TF", "TV", "PV", "HK", "FC", "FE"],
      },
      miniRules: {
        supplier: [1, 83, 62],
        status: ["OK", "TP", "TF", "TV", "PV", "HK", "FC", "FE", "HL"],
      },
      upSellPlan: {
        supplier: [45, 54, 69, 18, 57],
      },
      instantPurchase: {
        supplier: [44, 49, 50, 71, 54, 69, 18, 57, 42, 64],
      },
      bookOnHold: {
        // supplier: [1, 45, 55, 18, 57, 42, 44, 49, 50, 71, 72, 73, 64, 54, 58, 83, 60, 61, 74],
        // supplierWithAdditionalBookBtn: [44, 49, 50, 71, 72, 54],
      },
      availability: {
        supplier: [1, 45, 55]
      },
      editPassportInfo: {
        supplier: [1, 58, 45, 55, 83, 62],
        status: ["RQ", "HK", "FC", "NF", "TF"],
      },
      editPassportInfoAfterTicketed: {
        supplier: [83],
        status: ["OK"],
      },
      calendarSearch: {
        supplier: [1, 45, 55],
      },
    },
    hotels: {
      suppliersWithPrecancel: [29, 82],
      suppliersWithHotelWiseCancellation: [22, 27, 40, 43, 30],
      suppliersWithMealTypeInRoomName: [25, 40],
      suppliersWithHotelAndRoomDetailsAsync: [],
      //if log is none don't send RQ
      //debug - save all logs
      //error - save errors
      logLevel: "none",
      additionalSupplierInformation: [{
        supplierId: 53,
        customerSupportNumber: {
          show: true,
          data: "(800) 273-8034"
        },
        termsAndConditions: {
          show: true,
          data: "https://developer.expediapartnersolutions.com/terms/en/"
        },
        specialInstructions: {
          show: true
        },
      },],
    },
  },
};