﻿var HubServiceUrls = {
  "hubConnection": {
    "baseUrl": "https://staginghubv3.oneviewitsolutions.com:",
    "webSocketUrl": "wss://staginghubv3.oneviewitsolutions.com:8012/events",
    "cmsUrl": "https://cms.oneviewitsolutions.com:",
    "baseUrlOffline": "https://staginghubv3.oneviewitsolutions.com:",
    // "baseUrl": "https://devhub.ntravel.ae:",
    // "cmsUrl": "https://cms.ntravel.ae:",
    // "baseUrl": "https://livehubv3.oneviewitsolutions.com:",
    // "webSocketUrl": "wss://livehubv3.oneviewitsolutions.com:8012/events",
    "ipAddress": 9443,
    "webSocketIpAddress": 8012,
    "logoBaseUrl": "http://staginghub.oneviewitsolutions.com:8080/premier-v3/javax.faces.resource/",
    "proxyUrl": "https://cors-anywhere.herokuapp.com/",
    "fileUploadUrl": "https://fileupload.oneviewitsolutions.com:",
    "hubServices": {
      "authenticateUrl": "/authenticate",
      "forgotPasswordUrl": "/profile/forgot",
      "creditLimit": "/transaction/availablecreditlimit",
      "reportUrl": "/commonReport/count",
      "dashboardReports": "/commonReport/upcomingcount",
      "reNewToken": "/refreshtoken",
      "myBookings": "/commonReport",
      "resetPassword": "/profile/password/reset",
      "changePassword": "/profile/password",
      "creditLimitWebSocket": "/events",
      "paymentGatewayCredentials": "/payment",
      "paymentGatewayUpdateStatus": "/payment/exception",
      "paymentGatewayHQ": "/payment/paymentGateway/",
      "contact": "/contact",
      "emailSettings": "/emailSettings",
      "getLoginPageUrl": "/loginpage",
      "getBookingRef": "/payment/bookingRef",
      "getAgencyCodes": "/agencyCodes",
      "updateProfile": "/profile/update",
      "registerUser": "/registerUser",
      "getChildAgencyCodes": "/commonReport/childAgencyCodes",
      "nodeList": "/node",
      "switchNode": "/switchNode",
      "upload": "/upload",
      "addQuote": "/quotes/addQuote",
      "pendingQuoteDetails": "/quotes/pendingQuoteDetails",
      "removeQuote": "/quotes/remove",
      "sendQuote": "/quotes/saveSendQuote",
      "quoteDetails": "/quotes/quoteDetails",
      "allQuotes": "/quotes/allQuotes",
      "accountManager": "/account",
      "pgPurchase": "/payment/purchase",
      "pgPurchaseDone": "/payment",
      "pgPayStack": "/paystack/initialise",
      "pgSyberPayInitialise": "/syberPay/initialise",
      "pgSyberPayVerify": "/syberPay/verify",
      "pgFlutterwave": "/flutterwave/initialise",
      "pgFAB": "/epg/initialise",
      "pgWaafiPay": "/waafipay/initialise",
      "pgWaafiPayEvc": "/waafipay/evc/initialise",
      "pgWaafiPayZaad": "/waafipay/zaad/initialise",
      "pgNGeniusPay": "/ngenius/initialise",
      "pgEDahab": "/edahab/initialise",
      "stanbic": "/stanbic/initialise",
      "flights": {
        "airSearch": "/flightSearch/search",
        "airSelect": "/flightSelect",
        "airBooking": "/flightBook",
        "airTripDetails": "/tripDetail",
        "airIssueTicket": "/issueFlight",
        "AircancelPnr": "/cancelFlight",
        "AircancelTicket": "/cancelTicket",
        "myBookingsCriteria": "/commonReport/flight",
        "getCoTravellers": "/traveler/1",
        "airMiniRule": "/miniRules",
        "airSeatMap": "/airSeatMap",
        "airmeals": "/airmeals",
        "airBaggage": "/airBaggage",
        "ancillaryPriceQuote": "/ancillarypricequote",
        "pnrImport": "/pnrImport",
        "offlineRequest": "/offlinereq",
        "offlineRequestFare": "/offlinereq/ticketing/fare/NULL",
        "offlineRequestFareWithBookingRef": "/offlinereq/ticketing/fare/",
        "offlineRequestTicketing": "/offlinereq/ticketing",
        "offlineRequestUpdate": "/offlinereq/update",
        "myBookingsConsolidateCriteria": "/commonReport/consolidated/flight",
        "repriceRBD": "/repriceRBD",
        "repricingRBD": "/repricingRBD",
        "repriceLowerfare": "/repriceLowerfare",
        "repricingLowerfare": "/repricingLowerfare",
        "offlineRefund": "/refund",
        "offlineRequestRefund": "/insert",
        "offlineRefundCancel": "/cancel",
        "cancelTicketCharge": "/cancelTicket/charge",
        "visaRequest": "/visaRequest",
        "offlineVisaCancel": "/visaRequest/cancel",
        "airAvailability": "/airAvailability",
        "updatePassportModifyBook": "/flightModifyBook",
      },
      "transfer": {
        "bookTransfer": "/bookTransfer",
        "myBookingsCriteria": "/commonReport/transfer",
        "transferbookdetails": "/transfer/bookingDetails",
        "transferSearch": "/searchTransfer",
        "transfercancel": "/cancelTransfer"

      },
      "hotels": {
        "hotelBooking": "hotel/booking",
        "bookingReference": "hotel/booking/bookingbyref/",
        "bookingChangeStatus": "hotel/booking/changeStatus",
        "searchBookings": "/commonReport/hotel",
        "myBookingsConsolidateCriteria": "/commonReport/consolidated/hotel",
        "allBookings": "/hotel/allBookings",
        "bookDetails": "/hotelBook/details",
      },
      "insurance": {
        "tuneinsuranceDetails": "/insurDetails",
        "myBookingsCriteria": "/commonReport/insurance",
        "tpGetCurrencyCode": "/country/countries",
        "purchase": "/purchase",
        "tuneprotectDetail": "/tuneprotectDetail"
      },
      "wisInsurance": {
        "finaliseInsurance": "/finaliseInsurance",
        "purchaseInsurance": "/purchaseInsurance",
        "insuranceDetails": "/insuranceDetails",
        "insuranceDocument": "/insuranceDocument"
      },
      "unitrust": {
        "singleTripInsurance": "/singleTripInsurance",
        "insuranceDetails": "/insuranceDetails/uniTrust"
      }
    }
  },
  "cacheServiceApi": {
    "apiUrl": "http://oneviewcachedata.oneviewitsolutions.com",
    "services": {
      "getMinPrice": "/price-calendar/get-min-price",
      "addMinPrice": "/price-calendar/add-min-price",
      "transferCity": "/transfer/get-cities",
      "transferSubcity": "/transfer/get-subcities",
      "transferHotel": "transfer/get-transferhotels"
    }
  },
  "elasticSearch": {
    "elasticsearchHost": "ee3b963212284c488a7309cdab3b262c.ap-southeast-1.aws.found.io",
    "auth": "OVIT_BookingPortal:4Rfv7Ujm$",
    "protocol": "https",
    "port": "9243",
    "url": "https://airportinfo.oneviewitsolutions.com/",
    "airFilter": "https://api.oneviewitsolutions.com/matrixhub/airfilter"
  },
  "emailServices": {
    // "emailApi": "https://gjzlfhhb77.execute-api.us-east-1.amazonaws.com/default/GenericMail"
    "emailApi": "https://6ejwh0rlo3.execute-api.ap-south-1.amazonaws.com/default/GenericMail",
    "generatePdf": "https://template.oneviewitsolutions.com:9483/pdfGenerate",
    "getTemplate": "https://template.oneviewitsolutions.com:9483/template/all/",
    "htmlGenerate": "https://template.oneviewitsolutions.com:9483/htmlGenerate",
    "emailApiWithAttachment": "https://6ejwh0rlo3.execute-api.ap-south-1.amazonaws.com/default/GenericMail/attachment"
  },
  "fileUploads": [
    {
      "agency": "AGY389",
      "password": "6Yhn7Ujm^"
    },
    {
      "agency": "AGY20254",
      "password": "5Tgb6Yhn%"
    },
    {
      "agency": "AGY20254",
      "password": "5Tgb6Yhn%"
    },
  ]
}
