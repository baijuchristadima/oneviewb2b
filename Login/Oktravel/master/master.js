var HeaderComponent = Vue.component('headeritem', {
    template: `
        <div id="header">
            <nav id="navbar-main" class="navbar navbar-default navbar-fixed-top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                                <a class="navbar-brand" href="index.html"><img :src="TopSection.Logo_128_x_86px" alt=""></a>
                            </div>
                            <div class="call-us">
                                <img src="assets/images/phone.png" alt="">
                                <a :href="'tel:'+FooterLabels.Phone_Number">{{FooterLabels.Phone_Number}}</a>
                            </div>
                            <div class="collapse navbar-collapse navbar-right" id="myNavbar">
                                <ul class="nav navbar-nav">
                                    <li><a :class="{ 'active': activeClass === '/index.html' || activeClass === '/'}" href="index.html">{{TopSection.Home_Label}}</a></li>
                                    <li><a  :class="{ 'active': activeClass === '/about-us.html'}" href="about-us.html">{{TopSection.About_Us_Label}}</a></li>
                                    <li><a :class="{ 'active': activeClass === '/contact-us.html'}" href="contact-us.html">{{TopSection.Contact__Us_Label}}</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>`,
    data() {
        return {
            agencyCode: '',
            TopSection:{},
            FooterLabels:{},
            activeClass: '',
        }
    },
    created() {
        var self=this;
        var pathArray = window.location.pathname.split('/');
        self.activeClass ='/'+ pathArray[pathArray.length-1];
       
      },
    computed:{
        checkMobileOrNot: function () {
          if (navigator.userAgent.match(/Android/i) ||
              navigator.userAgent.match(/webOS/i) ||
              navigator.userAgent.match(/iPhone/i) ||
              navigator.userAgent.match(/iPad/i) ||
              navigator.userAgent.match(/iPod/i) ||
              navigator.userAgent.match(/BlackBerry/i) ||
              navigator.userAgent.match(/Windows Phone/i)) {
              return true;
          }
          else { return false; }
        }
      },
    methods: {
        getPageheader: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {

                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }
                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/01 Master/01 Master/01 Master.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.content = response.data;
                            if (self.content != undefined && self.content != null) {
                                var mainData = self.pluck('Footer_Section',self.content.area_List);
                                self.FooterLabels = self.getAllMapData(mainData[0].component);
                                var meData = self.pluck('Header_Section', self.content.area_List);
                                self.TopSection = self.getAllMapData(meData[0].component);
                                
                              }
                        }).catch(function (error) {
                            console.log('Error');
                        });
                    }

                });

            });

        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },
    },
    mounted: function () {

        this.getPageheader();
    },
})
var headerinstance = new Vue({
    el: 'header',
    name: 'headerArea',
    data() {
        return {
            key: '',
            content: null,
            getdata: true
        }

    },

});
Vue.component('footeritem', {
    template: `
    <div>
    <footer :style="'background:url('+FooterLabels.Background_Image_1920_x_529px+') no-repeat top;'">
        <div class="container">
            <div class="inner">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="about">
                            <div class="ftr-logo"><img :src="FooterLabels.Footer_Logo_112_x_76px" alt=""></div>
                            <!--<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, Lorem ipsum dolor sit amet, consetetur sadipscing elitr,</p>-->
                            <ul class="social">
                                <li v-for="item in FooterLabels.Social_Media_List"><a :href="item.URL" target="_blank"><i aria-hidden="true" class="fa" v-bind:class="item.Icon"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="contact">
                            <h3>{{FooterLabels.Get_In_Touch_Label}}</h3>
                            <ul>
                                <li>
                                    <i class="fa fa-mobile fnt-sz-22" aria-hidden="true"></i>
                                    <a :href="'tel:'+FooterLabels.Phone_Number">{{FooterLabels.Phone_Number}}</a>
                                </li>
                                <li>
                                    <i class="fa fa-envelope" aria-hidden="true"></i>
                                    <a href="mailto:oktravels786@gmail.com">{{FooterLabels.Email}}</a>
                                </li>
                                <li>
                                    <i class="fa fa-map-marker fnt-sz-17" aria-hidden="true"></i>
                                   {{FooterLabels.Address}}
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <div class="link">
                            <h3>Site Map</h3>
                            <ul>
                                <li><a href="index.html">{{menuData.Home_Label}}</a></li>
                                <li><a href="about-us.html">{{menuData.About_Us_Label}}</a></li>
                                <li><a href="contact-us.html">{{menuData.Contact__Us_Label}}</a></li>
                            </ul>
                            <ul>
                                <li><a href="#">{{FooterLabels.Terms_And_Condition_Label}}</a></li>
                                <li><a href="#">{{FooterLabels.Privacy_Policy_Label}}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ftr-btm">
            <div class="container">
                <div class="inner">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="copyright">{{FooterLabels.Copy_Right_Description}}</div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <!--<div class="powered-by">Powered by: &nbsp; <a href="http://www.oneviewit.com/" target="_blank"><img alt="" src="assets/images/oneview-logo.png"></a></div>-->
                        </div>
                    </div>
                </div>
                <div class="back-top text-center" title="Back to Top"> <a href="javascript:void(0);" id="toTop" style="display: inline;"> <i class="fa fa-chevron-up"></i> </a> </div>
            </div>
        </div>
        </footer>
        </div>`,
    data() {
        return {
            //commonStore: vueCommonStore.state,
            agencyCode: '',
            FooterLabels:{},
            menuData:{},
            newsltremail:'',
            isLoading: false

        }
    },
    methods: {
        getPagefooter: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var Footer = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/01 Master/01 Master/01 Master.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(Footer, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.content = response.data;
                            if (self.content != undefined && self.content != null) {
                                var mainData = self.pluck('Footer_Section',self.content.area_List);
                                self.FooterLabels = self.getAllMapData(mainData[0].component);
                                var meData = self.pluck('Header_Section', self.content.area_List);
                                self.menuData = self.getAllMapData(meData[0].component);
                                
                              }
                            
                        }).catch(function (error) {
                            console.log('Error');
                            self.content = [];
                        });
                    }

                });

            });

        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        }
    },
    mounted: function () {
        this.getPagefooter();
    },

})
var footerinstance = new Vue({
    el: 'footers',
    name: 'footerArea',
    data() {
        return {
            key: 0,
            content: null,
            getdata: true,
        }

    },
});
