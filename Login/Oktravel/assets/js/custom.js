

/* Scroll To Top JS
============================================================== */
jQuery(window).scroll(function () {
    scrollToTop('show');
});

jQuery(document).ready(function () {
    scrollToTop('click');
});

/* Animated Function */
function scrollToTop(i) {
    if (i == 'show') {
        if (jQuery(this).scrollTop() != 0) {
            jQuery('#toTop').fadeIn();
        } else {
            jQuery('#toTop').fadeOut();
        }
    }
    if (i == 'click') {
        jQuery('#toTop').click(function () {
            jQuery('body,html').animate({
                scrollTop: 0
            }, 500);
            return false;
        });
    }
}



/* Our Services Slider JS
============================================================== */
$(document).ready(function () {

    // $("#owl-demo-2").owlCarousel({
    //     items: 4,
    //     lazyLoad: true,
    //     loop: true,
    //     margin: 30,
    //     navigation: true,
    //     itemsDesktop: [1024, 3],
    //     itemsDesktopSmall: [979, 3],
    //     itemsTablet: [768, 2],
    //     itemsMobile: [767, 1],
    // });
    // $(".owl-prev").html('<i class="fa  fa-angle-left"></i>');
    // $(".owl-next").html('<i class="fa  fa-angle-right"></i>');


    $(".forgot_click").click(function () {
        $(".login-form").hide();
        $(".forgot_form").show();
    });
    $(".back").click(function () {
        $(".login-form").show();
        $(".forgot_form").hide();
    });
});


