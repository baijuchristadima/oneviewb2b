/*owl carousel start */

$(function () {

    $("#owl-demo-2").owlCarousel({
        items: 2,
        lazyLoad: true,
        loop: true,
        margin: 30,
        navigation: true,
        itemsDesktop: [991, 2],
        itemsDesktopSmall: [979, 2],
        itemsTablet: [768, 2],
        itemsMobile: [480, 1],
    });
    $(".owl-prev").html('<i class="fa fa-angle-left"></i>');
    $(".owl-next").html('<i class="fa fa-angle-right"></i>');




});

/*owl carousel end */


/*-----------------------------------------*/

/*Smooth scrolling start */

$(document).ready(function () {
    setTimeout(function () {
        $('#pageLoader').hide();
    }, 1000);
    // Select all links with hashes
    $('a[href*="#"]')
        // Remove links that don't actually link to anything
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function (event) {
            // On-page links
            if (
                location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
                location.hostname == this.hostname
            ) {
                // Figure out element to scroll to
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                // Does a scroll target exist?
                if (target.length) {
                    // Only prevent default if animation is actually gonna happen
                    event.preventDefault();
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000, function () {
                        // Callback after animation
                        // Must change focus!
                        var $target = $(target);
                        $target.focus();
                        if ($target.is(":focus")) { // Checking if the target was focused
                            return false;
                        } else {
                            $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                            $target.focus(); // Set focus again
                        };
                    });
                }
            }
        });
    $(".forgot").click(function () {
        $(".loginform").hide();
        $(".forgot").hide();
        $(".backlogin").show();
        $(".login_form02").show();
    });
    $(".backlogin").click(function () {
        $(".login_form02").hide();
        $(".forgot").show();
        $(".backlogin").hide();
        $(".loginform").show();
    });
});
/*Smooth scrolling end */

/*-----------------------------------------*/


jQuery(window).scroll(function () {
    scrollToTop('show');
    
});

jQuery(document).ready(function () {
    scrollToTop('click');
});
function scrollToTop(i) {
    if (i == 'show') {
        if (jQuery(this).scrollTop() != 0) {
            jQuery('#toTop').fadeIn();
        } else {
            jQuery('#toTop').fadeOut();
        }
    }
    if (i == 'click') {
        jQuery('#toTop').click(function () {
            jQuery('body,html').animate({
                scrollTop: 0
            }, 500);
            return false;
        });
    }
}



