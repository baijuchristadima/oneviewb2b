var home = new Vue({
    el: '#homepage',
    name: 'homepage',
    data: {
        commonStore: vueCommonStore.state,
        agencyCode: '',
        topSection: {},
        mainSection: {},
        footerSection: {},
        copyRightSection: {},
        bannerSection: {},
        loginSection: {},
        aboutSection: {},
        whyChooseSection: {},
        isLoading: false,

    },
    filters: {
        subStr: function (string) {
            if (string.length > 150)
                return string.substring(0, 300) + '...';
            else
                return string;
        }
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },
        getPagecontent: function () {
            var self = this;
            // self.isLoading = true;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }
                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var Page = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/02 Home/02 Home/02 Home.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(Page, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            var res = response.data;
                            if (res.area_List) {
                                var BannerSection = self.pluck(
                                    "Banner_Section",
                                    res.area_List
                                );
                                self.bannerSection = self.getAllMapData(
                                    BannerSection[0].component
                                );

                                var LoginSection = self.pluck(
                                    "Login_Section",
                                    res.area_List
                                );
                                self.loginSection = self.getAllMapData(
                                    LoginSection[0].component
                                );

                                var AboutSection = self.pluck(
                                    "About_Section",
                                    res.area_List
                                );
                                self.aboutSection = self.getAllMapData(
                                    AboutSection[0].component
                                );

                                var WhyChooseSection = self.pluck(
                                    "Why_Choose_Section",
                                    res.area_List
                                );
                                self.whyChooseSection = self.getAllMapData(
                                    WhyChooseSection[0].component
                                );
                                setTimeout(() => {
                                    owlCarouselFunction();
                                }, 100);
                                // self.isLoading = false;
                                self.stopLoader();
                            }
                        }).catch(function (error) {
                            // self.isLoading = false;
                            self.stopLoader();
                        });

                    }

                });

            });

        },
        // getMasterPagecontent: function () {
        //     var self = this;
        //     // self.isLoading = true;
        //     axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
        //         var agencyFolderName = '';
        //         var agy;
        //         try {
        //             response.data.forEach(function (agent, agentIndex) {
        //                 agent.domain.forEach(function (dom, domIndex) {
        //                     if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
        //                         agy = agent;
        //                         agencyFolderName = agy.agencyFolderName;
        //                     }
        //                 });
        //             });
        //         } catch (err) {
        //             console.log(err);
        //         }
        //         agy.registeredUsers.forEach(function (agyCode, domIndex) {
        //             if (domIndex == 0) {
        //                 var huburl = HubServiceUrls.hubConnection.cmsUrl;
        //                 var portno = HubServiceUrls.hubConnection.ipAddress;
        //                 var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
        //                 var Page = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/01 Master/01 Master/01 Master.ftl';
        //                 var agencyCode = agyCode;
        //                 self.agencyCode = agencyCode;
        //                 axios.get(Page, {
        //                     headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
        //                 }).then(function (response) {
        //                     var res = response.data;
        //                     if (res.area_List) {
        //                         var TopSection = self.pluck(
        //                             "Top_Section",
        //                             res.area_List
        //                         );
        //                         self.topSection = self.getAllMapData(
        //                             TopSection[0].component
        //                         );

        //                         var MainSection = self.pluck(
        //                             "Main_Section",
        //                             res.area_List
        //                         );
        //                         self.mainSection = self.getAllMapData(
        //                             MainSection[0].component
        //                         );

        //                         var FooterSection = self.pluck(
        //                             "Footer_Section",
        //                             res.area_List
        //                         );
        //                         self.footerSection = self.getAllMapData(
        //                             FooterSection[0].component
        //                         );

        //                         var CopyRightSection = self.pluck(
        //                             "Copy_Right_Section",
        //                             res.area_List
        //                         );
        //                         self.copyRightSection = self.getAllMapData(
        //                             CopyRightSection[0].component
        //                         );
        //                         // self.isLoading = false;

        //                     }
        //                 }).catch(function (error) {
        //                     // self.isLoading = false;
        //                     self.stopLoader();
        //                 });

        //             }

        //         });

        //     });

        // },
        stopLoader: function () {
            // var div = document.getElementById("preloader");
            // div.style.display = "none";
            $('#preloader').delay(50).fadeOut(250);
        }
    },
    mounted: function () {
        // sessionStorage.active_er = 1;
        // this.getMasterPagecontent();
        this.getPagecontent();
        try {
            if ($.cookie) {
                if ($.cookie("remember")) {
                    var cookieDetails = JSON.parse(atob($.cookie("remember")));
                    document.getElementById("rememberMe").setAttribute("checked", "checked");
                    document.getElementById("txtAgencyCode").value = cookieDetails.agencyCode;
                    document.getElementById("txtUname").value = cookieDetails.userName;
                } else {
                    document.getElementById("rememberMe").removeAttribute("checked");
                    document.getElementById("txtAgencyCode").value = "";
                    document.getElementById("txtUname").value = "";
                }
            }
        } catch (error) { }

    },


});
function owlCarouselFunction() {
    $("#owl-demo-1").owlCarousel({
        autoplay: true,
        autoPlay: 8000,
        autoplayHoverPause: true,
        stopOnHover: false,
        items: 1,
        margin: 10,
        lazyLoad: true,
        navigation: true,
        itemsDesktop: [1199, 1],
        itemsDesktopSmall: [979, 1],
        itemsTablet: [768, 1],
    });

    $(".owl-prev").html('<i class="fa   fa-angle-left"></i>');
    $(".owl-next").html('<i class="fa  fa-angle-right"></i>');
}