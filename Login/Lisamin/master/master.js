var HeaderComponent = Vue.component('headeritem', {
    template: ` 
    <div>
    <header>
    <div class="container">
        <div class="row">
            <div class="col-md-offset-4  col-md-8">
                <div class="contact-info">
                    <ul>
                        <li> <i class="fa fa-phone"></i> <a :href="'tel:'+topSection.Phone_Number">
                                {{topSection.Phone_Text_Label}} {{topSection.Phone_Number}} </a></li>
                        <li> <i class="fa fa-envelope"></i> <a :href="'mailto:'+topSection.Email_Address">
                                {{topSection.Email_Address}}</a> </li>
                    </ul>
                </div>

                <div class="social-media">
                    <ul>
                        <li v-for="social in topSection.Social_Media_List"><a :href="social.Url"
                                target="_blank"><i :class="social.Item_Icon"></i></a></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</header>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-11 col-md-offset-1">
                <nav id="navbar-main" class="navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#myNavbar">
                            <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                                class="icon-bar"></span> </button>
                        <a class="navbar-brand" href="index.html"><img :src="mainSection.Logo_198x141px"
                                alt=""></a> </div>
                    <div class="collapse navbar-collapse navbar-right" id="myNavbar">
                        <ul class="nav navbar-nav">
                            <li :class="activePageName=='Home' ? 'active' :''"><a href="index.html">{{mainSection.Home_Label}}</a></li>
                            <li :class="activePageName=='about' ? 'active' :''"><a href="about.html">{{mainSection.About_Us_Label}}</a></li>
                            <li :class="activePageName=='contact' ? 'active' :''"><a href="contact.html">{{mainSection.Contact_Us_Label}}</a></li>
                        </ul>
                    </div>
                </nav>
                <!--
                <div class="R-btn">
                    <a href="#">{{mainSection.Registration_Label}}</a>
                </div>
                -->
            </div>
        </div>
    </div>
</section>
    </div>
    `,
    data() {
        return {

            agencyCode: '',
            topSection: {},
            footerSection: {},
            copyRightSection: {},
            mainSection: {},
            activePageName: '',

        }
    },
    methods: {
        getPageheader: function () {
            var self = this;
            // self.isLoading = true;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) {
                    console.log(err);
                }
                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var Page = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/01 Master/01 Master/01 Master.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(Page, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            var res = response.data;
                            if (res.area_List) {
                                var TopSection = self.pluck(
                                    "Top_Section",
                                    res.area_List
                                );
                                self.topSection = self.getAllMapData(
                                    TopSection[0].component
                                );

                                var MainSection = self.pluck(
                                    "Main_Section",
                                    res.area_List
                                );
                                self.mainSection = self.getAllMapData(
                                    MainSection[0].component
                                );

                                var FooterSection = self.pluck(
                                    "Footer_Section",
                                    res.area_List
                                );
                                self.footerSection = self.getAllMapData(
                                    FooterSection[0].component
                                );

                                var CopyRightSection = self.pluck(
                                    "Copy_Right_Section",
                                    res.area_List
                                );
                                self.copyRightSection = self.getAllMapData(
                                    CopyRightSection[0].component
                                );
                                // self.isLoading = false;

                            }
                        }).catch(function (error) {
                            // self.isLoading = false;
                            self.stopLoader();
                        });

                    }

                });

            });

        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },
        stopLoader: function () {
            // var div = document.getElementById("preloader");
            // div.style.display = "none";
            $('#preloader').delay(50).fadeOut(250);
        },
        headerData: function () {
            var self = this;
            var url = window.location.href;
            if (url.includes('index.html')) {
                this.activePageName = "Home";
            } else if (url.includes('about.html')) {
                this.activePageName = "about";
            } else if (url.includes('contact.html')) {
                this.activePageName = "contact";
            } else {
                this.activePageName = "Home";
            }
        }
    },
    mounted: function () {
        this.getPageheader();
    },
    updated: function () {
        this.headerData();
    }
})
var headerinstance = new Vue({
    el: '#headarea',
    name: 'headarea',
    data() {
        return {
            key: '',
            content: null,
            getdata: true
        }

    },

});
Vue.component('footeritem', {
    template: `
    <div>
    <footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4 ">
                <div class="footer-contact">
                    <i class="fa fa-map-marker"></i>
                    <div class="contact-d">
                        <h3>{{footerSection.Address_Label}}</h3>
                        <p>{{footerSection.Address_Content}} </p>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-sm-4 v-border">
                <div class="footer-contact">
                    <i class="fa fa-envelope"></i>
                    <div class="contact-d">
                        <h3>{{footerSection.Email_Label}}</h3>
                        <a :href="'mailto:'+topSection.Email_Address">{{topSection.Email_Address}}</a>
                    </div>
                </div>
            </div>


            <div class="col-md-4 col-sm-4 v-border">
                <div class="footer-contact">
                    <i class="fa fa-phone"></i>
                    <div class="contact-d">
                        <h3>{{footerSection.Phone_Label}}</h3>
                        <a :href="'tel:'+topSection.Phone_Number">{{topSection.Phone_Number}}</a>
                    </div>
                </div>
            </div>

        </div>
        <div class="link-media">
            <div class="row">
                <div class="col-md-8 col-sm-8">
                    <div class="quick-link">
                        <ul>
                            <li><a href="index.html">{{mainSection.Home_Label}}</a></li>
                            <li><a href="about.html">{{mainSection.About_Us_Label}}</a></li>
                            <li><a href="contact.html">{{mainSection.Contact_Us_Label}}</a></li>
                    <!--
                            <li><a href="#">{{footerSection.Disclaimer_Label}} </a></li>
                            <li><a href="#">{{footerSection.Privacy_Policy_Label}}</a></li>
                    -->
                        </ul>
                    </div>
                </div>


                <div class="col-md-4 col-sm-4">
                    <div class="footer-social">
                        <ul>
                            <li v-for="social in topSection.Social_Media_List"><a :href="social.Url"
                                    target="_blank"><i :class="social.Item_Icon"></i></a></li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>

<!--
        <div class="newsletter">
            <div class="row">
                <div class="col-md-5 col-sm-5">
                    <h2>{{footerSection.Subscribe_Newsletter_Label}}</h2>
                </div>

                <div class="col-md-7 col-sm-7">
                    <input type="email" id="email" v-model="newsltremail" name="email" :placeholder="footerSection.Email_Placeholder">
                    <a href="javascript:void(0);" class="btn" @click="sendnewsletter()">{{footerSection.Subscribe_Label}}</a>
                </div>
            </div>
        </div>
-->
    </div>
</footer>

<section class="copy-right">
<div class="container">
    <div class="row">
        <div class="col-md-8 col-sm-8">
            <p>{{copyRightSection.Copy_Right_Description}}</p>
        </div>

        <div class="col-md-4 col-sm-4">
            <div class="powered">
                <p>{{copyRightSection.Powered_By_Label}}</p>
                <a href="#"><img src="images/oneview-logo.png" alt="logo"> </a>
            </div>
        </div>
    </div>
</div>
</section>

</div>
    `,
    data() {
        return {
            commonStore: vueCommonStore.state,
            agencyCode: '',
            newsltremail: "",
            mainSection: {},
            topSection: {},
            footerSection: {},
            copyRightSection: {},
        }
    },
    methods: {
        getPagefooter: function () {
            var self = this;
            // self.isLoading = true;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) {
                    console.log(err);
                }
                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var Page = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/01 Master/01 Master/01 Master.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(Page, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            var res = response.data;
                            if (res.area_List) {
                                var TopSection = self.pluck(
                                    "Top_Section",
                                    res.area_List
                                );
                                self.topSection = self.getAllMapData(
                                    TopSection[0].component
                                );

                                var MainSection = self.pluck(
                                    "Main_Section",
                                    res.area_List
                                );
                                self.mainSection = self.getAllMapData(
                                    MainSection[0].component
                                );

                                var FooterSection = self.pluck(
                                    "Footer_Section",
                                    res.area_List
                                );
                                self.footerSection = self.getAllMapData(
                                    FooterSection[0].component
                                );

                                var CopyRightSection = self.pluck(
                                    "Copy_Right_Section",
                                    res.area_List
                                );
                                self.copyRightSection = self.getAllMapData(
                                    CopyRightSection[0].component
                                );
                                // self.isLoading = false;

                            }
                        }).catch(function (error) {
                            // self.isLoading = false;
                            console.log(error);
                            self.stopLoader();
                        });

                    }

                });

            });

        },
        stopLoader: function () {
            // var div = document.getElementById("preloader");
            // div.style.display = "none";
            $('#preloader').delay(50).fadeOut(250);
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        },
        sendnewsletter: async function () {
            if (!this.newsltremail) {
                alertify.alert('Alert', 'Email Id required.');
                return false;;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.newsltremail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.');
                return false;
            }
            else {
                var agencyCode = this.agencyCode
                var filterValue = "type='Newsletter' AND keyword1='" + this.newsltremail + "'";
                var allDBData = await this.getDbData4Table(agencyCode, filterValue, "date1");

                if (allDBData != undefined && allDBData.length > 0) {
                    alertify.alert('Alert', 'Email address already enabled.').set('closable', false);
                    return false;
                }
                else {
                    var logourl = window.location.origin + "/Login/Lisamin/website-informations/logo/logo.png"
                    var postData = {
                        type: "UserAddedRequest",
                        fromEmail: this.topSection.Email_Address || this.commonStore.fallBackEmail,
                        // fromEmail: this.commonStore.fallBackEmail,
                        toEmails: Array.isArray(this.newsltremail) ? this.newsltremail : [this.newsltremail],
                        logo: logourl || "",
                        agencyName: "Lisamin Travels & Tourism",
                        agencyAddress: this.footerSection.Address_Content || "",
                        personName: this.newsltremail.split("@")[0],
                        primaryColor: "#ffffff",
                        secondaryColor: "#f46c00"
                    };
                    var agencyCode = this.agencyCode
                    let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                    let insertSubscibeData = {
                        type: "Newsletter",
                        date1: requestedDate,
                        keyword1: this.newsltremail,
                        keyword2: "Subscribe Newsletter",
                        nodeCode: agencyCode
                    };
                    let responseObject = await this.cmsRequestData("POST", "cms/data", insertSubscibeData, null);
                    try {
                        let insertID = Number(responseObject);
                        var self = this;
                        mailUrl = self.commonStore.hubUrls.emailServices.emailApi;
                        sendMailService(mailUrl, postData);
                        this.newsltremail = '';
                        alertify.alert('Newsletter', 'Thank you for subscribing !');
                    } catch (e) {

                    }
                }

            }
        },
        async getDbData4Table(agencyCode, extraFilter, sortField) {

            var allDBData = [];
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            var cmsURL = huburl + portno + '/cms/data/search/byQuery';
            var queryStr = "select * from cms_forms_data where nodeCode = '" + agencyCode + "'";
            if (extraFilter != undefined && extraFilter != '') {
                queryStr = queryStr + " AND " + extraFilter;
            }
            var requestObject = {
                query: queryStr,
                sortField: sortField,
                from: 0,
                orderBy: "desc"
            };
            let responseObject = await this.cmsRequestData("POST", "cms/data/search/byQuery", requestObject, null);
            if (responseObject != undefined && responseObject.data != undefined) {
                allDBData = responseObject.data;
            }
            return allDBData;

        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },

    },
    mounted: function () {
        this.getPagefooter();
    },

})
Vue.component('whychoose', {
    template: `
    <div class="container">
        <div class="why-choose">
            <div class="title">
                <h1>{{whyChooseSection.Choose_Header_Label}}</h1>
                <img src="images/title-pattern.png" alt="pattern">
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="why-lisamin">
                        <div class="icon">
                            <svg width="45px" height="45px" viewBox="0 0 45 45" enable-background="new 0 0 45 45"
                                xml:space="preserve">
                                <g>
                                    <g>
                                        <path d="M40.23,14.066c-0.281-0.449-0.722-0.763-1.238-0.881l-2.726-0.625l1.259-0.353c1.042-0.291,1.67-1.403,1.378-2.447
        c-0.292-1.041-1.405-1.668-2.45-1.376l-5.866,1.64c-0.152-0.277-0.37-0.516-0.632-0.692c1.191-2.213,2.906-5.969,2.906-7.585
        c0-0.395-0.373-0.699-0.76-0.621c-2.328,0.47-4.499,0.687-6.633,0.663c-2.195-0.024-4.925-0.296-6.641-0.662
        c-0.53-0.113-0.956,0.479-0.676,0.945c0.418,0.696,0.674,1.433,0.815,2.175c-2.153,0.065-4.386-0.195-6.077-1.039
        c-0.455-0.228-0.993,0.17-0.909,0.671c0.532,3.187,1.604,5.054,2.679,6.465h-0.024c-1.096,0-1.987,0.891-1.987,1.985
        c0,1.007,0.754,1.841,1.728,1.969c-0.709,1.188-1.723,2.313-2.788,3.496c-2.265,2.514-4.832,5.364-5.695,9.965
        c-0.414,2.201-0.4,4.262,0.041,6.123c0.412,1.737,1.195,3.31,2.328,4.675c1.504,1.811,3.571,3.216,6.143,4.176
        c2.317,0.865,4.97,1.323,7.673,1.323s5.356-0.458,7.673-1.323c2.572-0.96,4.639-2.365,6.144-4.176
        c1.133-1.365,1.916-2.938,2.328-4.675c0.439-1.861,0.453-3.922,0.041-6.123c-0.864-4.601-3.431-7.451-5.695-9.965
        c-0.769-0.854-1.5-1.667-2.113-2.493l7.646,1.755c0.811,0.186,1.674-0.168,2.121-0.867C40.632,15.548,40.635,14.71,40.23,14.066z
         M25.454,3.057c1.953,0.021,3.93-0.149,6.018-0.521c-0.383,1.563-1.612,4.345-2.778,6.463c-0.809,0.062-1.482,0.606-1.732,1.346
        h-1.762c-0.039-0.085-0.083-0.18-0.133-0.283c-0.544-1.156-1.678-3.563-1.678-5.61c0-0.394-0.371-0.698-0.758-0.621
        c-0.738,0.147-1.55,0.267-2.395,0.342c-0.089-0.529-0.226-1.059-0.426-1.579C21.514,2.855,23.66,3.037,25.454,3.057z
         M28.133,10.937c0.022-0.375,0.329-0.677,0.711-0.677c0.396,0,0.719,0.322,0.719,0.718v2.704c0,0.396-0.322,0.718-0.719,0.718
        s-0.719-0.322-0.719-0.718v-2.704h0.043C28.168,10.978,28.148,10.932,28.133,10.937z M13.459,4.813
        c1.586,0.541,3.536,0.78,5.643,0.706l1.257-0.079c0.593-0.055,1.194-0.133,1.801-0.234c0.197,1.96,1.094,3.975,1.639,5.139h-7.521
        c-0.014-0.022-0.029-0.044-0.046-0.064C15.171,9.004,14.094,7.529,13.459,4.813z M31.625,18.642c2.153,2.39,4.594,5.1,5.392,9.351
        c0.733,3.912,0.028,7.194-2.099,9.754c-2.622,3.157-7.422,5.041-12.84,5.041c-5.417,0-10.218-1.884-12.84-5.04
        c-2.126-2.561-2.833-5.843-2.098-9.755c0.798-4.251,3.238-6.961,5.391-9.351c1.287-1.43,2.503-2.781,3.283-4.327h1.549
        c0.351,0,0.634-0.283,0.634-0.633c0-0.351-0.284-0.634-0.634-0.634h-2.728c-0.396,0-0.719-0.322-0.719-0.719
        c0-0.396,0.322-0.718,0.719-0.718h12.221v1.437h-6.47c-0.351,0-0.634,0.283-0.634,0.634c0,0.35,0.284,0.633,0.634,0.633h6.574
        c0.266,0.785,1.009,1.353,1.883,1.353c0.102,0,0.2-0.008,0.297-0.022C29.85,16.667,30.712,17.628,31.625,18.642z M30.832,11.272
        l5.963-1.668c0.378-0.105,0.781,0.121,0.887,0.498c0.105,0.378-0.121,0.78-0.499,0.885l-3.408,0.953
        c-0.027,0.008-0.054,0.019-0.08,0.029l-2.862-0.657V11.272z M39.247,15.281c-0.089,0.387-0.475,0.628-0.861,0.539l-7.594-1.743
        c0.026-0.128,0.04-0.26,0.04-0.396v-1.068l7.875,1.809C39.09,14.508,39.335,14.899,39.247,15.281z M23.728,19.85v-1.099
        c0-0.909-0.74-1.647-1.649-1.647c-0.909,0-1.649,0.738-1.649,1.647v1.183c-2.351,0.523-4.06,2.611-4.06,5.069
        c0,2.865,2.333,5.196,5.201,5.196h1.015c1.049,0,1.903,0.853,1.903,1.9c0,1.049-0.854,1.901-1.903,1.901h-1.522
        c-0.77,0-1.396-0.626-1.396-1.395c0-0.908-0.74-1.647-1.649-1.647s-1.649,0.739-1.649,1.647c0,2.371,1.771,4.337,4.06,4.646v1.099
        c0,0.909,0.74,1.648,1.649,1.648c0.909,0,1.648-0.739,1.648-1.648V37.17c2.352-0.524,4.061-2.612,4.061-5.07
        c0-2.865-2.334-5.195-5.202-5.195h-1.015c-1.049,0-1.903-0.854-1.903-1.901c0-1.049,0.854-1.901,1.903-1.901h1.523
        c0.77,0,1.395,0.625,1.395,1.395v1.014c0,0.908,0.74,1.647,1.649,1.647s1.649-0.739,1.649-1.647v-1.014
        C27.787,22.125,26.018,20.159,23.728,19.85z M26.519,25.51c0,0.21-0.171,0.38-0.381,0.38s-0.38-0.17-0.38-0.38v-1.014
        c0-1.468-1.195-2.661-2.664-2.661h-1.523c-1.749,0-3.171,1.421-3.171,3.168s1.423,3.168,3.171,3.168h1.015
        c2.168,0,3.933,1.763,3.933,3.929c0,2.005-1.503,3.683-3.495,3.904c-0.321,0.035-0.564,0.307-0.564,0.629v1.719
        c0,0.21-0.171,0.381-0.381,0.381s-0.38-0.171-0.38-0.381v-1.689c0-0.35-0.284-0.634-0.634-0.634c-1.889,0-3.425-1.535-3.425-3.421
        c0-0.21,0.171-0.381,0.38-0.381c0.21,0,0.381,0.171,0.381,0.381c0,1.467,1.195,2.661,2.664,2.661h1.522
        c1.748,0,3.172-1.422,3.172-3.169s-1.424-3.168-3.172-3.168h-1.015c-2.168,0-3.933-1.763-3.933-3.929
        c0-2.005,1.503-3.683,3.495-3.904c0.321-0.035,0.564-0.307,0.564-0.63v-1.718c0-0.21,0.17-0.38,0.38-0.38s0.381,0.17,0.381,0.38
        v1.689c0,0.35,0.284,0.634,0.635,0.634c1.889,0,3.425,1.535,3.425,3.422V25.51z" />
                                    </g>
                                </g>
                            </svg>
                        </div>
                        <h3>{{whyChooseSection.Rate_Label}}</h3>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="why-lisamin">
                        <div class="icon">
                            <svg enable-background="new 0 0 64 64" viewBox="0 0 64 64" width="512">
                                <path
                                    d="m56 40.10529v-28.10529c0-2.75684-2.24316-5-5-5h-2v-2c0-1.6543-1.3457-3-3-3s-3 1.3457-3 3v2h-5v-2c0-1.6543-1.3457-3-3-3s-3 1.3457-3 3v2h-6v-2c0-1.6543-1.3457-3-3-3s-3 1.3457-3 3v2h-5v-2c0-1.6543-1.3457-3-3-3s-3 1.3457-3 3v2h-2c-2.75684 0-5 2.24316-5 5v40c0 2.75684 2.24316 5 5 5h33.62347c2.07868 3.58081 5.94617 6 10.37653 6 6.61719 0 12-5.38281 12-12 0-4.83142-2.87561-8.99408-7-10.89471zm-11-35.10529c0-.55176.44824-1 1-1s1 .44824 1 1v6c0 .55176-.44824 1-1 1s-1-.44824-1-1zm-11 0c0-.55176.44824-1 1-1s1 .44824 1 1v6c0 .55176-.44824 1-1 1s-1-.44824-1-1zm-12 0c0-.55176.44824-1 1-1s1 .44824 1 1v6c0 .55176-.44824 1-1 1s-1-.44824-1-1zm-11 0c0-.55176.44824-1 1-1s1 .44824 1 1v6c0 .55176-.44824 1-1 1s-1-.44824-1-1zm-4 4h2v2c0 1.6543 1.3457 3 3 3s3-1.3457 3-3v-2h5v2c0 1.6543 1.3457 3 3 3s3-1.3457 3-3v-2h6v2c0 1.6543 1.3457 3 3 3s3-1.3457 3-3v-2h5v2c0 1.6543 1.3457 3 3 3s3-1.3457 3-3v-2h2c1.6543 0 3 1.3457 3 3v5h-50v-5c0-1.6543 1.3457-3 3-3zm0 46c-1.6543 0-3-1.3457-3-3v-33h50v20.39484c-.96082-.24866-1.96246-.39484-3-.39484-.6828 0-1.34808.07056-2 .1806v-5.1806c0-.55273-.44727-1-1-1h-6c-.55273 0-1 .44727-1 1v6c0 .55273.44727 1 1 1h2.38086c-3.23914 2.15106-5.38086 5.82843-5.38086 10 0 1.40411.25494 2.74664.70001 4zm40-16h-4v-4h4zm4 22c-5.51367 0-10-4.48633-10-10s4.48633-10 10-10 10 4.48633 10 10-4.48633 10-10 10z" />
                                <path
                                    d="m52 49.2774v-6.2774h-2v6.2774c-.59528.34644-1 .98413-1 1.7226 0 .10126.01526.19836.02979.29553l-3.65479 2.92322 1.25 1.5625 3.65161-2.92133c.22492.08759.46753.14008.72339.14008 1.10455 0 2-.89545 2-2 0-.73846-.40472-1.37616-1-1.7226z" />
                                <path
                                    d="m15 22h-6c-.55273 0-1 .44727-1 1v6c0 .55273.44727 1 1 1h6c.55273 0 1-.44727 1-1v-6c0-.55273-.44727-1-1-1zm-1 6h-4v-4h4z" />
                                <path
                                    d="m26 22h-6c-.55273 0-1 .44727-1 1v6c0 .55273.44727 1 1 1h6c.55273 0 1-.44727 1-1v-6c0-.55273-.44727-1-1-1zm-1 6h-4v-4h4z" />
                                <path
                                    d="m37 22h-6c-.55273 0-1 .44727-1 1v6c0 .55273.44727 1 1 1h6c.55273 0 1-.44727 1-1v-6c0-.55273-.44727-1-1-1zm-1 6h-4v-4h4z" />
                                <path
                                    d="m42 30h6c.55273 0 1-.44727 1-1v-6c0-.55273-.44727-1-1-1h-6c-.55273 0-1 .44727-1 1v6c0 .55273.44727 1 1 1zm1-6h4v4h-4z" />
                                <path
                                    d="m15 33h-6c-.55273 0-1 .44727-1 1v6c0 .55273.44727 1 1 1h6c.55273 0 1-.44727 1-1v-6c0-.55273-.44727-1-1-1zm-1 6h-4v-4h4z" />
                                <path
                                    d="m26 33h-6c-.55273 0-1 .44727-1 1v6c0 .55273.44727 1 1 1h6c.55273 0 1-.44727 1-1v-6c0-.55273-.44727-1-1-1zm-1 6h-4v-4h4z" />
                                <path
                                    d="m37 33h-6c-.55273 0-1 .44727-1 1v6c0 .55273.44727 1 1 1h6c.55273 0 1-.44727 1-1v-6c0-.55273-.44727-1-1-1zm-1 6h-4v-4h4z" />
                                <path
                                    d="m15 44h-6c-.55273 0-1 .44727-1 1v6c0 .55273.44727 1 1 1h6c.55273 0 1-.44727 1-1v-6c0-.55273-.44727-1-1-1zm-1 6h-4v-4h4z" />
                                <path
                                    d="m26 44h-6c-.55273 0-1 .44727-1 1v6c0 .55273.44727 1 1 1h6c.55273 0 1-.44727 1-1v-6c0-.55273-.44727-1-1-1zm-1 6h-4v-4h4z" />
                                <path
                                    d="m37 44h-6c-.55273 0-1 .44727-1 1v6c0 .55273.44727 1 1 1h6c.55273 0 1-.44727 1-1v-6c0-.55273-.44727-1-1-1zm-1 6h-4v-4h4z" />
                            </svg>
                        </div>
                        <h3>{{whyChooseSection.Booking_Label}}</h3>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="why-lisamin">
                        <div class="icon">
                            <svg enable-background="new 0 0 512 512" viewBox="0 0 512 512" width="512">
                                <g>
                                    <path
                                        d="m494.398 381.841h-38.968v-26.845c0-4.142-3.357-7.5-7.5-7.5h-32.906v-19.346h32.906c4.143 0 7.5-3.358 7.5-7.5 0-100.566-74.827-183.971-171.728-197.497v-18.249h5.127c13.883 0 25.178-11.294 25.178-25.178s-11.295-25.178-25.178-25.178h-65.66c-13.883 0-25.178 11.294-25.178 25.178s11.295 25.178 25.178 25.178h5.127v18.249c-96.9 13.527-171.728 96.931-171.728 197.497 0 4.142 3.357 7.5 7.5 7.5h32.906v19.346h-32.905c-4.143 0-7.5 3.358-7.5 7.5v26.845h-38.967c-9.706 0-17.602 7.896-17.602 17.602v40.407c0 9.706 7.896 17.602 17.602 17.602h476.797c9.705 0 17.602-7.896 17.602-17.602v-40.407c-.001-9.706-7.897-17.602-17.603-17.602zm-281.406-302.114c0-5.612 4.565-10.178 10.178-10.178h65.66c5.612 0 10.178 4.566 10.178 10.178s-4.565 10.178-10.178 10.178h-65.66c-5.612-.001-10.178-4.567-10.178-10.178zm30.305 25.177h25.406v16.732c-4.202-.266-8.435-.416-12.703-.416s-8.501.151-12.703.416zm-171.728 257.592h63.431c4.143 0 7.5-3.358 7.5-7.5s-3.357-7.5-7.5-7.5h-23.024v-19.346h240.917c4.143 0 7.5-3.358 7.5-7.5s-3.357-7.5-7.5-7.5h-281.173c3.951-98.229 85.098-176.93 184.28-176.93s180.329 78.701 184.28 176.931h-52.352c-4.143 0-7.5 3.358-7.5 7.5s3.357 7.5 7.5 7.5h12.096v19.346h-230.003c-4.143 0-7.5 3.358-7.5 7.5s3.357 7.5 7.5 7.5h270.409v19.345h-368.861zm425.431 77.354c0 1.435-1.167 2.602-2.602 2.602h-476.796c-1.435 0-2.602-1.167-2.602-2.602v-40.407c0-1.435 1.167-2.602 2.602-2.602h476.797c1.435 0 2.602 1.167 2.602 2.602v40.407z" />
                                </g>
                            </svg>
                        </div>
                        <h3>{{whyChooseSection.Hotel_Label}}
                        </h3>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="why-lisamin">
                        <div class="icon">
                            <svg enable-background="new 0 0 512 512" viewBox="-38 0 512 512.00142">
                                <g id="surface1">
                                    <path
                                        d="M 435.488281 138.917969 L 435.472656 138.519531 C 435.25 133.601562 435.101562 128.398438 435.011719 122.609375 C 434.59375 94.378906 412.152344 71.027344 383.917969 69.449219 C 325.050781 66.164062 279.511719 46.96875 240.601562 9.042969 L 240.269531 8.726562 C 227.578125 -2.910156 208.433594 -2.910156 195.738281 8.726562 L 195.40625 9.042969 C 156.496094 46.96875 110.957031 66.164062 52.089844 69.453125 C 23.859375 71.027344 1.414062 94.378906 0.996094 122.613281 C 0.910156 128.363281 0.757812 133.566406 0.535156 138.519531 L 0.511719 139.445312 C -0.632812 199.472656 -2.054688 274.179688 22.9375 341.988281 C 36.679688 379.277344 57.492188 411.691406 84.792969 438.335938 C 115.886719 468.679688 156.613281 492.769531 205.839844 509.933594 C 207.441406 510.492188 209.105469 510.945312 210.800781 511.285156 C 213.191406 511.761719 215.597656 512 218.003906 512 C 220.410156 512 222.820312 511.761719 225.207031 511.285156 C 226.902344 510.945312 228.578125 510.488281 230.1875 509.925781 C 279.355469 492.730469 320.039062 468.628906 351.105469 438.289062 C 378.394531 411.636719 399.207031 379.214844 412.960938 341.917969 C 438.046875 273.90625 436.628906 199.058594 435.488281 138.917969 Z M 384.773438 331.523438 C 358.414062 402.992188 304.605469 452.074219 220.273438 481.566406 C 219.972656 481.667969 219.652344 481.757812 219.320312 481.824219 C 218.449219 481.996094 217.5625 481.996094 216.679688 481.820312 C 216.351562 481.753906 216.03125 481.667969 215.734375 481.566406 C 131.3125 452.128906 77.46875 403.074219 51.128906 331.601562 C 28.09375 269.097656 29.398438 200.519531 30.550781 140.019531 L 30.558594 139.683594 C 30.792969 134.484375 30.949219 129.039062 31.035156 123.054688 C 31.222656 110.519531 41.207031 100.148438 53.765625 99.449219 C 87.078125 97.589844 116.34375 91.152344 143.234375 79.769531 C 170.089844 68.402344 193.941406 52.378906 216.144531 30.785156 C 217.273438 29.832031 218.738281 29.828125 219.863281 30.785156 C 242.070312 52.378906 265.921875 68.402344 292.773438 79.769531 C 319.664062 91.152344 348.929688 97.589844 382.246094 99.449219 C 394.804688 100.148438 404.789062 110.519531 404.972656 123.058594 C 405.0625 129.074219 405.21875 134.519531 405.453125 139.683594 C 406.601562 200.253906 407.875 268.886719 384.773438 331.523438 Z M 384.773438 331.523438 "
                                        style=" stroke:none;fill-rule:nonzero;fill-opacity:1;" />
                                    <path
                                        d="M 217.996094 128.410156 C 147.636719 128.410156 90.398438 185.652344 90.398438 256.007812 C 90.398438 326.367188 147.636719 383.609375 217.996094 383.609375 C 288.351562 383.609375 345.59375 326.367188 345.59375 256.007812 C 345.59375 185.652344 288.351562 128.410156 217.996094 128.410156 Z M 217.996094 353.5625 C 164.203125 353.5625 120.441406 309.800781 120.441406 256.007812 C 120.441406 202.214844 164.203125 158.453125 217.996094 158.453125 C 271.785156 158.453125 315.546875 202.214844 315.546875 256.007812 C 315.546875 309.800781 271.785156 353.5625 217.996094 353.5625 Z M 217.996094 353.5625 "
                                        style=" stroke:none;fill-rule:nonzero;fill-opacity:1;" />
                                    <path
                                        d="M 254.667969 216.394531 L 195.402344 275.660156 L 179.316406 259.574219 C 173.449219 253.707031 163.9375 253.707031 158.070312 259.574219 C 152.207031 265.441406 152.207031 274.953125 158.070312 280.816406 L 184.78125 307.527344 C 187.714844 310.460938 191.558594 311.925781 195.402344 311.925781 C 199.246094 311.925781 203.089844 310.460938 206.023438 307.527344 L 275.914062 237.636719 C 281.777344 231.769531 281.777344 222.257812 275.914062 216.394531 C 270.046875 210.523438 260.535156 210.523438 254.667969 216.394531 Z M 254.667969 216.394531 "
                                        style=" stroke:none;fill-rule:nonzero;fill-opacity:1;" />
                                </g>
                            </svg>
                        </div>
                        <h3>{{whyChooseSection.Booking_Resource_Label}}</h3>
                    </div>
                </div>

            </div>
        </div>
    </div>
    `,
    data() {
        return {
            commonStore: vueCommonStore.state,
            agencyCode: '',
            whyChooseSection: {},
        }
    },
    methods: {
        getPagecontent: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }
                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var Page = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/02 Home/02 Home/02 Home.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(Page, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            var res = response.data;
                            if (res.area_List) {
                                // var BannerSection = self.pluck(
                                //     "Banner_Section",
                                //     res.area_List
                                // );
                                // self.bannerSection = self.getAllMapData(
                                //     BannerSection[0].component
                                // );

                                // var LoginSection = self.pluck(
                                //     "Login_Section",
                                //     res.area_List
                                // );
                                // self.loginSection = self.getAllMapData(
                                //     LoginSection[0].component
                                // );

                                // var AboutSection = self.pluck(
                                //     "About_Section",
                                //     res.area_List
                                // );
                                // self.aboutSection = self.getAllMapData(
                                //     AboutSection[0].component
                                // );

                                var WhyChooseSection = self.pluck(
                                    "Why_Choose_Section",
                                    res.area_List
                                );
                                self.whyChooseSection = self.getAllMapData(
                                    WhyChooseSection[0].component
                                );
                                // self.isLoading = false;
                                self.stopLoader();
                            }
                        }).catch(function (error) {
                            // self.isLoading = false;
                            self.stopLoader();
                        });

                    }

                });

            });

        },
        stopLoader: function () {
            // var div = document.getElementById("preloader");
            // div.style.display = "none";
            $('#preloader').delay(50).fadeOut(250);
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },

    },
    mounted: function () {
        this.getPagecontent();
    },

})
var footerinstance = new Vue({
    el: '#footerArea',
    name: 'footerArea',
    data() {
        return {
            key: 0,
            content: null,
            getdata: true
        }

    },
});
