var contact = new Vue({
    el: '#contactpage',
    name: 'contactpage',
    data: {
        commonStore: vueCommonStore.state,
        agencyCode: '',
        bannerSection: {},
        mainSection: {},
        contactArea: {},
        isLoading: false,
        cntemail: "",
        cntname: "",
        cntcontact: "",
        cntmessage: "",
        cntsubject: "",

    },
    filters: {
        subStr: function (string) {
            if (string.length > 150)
                return string.substring(0, 300) + '...';
            else
                return string;
        }
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },
        getPagecontent: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }
                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var Page = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/04 Contact Us/04 Contact Us/04 Contact Us.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(Page, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            var res = response.data;
                            if (res.area_List) {
                                var BannerSection = self.pluck(
                                    "Banner_Area",
                                    res.area_List
                                );
                                self.bannerSection = self.getAllMapData(
                                    BannerSection[0].component
                                );


                                var MainArea = self.pluck(
                                    "Main_Area",
                                    res.area_List
                                );
                                self.mainSection = self.getAllMapData(
                                    MainArea[0].component
                                );

                                var ContactArea = self.pluck(
                                    "Contact_Area",
                                    res.area_List
                                );
                                self.contactArea = self.getAllMapData(
                                    ContactArea[0].component
                                );

                                self.stopLoader();
                            }
                        }).catch(function (error) {
                            // self.isLoading = false;
                            self.stopLoader();
                        });

                    }

                });

            });

        },
        stopLoader: function () {
            // var div = document.getElementById("preloader");
            // div.style.display = "none";
            $('#preloader').delay(50).fadeOut(250);
        },
        sendcontactus: function () {
            if (!this.cntname) {
                alertify.alert('Alert', 'First name required.').set('closable', false);
                return false;
            }
            if (!this.cntemail) {
                alertify.alert('Alert', 'Email Id required.').set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.cntemail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                return false;
            }
            if (!this.cntcontact) {
                alertify.alert('Alert', 'Mobile number required.').set('closable', false);
                return false;
            }
            if (this.cntcontact.length < 8) {
                alertify.alert('Alert', 'Enter valid mobile number.').set('closable', false);
                return false;
            }
            if (!this.cntsubject) {
                alertify.alert('Alert', 'Subject required.').set('closable', false);
                return false;
            } if (!this.cntmessage) {
                alertify.alert('Alert', 'Message required.').set('closable', false);
                return false;
            } else {
                var self = this;
                var logourl = window.location.origin + "/Login/Lisamin/website-informations/logo/EmailLogo.png"
                // var frommail = this.contactArea.Email || this.commonStore.fallBackEmail;
                var frommail =  this.commonStore.fallBackEmail;
                var postData = {
                    type: "UserAddedRequest",
                    fromEmail: frommail,
                    toEmails: Array.isArray(this.cntemail) ? this.cntemail : [this.cntemail],
                    logo: logourl || "",
                    agencyName: "Lisamin Travels & Tourism",
                    agencyAddress: this.contactArea.Address || "",
                    personName: this.cntname || "",
                    primaryColor: "#ffffff",
                    secondaryColor: "#f46c00"
                };
                var agencyCode = this.agencyCode
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                let insertContactData = {
                    type: "Contact Us",
                    keyword1: this.cntname,
                    keyword2: this.cntsubject,
                    keyword3: this.cntemail,
                    keyword5: this.cntcontact,
                    text1: this.cntmessage,
                    date1: requestedDate,
                    nodeCode: agencyCode
                };
                let responseObject = this.cmsRequestData("POST", "cms/data", insertContactData, null);
                try {
                    let insertID = Number(responseObject);
                    var self = this;
                    mailUrl = self.commonStore.hubUrls.emailServices.emailApi;
                    sendMailService(mailUrl, postData);
                    this.cntemail = '';
                    this.cntname = '';
                    this.cntcontact = '';
                    this.cntmessage = '';
                    this.cntsubject = '';
                    alertify.alert('Contact us', 'Thank you for contacting us.We shall get back to you.');
                } catch (e) {

                }
            }
        },
        cmsRequestData: function (callMethod, urlParam, data, headerVal) {
            var vm = this;
            var huburl = vm.commonStore.hubUrls.hubConnection.cmsUrl;
            var portno = vm.commonStore.hubUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            return fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            }).then(function (response) {
                return response.json();
            });
        }
    },
    mounted: function () {
        // sessionStorage.active_er = 1;
        this.getPagecontent();
    },
});
