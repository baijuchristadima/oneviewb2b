var about = new Vue({
    el: '#aboutpage',
    name: 'aboutpage',
    data: {
        commonStore: vueCommonStore.state,
        agencyCode: '',
        bannerSection: {},
        mainSection: {},
        isLoading: false,

    },
    filters: {
        subStr: function (string) {
            if (string.length > 150)
                return string.substring(0, 300) + '...';
            else
                return string;
        }
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },
        getPagecontent: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }
                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var Page = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/03 About Us/03 About Us/03 About Us.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(Page, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            var res = response.data;
                            if (res.area_List) {
                                var BannerSection = self.pluck(
                                    "Banner_Area",
                                    res.area_List
                                );
                                self.bannerSection = self.getAllMapData(
                                    BannerSection[0].component
                                );


                                var MainArea = self.pluck(
                                    "Main_Area",
                                    res.area_List
                                );
                                self.mainSection = self.getAllMapData(
                                    MainArea[0].component
                                );
                                self.stopLoader();
                            }
                        }).catch(function (error) {
                            // self.isLoading = false;
                            self.stopLoader();
                        });

                    }

                });

            });

        },
        stopLoader: function () {
            // var div = document.getElementById("preloader");
            // div.style.display = "none";
            $('#preloader').delay(50).fadeOut(250);
        }
    },
    mounted: function () {
        // sessionStorage.active_er = 1;
        this.getPagecontent();
    },
});
