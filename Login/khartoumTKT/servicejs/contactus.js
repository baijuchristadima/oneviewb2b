var contact = new Vue({
    el: '#contactuspage',
    name: 'contactuspage',
    data: {
        commonStore: vueCommonStore.state,
        contact: {Phone_Number_1:''},
        cntname: '',
        cntemail: '',
        cntcontact: '',
        cntsubject:'',
        cntmessage: '',
        agencyCode: '',
        Follow:{Facebook_Link:''},
        notificationEmail: "",
        bannerArea:{},
        contactArea:{},
        isLoading:true,
        fullPage:true
    },
    methods: {
      
        getPagecontent: function () {
            var self = this;
            self.isLoading=true;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                                self.isLoading=false;
                            }
                        });
                        
                    });
                } catch (err) {self.isLoading=false; }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (sessionStorage.language) ? sessionStorage.language : "en";
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Contact Us/Contact Us/Contact Us.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;

                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            var data = response.data;

                            var Notification = pluck('Notifications', response.data.area_List);
                            var banner=pluck('Banner_Area', data.area_List);
                            self.bannerArea=getAllMapData(banner[0].component);
                            
                            if (Notification.length > 0) {
                                self.notificationEmail =pluckcom('Notification_Email', Notification[0].component);
                            }
                            var contact = pluck('Contact_Details', data.area_List);
                            self.contactArea=getAllMapData(contact [0].component);
                        }).catch(function (error) {
                    });
                    }

                });

            });

        },
        sendcontactus: function () {
            alertify.alert()
            .setting({
              'label':this.getValidationMsgByCode('M52'),
            //   'message': 'This dialog is : ' + (closable ? ' ' : ' not ') + 'closable.' ,
            //   'onok': function(){ alertify.success('Great');}
            })
                    if (!this.cntname) {
                alertify.alert(this.getValidationMsgByCode('M38'),this.getValidationMsgByCode('M01')).set('closable', false);
                return false;
            }
            if (!this.cntemail) {
                alertify.alert(this.getValidationMsgByCode('M38'),this.getValidationMsgByCode('M02')).set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray =
             this.cntemail.match(emailPat);
            if (matchArray == null) {
                alertify.alert(this.getValidationMsgByCode('M38'),this.getValidationMsgByCode('M03')).set('closable', false);
                return false;
            }
            if (!this.cntcontact) {
                alertify.alert(this.getValidationMsgByCode('M38'),this.getValidationMsgByCode('M19')).set('closable', false);
                return false;
            }
            if (this.cntcontact.length < 8) {
                alertify.alert(this.getValidationMsgByCode('M38'),this.getValidationMsgByCode('M20')).set('closable', false);
                return false;
            }
            if (!this.cntsubject) {
                alertify.alert(this.getValidationMsgByCode('M38'),this.getValidationMsgByCode('M21')).set('closable', false);
                return false;
            }if (!this.cntmessage) {
                alertify.alert(this.getValidationMsgByCode('M38'),this.getValidationMsgByCode('M22')).set('closable', false);
              
                                return false;
            } 
            else {
               var self = this;
                self.isLoading=true;
                logo=sessionStorage.getItem('logo');
                var frommail = this.notificationEmail || this.commonStore.fallBackEmail;
                var postData = {
                    type: "UserAddedRequest",
                    fromEmail: frommail,
                    toEmails: Array.isArray(this.cntemail) ? this.cntemail : [this.cntemail],
                    logo:logo || "",
                    agencyName: "Khartoum TKT",
                    agencyAddress: this.contactArea.Address || "",
                    personName: this.cntname || "",
                    primaryColor: "#ffffff",
                    secondaryColor: "#842452"
                };
                var agencyCode = this.agencyCode
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                let insertContactData = {
                    type: "Contact Us",
                    keyword1: this.cntname,
                    keyword2: this.cntsubject,
                    keyword3:this.cntemail,
                    keyword5: this.cntcontact,
                    text1: this.cntmessage,
                    date1: requestedDate,
                    nodeCode: agencyCode
                };
                let responseObject = this.cmsRequestData("POST", "cms/data", insertContactData, null);
                try {
                    let insertID = Number(responseObject);
                    var self = this;
                    mailUrl = self.commonStore.hubUrls.emailServices.emailApi;
                    sendMailService(mailUrl, postData);
                    this.cntemail = '';
                    this.cntname = '';
                    this.cntlastname = '';
                    this.cntcontact = '';
                    this.cntmessage = '';
                    this.cntsubject='';
                    this.isLoading=false;
                 alertify.alert(this.getValidationMsgByCode('M15'),this.getValidationMsgByCode('M23'))
                } catch (e) {
                    this.isLoading=false;
            
                }
            }
        },
           getValidationMsgByCode: function (code) {
            if (sessionStorage.validationItems !== undefined) {
                var validationList = JSON.parse(sessionStorage.validationItems);
                for (let validationItem of validationList.Validation_List) {
                    if (code === validationItem.Code) {
                        return validationItem.Message;
                    }
                }
            }
         },
        cmsRequestData: function (callMethod, urlParam, data, headerVal) {
            var vm = this;
            var huburl = vm.commonStore.hubUrls.hubConnection.cmsUrl;
            var portno = vm.commonStore.hubUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
              data = JSON.stringify(data);
            }
            return fetch(url, {
              method: callMethod, // *GET, POST, PUT, DELETE, etc.
              credentials: "same-origin", // include, *same-origin, omit
              headers: { 'Content-Type': 'application/json' },
              body: data, // body data type must match "Content-Type" header
            }).then(function (response) {
              return response.json();
            });
          }
    },
    mounted: function () {
        this.getPagecontent();
        sessionStorage.active_er=3; 
    },
});