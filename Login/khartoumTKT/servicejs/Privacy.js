var Privacy = new Vue({
    el: '#PrivacyandTerms',
    name: 'PrivacyandTerms',
    data: {
        commonStore: vueCommonStore.state,
        agencyCode: '',
        bannerArea:[],
        privacyList:[],
        isLoading:false,
        fullPage:true
    },
    methods: {
            getPagecontent: function () {
            var self = this;
            self.isLoading=true;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                                self.isLoading=false;
                            }
                        });
                    });
                } catch (err) {self.isLoading=false; }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (sessionStorage.language) ? sessionStorage.language : "en";
                        var cmsUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Privacy Policy Page/Privacy Policy Page/Privacy Policy Page.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(cmsUrl, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            var data = response.data;
                            var Terms = pluck('Main_Area', data.area_List);
                                self.privacyList = getAllMapData(Terms[0].component);
                            var banner=pluck('Banner_Area', data.area_List);
                            self.bannerArea = getAllMapData(banner[0].component);
                        }).catch(function (error) {
                            console.log('Error');
                        });
                    }

                });

            });

        }
    },
    mounted: function () {
        sessionStorage.active_er=5; 
        this.getPagecontent();
    },
});