var aboutus = new Vue({
    el: '#aboutuspage',
    name: 'aboutuspage',
    data: {
        commonStore: vueCommonStore.state,
        agencyCode: '',
        aboutArea: {},
        bannerArea: {},
        isLoading:false,
        fullPage:true
    },
    methods: {
        getPagecontent: function () {
            var self = this;
            self.isLoading=true;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                            var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                                self.isLoading=false;
                            }
                        });
                    });
                } catch (err) { self.isLoading=false;}

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (sessionStorage.language) ? sessionStorage.language : "en";
                        var about_Page = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/About Us/About Us/About Us.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(about_Page, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            var data = response.data;
                            var About = pluck('About_Us', data.area_List);
                            self.aboutArea = getAllMapData(About[0].component);
                            var banner = pluck('Banner_Area', data.area_List);
                            self.bannerArea = getAllMapData(banner[0].component);
                        }).catch(function (error) {
                            console.log('Error');
                        });
                    }

                });

            });

        }
    },
    mounted: function () {
        sessionStorage.active_er = 2;
        this.getPagecontent();
    },
});