var aboutus = new Vue({
    el: '#homeVue',
    name: 'homeVue',
    data: {
        commonStore: vueCommonStore.state,
        agencyCode: '',
        About: {About_Us:''},
        mainData:{},
        fullPage:true,
        isLoading:false,
    },
    methods: {
    
        getPagecontent: function () {
            var self = this;
            self.isLoading=true;
            //sessionStorage.setItem("1",)
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                    var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                                self.isLoading=false;
                            }
                        });
                    });
                } catch (err) {self.isLoading=false; }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                       var langauage = (sessionStorage.language) ? sessionStorage.language : "en";
                    //    if(!langauage){ sessionStorage.setItem("language",'en')}
                    //    console.log(langauage);
                       var about_Page = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Home/Home/Home.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(about_Page, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var main = pluck('Main_Area', self.data.area_List);
                                self.mainData = getAllMapData( main[0].component);
                                                        
                        }).catch(function (error) {
                            console.log('Error');
                        });
                    }

                });

            });

        }, getValidationMsgByCode: function (code) {
            if (sessionStorage.validationItems !== undefined) {
                var validationList = JSON.parse(sessionStorage.validationItems);
                for (let validationItem of validationList.Validation_List) {
                    if (code === validationItem.Code) {
                        return validationItem.Message;
                    }
                }
            }
         },
        login(){
            var userName = document.getElementById("txtUname").value;
            var password = document.getElementById("txtPwd").value;
            var agencyCode = document.getElementById("txtAgencyCode").value;
          //  var rmCheck = document.getElementById("rememberMe");
            var continueLogin;
            alertify.alert()
            .setting({
              'label':this.getValidationMsgByCode('M52'),
            //   'message': 'This dialog is : ' + (closable ? ' ' : ' not ') + 'closable.' ,
            //   'onok': function(){ alertify.success('Great');}
            })
            if (stringIsNullorEmpty(agencyCode)) {
                alertify.alert(this.getValidationMsgByCode('M38'),this.getValidationMsgByCode('M50'),this.getValidationMsgByCode('M52'));
                continueLogin = false;
                return false;
            } else if (stringIsNullorEmpty(userName)) {
                alertify.alert(this.getValidationMsgByCode('M38'),this.getValidationMsgByCode('M51'),this.getValidationMsgByCode('M52'));
                continueLogin = false;
                return false;
            } else if (stringIsNullorEmpty(password)) {
                alertify.alert(this.getValidationMsgByCode('M38'),this.getValidationMsgByCode('M33'),this.getValidationMsgByCode('M52'));
                continueLogin = false;
                return false;
            }
            else{  
                sendLogin();
            }
        },
              forgot() {
                var emailID = document.getElementById("txtEmail").value;
                var agyCode = document.getElementById("txtAgency").value;
                var isValidEmailString = true;
                var isAgencyCodeNotNull = true;
            alertify.alert().setting({
              'label':this.getValidationMsgByCode('M52'),
            //   'message': 'This dialog is : ' + (closable ? ' ' : ' not ') + 'closable.' ,
            //   'onok': function(){ alertify.success('Great');}
            })
          
            if (stringIsNullorEmpty(agyCode)) {
                alertify.alert(this.getValidationMsgByCode('M38'),this.getValidationMsgByCode('M50'));
                isAgencyCodeNotNull = false;
            } else if (stringIsNullorEmpty(emailID)) {
                alertify.alert(this.getValidationMsgByCode('M38'),this.getValidationMsgByCode('M02'));
                isValidEmailString = false;
            } else if (!isValidEmail(emailID)) {
                alertify.alert(this.getValidationMsgByCode('M38'),this.getValidationMsgByCode('M03'));
                isValidEmailString = false;
            } else {
                document.getElementById("loderForgot").style.display = "block";

            } 
                if (validPassword) {
        var url = window.location.href;
        var token = url.split("ResetPassword.html?")[1];
        if (token != undefined) {
            var data = {
                newPassword: newPwd
            };
            let axiosConfig = {
                headers: {
                    "Content-Type": "application/json;charset=UTF-8",
                    Authorization: "Bearer " + token
                }
            };
            var hubUrls = HubServiceUrls;
            var hubUrl = hubUrls.hubConnection.baseUrl;
            var port = hubUrls.hubConnection.ipAddress;
            var resetPassword = hubUrls.hubConnection.hubServices.resetPassword;
            axios.put(hubUrl + port + resetPassword, data, axiosConfig).then(function(res) {
                alertify.alert(this.getValidationMsgByCode('M15'),this.getValidationMsgByCode('M58'));
                    window.location.href = "/";
              
            }).catch(function(err){
                if (err.response.data.message) {
                    alertify.alert(this.getValidationMsgByCode('M38'), err.response.data.message);
                }else {
                    alertify.alert(this.getValidationMsgByCode('M38'),this.getValidationMsgByCode('M36'));
                }
            });
        } else {
            alertify.alert(this.getValidationMsgByCode('M38'),this.getValidationMsgByCode('M36'));
                window.location.href = "/";
            }
    }   
              },
          
    },
    mounted: function () {
        sessionStorage.active_er=1; 
        this.getPagecontent();
      

          },
});