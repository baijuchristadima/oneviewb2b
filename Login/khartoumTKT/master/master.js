var HeaderComponent = Vue.component('headeritem', {
    template: ` 
    <div id="header">
    <div class="top">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                    <div class="contact-info">
              <div class="phone-number"> <i class="fa fa-phone"></i> <a :href="'tel:'+contactArea.Phone_Number">{{contactArea.Phone_Number}}</a> </div>
              <div class="email"> <i class="fa fa-envelope"></i> <a :href="'tel:'+contactArea.Email"> {{contactArea.Email }}</a> </div>
               <ul class="list-unstyled list-inline lang" style="margin-block-start: -13px;">
                  <li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:void(0)" aria-expanded="false">{{language}} <span class="caret"><i class="fa fa-angle-down" aria-hidden="true"></i></span></a>
                    <ul class="dropdown-menu">
                      <li v-for="item in headerArea.Language_List" @click="languagechange(item.Code,item.Direction)"><a href="#">{{item.Language}}</a></li>
                        </ul>
                  </li>
                </ul>
              </div>
          </div>
        </div>
      </div>
    </div>
    <nav id="navbar-main" class="navbar navbar-default">
      <div class="container">
        <div class="row">
        <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              <a class="navbar-brand" href="index.html"><img :src="headerArea.Logo_165x164px" alt=""></a> </div>
            <div class="collapse navbar-collapse navbar-right" id="myNavbar">
              <ul class="nav navbar-nav">
                <li @click="activate(1)" :class="{ active : active_er == 1 }"><a href="index.html">{{headerArea.Home_Label}}</a></li>
                  <li @click="activate(2)" :class="{ active : active_er == 2 }"><a href="about-us.html">{{headerArea.About_Label}}</a></li>
                <li @click="activate(3)" :class="{ active : active_er == 3 }"><a href="contact-us.html">{{headerArea.Contact_Us_Label}}</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </nav>
  </div>`,
    data() {
        return {

            agencyCode: '',
            notificationEmail:{},
            active_er: (sessionStorage.active_er) ? sessionStorage.active_er : 1,
            contactArea:{},
            headerArea:{},    
            language:"", 
        
           // Email :''
        }
    },
    methods: {
        activate: function (el) {
            sessionStorage.active_er = el;
            this.active_er = el;
            if (el == 1 || el == 2) {
                maininstance.actvetab = el;
            }
            else {
                maininstance.actvetab = 0;
            }
        },

        getPageheader: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                var agencyFolderName = '';
                var agy;
          
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        // var langauage = sessionStorage.getItem("language");
                        // // if(langauage==''||null||undefined){ sessionStorage.setItem("language",'en')}
                        var langauage = (sessionStorage.language) ? sessionStorage.language : "en";
                        var Header = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Header/Header/Header.ftl';
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Contact Us/Contact Us/Contact Us.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                       
                        
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language':langauage}
                        }).then(function (response) {
                            self.data = response.data;
                            var Notification = self.pluck('Notifications', response.data.area_List);
                            if (Notification.length > 0) {
                                self.notificationEmail = self.pluckcom('Notification_Email', Notification[0].component);
                            }
                          var contact = self.pluck('Contact_Details', self.data.area_List);
                           self.contactArea=self.getAllMapData(contact[0].component);
                        }).catch(function (error) {
                            console.log('Error');
                            self.content = [];
                        });
                        axios.get(Header, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language':langauage}
                        }).then(function (response) {
                            self.data = response.data;
                            var Description = self.pluck('Main_Area', self.data.area_List);
                           self.headerArea=self.getAllMapData(Description[0].component);
                           self.headerArea.Language_List.forEach(element => {
                            if(langauage==element.Code){self.language=element.Language;}
                        });
                                                  
                        }).catch(function (error) {
                            console.log('Error');
                            self.content = [];
                        });
                    }

                });

            });
        },

        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },
        languagechange(lan,dir)
        {  sessionStorage.setItem('languageChange','1');
           sessionStorage.setItem("language",lan);
         sessionStorage.setItem("direction",dir);
         this.headerArea.Language_List.forEach(element => {if(lan==(element.Code)){
            this.language=element.Language; 

         }
             location.reload();
         });
          if (document.readyState == "complete") {
            if (sessionStorage.direction == 'rtl') {
                document.getElementById("styleid").setAttribute("href", 'assets/css/style-rtl.css');
                document.getElementById("mediaid").setAttribute("href", 'assets/css/media-rtl.css');
                alertify.defaults.glossary.ok = 'حسنا';
                } else {
                document.getElementById("styleid").setAttribute("href", 'assets/css/style-ltr.css');
                document.getElementById("mediaid").setAttribute("href", 'assets/css/media-ltr.css');
                alertify.defaults.glossary.ok = 'OK';
            }
        }  this.getPageheader();
        //  location.reload();
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
    },
    mounted: function () {
        this.getPageheader();

        document.onreadystatechange = () => {
            if (document.readyState == "complete") {
                if (sessionStorage.direction == 'rtl') {
                    document.getElementById("styleid").setAttribute("href", 'assets/css/style-rtl.css');
                    document.getElementById("mediaid").setAttribute("href", 'assets/css/media-rtl.css');
                    } else {
                    document.getElementById("styleid").setAttribute("href", 'assets/css/style-ltr.css');
                    document.getElementById("mediaid").setAttribute("href", 'assets/css/media-ltr.css');
                  
                }
            }
        }
    },
})
var headerinstance = new Vue({
    el: 'header',
    name: 'headerArea',
    data() {
        return {
            key: '',
            content: null,
            getdata: true
        }

    },

});
Vue.component('footeritem', {
    template: `
    <div>
    <div class="container">
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
          <div class="ftr-cntc">
            <h3>{{footerArea.Get_In_Touch_Label}}</h3>
            <p>{{footerArea.Get_In_Touch_Description}}</p>
            <ul>
              <li>
                <i class="fa fa-map-marker fnt-sz-25" aria-hidden="true"></i>
                {{contactArea.Address}}</li>
              <li>
                <i class="fa fa-envelope" aria-hidden="true"></i>
                <a :href="'mailto:'+contactArea.Email">{{contactArea.Email}}</a></li>
              <li>
                <i class="fa fa-phone fnt-sz-22" aria-hidden="true"></i>
                <a :href="'tel:'+contactArea.Phone_Number">{{contactArea.Phone_Number}}</a></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <div class="quick-links">
            <h3>{{footerArea.Quick_Links_Label}}</h3>
            <ul>
              <li><i class="fa  fa-angle-double-right"></i><a href="index.html"> {{headerArea.Home_Label}}</a></li>
              <li><i class="fa  fa-angle-double-right"></i><a href="about-us.html">{{headerArea.About_Label}}</a></li>
              <li><i class="fa  fa-angle-double-right"></i><a href="contact-us.html">{{headerArea.Contact_Us_Label}}</a></li>
              <li><i class="fa  fa-angle-double-right"></i><a href="terms-of-use.html">{{footerArea.Terms_And_Conditions__Label}}</a></li>
              <li><i class="fa  fa-angle-double-right"></i><a href="privacy-policy.html">{{footerArea.Privacy_Label}}</a></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
          <div class="newsletter">
            <h3>{{footerArea.Subscribe_Label}}</h3>
            <p>{{footerArea.Newsletter_Description}}</p>
            <div class="news-sub">
              <input type="text" id="text" name="text" :placeholder="footerArea.News_placeholder" v-model="newsltremail">
              <button type="submit" v-on:click="sendnewsletter"><i class="fa  fa-paper-plane"></i></button>
            </div>
            <div class="foot-social">
              <ul>
              <li v-for= "list in socialMediaList.Social_Media_List"> <a :href="list.Path" target="_blank"><i aria-hidden="true" :class="list.Icon"></i></a></li>
        
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="all-reserved">
        <div class="reserved">
          <p>{{footerArea.Copyright_Content}}</p>
        </div>
        <div class="powered">
          <p>{{footerArea.Powered_By_Label}}</p>
          <a href="http://www.oneviewit.com/" target="_blank"><img :src="footerArea.Oneview_Logo"
              alt="logo"></a>
        </div>
      </div>
    </div>
  </div>`,
    data() {
        return {
            commonStore: vueCommonStore.state,
            agencyCode: '',
            footerArea:[],
            contactArea:{},
            notificationEmail:{},
            socialMediaList:{},
            newsltremail:'',
            headerArea:{}
        }
    },
    methods: {
        getPagefooter: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {

                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
             //   var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : 'en';
             var langauage = (sessionStorage.language) ? sessionStorage.language : "en";
                        if(langauage==''||null){ sessionStorage.setItem("language",'en')}
                        var Footer = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Footer/Footer/Footer.ftl';
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Contact Us/Contact Us/Contact Us.ftl';
                        var Header = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Header/Header/Header.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var Notification = self.pluck('Notifications', response.data.area_List);
                            if (Notification.length > 0) {
                                self.notificationEmail = self.pluckcom('Notification_Email', Notification[0].component);
                            }
                                var contact = self.pluck('Contact_Details', self.data.area_List);
                           self.contactArea=self.getAllMapData(contact[0].component);
                           
                        }).catch(function (error) {
                            console.log('Error');
                            self.content = [];
                        });
                        axios.get(Header, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                          var data = response.data;
                            var Notification = self.pluck('Main_Area', data.area_List);
                         self.headerArea=self.getAllMapData(Notification[0].component);
                         sessionStorage.setItem('logo',self.headerArea.Email_Logo_);
                           
                        }).catch(function (error) {
                            console.log('Error');
                           // self.content = [];
                        });
                        axios.get(Footer, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                           var data = response.data;
                            var Description = self.pluck('Footer', data.area_List);
                           self.footerArea=self.getAllMapData(Description[0].component);
                           var socialMedia=self.pluck('Social_Media_Links', data.area_List);
                           self.socialMediaList=self.getAllMapData(socialMedia[0].component)
                        }).catch(function (error) {
                            console.log('Error');
                            //self.content = [];
                        });
                    }

                });

            });
    
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        }, 
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        },
        getValidationMsgs: function () {

                var self = this;
                axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                    //console.log(document.location.hostname.toLowerCase());
    
                    var agencyFolderName = '';
                    var agy;
                    try {
                        response.data.forEach(function (agent, agentIndex) {
                            agent.domain.forEach(function (dom, domIndex) {
                                if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                    agy = agent;
                                    agencyFolderName = agy.agencyFolderName;
                                }
                            });
                        });
                    } catch (err) { }
    
                    agy.registeredUsers.forEach(function (agyCode, domIndex) {
                        if (domIndex == 0) {
                            var huburl = HubServiceUrls.hubConnection.cmsUrl;
                            var portno = HubServiceUrls.hubConnection.ipAddress;
                            var langauage = (sessionStorage.language) ? sessionStorage.language : "en";
                            var about_Page = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Validation/Validation/Validation.ftl';
                            var agencyCode = agyCode;
                            self.agencyCode = agencyCode;
                            axios.get(about_Page, {
                                headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                            }).then(function (response) {
                                var data = response.data;
                                var validations = this.pluck('Validations', data.area_List);
                               var validationList = this.getAllMapData(validations[0].component);
                                sessionStorage.setItem('validationItems', JSON.stringify(validationList));
                               
    
                            }).catch(function (error) {
                                console.log('Error');
                            });
                        }
    
                    });
    
                });
    
               
          },
          getValidationMsgByCode: function (code) {
            if (sessionStorage.validationItems !== undefined) {
                var validationList = JSON.parse(sessionStorage.validationItems);
                for (let validationItem of validationList.Validation_List) {
                    if (code === validationItem.Code) {
                        return validationItem.Message;
                    }
                }
            }
         },
        sendnewsletter: async function () {
            alertify.alert()
            .setting({
              'label':this.getValidationMsgByCode('M52'),
            //   'message': 'This dialog is : ' + (closable ? ' ' : ' not ') + 'closable.' ,
            //   'onok': function(){ alertify.success('Great');}
            })
            if (!this.newsltremail) {
                alertify.alert(this.getValidationMsgByCode('M38'),this.getValidationMsgByCode('M02'));
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.newsltremail.match(emailPat);
            if (matchArray == null) {
                alertify.alert(this.getValidationMsgByCode('M38'),this.getValidationMsgByCode('M03'));
                return false;
            } else {
                var agencyCode = this.agencyCode
                var filterValue = "type='Newsletter' AND keyword1='" + this.newsltremail + "'";
                var allDBData = await this.getDbData4Table(agencyCode, filterValue, "date1");

                if (allDBData != undefined && allDBData.length > 0) {
                    alertify.alert(this.getValidationMsgByCode('M38'),this.getValidationMsgByCode('M04')).set('closable', false);
                    return false;
                }
                else {
                    this.isLoading=true;
                    logo=sessionStorage.getItem('logo');
                    var postData = {
                        type: "UserAddedRequest",
                        fromEmail: this.notificationEmail || this.commonStore.fallBackEmail,
                        toEmails: Array.isArray(this.newsltremail) ? this.newsltremail : [this.newsltremail],
                        logo:this.Logos.Logo || "",
                        agencyName: "Khartoum TKT",
                        agencyAddress: this.contactArea.Address || "",
                        personName: this.newsltremail.split("@")[0],
                        primaryColor: "#ffffff",
                        secondaryColor: "#842452"
                    };
                    var agencyCode = this.agencyCode
                    let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                    let insertSubscibeData = {
                        type: "Newsletter",
                        date1: requestedDate,
                        keyword1: this.newsltremail,
                        keyword2: "Subscribe Newsletter",
                        nodeCode: agencyCode
                    };
                    let responseObject = await this.cmsRequestData("POST", "cms/data", insertSubscibeData, null);
                    try {
                        let insertID = Number(responseObject);
                        var self = this;
                        mailUrl = self.commonStore.hubUrls.emailServices.emailApi;
                        sendMailService(mailUrl, postData);
                        this.newsltremail = '',
                       self.isLoading=false;
                        alertify.alert(this.getValidationMsgByCode('M15'),this.getValidationMsgByCode('M05'));
                    } catch (e) {
                        this.isLoading=false;
                    }
                }

            }
        },
        async getDbData4Table(agencyCode, extraFilter, sortField) {

            var allDBData = [];
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            var cmsURL = huburl + portno + '/cms/data/search/byQuery';
            var queryStr = "select * from cms_forms_data where nodeCode = '" + agencyCode + "'";
            if (extraFilter != undefined && extraFilter != '') {
                queryStr = queryStr + " AND " + extraFilter;
            }
            var requestObject = {
                query: queryStr,
                sortField: sortField,
                from: 0,
                orderBy: "desc"
            };
            let responseObject = await this.cmsRequestData("POST", "cms/data/search/byQuery", requestObject, null);
            if (responseObject != undefined && responseObject.data != undefined) {
                allDBData = responseObject.data;
            }
            return allDBData;

        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },
    },
    mounted: function () {
       
        this.getPagefooter();
        this.getValidationMsgs();
    },

})
var footerinstance = new Vue({
    el: 'footer',
    name: 'footerArea',
    data() {
        return {
            key: 0,
            content: null,
            getdata: true
        }

    },
});
