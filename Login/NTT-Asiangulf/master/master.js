var HeaderComponent = Vue.component('headeritem', {
    template: ` 
    <div id="header">
            <nav id="navbar-main" class="navbar navbar-default">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                                <a class="navbar-brand" href="index.html"><img :src="headerItems.Logo_194x79px" alt=""></a>
                            </div>
                            <div class="collapse navbar-collapse navbar-right" id="myNavbar">
                                <ul class="nav navbar-nav">
                                    <li><a href="index.html" :class="activePageName=='index.html' ? 'active' :''">{{headerItems.Home_Label}}</a></li>
                                    <li><a href="about-us.html" :class="activePageName=='about-us.html' ? 'active' :''">{{headerItems.About_Us_Label}}</a></li>
                                    <li><a href="#">{{headerItems.Contact_Us_Label}}</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>            
     `,
    data() {
        return {

            agencyCode: '',
            activePageName:'',
           
            headerItems:[],

        }
    },
    methods: {
            getPageheader: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/01 Master/01 Master/01 Master.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                                var contact = self.pluck('Header_Area', self.data.area_List);
                            self.headerItems=self.getAllMapData(contact[0].component);
                            }).catch(function (error) {
                            console.log('Error');
                            self.content = [];
                        });
                    }

                });

            });

        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
              contentArry.map(function (item) {
                let allKeys = Object.keys(item)
                for (let j = 0; j < allKeys.length; j++) {
                  let key = allKeys[j];
                  let value = item[key];
                  if (key != 'name' && key != 'type') {
                    if (value == undefined || value == null) {
                      value = "";
                    }
                    tempDataObject[key] = value;
                  }
                }
              });
            }
            return tempDataObject;
          },
    },
    mounted: function () {
        this.activePageName=window.location.pathname.split('/NTT-Asiangulf/')[1];
        this.getPageheader();
    },
})
var headerinstance = new Vue({
    el: 'header',
    name: 'headerArea',
    data() {
        return {
            key: '',
            content: null,
            getdata: true
        }

    },

});
Vue.component('footeritem', {
    template: `
  <div>                  
    <div class="container">
    <div class="inner">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="about">
                    <div class="ftr-logo"><img :src="footerItems.Logo260x110px" alt=""></div>
                    <h6>{{footerItems.Stay_Connected_With_Us_Label}}</h6>
                    <ul class="social" v-for="(item,index) in footerItems.Social_Media_List">
                        <li><a :href="item.Url" target="_blank"><i aria-hidden="true" :class="item.Icon"></i></a></li>
                      </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 bdr-left">
                <div class="contact">
                    <h3>{{footerItems.Contact_Info_Label}}</h3>
                    <ul>
                        <li>
                            <i class="fa fa-mobile fnt-sz-22" aria-hidden="true"></i>
                            <a :href="'tel:'+footerItems.Phone_Number">{{footerItems.Phone_Number}}</a>
                        </li>
                        <li>
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                            <a :href="'mailto:'+footerItems.Email_Id">{{footerItems.Email_Id}}</a>
                        </li>
                        <li>
                            <i class="fa fa-map-marker fnt-sz-17" aria-hidden="true"></i>
                           {{footerItems.Address}}
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 bdr-left">
                <div class="link">
                    <h3>{{footerItems.Quick_Links_Label}}</h3>
                    <ul>
                        <li><a href="index.html">{{headerItems.Home_Label}}</a></li>
                        <li><a href="about-us.html">{{headerItems.About_Us_Label}}</a></li>
                        <li><a href="#">{{headerItems.Contact_Us_Label}}</a></li>
                    </ul>
                    <ul>
                        <li><a href="#">{{footerItems.Privacy_Label}}</a></li>
                        <li><a href="#">{{footerItems.Terms_Label}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="ftr-btm">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="copyright">{{footerItems.Copyright_Label}}.</div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="powered-by">{{footerItems.Powered_Label}} &nbsp; <a href="http://www.oneviewit.com/" target="_blank"><img alt="" :src="footerItems.Oneview_Logo_92x33px"></a></div>

            </div>
        </div>
        <div class="back-top text-center" title="Back to Top"> <a href="javascript:void(0);" id="toTop" style="display: inline;"> <i class="fa fa-chevron-up"></i> </a> </div>
    </div>
</div>
</div>
 `,
    data() {
        return {
           // commonStore: vueCommonStore.state,
            agencyCode: '',
            footerItems:[],
            headerItems:[],
        }
    },
    methods: {
        getPagefooter: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var Footer = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/01 Master/01 Master/01 Master.ftl';
                     //   var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Contact Us/Contact Us/Contact Us.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                                axios.get(Footer, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                           var data = response.data;
                            var obj2 = self.pluck('Footer_Area',data.area_List);
                                self.footerItems=self.getAllMapData(obj2[0].component);
                            var obj = self.pluck('Header_Area',data.area_List);
                            self.headerItems=self.getAllMapData(obj[0].component);
                           
                        }).catch(function (error) {
                            console.log('Error');
                            
                        });
                    }

                });

            });

        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },

        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },

        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
              contentArry.map(function (item) {
                let allKeys = Object.keys(item)
                for (let j = 0; j < allKeys.length; j++) {
                  let key = allKeys[j];
                  let value = item[key];
                  if (key != 'name' && key != 'type') {
                    if (value == undefined || value == null) {
                      value = "";
                    }
                    tempDataObject[key] = value;
                  }
                }
              });
            }
            return tempDataObject;
          },
    },
    mounted: function () {
        this.getPagefooter();
    },

})
var footerinstance = new Vue({
    el: 'footer',
    name: 'footerArea',
    data() {
        return {
            key: 0,
            content: null,
            //getdata: 
            
        }

    },
});
