var aboutus = new Vue({
    el:'#index',
    name:'index',
    data: {
      //  commonStore: vueCommonStore.state,
        agencyCode: '',
        pageContents:{},
        offers:[],
        isLoading:false,
        fullPage:true,

    },
    filters: {

      subStr: function (string) {
          if (string.length > 8)
              return string.substring(0, 9) + '...';

          else
              return string;
      }

  },
    methods: {
        getPage: function () {
            var self = this;
            self.isLoading=true;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {

                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                   
                } catch (err) {self.isLoading=false; }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Home Page/Home Page/Home Page.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            var data = response.data;
                                var pages=self.pluck('Banner_Area',data.area_List);
                        self.pageContents=self.getAllMapData(pages[0].component);
                        self.isLoading=false;
                       //  self.data = response.data;
                            //                     if (response.data.area_List) {
                            //  var banner = pluck("Banner_Area",data.area_List);
                            //  self.pageContents =getAllMapData(banner[0].component);
                            // //  var form = pluck("Form_Section", self.data.area_List);
                            // //  self.formSection = getAllMapData(form[0].component);
                         
                            // var contact = 
                            // if (contact.length > 0) {
                            //     self.contact.Phone_Number_1 = self.pluckcom('Phone_Number', contact[0].component);
                            //     self.contact.Email = self.pluckcom('Email', contact[0].component);
                           // }
                        }).catch(function (error) {
                            console.log('Error');
                            self.isLoading=false;
                        });
                    }

                });

            });

        },
        //Master Table/Packages/Packages/Packages.ftl
        getOffer: function () {
            var self = this;
            debugger;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {

                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Master Table/Offers/Offers/Offers.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            var data = response.data;
                           // console.log("data",data);
                        self.offers=data.Values;
                        //console.log(offers);
                        // self.pageContents=self.getAllMapData(pages[0].component);
                       //  self.data = response.data;
                            //                     if (response.data.area_List) {
                            //  var banner = pluck("Banner_Area",data.area_List);
                            //  self.pageContents =getAllMapData(banner[0].component);
                            // //  var form = pluck("Form_Section", self.data.area_List);
                            // //  self.formSection = getAllMapData(form[0].component);
                         
                            // var contact = 
                            // if (contact.length > 0) {
                            //     self.contact.Phone_Number_1 = self.pluckcom('Phone_Number', contact[0].component);
                            //     self.contact.Email = self.pluckcom('Email', contact[0].component);
                           // }
                        }).catch(function (error) {
                            console.log('Error');
                            
                        });
                    }

                });

            });

        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
              contentArry.map(function (item) {
                let allKeys = Object.keys(item)
                for (let j = 0; j < allKeys.length; j++) {
                  let key = allKeys[j];
                  let value = item[key];
                  if (key != 'name' && key != 'type') {
                    if (value == undefined || value == null) {
                      value = "";
                    }
                    tempDataObject[key] = value;
                  }
                }
              });
            }
            return tempDataObject;
          },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
       
        getmoreinfo(url) {
            if (url != null) {
              if (url != "") {
                url = url.split("/Template/")[1];
                url = url.split(' ').join('-');
                url = url.split('.').slice(0, -1).join('.');
                url = "package-detail.html?page=" + url + "&from=pkg";
                window.location.href = url;
              }
              else {
                url = "#";
              }
            }
            else {
              url = "#";
            }
            return url;
          }
    },
    mounted: function () {
        sessionStorage.active_er=1; 
        this.getPage();
        this.getOffer();
    },
});
function getQueryStringValue(key) {
  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}
function packages(){
    $("#owl-demo-3").owlCarousel({
        autoplay:true,
        autoPlay : 8000,
        autoplayHoverPause:true, 
        stopOnHover : false,  
        items : 3,
        margin:10,  
        lazyLoad : true,
        navigation : true,
        itemsDesktop : [1199, 3],
        itemsDesktopSmall : [991,2],
          itemsTablet : [600, 1]
      });
          $( ".owl-prev").html('<i class="fa  fa-chevron-left"></i>');
         $( ".owl-next").html('<i class="fa  fa-chevron-right"></i>');
}