var contact = new Vue({
    el: '#registrationpage',
    name: 'registrationpage',
    data: {
        commonStore: vueCommonStore.state,
        contact: {Address:''},
        //form
        Agyname: '',
        Agyaddress: '',
        Agyphone: '',
        Agybox:'',
        Agycity: '',
        Country:'',
        fname: '',
        lname: '',
        gemail: '',
        gphone:'',
        sname: '',
        bname:'',
        bbranch: '',
        pname: '',
        pphone: '',
        pemail:'',
        registration:'',
        License:'',
        Commerce:'',
        GPassport:'',
        GEmirates:'',
        SPassport:'',
        SEmirates:'',
        agencyCode: '',
        Logos:{Logo:''},
        nationalitycountry: {
          countries: countryList
      },
      notificationEmail: ""
    },
    methods: {
          submitRegistration() {
            var self = this;
            if (self.file != null && self.file != undefined) {
              $.getJSON('/Login/ShamsAbuDhabi/fileupload.json', function (json) {
      
                var encodedString = btoa(json.User_Name + ":" + json.Password);
                let formData = new FormData();
                formData.append('file', self.file);
                // console.log('>> formData >> ', formData);
                var url = "https://fileupload.oneviewitsolutions.com:9443/upload/" + "Registration no"
                // You should have a server side REST API 
                axios.post(url,
                  formData, {
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data',
                    'Authorization': 'Basic ' + encodedString
                  }
                }
                ).then(function (repo) {
                  console.log('SUCCESS!!', repo.data.message);
                  self.registration = repo.data.message;
                  alertify.alert("Success", "File Successfully uploaded.");
                  self.file=null;
                })
                  .catch(function () {
                    console.log('FAILURE!!');
                    self.registration = null;
                  });
              });
            }else {
              alertify.alert("warning", "please select the file");
            }
          },
          handleFileUpload() {
            var self = this;
            file_temp = self.$refs.file.files[0];
            var ext = file_temp.name.split('.')[1];
            if (ext == "pdf" || ext == "docx" || ext == "doc") {
                self.file = self.$refs.file.files[0];
            } else {
                alertify.alert('Warning', 'File should be in PDF or document format!.').set('closable', false);
                self.file = null;
            }
            // console.log('>>>> 1st element in files array >>>> ', this.file);
        },

        submitLicense() {
            var self = this;
            if (self.file2 != null && self.file2 != undefined) {
              $.getJSON('/Login/ShamsAbuDhabi/fileupload.json', function (json) {
      
                var encodedString = btoa(json.User_Name + ":" + json.Password);
                let formData = new FormData();
                formData.append('file', self.file2);
                // console.log('>> formData >> ', formData);
                var url = "https://fileupload.oneviewitsolutions.com:9443/upload/" + "License no"
                // You should have a server side REST API 
                axios.post(url,
                  formData, {
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data',
                    'Authorization': 'Basic ' + encodedString
                  }
                }
                ).then(function (repo) {
                  console.log('SUCCESS!!', repo.data.message);
                  self.License = repo.data.message;
                  alertify.alert("Success", "File Successfully uploaded.");
                  self.file2=null;
                })
                  .catch(function () {
                    console.log('FAILURE!!');
                    self.License = null;
                  });
              });
            }else {
              alertify.alert("warning", "please select the file");
            }
          },
          handleFileUpload2() {
            var self = this;
            file_temp = self.$refs.file2.files[0];
            var ext = file_temp.name.split('.')[1];
            if (ext == "pdf" || ext == "docx" || ext == "doc") {
                self.file2 = self.$refs.file2.files[0];
            } else {
                alertify.alert('Warning', 'File should be in PDF or document format!.').set('closable', false);
                self.file2 = null;
            }
            // console.log('>>>> 1st element in files array >>>> ', this.file);
        },
        submitCommerce() {
            var self = this;
            if (self.file3 != null && self.file3 != undefined) {
              $.getJSON('/Login/ShamsAbuDhabi/fileupload.json', function (json) {
      
                var encodedString = btoa(json.User_Name + ":" + json.Password);
                let formData = new FormData();
                formData.append('file', self.file3);
                // console.log('>> formData >> ', formData);
                var url = "https://fileupload.oneviewitsolutions.com:9443/upload/" + "Commerce no"
                // You should have a server side REST API 
                axios.post(url,
                  formData, {
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data',
                    'Authorization': 'Basic ' + encodedString
                  }
                }
                ).then(function (repo) {
                  console.log('SUCCESS!!', repo.data.message);
                  self.Commerce = repo.data.message;
                  alertify.alert("Success", "File Successfully uploaded.");
                  self.file3=null;
                })
                  .catch(function () {
                    console.log('FAILURE!!');
                    self.Commerce = null;
                  });
              });
            }else {
              alertify.alert("warning", "please select the file");
            }
          },
          handleFileUpload3() {
            var self = this;
            file_temp = self.$refs.file3.files[0];
            var ext = file_temp.name.split('.')[1];
            if (ext == "pdf" || ext == "docx" || ext == "doc") {
                self.file3 = self.$refs.file3.files[0];
            } else {
                alertify.alert('Warning', 'File should be in PDF or document format!.').set('closable', false);
                self.file3 = null;
            }
            // console.log('>>>> 1st element in files array >>>> ', this.file);
        },
        submitGPassport() {
            var self = this;
            if (self.file4 != null && self.file4 != undefined) {
              $.getJSON('/Login/ShamsAbuDhabi/fileupload.json', function (json) {
      
                var encodedString = btoa(json.User_Name + ":" + json.Password);
                let formData = new FormData();
                formData.append('file', self.file4);
                // console.log('>> formData >> ', formData);
                var url = "https://fileupload.oneviewitsolutions.com:9443/upload/" + "Passport"
                // You should have a server side REST API 
                axios.post(url,
                  formData, {
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data',
                    'Authorization': 'Basic ' + encodedString
                  }
                }
                ).then(function (repo) {
                  console.log('SUCCESS!!', repo.data.message);
                  self.GPassport = repo.data.message;
                  alertify.alert("Success", "File Successfully uploaded.");
                  self.file4=null;
                })
                  .catch(function () {
                    console.log('FAILURE!!');
                    self.GPassport = null;
                  });
              });
            }else {
              alertify.alert("warning", "please select the file");
            }
          },
          handleFileUpload4() {
            var self = this;
            file_temp = self.$refs.file4.files[0];
            var ext = file_temp.name.split('.')[1];
            if (ext == "pdf" || ext == "docx" || ext == "doc") {
                self.file4 = self.$refs.file4.files[0];
            } else {
                alertify.alert('Warning', 'File should be in PDF or document format!.').set('closable', false);
                self.file4 = null;
            }
            // console.log('>>>> 1st element in files array >>>> ', this.file);
        },
        submitGEmirates() {
            var self = this;
            if (self.file5 != null && self.file5 != undefined) {
              $.getJSON('/Login/ShamsAbuDhabi/fileupload.json', function (json) {
      
                var encodedString = btoa(json.User_Name + ":" + json.Password);
                let formData = new FormData();
                formData.append('file', self.file5);
                // console.log('>> formData >> ', formData);
                var url = "https://fileupload.oneviewitsolutions.com:9443/upload/" + "GEmirates ID"
                // You should have a server side REST API 
                axios.post(url,
                  formData, {
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data',
                    'Authorization': 'Basic ' + encodedString
                  }
                }
                ).then(function (repo) {
                  console.log('SUCCESS!!', repo.data.message);
                  self.GEmirates = repo.data.message;
                  alertify.alert("Success", "File Successfully uploaded.");
                  self.file5=null;
                })
                  .catch(function () {
                    console.log('FAILURE!!');
                    self.GEmirates = null;
                  });
              });
            }else {
              alertify.alert("warning", "please select the file");
            }
          },
          handleFileUpload5() {
            var self = this;
            file_temp = self.$refs.file5.files[0];
            var ext = file_temp.name.split('.')[1];
            if (ext == "pdf" || ext == "docx" || ext == "doc") {
                self.file5 = self.$refs.file5.files[0];
            } else {
                alertify.alert('Warning', 'File should be in PDF or document format!.').set('closable', false);
                $("#GEmirates").val(null);
                self.file5 = null;
            }
            // console.log('>>>> 1st element in files array >>>> ', this.file);
        },
        submitSPassport() {
            var self = this;
            if (self.file6 != null && self.file6 != undefined) {
              $.getJSON('/Login/ShamsAbuDhabi/fileupload.json', function (json) {
      
                var encodedString = btoa(json.User_Name + ":" + json.Password);
                let formData = new FormData();
                formData.append('file', self.file6);
                // console.log('>> formData >> ', formData);
                var url = "https://fileupload.oneviewitsolutions.com:9443/upload/" + "SPassport"
                // You should have a server side REST API 
                axios.post(url,
                  formData, {
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data',
                    'Authorization': 'Basic ' + encodedString
                  }
                }
                ).then(function (repo) {
                  console.log('SUCCESS!!', repo.data.message);
                  self.SPassport = repo.data.message;
                  alertify.alert("Success", "File Successfully uploaded.");
                  self.file6=null;
                })
                  .catch(function () {
                    console.log('FAILURE!!');
                    self.SPassport = null;
                  });
              });
            }else {
              alertify.alert("warning", "please select the file");
            }
          },
          handleFileUpload6() {
            var self = this;
            file_temp = self.$refs.file6.files[0];
            var ext = file_temp.name.split('.')[1];
            if (ext == "pdf" || ext == "docx" || ext == "doc") {
                self.file6 = self.$refs.file6.files[0];
            } else {
                alertify.alert('Warning', 'File should be in PDF or document format!.').set('closable', false);
                $("#SPassport").val(null);
                self.file6 = null;
            }
            // console.log('>>>> 1st element in files array >>>> ', this.file);
        },
        submitSEmirates() {
            var self = this;
            if (self.file7 != null && self.file7 != undefined) {
              $.getJSON('/Login/ShamsAbuDhabi/fileupload.json', function (json) {
      
                var encodedString = btoa(json.User_Name + ":" + json.Password);
                let formData = new FormData();
                formData.append('file', self.file7);
                // console.log('>> formData >> ', formData);
                var url = "https://fileupload.oneviewitsolutions.com:9443/upload/" + "SEmirates ID"
                // You should have a server side REST API 
                axios.post(url,
                  formData, {
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'multipart/form-data',
                    'Authorization': 'Basic ' + encodedString
                  }
                }
                ).then(function (repo) {
                  console.log('SUCCESS!!', repo.data.message);
                  self.SEmirates = repo.data.message;
                  alertify.alert("Success", "File Successfully uploaded.");
                  self.file7=null;
                })
                  .catch(function () {
                    console.log('FAILURE!!');
                    self.SEmirates = null;
                  });
              });
            }else {
              alertify.alert("warning", "please select the file");
            }
          },
          handleFileUpload7() {
            var self = this;
            file_temp = self.$refs.file7.files[0];
            var ext = file_temp.name.split('.')[1];
            if (ext == "pdf" || ext == "docx" || ext == "doc") {
                self.file7 = self.$refs.file7.files[0];
            } else {
                alertify.alert('Warning', 'File should be in PDF or document format!.').set('closable', false);
                self.file7 = null;
            }
            // console.log('>>>> 1st element in files array >>>> ', this.file);
        },

        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Contact Us/Contact Us/Contact Us.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var Notification = self.pluck('Notifications', response.data.area_List);
                            if (Notification.length > 0) {
                              self.notificationEmail = self.pluckcom('Notification_Email', Notification[0].component);
                            }
                            var contact = self.pluck('Contact_Details', self.data.area_List);
                            if (contact.length > 0) {
                                self.contact.Address = self.pluckcom('Address', contact[0].component);
                            }
                        }).catch(function (error) {
                    });
                    }

                });

            });

        },
        Registration: async function () {
            if (!this.Agyname) {
                alertify.alert('Alert', 'Travel Agency Name required.').set('closable', false);
                return false;
            }
            if (!this.Agyaddress) {
                alertify.alert('Alert', 'Address required.').set('closable', false);
                return false;
            }
            if (!this.Agyphone) {
                alertify.alert('Alert', 'Phone number required.').set('closable', false);
                return false;
            }
            if (this.Agyphone.length < 8) {
                alertify.alert('Alert', 'Enter valid phone number.').set('closable', false);
                return false;
            }
            if (!this.Agybox) {
                alertify.alert('Alert', 'P.O.Box required.').set('closable', false);
                return false;
            }
            if (!this.Agycity) {
                alertify.alert('Alert', 'City required.').set('closable', false);
                return false;
            }
            if (!this.Country) {
                alertify.alert('Alert', 'Select Country.').set('closable', false);
                return false;
            }
            if (!this.registration) {
                alertify.alert('Alert', 'Please upload Vat Registration No.').set('closable', false);
                return false;
            }
            if (!this.License) {
                alertify.alert('Alert', 'Please upload Municipality Trade License No.').set('closable', false);
                return false;
            }
            if (!this.Commerce) {
                alertify.alert('Alert', ' Please upload Chamber Of Commerce No.').set('closable', false);
                return false;
            }
            if (!this.fname) {
                alertify.alert('Alert', 'First Name required.').set('closable', false);
                return false;
            }
            if (!this.lname) {
                alertify.alert('Alert', 'Last Name required..').set('closable', false);
                return false;
            }
            if (!this.gemail) {
                alertify.alert('Alert', 'Email Id required.').set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.gemail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                return false;
            }
            if (!this.gphone) {
                alertify.alert('Alert', 'Mobile number required.').set('closable', false);
                return false;
            }
            if (this.gphone.length < 8) {
                alertify.alert('Alert', 'Enter valid mobile number.').set('closable', false);
                return false;
            }
            if (!this.GPassport) {
                alertify.alert('Alert', 'Please upload General Passport.').set('closable', false);
                return false;
            }
            if (!this.GEmirates) {
                alertify.alert('Alert', 'Please upload General Emirates ID.').set('closable', false);
                return false;
            }
            if (!this.sname) {
                alertify.alert('Alert', 'Sponser Name required.').set('closable', false);
                return false;
            }
            if (!this.SPassport) {
                alertify.alert('Alert', 'Please upload Sponsor Passport.').set('closable', false);
                return false;
            }
            if (!this.SEmirates) {
                alertify.alert('Alert', 'Please upload Sponsor Emirates ID.').set('closable', false);
                return false;
            }
            if (!this.bname) {
                alertify.alert('Alert', 'Bankers Name required.').set('closable', false);
                return false;
            }
            if (!this.bbranch) {
                alertify.alert('Alert', 'Branch/Address required.').set('closable', false);
                return false;
            }
            if (!this.pname) {
                alertify.alert('Alert', 'Person Name required.').set('closable', false);
                return false;
            }
            if (!this.pphone) {
                alertify.alert('Alert', 'Person Mobile number required.').set('closable', false);
                return false;
            }
            if (this.pphone.length < 8) {
                alertify.alert('Alert', 'Enter valid mobile number.').set('closable', false);
                return false;
            }
            if (!this.pemail) {
                alertify.alert('Alert', 'Person Email Id required.').set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.pemail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                return false;
            }
         else {
                var self = this;
                var logourl = window.location.origin + "/Login/ShamsAbuDhabi/website-informations/logo/logo.png"
                var frommail = this.notificationEmail || this.commonStore.fallBackEmail;
                var postData = {
                    type: "UserAddedRequest",
                    fromEmail: frommail,
                    toEmails: Array.isArray(this.pemail) ? this.pemail : [this.pemail],
                    logo:logourl,
                    agencyName: "Shams Abu Dhabi Travel",
                    agencyAddress: this.contact.Address || "",
                    personName:this.pname || "",
                    primaryColor: "#ffffff",
                    secondaryColor: "#364793"
                };
                var agencyCode = this.agencyCode
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                let insertContactData = {
                    type: "Registration",
                    keyword1: this.Agyname,
                    text1:this.Agyaddress,
                    keyword5: this.Agyphone,
                    keyword2: this.Agybox,
                    keyword3:this.Agycity,
                    keyword4:this.Country,
                    text2:this.registration,
                    text3:this.License,
                    text4:this.Commerce,
                    keyword8:this.fname,
                    keyword10:this.lname,
                    keyword11:this.gemail,
                    keyword6:this.gphone,
                    text5:this.GPassport,
                   text6:this.GEmirates,
                   keyword14:this.sname,
                   text7:this.SPassport,
                   text8:this.SEmirates,
                   keyword17:this.bname,
                   keyword18:this.bbranch,
                   keyword19:this.pname,
                   keyword7:this.pphone,
                   keyword20:this.pemail,
                   date1: requestedDate,
                    nodeCode: agencyCode
                };
                let responseObject = await this.cmsRequestData("POST", "cms/data", insertContactData, null);
                try {
                    let insertID = Number(responseObject);
                    var self = this;
                    mailUrl = self.commonStore.hubUrls.emailServices.emailApi;
                    sendMailService(mailUrl, postData);
                    this.Agyname= "",
                    this.Agyaddress ="",
                    this.Agyphone="",
                    this.Agybox="",
                    this.Agycity="",
                    this.Country="",
                    this.fname="",
                    this.lname="",
                    this.gemail="",
                    this.gphone="",
                    this.sname="",
                   this.bname="",
                   this.bbranch="",
                   this.pname="",
                   this.pphone="",
                   this.pemail="",
                   $("#Registration").val(null);
                   $("#Commerce").val(null);
                   $("#License").val(null);
                   $("#GPassport").val(null);
                   $("#SEmirates").val(null);
                   $("#GEmirates").val(null);
                   $("#SPassport").val(null);
                    alertify.alert('Registration', 'Thank you for Registration.We shall get back to you.');
                } catch (e) {

                }



            }
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var vm = this;
            var huburl = vm.commonStore.hubUrls.hubConnection.cmsUrl;
            var portno = vm.commonStore.hubUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        }
    },
    mounted: function () {
        this.getPagecontent();
        sessionStorage.active_er=4; 
    },
});