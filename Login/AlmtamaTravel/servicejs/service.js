var contact = new Vue({
    el: '#servicepage',
    name: 'servicepage',
    data: {
        commonStore: vueCommonStore.state,
        agencyCode: '',
        abouts: {
            Features: null
        },
        Corporate: {},
        Overview: {},
        Holidays:{},
        Tours:{},
        Place:{},
        Cargo:{},
        Airline:{},
        Logistic:{}

    },
    filters: {

        subStr: function (string) {
            if (string.length > 100)
                return string.substring(0, 100) + '...';

            else
                return string;
        }

    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Contact Us/Contact Us/Contact Us.ftl';
                        var service_Page = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Services/Services/Services.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(service_Page, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var Corporate = self.pluck('Corporate_Travel_Services', self.data.area_List);
                            if (Corporate.length > 0) {
                                self.Corporate.List = self.pluckcom('List', Corporate[0].component);
                            }
                            var Overview = self.pluck('Overview', self.data.area_List);
                            if (Overview.length > 0) {
                                self.Overview.List = self.pluckcom('Overviews', Overview[0].component);
                            }
                            var abouts = self.pluck('Features_Area', self.data.area_List);
                            if (abouts.length > 0) {
                                self.abouts.Features = self.pluckcom('Features', abouts[0].component);
                            }
                            var Holidays = self.pluck('Our_Holidays_Tab', self.data.area_List);
                            if (Holidays.length > 0) {
                                self.Holidays.Our_Holidays_Tab_Name = self.pluckcom('Our_Holidays_Tab_Name', Holidays[0].component);
                                self.Holidays.Image = self.pluckcom('Image', Holidays[0].component);
                                self.Holidays.Content_Section_1 = self.pluckcom('Content_Section_1', Holidays[0].component);
                                self.Holidays.Content_Section_2 = self.pluckcom('Content_Section_2', Holidays[0].component);
                            }
                            var Tours = self.pluck('Inbound_Tours', self.data.area_List);
                            if (Tours.length > 0) {
                                self.Tours.Content = self.pluckcom('Content', Tours[0].component);
                                self.Tours.Inbound_Tours_Tab_Name = self.pluckcom('Inbound_Tours_Tab_Name', Tours[0].component);
                             
                            }
                            var Place= self.pluck('Inbound_Tours', self.data.area_List);
                            if (Place.length > 0) {
                                self.Place.Points_of_Interest = self.pluckcom('Points_of_Interest', Place[0].component);
                            }
                            var Airline = self.pluck('Airline_Tab', self.data.area_List);
                            if (Airline.length > 0) {
                                self.Airline.Airline_Tab_Name = self.pluckcom('Airline_Tab_Name', Airline[0].component);
                                self.Airline.Content = self.pluckcom('Content', Airline[0].component);
                            }
                            var Logistic = self.pluck('Logistics_Tab', self.data.area_List);
                            if (Logistic.length > 0) {
                                self.Logistic.Content = self.pluckcom('Content', Logistic[0].component);
                                self.Logistic.Services = self.pluckcom('Services', Logistic[0].component);
                                self.Logistic.Logistics_Tab_Name = self.pluckcom('Logistics_Tab_Name', Logistic[0].component);
                                
                            }
                            var Cargo = self.pluck('Cargo_Tab', self.data.area_List);
                            if (Cargo.length > 0) {
                                self.Cargo.Cargo_Tab_Name = self.pluckcom('Cargo_Tab_Name', Cargo[0].component);
                                self.Cargo.Content = self.pluckcom('Content', Cargo[0].component);
                                self.Cargo.Services = self.pluckcom('Services', Cargo[0].component);
                                
                            }


                        }).catch(function (error) {
                            //console.log('Error');
                            self.content = [];
                        });
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var contact = self.pluck('Contacts', self.data.area_List);
                            if (contact.length > 0) {
                                self.contact.Address = self.pluckcom('Address', contact[0].component);
                                self.contact.Phone_Number_1 = self.pluckcom('Phone_Number_1', contact[0].component);
                                self.contact.Phone_Number_2 = self.pluckcom('Phone_Number_2', contact[0].component);
                                self.contact.Email = self.pluckcom('Email', contact[0].component);
                            }


                        }).catch(function (error) {
                            //console.log('Error');
                            self.content = [];
                        });
                    }

                });

            });

        }
    },
    mounted: function () {
        this.getPagecontent();
        sessionStorage.active_el=2; 
    },
});