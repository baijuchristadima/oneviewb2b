﻿var AgencyInformation = {
    AirInformation: {
        homeUrl: "Flights/index.html",
        totalBookingingsHome: 5,
        AirFolderName: "Flights",

    },
    hotelService: {
        homeUrl: "Hotels/hotelsearch.html",
        totalBookingingsHome: 5,
    },
    sightseeingService: {
        homeUrl: "sightseeing/index.html",
        totalBookingingsHome: 5,
    },
    insuranceService: {
        homeUrl: "Insurance/index.html",
        totalBookingingsHome: 5,
    },
    transferService: {
        homeUrl: "Insurance/index.html",
        totalBookingingsHome: 5,
    },
    DomainUrl: "http://b2b.oneviewitsolutions.com",
    systemSettings: {
        systemDateFormat: 'dd M y,D',
        calendarDisplay: 2,
        calendarDisplayInMobile: 1
    },


    AgencyAddress: {
        AgencyName: "Fahad Express Travel and Tourism",
        AgencyTitle: "Treasures Travel & Tourism | Online Booking for Cheap Flights &amp; Airline Tickets",
        AgencyShortName: "Treasures Travel & Tourism",
        AgencyAddress: "Po Box 393466, Office # 1406, Exchange Tower, Business Bay, Dubai, U.A.E.",
        AboutAgency: "Treasures Travel & Tourism, prides itself as one of the leading Destination Management Company, which provides a complete high standard service to our guests.",
        AgencyWebsite: "http://www.oneviewit.com",
        Helpline: "+971 4 361 5551",
        Email: "info@treasurestt.com",
        CopyRight: "© 2018. Treasures Travel & Tourism - All Rights Reserved",
        Facebook: "http://www.fb.com/oneviewitsolutions",
        Twitter: "http://www.twitter.com/oneviewitsolutions",
        GooglePlus: "http://plus.google.com/oneviewitsolutions",
        Linkedln: "http://www.linkedin.com/oneviewitsolutions",
        GooglePlayStore: "",
        AppStore: "",
        YouTube: ""
    }
}