var HeaderComponent = Vue.component('headeritem', {
    template: `     
    <div>
    <nav id="navbar-main" class="navbar navbar-default">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse"
                            data-target="#myNavbar"> <span class="icon-bar"></span> <span
                                class="icon-bar"></span> <span class="icon-bar"></span> </button>
                        <a class="navbar-brand" href="index.html"><img src="website-informations/logo/logo.png"
                                alt=""></a>
                    </div>
                    <div class="collapse navbar-collapse navbar-right" id="myNavbar">
                        <ul class="nav navbar-nav">
                            <li @click="activate(0)" :class="{ active : active_el == 0 }"><a href="index.html">Home</a></li>
                            <li @click="activate(1)" :class="{ active : active_el == 1 }"><a href="about-us.html">About Us</a></li>
                            <li @click="activate(2)" :class="{ active : active_el == 2 }"><a href="our-services.html">Our Services</a></li>
                            <li @click="activate(3)" :class="{ active : active_el == 3 }"><a href="contact-us.html">Contact Us</a></li>
                        </ul>
                        <div class="nav navbar-nav navbar-right right_area">
                            <span><i class="fa fa-headphones"></i>Free 24/7 Support</span>
                            <h3>{{contact.Phone_Number_1}}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    </div>`,
    data() {
        return {

            agencyCode: '',
            contact: { Phone_Number_1: '' },
            active_el: (sessionStorage.active_el) ? sessionStorage.active_el : 0,

        }
    },
    methods: {
        activate: function (el) {
            sessionStorage.active_el = el;
            this.active_el = el;
            if (el == 1 || el == 2) {
                maininstance.actvetab = el;
            }
            else {
                maininstance.actvetab = 0;
            }
        },
        getPageheader: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {

                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Contact Us/Contact Us/Contact Us.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var contact = self.pluck('Contacts', self.data.area_List);
                            if (contact.length > 0) {
                                self.contact.Address = self.pluckcom('Address', contact[0].component);
                                self.contact.Phone_Number_1 = self.pluckcom('Phone_Number_1', contact[0].component);
                                self.contact.Phone_Number_2 = self.pluckcom('Phone_Number_2', contact[0].component);
                                self.contact.Email = self.pluckcom('Email', contact[0].component);
                            }
                        }).catch(function (error) {
                            console.log('Error');
                            self.content = [];
                        });
                    }

                });

            });

        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
    },
    mounted: function () {

        this.getPageheader();
    },
})
var headerinstance = new Vue({
    el: 'header',
    name: 'headerArea',
    data() {
        return {
            key: '',
            content: null,
            getdata: true
        }

    },

});
Vue.component('footeritem', {
    props: {
        item: Number
    },
    template: `
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 address">
                <h6>Address</h6>
                <p>{{contact.Address}}</p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 footermenu">
                <h6>Quick Link</h6>
                <ul>
                    <li :class="{ active : item == 0 }"><a href="index.html">Home</a></li>
                    <li :class="{ active : item == 1 }"><a href="about-us.html">About Us</a></li>
                    <li :class="{ active : item == 2 }"><a href="our-services.html">Our Services</a></li>
                    <li :class="{ active : item == 3 }"><a href="contact-us.html">Contact Us</a></li>
                </ul>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 footer-contact">
                <h6>Contact Us</h6>
                <ul>
                    <li><a :href="'tel:'+contact.Phone_Number_1"><i
                                class="fa fa-phone"></i>{{contact.Phone_Number_1}}</a></li>
                    <li><a :href="'mailto:'+contact.Email"><i
                                class="fa fa-envelope"></i>{{contact.Email}}</a></li>
                </ul>
                <div class="socialMedia">
                    <a href="https://www.facebook.com/GulfWingsTravelAgency" target="_blank"><i
                            class="fa fa-facebook"></i></a>
                    <a href="https://twitter.com/GulfWingsTravel" target="_blank"><i
                            class="fa fa-twitter"></i></a>
                    <a href="https://www.instagram.com/GulfWings/" target="_blank"><i
                            class="fa fa-instagram"></i></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="newsletter">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <h5><span>SUBSCRIBE OUR</span><br>NEWSLETTER</h5>
                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                            <input type="text" placeholder="Email Address" v-model="newsltremail">
                            <button v-on:click="sendnewsletter">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 copyright">
                <p>© 2020 All Rights Reserved Terms of Use and Privacy Policy</p>
            </div>
        </div>
    </div>
   `,
    data() {
        return {
            commonStore: vueCommonStore.state,
            agencyCode: '',
            contact: { Phone_Number_1: '' },
            newsltremail: null,
            Logo:{},
            notificationEmail: ""
        }
    },
    methods: {
        getPagefooter: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {

                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Contact Us/Contact Us/Contact Us.ftl';
                        var Page = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Home/Home/Home.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var Notification = self.pluck('Notifications', response.data.area_List);
                            if (Notification.length > 0) {
                                self.notificationEmail = self.pluckcom('Notification_Email', Notification[0].component);
                            }
                            var contact = self.pluck('Contacts', self.data.area_List);
                            if (contact.length > 0) {
                                self.contact.Address = self.pluckcom('Address', contact[0].component);
                                self.contact.Phone_Number_1 = self.pluckcom('Phone_Number_1', contact[0].component);
                                self.contact.Phone_Number_2 = self.pluckcom('Phone_Number_2', contact[0].component);
                                self.contact.Email = self.pluckcom('Email', contact[0].component);
                            }
                        }).catch(function (error) {
                            console.log('Error');
                            self.content = [];
                        });
                        axios.get(Page, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var Logo = self.pluck('Logo', self.data.area_List);
                            if (Logo.length > 0) {
                                self.Logo.Image = self.pluckcom('Image', Logo[0].component);
                            }
                        }).catch(function (error) {
                            self.content = [];
                        });
                    }

                });

            });

        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        },
        sendnewsletter: async function () {

            if (!this.newsltremail) {
                alertify.alert('Alert', 'Email Id required.');
                return false;;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.newsltremail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.');
                return false;
            } else {
                var agencyCode = this.agencyCode
                var filterValue = "type='Newsletter' AND keyword1='" + this.newsltremail + "'";
                var allDBData = await this.getDbData4Table(agencyCode, filterValue, "date1");

                if (allDBData != undefined && allDBData.length > 0) {
                    alertify.alert('Alert', 'Email address already enabled.').set('closable', false);
                    return false;
                }
                else {
                    var postData = {
                        type: "UserAddedRequest",
                        fromEmail: this.notificationEmail || this.commonStore.fallBackEmail,
                        toEmails: Array.isArray(this.newsltremail) ? this.newsltremail : [this.newsltremail],
                        logo:this.Logo.Image || "",
                        agencyName: "GULF WINGS TRAVEL AGENCY",
                        agencyAddress: this.contact.Address || "",
                        personName: this.newsltremail.split("@")[0],
                        primaryColor: "#FFFFFF",
                        secondaryColor: "#1c4c99"
                    };
                    var agencyCode = this.agencyCode
                    let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                    let insertSubscibeData = {
                        type: "Newsletter",
                        date1: requestedDate,
                        keyword1: this.newsltremail,
                        keyword2: "Subscribe Newsletter",
                        nodeCode: agencyCode
                    };
                    let responseObject = await this.cmsRequestData("POST", "cms/data", insertSubscibeData, null);
                    try {
                        let insertID = Number(responseObject);
                        var self = this;
                        mailUrl = self.commonStore.hubUrls.emailServices.emailApi;
                        sendMailService(mailUrl, postData);
                        this.newsltremail = '';
                        alertify.alert('Newsletter', 'Thank you for subscribing !');
                    } catch (e) {

                    }
                }

            }
        },
        async getDbData4Table(agencyCode, extraFilter, sortField) {

            var allDBData = [];
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            var cmsURL = huburl + portno + '/cms/data/search/byQuery';
            var queryStr = "select * from cms_forms_data where nodeCode = '" + agencyCode + "'";
            if (extraFilter != undefined && extraFilter != '') {
                queryStr = queryStr + " AND " + extraFilter;
            }
            var requestObject = {
                query: queryStr,
                sortField: sortField,
                from: 0,
                orderBy: "desc"
            };
            let responseObject = await this.cmsRequestData("POST", "cms/data/search/byQuery", requestObject, null);
            if (responseObject != undefined && responseObject.data != undefined) {
                allDBData = responseObject.data;
            }
            return allDBData;

        }

    },
    mounted: function () {
        this.getPagefooter();
    },

})
var footerinstance = new Vue({
    el: 'footer',
    name: 'footerArea',
    data() {
        return {
            key: 0,
            content: null,
            getdata: true
        }

    },
});