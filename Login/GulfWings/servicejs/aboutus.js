var aboutus = new Vue({
    el: '#aboutuspage',
    name: 'aboutuspage',
    data: {
        commonStore: vueCommonStore.state,
        agencyCode: '',
        data: null,
        About: { About_Us_Description: '' },
        contact: { Phone_Number_1: '' },
        Intro: {Content:''},
        Mes: {},
        Activities: {},
    },
    filters: {

        subStr: function (string) {
            if (string.length > 100)
                return string.substring(0, 100) + '...';

            else
                return string;
        }

    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Contact Us/Contact Us/Contact Us.ftl';
                        var about_Page = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/About Us/About Us/About Us.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var contact = self.pluck('Contacts', self.data.area_List);
                            if (contact.length > 0) {
                                self.contact.Address = self.pluckcom('Address', contact[0].component);
                                self.contact.Phone_Number_1 = self.pluckcom('Phone_Number_1', contact[0].component);
                                self.contact.Phone_Number_2 = self.pluckcom('Phone_Number_2', contact[0].component);
                                self.contact.Email = self.pluckcom('Email', contact[0].component);
                            }
                        }).catch(function (error) {
                            console.log('Error');
                            self.content = [];
                        });
                        axios.get(about_Page, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var Intro = self.pluck('Introduction', self.data.area_List);
                            if (Intro.length > 0) {
                                self.Intro.Content = self.pluckcom('Content', Intro[0].component);
                                self.Intro.List = self.pluckcom('List', Intro[0].component);

                            }
                            var Mes = self.pluck('MD_Message', self.data.area_List);
                            if (Mes.length > 0) {
                                self.Mes.MD_Image = self.pluckcom('MD_Image', Mes[0].component);
                                self.Mes.Message = self.pluckcom('Message', Mes[0].component);
                            }
                            var Activities = self.pluck('Introduction_And_Core_Activities', self.data.area_List);
                            if (Activities.length > 0) {
                                self.Activities.Introduction_List = self.pluckcom('Introduction_List', Activities[0].component);
                                self.Activities.Core_Activities_List = self.pluckcom('Core_Activities_List', Activities[0].component);
                            }
                            var About = self.pluck('About_Us', self.data.area_List);
                            if (About.length > 0) {
                                self.About.About_Us_Description = self.pluckcom('About_Us_Description', About[0].component);
                            }
                        }).catch(function (error) {
                            console.log('Error');
                        });
                    }

                });

            });

        }
    },
    mounted: function () {
        sessionStorage.active_el=1; 
        this.getPagecontent();
    },
});