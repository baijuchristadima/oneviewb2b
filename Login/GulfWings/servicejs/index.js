var home = new Vue({
    el: '#homepage',
    name: 'homepage',
    data: {
        commonStore: vueCommonStore.state,
        Intro: {
            Introduction_Content: ''
        },
        feature: {
            Features_List: null
        },
        agencyCode: '',
        Partners: {},

    },
    // filters: {

    //     subStr: function (string) {
    //         if (string.length > 100)
    //             return string.substring(0, 300) + '...';

    //         else
    //             return string;
    //     }

    // },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            //var agencyCode = document.getElementById("txtAgencyCode").value;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {

                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var about_Page = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/About Us/About Us/About Us.ftl';
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Contact Us/Contact Us/Contact Us.ftl';
                        var Page = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Home/Home/Home.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(about_Page, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var abouts = self.pluck('Services_Area', self.data.area_List);
                            if (abouts.length > 0) {
                                self.abouts.Services = self.pluckcom('Services', abouts[0].component);
                            }
                        }).catch(function (error) {
                            //console.log('Error');
                        });
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var contact = self.pluck('Contacts', self.data.area_List);
                            if (contact.length > 0) {
                                self.contact.Address = self.pluckcom('Address', contact[0].component);
                                self.contact.Phone_Number_1 = self.pluckcom('Phone_Number_1', contact[0].component);
                                self.contact.Phone_Number_2 = self.pluckcom('Phone_Number_2', contact[0].component);
                                self.contact.Email = self.pluckcom('Email', contact[0].component);
                            }


                        }).catch(function (error) {
                            //console.log('Error');
                            self.content = [];
                        });
                        axios.get(Page, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var feature = self.pluck('Features', self.data.area_List);
                            if (feature.length > 0) {
                                self.feature.Features_List = self.pluckcom('Features_List', feature[0].component);
                            }
                            var Intro = self.pluck('Introduction', self.data.area_List);
                            if (feature.length > 0) {
                                self.Intro.Introduction_Content = self.pluckcom('Introduction_Content', Intro[0].component);
                            }


                        }).catch(function (error) {
                            //console.log('Error');
                            self.content = [];
                        });
                        // axios.get(Partner, {
                        //     headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        // }).then(function (response) {
                        //     self.data = response.data;
                        //     var Partners = self.pluck('Partners_Image', self.data.area_List);
                        //     if (Partners.length > 0) {
                        //         self.Partners.Image = self.pluckcom('Image', Partners[0].component);
                        //     }

                        // }).catch(function (error) {
                        //     console.log('Error');
                        // });

                    }

                });

            });

        }
    },
    mounted: function () {
        this.getPagecontent();
        sessionStorage.active_el=0;
        //this.partnersLogo();
    },


});