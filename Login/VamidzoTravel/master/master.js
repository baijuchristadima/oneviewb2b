var HeaderComponent = Vue.component('headeritem', {
    template: ` 
    <div id="header">
    <div class="vld-parent">
    <loading :active.sync="isLoading" :can-cancel="true" :is-full-page="fullPage"></loading>
</div>
    <div class="header-top">
    <div class="container relative">
       <div class="row">
           <div class="col-md-9 col-sm-9">
               <div class="top-nav">
               <nav id="navbar-main" class="navbar navbar-default">
                <div class="navbar-header">
                    <a href="index.html" class="navbar-brand"><img :src="TopSection.Logo_188x70px" alt="logo"></a>
                   <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                </div>
                <div class="collapse navbar-collapse navbar-right" id="myNavbar">
                   <ul class="nav navbar-nav">
                      <li :class="{ 'active': activeClass === '/index.html'}"><a href="index.html">{{TopSection.Home_Label}}</a></li>
                      <li :class="{ 'active': activeClass === '/about.html'}"><a href="about.html">{{TopSection.About_Us_Label}}</a></li>
                      <li :class="{ 'active': activeClass === '/services.html'}"><a href="services.html">{{TopSection.Service_Label}}</a></li>
                       <li :class="{ 'active': activeClass === '/contact.html'}"><a href="contact.html">{{TopSection.Contact_Us_Label}}</a></li>
                   </ul>
                </div>
             </nav>
           </div>
               </div>
           
            <div class="col-md-3 col-sm-3">
             <div class="header-contact">
                 <span><i class="fa fa-headphones"></i>{{TopSection.Customer_Service_}}</span> <a :href="'tel:'  + TopSection.Customer_Service_Number">{{TopSection.Customer_Service_Number}} </a></div>
          </div>
           
       </div>
    </div>
 </div>
  </div>`,
    data() {

        return {
            TopSection: {},
            agencyCode: '',
            contact: { Phone_Number_1: '' },
            active_er: (sessionStorage.active_er) ? sessionStorage.active_er : 1,
            isLoading: false,
            fullPage: true,
            activeClass: '',

        }
    },
    created() {
        var self = this;
        var pathArray = window.location.pathname.split('/');
        self.activeClass = '/' + pathArray[pathArray.length - 1];

    },
    methods: {
        activate: function(el) {
            sessionStorage.active_er = el;
            this.active_er = el;
            if (el == 1 || el == 2) {
                maininstance.actvetab = el;
            } else {
                maininstance.actvetab = 0;
            }
        },
        getPageheader: function() {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function(response) {
                // self.isLoading = true;
                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function(agent, agentIndex) {
                        agent.domain.forEach(function(dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) {
                    // self.isLoading = false;
                }
                agy.registeredUsers.forEach(function(agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/01 Master/01 Master/01 Master.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function(response) {
                            self.data = response.data;
                            var mainData = self.pluck('Header_Section', self.data.area_List);
                            self.TopSection = self.getAllMapData(mainData[0].component);
                            // self.isLoading = false;
                        }).catch(function(error) {
                            self.isLoading = false;
                            console.log('Error');
                        });
                    }

                });

            });

        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getAllMapData: function(contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function(item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },
        getValidationMsgs: function() {

            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function(response) {

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function(agent, agentIndex) {
                        agent.domain.forEach(function(dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) {}
                agy.registeredUsers.forEach(function(agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Validation/Validation/Validation.ftl';

                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function(response) {
                            if (response.data.area_List.length) {

                                var validationData = self.pluck("Validations", response.data.area_List);
                                self.validationList = self.getAllMapData(validationData[0].component);
                                sessionStorage.setItem('validationItems', JSON.stringify(self.validationList));

                            }
                        }).catch(function(error) {
                            console.log('Error');
                        });
                    }

                });

            });


        },
    },
    mounted: function() {

        this.getPageheader();
        this.getValidationMsgs();
    },
})
var headerinstance = new Vue({
    el: 'header',
    name: 'headerArea',
    data() {
        return {
            key: '',
            content: null,
            getdata: true
        }

    },

});
Vue.component('footeritem', {
    template: `
    <div id="footer">
    <div class="vld-parent">
    <loading :active.sync="isLoading" :can-cancel="true" :is-full-page="fullPage"></loading>
</div>

    <div class="partners">
    <div id="owl-demo-3" class="owl-carousel owl-theme">

        <div class="item" v-for="partner in partnerSection.Image_List">
            <img :src="partner.Image_279x126px" alt="partners">
        </div>

    </div>
</div> 
    <div class="footer-in">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="about-footer">
                            <div class="ftr-logo">
                                <a href="index.html"><img :src="FooterSection.Logo_184x69px" alt="footer-logo"></a>
                            </div>
                            <h4>{{FooterSection.Address_Label}}</h4>
                            <p v-html="FooterSection.Address_Content"></p>

                        </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                        <div class="quick-link">
                            <h3>{{FooterSection.Quick_Link_Label}}</h3>
                            <ul class="ft-link">
                                <li><a href="index.html">{{HeaderSection.Home_Label}}</a></li>
                                <li><a href="about.html">{{HeaderSection.About_Us_Label}}</a></li>
                                <li><a href="services.html">{{HeaderSection.Service_Label}}</a></li>
                                <li><a href="contact.html">{{HeaderSection.Contact_Us_Label}}</a></li>
                                <li><a href="privacy-policy.html">{{HeaderSection.Privacy_Policy_Label}}</a></li>
                                <li><a href="terms.html">{{HeaderSection.Terms_And_Conditions_Label}}</a></li>
                            </ul>

                        </div>
                    </div>

                    <div class="col-md-4 col-sm-4">
                        <div class="quick-link">
                            <h3>{{HeaderSection.Contact_Us_Label}}</h3>
                            <ul class="ftr-contact">
                                <li><i :class="FooterSection.Phone_Icon"></i><a :href="'tel:' + FooterSection.Phone_Number">{{FooterSection.Phone_Number}}</a></li>
                                <li><i :class="FooterSection.Email_Icon"></i><a :href="'mailto:' + FooterSection.Email_ID">{{FooterSection.Email_ID}}</a></li>
                            </ul>

                            <ul class="social">
                                <li v-for="meadiaItem in FooterSection.Social_Media_List"><a :href="meadiaItem.Path"  target="_blank"><i aria-hidden="true" :class="meadiaItem.Icon"></i></a></li>
                              
                            </ul>
                        </div>
                    </div>


                    <div class="col-md-12 col-sm-12">
                        <div class="ft-newsletter">
                            <div class="row">
                                <div class="col-md-4 col-sm-4">
                                    <h3><span>{{NewsletterSection.Newsletter_Heading1}}</span><br> {{NewsletterSection.Newsletter_Heading2}}</h3>
                                </div>
                                <div class="col-md-8 col-sm-8">
                                    <input type="email" id="email" name="email" :placeholder="NewsletterSection.Email_Placeholder" v-model="newsltremail">
                                    <button type="submit" v-on:click="sendnewsletter">{{NewsletterSection.Submit_Button}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>


            <div class="container">
                <div class="ftr-btm">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="copyright">{{FooterSection.Copyright_Description}}</div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="powered-by">{{FooterSection.Powered_By_Label}} &nbsp;
                                <a href="http://www.oneviewit.com/" target="_blank"><img alt="" :src="FooterSection.Powered_By_Logo_85x30px"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

  </div>`,
    data() {
        return {
            commonStore: vueCommonStore.state,
            Logos: { Logo: '' },
            agencyCode: '',
            FooterSection: {},
            HeaderSection: {},
            NewsletterSection: {},
            partnerSection: {},
            newsltremail: '',
            isLoading: false,
            fullPage: true,
        }
    },
    methods: {
        getPagefooter: function() {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function(response) {
                // self.isLoading = true;
                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function(agent, agentIndex) {
                        agent.domain.forEach(function(dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) {
                    self.isLoading = false;
                }

                agy.registeredUsers.forEach(function(agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var Footer = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/01 Master/01 Master/01 Master.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(Footer, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function(response) {
                            self.content = response.data;
                            if (self.content != undefined && self.content != null) {
                                var mainData = self.pluck('Footer_Section', self.content.area_List);
                                self.FooterSection = self.getAllMapData(mainData[0].component);
                                var meData = self.pluck('Header_Section', self.content.area_List);
                                self.HeaderSection = self.getAllMapData(meData[0].component);
                                var newsleter = self.pluck('Newsletter_Section', self.content.area_List);
                                self.NewsletterSection = self.getAllMapData(newsleter[0].component);
                                self.isLoading = false;
                            }

                        }).catch(function(error) {
                            console.log('Error');
                            self.isLoading = false;
                            self.content = [];
                        });
                    }

                });

            });

        },
        getPagepartner: function() {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function(response) {
                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function(agent, agentIndex) {
                        agent.domain.forEach(function(dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) {}

                agy.registeredUsers.forEach(function(agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var partner = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Partners/Partners/Partners.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(partner, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function(response) {
                            self.content = response.data;
                            if (self.content != undefined && self.content != null) {
                                var mainData = self.pluck('Partners_Section', self.content.area_List);
                                self.partnerSection = self.getAllMapData(mainData[0].component);
                                setTimeout(() => {
                                    this.carosal();
                                }, 200);

                            }

                        }).catch(function(error) {
                            console.log('Error');
                            self.content = [];
                        });
                    }

                });

            });

        },
        getAllMapData: function(contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function(item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        sendnewsletter: async function() {
            var self = this;
            if (!this.newsltremail) {
                alertify.alert(this.getValidationMsgByCode('M11'), this.getValidationMsgByCode('M02')).set('closable', false);
                return false;;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.newsltremail.match(emailPat);
            if (matchArray == null) {
                alertify.alert(this.getValidationMsgByCode('M11'), this.getValidationMsgByCode('M04')).set('closable', false);
                return false;
            } else {
                self.isLoading = true;
                var agencyCode = this.agencyCode
                var filterValue = "type='Newsletter' AND keyword1='" + this.newsltremail + "'";
                var allDBData = await this.getDbData4Table(agencyCode, filterValue, "date1");

                if (allDBData != undefined && allDBData.length > 0) {
                    alertify.alert(this.getValidationMsgByCode('M11'), this.getValidationMsgByCode('M12')).set('closable', false);
                    self.isLoading = false;
                    return false;
                } else {
                    var frommail = "itsolutionsoneview@gmail.com";
                    var postData = {
                        type: "UserAddedRequest",
                        // fromEmail: this.notificationEmail || this.commonStore.fallBackEmail,
                        fromEmail: frommail,
                        toEmails: Array.isArray(this.newsltremail) ? this.newsltremail : [this.newsltremail],
                        logo: this.NewsletterSection.Email_Logo170x40px || "",
                        agencyName: "Vamidzo Travel",
                        agencyAddress: this.FooterSection.Address_Content || "",
                        personName: this.newsltremail.split("@")[0],
                        primaryColor: "#ffffff",
                        secondaryColor: "#94c11f"
                    };
                    var agencyCode = this.agencyCode
                    let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                    let insertSubscibeData = {
                        type: "Newsletter",
                        date1: requestedDate,
                        keyword1: this.newsltremail,
                        nodeCode: agencyCode
                    };
                    let responseObject = await this.cmsRequestData("POST", "cms/data", insertSubscibeData, null);
                    try {
                        let insertID = Number(responseObject);
                        var self = this;
                        mailUrl = self.commonStore.hubUrls.emailServices.emailApi;
                        sendMailService(mailUrl, postData);
                        this.newsltremail = '';
                        alertify.alert(this.getValidationMsgByCode('M09'), this.getValidationMsgByCode('M13')).set('closable', false);
                        self.isLoading = false;
                    } catch (e) {
                        self.isLoading = false;
                    }
                }

            }
        },
        async getDbData4Table(agencyCode, extraFilter, sortField) {
            try {
                var allDBData = [];
                var huburl = HubServiceUrls.hubConnection.cmsUrl;
                var portno = HubServiceUrls.hubConnection.ipAddress;
                var cmsURL = huburl + portno + '/cms/data/search/byQuery';
                var queryStr = "select * from cms_forms_data where nodeCode = '" + agencyCode + "'";
                if (extraFilter != undefined && extraFilter != '') {
                    queryStr = queryStr + " AND " + extraFilter;
                }
                var requestObject = {
                    query: queryStr,
                    sortField: sortField,
                    from: 0,
                    orderBy: "desc"
                };
                let responseObject = await this.cmsRequestData("POST", "cms/data/search/byQuery", requestObject, null);
                if (responseObject != undefined && responseObject.data != undefined) {
                    allDBData = responseObject.data;
                }
                return allDBData;
            } catch (error) {
                self.isLoading = false;
            }


        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            try {
                var huburl = HubServiceUrls.hubConnection.cmsUrl;
                var portno = HubServiceUrls.hubConnection.ipAddress;
                const url = huburl + portno + "/" + urlParam;
                if (data != null) {
                    data = JSON.stringify(data);
                }
                const response = await fetch(url, {
                    method: callMethod, // *GET, POST, PUT, DELETE, etc.
                    credentials: "same-origin", // include, *same-origin, omit
                    headers: { 'Content-Type': 'application/json' },
                    body: data, // body data type must match "Content-Type" header
                });
                try {
                    const myJson = await response.json();
                    return myJson;
                } catch (error) {
                    return object;
                }
            } catch (error) {
                self.isLoading = false;
            }

        },
        getValidationMsgByCode: function(code) {
            if (sessionStorage.validationItems !== undefined) {
                var validationList = JSON.parse(sessionStorage.validationItems);
                for (let validationItem of validationList.Validation_List) {
                    if (code === validationItem.Code) {
                        return validationItem.Message;
                    }
                }
            }
        },



    },
    mounted: function() {
        this.getPagefooter();
        this.getPagepartner();
    },

})
var footerinstance = new Vue({
    el: 'footer',
    name: 'footerArea',
    data() {
        return {
            key: 0,
            content: null,
            getdata: true
        }

    },
});
function carosal() {
    $("#owl-demo-3").owlCarousel({
        autoplay: true,
        autoPlay: 3000,
        autoplayHoverPause: true,
        stopOnHover: false,
        items: 6,
        margin: 10,
        lazyLoad: true,
        navigation: true,
        itemsDesktop: [1199, 4],
        itemsDesktopSmall: [991, 3],
        itemsTablet: [767, 2]
    });

    $(".owl-prev").html('<i class="fa  fa-angle-left"></i>');
    $(".owl-next").html('<i class="fa  fa-angle-right"></i>');
}