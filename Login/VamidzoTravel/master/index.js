var maininstance = new Vue({
    el: '#indexPage',
    name: 'indexPage',
    data: {
        agencyCode: '',
        mainContent: {},
        BannerSection: {},
        LoginSection: {},
        MainSection: {},
        isLoading: false,
        fullPage: true,
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getAllMapData: function(contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function(item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },
        getPagecontent: function() {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function(response) {

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function(agent, agentIndex) {
                        agent.domain.forEach(function(dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) {}
                agy.registeredUsers.forEach(function(agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var homePageUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Home/Home/Home.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(homePageUrl, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function(response) {
                            self.content = response.data;
                            if (self.content != undefined && self.content != null) {
                                var bannerData = self.pluck('Banner_Section', self.content.area_List);
                                self.BannerSection = self.getAllMapData(bannerData[0].component);
                                var logData = self.pluck('Login_Section', self.content.area_List);
                                self.LoginSection = self.getAllMapData(logData[0].component);
                                var aboutData = self.pluck('Main_Section', self.content.area_List);
                                self.MainSection = self.getAllMapData(aboutData[0].component);

                            }

                            self.stopLoader();
                        }).catch(function(error) {
                            console.log('Error');
                            self.stopLoader();
                        });
                    }

                });

            });

        },
        stopLoader: function() {
            $('#preloader').delay(50).fadeOut(250);

        }
    },
    mounted: function() {
        this.getPagecontent();

    },
});