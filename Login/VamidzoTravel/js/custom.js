(function($) {


    /*==========Preloader-Js===========*/

    (function($) {
        'use strict';

        // Preloader
        $(window).on('load', function() {
            $('#preloader')
                .delay(500)
                .fadeOut('slow', function() {
                    $(this).remove();
                });
        });
    })(window.jQuery);





    /* Language JS
    ============================================================== */
    $(function() {
        $('.selectpicker').selectpicker();
    });


    /*--Back-to-top--*/
    $(document).ready(function() {

        //Check to see if the window is top if not then display button
        $(window).scroll(function() {
            if ($(this).scrollTop() > 100) {
                $('.scrollToTop').fadeIn();
            } else {
                $('.scrollToTop').fadeOut();
            }
        });

        //Click event to scroll to top
        $('.scrollToTop').click(function() {
            $('html, body').animate({ scrollTop: 0 }, 800);
            return false;
        });

    });




    /*--nav-dropdown-hover--*/
    $('ul.nav li.dropdown').hover(function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).slideDown(300);
    }, function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).slideUp(300);
    });





    /*--------Offer-Slider---------*/
    //  $(document).ready(function() {

    //   $("#owl-demo-3").owlCarousel({
    //     autoplay:true,
    //     autoPlay : 8000,
    //     autoplayHoverPause:true, 
    //     stopOnHover : false,  
    //     items : 6,
    //     margin:10,  
    //     lazyLoad : true,
    //     navigation : true,
    //     itemsDesktop : [1199, 4],
    //     itemsDesktopSmall : [991,3],
    //       itemsTablet : [767, 2]
    //   });

    //      $( ".owl-prev").html('<i class="fa  fa-angle-left"></i>');
    //      $( ".owl-next").html('<i class="fa  fa-angle-right"></i>');
    // });





})(jQuery);