/*owl carousel start */
$(document).ready(function(){
    $(".forgot_click").click(function () {
        $(".loginform").hide();
        $(".forgot_form").show();
    });
    $(".back").click(function () {
        $(".loginform").show();
        $(".forgot_form").hide();
    });
})
$(function () {
    $('.owl-carousel').owlCarousel({
        loop: true,
        autoplay: true,

        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true

            },
            950: {
                items: 2,
                nav: true
            },
            1006: {
                items: 3,

            }
        }
    })

    $("#homepackage").owlCarousel({
        navigation: true, // Show next and prev buttons
        slideSpeed: 2000,
        pagination: false,
        paginationSpeed: 2000,
        singleItem: false,
        dots: true,
        mouseDrag: true,
        items: 3,
        transitionStyle: "goDown",
        itemsCustom: [
            [0, 1],
            [450, 1],
            [600, 2],
            [700, 2],
            [1000, 2],
            [1200, 3]
        ],
    });




});

/*owl carousel end */


/*-----------------------------------------*/

/*Smooth scrolling start */

$(document).ready(function () {
    // Select all links with hashes
    $('a[href*="#"]')
        // Remove links that don't actually link to anything
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function (event) {
            // On-page links
            if (
                location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
                location.hostname == this.hostname
            ) {
                // Figure out element to scroll to
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                // Does a scroll target exist?
                if (target.length) {
                    // Only prevent default if animation is actually gonna happen
                    event.preventDefault();
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000, function () {
                        // Callback after animation
                        // Must change focus!
                        var $target = $(target);
                        $target.focus();
                        if ($target.is(":focus")) { // Checking if the target was focused
                            return false;
                        } else {
                            $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                            $target.focus(); // Set focus again
                        };
                    });
                }
            }
        });
    
});
/*Smooth scrolling end */

/*-----------------------------------------*/

$(function () {
    var viewportWidth = $(window).width();
    if (viewportWidth < 768) {
        $("body").removeClass("menuadd").addClass("pushmenu-push");
    }
});

/*Wow js start */
$(function () {
    new WOW().init();

});

/*Wow js end */

