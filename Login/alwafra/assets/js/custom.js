

/* Scroll To Top JS
============================================================== */
jQuery(window).scroll(function () {
    scrollToTop('show');
});

jQuery(document).ready(function () {
    
    $(".forgot_click").click(function () {
        $(".login-form").hide();
        $(".forgot_form").show();
    });
    $(".back").click(function () {
        $(".login-form").show();
        $(".forgot_form").hide();
    });
    scrollToTop('click');
});

/* Animated Function */
function scrollToTop(i) {
    if (i == 'show') {
        if (jQuery(this).scrollTop() != 0) {
            jQuery('#toTop').fadeIn();
        } else {
            jQuery('#toTop').fadeOut();
        }
    }
    if (i == 'click') {
        jQuery('#toTop').click(function () {
            jQuery('body,html').animate({
                scrollTop: 0
            }, 500);
            return false;
        });
    }
}


