﻿var AgencyInformation = {
    AirInformation: {
        homeUrl: "Flights/index.html",
        totalBookingingsHome: 5,
        AirFolderName: "Flights",

    },
    hotelService: {
        homeUrl: "Hotels/hotelsearch.html",
        totalBookingingsHome: 5,
    },
    sightseeingService: {
        homeUrl: "sightseeing/index.html",
        totalBookingingsHome: 5,
    },
    insuranceService: {
        homeUrl: "Insurance/index.html",
        totalBookingingsHome: 5,
    },
    transferService: {
        homeUrl: "Insurance/index.html",
        totalBookingingsHome: 5,
    },
    DomainUrl: "http://b2b.oneviewitsolutions.com",
    systemSettings: {
        systemDateFormat: 'dd M y,D',
        calendarDisplay: 2,
        calendarDisplayInMobile: 1
    },


    AgencyAddress: {
        AgencyName: "Elite Travel",
        AgencyTitle: "Elite Travel | Online Booking for Cheap Flights &amp; Airline Tickets",
        AgencyShortName: "Elite Travel",
        AgencyAddress: "Loren Ipsum dummy text lorem",
        AboutAgency: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, Lorem ipsum dolor sit amet, consetetur sadipscing elitr,",
        AgencyWebsite: "http://www.oneviewit.com",
        Helpline: "+123 456 7890",
        Email: "loremipsum@xyz.com",
        CopyRight: "Copyright © 2019 Elite Travel All rights reserved.",
        Facebook: "http://www.fb.com/oneviewitsolutions",
        Twitter: "http://www.twitter.com/oneviewitsolutions",
        GooglePlus: "http://plus.google.com/oneviewitsolutions",
        Linkedln: "http://www.linkedin.com/oneviewitsolutions",
        GooglePlayStore: "",
        AppStore: "",
        YouTube: ""
    }
}