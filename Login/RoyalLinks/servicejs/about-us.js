var aboutus = new Vue({
  el: '#about',
  name: 'about',
  data: {
    commonStore: vueCommonStore.state,
    agencyCode: '',
    BannerSection: {},
    AboutUsSection: {},
    mainSection: {},
  },
  methods: {
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getPagecontent: function () {
      var self = this;
      axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
        var agencyFolderName = '';
        var agy;
        try {
          response.data.forEach(function (agent, agentIndex) {
            agent.domain.forEach(function (dom, domIndex) {
              if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                agy = agent;
                agencyFolderName = agy.agencyFolderName;
              }
            });
          });
        } catch (err) { }

        agy.registeredUsers.forEach(function (agyCode, domIndex) {
          if (domIndex == 0) {
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            var agencyCode = agyCode;
            self.agencyCode = agencyCode;
            var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agencyCode + '/Template/About Us/About Us/About Us.ftl';
            axios.get(pageurl, {
              headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
            }).then(function (response) {
              var content = response.data;
              if (response.data.area_List.length) {
                var bannerDetails = self.pluck('Banner_Section', content.area_List);
                if (bannerDetails != undefined) {
                  self.BannerSection = self.getAllMapData(bannerDetails[0].component);
                }
                var AboutUsData = self.pluck('About_Us_Section', content.area_List);
                if (AboutUsData != undefined) {
                  self.AboutUsSection = self.getAllMapData(AboutUsData[0].component);
                }
                var AboutUs = self.pluck('Main_Section', content.area_List);
                if (AboutUs != undefined) {
                  self.mainSection = self.getAllMapData(AboutUs[0].component);
                }
              }
            }).catch(function (error) {
              console.log('Error');
              self.BannerSection = [];
              self.AboutUsSection = [];
              self.mainSection = [];
            });
          }

        });

      });

    },
    getAllMapData: function (contentArry) {
      var tempDataObject = {};
      if (contentArry != undefined) {
        contentArry.map(function (item) {
          let allKeys = Object.keys(item)
          for (let j = 0; j < allKeys.length; j++) {
            let key = allKeys[j];
            let value = item[key];
            if (key != 'name' && key != 'type') {
              if (value == undefined || value == null) {
                value = "";
              }
              tempDataObject[key] = value;
            }
          }
        });
      }
      return tempDataObject;
    },
  },
  mounted: function () {
    sessionStorage.active_er = 2;
    this.getPagecontent();
  },
});
