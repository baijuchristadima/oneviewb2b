var maininstance = new Vue({
  el: '#homepage',
  name: 'homepage',
  data: {
    commonStore: vueCommonStore.state,
    agencyCode: '',
    packages: [],
    homePageContent: [],
    content: '',
    bannerImage: null,
    mainTitle:null,
    subTitle : null
    // agyyCode: 20510
  },
  filters: {

    subStr: function (string) {
      if (string.length > 8)
        return string.substring(0, 9) + '...';

      else
        return string;
    }

  },
  methods: {
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getPagecontent: function () {
      var self = this;
      axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
        //console.log(document.location.hostname.toLowerCase());

        var agencyFolderName = '';
        var agy;
        try {
          response.data.forEach(function (agent, agentIndex) {
            agent.domain.forEach(function (dom, domIndex) {
              if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                agy = agent;
                agencyFolderName = agy.agencyFolderName;
              }
            });
          });
        } catch (err) { }

        agy.registeredUsers.forEach(function (agyCode, domIndex) {
          if (domIndex == 0) {
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            var Packageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Master Table/Packages/Packages/Packages.ftl';
            var homePageUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Home Page/Home Page/Home Page.ftl';
            var agencyCode = agyCode;
            self.agencyCode = agencyCode;
          
            axios.get(homePageUrl, {
                headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
              }).then(function (response) {
                console.log('responsee', response);
                self.content = response.data;
                if (self.content != undefined && self.content != null) {
                  self.homePageContent = self.pluck('Home_Page_Area',self.content.area_List);
                  if (self.homePageContent.length > 0) {
                    self.bannerImage = self.pluckcom('Background_Image',self.homePageContent[0].component);
                    self.mainTitle = self.pluckcom('Main_Heading',self.homePageContent[0].component);
                    self.subTitle = self.pluckcom('Sub_Heading',self.homePageContent[0].component);
                  }
                 
                 
                }
              });
          }

        });

      });

    },
  },
  mounted: function () {
    sessionStorage.active_er = 1;
    this.getPagecontent();
  },
});
