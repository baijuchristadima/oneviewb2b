var contact = new Vue({
    el: '#contactuspage',
    name: 'contactuspage',
    data: {
        commonStore: vueCommonStore.state,
        contactBanner: { banner: '' },
        contactSection: {
            Content_First_Title: '',
            Content_Main_Title: '',
        },
        policy: { header: '', date: '' },
        Location: { Map: '' },
        privacyContent: { Content: '' },
        termsConditions:{header: '', date: '', contentList:''},
        notificationEmail: ""
    },
    methods: {
        dateFormat: function (d) {
            return moment(d).format('DD-MMM-YYYY');
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Contact Us/Contact Us/Contact Us.ftl';
                        var home = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Home/Home/Home.ftl';
                        var privacy = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Privacy Policy/Privacy Policy/Privacy Policy.ftl';
                        var terms = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Terms and Conditions/Terms and Conditions/Terms and Conditions.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            // var contact = self.pluck('Contact_Details', self.data.area_List);
                            // if (contact.length > 0) {
                            //     self.contact.Address = self.pluckcom('Address', contact[0].component);
                            //     self.contact.Phone_Number_1 = self.pluckcom('Phone_Number', contact[0].component);
                            //     self.contact.Email = self.pluckcom('Email', contact[0].component);
                            //     self.contact.Location_Link = self.pluckcom('Location_Link', contact[0].component);
                            // }

                            var Notification = self.pluck('Notifications', response.data.area_List);
                            if (Notification.length > 0) {
                                self.notificationEmail = self.pluckcom('Notification_Email', Notification[0].component);
                            }
                            var contactBanner = self.pluck('Banner_Area', self.data.area_List);
                            if (contactBanner.length > 0) {
                                self.contactBanner.banner = self.pluckcom('Banner_Image', contactBanner[0].component);
                            }

                            var contactSection = self.pluck('Content_Section', self.data.area_List);
                            if (contactSection.length > 0) {
                                self.contactSection.Content_First_Title = self.pluckcom('Content_First_Title', contactSection[0].component);
                                self.contactSection.Content_Main_Title = self.pluckcom('Content_Main_Title', contactSection[0].component);
                                self.contactSection.ContactList = self.pluckcom('Details_Section', contactSection[0].component);
                            }

                            var Location = self.pluck('Map_Section', self.data.area_List);
                            if (Location.length > 0) {
                                self.Location.Map = self.pluckcom('Map', Location[0].component);
                            }

                        }).catch(function (error) {
                        });

                        axios.get(privacy, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var policy = self.pluck('Header_Section', self.data.area_List);
                            if (policy.length > 0) {
                                self.policy.header = self.pluckcom('Title', policy[0].component);
                                self.policy.date = self.pluckcom('Date', policy[0].component);
                            }
                            var privacyContent = self.pluck('Content_Section', self.data.area_List);
                            if (privacyContent.length > 0) {
                                self.privacyContent.Content = self.pluckcom('Content', privacyContent[0].component);
                                self.privacyContent.PolicyList = self.pluckcom('Policy_Title', privacyContent[0].component);
                            }
                        }).catch(function (error) {
                        });

                        axios.get(terms, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var termsConditions = self.pluck('Header_Section', self.data.area_List);
                            if (termsConditions.length > 0) {
                                self.termsConditions.header = self.pluckcom('Title', termsConditions[0].component);
                                self.termsConditions.date = self.pluckcom('Date', termsConditions[0].component);
                                self.termsConditions.contentList = self.pluckcom('Content_Section', termsConditions[0].component);
                            }
                            
                        }).catch(function (error) {
                        });

                        axios.get(home, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var Logos = self.pluck('Common_Area', self.data.area_List);
                            if (Logos.length > 0) {
                                self.Logos.Logo = self.pluckcom('Logo', Logos[0].component);
                            }
                        }).catch(function (error) {
                        });

                    }

                });

            });

        },
        sendcontactus: async function () {
            if (!this.cntname) {
                alertify.alert('Alert', 'First name required.').set('closable', false);
                return false;
            }
            if (!this.cntemail) {
                alertify.alert('Alert', 'Email Id required.').set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.cntemail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                return false;
            }
            if (!this.cntcontact) {
                alertify.alert('Alert', 'Mobile number required.').set('closable', false);
                return false;
            }
            if (this.cntcontact.length < 8) {
                alertify.alert('Alert', 'Enter valid mobile number.').set('closable', false);
                return false;
            }
            if (!this.cntsubject) {
                alertify.alert('Alert', 'Subject required.').set('closable', false);
                return false;
            } if (!this.cntmessage) {
                alertify.alert('Alert', 'Message required.').set('closable', false);
                return false;
            } else {
                var self = this;
                var frommail = this.notificationEmail || this.commonStore.fallBackEmail;
                var postData = {
                    type: "UserAddedRequest",
                    fromEmail: frommail,
                    toEmails: Array.isArray(this.cntemail) ? this.cntemail : [this.cntemail],
                    logo: this.Logos.Logo || "",
                    agencyName: "Shams Abu Dhabi Travel",
                    agencyAddress: this.contact.Address || "",
                    personName: this.cntname || "",
                    primaryColor: "#ffffff",
                    secondaryColor: "#364793"
                };
                var agencyCode = this.agencyCode
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                let insertContactData = {
                    type: "Contact Us",
                    keyword1: this.cntname,
                    keyword2: this.cntemail,
                    keyword3: this.cntsubject,
                    number1: this.cntcontact,
                    text1: this.cntmessage,
                    date1: requestedDate,
                    nodeCode: agencyCode
                };
                let responseObject = await this.cmsRequestData("POST", "cms/data", insertContactData, null);
                try {
                    let insertID = Number(responseObject);
                    var self = this;
                    mailUrl = self.commonStore.hubUrls.emailServices.emailApi;
                    sendMailService(mailUrl, postData);
                    this.cntemail = '';
                    this.cntfirstname = '';
                    this.cntlastname = '';
                    this.cntcontact = '';
                    this.cntmessage = '';
                    alertify.alert('Contact us', 'Thank you for contacting us.We shall get back to you.');
                } catch (e) {

                }



            }
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var vm = this;
            var huburl = vm.commonStore.hubUrls.hubConnection.cmsUrl;
            var portno = vm.commonStore.hubUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        }
    },
    mounted: function () {
        this.getPagecontent();
        sessionStorage.active_er = 3;
    },
});