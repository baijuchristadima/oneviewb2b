var aboutus = new Vue({
    el: '#homepage',
    name: 'homepage',
    data: {
        commonStore: vueCommonStore.state,
        newsltremail: null,
        Pagecontent: { Banner_Title: '', Title_Description: '' },
        Partners: {},
        Features: { Title: '' },
        Logos: {},
        agencyCode: {},
        policy: { header: '', date: '' },
        privacyContent: { Content: '' },
        termsConditions:{header: '', date: '', contentList:''},
        notificationEmail: ""

    },
    methods: {
        dateFormat: function (d) {
            return moment(d).format('DD-MMM-YYYY');
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },

        getPagecontent: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var contactPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Contact Us/Contact Us/Contact Us.ftl';
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Home/Home/Home.ftl';
                        var privacy = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Privacy Policy/Privacy Policy/Privacy Policy.ftl';
                        var terms = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Terms and Conditions/Terms and Conditions/Terms and Conditions.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;

                        axios.get(contactPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            var Notification = self.pluck('Notifications', response.data.area_List);
                            if (Notification.length > 0) {
                                self.notificationEmail = self.pluckcom('Notification_Email', Notification[0].component);
                            }
                        }).catch(function (error) {
                            console.log('Error');
                        });

                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var Pagecontent = self.pluck('Banner_Area', self.data.area_List);
                            if (Pagecontent.length > 0) {
                                self.Pagecontent.Banner_Title = self.pluckcom('Banner_Title', Pagecontent[0].component);
                                self.Pagecontent.Title_Description = self.pluckcom('Title_Description', Pagecontent[0].component);
                                self.Pagecontent.Banner_Image = self.pluckcom('Banner_Image', Pagecontent[0].component);
                                self.Pagecontent.Email = self.pluckcom('Email', Pagecontent[0].component);
                            }

                            var Features = self.pluck('Feature_Area', self.data.area_List);
                            if (Features.length > 0) {
                                self.Features.Title = self.pluckcom('Title', Features[0].component);
                                self.Features.Features_List = self.pluckcom('Features_List', Features[0].component);
                            }

                            var Logos = self.pluck('Partner_Section', self.data.area_List);
                            if (Logos.length > 0) {
                                self.Logos.Partners_Icons = self.pluckcom('Partners_Icons', Logos[0].component);
                            }




                        }).catch(function (error) {
                            console.log('Error');
                            self.content = [];
                        });
                        axios.get(privacy, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var policy = self.pluck('Header_Section', self.data.area_List);
                            if (policy.length > 0) {
                                self.policy.header = self.pluckcom('Title', policy[0].component);
                                self.policy.date = self.pluckcom('Date', policy[0].component);
                            }
                            var privacyContent = self.pluck('Content_Section', self.data.area_List);
                            if (privacyContent.length > 0) {
                                self.privacyContent.Content = self.pluckcom('Content', privacyContent[0].component);
                                self.privacyContent.PolicyList = self.pluckcom('Policy_Title', privacyContent[0].component);
                            }
                        }).catch(function (error) {
                        });

                        axios.get(terms, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var termsConditions = self.pluck('Header_Section', self.data.area_List);
                            if (termsConditions.length > 0) {
                                self.termsConditions.header = self.pluckcom('Title', termsConditions[0].component);
                                self.termsConditions.date = self.pluckcom('Date', termsConditions[0].component);
                                self.termsConditions.contentList = self.pluckcom('Content_Section', termsConditions[0].component);
                            }
                            
                        }).catch(function (error) {
                        });
                    }

                });

            });

        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var vm = this;
            var huburl = vm.commonStore.hubUrls.hubConnection.cmsUrl;
            var portno = vm.commonStore.hubUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        },
        sendnewsletter: async function () {

            if (!this.newsltremail) {
                alertify.alert('Alert', 'Email Id required.');
                return false;;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.newsltremail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.');
                return false;
            } else {
                var agencyCode = this.agencyCode
                var filterValue = "type='Newsletter' AND keyword1='" + this.newsltremail + "'";
                var allDBData = await this.getDbData4Table(agencyCode, filterValue, "date1");

                if (allDBData != undefined && allDBData.length > 0) {
                    alertify.alert('Alert', 'Email address already enabled.').set('closable', false);
                    return false;
                }
                else {
                    var postData = {
                        type: "UserAddedRequest",
                        fromEmail: this.notificationEmail || this.commonStore.fallBackEmail,
                        toEmails: Array.isArray(this.newsltremail) ? this.newsltremail : [this.newsltremail],
                        logo: this.Logos.Logo || "",
                        agencyName: "Dartex Travel & Tourism Agency",
                        agencyAddress: this.contact.Address || "",
                        personName: this.newsltremail.split("@")[0],
                        primaryColor: "#f3f3f3",
                        secondaryColor: "#ed1c24"
                    };
                    var agencyCode = this.agencyCode
                    let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                    let insertSubscibeData = {
                        type: "Newsletter",
                        date1: requestedDate,
                        keyword1: this.newsltremail,
                        keyword2: "Subscribe Newsletter",
                        nodeCode: agencyCode
                    };
                    let responseObject = await this.cmsRequestData("POST", "cms/data", insertSubscibeData, null);
                    try {
                        let insertID = Number(responseObject);
                        var self = this;
                        mailUrl = self.commonStore.hubUrls.emailServices.emailApi;
                        sendMailService(mailUrl, postData);
                        this.newsltremail = '';
                        alertify.alert('Newsletter', 'Thank you for subscribing !');
                    } catch (e) {

                    }
                }

            }
        },
        async getDbData4Table(agencyCode, extraFilter, sortField) {

            var allDBData = [];
            var vm = this;
            var huburl = vm.commonStore.hubUrls.hubConnection.cmsUrl;
            var portno = vm.commonStore.hubUrls.hubConnection.ipAddress;
            var cmsURL = huburl + portno + '/cms/data/search/byQuery';
            var queryStr = "select * from cms_forms_data where nodeCode = '" + agencyCode + "'";
            if (extraFilter != undefined && extraFilter != '') {
                queryStr = queryStr + " AND " + extraFilter;
            }
            var requestObject = {
                query: queryStr,
                sortField: sortField,
                from: 0,
                orderBy: "desc"
            };
            let responseObject = await this.cmsRequestData("POST", "cms/data/search/byQuery", requestObject, null);
            if (responseObject != undefined && responseObject.data != undefined) {
                allDBData = responseObject.data;
            }
            return allDBData;

        }
    },
    mounted: function () {
        this.getPagecontent();
    },
});