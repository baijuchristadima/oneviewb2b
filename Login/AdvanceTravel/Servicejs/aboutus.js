var aboutus = new Vue({
    el: '#aboutuspage',
    name: 'aboutuspage',
    data: {
        commonStore: vueCommonStore.state,
        agencyCode: '',
        aboutus: { slider: '' },
        aboutContent:{title:'',mainTitle:'',About_Us_Content:''},
        policy: { header: '', date: '' },
        privacyContent: { Content: '' },
        termsConditions:{header: '', date: '', contentList:''},
    },
    filters: {

        subStr: function (string) {
            if (string.length > 100)
                return string.substring(0, 100) + '...';

            else
                return string;
        }

    },
    methods: {
        dateFormat: function (d) {
            return moment(d).format('DD-MMM-YYYY');
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var about_Page = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/About Us/About Us/About Us.ftl';
                        var privacy = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Privacy Policy/Privacy Policy/Privacy Policy.ftl';
                        var terms = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Terms and Conditions/Terms and Conditions/Terms and Conditions.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(about_Page, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;

                            var aboutus = self.pluck('Banner_Area', self.data.area_List);
                            if (aboutus.length > 0) {
                                self.aboutus.slider = self.pluckcom('Banner_Image', aboutus[0].component);
                            }
                            var aboutContent = self.pluck('Content_Section', self.data.area_List);
                            if (aboutContent.length > 0) {
                                self.aboutContent.title = self.pluckcom('Content_Title', aboutContent[0].component);
                                self.aboutContent.mainTitle = self.pluckcom('Content_Main_Title', aboutContent[0].component);
                                self.aboutContent.About_Us_Content = self.pluckcom('About_Us_Content', aboutContent[0].component);
                            }

                        }).catch(function (error) {
                            console.log('Error');
                        });
                        axios.get(privacy, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var policy = self.pluck('Header_Section', self.data.area_List);
                            if (policy.length > 0) {
                                self.policy.header = self.pluckcom('Title', policy[0].component);
                                self.policy.date = self.pluckcom('Date', policy[0].component);
                            }
                            var privacyContent = self.pluck('Content_Section', self.data.area_List);
                            if (privacyContent.length > 0) {
                                self.privacyContent.Content = self.pluckcom('Content', privacyContent[0].component);
                                self.privacyContent.PolicyList = self.pluckcom('Policy_Title', privacyContent[0].component);
                            }
                        }).catch(function (error) {
                        });

                        axios.get(terms, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var termsConditions = self.pluck('Header_Section', self.data.area_List);
                            if (termsConditions.length > 0) {
                                self.termsConditions.header = self.pluckcom('Title', termsConditions[0].component);
                                self.termsConditions.date = self.pluckcom('Date', termsConditions[0].component);
                                self.termsConditions.contentList = self.pluckcom('Content_Section', termsConditions[0].component);
                            }
                            
                        }).catch(function (error) {
                        });
                    }

                });

            });

        }
    },
    mounted: function () {
        sessionStorage.active_er = 2;
        this.getPagecontent();
    },
});