var HeaderComponent = Vue.component('headeritem', {
    template: `     
    <header>
    <div class="mainHeader">
        <div class="container">
            <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>                        
                    </button>
                    <a class="navbar-brand" href="/Login/Elite/index.html"><img src="website-informations/logo/logo1.png" alt="logo"></a>
                  </div>
                  <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav brdr-right">
                      <li @click="activate(1)" :class="{ active : active_er == 1 }"><a href="index.html">Home</a></li>
                      <li @click="activate(2)" :class="{ active : active_er == 2 }"><a href="aboutus.html">About Us</a></li>
                      <li @click="activate(3)" :class="{ active : active_er == 3 }"><a href="contactus.html">Contact Us</a></li>
                    </ul>
                   
                  </div>
                </div>
                
              </nav>
        </div>
            </div>
    </div>
</header>`,
    data() {
        return {
            active_er: (sessionStorage.active_er) ? sessionStorage.active_er : 1,
        }
    },
    methods: {
        activate: function(el) {           
            sessionStorage.active_el = el;
            this.active_el = el;
        },
    },
    mounted: function() {
    }
})
var headerinstance = new Vue({
    el: 'header',
    name: 'headerArea',
    data() {
        return {
            key: '',
            content: null,
            getdata: true
        }

    },

});


Vue.component('footeritem', {
    props:{
        item:Number
    },
    template: `
    <div>
    <section class="main_footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="footer-address">
                        <ul>
                            <li><i class="fa fa-map-marker"></i>{{contact.Address}}</li>
                            <li><i class="fa fa-envelope"></i><a :href="'mailto:'+contact.Email">{{contact.Email}}</a></li>
                            <li><i class="fa fa-phone"></i><a :href="'tel:'+contact.PhoneNumber">{{contact.PhoneNumber}}</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 footer2">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 footeraddress">
                            <img src="website-informations/logo/footer-logo.png" alt="footer logo">
                            <p>{{Logos.Description}}</p>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 footerlinks">
                            <h2>Quick Links</h2>
                            <ul>
                                <li><a href="index.html">Home</a></li>
                                <li><a href="contactus.html">Contact Us</a></li>
                                <li><a href="termsandconditions.html">Terms & Condition</a></li>
                            </ul>
                            <ul>
                                <li><a href="aboutus.html">About Us</a></li>
                                <li><a href="privacypolicy.html">Privacy Policy</a></li>
                            </ul>
                            <div class="follow">
                                <h2>Follow Us</h2>
                                <a :href="Follow.Facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                                <a :href="Follow.Twitter" target="_blank"><i class="fa fa-twitter"></i></a>
                                <a :href="Follow.Linkedin" target="_blank"><i class="fa fa-linkedin"></i></a>
                                <a :href="Follow.Instagram" target="_blank"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 subscribe">
                                <h2>Subscribe Our Newsletter</h2>
                                <input type="text" placeholder="Enter Your Email" v-model="newsltremail">
                                <button type="submit" class="button" v-on:click="sendnewsletter">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
   
    <section class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 copyright-txt"><p>© 2020. EliteTravel - All Rights Reserved</p></div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 powered-txt"><p>Powered by:<img src="website-informations/logo/oneview-logo.png" alt="oneview"></p></div>
            </div>
        </div>
    </section>
    </div>`,
    data() {
        return {
            commonStore: vueCommonStore.state,
            agencyCode: '',
            contact: {
                Address: '',
                Email: '',
                PhoneNumber: '',
            },
            Logos: {
                Logo: '',
                Description: ''
            },
            Follow: {Facebook:'',Twitter:'',Linkedin:'',Instagram:''
            },
            newsltremail: null,
            notificationEmail: ""

        }
    },
    methods: {
        getfooter: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var contact = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Contact Us Page/Contact Us Page/Contact Us Page.ftl';
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Footer/Footer/Footer.ftl';
                        var agencyCode=agyCode;
                        self.agencyCode=agencyCode;
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var Follow = self.pluck('Social_Media_Links', self.data.area_List);
                            if (Follow.length > 0) {
                                self.Follow.Facebook = self.pluckcom('Facebook', Follow[0].component);
                                self.Follow.Twitter = self.pluckcom('Twitter', Follow[0].component);
                                self.Follow.Linkedin = self.pluckcom('LinkedIn', Follow[0].component);
                                self.Follow.Instagram = self.pluckcom('Instagram', Follow[0].component);
                            }
                            var Logos = self.pluck('Footer', self.data.area_List);
                            self.Logos.Logo = self.pluckcom('Logo', Logos[0].component);
                            self.Logos.Description = self.pluckcom('Description', Logos[0].component);

                        }).catch(function (error) {
                            console.log('Error');
                            self.content = [];
                        });
                        axios.get(contact, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;

                            var Notification = self.pluck('Notifications', response.data.area_List);
                            if (Notification.length > 0) {
                                self.notificationEmail = self.pluckcom('Notification_Email', Notification[0].component);
                            }
                            var contact = self.pluck('Contact_Info', self.data.area_List);
                            if (contact.length > 0) {
                                self.contact.PhoneNumber = self.pluckcom('Phone_Number', contact[0].component);
                                self.contact.Email = self.pluckcom('Email', contact[0].component);
                                self.contact.Address = self.pluckcom('Address', contact[0].component);
                            }
                        }).catch(function (error) {
                            console.log('Error');
                            self.content = [];
                        });
                    }

                });

            });

        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },


        // Form Post


        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        },
        sendnewsletter: async function () {

            if (!this.newsltremail) {
                alertify.alert('Alert', 'Email Id required.');
                return false;;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.newsltremail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.');
                return false;
            } else {
                var agencyCode = this.agencyCode
                var filterValue = "type='Newsletter' AND keyword1='" + this.newsltremail + "'";
                var allDBData = await this.getDbData4Table(agencyCode, filterValue, "date1");

                if (allDBData != undefined && allDBData.length > 0) {
                    alertify.alert('Alert', 'Email address already enabled.').set('closable', false);
                    return false;
                }
                else {
                    var postData = {
                        type: "UserAddedRequest",
                        fromEmail: this.notificationEmail || this.commonStore.fallBackEmail,
                        toEmails: Array.isArray(this.newsltremail) ? this.newsltremail : [this.newsltremail],
                        logo:this.Logos.Logo || "",
                        agencyName: "Elite international travel center",
                        agencyAddress: this.contact.Address || "",
                        personName: this.newsltremail.split("@")[0],
                        primaryColor: "#FFFFFF",
                        secondaryColor: "#3f4d9d"
                    };
                    var agencyCode = this.agencyCode
                    let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                    let insertSubscibeData = {
                        type: "Newsletter",
                        date1: requestedDate,
                        keyword1: this.newsltremail,
                        keyword2: "Subscribe Newsletter",
                        nodeCode: agencyCode
                    };
                    let responseObject = await this.cmsRequestData("POST", "cms/data", insertSubscibeData, null);
                    try {
                        let insertID = Number(responseObject);
                        var self = this;
                        mailUrl = self.commonStore.hubUrls.emailServices.emailApi;
                        sendMailService(mailUrl, postData);
                        this.newsltremail = '';
                        alertify.alert('Newsletter', 'Thank you for subscribing !');
                    } catch (e) {

                    }
                }

            }
        },
        async getDbData4Table(agencyCode, extraFilter, sortField) {

            var allDBData = [];
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            var cmsURL = huburl + portno + '/cms/data/search/byQuery';
            var queryStr = "select * from cms_forms_data where nodeCode = '" + agencyCode + "'";
            if (extraFilter != undefined && extraFilter != '') {
                queryStr = queryStr + " AND " + extraFilter;
            }
            var requestObject = {
                query: queryStr,
                sortField: sortField,
                from: 0,
                orderBy: "desc"
            };
            let responseObject = await this.cmsRequestData("POST", "cms/data/search/byQuery", requestObject, null);
            if (responseObject != undefined && responseObject.data != undefined) {
                allDBData = responseObject.data;
            }
            return allDBData;

        }




    },
    mounted: function() {
        this.getfooter();
    },

})

var footerinstance = new Vue({
    el: 'footer',
    name: 'footerArea',
    data() {
        return {
            key: 0,
            content: null,
            getdata: true
        }

    },
});