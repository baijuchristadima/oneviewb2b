var aboutus = new Vue({
    el: '#privacypolicy',
    name: 'privacypolicy',
    data: {
        commonStore: vueCommonStore.state,
        agencyCode: '',
        pagecontent:{Privacy_and_Policy:''}
       
    },
    filters: {

        subStr: function (string) {
            if (string.length > 100)
                return string.substring(0, 100) + '...';

            else
                return string;
        }

    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Privacy Policy/Privacy Policy/Privacy Policy.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(pageurl, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.content = response.data;
                            var pagecontent = self.pluck('Privacy_Policy', self.content.area_List);
                            if (pagecontent.length > 0) {
                                self.pagecontent.Privacy_and_Policy = self.pluckcom('Privacy_and_Policy', pagecontent[0].component);
                            }
                        })
                    }

                });

            });

        }
    },
    mounted: function () { 
        this.getPagecontent();
        sessionStorage.active_er = 1;
    },
});