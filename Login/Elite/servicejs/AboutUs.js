var aboutus = new Vue({
    el: '#aboutuspage',
    name: 'aboutuspage',
    data: {
        commonStore: vueCommonStore.state,
        agencyCode: '',
        data: null,
        Main: { Title:''},
        About:{About:''}
       
    },
    filters: {

        subStr: function (string) {
            if (string.length > 100)
                return string.substring(0, 100) + '...';

            else
                return string;
        }

    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var about_Page = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/About Us/About Us/About Us.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(about_Page, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var Main = self.pluck('Main_Area', self.data.area_List);
                            if (Main.length > 0) {
                                self.Main.Title = self.pluckcom('Title', Main[0].component);
                                self.Main.Description = self.pluckcom('Description', Main[0].component);
                            }
                            var About = self.pluck('About_Us', self.data.area_List);
                            self.About.Title = self.pluckcom('Title', About[0].component);
                            self.About.About = self.pluckcom('About', About[0].component);

                        }).catch(function (error) {
                            console.log('Error');
                        });
                    }

                });

            });

        }
    },
    mounted: function () { 
        this.getPagecontent();
        sessionStorage.active_er = 2;
    },
});