var contact = new Vue({
    el: '#contactuspage',
    name: 'contactuspage',
    data: {
        commonStore: vueCommonStore.state,
        contact: {Address:''},
        cntusername: '',
        cntemail: '',
        cntcontact: '',
        cntwebsite:'',
        cntmessage: '',
        agencyCode: '',
        Logos:{Logo:''},
        Follow:{Facebook:''},
        notificationEmail: ""
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Contact Us Page/Contact Us Page/Contact Us Page.ftl';
                        var Page = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Footer/Footer/Footer.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;

                            var Notification = self.pluck('Notifications', response.data.area_List);
                            if (Notification.length > 0) {
                                self.notificationEmail = self.pluckcom('Notification_Email', Notification[0].component);
                            }
                            var contact = self.pluck('Contact_Info', self.data.area_List);
                            if (contact.length > 0) {
                                self.contact.Address = self.pluckcom('Address', contact[0].component);
                                self.contact.Phone_Number = self.pluckcom('Phone_Number', contact[0].component);
                                self.contact.Location_Link = self.pluckcom('Location_Link', contact[0].component);
                                self.contact.Email = self.pluckcom('Email', contact[0].component);
                            }
                        }).catch(function (error) {
                            self.content = [];
                        });
                        axios.get(Page, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var Follow = self.pluck('Social_Media_Links', self.data.area_List);
                            if (Follow.length > 0) {
                                self.Follow.Facebook = self.pluckcom('Facebook', Follow[0].component);
                                self.Follow.Twitter = self.pluckcom('Twitter', Follow[0].component);
                                self.Follow.Linkedin = self.pluckcom('LinkedIn', Follow[0].component);
                                self.Follow.Instagram = self.pluckcom('Instagram', Follow[0].component);
                            }
                            var Logos = self.pluck('Footer', self.data.area_List);
                            self.Logos.Logo = self.pluckcom('Logo', Logos[0].component);

                        }).catch(function (error) {
                            console.log('Error');
                            self.content = [];
                        });
                    }

                });

            });

        },
        sendcontactus: async function () {
            if (!this.cntusername) {
                alertify.alert('Alert', 'Name required.').set('closable', false);
                return false;
            }
            if (!this.cntemail) {
                alertify.alert('Alert', 'Email Id required.').set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.cntemail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                return false;
            }
            if (!this.cntcontact) {
                alertify.alert('Alert', 'Mobile number required.').set('closable', false);
                return false;
            }
            if (this.cntcontact.length < 8) {
                alertify.alert('Alert', 'Enter valid mobile number.').set('closable', false);
                return false;
            }
            if (!this.cntmessage) {
                alertify.alert('Alert', 'Message required.').set('closable', false);
                return false;
            } else {
                var self = this;
                var frommail = this.notificationEmail || this.commonStore.fallBackEmail;
                var postData = {
                    type: "UserAddedRequest",
                    fromEmail: frommail,
                    toEmails: Array.isArray(this.cntemail) ? this.cntemail : [this.cntemail],
                    logo: this.Logos.Logo || "",
                    agencyName: "Elite international travel center",
                    agencyAddress: this.contact.Address || "",
                    personName: this.cntusername || "",
                    primaryColor: "#FFFFFF",
                    secondaryColor: "#3f4d9d"
                };
                var agencyCode = this.agencyCode
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                let insertContactData = {
                    type: "Contact Us",
                    keyword1: this.cntname,
                    keyword2: this.cntemail,
                    keyword3:this.cntwebsite,
                    number1: this.cntcontact,
                    text1: this.cntmessage,
                    date1: requestedDate,
                    nodeCode: agencyCode
                };
                let responseObject = await this.cmsRequestData("POST", "cms/data", insertContactData, null);
                try {
                    let insertID = Number(responseObject);
                    var self = this;
                    mailUrl = self.commonStore.hubUrls.emailServices.emailApi;
                    sendMailService(mailUrl, postData);
                    this.cntemail = '';
                    this.cntfirstname = '';
                    this.cntlastname = '';
                    this.cntcontact = '';
                    this.cntmessage = '';
                    alertify.alert('Contact us', 'Thank you for contacting us.We shall get back to you.');
                } catch (e) {

                }



            }
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var vm = this;
            var huburl = vm.commonStore.hubUrls.hubConnection.cmsUrl;
            var portno = vm.commonStore.hubUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        }
    },
    mounted: function () {
        this.getPagecontent();
        sessionStorage.active_er = 3;
    },
});