var maininstance = new Vue({
  el: '#homepage',
  name: 'homepage',
  data: {
    commonStore: vueCommonStore.state,
    agencyCode: '',
    flagForgot: false,
    mainContent:{},
    bannerSection:{},
    isLoading: false,
    fullPage: true,
  },
  methods: {
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getAllMapData: function (contentArry) {
      var tempDataObject = {};
      if (contentArry != undefined) {
          contentArry.map(function (item) {
              let allKeys = Object.keys(item)
              for (let j = 0; j < allKeys.length; j++) {
                  let key = allKeys[j];
                  let value = item[key];
                  if (key != 'name' && key != 'type') {
                      if (value == undefined || value == null) {
                          value = "";
                      }
                      tempDataObject[key] = value;
                  }
              }
          });
      }
      return tempDataObject;
  },
    getPagecontent: function () {
      var self = this;
      // self.isLoading = true;
      axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
        var agencyFolderName = '';
        var agy;
        try {
          response.data.forEach(function (agent, agentIndex) {
            agent.domain.forEach(function (dom, domIndex) {
              if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                agy = agent;
                agencyFolderName = agy.agencyFolderName;
              }
            });
          });
        } catch (err) { }
        agy.registeredUsers.forEach(function (agyCode, domIndex) {
          if (domIndex == 0) {
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            var homePageUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/02 Index/02 Index/02 Index.ftl';
            var agencyCode = agyCode;
            self.agencyCode = agencyCode;
            axios.get(homePageUrl, {
              headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
            }).then(function (response) {
              console.log('responsee', response);
              self.content = response.data;
              if (self.content != undefined && self.content != null) {
                var bannerData = self.pluck('Banner_Section',self.content.area_List);
                self.bannerSection = self.getAllMapData(bannerData[0].component);
                var mainData = self.pluck('Main_Content', self.content.area_List);
                self.mainContent = self.getAllMapData(mainData[0].component);
                
              }
              self.stopLoader();
              // self.isLoading = false;
            }).catch(function (error) {
              console.log('Error');
              // self.isLoading = false;
              self.stopLoader();
            });
          }

        });

      });

    },
    stopLoader: function () {
      var div = document.getElementById("preloader");
      // if (div.style.display !== "none") {  
      div.style.display = "none";
      // }  else {  
      //     div.style.display = "block";  
      // }
  }
  },
  mounted: function () {
    // sessionStorage.active_er = 1;
    this.getPagecontent();
  },
});
