var HeaderComponent = Vue.component('headeritem', {
    template: `
    <div id="header">
    <div class="top">
      <div class="container">
        <ul class="h-detail">
              <li>{{TopSection.PhoneLabel}} :<a :href="'tel:'+TopSection.Phone"> {{TopSection.Phone}}</a></li>
              <li>{{TopSection.EmailLabel}} : <a :href="'mailto:'+TopSection.Email"> {{TopSection.Email}}</a></li>
            </ul>
      </div>
    </div>
    <nav id="navbar-main" class="navbar navbar-default">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              <a class="navbar-brand" href="index.html"><img :src="TopSection.Logo" alt=""></a> </div>
              <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li :class="{ 'active': activeClass === '/index.html'}"><a href="index.html">{{TopSection.Home}}</a></li>
                    <li :class="{ 'active': activeClass === '/about.html'}"><a href="about.html">{{TopSection.AboutUs}}</a></li>
                    <li :class="{ 'active': activeClass === '/contact.html'}"><a href="contact.html">{{TopSection.Contact}}</a></li>
                    <li :class="{ 'active': activeClass === '/terms-and-conditions.html'}" v-if="checkMobileOrNot"><a href="terms-and-conditions.html">Terms and Condition</a></li>
                    <li :class="{ 'active': activeClass === '/privacy-policy.html'}" v-if="checkMobileOrNot"><a href="privacy-policy.html">Privacy Policy</a></li>
                    <li class="d-none"> <a :href="'mailto:'+TopSection.Email"><span>{{TopSection.EmailLabel}}</span><br>{{TopSection.Email}}</a></li>
                    <li class="d-none"> <a :href="'tel:'+TopSection.Phone"><span>{{TopSection.PhoneLabel}}</span><br> {{TopSection.Phone}}</a></li>
                </ul>
              </div>
          </div>
        </div>
      </div>
    </nav>
    </div>`,
    data() {
        return {
            agencyCode: '',
            TopSection:{
                Home:''
            },
            activeClass: '',

            xyz:'text'
        }
    },
    created() {
        var self=this;
        var pathArray = window.location.pathname.split('/');
        self.activeClass ='/'+ pathArray[pathArray.length-1];
       
      },
    computed:{
        checkMobileOrNot: function () {
          if (navigator.userAgent.match(/Android/i) ||
              navigator.userAgent.match(/webOS/i) ||
              navigator.userAgent.match(/iPhone/i) ||
              navigator.userAgent.match(/iPad/i) ||
              navigator.userAgent.match(/iPod/i) ||
              navigator.userAgent.match(/BlackBerry/i) ||
              navigator.userAgent.match(/Windows Phone/i)) {
              return true;
          }
          else { return false; }
        }
      },
    methods: {
        getPageheader: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {

                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }
                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/01 Master/01 Master/01 Master.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var menuData = self.pluck('Header_Section', self.data.area_List);
                            if (menuData.length > 0) {
                            self.TopSection.Home = self.pluckcom('Home_Label', menuData[0].component);
                            self.TopSection.AboutUs = self.pluckcom('About_Us_Label', menuData[0].component);
                            self.TopSection.Contact = self.pluckcom('Contact__Us_Label', menuData[0].component);
                            self.TopSection.Logo = self.pluckcom('Logo_260x70px', menuData[0].component);
                            self.TopSection.EmailLabel = self.pluckcom('Email_Label', menuData[0].component);
                            self.TopSection.PhoneLabel = self.pluckcom('Call_Us_Label', menuData[0].component);
                            self.TopSection.Email = self.pluckcom('Email_Description', menuData[0].component);
                            self.TopSection.Phone = self.pluckcom('Call_Description', menuData[0].component);
                            console.log(self.TopSection);
                            }
                        }).catch(function (error) {
                            console.log('Error');
                        });
                    }

                });

            });

        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
    },
    mounted: function () {

        this.getPageheader();
    },
})
var headerinstance = new Vue({
    el: 'header',
    name: 'headerArea',
    data() {
        return {
            key: '',
            content: null,
            getdata: true
        }

    },

});
Vue.component('footeritem', {
    template: `
    <div id="footer">
    <div class="container" v-if="!checkMobileOrNot">
    <div class="row">
      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <div class="ftr-cntc">
        	<h3>{{FooterLabels.Get_In_Touch_Label}}</h3>
          <p v-html="FooterLabels.Description"></p>
			<ul>
                <li>
                  <i :class="'fa '+FooterLabels.Address_Icon" aria-hidden="true"></i>
                 {{FooterLabels.Address_Description}}</li>
                <li>
                  <i class="fa fa-envelope" aria-hidden="true"></i>
                  <a href="'mailto:'+menuData.Email_Description">{{menuData.Email_Description}}</a></li>
                <li>
                  <i :class="'fa '+FooterLabels.Phone_Icon" aria-hidden="true"></i>
                  <a href="tel:+971026444494">{{FooterLabels.Phone_Number}}</a></li>
            </ul>
        </div>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <div class="quick-links">
          <h3>{{FooterLabels.Quick_List_Label}}</h3>
          <ul>
            <li><i class="fa  fa-angle-double-right"></i><a href="index.html">{{menuData.Home_Label}}</a></li>
            <li><i class="fa  fa-angle-double-right"></i><a href="about.html">{{menuData.About_Us_Label}}</a></li>
            <li><i class="fa  fa-angle-double-right"></i><a href="contact.html">{{menuData.Contact__Us_Label}}</a></li>
            <li><i class="fa  fa-angle-double-right"></i><a href="terms-and-conditions.html">{{FooterLabels.Terms_And_Condition_Label}}</a></li>
            <li><i class="fa  fa-angle-double-right"></i><a href="privacy-policy.html">{{FooterLabels.Privacy_Policy_Label}}</a></li>
          </ul>
        </div>
      </div>
      <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
        <div class="newsletter">
          <h3>{{FooterLabels.Subscribe_Newsletter_Label}}</h3>
          <p v-html="FooterLabels.Newsletter_Description"></p>
          <div class="news-sub">
            <input type="email" id="email" name="email" :placeholder="FooterLabels.Email_Placeholder" v-model="newsltremail">
            <button type="submit" v-on:click="sendnewsletter"><i class="fa  fa-paper-plane"></i></button>
          </div>
          <div class="foot-social">
          <ul>
            <li v-for="item in FooterLabels.Social_Media_List"><a :href="item.Url" target="_blank"><i class="fa" v-bind:class="item.Name"></i></a></li>
          </ul>
        </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="all-reserved">
      <div class="reserved">
        <p v-html="FooterLabels.Copy_Right_Description"></p>
      </div>

<!-- 

      <div class="Powered">
        <p v-html="FooterLabels.Powered_By_Label">:</p>
        <a href="http://www.oneviewit.com/" target="_blank"><img :src="FooterLabels.Powered_By_Logo_92x33px" alt="logo"></a>
        </div>
-->

</div>
  </div>
  </div>`,
    data() {
        return {
            commonStore: vueCommonStore.state,
            agencyCode: '',
            FooterLabels:{},
            menuData:{},
            newsltremail:'',
            isLoading: false

        }
    },
    computed:{
        checkMobileOrNot: function () {
          if (navigator.userAgent.match(/Android/i) ||
              navigator.userAgent.match(/webOS/i) ||
              navigator.userAgent.match(/iPhone/i) ||
              navigator.userAgent.match(/iPad/i) ||
              navigator.userAgent.match(/iPod/i) ||
              navigator.userAgent.match(/BlackBerry/i) ||
              navigator.userAgent.match(/Windows Phone/i)) {
              return true;
          }
          else { return false; }
      }
      },
    methods: {
        getPagefooter: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var Footer = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/01 Master/01 Master/01 Master.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(Footer, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.content = response.data;
                            if (self.content != undefined && self.content != null) {
                                var mainData = self.pluck('Footer_Section',self.content.area_List);
                                self.FooterLabels = self.getAllMapData(mainData[0].component);
                                var meData = self.pluck('Header_Section', self.content.area_List);
                                self.menuData = self.getAllMapData(meData[0].component);
                                
                              }
                            
                        }).catch(function (error) {
                            console.log('Error');
                            self.content = [];
                        });
                    }

                });

            });

        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        sendnewsletter: async function () {

            if (!this.newsltremail) {
                alertify.alert('Alert', 'Email Id required.');
                return false;;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.newsltremail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.');
                return false;
            } else {
                self.isLoading = true;
                var agencyCode = this.agencyCode
                var filterValue = "type='Newsletter' AND keyword1='" + this.newsltremail + "'";
                var allDBData = await this.getDbData4Table(agencyCode, filterValue, "date1");

                if (allDBData != undefined && allDBData.length > 0) {
                    alertify.alert('Alert', 'Email address already enabled.').set('closable', false);
                    self.isLoading = false;
                    return false;
                }
                else {
                
                    var agencyCode = this.agencyCode
                    let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                    let insertSubscibeData = {
                        type: "Newsletter",
                        date1: requestedDate,
                        keyword1: this.newsltremail,
                        nodeCode: agencyCode
                    };
                    let responseObject = await this.cmsRequestData("POST", "cms/data", insertSubscibeData, null);
                    try {
                        let insertID = Number(responseObject);
                       
                        this.newsltremail = '';
                        alertify.alert('Newsletter', 'Thank you for subscribing !');
                        self.isLoading = false;
                    } catch (e) {
                        self.isLoading = false;
                    }
                }

            }
        },
        async getDbData4Table(agencyCode, extraFilter, sortField) {
            try {
                var allDBData = [];
                var huburl = HubServiceUrls.hubConnection.cmsUrl;
                var portno = HubServiceUrls.hubConnection.ipAddress;
                var cmsURL = huburl + portno + '/cms/data/search/byQuery';
                var queryStr = "select * from cms_forms_data where nodeCode = '" + agencyCode + "'";
                if (extraFilter != undefined && extraFilter != '') {
                    queryStr = queryStr + " AND " + extraFilter;
                }
                var requestObject = {
                    query: queryStr,
                    sortField: sortField,
                    from: 0,
                    orderBy: "desc"
                };
                let responseObject = await this.cmsRequestData("POST", "cms/data/search/byQuery", requestObject, null);
                if (responseObject != undefined && responseObject.data != undefined) {
                    allDBData = responseObject.data;
                }
                return allDBData;   
            } catch (error) {
                self.isLoading = false;
            }
           

        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            try {
                var huburl = HubServiceUrls.hubConnection.cmsUrl;
                var portno = HubServiceUrls.hubConnection.ipAddress;
                const url = huburl + portno + "/" + urlParam;
                if (data != null) {
                    data = JSON.stringify(data);
                }
                const response = await fetch(url, {
                    method: callMethod, // *GET, POST, PUT, DELETE, etc.
                    credentials: "same-origin", // include, *same-origin, omit
                    headers: { 'Content-Type': 'application/json' },
                    body: data, // body data type must match "Content-Type" header
                });
                try {
                    const myJson = await response.json();
                    return myJson;
                } catch (error) {
                    return object;
                }
            } catch (error) {
               self.isLoading = false; 
            }
           
        }
        
        

    },
    mounted: function () {
        this.getPagefooter();
    },

})
var footerinstance = new Vue({
    el: 'footer',
    name: 'footerArea',
    data() {
        return {
            key: 0,
            content: null,
            getdata: true
        }

    },
});
