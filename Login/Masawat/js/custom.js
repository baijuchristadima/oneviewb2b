(function($) { 

/***************************************************************
		BEGIN: VARIOUS DATEPICKER & SPINNER INITIALIZATION
***************************************************************/
$(document).ready(function () {
	$( function() {
    var dateFormat = "mm/dd/yy",
      from = $( "#from, #from1, #from2" )
        .datepicker({
          minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
          buttonText: "<i class='fa fa-calendar'></i>",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "down"} 
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#to, #to1, #to2" ).datepicker({
        minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
          buttonText: "<i class='fa fa-calendar'></i>",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "down"} 
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  } );
});
    
    
    
       
/*--date-picker--*/
     $( function() {
    var dateFormat = "mm/dd/yy",
      from = $( "#from-1,#from-2,#from-3,#from-4,#from-5,#from-6,#from-7,#from-8,#from-9,#from-10" )
        .datepicker({
          minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
          buttonText: " ",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "up"} 
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#to-1,#to-2,#to-3,#to-4,#to-5,#to-6,#to-7,#to-8,#to-9,#to-10" ).datepicker({
        minDate: "dateToday",
          numberOfMonths: 1,
          showOn: "both",
          buttonText: " ",
          duration: "fast",
          showAnim: "slide", 
          showOptions: {direction: "up"} 
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  } );
    
    

/*----------------------------------------------------*/
// Select2
/*----------------------------------------------------*/

jQuery(document).ready(function ($) {
	$('.myselect').select2({
		minimumResultsForSearch: Infinity,
		'width': '100%'
	});
	$('b[role="presentation"]').hide();
	$('.select2-selection__arrow').append('<i class="fa fa-angle-down"></i>');
});


})(jQuery);

/* Destinations Slider JS
============================================================== */
    $(document).ready(function() {

      $("#owl-demo-1").owlCarousel({
        items: 4,
        lazyLoad: true,
        loop: true,
        margin: 30,
        navigation : true,
         itemsDesktop : [1024, 3],
        itemsDesktopSmall : [979, 3],
        itemsTablet : [768, 2],
		itemsMobile : [767, 1],  
      });
          $( ".owl-prev").html('<i class="fa  fa-chevron-left"></i>');
         $( ".owl-next").html('<i class="fa  fa-chevron-right"></i>');

    });

/* Tours Slider JS
============================================================== */
    $(document).ready(function() {

      $("#owl-demo-2").owlCarousel({
        items: 4,
        lazyLoad: true,
        loop: true,
        margin: 30,
        navigation : true,
         itemsDesktop : [1024, 3],
        itemsDesktopSmall : [979, 3],
        itemsTablet : [768, 2],
		itemsMobile : [767, 1],  
      });
          $( ".owl-prev").html('<i class="fa  fa-chevron-left"></i>');
         $( ".owl-next").html('<i class="fa  fa-chevron-right"></i>');

    });

/* Testimonials Slider JS
============================================================== */
$(document).ready(function () {
            $("#owl").owlCarousel({
                navigation: false,
                slideSpeed: 100,
                paginationSpeed: 800,
                singleItem: true,
                autoPlay: true
            });
});


/*--Back-to-top--*/ 
    $(document).ready(function(){
	
	//Check to see if the window is top if not then display button
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.scrollToTop').fadeIn();
		} else {
			$('.scrollToTop').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
	
});
