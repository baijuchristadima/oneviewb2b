var HeaderComponent = Vue.component('headeritem', {
    template: ` 
    <div>    
    <div class="container">
            <div class="row">
               <div class="col-md-4 col-sm-6 col-xs-8">
                     <div class="logo"> <a href="index.html"><img src="website-informations/logo/logo.png" alt="logo"></a> </div>
                  </div>
                
                
                <div class="col-md-5">
                    <div class="menu-nav">
                  <nav class="navbar navbar-default" id="nav_bar" >
                     <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                     </div>
                     <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-center">
                           <li @click="activate(1)" :class="{ active : active_er == 1 }"><a  href="index.html">Home</a></li>
                           <li @click="activate(2)" :class="{ active : active_er == 2 }"><a href="about.html">About Us</a></li>
                              <!--<ul class="dropdown-menu" role="menu">
                                 <li><a href="flight-deals.html">Airlines</a></li>
                                 <li><a href="package-deals.html">Packages</a></li>
                              </ul>-->
                           
                           <li @click="activate(3)" :class="{ active : active_er == 3 }"><a href="contact.html">Contact Us</a></li>
                           <li @click="activate(4)" :class="{ active : active_er == 4 }"><a href="register.html"> Register</a></li>
                          
                        </ul>
                     </div>
                    
                  </nav>
               
       </div>
                </div>
                
                
                <div class="col-md-3 col-sm-6">
                     <div class="top-email">
               <ul>
                    <li>
                        <i class="fa fa-phone"></i> 
                        <p>Contact us <br> <a :href="'tel:'+contact.Phone_Number">{{contact.Phone_Number}}</a></p> 
                   </li>
                 
               </ul>
                    </div>
                </div>
            </div>
         </div>
         </div>`,
    data() {
        return {

            agencyCode: '',
            contact: { Phone_Number: '' },
            active_er: (sessionStorage.active_er) ? sessionStorage.active_er : 1,
            Logos:'',
            data:{}
        }
    },
    methods: {
        activate: function (el) {
            sessionStorage.active_er = el;
            this.active_er = el;
            try {
              if (el == 1 || el == 2) {
                maininstance.actvetab = el;
              } else {
                maininstance.actvetab = 0;
              }
            } catch (error) {}
        },
        getPageheader: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {

                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Contact Us/Contact Us/Contact Us.ftl';
                        var Footer = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Footer/Footer/Footer.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var contact = self.pluck('Contact_Details', self.data.area_List);
                            if (contact.length > 0) {
                                self.contact.Phone_Number = self.pluckcom('Phone_Number', contact[0].component);
                                //self.data = response.data;
                               
                           
                           
                            }

                        }).catch(function (error) {
                            console.log('Error');
                            self.content = [];
                        });
                        axios.get(Footer, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            var datas = response.data;
                           
                            var Logos = self.pluck('Footer', datas.area_List);
                            if (Logos.length > 0) {
                                console.log("ee",Logos);
                                self.Logos = self.pluckcom('Logo', Logos[0].component);
                              
                            }
                        }).catch(function (error) {
                            console.log('Error');
                            self.content = [];
                        });
                    }

                });

            });

        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
    },
    mounted: function () {

        this.getPageheader();
    },
})
var headerinstance = new Vue({
    el: 'header',
    name: 'headerArea',
    data() {
        return {
            key: '',
            content: null,
            getdata: true
        }

    },

});
Vue.component('partner', {
    template: `
    <div class="partner-sec">
    <div id="owl-demo-5" class="owl-carousel owl-theme">
        <div class="item" v-for="item in Partner.Logo">
            <div class="partner">
                <img :src="item.Logo">
            </div>
        </div>
    </div>
</div>
    `,
    data() {
        return {
            commonStore: vueCommonStore.state,
            agencyCode: '',
            Partner:{Logo:''}

        }
    },
    methods: {
        getPartner: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {

                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var Page = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Partners/Partners/Partners.ftl';
                                          var Footer = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Footer/Footer/Footer.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(Page, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var Partner = self.pluck('Partners', self.data.area_List);
                            if (Partner.length > 0) {
                                self.Partner.Logo = self.pluckcom('Logo', Partner[0].component);
                                setTimeout(function () { partners() }, 1000);
                            }
                        }).catch(function (error) {
                        }); 
                    }

                });

            });

        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },

    },
    mounted: function() {
        this.getPartner();

    }

});
var partnerinstance = new Vue({
    el: 'partner',
    name: 'partner',
    data() {
        return {
            key: '',
            content: null,
            getdata: true
        }

    },

});
Vue.component('footeritem', {
    props: {
        item: Number
    },
    template: `
    <div>
  <div class="container">
    <div class="inner">
    	<div class="row">
        	<div class="newsletter">
            <div class="col-lg-6 col-md-6 col-sm-12 ">
              <i class="fa fa-envelope-o" aria-hidden="true"></i>
              <h2>Sign Up For Newsletter</h2>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 ">
                <div class="w3-container">
                  <input class="input" type="text" placeholder="Enter Your Email Address" v-model="newsltremail">
                  <button type="submit" class="button" v-on:click="sendnewsletter">Submit</button>
                </div>
            </div>
          </div>
          <div class="col-lg-12 text-center">
            <div class="link">
              <ul>
                <li><a href="index.html">Home</a></li>
                <li><a href="about.html">About Us</a></li>
                <li><a href="contact.html">Contact Us</a></li>
              </ul>
            </div>
          </div>
          <div class="col-lg-12 text-center">
            <div class="get-in-touch">
            	<div class="address bdr-rght"> <i class="fa fa-map-marker"></i> {{contact.Address}}</div>
              <div class="phone-number bdr-rght"> <i class="fa fa-phone"></i> <a :href="'tel:'+contact.Phone_Number">{{contact.Phone_Number}}</a> </div>
              <div class="email"> <i class="fa fa-envelope"></i> <a :href="'mailto:'+contact.Email"> {{contact.Email}}</a> </div>
            </div>
          </div>
          <div class="col-lg-12 text-center">
            <ul class="social">
            <li><a :href="Follow.Facebook_Link" target="_blank"><i aria-hidden="true" class="fa fa-facebook"></i></a></li>
            <li><a :href="Follow.Twitter_Link" target="_blank"><i aria-hidden="true" class="fa fa-twitter"></i></a></li>
            <li><a :href="Follow.Linkedin_Link" target="_blank"><i aria-hidden="true" class="fa fa-linkedin"></i></a></li>
            <li><a :href="Follow.Instagram_Link" target="_blank"><i aria-hidden="true" class="fa fa-instagram"></i></a></li>
              </ul>
          </div>
        </div>
    </div>
  </div>
  <div class="ftr-btm">
    <div class="container">
		<div class="inner">
        	<div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="copyright">©2021 Sky Pass - All Rights Reserved.
</div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="powered-by">Powered by: &nbsp; <a href="http://www.oneviewit.com/" target="_blank"><img alt="" src="website-informations/logo/oneview-logo.png"></a></div>
              </div>
            </div>
            </div>
    </div>
  </div>
  </div>`,
    data() {
        return {
            commonStore: vueCommonStore.state,
            agencyCode: '',
            contact: { Address: '',Phone_Number:'' },
            newsltremail: null,
            //Logos:{Logo:''},
            Logos:'',
            Follow:{},
            notificationEmail: ""

        }
    },
    methods: {
        getPagefooter: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {

                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var Footer = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Footer/Footer/Footer.ftl';
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Contact Us/Contact Us/Contact Us.ftl';
                        var Page = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Home/Home/Home.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(Footer, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var Follow = self.pluck('Social_Media_Links', self.data.area_List);
                            if (Follow.length > 0) {
                                self.Follow.Facebook_Link = self.pluckcom('Facebook', Follow[0].component);
                                self.Follow.Twitter_Link = self.pluckcom('Twitter', Follow[0].component);
                                self.Follow.Linkedin_Link = self.pluckcom('LinkedIn', Follow[0].component);
                                self.Follow.Instagram_Link = self.pluckcom('Instagram', Follow[0].component);
                            }
                        }).catch(function (error) {
                            console.log('Error');
                            self.content = [];
                        });
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var Notification = self.pluck('Notifications', response.data.area_List);
                            if (Notification.length > 0) {
                                self.notificationEmail = self.pluckcom('Notification_Email', Notification[0].component);
                            }
                            var contact = self.pluck('Contact_Details', self.data.area_List);
                            if (contact.length > 0) {
                                self.contact.Address = self.pluckcom('Address', contact[0].component);
                                self.contact.Phone_Number = self.pluckcom('Phone_Number', contact[0].component);
                                self.contact.Email = self.pluckcom('Email', contact[0].component);
                            }
                        }).catch(function (error) {
                            self.content = [];
                        });
                        
                        axios.get(Page, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var Logos = self.pluck('Common_Area', self.data.area_List);
                            self.Logos = self.pluckcom('Logo', Logos[0].component);
                        }).catch(function (error) {
                        });
                    }

                });

            });

        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        },
        sendnewsletter: async function () {

            if (!this.newsltremail) {
                alertify.alert('Alert', 'Email Id required.');
                return false;;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.newsltremail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.');
                return false;
            } else {
                var agencyCode = this.agencyCode
                var filterValue = "type='Newsletter' AND keyword1='" + this.newsltremail + "'";
                var allDBData = await this.getDbData4Table(agencyCode, filterValue, "date1");

                 if (allDBData != undefined && allDBData.length > 0) {
                  alertify.alert('Alert', 'Email address already enabled.').set('closable', false);
                    return false;
                 }
                 else {
                    var postData = {
                        type: "UserAddedRequest",
                        fromEmail: this.notificationEmail || this.commonStore.fallBackEmail,
                        toEmails: Array.isArray(this.newsltremail) ? this.newsltremail : [this.newsltremail],
                        logo:this.Logos|| "",
                        agencyName: "Sky Pass Travel",
                        agencyAddress: this.contact.Address || "",
                        personName: this.newsltremail.split("@")[0],
                        primaryColor: "#02a0fb",
                        secondaryColor: "#062f89"
                    };
                    var agencyCode = this.agencyCode
                    let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                    let insertSubscibeData = {
                        type: "Newsletter",
                        date1: requestedDate,
                        keyword1: this.newsltremail,
                        keyword2: "Subscribe Newsletter",
                        nodeCode: agencyCode
                    };
                    let responseObject = await this.cmsRequestData("POST", "cms/data", insertSubscibeData, null);
                    try {
                        let insertID = Number(responseObject);
                        var self = this;
                        mailUrl = self.commonStore.hubUrls.emailServices.emailApi;
                        sendMailService(mailUrl, postData);
                        this.newsltremail = '';
                        alertify.alert('Newsletter', 'Thank you for subscribing !');
                    } catch (e) {

                    }
                 }

            }
        },
        async getDbData4Table(agencyCode, extraFilter, sortField) {

            var allDBData = [];
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            var cmsURL = huburl + portno + '/cms/data/search/byQuery';
            var queryStr = "select * from cms_forms_data where nodeCode = '" + agencyCode + "'";
            if (extraFilter != undefined && extraFilter != '') {
                queryStr = queryStr + " AND " + extraFilter;
            }
            var requestObject = {
                query: queryStr,
                sortField: sortField,
                from: 0,
                orderBy: "desc"
            };
            let responseObject = await this.cmsRequestData("POST", "cms/data/search/byQuery", requestObject, null);
            if (responseObject != undefined && responseObject.data != undefined) {
                allDBData = responseObject.data;
            }
            return allDBData;

        }

    },
    mounted: function () {
        this.getPagefooter();
    },

})
var footerinstance = new Vue({
    el: 'footer',
    name: 'footerArea',
    data() {
        return {
            key: 0,
            content: null,
            getdata: true
        }

    },
});
function partners() {
    $("#owl-demo-5").owlCarousel({
        autoplay:true,
        autoPlay : 8000,
        autoplayHoverPause:true, 
        stopOnHover : false,  
        items : 6,
        margin:10,  
        lazyLoad : true,
        navigation : true,
        itemsDesktop : [1199, 5],
        itemsDesktopSmall : [991,3],
          itemsTablet : [600, 2],
          itemsMobile : [400, 2]
         
      });

         $( ".owl-prev").html('<i class="fa  fa-angle-left"></i>');
         $( ".owl-next").html('<i class="fa  fa-angle-right"></i>');
}
