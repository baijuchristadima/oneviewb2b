var home = new Vue({
    el: '#homepage',
    name: 'homepage',
    data: {
        commonStore: vueCommonStore.state,
        Pagecontent: {
            Title_1: '',Title_2:'',Title_Description:'',About_Us_Content:''
        },
        feature: {
            Features_List: null
        },
        Logos:{},
        agencyCode: '',

    },
    filters: {

        subStr: function (string) {
            if (string.length > 150)
                return string.substring(0, 300) + '...';

            else
                return string;
        }

    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            //var agencyCode = document.getElementById("txtAgencyCode").value;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {

                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var Page = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Home/Home/Home.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(Page, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var Pagecontent = self.pluck('Page_Content', self.data.area_List);
                            if (Pagecontent.length > 0) {
                                self.Pagecontent.Title_1 = self.pluckcom('Title_1', Pagecontent[0].component);
                                self.Pagecontent.Title_2 = self.pluckcom('Title_2', Pagecontent[0].component);
                                self.Pagecontent.Title_Description = self.pluckcom('Title_Description', Pagecontent[0].component);
                                self.Pagecontent.About_Us_Content = self.pluckcom('About_Us_Content', Pagecontent[0].component);
                            }
                            var feature = self.pluck('Features', self.data.area_List);
                            self.feature.Features_List = self.pluckcom('Features_List', feature[0].component);
                        }).catch(function (error) {
                        }); 

                    }

                });

            });

        }
    },
    mounted: function () {
        this.getPagecontent();
        sessionStorage.active_er=1;
    },


});