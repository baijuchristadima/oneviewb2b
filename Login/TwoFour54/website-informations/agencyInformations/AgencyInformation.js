﻿var AgencyInformation = {
    AirInformation: {
        homeUrl: "Flights/index.html",
        totalBookingingsHome: 5,
        AirFolderName: "Flights",

    },
    hotelService: {
        homeUrl: "Hotels/hotelsearch.html",
        totalBookingingsHome: 5,
    },
    sightseeingService: {
        homeUrl: "sightseeing/index.html",
        totalBookingingsHome: 5,
    },
    insuranceService: {
        homeUrl: "Insurance/index.html",
        totalBookingingsHome: 5,
    },
    transferService: {
        homeUrl: "Insurance/index.html",
        totalBookingingsHome: 5,
    },
    DomainUrl: "http://b2b.oneviewitsolutions.com",
    systemSettings: {
        systemDateFormat: 'dd M y,D',
        calendarDisplay: 2,
        calendarDisplayInMobile: 1
    },


    AgencyAddress: {
        AgencyName: "Twofour54",
        AgencyTitle: "Twofour54 | Online Booking for Cheap Flights &amp; Airline Tickets",
        AgencyShortName: "Twofour54",
        AgencyAddress: "Sheikh Zayed Street, Opposite Khalifa Park Abu Dhabi, United Arab Emirates",
        AboutAgency: "twofour54 Abu Dhabi is a media zone located at the heart of one of the fastest growing media markets in the world with attractive economic benefits for companies,..",
        AgencyWebsite: "http://www.oneviewit.com",
        Helpline: "+971 2 401 2454",
        Email: "travel@twofour54.com",
        CopyRight: "© 2018. Twofour54 media & entertainment hub - All Rights Reserved",
        Facebook: "http://www.fb.com/oneviewitsolutions",
        Twitter: "http://www.twitter.com/oneviewitsolutions",
        GooglePlus: "http://plus.google.com/oneviewitsolutions",
        Linkedln: "http://www.linkedin.com/oneviewitsolutions",
        GooglePlayStore: "",
        AppStore: "",
        YouTube: ""
    }
}