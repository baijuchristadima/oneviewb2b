var HeaderComponent = Vue.component('headeritem', {
    template: ` 
    <div id="header">
    <div class="top">
      <div class="container">
        <div class="row">
          <div class="col-lg-offset-4 col-sm-offset-2 col-lg-8 col-sm-10  col-xs-12">
            <div class="contact-info">
              <div class="phone-number"> <i class="fa fa-phone"></i> <a :href="'tel:'+contact.Phone_Number_1">{{contact.Phone_Number_1}}</a> </div>
              <div class="email"> <i class="fa fa-envelope"></i> <a :href="'tel:'+contact.Email"> {{contact.Email }}</a> </div>
            </div>
          </div>
        
        </div>
      </div>
    </div>
    <nav id="navbar-main" class="navbar navbar-default">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              <a class="navbar-brand" href="index.html"><img src="assets/images/logo.png" alt=""></a> </div>
            <div class="collapse navbar-collapse navbar-right" id="myNavbar">
              <ul class="nav navbar-nav">
                <li @click="activate(1)" :class="{ active : active_er == 1 }"><a href="index.html">Home</a></li>
                  <li @click="activate(2)" :class="{ active : active_er == 2 }"><a href="about.html">About Us</a></li>
                <li @click="activate(3)" :class="{ active : active_er == 3 }"><a href="contact.html">Contact Us</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </nav>
  </div>`,
    data() {
        return {

            agencyCode: '',
            contact: { Phone_Number_1: '' },
            active_er: (sessionStorage.active_er) ? sessionStorage.active_er : 1,

        }
    },
    methods: {
        activate: function (el) {
            sessionStorage.active_er = el;
            this.active_er = el;
            if (el == 1 || el == 2) {
                maininstance.actvetab = el;
            }
            else {
                maininstance.actvetab = 0;
            }
        },
        getPageheader: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {

                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Contact Us/Contact Us/Contact Us.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var contact = self.pluck('Contact_Details', self.data.area_List);
                            if (contact.length > 0) {
                                self.contact.Phone_Number_1 = self.pluckcom('Phone_Number', contact[0].component);
                                self.contact.Email = self.pluckcom('Email', contact[0].component);
                            }
                        }).catch(function (error) {
                            console.log('Error');
                            self.content = [];
                        });
                    }

                });

            });

        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
    },
    mounted: function () {

        this.getPageheader();
    },
})
var headerinstance = new Vue({
    el: 'header',
    name: 'headerArea',
    data() {
        return {
            key: '',
            content: null,
            getdata: true
        }

    },

});
Vue.component('footeritem', {
    template: `
    <div>
    <div class="container">
    	<div class="row">
            <div class="col-md-12">
                    <div class="newsletter">
                        <div class="col-md-6">
                            <h3>Newsletter</h3>
                            <p>Sign up for our mailing list to get updates and offers.</p>
                        </div>
                        
                        <div class="col-md-6">
                            <div class="input-form">
                                <input type="email" id="email" placeholder="Enter Your Email Address.." v-model="newsltremail">
                                <button v-on:click="sendnewsletter"><i class="fa fa-paper-plane"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="col-md-4 col-sm-4 footer-border">
            <div class="about-footer">
              <div class="ftr-logo"><a href="#"><img src="assets/images/footer-logo.png" alt="footer-logo"></a></div>
                
                <p>{{Description.Description}}</p>
                
                <ul class="social">
                <li><a :href="Follow.Facebook_Link" target="_blank"><i aria-hidden="true" class="fa fa-facebook"></i></a></li>
                <li><a :href="Follow.Twitter_Link" target="_blank"><i aria-hidden="true" class="fa fa-twitter"></i></a></li>
                <li><a :href="Follow.Linkedin_Link" target="_blank"><i aria-hidden="true" class="fa fa-linkedin"></i></a></li>
                <li><a :href="Follow.Instagram_Link" target="_blank"><i aria-hidden="true" class="fa fa-instagram"></i></a></li>
              </ul>
                
            </div>
          </div>
            <div class="col-md-4 col-sm-4 footer-border">
                <div class="contact-footer">
                    <h3>Contact Info</h3>
                    
                    <ul>
                        <li>
                            <i class="fa fa-map-marker"></i>
                            <h4>Address</h4>
                            <p>{{contact.Address}}</p>
                        </li>
                        
                        <li>
                            <i class="fa fa-envelope"></i>
                            <h4>Email</h4>
                            <a :href="'mailto:'+contact.Email">{{contact.Email}}</a>
                        </li>
                        
                        <li>
                            <i class="fa fa-phone"></i>
                            <h4>Phone Number</h4>
                            <a :href="'tel:'+contact.Phone_Number_1">{{contact.Phone_Number_1}}</a>
                        </li>
                        
                        
                        
                    </ul>
                </div>
            </div>
            <div class="col-md-4 col-sm-4 footer-border">
                <div class="quick-link">
                    <h3>Quick Links</h3>
                    
                    <ul>
                        <li><i class="fa  fa-angle-double-right"></i><a href="index.html">Home</a></li>
                        <li><i class="fa  fa-angle-double-right"></i><a href="about.html">About Us</a></li>
                        <li><i class="fa  fa-angle-double-right"></i><a href="packags.html">Holiday Packages</a></li>
                        <li><i class="fa  fa-angle-double-right"></i><a href="contact.html">Contact Us</a></li>
                        <li><i class="fa  fa-angle-double-right"></i><a href="terms-of-use.html">Terms of Use</a></li>
                        <li><i class="fa  fa-angle-double-right"></i><a href="privacy-policy.html">Privacy Policy</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
  <div class="ftr-btm">
    <div class="container">
        	<div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="copyright">@2020. Flyonline - All Rights Reserved.</div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
               <!-- <div class="powered-by">Powered by: &nbsp; <a href="http://www.oneviewit.com/" target="_blank">
                <img alt="" src="assets/images/oneview-logo.png">
                </a>
                </div> -->

              </div>
            </div>
          
    </div>
  </div>
  </div>`,
    data() {
        return {
            commonStore: vueCommonStore.state,
            agencyCode: '',
            contact: { Address: '', Phone_Number: '' },
            newsltremail: null,
            Logos: { Logo: '' },
            Follow: { Facebook_Link: '' },
            Description: { Description: '' },
            notificationEmail: ""
        }
    },
    methods: {
        getPagefooter: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {

                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var Footer = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Footer/Footer/Footer.ftl';
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Contact Us/Contact Us/Contact Us.ftl';
                        //var Page = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Home/Home/Home.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var Notification = self.pluck('Notifications', response.data.area_List);
                            if (Notification.length > 0) {
                                self.notificationEmail = self.pluckcom('Notification_Email', Notification[0].component);
                            }
                            var Follow = self.pluck('Social_Media', self.data.area_List);
                            if (Follow.length > 0) {
                                self.Follow.Facebook_Link = self.pluckcom('Facebook_Link', Follow[0].component);
                                self.Follow.Twitter_Link = self.pluckcom('Twitter_Link', Follow[0].component);
                                self.Follow.Linkedin_Link = self.pluckcom('LinkedIn_Link_', Follow[0].component);
                                self.Follow.Instagram_Link = self.pluckcom('Instagram_Link', Follow[0].component);
                            }
                            var contact = self.pluck('Contact_Details', self.data.area_List);
                            if (contact.length > 0) {
                                self.contact.Address = self.pluckcom('Address', contact[0].component);
                                self.contact.Phone_Number_1 = self.pluckcom('Phone_Number', contact[0].component);
                                self.contact.Email = self.pluckcom('Email', contact[0].component);
                            }
                        }).catch(function (error) {
                            console.log('Error');
                            self.content = [];
                        });
                        axios.get(Footer, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var Description = self.pluck('Footer', self.data.area_List);
                            if (Description.length > 0) {
                                self.Description.Description = self.pluckcom('Description', Description[0].component);
                            }
                        }).catch(function (error) {
                            console.log('Error');
                            self.content = [];
                        });
                    }

                });

            });

        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        },
        sendnewsletter: async function () {

            if (!this.newsltremail) {
                alertify.alert('Alert', 'Email Id required.');
                return false;;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.newsltremail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.');
                return false;
            } else {
                var agencyCode = this.agencyCode
                var filterValue = "type='Newsletter' AND keyword1='" + this.newsltremail + "'";
                var allDBData = await this.getDbData4Table(agencyCode, filterValue, "date1");

                if (allDBData != undefined && allDBData.length > 0) {
                    alertify.alert('Alert', 'Email address already enabled.').set('closable', false);
                    return false;
                }
                else {
                    var logourl = window.location.origin + "/Login/flyonline/website-informations/logo/logo.png"
                    var postData = {
                        type: "UserAddedRequest",
                        fromEmail: this.notificationEmail || this.commonStore.fallBackEmail,
                        toEmails: Array.isArray(this.newsltremail) ? this.newsltremail : [this.newsltremail],
                        logo: logourl || "",
                        agencyName: "FLYONLINE",
                        agencyAddress: this.contact.Address || "",
                        personName: this.newsltremail.split("@")[0],
                        primaryColor: "#ffffff",
                        secondaryColor: "#364793"
                    };
                    var agencyCode = this.agencyCode
                    let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                    let insertSubscibeData = {
                        type: "Newsletter",
                        date1: requestedDate,
                        keyword1: this.newsltremail,
                        keyword2: "Subscribe Newsletter",
                        nodeCode: agencyCode
                    };
                    let responseObject = await this.cmsRequestData("POST", "cms/data", insertSubscibeData, null);
                    try {
                        let insertID = Number(responseObject);
                        var self = this;
                        mailUrl = self.commonStore.hubUrls.emailServices.emailApi;
                        sendMailService(mailUrl, postData);
                        this.newsltremail = '';
                        alertify.alert('Newsletter', 'Thank you for subscribing !');
                    } catch (e) {

                    }
                }

            }
        },
        async getDbData4Table(agencyCode, extraFilter, sortField) {

            var allDBData = [];
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            var cmsURL = huburl + portno + '/cms/data/search/byQuery';
            var queryStr = "select * from cms_forms_data where nodeCode = '" + agencyCode + "'";
            if (extraFilter != undefined && extraFilter != '') {
                queryStr = queryStr + " AND " + extraFilter;
            }
            var requestObject = {
                query: queryStr,
                sortField: sortField,
                from: 0,
                orderBy: "desc"
            };
            let responseObject = await this.cmsRequestData("POST", "cms/data/search/byQuery", requestObject, null);
            if (responseObject != undefined && responseObject.data != undefined) {
                allDBData = responseObject.data;
            }
            return allDBData;

        }

    },
    mounted: function () {
        this.getPagefooter();
    },

})
var footerinstance = new Vue({
    el: 'footer',
    name: 'footerArea',
    data() {
        return {
            key: 0,
            content: null,
            getdata: true
        }

    },
});
