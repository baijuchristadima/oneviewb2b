var aboutus = new Vue({
    el: '#homepage',
    name: 'homepage',
    data: {
        commonStore: vueCommonStore.state,
        agencyCode: '',
        packages:[]
    },
    filters: {

      subStr: function (string) {
          if (string.length > 8)
              return string.substring(0, 9) + '...';

          else
              return string;
      }

  },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var Packageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Master Table/Packages/Packages/Packages.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(Packageurl, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                          }).then(function (response) {
                            self.content = response.data;
                            let holidayPackageListTemp = [];
                            if (self.content != undefined && self.content.Values != undefined) {
                              holidayPackageListTemp = self.content.Values.filter(function (el) {
                                return el.Status == true && el.Show_In_Home_Page == true
                              });
                            }
                            self.packages = holidayPackageListTemp;
                            setTimeout(function () { packages() }, 1000);                
                          }).catch(function (error) {
                            console.log('Error');
                            self.content = [];
                          });
                    }

                });

            });

        },
        getmoreinfo(url) {
            if (url != null) {
              if (url != "") {
                url = url.split("/Template/")[1];
                url = url.split(' ').join('-');
                url = url.split('.').slice(0, -1).join('.');
                url = "package-detail.html?page=" + url + "&from=pkg";
                window.location.href = url;
              }
              else {
                url = "#";
              }
            }
            else {
              url = "#";
            }
            return url;
          }
    },
    mounted: function () {
        sessionStorage.active_er=1; 
        this.getPagecontent();
    },
});
function getQueryStringValue(key) {
  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}
function packages(){
    $("#owl-demo-3").owlCarousel({
        autoplay:true,
        autoPlay : 8000,
        autoplayHoverPause:true, 
        stopOnHover : false,  
        items : 3,
        margin:10,  
        lazyLoad : true,
        navigation : true,
        itemsDesktop : [1199, 3],
        itemsDesktopSmall : [991,2],
          itemsTablet : [600, 1]
      });
          $( ".owl-prev").html('<i class="fa  fa-chevron-left"></i>');
         $( ".owl-next").html('<i class="fa  fa-chevron-right"></i>');
}