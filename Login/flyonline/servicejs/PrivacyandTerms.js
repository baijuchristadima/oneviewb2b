var aboutus = new Vue({
    el: '#PrivacyandTerms',
    name: 'PrivacyandTerms',
    data: {
        commonStore: vueCommonStore.state,
        agencyCode: '',
        Terms:[],
        Policy:[]
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var cmsUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Footer/Footer/Footer.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(cmsUrl, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var Terms = self.pluck('Terms_Of_Use', self.data.area_List);
                            if (Terms.length > 0) {
                                self.Terms = self.pluckcom('Terms_Of_Use_List', Terms[0].component);
                            }
                            var Policy = self.pluck('Privacy_Policy', self.data.area_List);
                            if (Policy.length > 0) {
                                self.Policy = self.pluckcom('Privacy_Policy_List', Policy[0].component);
                            }
                        }).catch(function (error) {
                            console.log('Error');
                        });
                    }

                });

            });

        }
    },
    mounted: function () {
        sessionStorage.active_er=5; 
        this.getPagecontent();
    },
});