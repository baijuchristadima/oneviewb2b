var aboutus = new Vue({
    el: '#Packagepage',
    name: 'Packagepage',
    data: {
        commonStore: vueCommonStore.state,
        agencyCode: '',
        packages:[],
        totalpackage: 0,
        currentPage: 1,
        currentPages: 1,
        fromPage: 1,
        totalpage: 1,
        constructedNumbers: [],
        showPackages: [],
        pageLimit: 3,
        paginationLimit: 1,
    },
    filters: {

      subStr: function (string) {
          if (string.length > 8)
              return string.substring(0, 9) + '...';

          else
              return string;
      }

  },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var Packageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Master Table/Packages/Packages/Packages.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(Packageurl, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                          }).then(function (response) {
                            self.content = response.data;
                            let holidayPackageListTemp = [];
                            if (self.content != undefined && self.content.Values != undefined) {
                              holidayPackageListTemp = self.content.Values.filter(function (el) {
                                return el.Status == true
                              });
                            }
                            self.packages = holidayPackageListTemp;
                            self.setTotalPackageCount();                   
                          }).catch(function (error) {
                            console.log('Error');
                            self.content = [];
                          });
                    }

                });

            });

        },
        setTotalPackageCount: function () {
          if (this.packages != undefined && this.packages != undefined) {
            this.totalpackage = Number(this.packages.length);
            this.totalpage = Math.ceil(this.totalpackage / this.pageLimit);
            this.currentPage = 1;
            this.fromPage = 1;
            if (Number(this.totalpackage) < 6) {
              // this.pageLimit=3;
            }
    
            this.constructAllPagianationLink();
    
          }
        },
        constructAllPagianationLink: function () {
          let limit = this.paginationLimit;
          this.constructedNumbers = [];
          for (let i = Number(this.fromPage); i <= (Number(this.totalpage) + limit); i++) {
            if (i <= Number(this.totalpage)) {
              this.constructedNumbers.push(i);
    
            }
    
          }
          this.currentPage = this.constructedNumbers;
          this.setPackageItems();
        },
        prevNextEvent: function (type) {
    
          let limit = this.paginationLimit;
          if (type == 'Previous') {
            if (this.currentPages > this.totalpage) {
              this.currentPages = this.currentPages - 1;
            }
            this.fromPage = this.currentPages;
            if (Number(this.fromPage) != 1) {
              this.currentPages = Number(this.currentPages) - 1;
              this.setPackageItems();
            }
    
          } else if (type == 'Next') {
            if (this.currentPages == 'undefined' || this.currentPages == '') {
            }
            if (this.currentPages <= this.totalpage) {
              let limit = this.paginationLimit;
              this.fromPage = this.currentPages;
              var totalP = (this.totalpage) + 1;
              if (Number(this.fromPage) != totalP) {
                var count = this.currentPages + limit;
                if (Number(count) <= Number(this.totalpage)) {
                  this.selectPackages(this.currentPages);
                }
              }
            }
          }
        },
        selectPackages: function (ev) {
          let limit = 1;
          this.currentPages = this.currentPage[ev];
          this.setPackageItems();
        },
        selected: function (ev) {
          let limit = 1;
          this.currentPages = this.currentPage[ev];
          this.setPackageItems();
        },
        setPackageItems: function () {
          if (this.packages != undefined && this.packages != undefined) {
            let start = 0;
            let end = Number(start) + Number(this.pageLimit);
            if (Number(this.currentPages) == 1) {
            } else {
              var limit = this.totalpage;
              start = (Number(this.currentPages) + Number(this.pageLimit)) - 2;
              end = Number(start) + Number(this.pageLimit);
            }
            this.showPackages = this.packages.slice(start, end);
          }
        },
        getmoreinfo(url) {
            if (url != null) {
              if (url != "") {
                url = url.split("/Template/")[1];
                url = url.split(' ').join('-');
                url = url.split('.').slice(0, -1).join('.');
                url = "package-detail.html?page=" + url + "&from=pkg";
                window.location.href = url;
              }
              else {
                url = "#";
              }
            }
            else {
              url = "#";
            }
            return url;
          }
    },
    mounted: function () {
        sessionStorage.active_er=5; 
        this.getPagecontent();
    },
});
function getQueryStringValue(key) {
  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}