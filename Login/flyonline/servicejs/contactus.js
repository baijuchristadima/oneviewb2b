var contact = new Vue({
    el: '#contactuspage',
    name: 'contactuspage',
    data: {
        commonStore: vueCommonStore.state,
        contact: { Phone_Number_1: '' },
        cntname: '',
        cntemail: '',
        cntcontact: '',
        cntsubject: '',
        cntmessage: '',
        agencyCode: '',
        Follow: { Facebook_Link: '' },
        notificationEmail: ""
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Contact Us/Contact Us/Contact Us.ftl';
                        //var home = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Home/Home/Home.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var Notification = self.pluck('Notifications', response.data.area_List);
                            if (Notification.length > 0) {
                                self.notificationEmail = self.pluckcom('Notification_Email', Notification[0].component);
                            }
                            var contact = self.pluck('Contact_Details', self.data.area_List);
                            if (contact.length > 0) {
                                self.contact.Address = self.pluckcom('Address', contact[0].component);
                                self.contact.Phone_Number_1 = self.pluckcom('Phone_Number', contact[0].component);
                                self.contact.Email = self.pluckcom('Email', contact[0].component);
                                self.contact.Location_Link = self.pluckcom('Location_Link', contact[0].component);
                                self.contact.Title_Description = self.pluckcom('Title_Description', contact[0].component);
                                self.contact.Helpline_Number = self.pluckcom('Helpline_Number', contact[0].component);
                            }
                            var Follow = self.pluck('Social_Media', self.data.area_List);
                            if (Follow.length > 0) {
                                self.Follow.Facebook_Link = self.pluckcom('Facebook_Link', Follow[0].component);
                                self.Follow.Twitter_Link = self.pluckcom('Twitter_Link', Follow[0].component);
                                self.Follow.Linkedin_Link = self.pluckcom('LinkedIn_Link_', Follow[0].component);
                                self.Follow.Instagram_Link = self.pluckcom('Instagram_Link', Follow[0].component);
                            }
                        }).catch(function (error) {
                        });
                    }

                });

            });

        },
        sendcontactus: function () {
            if (!this.cntname) {
                alertify.alert('Alert', 'First name required.').set('closable', false);
                return false;
            }
            if (!this.cntemail) {
                alertify.alert('Alert', 'Email Id required.').set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.cntemail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                return false;
            }
            if (!this.cntcontact) {
                alertify.alert('Alert', 'Mobile number required.').set('closable', false);
                return false;
            }
            if (this.cntcontact.length < 8) {
                alertify.alert('Alert', 'Enter valid mobile number.').set('closable', false);
                return false;
            }
            if (!this.cntsubject) {
                alertify.alert('Alert', 'Subject required.').set('closable', false);
                return false;
            } if (!this.cntmessage) {
                alertify.alert('Alert', 'Message required.').set('closable', false);
                return false;
            } else {
                var self = this;
                var logourl = window.location.origin + "/Login/flyonline/website-informations/logo/logo.png"
                var frommail = this.notificationEmail || this.commonStore.fallBackEmail;
                var postData = {
                    type: "UserAddedRequest",
                    fromEmail: frommail,
                    toEmails: Array.isArray(this.cntemail) ? this.cntemail : [this.cntemail],
                    logo: logourl || "",
                    agencyName: "FLYONLINE",
                    agencyAddress: this.contact.Address || "",
                    personName: this.cntname || "",
                    primaryColor: "#ffffff",
                    secondaryColor: "#364793"
                };
                var agencyCode = this.agencyCode
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                let insertContactData = {
                    type: "Contact Us",
                    keyword1: this.cntname,
                    keyword2: this.cntsubject,
                    keyword3: this.cntemail,
                    keyword5: this.cntcontact,
                    text1: this.cntmessage,
                    date1: requestedDate,
                    nodeCode: agencyCode
                };
                let responseObject = this.cmsRequestData("POST", "cms/data", insertContactData, null);
                try {
                    let insertID = Number(responseObject);
                    var self = this;
                    mailUrl = self.commonStore.hubUrls.emailServices.emailApi;
                    sendMailService(mailUrl, postData);
                    this.cntemail = '';
                    this.cntfirstname = '';
                    this.cntlastname = '';
                    this.cntcontact = '';
                    this.cntmessage = '';
                    alertify.alert('Contact us', 'Thank you for contacting us.We shall get back to you.');
                } catch (e) {

                }
            }
        },
        cmsRequestData: function (callMethod, urlParam, data, headerVal) {
            var vm = this;
            var huburl = vm.commonStore.hubUrls.hubConnection.cmsUrl;
            var portno = vm.commonStore.hubUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            return fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            }).then(function (response) {
                return response.json();
            });
        }
    },
    mounted: function () {
        this.getPagecontent();
        sessionStorage.active_er = 3;
    },
});