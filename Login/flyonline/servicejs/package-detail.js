var aboutus = new Vue({
    el: '#Packagedetail',
    name: 'Packagedetail',
    data: {
        commonStore: vueCommonStore.state,
        agencyCode: '',
        Package: { Package_Name: '', Content_1: '', Location_Link: '' },
        Itinerary: [],
        Included: [],
        NotIncluded: [],
        Name: '',
        Email: '',
        Review: '',
        allReview: [],
        avgrating: '',
        ratingcount: '',
        fname: '',
        lname: '',
        femail: '',
        fphone: '',
        fdate: '',
        fadult: 1,
        fchild: 0,
        finfants: 0,
        fmessage: '',
        notificationEmail: ""
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        packageurl = getQueryStringValue('page');
                        if (packageurl != "") {

                            packageurl = packageurl.split('-').join(' ');
                            //  langauage = packageurl.split('_')[1];
                            packageurl = packageurl.split('_')[0];
                            self.pageURLLink = packageurl;
                            var topackageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/' + packageurl + '.ftl';
                            var contactPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Contact Us/Contact Us/Contact Us.ftl';

                            axios.get(contactPage, {
                                headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                            }).then(function (response) {
                                var Notification = self.pluck('Notifications', response.data.area_List);
                                if (Notification.length > 0) {
                                    self.notificationEmail = self.pluckcom('Notification_Email', Notification[0].component);
                                }
                            }).catch(function (error) {
                                console.log('Error');
                            });

                            axios.get(topackageurl, {
                                headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                            }).then(function (response) {
                                self.content = response.data;
                                var pagecontent = self.pluck('main', self.content.area_List);
                                if (pagecontent.length > 0) {
                                    self.Package.Package_Name = self.pluckcom('Package_Name', pagecontent[0].component);
                                    self.Package.Package_Image = self.pluckcom('Package_Image', pagecontent[0].component);
                                    self.Package.Price = self.pluckcom('Price', pagecontent[0].component);
                                    self.Package.Facebook_Link = self.pluckcom('Facebook_Link', pagecontent[0].component);
                                    self.Package.No_of_Days = self.pluckcom('No_of_Days', pagecontent[0].component);
                                    self.Package.No_of_Nights = self.pluckcom('No_of_Nights', pagecontent[0].component);
                                    self.Package.Twitter_Link = self.pluckcom('Twitter_Link', pagecontent[0].component);
                                    self.Package.LinkedIn_Link = self.pluckcom('LinkedIn_Link', pagecontent[0].component);
                                    self.Package.Instagram_Link = self.pluckcom('Instagram_Link', pagecontent[0].component);
                                    self.Validity = self.pluckcom('Validity', pagecontent[0].component);
                                }

                                var pagecontent = self.pluck('Itinerary', self.content.area_List);
                                if (pagecontent.length > 0) {
                                    self.Itinerary = self.pluckcom('Itinerary_List', pagecontent[0].component);
                                }

                                var pagecontent = self.pluck('Description', self.content.area_List);
                                if (pagecontent.length > 0) {
                                    self.Package.Content_1 = self.pluckcom('Content_1', pagecontent[0].component);
                                    self.Package.Departure = self.pluckcom('Departure', pagecontent[0].component);
                                    self.Package.Departure_Time = self.pluckcom('Departure_Time', pagecontent[0].component);
                                    self.Package.Return_Time = self.pluckcom('Return_Time', pagecontent[0].component);
                                    self.Package.Content_2 = self.pluckcom('Content_2', pagecontent[0].component);
                                    self.Package.Included = self.pluckcom('Included', pagecontent[0].component);
                                    self.Included = self.Package.Included;
                                    self.Package.Not_Included = self.pluckcom('Not_Included', pagecontent[0].component);
                                    self.NotIncluded = self.Package.Not_Included;

                                }
                                var pagecontent = self.pluck('Location', self.content.area_List);
                                if (pagecontent.length > 0) {
                                    self.Package.Location_Link = self.pluckcom('Location_Link', pagecontent[0].component);
                                }

                            }).catch(function (error) {
                                //window.location.href = "/Alwafra/package-detail.html";
                                self.content = [];
                                self.getdata = true;
                            });
                            self.viewReview();
                        }
                    }

                });

            });

        },
        userRating: function (num) {
            var ulList = document.getElementById("ratingID");
            let m = 0;
            for (let i = 0; i < ulList.childNodes.length; i++) {
                let childNode = ulList.childNodes[i];
                if (childNode.childNodes != undefined && childNode.childNodes.length > 0) {
                    m = m + 1;
                    for (let k = 0; k < childNode.childNodes.length; k++) {
                        let style = childNode.childNodes[k].style.color;
                        if ((m) < Number(num)) {
                            childNode.childNodes[k].style = "color: rgb(239, 158, 8)";
                        } else if ((m) == Number(num)) {
                            if (style.trim() == "rgb(239, 158, 8)") {
                                childNode.childNodes[k].style = "color: a9a9a9;";
                            } else {
                                childNode.childNodes[k].style = "color: rgb(239, 158, 8);";
                            }
                        } else {
                            childNode.childNodes[k].style = "color: a9a9a9";
                        }
                    }
                }
            }
        },
        getUserRating: function () {
            var ulList = document.getElementById("ratingID");
            let m = 0;
            for (let i = 0; i < ulList.childNodes.length; i++) {
                let childNode = ulList.childNodes[i];
                if (childNode.childNodes != undefined && childNode.childNodes.length > 0) {
                    let style = "";
                    for (let k = 0; k < childNode.childNodes.length; k++) {
                        style = childNode.childNodes[k].style.color;
                        if (style != undefined && style != '') {
                            break;
                        }
                    }
                    if (style.trim() == "a9a9a9") {
                        break;
                    } else if (style.trim() == "rgb(239, 158, 8)") {
                        m = m + 1;
                    }
                }
            }
            return m;
        },
        addReviews: function () {
            let ratings = this.getUserRating();
            ratings = Number(ratings);
            if (ratings == 0) {
                alertify.alert('Alert', 'Rating required');
                return;
            }
            let requestedDate = new Date();
            var self = this;
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.Email.match(emailPat);
            if (this.Name == undefined || this.Name == '') {
                alertify.alert('Alert', 'Name required.').set('closable', false);
                return;
            } else if (this.Email == undefined || this.Email == '') {
                alertify.alert('Alert', 'Email Id required.').set('closable', false);
                return;

            } else if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                return false;
            } else if (this.Review == undefined || this.Review == '') {
                alertify.alert('Alert', 'Review required.').set('closable', false);
                return;
            } else {
                var agencyCode = this.agencyCode
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                let requestObject = {
                    type: "Booking Review",
                    keyword4: self.pageURLLink,
                    keyword1: self.Package.Package_Name,
                    keyword2: self.Name,
                    keyword3: self.Email,
                    text1: self.Review,
                    number1: ratings,
                    date1: requestedDate,
                    nodeCode: agencyCode
                };
                var self = this;
                var logourl = window.location.origin + "/Login/flyonline/website-informations/logo/logo.png"
                var frommail = this.notificationEmail || this.commonStore.fallBackEmail;
                var postData = {
                    type: "UserAddedRequest",
                    fromEmail: frommail,
                    toEmails: Array.isArray(self.Email) ? self.Email : [self.Email],
                    logo: logourl || "",
                    agencyName: "FLYONLINE",
                    agencyAddress: "Tourist Club Area, Behind ADCB, Abu Dhabi",
                    personName: self.Name || "",
                    message: self.Review || "",
                    primaryColor: "#ffff",
                    secondaryColor: "#364793"
                };
                let responseObject = this.cmsRequestData("POST", "cms/data", requestObject, null);
                try {
                    let insertID = Number(responseObject);
                    mailUrl = self.commonStore.hubUrls.emailServices.emailApi;
                    sendMailService(mailUrl, postData);
                    this.Name = "";
                    this.Email = "";
                    this.Review = "";
                    document.getElementById("ratingID").value = null;
                    alertify.alert('Review', 'Thank you for Review.');
                    setTimeout(function () { self.viewReview(); }, 3000);
                    //this.viewReview();
                } catch (e) {
                }
            }
        },
        viewReview: function () {
            var self = this;
            var agencyCode = this.agencyCode
            let allReview = [];
            let requestObject = { from: 0, size: 100, type: "Booking Review", nodeCode: agencyCode, orderBy: "desc" };
            let responseObject = this.cmsRequestData("POST", "cms/data/search", requestObject, null).then(function (responseObject) {
                if (responseObject != undefined && responseObject.data != undefined) {
                    allResult = JSON.parse(JSON.stringify(responseObject)).data;
                    for (let i = 0; i < allResult.length; i++) {
                        if (allResult[i].keyword4 == self.pageURLLink) {
                            let object = {
                                Name: allResult[i].keyword2,
                                Date: moment(String(allResult[i].date1), "YYYY-MM-DDThh:mm:ss").format('MMM DD,YYYY'),
                                comment: allResult[i].text1,
                                Ratings: allResult[i].number1,
                            };
                            allReview.push(object);
                        }
                    }
                    self.allReview = allReview;



                }
                self.ratingcount = allReview.length;
                allratingcount = self.ratingcount;
                var avgratingtemp = 0;
                for (let i = 0; i < self.ratingcount; i++) {

                    avgratingtemp = avgratingtemp + allReview[i].Ratings;
                }
                self.avgrating = ((avgratingtemp) / allratingcount).toFixed(2);
                if (self.avgrating > 0) {
                    self.reviewAvailable = true;
                }
            });
        },
        addbooking: function () {
            let dateObj = document.getElementById("from");
            let dateValue = dateObj.value;
            // datevalue = moment(dateValue, 'DD/MM/YYYY').format('YYYY-MM-DD[T]HH:mm:ss');
            dateValue = moment(String(dateValue)).format('YYYY-MM-DDThh:mm:ss');
            var self = this;
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.femail.match(emailPat);
            if (this.fname == undefined || this.fname == '') {
                alertify.alert('Alert', 'First name required.').set('closable', false);
                return;
            }
            else if (this.femail == undefined || this.femail == '') {
                alertify.alert('Alert', 'Email Id required.').set('closable', false);
                return;

            }
            else if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                return false;
            } else if (this.fphone == undefined || this.fphone == '') {
                alertify.alert('Alert', 'Mobile number required.').set('closable', false);
                return;
            }
            else if (dateValue == undefined || dateValue == '') {
                alertify.alert('Alert', 'Select Date.').set('closable', false);
                return;

            }

            else if (this.fadult == undefined || this.fadult == '' || this.fadult == 'AL') {
                alertify.alert('Alert', 'Please select adult.').set('closable', false);
                return;
            } else {
                let dateValue = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                var agencyCode = this.agencyCode
                var requestObject = {
                    type: "Package Booking",
                    keyword1: self.fname,
                    keyword6: self.lname,
                    keyword3: self.femail,
                    keyword4: self.fphone,
                    keyword5: self.Package.Package_Name,
                    date1: dateValue,
                    number1: self.fadult,
                    number2: self.fchild,
                    number3: self.finfants,
                    text1: self.fmessage,
                    nodeCode: agencyCode
                };
                var self = this;
                var logourl = window.location.origin + "/Login/flyonline/website-informations/logo/logo.png"
                var frommail = this.notificationEmail || this.commonStore.fallBackEmail;
                var custmail = {
                    type: "UserAddedRequest",
                    fromEmail: frommail,
                    toEmails: Array.isArray(self.femail) ? self.femail : [self.femail],
                    logo: logourl || "",
                    agencyName: "FLYONLINE",
                    agencyAddress: "Tourist Club Area, Behind ADCB, Abu Dhabi",
                    personName: self.fname || "",
                    primaryColor: "#ffff",
                    secondaryColor: "#364793"
                };
                let responseObject = this.cmsRequestData("POST", "cms/data", requestObject, null);
                try {

                    let insertID = Number(responseObject);
                    mailUrl = self.commonStore.hubUrls.emailServices.emailApi;
                    sendMailService(mailUrl, custmail);
                    alertify.alert('Success', 'Thank you for Booking.We shall get back to you.');
                    self.fname = "";
                    self.lname = "";
                    self.femail = "";
                    self.fphone = "";
                    self.fdate = "";
                    self.fadult = "";
                    self.fchild = "0";
                    self.finfants = "0";
                    self.fmessage = "";
                    // document.getElementById('Infantmyselect').innerText = null
                    // document.getElementById('Childmyselect').innerText = null
                } catch (e) {

                }
            }
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var vm = this;
            var huburl = vm.commonStore.hubUrls.hubConnection.cmsUrl;
            var portno = vm.commonStore.hubUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        },
        setCalender() {
            var dateFormat = "dd/mm/yy"
            $("#from").datepicker({
              minDate: "0d",
              maxDate: "360d",
              numberOfMonths: 1,
              changeMonth: true,
              changeYear: true,
              showOn: "both",
              duration: "fast",
              showAnim: "slide",
              showOptions: { direction: "up" },
              showButtonPanel: false,
              dateFormat: dateFormat,
                
            });
        },

    },
    mounted: function () {
        sessionStorage.active_er=5;
        //this.viewReview();
        this.getPagecontent();
        this.setCalender();
    },
});
function getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}
