var platform = new Vue({
  el: '#platform',
  data: {
    bannerSection: {},
    platformSection: {}
  },
  methods: {
    getPagecontent: function () {
      var self = this;
      self.isLoading = true;
      axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
        var agencyFolderName = '';
        var agy;
        try {
          response.data.forEach(function (agent, agentIndex) {
            agent.domain.forEach(function (dom, domIndex) {
              if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                agy = agent;
                agencyFolderName = agy.agencyFolderName;
              }
            });
          });
        } catch (err) {}
        agy.registeredUsers.forEach(function (agyCode, domIndex) {
          if (domIndex == 0) {
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            var cmsUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Platform Page/Platform Page/Platform Page.ftl';
            axios.get(cmsUrl, {
              headers: {
                'content-type': 'text/html',
                'Accept': 'text/html',
                'Accept-Language': langauage
              }
            }).then(function (response) {
              self.content = response.data;
              if (self.content != undefined && self.content != null) {
                var mainData = pluck('Banner_Section', self.content.area_List);
                self.bannerSection = getAllMapData(mainData[0].component);

                var mainData = pluck('Main_Section', self.content.area_List);
                self.platformSection = getAllMapData(mainData[0].component);
              }
            }).catch(function (error) {
              console.log('Error');

            });
          }

        });

      });

    },
  },
  mounted: function () {
    this.getPagecontent();
  },

});