var service = new Vue({
    el: '#servicedetail',
    name: 'servicedetail',
    data: {

        bannerContent: {},
        serviceContent: {},
        offerList:[]
    },
    methods: {
        getPagecontent: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) {}

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        packageurl = getQueryStringValue('page');
                        if (packageurl != "") {

                            packageurl = packageurl.split('-').join(' ');
                            //  langauage = packageurl.split('_')[1];
                            packageurl = packageurl.split('_')[0];
                            self.pageURLLink = packageurl;
                            var serviceUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/' + packageurl + '.ftl';
                            var offerUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Master Table/Offers/Offers/Offers.ftl';
                            axios.get(serviceUrl, {
                                headers: {
                                    'content-type': 'text/html',
                                    'Accept': 'text/html',
                                    'Accept-Language': langauage
                                }
                            }).then(function (response) {
                                self.content = response.data;
                                if (self.content != undefined && self.content != null) {
                                    var banner = pluck('Banner_Section', self.content.area_List);
                                    self.bannerContent = getAllMapData(banner[0].component);
                                    document.title = self.bannerContent.Title;
                                    var service = pluck('Service_Details', self.content.area_List);
                                    self.serviceContent = getAllMapData(service[0].component);

                                }
                            }).catch(function (error) {});

                            axios.get(offerUrl, {
                                headers: {
                                    'content-type': 'text/html',
                                    'Accept': 'text/html',
                                    'Accept-Language': langauage
                                }
                            }).then(function (response) {
                                var data = response.data;
                                let offerListTemp = [];
                                if (data != undefined && data.Values != undefined) {
                                    offerListTemp = data.Values.filter(function (el) {
                                        return el.Is_Active == true && el.Category == self.serviceContent.Title;
                                    });
                                }
                                self.offerList = offerListTemp;
                            }).catch(function (error) {});
                        }
                    }

                });

            });
        },
        dateFormatter: function (utc) {
            return (moment(utc).utcOffset("+05:30").format("DD MMMM YYYY"));
        },
    },
    mounted: function () {
        this.getPagecontent();
    },
});