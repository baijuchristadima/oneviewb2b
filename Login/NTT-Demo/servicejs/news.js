var news = new Vue({
    el: '#news',
    name: 'news',
    data: {
        agencyCode: '',
        PageDetails: {},
        newsList: [],
    },
    filters: {
        contentStr: function (string) {
          string = string.replace(/<\/?[^>]+(>|$)/g, "");
          if (string.length > 10)
            return string.substr(0, 40) + '...';
    
          else
            return string;
        }
      },
    methods: {
        getPagecontent: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) {}
                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/News/News/News.ftl';
                        var newsUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Master Table/Latest News List/Latest News List/Latest News List.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                          axios.get(cmsPage, {
                            headers: {
                              'content-type': 'text/html',
                              'Accept': 'text/html',
                              'Accept-Language': langauage
                            }
                          }).then(function (response) {
                            self.content = response.data;
                            if (self.content != undefined && self.content != null) {
                              var data = pluck('Page_Details', self.content.area_List);
                              self.PageDetails = getAllMapData(data[0].component);


                            }
                          }).catch(function (error) {
                            console.log('Error');
                          });
                        axios.get(newsUrl, {
                            headers: {
                                'content-type': 'text/html',
                                'Accept': 'text/html',
                                'Accept-Language': langauage
                            }
                        }).then(function (response) {
                            var data = response.data;
                            let newsListTemp = [];
                            if (data != undefined && data.Values != undefined) {
                                newsListTemp = data.Values.filter(function (el) {
                                    return el.Is_Active == true
                                });
                            }
                            self.newsList = newsListTemp;
                        }).catch(function (error) {});
                    }
                });
            });
        },
        dateFormatter: function (utc) {
            return (moment(utc).utcOffset("+05:30").format("DD MMMM YYYY"));
        },
        getmoreinfo(url) {
            if (url != null) {
              if (url != "") {
                url = url.split("/Template/")[1];
                url = url.split(' ').join('-');
                url = url.split('.').slice(0, -1).join('.');
                url = "news-detail.html?page=" + url + "&from=pkg";
                window.location.href = url;
              } else {
                url = "#";
              }
            } else {
              url = "#";
            }
            return url;
          }
    },
    mounted: function () {
        this.getPagecontent();
    },
});