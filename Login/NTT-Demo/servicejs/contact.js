
var contact = new Vue({
    el: '#contact',
    name:'contact',
    data: {
        commonStore: vueCommonStore.state,
        agencyCode:'',
        BannerSection:{},
        MainArea:{},
        FormSection:{},
        cntusername: '',
        cntemail: '',
        cntcontact: '',
        cntmessage: '',
        isLoading: false,
        fullPage: true
    },
    methods: {
  
      getPageData: function () {
          var self = this;
          axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
              var agencyFolderName = '';
              var agy;
              try {
                  response.data.forEach(function (agent, agentIndex) {
                      agent.domain.forEach(function (dom, domIndex) {
                          if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                              agy = agent;
                              agencyFolderName = agy.agencyFolderName;
                          }
                      });
                  });
              } catch (err) { }

              agy.registeredUsers.forEach(function (agyCode, domIndex) {
                  if (domIndex == 0) {
                      var huburl = HubServiceUrls.hubConnection.cmsUrl;
                      var portno = HubServiceUrls.hubConnection.ipAddress;
                      var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                      var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Contact Us/Contact Us/Contact Us.ftl';
                      var agencyCode = agyCode;
                      self.agencyCode = agencyCode;
                      axios.get(cmsPage, {
                          headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                      }).then(function (response) {
                          self.data = response.data;
                          if (response.data.area_List) {
                            var TopData = pluck("Banner_Area", self.data.area_List);
                            self.BannerSection = getAllMapData(TopData[0].component);
            
                            var mainData = pluck("Main_Area", self.data.area_List);
                            self.MainArea = getAllMapData(mainData[0].component);
            
                            var footerData = pluck("Form_Section", self.data.area_List);
                            self.FormSection = getAllMapData(footerData[0].component);
                          }
                      }).catch(function (error) {
                          console.log('Error');
                      });
                  }

              });


          });

      },
      sendcontact: async function () {
          if (!this.cntusername) {
            alertify.alert('Alert', 'Name required.').set('closable', false);
    
            return false;
          }
          if (!this.cntemail) {
            alertify.alert('Alert', 'Email Id required.').set('closable', false);
            return false;
          }
          var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
          var matchArray = this.cntemail.match(emailPat);
          if (matchArray == null) {
            alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
            return false;
          }
          if (!this.cntcontact) {
            alertify.alert('Alert', 'Mobile number required.').set('closable', false);
            return false;
          }
          if (this.cntcontact.length < 8) {
            alertify.alert('Alert', 'Enter valid mobile number.').set('closable', false);
            return false;
          }
          if (!this.cntmessage) {
            alertify.alert('Alert', 'Message required.').set('closable', false);
            return false;
          } else {
            this.isLoading = true;
            var frommail = this.notificationEmail || this.commonStore.fallBackEmail;
            var logourl = window.location.origin + "/Login/NTT-Demo/website-informations/logo/logo.png"
            var custmail = {
              type: "UserAddedRequest",
              fromEmail: frommail,
              toEmails: Array.isArray(this.cntemail) ? this.cntemail : [this.cntemail],
              logo: logourl || "",
              agencyName: "Nirvana Travel and Tourism",
              agencyAddress: "P.O. Box 41818, Breakwater, Corniche Road,Abu Dhabi Marina Area",
              personName: this.cntusername || "",
              primaryColor: "#11212e",
              secondaryColor: "#f4c927"
            };
            let agencyCode = this.agencyCode;
            let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
            let insertContactData = {
              type: "Contact Us",
              keyword1: this.cntusername,
              keyword3: this.cntemail,
              keyword2: this.cntcontact,
              text1: this.cntmessage,
              date1: requestedDate,
              nodeCode: agencyCode
            };
            let responseObject = await this.cmsRequestData("POST", "cms/data", insertContactData, null);
            try {
              let insertID = Number(responseObject);
              var self = this;
              var emailApi = self.commonStore.hubUrls.emailServices.emailApi;
              sendMailService(emailApi, custmail);
              this.cntemail = '';
              this.cntusername = '';
              this.cntcontact = '';
              this.cntmessage = '';
              this.isLoading = false;
              alertify.alert('Contact us', 'Thank you for contacting us.We shall get back to you.');
            } catch (e) {
              this.isLoading = false;
            }
          }
      },
      async cmsRequestData(callMethod, urlParam, data, headerVal) {
        try {
          var huburl = HubServiceUrls.hubConnection.cmsUrl;
          var portno = HubServiceUrls.hubConnection.ipAddress;
          const url = huburl + portno + "/" + urlParam;
          if (data != null) {
            data = JSON.stringify(data);
          }
          const response = await fetch(url, {
            method: callMethod, // *GET, POST, PUT, DELETE, etc.
            credentials: "same-origin", // include, *same-origin, omit
            headers: { 'Content-Type': 'application/json' },
            body: data, // body data type must match "Content-Type" header
          });
          try {
            const myJson = await response.json();
            return myJson;
          } catch (error) {
            return object;
          }
        } catch (error) {
          this.isLoading = false;
        }
         
      }
      
    },
    mounted: function () {
        this.getPageData();
    },
       
});