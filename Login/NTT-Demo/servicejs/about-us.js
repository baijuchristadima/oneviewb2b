var aboutUs = new Vue({
    el: '#about',
    data: {
        BannerSection:{},
        AboutUsSection: {}    
    },
    methods: {
          getPagecontent: function () {
            var self = this;
            self.isLoading = true;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
              var agencyFolderName = '';
              var agy;
              try {
                response.data.forEach(function (agent, agentIndex) {
                  agent.domain.forEach(function (dom, domIndex) {
                    if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                      agy = agent;
                      agencyFolderName = agy.agencyFolderName;
                    }
                  });
                });
              } catch (err) { }
              agy.registeredUsers.forEach(function (agyCode, domIndex) {
                if (domIndex == 0) {
                  var huburl = HubServiceUrls.hubConnection.cmsUrl;
                  var portno = HubServiceUrls.hubConnection.ipAddress;
                  var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                  var homePageUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/About Us/About Us/About Us.ftl';
                 
                  axios.get(homePageUrl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                  }).then(function (response) {
                    console.log('responsee', response);
                    self.content = response.data;
                    if (self.content != undefined && self.content != null) {
                      var mainData = pluck('Main_Area', self.content.area_List);
                      self.BannerSection = getAllMapData(mainData[0].component);
    
                      var mainData = pluck('About_Us_Section', self.content.area_List);
                      self.AboutUsSection = getAllMapData(mainData[0].component);
                    }
                  }).catch(function (error) {
                    console.log('Error');
                   
                  });
                }
      
              });
      
            });
            
          },
    },
    mounted: function () {
        this.getPagecontent();
    },
       
});