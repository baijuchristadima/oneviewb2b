var newsdetails = new Vue({
    el: '#newsdetail',
    name: 'newsdetail',
    data: {
        NewsDetails: {},
        PageDetails:{}
    },
    methods: {
        getPagecontent: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) {}

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        packageurl = getQueryStringValue('page');
                        if (packageurl != "") {
                            packageurl = packageurl.split('-').join(' ');
                            packageurl = packageurl.split('_')[0];
                            self.pageURLLink = packageurl;
                            var Url = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/' + packageurl + '.ftl';
                            var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/News Details/News Details/News Details.ftl';
                            axios.get(Url, {
                                headers: {
                                    'content-type': 'text/html',
                                    'Accept': 'text/html',
                                    'Accept-Language': langauage
                                }
                            }).then(function (response) {
                                self.content = response.data;
                                if (self.content != undefined && self.content != null) {
                                    var news = pluck('News_Details', self.content.area_List);
                                    self.NewsDetails = getAllMapData(news[0].component);

                                }
                            }).catch(function (error) {});
                            axios.get(cmsPage, {
                                headers: {
                                    'content-type': 'text/html',
                                    'Accept': 'text/html',
                                    'Accept-Language': langauage
                                }
                            }).then(function (response) {
                                self.content = response.data;
                                if (self.content != undefined && self.content != null) {
                                    var data = pluck('Page_Details', self.content.area_List);
                                    self.PageDetails = getAllMapData(data[0].component);

                                }
                            }).catch(function (error) {});
                        }
                    }

                });

            });

        },
        dateFormatter: function (utc) {
            return (moment(utc).utcOffset("+05:30").format("DD MMMM YYYY"));
        },
    },
    mounted: function () {
        this.getPagecontent();
    },
});