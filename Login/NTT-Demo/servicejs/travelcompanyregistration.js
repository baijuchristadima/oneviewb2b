var companyregistration = new Vue({
    el: '#companyregistration',
    name: 'companyregistration',
    data: {
        commonStore: vueCommonStore.state,
        agencyCode: '',
        BannerSection: {},
        formSection: {},

        // form
        userName: '',
        password: '',
        cnfmPassword: '',
        companyName: '',
        companyRegNo: '',
        IATAStatus: 'Approved',
        IATANumber: '',
        nature: '',
        country: '',
        city: '',
        pincode: '',
        address: '',
        website: '',
        logo: '',
        firstName: '',
        lastName: '',
        designation: '',
        phoneCode: '',
        phone: '',
        email: '',
        mobile: '',
        mobileCode: '',
        faxCode: '',
        fax: '',
        currency: '',
        timeZone: '',
        accountsName: '',
        accountsEmail: '',
        accountsCntNo: '',
        reservationName: '',
        reservationEmail: '',
        reservationCntNo: '',
        managementName: '',
        managementEmail: '',
        managementCntNo: '',

        isLoading: false,
        fullPage: true,
        agree: '',
        file2: '',
        fileLink: '',
        currencyList: {
            "AED": "United Arab Emirates Dirham",
            "AFN": "Afghan Afghani",
            "ALL": "Albanian Lek",
            "AMD": "Armenian Dram",
            "ANG": "Netherlands Antillean Guilder",
            "AOA": "Angolan Kwanza",
            "ARS": "Argentine Peso",
            "AUD": "Australian Dollar",
            "AWG": "Aruban Florin",
            "AZN": "Azerbaijani Manat",
            "BAM": "Bosnia-Herzegovina Convertible Mark",
            "BBD": "Barbadian Dollar",
            "BDT": "Bangladeshi Taka",
            "BGN": "Bulgarian Lev",
            "BHD": "Bahraini Dinar",
            "BIF": "Burundian Franc",
            "BMD": "Bermudan Dollar",
            "BND": "Brunei Dollar",
            "BOB": "Bolivian Boliviano",
            "BRL": "Brazilian Real",
            "BSD": "Bahamian Dollar",
            "BTC": "Bitcoin",
            "BTN": "Bhutanese Ngultrum",
            "BWP": "Botswanan Pula",
            "BYN": "Belarusian Ruble",
            "BZD": "Belize Dollar",
            "CAD": "Canadian Dollar",
            "CDF": "Congolese Franc",
            "CHF": "Swiss Franc",
            "CLF": "Chilean Unit of Account (UF)",
            "CLP": "Chilean Peso",
            "CNH": "Chinese Yuan (Offshore)",
            "CNY": "Chinese Yuan",
            "COP": "Colombian Peso",
            "CRC": "Costa Rican Colón",
            "CUC": "Cuban Convertible Peso",
            "CUP": "Cuban Peso",
            "CVE": "Cape Verdean Escudo",
            "CZK": "Czech Republic Koruna",
            "DJF": "Djiboutian Franc",
            "DKK": "Danish Krone",
            "DOP": "Dominican Peso",
            "DZD": "Algerian Dinar",
            "EGP": "Egyptian Pound",
            "ERN": "Eritrean Nakfa",
            "ETB": "Ethiopian Birr",
            "EUR": "Euro",
            "FJD": "Fijian Dollar",
            "FKP": "Falkland Islands Pound",
            "GBP": "British Pound Sterling",
            "GEL": "Georgian Lari",
            "GGP": "Guernsey Pound",
            "GHS": "Ghanaian Cedi",
            "GIP": "Gibraltar Pound",
            "GMD": "Gambian Dalasi",
            "GNF": "Guinean Franc",
            "GTQ": "Guatemalan Quetzal",
            "GYD": "Guyanaese Dollar",
            "HKD": "Hong Kong Dollar",
            "HNL": "Honduran Lempira",
            "HRK": "Croatian Kuna",
            "HTG": "Haitian Gourde",
            "HUF": "Hungarian Forint",
            "IDR": "Indonesian Rupiah",
            "ILS": "Israeli New Sheqel",
            "IMP": "Manx pound",
            "INR": "Indian Rupee",
            "IQD": "Iraqi Dinar",
            "IRR": "Iranian Rial",
            "ISK": "Icelandic Króna",
            "JEP": "Jersey Pound",
            "JMD": "Jamaican Dollar",
            "JOD": "Jordanian Dinar",
            "JPY": "Japanese Yen",
            "KES": "Kenyan Shilling",
            "KGS": "Kyrgystani Som",
            "KHR": "Cambodian Riel",
            "KMF": "Comorian Franc",
            "KPW": "North Korean Won",
            "KRW": "South Korean Won",
            "KWD": "Kuwaiti Dinar",
            "KYD": "Cayman Islands Dollar",
            "KZT": "Kazakhstani Tenge",
            "LAK": "Laotian Kip",
            "LBP": "Lebanese Pound",
            "LKR": "Sri Lankan Rupee",
            "LRD": "Liberian Dollar",
            "LSL": "Lesotho Loti",
            "LYD": "Libyan Dinar",
            "MAD": "Moroccan Dirham",
            "MDL": "Moldovan Leu",
            "MGA": "Malagasy Ariary",
            "MKD": "Macedonian Denar",
            "MMK": "Myanma Kyat",
            "MNT": "Mongolian Tugrik",
            "MOP": "Macanese Pataca",
            "MRO": "Mauritanian Ouguiya (pre-2018)",
            "MRU": "Mauritanian Ouguiya",
            "MUR": "Mauritian Rupee",
            "MVR": "Maldivian Rufiyaa",
            "MWK": "Malawian Kwacha",
            "MXN": "Mexican Peso",
            "MYR": "Malaysian Ringgit",
            "MZN": "Mozambican Metical",
            "NAD": "Namibian Dollar",
            "NGN": "Nigerian Naira",
            "NIO": "Nicaraguan Córdoba",
            "NOK": "Norwegian Krone",
            "NPR": "Nepalese Rupee",
            "NZD": "New Zealand Dollar",
            "OMR": "Omani Rial",
            "PAB": "Panamanian Balboa",
            "PEN": "Peruvian Nuevo Sol",
            "PGK": "Papua New Guinean Kina",
            "PHP": "Philippine Peso",
            "PKR": "Pakistani Rupee",
            "PLN": "Polish Zloty",
            "PYG": "Paraguayan Guarani",
            "QAR": "Qatari Rial",
            "RON": "Romanian Leu",
            "RSD": "Serbian Dinar",
            "RUB": "Russian Ruble",
            "RWF": "Rwandan Franc",
            "SAR": "Saudi Riyal",
            "SBD": "Solomon Islands Dollar",
            "SCR": "Seychellois Rupee",
            "SDG": "Sudanese Pound",
            "SEK": "Swedish Krona",
            "SGD": "Singapore Dollar",
            "SHP": "Saint Helena Pound",
            "SLL": "Sierra Leonean Leone",
            "SOS": "Somali Shilling",
            "SRD": "Surinamese Dollar",
            "SSP": "South Sudanese Pound",
            "STD": "São Tomé and Príncipe Dobra (pre-2018)",
            "STN": "São Tomé and Príncipe Dobra",
            "SVC": "Salvadoran Colón",
            "SYP": "Syrian Pound",
            "SZL": "Swazi Lilangeni",
            "THB": "Thai Baht",
            "TJS": "Tajikistani Somoni",
            "TMT": "Turkmenistani Manat",
            "TND": "Tunisian Dinar",
            "TOP": "Tongan Pa'anga",
            "TRY": "Turkish Lira",
            "TTD": "Trinidad and Tobago Dollar",
            "TWD": "New Taiwan Dollar",
            "TZS": "Tanzanian Shilling",
            "UAH": "Ukrainian Hryvnia",
            "UGX": "Ugandan Shilling",
            "USD": "United States Dollar",
            "UYU": "Uruguayan Peso",
            "UZS": "Uzbekistan Som",
            "VEF": "Venezuelan Bolívar Fuerte (Old)",
            "VES": "Venezuelan Bolívar Soberano",
            "VND": "Vietnamese Dong",
            "VUV": "Vanuatu Vatu",
            "WST": "Samoan Tala",
            "XAF": "CFA Franc BEAC",
            "XAG": "Silver Ounce",
            "XAU": "Gold Ounce",
            "XCD": "East Caribbean Dollar",
            "XDR": "Special Drawing Rights",
            "XOF": "CFA Franc BCEAO",
            "XPD": "Palladium Ounce",
            "XPF": "CFP Franc",
            "XPT": "Platinum Ounce",
            "YER": "Yemeni Rial",
            "ZAR": "South African Rand",
            "ZMW": "Zambian Kwacha",
            "ZWL": "Zimbabwean Dollar"
        },
        countryList: [],
    },
    methods: {

        getPageData: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) {}

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Company Registration/Company Registration/Company Registration.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(cmsPage, {
                            headers: {
                                'content-type': 'text/html',
                                'Accept': 'text/html',
                                'Accept-Language': langauage
                            }
                        }).then(function (response) {
                            self.data = response.data;
                            if (response.data.area_List) {
                                var banner = pluck("Banner_Section", self.data.area_List);
                                self.BannerSection = getAllMapData(banner[0].component);
                                var form = pluck("Form_Section", self.data.area_List);
                                self.formSection = getAllMapData(form[0].component);
                            }
                        }).catch(function (error) {
                            console.log('Error');
                        });
                    }

                });


            });

        },
        sendTravel: async function () {
            var self = this;
            if (!this.userName) {
                alertify.alert('Alert', 'Username required.').set('closable', false);
                return false;
            }
            if (!this.password) {
                alertify.alert('Alert', 'Password required.').set('closable', false);
                return false;
            }
            if (!this.cnfmPassword) {
                alertify.alert('Alert', 'Confirm password required').set('closable', false);
                return false;
            }
            if (this.password != this.cnfmPassword) {
                alertify.alert('Alert', 'Passwords do not match').set('closable', false);
                return false;
            }
            if (!this.companyName) {
                alertify.alert('Alert', 'Company name required.').set('closable', false);
                return false;
            }
            if (!this.companyRegNo) {
                alertify.alert('Alert', 'Company RegNo required.').set('closable', false);
                return false;
            }
            if (!this.country) {
                alertify.alert('Alert', 'Country required.').set('closable', false);
                return false;
            }
            if (!this.city) {
                alertify.alert('Alert', 'City required.').set('closable', false);
                return false;
            }
            if (!this.pincode) {
                alertify.alert('Alert', 'Pincode required.').set('closable', false);
                return false;
            }
            if (!this.address) {
                alertify.alert('Alert', 'Address required.').set('closable', false);
                return false;
            }
            if (!this.firstName) {
                alertify.alert('Alert', 'First name required.').set('closable', false);
                return false;
            }
            if (!this.lastName) {
                alertify.alert('Alert', 'Last name required.').set('closable', false);
                return false;
            }
            if (!this.email) {
                alertify.alert('Alert', 'Email Id required.').set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.email.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                return false;
            }
            if (!this.phone) {
                alertify.alert('Alert', 'Phone Number required.').set('closable', false);
                return false;
            }
            if (!this.mobile) {
                alertify.alert('Alert', 'Mobile number required.').set('closable', false);
                return false;
            }
            if (this.mobile.length < 8) {
                alertify.alert('Alert', 'Enter Mobile Contact number.').set('closable', false);
                return false;
            }
            if (!this.currency) {
                alertify.alert('Alert', 'Currency required.').set('closable', false);
                return false;
            }
            if (!this.timeZone) {
                alertify.alert('Alert', 'Time zone required.').set('closable', false);
                return false;
            }
            if (!this.accountsName) {
                alertify.alert('Alert', 'Accounts name required.').set('closable', false);
                return false;
            }
            if (!this.accountsEmail) {
                alertify.alert('Alert', 'Accounts email required.').set('closable', false);
                return false;
            }
            var matchArray = this.accountsEmail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                return false;
            }
            if (!this.accountsCntNo) {
                alertify.alert('Alert', 'Accounts contact required.').set('closable', false);
                return false;
            }
            // if (!this.reservationName) {
            //     alertify.alert('Alert', 'Reservation contact required.').set('closable', false);
            //     return false;
            // }
            // if (!this.reservationEmail) {
            //     alertify.alert('Alert', 'Reservation contact required.').set('closable', false);
            //     return false;
            // }
            if (this.reservationEmail) {
                var matchArray = self.reservationEmail.match(emailPat);
                if (matchArray == null) {
                    alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                    return false;
                }
            }
            // if (!this.reservationCntNo) {
            //     alertify.alert('Alert', 'Reservation contact required.').set('closable', false);
            //     return false;
            // }
            // if (!this.managementName) {
            //     alertify.alert('Alert', 'Management contact required.').set('closable', false);
            //     return false;
            // }
            // if (!this.managementEmail) {
            //     alertify.alert('Alert', 'Management contact required.').set('closable', false);
            //     return false;
            // }
            if (this.managementEmail) {
                var matchArray = self.managementEmail.match(emailPat);
                if (matchArray == null) {
                    alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                    return false;
                }
            }
            // if (!this.managementCntNo) {
            //     alertify.alert('Alert', 'Management contact required.').set('closable', false);
            //     return false;
            // }
            if (!this.agree) {
                alertify.alert('Alert', 'Please agree the terms & conditions').set('closable', false);
                return false;
            } else {
                self.isLoading = false;
                var frommail = this.notificationEmail || this.commonStore.fallBackEmail;
                var logourl = window.location.origin + "/Login/NTT-Demo/website-informations/logo/Nirvana.png"
                var custmail = {
                    type: "UserAddedRequest",
                    fromEmail: frommail,
                    toEmails: Array.isArray(this.email) ? this.email : [this.email],
                    logo: logourl || "",
                    agencyName: "Nirvana Travel and Tourism",
                    agencyAddress: "P.O. Box 41818, Breakwater, Corniche Road,Abu Dhabi Marina Area",
                    personName: this.firstName || "",
                    primaryColor: "#55aadf",
                    secondaryColor: "#f4c927"
                };
                let agencyCode = this.agencyCode;
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                let insertCompanyData = {
                    type: "Company Registration",
                    keyword1: this.companyName,
                    keyword2: this.companyRegNo,
                    keyword6: this.userName,
                    keyword7: this.password,
                    text9: this.IATAStatus,
                    keyword3: this.IATANumber,
                    keyword8: this.nature,
                    keyword9: this.country.country,
                    keyword10: this.city,
                    keyword12: this.pincode,
                    text1: this.address,
                    text2: this.website,
                    text3: this.fileLink,
                    keyword11: this.firstName,
                    keyword13: this.lastName,
                    text11: this.designation,
                    keyword16: this.phoneCode + this.phone,
                    keyword14: this.email,
                    keyword17: this.mobileCode + this.mobile,
                    keyword15: this.faxCode + this.fax,
                    text4: this.currency,
                    text5: this.timeZone,
                    keyword18: this.accountsName,
                    keyword19: this.accountsEmail,
                    keyword20: this.accountsCntNo,
                    text10: this.reservationName,
                    text12: this.reservationEmail,
                    text13: this.reservationCntNo,
                    text7: this.managementName,
                    text8: this.managementEmail,
                    text6: this.managementCntNo,
                    date1: requestedDate,
                    nodeCode: agencyCode
                };
                let responseObject = await this.cmsRequestData("POST", "cms/data", insertCompanyData, null);
                try {
                    let insertID = Number(responseObject);
                    var self = this;
                    var emailApi = self.commonStore.hubUrls.emailServices.emailApi;
                    sendMailService(emailApi, custmail);
                    self.cancelBtn();
                    self.isLoading = false;
                    alertify.alert('Success', 'Thank you for registration.We shall get back to you.');
                } catch (e) {
                    self.isLoading = false;
                }
            }
        },
        iataStatus: function (value) {
            var self = this;
            self.IATAStatus = value;
            this.$forceUpdate()
        },
        cancelBtn: function () {
            this.companyName = '';
            this.companyName = '',
                this.companyRegNo = '',
                this.userName = '',
                this.password = '',
                this.IATAStatus = '',
                this.IATANumber = '',
                this.nature = '',
                this.country = '',
                this.city = '',
                this.pincode = '',
                this.address = '',
                this.website = '',
                this.logo = '',
                this.firstName = '',
                this.lastName = '',
                this.designation = '',
                this.phone = '',
                this.email = '',
                this.mobile = '',
                this.fax = '',
                this.currency = '',
                this.timeZone = '',
                this.accountsName = '',
                this.accountsEmail = '',
                this.accountsCntNo = '',
                this.reservationName = '',
                this.reservationEmail = '',
                this.reservationCntNo = '',
                this.managementName = '',
                this.managementEmail = '',
                this.managementCntNo = ''
        },
        handleFile() {
            var self = this;
            self.isLoading = true;
            file_temp = self.$refs.file2.files[0];
            var ext = file_temp.name.split('.')[1];
            if (ext == "jpg" || ext == "png" || ext == "PNG" || ext == "JPEG" || ext == "jpeg") {
                self.file2 = self.$refs.file2.files[0];
                if (self.file2) {
                    self.submitFile()
                }
            } else {
                alertify.alert("Warning", "Please select valid File");
                self.file2 = null;
                self.isLoading = false;
            }
        },
        submitFile() {
            var self = this;
            if (self.file2 != null && self.file2 != undefined && !jQuery.isEmptyObject(self.file2)) {
                $.getJSON('fileupload.json', function (json) {
                    let Password = json.Password;
                    var LoggedUser = json.userName;
                    var encodedString = btoa(LoggedUser + ":" + Password);
                    var formData = new FormData();
                    formData.append('file', self.file2);
                    var url = "https://fileupload.oneviewitsolutions.com:9443/upload/" + "logo"
                    // You should have a server side REST API
                    axios.post(url,
                            formData, {
                                headers: {
                                    'Accept': 'application/json',
                                    'Content-Type': 'multipart/form-data',
                                    'Authorization': 'Basic ' + encodedString
                                }
                            }
                        ).then(function (repo) {
                            console.log('SUCCESS!!', repo.data.message);
                            self.fileLink = repo.data.message;
                            if (self.fileLink) {
                                var files = self.fileLink.split('/file/');
                                self.file = files[1];
                            }
                            self.isLoading = false;
                            alertify.alert("Success", "Successfully uploaded");
                            // self.file2 = null;

                        })
                        .catch(function () {
                            console.log('FAILURE!!');
                            self.fileLink = null;
                            self.isLoading = false;
                        });
                });
            } else {
                alertify.alert("Warning", "please select the file")
            }
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            try {
                var huburl = HubServiceUrls.hubConnection.cmsUrl;
                var portno = HubServiceUrls.hubConnection.ipAddress;
                const url = huburl + portno + "/" + urlParam;
                if (data != null) {
                    data = JSON.stringify(data);
                }
                const response = await fetch(url, {
                    method: callMethod, // *GET, POST, PUT, DELETE, etc.
                    credentials: "same-origin", // include, *same-origin, omit
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: data, // body data type must match "Content-Type" header
                });
                try {
                    const myJson = await response.json();
                    return myJson;
                } catch (error) {
                    return object;
                }
            } catch (error) {
                this.isLoading = false;
            }

        },
        getCountries: function () {
            var self = this;
            axios.get('assets/countries.min.json')
                .then(function (response) {
                    var countries = response.data;
                    Object.entries(countries).map(item => {
                        if (typeof item !== "undefined") {
                            self.countryList.push({
                                'country': item[0],
                                'city': item[1]
                            });
                        }
                    })
                });



        },
    },
    mounted: function () {
        this.getPageData();
        this.getCountries();
    },

});