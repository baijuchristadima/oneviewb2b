var gsaregistration = new Vue({
    el: '#gsaregistration',
    name: 'gsaregistration',
    data: {
        commonStore: vueCommonStore.state,
        agencyCode: '',
        BannerSection: {},
        FormSection: {},

        // form
        companyName: '',
        cntNumber: '',
        address: '',
        email: '',
        message: '',

        isLoading: false,
        fullPage: true
    },
    methods: {

          getPageData: function () {
              var self = this;
              axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                  var agencyFolderName = '';
                  var agy;
                  try {
                      response.data.forEach(function (agent, agentIndex) {
                          agent.domain.forEach(function (dom, domIndex) {
                              if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                  agy = agent;
                                  agencyFolderName = agy.agencyFolderName;
                              }
                          });
                      });
                  } catch (err) { }

                  agy.registeredUsers.forEach(function (agyCode, domIndex) {
                      if (domIndex == 0) {
                          var huburl = HubServiceUrls.hubConnection.cmsUrl;
                          var portno = HubServiceUrls.hubConnection.ipAddress;
                          var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                          var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/GSA Registration/GSA Registration/GSA Registration.ftl';
                          var agencyCode = agyCode;
                          self.agencyCode = agencyCode;
                          axios.get(cmsPage, {
                              headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                          }).then(function (response) {
                              self.data = response.data;
                              if (response.data.area_List) {
                                var banner = pluck("Banner_Area", self.data.area_List);
                                self.BannerSection = getAllMapData(banner[0].component);
                                var form = pluck("Form_Section", self.data.area_List);
                                self.FormSection = getAllMapData(form[0].component);
                              }
                          }).catch(function (error) {
                              console.log('Error');
                          });
                      }

                  });


              });

          },
        sendGsa: async function () {
            var self = this;
            if (!this.companyName) {
                alertify.alert('Alert', 'Company name required.').set('closable', false);

                return false;
            }
            if (!this.cntNumber) {
                alertify.alert('Alert', 'Contact number required.').set('closable', false);
                return false;
            }
            if (this.cntNumber.length < 8) {
                alertify.alert('Alert', 'Enter valid Contact number.').set('closable', false);
                return false;
            }
            if (!this.email) {
                alertify.alert('Alert', 'Email Id required.').set('closable', false);
                return false;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.email.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
                return false;
            } 
            if (!this.address) {
                alertify.alert('Alert', 'Address required.').set('closable', false);
                return false;
            }else {
                this.isLoading = true;
                var frommail = this.notificationEmail || this.commonStore.fallBackEmail;
                var logourl = window.location.origin + "/Login/NTT-Demo/website-informations/logo/Nirvana.png"
                var custmail = {
                    type: "UserAddedRequest",
                    fromEmail: frommail,
                    toEmails: Array.isArray(this.email) ? this.email : [this.email],
                    logo: logourl || "",
                    agencyName: "Nirvana Travel and Tourism",
                    agencyAddress: "P.O. Box 41818, Breakwater, Corniche Road,Abu Dhabi Marina Area",
                    personName: this.companyName || "",
                    primaryColor: "#55aadf",
                    secondaryColor: "#f4c927"
                };
                let agencyCode = this.agencyCode;
                let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                let insertGSAData = {
                    type: "GSA Registration",
                    keyword1: this.companyName,
                    keyword2: this.cntNumber,
                    keyword3: this.email,
                    text1: this.address,
                    text2: this.message,
                    date1: requestedDate,
                    nodeCode: agencyCode
                };
                let responseObject = await this.cmsRequestData("POST", "cms/data", insertGSAData, null);
                try {
                    let insertID = Number(responseObject);
                    var self = this;
                    var emailApi = self.commonStore.hubUrls.emailServices.emailApi;
                    sendMailService(emailApi, custmail);
                    self.cancelBtn();
                    this.isLoading = false;
                    alertify.alert('Success', 'Thank you for registration.We shall get back to you.');
                } catch (e) {
                    this.isLoading = false;
                }
            }
        },
        cancelBtn:function(){
            this.companyName = '';
            this.cntNumber = '';
            this.email = '';
            this.address = '';
            this.message = '';
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            try {
                var huburl = HubServiceUrls.hubConnection.cmsUrl;
                var portno = HubServiceUrls.hubConnection.ipAddress;
                const url = huburl + portno + "/" + urlParam;
                if (data != null) {
                    data = JSON.stringify(data);
                }
                const response = await fetch(url, {
                    method: callMethod, // *GET, POST, PUT, DELETE, etc.
                    credentials: "same-origin", // include, *same-origin, omit
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: data, // body data type must match "Content-Type" header
                });
                try {
                    const myJson = await response.json();
                    return myJson;
                } catch (error) {
                    return object;
                }
            } catch (error) {
                this.isLoading = false;
            }

        }

    },
    mounted: function () {
        this.getPageData();
    },

});