var offers = new Vue({
    el: '#offer',
    name: 'offer',
    data: {
        offerList: [],
        PageDetails:{}
    },
    methods: {
        getPagecontent: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) {}

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        var offerUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Master Table/Offers/Offers/Offers.ftl';
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Offer/Offer/Offer.ftl';
                        axios.get(cmsPage, {
                            headers: {
                              'content-type': 'text/html',
                              'Accept': 'text/html',
                              'Accept-Language': langauage
                            }
                          }).then(function (response) {
                            self.content = response.data;
                            if (self.content != undefined && self.content != null) {
                              var data = pluck('PageContent', self.content.area_List);
                              self.PageDetails = getAllMapData(data[0].component);


                            }
                          }).catch(function (error) {
                            console.log('Error');
                          });
                        
                        axios.get(offerUrl, {
                            headers: {
                                'content-type': 'text/html',
                                'Accept': 'text/html',
                                'Accept-Language': langauage
                            }
                        }).then(function (response) {
                            var data = response.data;
                            let offerListTemp = [];
                            if (data != undefined && data.Values != undefined) {
                                offerListTemp = data.Values.filter(function (el) {
                                    return el.Is_Active == true;
                                });
                            }
                            self.offerList = offerListTemp;
                        }).catch(function (error) {});
                    }

                });

            });

        },
        dateFormatter: function (utc) {
            return (moment(utc).utcOffset("+05:30").format("DD MMMM YYYY"));
        },
    },
    mounted: function () {
        this.getPagecontent();
    },
});