var HeaderComponent = Vue.component('headeritem', {
  template: `
    <div>
    <div class="tp"></div>
    <div id="fixed-top" class="top">
      <div class="container-fluid">
        <div class="row">
          <div class="hdr-left">
            <a class="cd-primary-nav-trigger" href="#0">
              <span class="cd-menu-text">Menu</span><span class="cd-menu-icon"></span>
            </a>
            <a class="logo" href="index.html"><img :src="mainData.Logo_183_x_64_px" alt=""></a>
          </div>
          <div class="hdr-right">
            <!--<ul class="lang">
              <li><a @click="changeLanguage('en')" v-if="showEnglish">En</a></li>
              <li><a @click="changeLanguage('ar')" v-if="showArabic">Ar</a></li>
            </ul>-->
            <ul class="social">
              <li v-for="item in mainData.Social_Media_List"><a :href="item.URL" target="_blank"><i class="fa" :class="item.Icon" aria-hidden="true"></i></a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  <nav>
    <div class=" cd-primary-nav">
      <ul class="menu">
        <li class="cd-label">{{menuData.Services_Label}}</li>
        <li v-for="item in serviceList"><a href="#" @click="getmoreinfo(item.More_details_page_link)">{{item.Title}}</a></li>
      </ul>
      <ul class="menu">
        <li class="cd-label">{{menuData.About_Platform_Title}}</li>
        <li><a href="contact-us.html">{{menuData.Contact_Us_Label}}</a></li>
        <li><a href="the-platform.html">{{menuData.The_Platform_Label}}</a></li>
        <li><a href="about-us.html">{{menuData.About_Us_Label}}</a></li>
        <li><a href="#">{{menuData.Online_Meeting_Label}}</a></li>
        <li><a href="news.html">{{menuData.News_Label}}</a></li>
        <li><a href="offers.html">{{menuData.Offers_Label}}</a></li>
      </ul>
      <ul class="menu menu2">
        <li class="cd-label">{{menuData.Registration_Title}}</li>
        <div class="travel">
          <h3>{{menuData.Travel_Company_Title}}</h3>
          <p>{{menuData.Travel_Company_Description}}</p>
          <a href="travel-company-registration.html" class="line-btn">{{menuData.Register_Button_Label}}</a>
        </div>
        <div class="hotel">
          <h3>{{menuData.Exclusive_GSA_Title}}</h3>
          <p>{{menuData.Exclusive_GSA_Description}}
          </p>
          <a href="gsa-registration.html" class="line-btn">{{menuData.Register_Button_Label}}</a>
        </div>
      </ul>
    </div>
  </nav>
  </div>`,
  data() {
    return {
      agencyCode: '',
      mainData: {},
      menuData: {},
      serviceList: [],
      activeClass: '',
      showEnglish: false,
      showArabic: true,
    }
  },
  created() {
    var self = this;
    var pathArray = window.location.pathname.split('/');
    self.activeClass = '/' + pathArray[pathArray.length - 1];

  },
  methods: {
    getPageheader: function () {
      var self = this;
      axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
        var agencyFolderName = '';
        var agy;
        try {
          response.data.forEach(function (agent, agentIndex) {
            agent.domain.forEach(function (dom, domIndex) {
              if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                agy = agent;
                agencyFolderName = agy.agencyFolderName;
              }
            });
          });
        } catch (err) {}
        agy.registeredUsers.forEach(function (agyCode, domIndex) {
          if (domIndex == 0) {
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            var serviceUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Master Table/Service List/Service List/Service List.ftl';
            var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Header/Header/Header.ftl';
            var agencyCode = agyCode;
            self.agencyCode = agencyCode;
            axios.get(cmsPage, {
              headers: {
                'content-type': 'text/html',
                'Accept': 'text/html',
                'Accept-Language': langauage
              }
            }).then(function (response) {
              self.data = response.data;
              var main = pluck('Main_Section', self.data.area_List);
              self.mainData = getAllMapData(main[0].component);
              var menu = pluck('Side_Bar_Section', self.data.area_List);
              self.menuData = getAllMapData(menu[0].component);
            }).catch(function (error) {
              console.log('Error');
            });

            axios.get(serviceUrl, {
              headers: {
                'content-type': 'text/html',
                'Accept': 'text/html',
                'Accept-Language': langauage
              }
            }).then(function (response) {
              var data = response.data;
              let serviceListTemp = [];
              if (data != undefined && data.Values != undefined) {
                serviceListTemp = data.Values;
              }
              self.serviceList = serviceListTemp;
            }).catch(function (error) {});
          }

        });

      });

    },
    getmoreinfo(url) {
      if (url != null) {
        if (url != "") {
          url = url.split("/Template/")[1];
          url = url.split(' ').join('-');
          url = url.split('.').slice(0, -1).join('.');
          url = "servicedetail.html?page=" + url + "&from=pkg";
          window.location.href = url;
        } else {
          url = "#";
        }
      } else {
        url = "#";
      }
      return url;
    },
    // changeLanguage: function (lang) {
    //   var pagePath = window.location.pathname.toString().toLowerCase();
    //   if (lang == "en") {
    //     this.showEnglish = false;
    //     this.showArabic = true,
    //     localStorage.Languagecode = "en";
    //     this.getPageheader();
    //   } else {
    //     this.showEnglish = true;
    //     this.showArabic = false,
    //     localStorage.Languagecode = "ar";
    //     this.getPageheader();
    //   }
    //   if (footerinstance) {
    //     footerinstance.key = Math.random();

    //     switch (pagePath) {
    //       case "/login/ntt-demo/":
    //         maininstance.getPagecontent();
    //         maininstance.key = Math.random();
    //         break;
    //       case "/login/ntt-demo/index.html":
    //         maininstance.getPagecontent();
    //         maininstance.key = Math.random();
    //         break;
    //       case "/login/ntt-demo/contact-us.html":
    //         contact.getPageData();
    //         contact.key = Math.random();
    //         break;
    //       case "/login/ntt-demo/about-us.html":
    //         aboutUs.getPagecontent();
    //         aboutUs.key = Math.random();
    //         break;
    //       case "/login/ntt-demo/news.html":
    //         news.getPagecontent();
    //         news.key = Math.random();
    //         break;
    //       case "/login/ntt-demo/news-detail.html":
    //         newsdetails.getPagecontent();
    //         newsdetails.key = Math.random();
    //         break;
    //       case "/login/ntt-demo/offers.html":
    //         offers.getPagecontent();
    //         offers.key = Math.random();
    //         break;
    //       case "/login/ntt-demo/servicedetail.html":
    //         service.getPagecontent();
    //         service.key = Math.random();
    //         break;
    //       case "/login/ntt-demo/the-platform.html":
    //         platform.getPagecontent();
    //         platform.key = Math.random();
    //         break;
    //       case "/login/ntt-demo/gsa-registration.html":
    //         gsaregistration.getPageData();
    //         gsaregistration.key = Math.random();
    //         break;
    //       case "/login/ntt-demo/travel-company-registration.html":
    //         companyregistration.getPageData();
    //         companyregistration.key = Math.random();
    //         break;
    //       default:
    //         window.location.reload();
    //         break;
    //     }
    //   }
    // }
  },
  mounted: function () {

    this.getPageheader();
  },
})
var headerinstance = new Vue({
  el: 'header',
  name: 'headerArea',
  data() {
    return {
      key: '',
      content: null,
      getdata: true
    }

  },

});


Vue.component('register', {
  template: `
  <section class="register-sec clearfix">
    <article class="detail fadeInLeft animated" data-wow-delay="0ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInLeft;">
      <div>
        <h3>{{menuData.Travel_Company_Title}}</h3>
        <p>{{menuData.Travel_Company_Description}}</p>
        <a href="travel-company-registration.html" class="line-btn">{{menuData.Register_Button_Label}}</a></div>
    </article>
    <article class="detail fadeInRight animated" data-wow-delay="0ms" data-wow-duration="1500ms" style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInRight;">
      <div>
        <h3>{{menuData.Exclusive_GSA_Title}}</h3>
        <p>{{menuData.Exclusive_GSA_Description}}</p>
        <a href="gsa-registration.html" class="line-btn">{{menuData.Register_Button_Label}}</a></div>
    </article>
  </section>
  `,
  data() {
    return {
      agencyCode: '',
      menuData: {}

    }
  },
  methods: {
    getRegisterData: function () {
      var self = this;
      axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
        var agencyFolderName = '';
        var agy;
        try {
          response.data.forEach(function (agent, agentIndex) {
            agent.domain.forEach(function (dom, domIndex) {
              if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                agy = agent;
                agencyFolderName = agy.agencyFolderName;
              }
            });
          });
        } catch (err) {}

        agy.registeredUsers.forEach(function (agyCode, domIndex) {
          if (domIndex == 0) {
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            var header = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Header/Header/Header.ftl';
            var agencyCode = agyCode;
            self.agencyCode = agencyCode;
            axios.get(header, {
              headers: {
                'content-type': 'text/html',
                'Accept': 'text/html',
                'Accept-Language': langauage
              }
            }).then(function (response) {
              self.content = response.data;
              if (self.content != undefined && self.content != null) {
                var menu = pluck('Side_Bar_Section', self.content.area_List);
                self.menuData = getAllMapData(menu[0].component);

              }

            }).catch(function (error) {
              console.log('Error');
            });
          }

        });

      });

    },

  },
  mounted: function () {
    this.getRegisterData();

  }

});
var partnerinstance = new Vue({
  el: 'register',
  name: 'register',
  data() {
    return {
      key: '',
      content: null,
      getdata: true
    }

  },

});


Vue.component('footeritem', {
  template: `
    <div>
    <div class="container-fluid">
      <div class="inner">
        <div class="row">
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 fadeInLeft animated" data-wow-delay="0ms"
            data-wow-duration="1500ms"
            style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInLeft;">
            <div class="link ftr-link">
              <ul>
                <li v-for="item in serviceList"><a href="#" @click="getmoreinfo(item.More_details_page_link)"><i class="fa fa-angle-double-right" aria-hidden="true"></i>{{item.Title}}</a></li>
                <li><a href="travel-company-registration.html"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Travel Company
                    Registration</a></li>
                <li><a href="gsa-registration.html"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Exclusive GSA
                    Registration</a></li>
              </ul>
            </div>
          </div>
          <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12 fadeInLeft animated" data-wow-delay="0ms"
            data-wow-duration="1500ms"
            style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInLeft;">
            <div class="link">
              <ul>
                <li><a href="contact-us.html"><i class="fa fa-angle-double-right" aria-hidden="true"></i>{{menuData.Contact_Us_Label}}</a></li>
                <li><a href="the-platform.html"><i class="fa fa-angle-double-right" aria-hidden="true"></i>{{menuData.The_Platform_Label}}</a></li>
                <li><a href="about-us.html"><i class="fa fa-angle-double-right" aria-hidden="true"></i>{{menuData.About_Us_Label}}</a></li>
                <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>{{menuData.Online_Meeting_Label}}</a></li>
                <li><a href="news.html"><i class="fa fa-angle-double-right" aria-hidden="true"></i>{{menuData.News_Label}}</a></li>
                <li><a href="offers.html"><i class="fa fa-angle-double-right" aria-hidden="true"></i>{{menuData.Offers_Label}}</a></li>
                <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>{{footerData.Privacy_Policy_Label}}</a></li>
                <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>{{footerData.Terms_Of_Use_Label}}</a></li>
              </ul>
            </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 fadeInRight animated" data-wow-delay="0ms"
            data-wow-duration="1500ms"
            style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInRight;">
            <div class="link">
              <ul>
                <li>
                  <h5>{{contactData.Call_Us_Label}}</h5>
                </li>
                <li><a :href="'tel:'+ contactData.Contact_Number">{{contactData.Contact_Number}}</a></li>
                <li>
                  <h5>{{contactData.Schedule_Meeting_Label}}</h5>
                </li>
                <li><a :href="contactData._Online_Meeting_Link">{{contactData._Online_Meeting_Link}}</a></li>
                <li>
                  <h5>{{contactData.Sales_Support_Label}}</h5>
                </li>
                <li><a :href="'mailto:'+ contactData.Sales_Email">{{contactData.Sales_Email}}</a></li>
                <li>
                  <h5>{{contactData.Booking_Support_Label}}</h5>
                </li>
                <li><a :href="'mailto:'+ contactData.Booking_Support_Email">{{contactData.Booking_Support_Email}}</a></li>
              </ul>
            </div>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 fadeInRight animated" data-wow-delay="0ms"
            data-wow-duration="1500ms"
            style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInRight;">
            <div class="map-responsive">
              <iframe
                :src="contactData.Map_Link"
                style="border:0" allowfullscreen="" width="600" height="450" frameborder="0"></iframe>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="ftr-btm">
      <div class="container-fluid">
        <div class="copyright">
          <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fadeInLeft animated" data-wow-delay="0ms"
              data-wow-duration="1500ms"
              style="visibility: visible; animation-duration: 1500ms; animation-delay: 0ms; animation-name: fadeInLeft;">
              <p>{{footerData.Copyright_Description}}</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>`,
  data() {
    return {
      agencyCode: '',
      footerData: {},
      menuData: {},
      serviceList: [],
      contactData: {}

    }
  },
  methods: {
    getPagefooter: function () {
      var self = this;
      axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
        var agencyFolderName = '';
        var agy;
        try {
          response.data.forEach(function (agent, agentIndex) {
            agent.domain.forEach(function (dom, domIndex) {
              if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                agy = agent;
                agencyFolderName = agy.agencyFolderName;
              }
            });
          });
        } catch (err) {}

        agy.registeredUsers.forEach(function (agyCode, domIndex) {
          if (domIndex == 0) {
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            var header = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Header/Header/Header.ftl';
            var Footer = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Footer/Footer/Footer.ftl';
            var serviceUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Master Table/Service List/Service List/Service List.ftl';
            var contactUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Contact Us/Contact Us/Contact Us.ftl';
            var agencyCode = agyCode;
            self.agencyCode = agencyCode;
            axios.get(Footer, {
              headers: {
                'content-type': 'text/html',
                'Accept': 'text/html',
                'Accept-Language': langauage
              }
            }).then(function (response) {
              self.content = response.data;
              if (self.content != undefined && self.content != null) {
                var main = pluck('Main_Section', self.content.area_List);
                self.footerData = getAllMapData(main[0].component);

              }

            }).catch(function (error) {
              console.log('Error');
              self.content = [];
            });
            axios.get(header, {
              headers: {
                'content-type': 'text/html',
                'Accept': 'text/html',
                'Accept-Language': langauage
              }
            }).then(function (response) {
              self.content = response.data;
              if (self.content != undefined && self.content != null) {
                var menu = pluck('Side_Bar_Section', self.content.area_List);
                self.menuData = getAllMapData(menu[0].component);

              }

            }).catch(function (error) {
              console.log('Error');
              self.content = [];
            });
            axios.get(serviceUrl, {
              headers: {
                'content-type': 'text/html',
                'Accept': 'text/html',
                'Accept-Language': langauage
              }
            }).then(function (response) {
              var data = response.data;
              let serviceListTemp = [];
              if (data != undefined && data.Values != undefined) {
                serviceListTemp = data.Values;
              }
              self.serviceList = serviceListTemp;
            }).catch(function (error) {});

            axios.get(contactUrl, {
              headers: {
                'content-type': 'text/html',
                'Accept': 'text/html',
                'Accept-Language': langauage
              }
            }).then(function (response) {
              self.data = response.data;
              if (response.data.area_List) {
                var mainData = pluck("Main_Area", self.data.area_List);
                self.contactData = getAllMapData(mainData[0].component);
              }
            }).catch(function (error) {
              console.log('Error');
            });
          }

        });

      });

    },
    getmoreinfo(url) {
      if (url != null) {
        if (url != "") {
          url = url.split("/Template/")[1];
          url = url.split(' ').join('-');
          url = url.split('.').slice(0, -1).join('.');
          url = "servicedetail.html?page=" + url + "&from=pkg";
          window.location.href = url;
        } else {
          url = "#";
        }
      } else {
        url = "#";
      }
      return url;
    }
  },
  mounted: function () {
    this.getPagefooter();
  },

})
var footerinstance = new Vue({
  el: 'footer',
  name: 'footerArea',
  data() {
    return {
      key: 0,
      content: null,
      getdata: true
    }

  },
});

function getQueryStringValue(key) {
  return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
}