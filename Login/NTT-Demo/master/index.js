var maininstance = new Vue({
  el: '#homepage',
  name: 'homepage',
  data: {
    agencyCode: '',
    bannerSection: {},
    loginContent: {},
    offersContent: {},
    servicesContent: {},
    newsContent: {},
    benefitsContent: {},
    registrationContent: {},
    newsList: [],
    offerList:[],
    platformSection:[]
  },
  filters: {
    contentStr: function (string) {
      string = string.replace(/<\/?[^>]+(>|$)/g, "");
      if (string.length > 10)
        return string.substr(0, 40) + '...';

      else
        return string;
    }
  },
  methods: {
    getPagecontent: function () {
      var self = this;
      // self.isLoading = true;
      axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
        var agencyFolderName = '';
        var agy;
        try {
          response.data.forEach(function (agent, agentIndex) {
            agent.domain.forEach(function (dom, domIndex) {
              if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                agy = agent;
                agencyFolderName = agy.agencyFolderName;
              }
            });
          });
        } catch (err) {}
        agy.registeredUsers.forEach(function (agyCode, domIndex) {
          if (domIndex == 0) {
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            var homePageUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Home/Home/Home.ftl';
            var home = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Master Table/Latest News List/Latest News List/Latest News List.ftl';
            var offerUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Master Table/Offers/Offers/Offers.ftl';
            var platformUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Platform Page/Platform Page/Platform Page.ftl';
            var agencyCode = agyCode;
            self.agencyCode = agencyCode;
            axios.get(homePageUrl, {
              headers: {
                'content-type': 'text/html',
                'Accept': 'text/html',
                'Accept-Language': langauage
              }
            }).then(function (response) {
              self.content = response.data;
              if (self.content != undefined && self.content != null) {
                var bannerData = pluck('Banner_Section', self.content.area_List);
                self.bannerSection = getAllMapData(bannerData[0].component);
                var loginData = pluck('Login_Form', self.content.area_List);
                self.loginContent = getAllMapData(loginData[0].component);

                var offersData = pluck('Special_Offers_Section', self.content.area_List);
                self.offersContent = getAllMapData(offersData[0].component);

                var servicesData = pluck('Our_Services_Section', self.content.area_List);
                self.servicesContent = getAllMapData(servicesData[0].component);

                var newsData = pluck('Latest_News_Section', self.content.area_List);
                self.newsContent = getAllMapData(newsData[0].component);

                var benefitsData = pluck('Platform_Benefits_Section', self.content.area_List);
                self.benefitsContent = getAllMapData(benefitsData[0].component);

                var registrationData = pluck('Registration_Section', self.content.area_List);
                self.registrationContent = getAllMapData(registrationData[0].component);

              }
            }).catch(function (error) {
              console.log('Error');
            });
            axios.get(home, {
              headers: {
                'content-type': 'text/html',
                'Accept': 'text/html',
                'Accept-Language': langauage
              }
            }).then(function (response) {
              var data = response.data;
              let newsListTemp = [];
              if (data != undefined && data.Values != undefined) {
                newsListTemp = data.Values.filter(function (el) {
                  return el.Is_Active == true
                });
              }
              self.newsList = newsListTemp;
            }).catch(function (error) {});
            axios.get(offerUrl, {
              headers: {
                'content-type': 'text/html',
                'Accept': 'text/html',
                'Accept-Language': langauage
              }
            }).then(function (response) {
              var data = response.data;
              let offerListTemp = [];
              if (data != undefined && data.Values != undefined) {
                offerListTemp = data.Values.filter(function (el) {
                  return el.Is_Active == true && el.Show_In_Home == true
                });
              }
              self.offerList = offerListTemp;
            }).catch(function (error) {});

            axios.get(platformUrl, {
              headers: {
                'content-type': 'text/html',
                'Accept': 'text/html',
                'Accept-Language': langauage
              }
            }).then(function (response) {
              self.content = response.data;
              if (self.content != undefined && self.content != null) {
                var mainData = pluck('Main_Section', self.content.area_List);
                self.platformSection = getAllMapData(mainData[0].component);
              }
            }).catch(function (error) {
              console.log('Error');

            });

          }

        });

      });

    },
    dateFormatter: function (utc) {
      return (moment(utc).utcOffset("+05:30").format("DD MMMM YYYY"));
    },
    getmoreinfo(url) {
      if (url != null) {
        if (url != "") {
          url = url.split("/Template/")[1];
          url = url.split(' ').join('-');
          url = url.split('.').slice(0, -1).join('.');
          url = "news-detail.html?page=" + url + "&from=pkg";
          window.location.href = url;
        } else {
          url = "#";
        }
      } else {
        url = "#";
      }
      return url;
    }
  },
  mounted: function () {
    this.getPagecontent();
  },
});