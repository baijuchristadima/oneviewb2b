var HeaderComponent = Vue.component('headeritem', {
    template: `
    <div id="header">
    <div class="top-bar">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="contact-info">
                <ul>
            <li> <i class="fa fa-phone"></i> <a :href="'tel:'+ TopSection.Phone_Number">  Lagos {{TopSection.Phone_Number}}  </a></li> 
            <li>  <i class="fa fa-envelope"></i> <a :href="'mailto:' + TopSection.Email_Id"> {{TopSection.Email_Id}}</a> </li>
                    </ul>
            </div>
            
                <div class="social-media">
                    <ul>
                        <li v-for="meadiaItem in TopSection.Social_Media_List"><a :href="meadiaItem.Url" target="_blank"><i :class="meadiaItem.Icon"></i></a></li>
                       
                    </ul>
                </div>
                  </div>

        </div>
      </div>
    </div>
    
    <div class="container">
          <div class="row">
              <div class="col-md-12 ma-10">
            <nav id="navbar-main" class="navbar navbar-default">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              <a class="navbar-brand" href="index.html"><img :src="TopSection.Logo_Image_192_x_71_px" alt=""></a> </div>
            <div class="collapse navbar-collapse navbar-right" id="myNavbar">
              <ul class="nav navbar-nav">
                <li :class="{ 'active': activeClass === '/index.html'}"><a href="index.html">{{TopSection.Home_Label}}</a></li>
                  <li :class="{ 'active': activeClass === '/about-us.html'}"><a href="about-us.html">{{TopSection.About_Us_Label}}</a></li>
                <li :class="{ 'active': activeClass === '/contact-us.html'}"><a href="contact-us.html">{{TopSection.Contact_Label}}</a></li>
              </ul>
            </div>
    </nav>
                  
               
                  
              </div>
        </div>
   </div> 
    </div>
    `,
    data() {
        return {
            agencyCode: '',
            TopSection: {},
            activeClass: '',
            validationList: {},


        }
    },
    created() {
        var self = this;
        var pathArray = window.location.pathname.split('/');
        self.activeClass = '/' + pathArray[pathArray.length - 1];

    },
    computed: {
        checkMobileOrNot: function () {
            if (navigator.userAgent.match(/Android/i) ||
                navigator.userAgent.match(/webOS/i) ||
                navigator.userAgent.match(/iPhone/i) ||
                navigator.userAgent.match(/iPad/i) ||
                navigator.userAgent.match(/iPod/i) ||
                navigator.userAgent.match(/BlackBerry/i) ||
                navigator.userAgent.match(/Windows Phone/i)) {
                return true;
            }
            else { return false; }
        }
    },
    methods: {
        getPageheader: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }
                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/01 Master Page/01 Master Page/01 Master Page.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var mainData = self.pluck('Header_Section', self.data.area_List);
                            self.TopSection = self.getAllMapData(mainData[0].component);
                        }).catch(function (error) {
                            console.log('Error');
                        });
                    }

                });

            });

        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },
        getValidationMsgs: function () {

            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }
                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Validation/Validation/Validation.ftl';

                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            if (response.data.area_List.length) {

                                var validationData = self.pluck("Validations", response.data.area_List);
                                self.validationList = self.getAllMapData(validationData[0].component);
                                sessionStorage.setItem('validationItems', JSON.stringify(self.validationList));

                            }
                        }).catch(function (error) {
                            console.log('Error');
                        });
                    }

                });

            });


        },
    },
    mounted: function () {

        this.getPageheader();
        this.getValidationMsgs();
    },
})
var headerinstance = new Vue({
    el: 'header',
    name: 'headerArea',
    data() {
        return {
            key: '',
            content: null,
            getdata: true
        }

    },

});
Vue.component('footeritem', {
    template: `
    <div id="footer">
    <div class="container">
    <div class="row">
        <div class="col-md-4 col-sm-4 ">
            <div class="footer-contact">
              
                <div class="contact-d">
                <h3> <i class="fa fa-map-marker"></i>{{FooterSection.Address_Label}}</h3>
                <p v-html="FooterSection.Address"></p>
                    </div>
            </div>
        </div>
        
        <div class="col-md-4 col-sm-4 v-border">
            <div class="footer-contact">

                <div class="contact-d">
                <h3>   <i class="fa fa-envelope"></i> {{FooterSection.Email_Label}}</h3>
                <a :href="'mailto:' + HeaderSection.Email_Id">{{HeaderSection.Email_Id}}</a>
                </div>
            </div>
        </div>
        
        
        <div class="col-md-4 col-sm-4 v-border">
            <div class="footer-contact">
            
                <div class="contact-d">
                <h3> <i class="fa fa-phone"></i> {{FooterSection.Phone_Label}}</h3>
                <a href="'tel:' + HeaderSection.Phone_Number">Lagos {{HeaderSection.Phone_Number}}</a>
                </div>
            </div>
        </div>
    </div>
    
    <div class="link-media">
        <div class="row">
            <div class="col-md-8 col-sm-8">
                <div class="quick-link">
                    <ul>
                        <li><a href="index.html">{{HeaderSection.Home_Label}}</a></li>
                        <li><a href="about-us.html">{{HeaderSection.About_Us_Label}}</a></li>
                        <li><a href="contact-us.html">{{HeaderSection.Contact_Label}}</a></li>
                        <li><a href="privacy-policy.html">{{FooterSection.Privacy_Policy_Label}}</a></li>
                        <li><a href="terms-and-conditions.html">{{FooterSection.Terms_And_Conditions_Label}}</a></li>
                    </ul>
                </div>
            </div>
            
            
            <div class="col-md-4 col-sm-4">
                <div class="footer-social">
                    <ul>
                    <li v-for="meadiaItem in HeaderSection.Social_Media_List"><a :href="meadiaItem.Url" target="_blank">
                    <i :class="meadiaItem.Icon"></i></a></li>
                    </ul>
                </div>
            </div>
            
        </div>
    </div>
    </div>
<!-- Copy-Right -->
<div class="copy-right">
<div class="container">
    <div class="row">
        <div class="col-md-8 col-sm-8">
            <p v-html="FooterSection.Copyright_Description"></p>
        </div>
        
        <div class="col-md-4 col-sm-4">
            <div class="powered">
                <p v-html="FooterSection.Powered_By_Label"></p>
                <a href="#"><img :src="FooterSection.Powered_By_Logo_95_x37_px" alt="logo"> </a>
            </div>
        </div>
        
        
    </div>
</div>
</div>
<a href="#" class="scrollToTop" style="display: inline;"><i :class="FooterSection.ScrollToTop_Icon"></i></a> 
  </div>`,
    data() {
        return {
            agencyCode: '',
            FooterSection: {},
            HeaderSection: {},
            isLoading: false

        }
    },
    computed: {
        checkMobileOrNot: function () {
            if (navigator.userAgent.match(/Android/i) ||
                navigator.userAgent.match(/webOS/i) ||
                navigator.userAgent.match(/iPhone/i) ||
                navigator.userAgent.match(/iPad/i) ||
                navigator.userAgent.match(/iPod/i) ||
                navigator.userAgent.match(/BlackBerry/i) ||
                navigator.userAgent.match(/Windows Phone/i)) {
                return true;
            }
            else { return false; }
        }
    },
    methods: {
        getPagefooter: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var Footer = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/01 Master Page/01 Master Page/01 Master Page.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(Footer, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.content = response.data;
                            if (self.content != undefined && self.content != null) {
                                var mainData = self.pluck('Footer_Section', self.content.area_List);
                                self.FooterSection = self.getAllMapData(mainData[0].component);
                                var meData = self.pluck('Header_Section', self.content.area_List);
                                self.HeaderSection = self.getAllMapData(meData[0].component);

                            }

                        }).catch(function (error) {
                            console.log('Error');
                            self.content = [];
                        });
                    }

                });

            });

        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },



    },
    mounted: function () {
        this.getPagefooter();
    },

})
var footerinstance = new Vue({
    el: 'footer',
    name: 'footerArea',
    data() {
        return {
            key: 0,
            content: null,
            getdata: true
        }

    },
});
