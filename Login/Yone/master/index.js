var maininstance = new Vue({
    el: '#indexPage',
    name: 'indexPage',
    data: {
        agencyCode: '',
        mainContent: {},
        BannerSection: {},
        LoginSection: {},
        AboutSection: {},
        ChooseSection: {},
        isLoading: false,
        fullPage: true,
        flagForgot: false,
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },
        getPagecontent: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }
                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var homePageUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/02 Home Page/02 Home Page/02 Home Page.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(homePageUrl, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            console.log('responsee', response);
                            self.content = response.data;
                            if (self.content != undefined && self.content != null) {
                                var bannerData = self.pluck('Banner_Section', self.content.area_List);
                                self.BannerSection = self.getAllMapData(bannerData[0].component);
                                var logData = self.pluck('Login_Section', self.content.area_List);
                                self.LoginSection = self.getAllMapData(logData[0].component);
                                var aboutData = self.pluck('About_Section', self.content.area_List);
                                self.AboutSection = self.getAllMapData(aboutData[0].component);
                                var chooseData = self.pluck('Choose_Yone_Section', self.content.area_List);
                                self.ChooseSection = self.getAllMapData(chooseData[0].component);
                                setTimeout(() => {
                                    this.carosal();
                                }, 100);
                            }
                           
                            self.stopLoader();
                        }).catch(function (error) {
                            console.log('Error');
                            self.stopLoader();
                        });
                    }

                });

            });

        },
        stopLoader: function () {
            $('#preloader').delay(50).fadeOut(250);

        }
    },
    mounted: function () {
      this.getPagecontent();
      try {
        if ($.cookie) {
            if ($.cookie("remember")) {
                var cookieDetails = JSON.parse(atob($.cookie("remember")));
                document.getElementById("rememberMe").setAttribute("checked", "checked");
                document.getElementById("txtAgencyCode").value = cookieDetails.agencyCode;
                document.getElementById("txtUname").value = cookieDetails.userName;
            } else {
                document.getElementById("rememberMe").removeAttribute("checked");
                document.getElementById("txtAgencyCode").value = "";
                document.getElementById("txtUname").value = "";
            }
        }
    } catch (error) { }
    },
});
function carosal () {
    $("#owl-demo-1").owlCarousel({
        autoplay:true,
        autoPlay : 8000,
        autoplayHoverPause:true, 
        stopOnHover : false,  
        items : 1,
        margin:10,  
        lazyLoad : true,
        navigation : true,
        itemsDesktop : [1199, 1],
        itemsDesktopSmall : [979, 1],
        itemsTablet : [768, 1],
      });
         
         $( ".owl-prev").html('<i class="fa   fa-angle-left"></i>');
         $( ".owl-next").html('<i class="fa  fa-angle-right"></i>');
}
