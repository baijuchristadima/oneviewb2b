
var contact = new Vue({
    el: '#contact',
    data: {
        commonStore: vueCommonStore.state,
        BannerSection:{},
        ContactSection:{},
        ContactFormSection: {},
        MapSection:{},
        agencyCode: '',
        cntusername: '',
        cntemail: '',
        cntcontact: '',
        cntsubject: '',
        cntmessage: '',
        isLoading: false,
        fullPage: true
    },
    methods: {
  
      getPageData: function () {
          var self = this;
          self.isLoading = true;
          axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {

              var agencyFolderName = '';
              var agy;
              try {
                  response.data.forEach(function (agent, agentIndex) {
                      agent.domain.forEach(function (dom, domIndex) {
                          if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                              agy = agent;
                              agencyFolderName = agy.agencyFolderName;
                          }
                      });
                  });
              } catch (err) { }

              agy.registeredUsers.forEach(function (agyCode, domIndex) {
                  if (domIndex == 0) {
                      var huburl = HubServiceUrls.hubConnection.cmsUrl;
                      var portno = HubServiceUrls.hubConnection.ipAddress;
                      var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                      var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/04 Contact Us/04 Contact Us/04 Contact Us.ftl';
                      
                      var agencyCode = agyCode;
                      self.agencyCode = agencyCode;
                      axios.get(cmsPage, {
                          headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                      }).then(function (response) {
                        
                          self.data = response.data;
                          if (response.data.area_List) {
                              var TopData = self.pluck("Banner_Section", self.data.area_List);
                              self.BannerSection = self.getAllMapData(TopData[0].component);
      
                              var mainData = self.pluck("Contact_Section", self.data.area_List);
                              self.ContactSection = self.getAllMapData(mainData[0].component);
                              var mainData = self.pluck("Contact_Form_Section", self.data.area_List);
                              self.ContactFormSection = self.getAllMapData(mainData[0].component);
                              
                              var footerData = self.pluck("Map_Section", self.data.area_List);
                              self.MapSection = self.getAllMapData(footerData[0].component);
                          }
                          console.log('Banner  list',self.BannerSection);
                          console.log('Contact  list',self.ContactSection);
                          console.log('map list',self.MapSection);
                          self.isLoading = false;
                      }).catch(function (error) {
                        self.isLoading = false;
                          console.log('Error');
                      });
                  }

              });


          });

      },
      pluck(key, contentArry) {
          var Temparry = [];
          contentArry.map(function (item) {
              if (item[key] != undefined) {
                  Temparry.push(item[key]);
              }
          });
          return Temparry;
      },
      pluckcom(key, contentArry) {
          var Temparry = [];
          contentArry.map(function (item) {
              if (item[key] != undefined) {
                  Temparry = item[key];
              }
          });
          return Temparry;
      },
      getAllMapData: function (contentArry) {
          var tempDataObject = {};
          if (contentArry != undefined) {
              contentArry.map(function (item) {
                  let allKeys = Object.keys(item)
                  for (let j = 0; j < allKeys.length; j++) {
                      let key = allKeys[j];
                      let value = item[key];
                      if (key != 'name' && key != 'type') {
                          if (value == undefined || value == null) {
                              value = "";
                          }
                          tempDataObject[key] = value;
                      }
                  }
              });
          }
          return tempDataObject;
      },
      sendcontact: async function () {
          if (!this.cntusername) {
            alertify.alert(this.getValidationMsgByCode('M11'),this.getValidationMsgByCode('M01')).set('closable', false);
    
            return false;
          }
          if (!this.cntemail) {
            alertify.alert(this.getValidationMsgByCode('M11'),this.getValidationMsgByCode('M02')).set('closable', false);
            return false;
          }
          var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
          var matchArray = this.cntemail.match(emailPat);
          if (matchArray == null) {
            alertify.alert(this.getValidationMsgByCode('M11'),this.getValidationMsgByCode('M04')).set('closable', false);
            return false;
          }
          if (!this.cntcontact) {
            alertify.alert(this.getValidationMsgByCode('M11'),this.getValidationMsgByCode('M03')).set('closable', false);
            return false;
          }
          if (this.cntcontact.length < 8) {
            alertify.alert(this.getValidationMsgByCode('M11'),this.getValidationMsgByCode('M05')).set('closable', false);
            return false;
          }
          if (!this.cntsubject) {
            alertify.alert(this.getValidationMsgByCode('M11'),this.getValidationMsgByCode('M06')).set('closable', false);
            return false;
          }
          if (!this.cntmessage) {
            alertify.alert(this.getValidationMsgByCode('M11'),this.getValidationMsgByCode('M07')).set('closable', false);
            return false;
          } else {
            this.isLoading = true;
            var frommail = this.notificationEmail || this.commonStore.fallBackEmail;
            var custmail = {
              type: "UserAddedRequest",
              fromEmail: frommail,
              toEmails: Array.isArray(this.cntemail) ? this.cntemail : [this.cntemail],
              logo: this.BannerSection.Banner_Image1890x207px || "",
              agencyName: "EuroAfrica",
              agencyAddress: this.ContactSection.Address_Detail || "",
              personName: this.cntusername || "",
              primaryColor: "#3c8044",
              secondaryColor: "#db6126"
            };
            let agencyCode = this.agencyCode;
            let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
            let insertContactData = {
              type: "Contact Us",
              keyword1: this.cntusername,
              keyword2: this.cntemail,
              keyword3: this.cntcontact,
              keyword4: this.cntsubject,
              text1: this.cntmessage,
              date1: requestedDate,
              nodeCode: agencyCode
            };
            let responseObject = await this.cmsRequestData("POST", "cms/data", insertContactData, null);
            try {
              let insertID = Number(responseObject);
              var self = this;
              var emailApi = self.commonStore.hubUrls.emailServices.emailApi;
              sendMailService(emailApi, custmail);
              this.cntemail = '';
              this.cntusername = '';
              this.cntcontact = '';
              this.cntmessage = '';
              this.cntsubject = '';
              this.isLoading = false;
              alertify.alert(this.getValidationMsgByCode('M09'),this.getValidationMsgByCode('M08'));
            } catch (e) {
              this.isLoading = false;
            }
          }
      },
      async cmsRequestData(callMethod, urlParam, data, headerVal) {
        try {
          var huburl = HubServiceUrls.hubConnection.cmsUrl;
          var portno = HubServiceUrls.hubConnection.ipAddress;
          const url = huburl + portno + "/" + urlParam;
          if (data != null) {
            data = JSON.stringify(data);
          }
          const response = await fetch(url, {
            method: callMethod, // *GET, POST, PUT, DELETE, etc.
            credentials: "same-origin", // include, *same-origin, omit
            headers: { 'Content-Type': 'application/json' },
            body: data, // body data type must match "Content-Type" header
          });
          try {
            const myJson = await response.json();
            return myJson;
          } catch (error) {
            return object;
          }
        } catch (error) {
          this.isLoading = false;
        }
         
      },
      getValidationMsgByCode: function (code) {
        if (sessionStorage.validationItems !== undefined) {
            var validationList = JSON.parse(sessionStorage.validationItems);
            for (let validationItem of validationList.Validation_List) {
                if (code === validationItem.Code) {
                    return validationItem.Message;
                }
            }
        }
    },
      
    },
    mounted: function () {
        this.getPageData();
    },
       
});