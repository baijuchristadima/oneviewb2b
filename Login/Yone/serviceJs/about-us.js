var aboutUs = new Vue({
    el: '#about',
    name: 'about',
    data: {
        BannerSection:{},
        AboutUsSection: {},
        isLoading: false,
        fullPage: true
        
    },
    methods: {
      
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
              if (item[key] != undefined) {
                Temparry.push(item[key]);
              }
            });
            return Temparry;
          },
          pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
              if (item[key] != undefined) {
                Temparry = item[key];
              }
            });
            return Temparry;
          },
          getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
              contentArry.map(function (item) {
                let allKeys = Object.keys(item)
                for (let j = 0; j < allKeys.length; j++) {
                  let key = allKeys[j];
                  let value = item[key];
                  if (key != 'name' && key != 'type') {
                    if (value == undefined || value == null) {
                      value = "";
                    }
                    tempDataObject[key] = value;
                  }
                }
              });
            }
            return tempDataObject;
          },
          getPagecontent: function () {
            var self = this;
            self.isLoading = true;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
              var agencyFolderName = '';
              var agy;
              try {
                response.data.forEach(function (agent, agentIndex) {
                  agent.domain.forEach(function (dom, domIndex) {
                    if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                      agy = agent;
                      agencyFolderName = agy.agencyFolderName;
                    }
                  });
                });
              } catch (err) { }
              agy.registeredUsers.forEach(function (agyCode, domIndex) {
                if (domIndex == 0) {
                  var huburl = HubServiceUrls.hubConnection.cmsUrl;
                  var portno = HubServiceUrls.hubConnection.ipAddress;
                  var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                  var homePageUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/03 About  Us Page/03 About  Us Page/03 About  Us Page.ftl';
                    
                  axios.get(homePageUrl, {
                    headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                  }).then(function (response) {
                    console.log('responsee', response);
                    self.content = response.data;
                   
                    if (self.content != undefined && self.content != null) {
                      var mainData = self.pluck('Banner_Section', self.content.area_List);
                      self.BannerSection = self.getAllMapData(mainData[0].component);
    
                      var mainData = self.pluck('About_Section', self.content.area_List);
                      self.AboutUsSection = self.getAllMapData(mainData[0].component);
                    }
                    self.isLoading = false;
                  }).catch(function (error) {
                    self.isLoading = false;
                    console.log('Error');
                   
                  });
                }
      
              });
      
            });
            
          },
    },
    mounted: function () {
       
        this.getPagecontent();
    },
       
});