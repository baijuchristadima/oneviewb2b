var HeaderComponent = Vue.component('headeritem', {
    template: `
  <div id="header" v-cloak>
    <div class="top">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-sm-12 col-md-12">
            <div class="contact-info">
              <div class="phone-number"> <span>Call Us :</span> <a :href="'tel:'+HeaderContent.Phone_Number">{{HeaderContent.Phone_Number}}</a> </div>
              <div class="email"> <span>Email :</span> <a :href="'mailto:'+HeaderContent.Email">{{HeaderContent.Email}}</a> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <nav id="navbar-main" class="navbar navbar-default">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              <a class="navbar-brand" href="index.html"><img :src="HeaderContent.Logo" alt=""></a> </div>
            <div class="collapse navbar-collapse navbar-right" id="myNavbar">
              <ul class="nav navbar-nav">
                <li :class="activePageName=='Home' ? 'active' :''"><a href="index.html">{{Menu.Home_Label}}</a></li>
                <li :class="activePageName=='about' ? 'active' :''"><a href="about-us.html">{{Menu.About_Us_Label}}</a></li>
                <li :class="activePageName=='contact' ? 'active' :''"><a href="contact-us.html">{{Menu.Contact_Us_Label}}</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </nav>
  </div>
  `,
    data() {
        return {

            agencyCode: '',
            contact: { Phone_Number_1: '' },
            active_er: (sessionStorage.active_er) ? sessionStorage.active_er : 1,
            commonStore: vueCommonStore.state,
            HeaderContent: {},
            Menu: {},
            activePageName: '',

        }
    },
    methods: {
        activate: function (el) {
            sessionStorage.active_er = el;
            this.active_er = el;
            if (el == 1 || el == 2) {
                maininstance.actvetab = el;
            }
            else {
                maininstance.actvetab = 0;
            }
        },
        headerData: function () {
            var self = this;
            var url = window.location.href;
            if (url.includes('index.html')) {
                this.activePageName = "Home";
            } else if (url.includes('about-us.html')) {
                this.activePageName = "about";
            } else if (url.includes('contact-us.html')) {
                this.activePageName = "contact";
            } else {
                this.activePageName = "Home";
            }
        },
        getPageheader: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {

                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Header/Header/Header.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            if (response.data.area_List.length) {
                                var content = response.data;
                                var Headers = self.pluck('Header', content.area_List);
                                if (Headers != undefined) {
                                    self.HeaderContent = self.getAllMapData(Headers[0].component);
                                }
                                var Menu = self.pluck('Menu', content.area_List);
                                if (Menu != undefined) {
                                    self.Menu = self.getAllMapData(Menu[0].component);
                                }
                            }
                        }).catch(function (error) {
                            console.log('Error');
                            self.HeaderContent = [];
                            self.Menu = [];
                        });
                    }

                });

            });

        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },
    },
    mounted: function () {
        this.getPageheader();
        this.headerData();
    },
})
var headerinstance = new Vue({
    el: 'header',
    name: 'headerArea',
    data() {
        return {
            key: '',
            content: null,
            getdata: true
        }

    },

});
Vue.component('footeritem', {
    template: `
    <div class="footer" v-cloak>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="ftr-cntc">
                    <h3>{{FooterContent.Get_in_Touch_Label}}</h3>
                    <p>{{FooterContent.Get_in_Touch_Description}}</p>
                    <ul>
                        <li>
                            <i class="fa fa-map-marker fnt-sz-25" aria-hidden="true"></i>
                            {{ContactData.Address}}</li>
                        <li>
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                            <a :href="'mailto:'+ContactData.Email">{{ContactData.Email}}</a></li>
                        <li>
                            <i class="fa fa-phone fnt-sz-22" aria-hidden="true"></i>
                            <a :href="'tel:'+ContactData.Phone_Number">{{ContactData.Phone_Number}}</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="quick-links">
                    <h3>{{FooterContent.Quick_Links_Label}}</h3>
                    <ul>
                        <li><i class="fa  fa-angle-double-right"></i><a href="index.html">{{Menu.Home_Label}}</a></li>
                        <li><i class="fa  fa-angle-double-right"></i><a href="about-us.html">{{Menu.About_Us_Label}}</a></li>
                        <li><i class="fa  fa-angle-double-right"></i><a href="contact-us.html">{{Menu.Contact_Us_Label}}</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <div class="newsletter">
                    <h3>{{FooterContent.Newsletter_Title}}</h3>
                    <p>{{FooterContent.Newsletter_Description}}</p>
                    <div class="news-sub">
                        <input type="text" id="text" v-model="newsltremail"
                            :placeholder="FooterContent.Newsletter_Placeholder">
                        <button type="submit" v-on:click="sendnewsletter"><i class="fa  fa-paper-plane"></i></button>
                    </div>
                    <div class="foot-social">
                        <ul>
                            <li><a :href="SocialContent.Facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            <li><a :href="SocialContent.Twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li><a :href="SocialContent.LinkedIn" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                            <li><a :href="SocialContent.Instagram" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
    <div class="all-reserved">
    <div class="reserved">
        <p>{{FooterContent.Copyright_Content}}</p>
    </div>
    <div class="powered">
        <p>Powered by:</p>
        <a href="http://www.oneviewit.com/" target="_blank"><img src="images/oneview-logo.png" alt="logo"></a>
    </div>
    </div>
    </div>
    </div>


  `,
    data() {
        return {
            commonStore: vueCommonStore.state,
            agencyCode: '',
            ContactData: {},
            newsltremail: null,
            FooterContent: {},
            SocialContent: {},
            HeaderContent: {},
            Menu: {},
            notificationEmail: ""
        }
    },
    methods: {
        getPagefooter: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {

                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var contacturl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Contact Us/Contact Us/Contact Us.ftl';
                        var Headerurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Header/Header/Header.ftl';
                        var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Footer/Footer/Footer.ftl';
                        self.agencyCode = agyCode;
                        axios.get(pageurl, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            var content = response.data;
                            if (response.data.area_List.length) {
                                var Footers = self.pluck('Footer', content.area_List);
                                if (Footers != undefined) {
                                    var FootersTemp = self.getAllMapData(Footers[0].component);
                                    self.FooterContent = FootersTemp;
                                }

                                var Social = self.pluck('Social_Media_Link', content.area_List);
                                if (Social != undefined) {
                                    var SocialsTemp = self.getAllMapData(Social[0].component);
                                    self.SocialContent = SocialsTemp;
                                }
                            }
                        }).catch(function (error) {
                            console.log('Error');
                            self.footer = [];
                        });
                        axios.get(Headerurl, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.content = response.data;
                            if (response.data.area_List.length) {
                                var Menu = self.pluck('Menu', self.content.area_List);
                                if (Menu != undefined) {
                                    var MenuTemp = self.getAllMapData(Menu[0].component);
                                    self.Menu = MenuTemp;
                                }
                            }
                        }).catch(function (error) {
                            console.log('Error');
                            self.footer = [];
                        });
                        axios.get(contacturl, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.content = response.data;
                            var Notification = self.pluck('Notifications', response.data.area_List);
                            if (Notification.length > 0) {
                                self.notificationEmail = self.pluckcom('Notification_Email', Notification[0].component);
                            }
                            if (response.data.area_List.length) {
                                var ContactData = self.pluck('Contact_Us_Section', self.content.area_List);
                                if (ContactData != undefined) {
                                    self.ContactData.Address = self.pluckcom('Address', ContactData[0].component);
                                    self.ContactData.Email = self.pluckcom('Email', ContactData[0].component);
                                    self.ContactData.Phone_Number = self.pluckcom('Phone_Number', ContactData[0].component);
                                }
                            }
                        }).catch(function (error) {
                            console.log('Error');
                            self.aboutUsContent = [];
                        });
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Header/Header/Header.ftl';
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            if (response.data.area_List.length) {
                                var content = response.data;
                                var Headers = self.pluck('Header', content.area_List);
                                if (Headers != undefined) {
                                    self.HeaderContent = self.getAllMapData(Headers[0].component);
                                }
                            }
                        }).catch(function (error) {
                            console.log('Error');
                            self.HeaderContent = [];
                        });
                    }

                });

            });

        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        },
        sendnewsletter: async function () {

            if (!this.newsltremail) {
                alertify.alert('Alert', 'Email Id required.');
                return false;;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.newsltremail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.');
                return false;
            } else {
                var agencyCode = this.agencyCode
                var filterValue = "type='Newsletter' AND keyword1='" + this.newsltremail + "'";
                var allDBData = await this.getDbData4Table(agencyCode, filterValue, "date1");

                if (allDBData != undefined && allDBData.length > 0) {
                    alertify.alert('Alert', 'Email address already enabled.').set('closable', false);
                    return false;
                }
                else {
                    var postData = {
                        type: "UserAddedRequest",
                        fromEmail: this.notificationEmail || this.commonStore.fallBackEmail,
                        toEmails: Array.isArray(this.newsltremail) ? this.newsltremail : [this.newsltremail],
                        logo: this.HeaderContent.Logo || "",
                        agencyName: "Maktwo Travels",
                        agencyAddress: this.ContactData.Address || "",
                        personName: this.newsltremail.split("@")[0],
                        primaryColor: "#99373c",
                        secondaryColor: "#362e3f"
                    };
                    var agencyCode = this.agencyCode
                    let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                    let insertSubscibeData = {
                        type: "Newsletter",
                        date1: requestedDate,
                        keyword1: this.newsltremail,
                        keyword2: "Subscribe Newsletter",
                        nodeCode: agencyCode
                    };
                    let responseObject = await this.cmsRequestData("POST", "cms/data", insertSubscibeData, null);
                    try {
                        let insertID = Number(responseObject);
                        var self = this;
                        mailUrl = self.commonStore.hubUrls.emailServices.emailApi;
                        sendMailService(mailUrl, postData);
                        this.newsltremail = '';
                        alertify.alert('Newsletter', 'Thank you for subscribing !');
                    } catch (e) {

                    }
                }

            }
        },
        async getDbData4Table(agencyCode, extraFilter, sortField) {

            var allDBData = [];
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            var cmsURL = huburl + portno + '/cms/data/search/byQuery';
            var queryStr = "select * from cms_forms_data where nodeCode = '" + agencyCode + "'";
            if (extraFilter != undefined && extraFilter != '') {
                queryStr = queryStr + " AND " + extraFilter;
            }
            var requestObject = {
                query: queryStr,
                sortField: sortField,
                from: 0,
                orderBy: "desc"
            };
            let responseObject = await this.cmsRequestData("POST", "cms/data/search/byQuery", requestObject, null);
            if (responseObject != undefined && responseObject.data != undefined) {
                allDBData = responseObject.data;
            }
            return allDBData;

        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },

    },
    mounted: function () {
        this.getPagefooter();
    },

})
var footerinstance = new Vue({
    el: 'footer',
    name: 'footerArea',
    data() {
        return {
            key: 0,
            content: null,
            getdata: true
        }

    },
});
