//global variables
var agencyName = "";
var agencyCodes = [];

try {
    if ($.cookie) {  
        if ($.cookie("remember")) {
            var cookieDetails = JSON.parse(atob($.cookie("remember")));
            document.getElementById("rememberMe").setAttribute("checked", "checked");
            document.getElementById("txtAgencyCode").value = cookieDetails.agencyCode;
            document.getElementById("txtUname").value = cookieDetails.userName;
        } else {
            document.getElementById("rememberMe").removeAttribute("checked");
            document.getElementById("txtAgencyCode").value = "";
            document.getElementById("txtUname").value = "";
        }
    }
} catch (error) {}

window.onload = function() {
    if(!window.location.href.includes("#")&&!window.location.href.includes(".html")) {
    window.location = window.location + '#';
    window.location.reload();
    }
}

if (window.location.pathname == "/") {
    setLoginPage("login");
} else {
    if (window.location.href.toLowerCase().indexOf("/login") >= 0) {
        setLoginPage("login");
    }
    var timer = setInterval(function () { checkLoginorNot(); }, 1000);
}

function setLoginPage(reason) {
    // if (agencyName == "") {
    axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
        var agencyFolderName = "";
        var users = [];

        for (var index = 0; index < response.data.length; index++) {
            var domain = response.data[index].domain;
            if (domain.indexOf(window.location.hostname) != -1) {
                agencyFolderName = response.data[index].agencyFolderName;
                users = response.data[index].registeredUsers;
                break;
            }
        }

        var url = window.location.href;

        if (url.toLowerCase().indexOf("/login") >= 0 || reason == "login") {
            if (url.toLowerCase().indexOf(agencyFolderName.toLowerCase()) == -1 || reason == "timeout") {
                window.localStorage.clear();
                window.sessionStorage.clear();
                window.location.href = "/" + agencyFolderName;
            } else if (agencyFolderName == "") {
                window.location.href = "/404.html";
            }
        } else if (reason == "timeout" || reason == "error") {
            window.localStorage.clear();
            window.sessionStorage.clear();
            if (agencyFolderName) { window.location.href = "/" + agencyFolderName; }
            else { window.location.href = "/404.html"; }
        } else {
            window.location.href = "/404.html";
        }

        agencyName = agencyFolderName.split("/")[1];
        agencyCodes = users;

        // setTimeout(function () { document.getElementById("pageLoader").style.display = "none"; }, 1000);
    }).catch(function (error) {
        console.log(error);
    });
    // }
}

function checkLoginorNot() {
    if (window.location.href.indexOf("ResetPassword.html") == -1) {
        var sessionTime = moment(new Date(window.localStorage.getItem("sessionTime")));
        var timeoutTime = moment(new Date(window.localStorage.getItem("timeoutTime")));
        var accessToken = window.localStorage.getItem("accessToken");

        var currentTime = moment(new Date());
        var timeRemaining = moment.duration(currentTime.diff(sessionTime)).asMinutes();
        var timeOutTimeRemaining = moment.duration(currentTime.diff(timeoutTime)).asMinutes();


        if (accessToken && timeOutTimeRemaining > 20) {

            // Set the name of the hidden property and the change event for visibility
            var hidden;
            if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support 
                hidden = "hidden";
            } else if (typeof document.msHidden !== "undefined") {
                hidden = "msHidden";
            } else if (typeof document.webkitHidden !== "undefined") {
                hidden = "webkitHidden";
            }

            // Warn if the browser doesn't support addEventListener or the Page Visibility API
            if (typeof document.addEventListener === "undefined" || hidden === undefined) {
                console.log("Page Visibility API not supported.");
            } else {
                console.log(document[hidden], timeOutTimeRemaining);

                if (document[hidden] && timeOutTimeRemaining > 20) {
                    if (!alertify.confirm().isOpen()) {
                        // clearInterval(timer);
                        alertify.confirm("Warning", "Due to inactivity your session has expired. Please press OK to refresh and Cancel to log out.",
                            function () {
                                refreshToken();
                            },
                            function () { setLoginPage("timeout"); });
                        refreshToken();
                    }else {
                        refreshToken();
                    }
                } else {
                    refreshToken();
                }
            }
            window.localStorage.setItem("timeoutTime", new Date());
        } else if (accessToken && timeRemaining > 30) {
            if (!alertify.alert().isOpen()) {
                if (window.location.pathname.toLowerCase().indexOf("/login") >= 0) {
                    setLoginPage("timeout");
                } else {
                    alertify.alert("Timeout", "Your session has expired. Please log in.", function () {
                        setLoginPage("timeout");
                    });
                }
            }
        } else if (accessToken && timeRemaining > 9) {
            refreshToken();
        } else if (accessToken && timeRemaining <= 9) {
            if (window.location.href.toLowerCase().indexOf("/login") >= 0) {
                var agencyNode = window.localStorage.getItem("agencyNode");
                if (agencyNode) {
                    pushToPortal(JSON.parse(atob(agencyNode)));
                }
            }
        } else {
            if (window.location.pathname.toLowerCase().indexOf("/login") != -1) {
                try {
                    // setTimeout(function () { document.getElementById("pageLoader").style.display = "none"; }, 1000);
                } catch (error) { }
            } else {
                var agencyNode = window.localStorage.getItem("agencyNode");
                if (!agencyNode) {
                    setLoginPage("login");
                }
            }
        }
    }
}

function refreshToken() {
    var hubUrls = HubServiceUrls;
    var huburl = hubUrls.hubConnection.baseUrl;
    var port = hubUrls.hubConnection.ipAddress;
    var reNewTokenUrl = hubUrls.hubConnection.hubServices.reNewToken;
    var accessToken = window.localStorage.getItem("accessToken");

    axios.get(huburl + port + reNewTokenUrl, {
        headers: {
            Authorization: "Bearer " + accessToken
        }
    }).then(function (response) {
        //User is not active
        window.localStorage.setItem("sessionTime", new Date());
        window.localStorage.setItem("accessToken", response.headers.access_token);

        var agencyNode = window.localStorage.getItem("agencyNode");

        if (window.location.href.toLowerCase().indexOf("/login") >= 0) {
            pushToPortal(response.data.user);
        } else if (agencyNode != btoa(JSON.stringify(response.data.user))) {
            window.localStorage.setItem("agencyNode", btoa(JSON.stringify(response.data.user)));
        }
    }).catch(function (error) {
        setLoginPage("error")
    });
}

function sendLogin() {
    var continueLogin = true;
    var userName = document.getElementById("txtUname").value;
    var password = document.getElementById("txtPwd").value;
    var agencyCode = document.getElementById("txtAgencyCode").value;

    var rmCheck = document.getElementById("rememberMe");

    if (stringIsNullorEmpty(agencyCode)) {
        alertify.alert("Warning", "AgencyCode required!", function () { });
        continueLogin = false;
    } else if (stringIsNullorEmpty(userName)) {
        alertify.alert("Warning", "Username required!", function () { });
        continueLogin = false;
    } else if (stringIsNullorEmpty(password)) {
        alertify.alert("Warning", "Password required!", function () { });
        continueLogin = false;
    }

    if (continueLogin) {
        sendRequestToHUB(userName, password, agencyCode);
        if (rmCheck.checked) {
            $.cookie("remember", btoa(JSON.stringify({
                userName: userName,
                agencyCode: agencyCode,
                checkbox: rmCheck.value
            })), { expires: 50 });
        } else {
            $.removeCookie("remember"); 
        }
    }
}

function sendRequestToHUB(userName, password, agencyCode) {
    document.getElementById("loginLoader").style.display = "block";

    // axios.get("/Resources/HubUrls/HubServiceUrls.json").then(function (response) {
    var hubUrls = HubServiceUrls;
    var hubUrl = hubUrls.hubConnection.baseUrl;
    var port = hubUrls.hubConnection.ipAddress;
    var getAgencyCodesUrl = hubUrls.hubConnection.hubServices.getAgencyCodes;
    var authenticateUrl = hubUrls.hubConnection.hubServices.authenticateUrl;
    var encodedString = btoa(userName + ":" + password);
    var isSuperAdmin = agencyCodes.length == 0 ? true : false;

    if (!isSuperAdmin) {
        var data = {
            agencyCodeList: agencyCodes
        };
        var axiosConfig = {
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer "
            }
        };
        axios.post(hubUrl + port + getAgencyCodesUrl, data, axiosConfig).then(function (response) {
            if (response.data.indexOf(agencyCode.toUpperCase()) != -1) {
                axios.get(hubUrl + port + authenticateUrl + "/" + agencyCode, {
                    headers: {
                        Authorization: "Basic " + encodedString
                    }
                }).then(function (response) {
                    window.localStorage.setItem("sessionTime", new Date());
                    window.localStorage.setItem("timeoutTime", new Date());
                    window.localStorage.setItem("accessToken", response.headers.access_token);
                    window.localStorage.setItem("solutionId", response.data.user.loginNode.solutionId);
                    pushToPortal(response.data.user);
                }).catch(function (error) {
                    document.getElementById("loginLoader").style.display = "none";
                    alertify.alert("Warning", "Invalid username or password.");
                });
            } else {
                document.getElementById("loginLoader").style.display = "none";
                alertify.alert("Warning", "Invalid username or password.");
            }
        }).catch(function (error) {
            document.getElementById("loginLoader").style.display = "none";
            alertify.alert("Warning", "Invalid username or password.");
        });
    } else {
        axios.get(hubUrl + port + authenticateUrl + "/" + agencyCode, {
            headers: {
                Authorization: "Basic " + encodedString
            }
        }).then(function (response) {
            window.localStorage.setItem("sessionTime", new Date());
            window.localStorage.setItem("accessToken", response.headers.access_token);
            pushToPortal(response.data.user);
        }).catch(function (error) {
            document.getElementById("loginLoader").style.display = "none";
            alertify.alert("Warning", "Invalid username or password.");
        });
    }
    // }).catch(function (error) {
    //   document.getElementById("loginLoader").style.display = "none";
    //   alertify.alert("Warning", "Invalid username or password.");
    // });
}

function forgotPassword() {
    var emailID = document.getElementById("txtEmail").value;
    var agyCode = document.getElementById("txtAgency").value;
    var isValidEmailString = true;
    var isAgencyCodeNotNull = true;
    if (stringIsNullorEmpty(agyCode)) {
        alertify.alert("Warning", "Please fill agency node.", function () { });
        isAgencyCodeNotNull = false;
    } else if (stringIsNullorEmpty(emailID)) {
        alertify.alert("Warning", "Please fill email id.", function () { });
        isValidEmailString = false;
    } else if (!isValidEmail(emailID)) {
        alertify.alert("Warning", "Your email id seems incorrect.", function () { });
        isValidEmailString = false;
    } else {
        document.getElementById("loderForgot").style.display = "block";
    }
    


    if (isValidEmailString && isAgencyCodeNotNull) {
        axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
            var hubUrls = HubServiceUrls;

            var agencyFolderName = '';
            var agy;
            try {
                response.data.forEach(function (agent, agentIndex) {
                    agent.domain.forEach(function (dom, domIndex) {
                        if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                            agy = agent;
                            agencyFolderName = agy.agencyFolderName;
                        }
                    });
                });
            } catch (err) { }

            var hubUrl = hubUrls.hubConnection.baseUrl;
            var port = hubUrls.hubConnection.ipAddress;
            var requrl = hubUrls.hubConnection.hubServices.forgotPasswordUrl;

            var data = {
                agencyCode: agyCode,
                emailId: emailID,
                logoUrl: window.location.href + "website-informations/logo/logo.png",
                websiteUrl: window.location.origin,
                resetUri: window.location.origin + "/" + agencyFolderName + "/ResetPassword.html"
            };

            axios.post(hubUrl + port + requrl, data).then(function (response) {
                if (response.data != "") {
                    alertify.alert("Success", response.data);
                }else {
                    alertify.alert("Error", "Error in forgot password. Please contact admin.");
                }
                document.getElementById("loderForgot").style.display = "none";
            }).catch(function (error) {
                if (error.response.data.message != "") {
                    alertify.alert("Error", error.response.data.message);
                } else {
                    alertify.alert("Error", "Error in forgot password. Please contact admin.");
                }
                document.getElementById("loderForgot").style.display = "none";
            });
        });
    }
}

//send Mail Functions
function sendMailService(reqUrl, postData) {
    return axios
        .post(reqUrl, postData)
        .then(res => {
            console.log("Mail Status: ", res);
            return true;
        })
        .catch(err => {
            console.log("Mail Status: ", err);
            return false;
        });
}

function resetPassword() {
    var newPwd = document.getElementById("txtPassword").value;
    var cnfmPwd = document.getElementById("txtConfirmPassword").value;
    var validPassword = true;
    if (newPwd == "") {
        alertify.alert("Required.", "New password required.");
        validPassword = false;
    } else if (cnfmPwd == "") {
        alertify.alert("Required.", "Confirm password required.");
        validPassword = false;
    } else if (newPwd != cnfmPwd) {
        alertify.alert("Required.", "Password mismach.");
        validPassword = false;
    }

    if (validPassword) {
        var url = window.location.href;
        var token = url.split("ResetPassword.html?")[1];
        if (token != undefined) {
            var data = {
                newPassword: newPwd
            };
            let axiosConfig = {
                headers: {
                    "Content-Type": "application/json;charset=UTF-8",
                    Authorization: "Bearer " + token
                }
            };
            var hubUrls = HubServiceUrls;
            var hubUrl = hubUrls.hubConnection.baseUrl;
            var port = hubUrls.hubConnection.ipAddress;
            var resetPassword = hubUrls.hubConnection.hubServices.resetPassword;
            axios.put(hubUrl + port + resetPassword, data, axiosConfig).then(function(res) {
                alertify.alert("Message", "Password changed, Please Login.", function () {
                    window.location.href = "/";
                });
            }).catch(function(err){
                if (err.response.data.message) {
                    alertify.alert("Failed", err.response.data.message);
                }else {
                    alertify.alert("Failed", "We have found some technical difficulties. Please contact admin.");
                }
            });
        } else {
            alertify.alert("Warning.", "Token is invalid. Please contact admin.", function () {
                window.location.href = "/";
            });
        }
    }
}

function pushToPortal(user) {
  if (user != null) {
    window.localStorage.setItem(
        "lookNfeel",
        JSON.stringify(user.loginNode.lookNfeel)
      );
    //store agencyNode in storage
    window.localStorage.setItem("agencyNode", btoa(JSON.stringify(user)));

    window.location.href = "/search.html";
  } else {
    document.getElementById("loginLoader").style.display = "none";
    alertify.alert(
      "Error - HB0001",
      "No services activated. Please contact admin.",
      function () {
        setLoginPage("error");
      }
    );
  }
}