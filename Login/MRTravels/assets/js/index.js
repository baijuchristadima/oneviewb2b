var packagelist = new Vue({
  el: "#index",
  name: "index",
  data: {
    pageDetails: {},
    agencyCode: "",
    headerDetails: {},
    footerDetails: {},
    airlineDetails: [],
  },
  methods: {
    // slider: function () {
    //   if (this.airlineDetails != undefined && this.airlineDetails.length > 0) {
    //     Vue.nextTick(function () {
    //       $(".autoplay").slick({
    //         slidesToShow: 4,
    //         slidesToScroll: 1,
    //         autoplay: true,
    //         autoplaySpeed: 2000,
    //         responsive: [
    //           {
    //             breakpoint: 1024,
    //             settings: {
    //               slidesToShow: 3,
    //               slidesToScroll: 3,
    //               infinite: true,
    //               dots: false,
    //             },
    //           },
    //           {
    //             breakpoint: 990,
    //             settings: {
    //               slidesToShow: 2,
    //               slidesToScroll: 2,
    //             },
    //           },
    //           {
    //             breakpoint: 600,
    //             settings: {
    //               slidesToShow: 1,
    //               slidesToScroll: 1,
    //             },
    //           },
    //         ],
    //       });
    //     }, this);
    //   }
    // },
    getPagecontent: function () {
      var self = this;
      var agencyCode = document.getElementById("txtAgencyCode").value;
      axios
        .get("/Resources/AgencyInformations/AgencyInformation.json")
        .then(function (response) {
          console.log(document.location.hostname.toLowerCase());

          var agencyFolderName = "";
          var agy;
          try {
            response.data.forEach(function (agent, agentIndex) {
              agent.domain.forEach(function (dom, domIndex) {
                if (
                  dom.toLowerCase() == document.location.hostname.toLowerCase()
                ) {
                  agy = agent;
                  agencyFolderName = agy.agencyFolderName;
                }
              });
            });
          } catch (err) {}

          agy.registeredUsers.forEach(function (agyCode, domIndex) {
            if (domIndex == 0) {
              var huburl = HubServiceUrls.hubConnection.cmsUrl;
              var portno = HubServiceUrls.hubConnection.ipAddress;
              var langauage = localStorage.Languagecode
                ? localStorage.Languagecode
                : "en";
              // var airlineMaster = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Master Table/Air Line Deal Info/Air Line Deal Info/Air Line Deal Info.ftl';
              var contactUS =
                huburl +
                portno +
                "/persons/source?path=/B2B/AdminPanel/CMS/" +
                agyCode +
                "/Template/Contact Us/Contact Us/Contact Us.ftl";
              var Home_Page =
                huburl +
                portno +
                "/persons/source?path=/B2B/AdminPanel/CMS/" +
                agyCode +
                "/Template/Home Page/Home Page/Home Page.ftl";
              console.log(agencyFolderName);
              console.log(agyCode);

              // axios.get(airlineMaster, {
              //     headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
              // }).then(function(response) {
              //     if (response.data.Values !=undefined) {
              //         self.airlineDetails=response.data.Values;
              //         self.slider();
              //     }
              // }).catch(function(error) {
              //     console.log('Error');
              // });

              axios
                .get(contactUS, {
                  headers: {
                    "content-type": "text/html",
                    Accept: "text/html",
                    "Accept-Language": langauage,
                  },
                })
                .then(function (response) {
                  if (
                    response.data.area_List != undefined &&
                    response.data.area_List.length > 0 &&
                    response.data.area_List[0].Page_Details != undefined
                  ) {
                    var pageDetailsTem = self.getAllMapData(
                      response.data.area_List[0].Page_Details.component
                    );
                    self.pageDetails = pageDetailsTem;
                  }
                })
                .catch(function (error) {
                  console.log("Error");
                });

              axios
                .get(Home_Page, {
                  headers: {
                    "content-type": "text/html",
                    Accept: "text/html",
                    "Accept-Language": langauage,
                  },
                })
                .then(function (response) {
                  // headerDetails:{},footerDetails:{},

                  if (response.data.area_List != undefined) {
                    if (
                      response.data.area_List.length > 0 &&
                      response.data.area_List[0].Page_Details != undefined
                    ) {
                      var pageDetailsTem = self.getAllMapData(
                        response.data.area_List[0].Page_Details.component
                      );

                      if (pageDetailsTem.Deal_Title != undefined) {
                        var allList = pageDetailsTem.Deal_Title.split(" ");
                        pageDetailsTem.dealTitle1 = allList[0];
                        pageDetailsTem.dealTitle2 = pageDetailsTem.Deal_Title.substring(
                          allList[0].length
                        );
                      }

                      self.headerDetails = pageDetailsTem;
                    }
                    if (
                      response.data.area_List.length > 2 &&
                      response.data.area_List[2].Footer_Section != undefined
                    ) {
                      var pageDetailsTem = self.getAllMapData(
                        response.data.area_List[2].Footer_Section.component
                      );
                      self.footerDetails = pageDetailsTem;
                    }

                    console.log(self.headerDetails);
                    console.log(self.footerDetails);
                  }
                })
                .catch(function (error) {
                  console.log("Error");
                });
            }
          });
        });
    },
    getAllMapData: function (contentArry) {
      var tempDataObject = {};
      if (contentArry != undefined) {
        contentArry.map(function (item) {
          let allKeys = Object.keys(item);
          for (let j = 0; j < allKeys.length; j++) {
            let key = allKeys[j];
            let value = item[key];
            if (key != "name" && key != "type") {
              if (value == undefined || value == null) {
                value = "";
              }
              tempDataObject[key] = value;
            }
          }
        });
      }
      return tempDataObject;
    },
  },
  updated: function () {},
  mounted: function () {
    this.getPagecontent();
  },
  watch: {
    commonPageDetails: function () {},
  },
});
