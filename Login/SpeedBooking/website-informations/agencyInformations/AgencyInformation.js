﻿var AgencyInformation = {
    AirInformation: {
        homeUrl: "Flights/index.html",
        totalBookingingsHome: 5,
        AirFolderName: "Flights",

    },
    hotelService: {
        homeUrl: "Hotels/hotelsearch.html",
        totalBookingingsHome: 5,
    },
    sightseeingService: {
        homeUrl: "sightseeing/index.html",
        totalBookingingsHome: 5,
    },
    insuranceService: {
        homeUrl: "Insurance/index.html",
        totalBookingingsHome: 5,
    },
    transferService: {
        homeUrl: "Insurance/index.html",
        totalBookingingsHome: 5,
    },
    DomainUrl: "http://b2b.oneviewitsolutions.com",
    systemSettings: {
        systemDateFormat: 'dd M y,D',
        calendarDisplay: 2,
        calendarDisplayInMobile: 1
    },
}