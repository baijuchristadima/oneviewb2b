(function($) { 
  $(".forgot").click(function () {
    $(".login-form1").hide();
    $(".forgot_form").show();
});
$(".back").click(function () {
    $(".login-form1").show();
    $(".forgot_form").hide();
});
/*--Back-to-top--*/ 
    $(document).ready(function(){
	
	//Check to see if the window is top if not then display button
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			$('.scrollToTop').fadeIn();
		} else {
			$('.scrollToTop').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('.scrollToTop').click(function(){
		$('html, body').animate({scrollTop : 0},800);
		return false;
	});
	
});

    
    
/*--nav-dropdown-hover--*/
$('ul.nav li.dropdown').hover(function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(200).slideDown(300);
    }, function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(200).slideUp(300);
    });
    
    
    
    
    
/*--------logo-Slider---------*/  
    //  $(document).ready(function() {

    //   $("#owl-demo-3").owlCarousel({
    //     autoplay:true,
    //     autoPlay : 8000,
    //     autoplayHoverPause:true, 
    //     stopOnHover : false,  
    //     items : 3,
    //     margin:10,  
    //     lazyLoad : true,
    //     navigation : true,
    //     itemsDesktop : [1199, 3],
    //     itemsDesktopSmall : [991,2],
    //       itemsTablet : [600, 1]
    //   });

    //      $( ".owl-prev").html('<i class="fa  fa-angle-left"></i>');
    //      $( ".owl-next").html('<i class="fa  fa-angle-right"></i>');
    // });
    



})(jQuery);
