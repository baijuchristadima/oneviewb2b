var HeaderComponent = Vue.component('headeritem', {
    template: `
  <div id="header">
    <div class="top">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-sm-12 col-md-12">
            <div class="contact-info">
              <div class="phone-number"> <span>Call Us :</span> <a :href="'tel:'+contact.PhoneNumber">{{contact.PhoneNumber}}</a> </div>
              <div class="email"> <span>Email :</span> <a :href="'mailto:'+contact.Email"> {{contact.Email}}</a> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <nav id="navbar-main" class="navbar navbar-default">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              <a class="navbar-brand" href="index.html"><img :src="menuData.Logo" alt=""></a> </div>
            <div class="collapse navbar-collapse navbar-right" id="myNavbar">
              <ul class="nav navbar-nav">
                <li @click="activate(1)" :class="{ active : active_er == 1 }"><a href="index.html">{{menuData.Home}}</a></li>
                <li @click="activate(2)" :class="{ active : active_er == 2 }"><a href="/Login/aldiwanya/about-us.html"">{{menuData.AboutUs}}</a></li>
                <li @click="activate(3)" :class="{ active : active_er == 3 }"><a href="/Login/aldiwanya/contact-us.html">{{menuData.Contact}}</a></li>
                <li @click="activate(4)" :class="{ active : active_er == 4 }" v-if="checkMobileOrNot"><a href="/Login/aldiwanya/terms.html"">{{menuData.Terms}}</a></li>
                <li @click="activate(5)" :class="{ active : active_er == 5 }" v-if="checkMobileOrNot"><a href="/Login/aldiwanya/privacy.html"">{{menuData.Privacy}}</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </nav>
  </div>`,
    data() {
        return {

            agencyCode: '',
            contact: {Email:'',},
            active_er: (sessionStorage.active_er) ? sessionStorage.active_er : 1,
            menuData:{}

        }
    },
    computed:{
        checkMobileOrNot: function () {
          if (navigator.userAgent.match(/Android/i) ||
              navigator.userAgent.match(/webOS/i) ||
              navigator.userAgent.match(/iPhone/i) ||
              navigator.userAgent.match(/iPad/i) ||
              navigator.userAgent.match(/iPod/i) ||
              navigator.userAgent.match(/BlackBerry/i) ||
              navigator.userAgent.match(/Windows Phone/i)) {
              return true;
          }
          else { return false; }
      }
      },
    methods: {
        activate: function (el) {
            sessionStorage.active_er = el;
            this.active_er = el;
            if (el == 1 || el == 2) {
                maininstance.actvetab = el;
            }
            else {
                maininstance.actvetab = 1;
            }
        },
        getPageheader: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {

                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/02 Footer/02 Footer/02 Footer.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.data = response.data;
                            var contact = self.pluck('Contact_Details', self.data.area_List);
                            if (contact.length > 0) {
                                self.contact.Email = self.pluckcom('Email', contact[0].component);
                                self.contact.PhoneNumber = self.pluckcom('Phone_Number', contact[0].component);
                            }
                            var menuData = self.pluck('Footer_Labels', self.data.area_List);
                            if (menuData.length > 0) {
                            self.menuData.Home = self.pluckcom('Home_Label', menuData[0].component);
                            self.menuData.AboutUs = self.pluckcom('About_Us_Label', menuData[0].component);
                            self.menuData.Contact = self.pluckcom('Contact_Us_Label', menuData[0].component);
                            self.menuData.Privacy = self.pluckcom('Privacy_Policy_Label', menuData[0].component);
                            self.menuData.Terms = self.pluckcom('Terms_And_Condition_Label', menuData[0].component);
                            self.menuData.Logo = self.pluckcom('Logo_135_x_119px', menuData[0].component);
                            }
                        }).catch(function (error) {
                            console.log('Error');
                        });
                    }

                });

            });

        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
    },
    mounted: function () {

        this.getPageheader();
    },
})
var headerinstance = new Vue({
    el: 'header',
    name: 'headerArea',
    data() {
        return {
            key: '',
            content: null,
            getdata: true
        }

    },

});
Vue.component('footeritem', {
    template: `
    <div>
    <div class="container" v-if="checkMobileOrNot==false">
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
          <div class="ftr-cntc">
              <h3>{{FooterLabels.Get_In_Touch_Label}}</h3>
            <p>{{FooterLabels.Get_In_Touch_Description}}</p>
              <ul>
                  <li>
                    <i class="fa fa-map-marker fnt-sz-25" aria-hidden="true"></i>
                    {{ContactDetails.Address}}</li>
                  <li>
                    <i class="fa fa-envelope" aria-hidden="true"></i>
                    <a :href="'mailto:'+ContactDetails.Email">{{ContactDetails.Email}}</a></li>
                  <li>
                    <i class="fa fa-phone fnt-sz-22" aria-hidden="true"></i>
                    <a :href="'tel:'+ContactDetails.Phone_Number">{{ContactDetails.Phone_Number}}</a></li>
              </ul>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <div class="quick-links">
            <h3>{{FooterLabels.Quick_Links_Label}}</h3>
            <ul>
              <li><i class="fa  fa-angle-double-right"></i><a href="/Login/aldiwanya/index.html">{{FooterLabels.Home_Label}}</a></li>
              <li><i class="fa  fa-angle-double-right"></i><a href="/Login/aldiwanya/about-us.html">{{FooterLabels.About_Us_Label}}</a></li>
              <li><i class="fa  fa-angle-double-right"></i><a href="/Login/aldiwanya/contact-us.html">{{FooterLabels.Contact_Us_Label}}</a></li>
              <li><i class="fa  fa-angle-double-right"></i><a href="/Login/aldiwanya/terms.html">{{FooterLabels.Terms_And_Condition_Label}}</a></li>
              <li><i class="fa  fa-angle-double-right"></i><a href="/Login/aldiwanya/privacy.html">{{FooterLabels.Privacy_Policy_Label}}</a></li>
            </ul>
          </div>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
          <div class="newsletter">
            <h3>{{FooterLabels.Newsletter_Label}}</h3>
            <p>{{FooterLabels.Newsletter_Description}}</p>
            <div class="news-sub">
              <input type="text" id="text" name="text" placeholder="Enter your email address" v-model="newsltremail">
              <button type="submit" v-on:click="sendnewsletter"><i class="fa  fa-paper-plane"></i></button>
            </div>
            <div class="foot-social">
            <ul>
              <li v-for="item in SocialMedia.Social_Media_List"><a :href="item.URL" target="_blank"><i class="fa" v-bind:class="item.Icon"></i></a></li>
            </ul>
          </div>
          </div>
        </div>
      </div>
    </div>
    <div class="ftr-btm">
        <div class="container">
            <div class="reserved">
              <p>{{FooterLabels.Copyright_Content}}</p>
            </div>
            <div class="powered">
              <p>Powered by:</p>
              <a href="http://www.oneviewit.com/" target="_blank"><img src="images/oneview-logo.png" alt="logo"></a> </div>
        </div>
    </div>
    </div>`,
    data() {
        return {
            commonStore: vueCommonStore.state,
            agencyCode: '',
            FooterLabels:{},
            ContactDetails:{},
            SocialMedia:{},
            newsltremail:''

        }
    },
    computed:{
        checkMobileOrNot: function () {
          if (navigator.userAgent.match(/Android/i) ||
              navigator.userAgent.match(/webOS/i) ||
              navigator.userAgent.match(/iPhone/i) ||
              navigator.userAgent.match(/iPad/i) ||
              navigator.userAgent.match(/iPod/i) ||
              navigator.userAgent.match(/BlackBerry/i) ||
              navigator.userAgent.match(/Windows Phone/i)) {
              return true;
          }
          else { return false; }
      }
      },
    methods: {
        getPagefooter: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {

                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var Footer = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/02 Footer/02 Footer/02 Footer.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(Footer, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.content = response.data;
                            if (self.content != undefined && self.content != null) {
                                var mainData = self.pluck('Footer_Labels',self.content.area_List);
                                self.FooterLabels = self.getAllMapData(mainData[0].component);
            
                                var mainData = self.pluck('Contact_Details',self.content.area_List);
                                self.ContactDetails = self.getAllMapData(mainData[0].component);

                                var mainData = self.pluck('Social_Media',self.content.area_List);
                                self.SocialMedia = self.getAllMapData(mainData[0].component);
                              }
                            
                        }).catch(function (error) {
                            console.log('Error');
                            self.content = [];
                        });
                    }

                });

            });

        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        },
        sendnewsletter: async function () {

            if (!this.newsltremail) {
                alertify.alert('Alert', 'Email Id required.');
                return false;;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.newsltremail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.');
                return false;
            } else {
                var agencyCode = this.agencyCode
                var filterValue = "type='Newsletter' AND keyword1='" + this.newsltremail + "'";
                var allDBData = await this.getDbData4Table(agencyCode, filterValue, "date1");

                if (allDBData != undefined && allDBData.length > 0) {
                    alertify.alert('Alert', 'Email address already enabled.').set('closable', false);
                    return false;
                }
                else {
                    // var postData = {
                    //     type: "UserAddedRequest",
                    //     fromEmail: this.notificationEmail || this.commonStore.fallBackEmail,
                    //     toEmails: Array.isArray(this.newsltremail) ? this.newsltremail : [this.newsltremail],
                    //     logo: this.Logos.Logo || "",
                    //     agencyName: "Shams Abu Dhabi Travel",
                    //     agencyAddress: this.contact.Address || "",
                    //     personName: this.newsltremail.split("@")[0],
                    //     primaryColor: "#ffffff",
                    //     secondaryColor: "#364793"
                    // };
                    var agencyCode = this.agencyCode
                    let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                    let insertSubscibeData = {
                        type: "Newsletter",
                        date1: requestedDate,
                        keyword1: this.newsltremail,
                        nodeCode: agencyCode
                    };
                    let responseObject = await this.cmsRequestData("POST", "cms/data", insertSubscibeData, null);
                    try {
                        let insertID = Number(responseObject);
                        var self = this;
                        // mailUrl = self.commonStore.hubUrls.emailServices.emailApi;
                        // sendMailService(mailUrl, postData);
                        this.newsltremail = '';
                        alertify.alert('Newsletter', 'Thank you for subscribing !');
                    } catch (e) {

                    }
                }

            }
        },
        async getDbData4Table(agencyCode, extraFilter, sortField) {

            var allDBData = [];
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            var cmsURL = huburl + portno + '/cms/data/search/byQuery';
            var queryStr = "select * from cms_forms_data where nodeCode = '" + agencyCode + "'";
            if (extraFilter != undefined && extraFilter != '') {
                queryStr = queryStr + " AND " + extraFilter;
            }
            var requestObject = {
                query: queryStr,
                sortField: sortField,
                from: 0,
                orderBy: "desc"
            };
            let responseObject = await this.cmsRequestData("POST", "cms/data/search/byQuery", requestObject, null);
            if (responseObject != undefined && responseObject.data != undefined) {
                allDBData = responseObject.data;
            }
            return allDBData;

        }

    },
    mounted: function () {
        this.getPagefooter();
    },

})
var footerinstance = new Vue({
    el: 'footer',
    name: 'footerArea',
    data() {
        return {
            key: 0,
            content: null,
            getdata: true
        }

    },
});
