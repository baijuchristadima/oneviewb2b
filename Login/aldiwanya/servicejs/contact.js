var maininstance = new Vue({
    el: '#contactus',
    name: 'contactus',
    data: {
      commonStore: vueCommonStore.state,
      agencyCode: '',
      Banner:{},
      Contact:{},
      ContactForm:{},
      Header:{},
      cntusername: '',
      cntemail: '',
    cntcontact: '',
    cntsubject: '',
    cntmessage: '',
    isLoading: false,
    fullPage: true,

    },
    methods: {
      pluck(key, contentArry) {
        var Temparry = [];
        contentArry.map(function (item) {
          if (item[key] != undefined) {
            Temparry.push(item[key]);
          }
        });
        return Temparry;
      },
      pluckcom(key, contentArry) {
        var Temparry = [];
        contentArry.map(function (item) {
          if (item[key] != undefined) {
            Temparry = item[key];
          }
        });
        return Temparry;
      },
      getAllMapData: function (contentArry) {
        var tempDataObject = {};
        if (contentArry != undefined) {
          contentArry.map(function (item) {
            let allKeys = Object.keys(item)
            for (let j = 0; j < allKeys.length; j++) {
              let key = allKeys[j];
              let value = item[key];
              if (key != 'name' && key != 'type') {
                if (value == undefined || value == null) {
                  value = "";
                }
                tempDataObject[key] = value;
              }
            }
          });
        }
        return tempDataObject;
      },
      getPagecontent: function () {
        var self = this;
        self.isLoading = true;
        axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
          //console.log(document.location.hostname.toLowerCase());
  
          var agencyFolderName = '';
          var agy;
          try {
            response.data.forEach(function (agent, agentIndex) {
              agent.domain.forEach(function (dom, domIndex) {
                if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                  agy = agent;
                  agencyFolderName = agy.agencyFolderName;
                }
              });
            });
          } catch (err) { }
  
          agy.registeredUsers.forEach(function (agyCode, domIndex) {
            
            if (domIndex == 0) {
              var huburl = HubServiceUrls.hubConnection.cmsUrl;
              var portno = HubServiceUrls.hubConnection.ipAddress;
              var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
              var PageUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/06 Contact Us/06 Contact Us/06 Contact Us.ftl';
              var Url = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/01 Home/01 Home/01 Home.ftl';
              var agencyCode = agyCode;
              self.agencyCode = agencyCode;
              axios.get(PageUrl, {
                headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
              }).then(function (response) {
                console.log('responsee', response);
                self.content = response.data;
                if (self.content != undefined && self.content != null) {
                  var mainData = self.pluck('Banner_Section', self.content.area_List);
                  self.Banner = self.getAllMapData(mainData[0].component);
                  
                  var mainData = self.pluck('Contact_Details', self.content.area_List);
                  self.Contact = self.getAllMapData(mainData[0].component);

                  var mainData = self.pluck('Contact_Form', self.content.area_List);
                  self.ContactForm = self.getAllMapData(mainData[0].component);
                }
              });

              axios.get(Url, {
                headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
              }).then(function (response) {
                console.log('responsee', response);
                self.content = response.data;
                if (self.content != undefined && self.content != null) {
                  var mainData = self.pluck('Header', self.content.area_List);
                  self.Header = self.getAllMapData(mainData[0].component);
                }
                self.isLoading = false;
              }).catch(function (error) {
                console.log('Error');
                self.isLoading = false;
              });
              
            }
            
          });

        });
      },
      sendcontactus: async function () {
        if (!this.cntusername) {
          alertify.alert('Alert', 'Name required.').set('closable', false);
  
          return false;
        }
        if (!this.cntemail) {
          alertify.alert('Alert', 'Email Id required.').set('closable', false);
          return false;
        }
        var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        var matchArray = this.cntemail.match(emailPat);
        if (matchArray == null) {
          alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
          return false;
        }
        if (!this.cntcontact) {
          alertify.alert('Alert', 'Mobile number required.').set('closable', false);
          return false;
        }
        if (this.cntcontact.length < 8) {
          alertify.alert('Alert', 'Enter valid mobile number.').set('closable', false);
          return false;
        }
        if (!this.cntsubject) {
          alertify.alert('Alert', 'Subject required.').set('closable', false);
          return false;
        }
        if (!this.cntmessage) {
          alertify.alert('Alert', 'Message required.').set('closable', false);
          return false;
        } else {
            this.isLoading = true;
          var frommail = this.notificationEmail || this.commonStore.fallBackEmail;
          var custmail = {
            type: "UserAddedRequest",
            fromEmail: frommail,
            toEmails: Array.isArray(this.cntemail) ? this.cntemail : [this.cntemail],
            logo: this.ContactForm.Logo || "",
            agencyName: "Al-Diwanya Travel & Tourism",
            agencyAddress: this.Contact.Address || "",
            personName: this.cntusername || "",
            primaryColor: "#3c8044",
            secondaryColor: "#db6126"
          };
          let agencyCode = this.agencyCode;
          let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
          let insertContactData = {
            type: "Contact Us",
            keyword1: this.cntusername,
            keyword2: this.cntemail,
            keyword3: this.cntcontact,
            keyword4: this.cntsubject,
            text1: this.cntmessage,
            date1: requestedDate,
            nodeCode: agencyCode
          };
          let responseObject = await this.cmsRequestData("POST", "cms/data", insertContactData, null);
          try {
            let insertID = Number(responseObject);
            var self = this;
            var emailApi = self.commonStore.hubUrls.emailServices.emailApi;
            sendMailService(emailApi, custmail);
            this.cntemail = '';
            this.cntusername = '';
            this.cntcontact = '';
            this.cntmessage = '';
            this.cntsubject = '';
            this.isLoading = false;
            alertify.alert('Contact us', 'Thank you for contacting us.We shall get back to you.');
          } catch (e) {
  
          }
        }
      },
      async cmsRequestData(callMethod, urlParam, data, headerVal) {
        var huburl = HubServiceUrls.hubConnection.cmsUrl;
        var portno = HubServiceUrls.hubConnection.ipAddress;
        const url = huburl + portno + "/" + urlParam;
        if (data != null) {
          data = JSON.stringify(data);
        }
        const response = await fetch(url, {
          method: callMethod, // *GET, POST, PUT, DELETE, etc.
          credentials: "same-origin", // include, *same-origin, omit
          headers: { 'Content-Type': 'application/json' },
          body: data, // body data type must match "Content-Type" header
        });
        try {
          const myJson = await response.json();
          return myJson;
        } catch (error) {
          return object;
        }
      }
    },
    mounted: function () {
      sessionStorage.active_er = 3;
      this.getPagecontent();
    },
  })