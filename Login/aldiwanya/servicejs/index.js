var maininstance = new Vue({
  el: '#homepage',
  name: 'homepage',
  data: {
    commonStore: vueCommonStore.state,
    agencyCode: '',
    offer: [],
    homePageContent: [],
    OfferSection: '',
    flagForgot: false,
    isLoading: false,
    fullPage: true,
  },
  filters: {

    subStr: function (string) {
      if (string.length > 8)
        return string.substring(0, 9) + '...';

      else
        return string;
    }

  },
  methods: {
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getAllMapData: function (contentArry) {
      var tempDataObject = {};
      if (contentArry != undefined) {
        contentArry.map(function (item) {
          let allKeys = Object.keys(item)
          for (let j = 0; j < allKeys.length; j++) {
            let key = allKeys[j];
            let value = item[key];
            if (key != 'name' && key != 'type') {
              if (value == undefined || value == null) {
                value = "";
              }
              tempDataObject[key] = value;
            }
          }
        });
      }
      return tempDataObject;
    },
    getPagecontent: function () {
      var self = this;
      self.isLoading = true;
      axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
        //console.log(document.location.hostname.toLowerCase());

        var agencyFolderName = '';
        var agy;
        try {
          response.data.forEach(function (agent, agentIndex) {
            agent.domain.forEach(function (dom, domIndex) {
              if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                agy = agent;
                agencyFolderName = agy.agencyFolderName;
              }
            });
          });
        } catch (err) { }

        agy.registeredUsers.forEach(function (agyCode, domIndex) {
          if (domIndex == 0) {
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            var Packageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Master Table/Offer List/Offer List/Offer List.ftl';
            var homePageUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/01 Home/01 Home/01 Home.ftl';
            var agencyCode = agyCode;
            self.agencyCode = agencyCode;
            axios.get(homePageUrl, {
              headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
            }).then(function (response) {
              console.log('responsee', response);
              self.content = response.data;
              if (self.content != undefined && self.content != null) {
                var mainData = self.pluck('Main_Content', self.content.area_List);
                self.homePageContent = self.getAllMapData(mainData[0].component);

                var offer = self.pluck('Offers', self.content.area_List);
                self.OfferSection = self.getAllMapData(offer[0].component);
              }
            });
            axios.get(Packageurl, {
              headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
            }).then(function (response) {
              self.content = response.data;
              let offerTemp = [];
              if (self.content != undefined && self.content.Values != undefined) {
                offerTemp = self.content.Values.filter(function (el) {
                  return el.Is_Active == true
                });
              }

              self.offer = offerTemp;
              setTimeout(function () {
                carousel()
              }, 10);
              self.isLoading = false;
            }).catch(function (error) {
              console.log('Error');
              self.isLoading = false;
            });
          }
        });

      });
    },
  },
  mounted: function () {
    sessionStorage.active_er = 1;
    this.getPagecontent();
  },
});

function carousel() {
  $(document).ready(function () {

    $("#owl-demo-1").owlCarousel({
      items: 3,
      lazyLoad: true,
      loop: true,
      margin: 30,
      navigation: true,
      itemsDesktop: [991, 2],
      itemsDesktopSmall: [979, 2],
      itemsTablet: [768, 2],
      itemsMobile: [640, 1],
    });
    $(".owl-prev").html('<i class="fa fa-angle-left"></i>');
    $(".owl-next").html('<i class="fa fa-angle-right"></i>');

  });
}
