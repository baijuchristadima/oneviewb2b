var maininstance = new Vue({
    el: '#terms',
    name: 'terms',
    data: {
      commonStore: vueCommonStore.state,
      agencyCode: '',
      Terms:{},
      isLoading: false,
      fullPage: true,
    },
    methods: {
      pluck(key, contentArry) {
        var Temparry = [];
        contentArry.map(function (item) {
          if (item[key] != undefined) {
            Temparry.push(item[key]);
          }
        });
        return Temparry;
      },
      pluckcom(key, contentArry) {
        var Temparry = [];
        contentArry.map(function (item) {
          if (item[key] != undefined) {
            Temparry = item[key];
          }
        });
        return Temparry;
      },
      getAllMapData: function (contentArry) {
        var tempDataObject = {};
        if (contentArry != undefined) {
          contentArry.map(function (item) {
            let allKeys = Object.keys(item)
            for (let j = 0; j < allKeys.length; j++) {
              let key = allKeys[j];
              let value = item[key];
              if (key != 'name' && key != 'type') {
                if (value == undefined || value == null) {
                  value = "";
                }
                tempDataObject[key] = value;
              }
            }
          });
        }
        return tempDataObject;
      },
      getPagecontent: function () {
        var self = this;
        self.isLoading = true;
        axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
          //console.log(document.location.hostname.toLowerCase());
  
          var agencyFolderName = '';
          var agy;
          try {
            response.data.forEach(function (agent, agentIndex) {
              agent.domain.forEach(function (dom, domIndex) {
                if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                  agy = agent;
                  agencyFolderName = agy.agencyFolderName;
                }
              });
            });
          } catch (err) { }
  
          agy.registeredUsers.forEach(function (agyCode, domIndex) {
            if (domIndex == 0) {
              var huburl = HubServiceUrls.hubConnection.cmsUrl;
              var portno = HubServiceUrls.hubConnection.ipAddress;
              var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
              var homePageUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/04 Terms And Condition/04 Terms And Condition/04 Terms And Condition.ftl';
              var agencyCode = agyCode;
              self.agencyCode = agencyCode;
              axios.get(homePageUrl, {
                headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
              }).then(function (response) {
                console.log('responsee', response);
                self.content = response.data;
                if (self.content != undefined && self.content != null) {
                  var mainData = self.pluck('main', self.content.area_List);
                  self.Terms = self.getAllMapData(mainData[0].component);
                }
                self.isLoading = false;
              }).catch(function (error) {
                console.log('Error');
                self.isLoading = false;
              });
            }
  
          });
  
        });
      },
    },
    mounted: function () {
      sessionStorage.active_er = 4;
      this.getPagecontent();
    },
  })
  