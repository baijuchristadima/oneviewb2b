var contactUS = new Vue({
  el: '#contact',
  name: 'contact',
  data: {
    commonStore: vueCommonStore.state,
    agencyCode: '',
    BannerSection: {},
    FormSection: {},
    ContactSection: {},
    FormSection: {},
    MapSection: {},
    HeaderContent: {},
    cntusername: '',
    cntemail: '',
    cntcontact: '',
    cntsubject: '',
    cntmessage: '',
    BannerSection: '',
    FormSection: '',
    MapSection: '',
    BookSection: '',
    notificationEmail: ""
  },
  methods: {
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    pluckcom(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry = item[key];
        }
      });
      return Temparry;
    },
    getPagecontent: function () {
      var self = this;
      axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
        var agencyFolderName = '';
        var agy;
        try {
          response.data.forEach(function (agent, agentIndex) {
            agent.domain.forEach(function (dom, domIndex) {
              if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                agy = agent;
                agencyFolderName = agy.agencyFolderName;
              }
            });
          });
        } catch (err) { }

        agy.registeredUsers.forEach(function (agyCode, domIndex) {
          if (domIndex == 0) {
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            var agencyCode = agyCode;
            self.agencyCode = agencyCode;
            self.dir = langauage == "ar" ? "rtl" : "ltr";
            var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agencyCode + '/Template/Contact Us/Contact Us/Contact Us.ftl';
            axios.get(pageurl, {
              headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
            }).then(function (response) {
              self.content = response.data;
              var Notification = self.pluck('Notifications', response.data.area_List);
              if (Notification.length > 0) {
                self.notificationEmail = self.pluckcom('Notification_Email', Notification[0].component);
              }
              if (response.data.area_List.length) {
                var bannerDetails = self.pluck('Banner_Section', self.content.area_List);
                if (bannerDetails != undefined) {
                  self.BannerSection = self.getAllMapData(bannerDetails[0].component);
                }
                var ContactData = self.pluck('Contact_Us_Section', self.content.area_List);
                if (ContactData != undefined) {
                  self.ContactSection = self.getAllMapData(ContactData[0].component);
                }
                var Form_Section = self.pluck('Contact_Form_Section', self.content.area_List);
                if (Form_Section != undefined) {
                  self.FormSection = self.getAllMapData(Form_Section[0].component);
                }
                var Map = self.pluck('Map_Section', self.content.area_List);
                if (Map != undefined) {
                  self.MapSection = self.getAllMapData(Map[0].component);
                }
              }
            }).catch(function (error) {
              console.log('Error');
              self.BannerSection = [];
              self.ContactSection = [];
              self.FormSection = [];
              self.MapSection = [];
            });
            var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Header/Header/Header.ftl';
            axios.get(cmsPage, {
              headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
            }).then(function (response) {
              if (response.data.area_List.length) {
                var content = response.data;
                var Headers = self.pluck('Header', content.area_List);
                if (Headers != undefined) {
                  self.HeaderContent = self.getAllMapData(Headers[0].component);
                }
              }
            }).catch(function (error) {
              console.log('Error');
              self.HeaderContent = [];
            });
          }

        });

      });

    },
    getAllMapData: function (contentArry) {
      var tempDataObject = {};
      if (contentArry != undefined) {
        contentArry.map(function (item) {
          let allKeys = Object.keys(item)
          for (let j = 0; j < allKeys.length; j++) {
            let key = allKeys[j];
            let value = item[key];
            if (key != 'name' && key != 'type') {
              if (value == undefined || value == null) {
                value = "";
              }
              tempDataObject[key] = value;
            }
          }
        });
      }
      return tempDataObject;
    },
    sendcontactus: async function () {
      if (!this.cntusername) {
        alertify.alert('Alert', 'Name required.').set('closable', false);

        return false;
      }
      if (!this.cntemail) {
        alertify.alert('Alert', 'Email Id required.').set('closable', false);
        return false;
      }
      var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var matchArray = this.cntemail.match(emailPat);
      if (matchArray == null) {
        alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
        return false;
      }
      if (!this.cntcontact) {
        alertify.alert('Alert', 'Mobile number required.').set('closable', false);
        return false;
      }
      if (this.cntcontact.length < 8) {
        alertify.alert('Alert', 'Enter valid mobile number.').set('closable', false);
        return false;
      }
      if (!this.cntsubject) {
        alertify.alert('Alert', 'Subject required.').set('closable', false);
        return false;
      }
      if (!this.cntmessage) {
        alertify.alert('Alert', 'Message required.').set('closable', false);
        return false;
      } else {
        var frommail = this.notificationEmail || this.commonStore.fallBackEmail;
        var custmail = {
          type: "UserAddedRequest",
          fromEmail: frommail,
          toEmails: Array.isArray(this.cntemail) ? this.cntemail : [this.cntemail],
          logo: this.HeaderContent.Logo || "",
          agencyName: "byllwych Travel Tours",
          agencyAddress: this.ContactSection.Address || "",
          personName: this.cntusername || "",
          primaryColor: "#eb4b23",
          secondaryColor: "#050505"
        };
        let agencyCode = this.agencyCode;
        let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
        let insertContactData = {
          type: "User Request",
          keyword1: this.cntusername,
          keyword2: this.cntemail,
          text1: this.cntcontact,
          keyword3: this.cntsubject,
          keyword4: this.cntmessage,
          date1: requestedDate,
          nodeCode: agencyCode
        };
        let responseObject = await this.cmsRequestData("POST", "cms/data", insertContactData, null);
        try {
          let insertID = Number(responseObject);
          var self = this;
          var emailApi = self.commonStore.hubUrls.emailServices.emailApi;
          sendMailService(emailApi, custmail);
          this.cntemail = '';
          this.cntusername = '';
          this.cntcontact = '';
          this.cntmessage = '';
          this.cntsubject = '';
          alertify.alert('Contact us', 'Thank you for contacting us.We shall get back to you.');
        } catch (e) {

        }
      }
    },
    async cmsRequestData(callMethod, urlParam, data, headerVal) {
      var huburl = HubServiceUrls.hubConnection.cmsUrl;
      var portno = HubServiceUrls.hubConnection.ipAddress;
      const url = huburl + portno + "/" + urlParam;
      if (data != null) {
        data = JSON.stringify(data);
      }
      const response = await fetch(url, {
        method: callMethod, // *GET, POST, PUT, DELETE, etc.
        credentials: "same-origin", // include, *same-origin, omit
        headers: { 'Content-Type': 'application/json' },
        body: data, // body data type must match "Content-Type" header
      });
      try {
        const myJson = await response.json();
        return myJson;
      } catch (error) {
        return object;
      }
    },
  },
  mounted: function () {
    sessionStorage.active_er = 3;
    this.getPagecontent();
  },
});
