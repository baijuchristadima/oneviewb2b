var contactUS = new Vue({
  el: '#contact',
  name: 'contact',
  data: {
    commonStore: vueCommonStore.state,
    agencyCode: '',
    BannerSection: {},
    emailSection: {},
    ContactSection: {},
    FormSection: {},
    HeaderContent: {},

    cntusername: '',
    cntemail: '',
    cntcontact: '',
    cntsubject: '',
    cntmessage: '',
  },
  methods: {
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    getPagecontent: function () {
      var self = this;
      axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
        var agencyFolderName = '';
        var agy;
        try {
          response.data.forEach(function (agent, agentIndex) {
            agent.domain.forEach(function (dom, domIndex) {
              if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                agy = agent;
                agencyFolderName = agy.agencyFolderName;
              }
            });
          });
        } catch (err) { }

        agy.registeredUsers.forEach(function (agyCode, domIndex) {
          if (domIndex == 0) {
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            var agencyCode = agyCode;
            self.agencyCode = agencyCode;
            self.dir = langauage == "ar" ? "rtl" : "ltr";
            var pageurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agencyCode + '/Template/Contact Us/Contact Us/Contact Us.ftl';
            axios.get(pageurl, {
              headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
            }).then(function (response) {
              self.content = response.data;
             
              if (response.data.area_List.length) {
                var bannerDetails = self.pluck('Banner_Section', self.content.area_List);
                if (bannerDetails != undefined) {
                  self.BannerSection = self.getAllMapData(bannerDetails[0].component);
                }
                var ContactData = self.pluck('Page_Details', self.content.area_List);
                if (ContactData != undefined) {
                  self.ContactSection = self.getAllMapData(ContactData[0].component);
                }
                var Email_Section = self.pluck('Email_Section', self.content.area_List);
                if (Email_Section != undefined) {
                  self.emailSection = self.getAllMapData(Email_Section[0].component);
                }
               
              }
            }).catch(function (error) {
              console.log('Error');
              self.BannerSection = [];
              self.ContactSection = [];
              self.FormSection = [];
            });
          }

        });

      });

    },
    getAllMapData: function (contentArry) {
      var tempDataObject = {};
      if (contentArry != undefined) {
        contentArry.map(function (item) {
          let allKeys = Object.keys(item)
          for (let j = 0; j < allKeys.length; j++) {
            let key = allKeys[j];
            let value = item[key];
            if (key != 'name' && key != 'type') {
              if (value == undefined || value == null) {
                value = "";
              }
              tempDataObject[key] = value;
            }
          }
        });
      }
      return tempDataObject;
    },
    sendcontactus: async function () {
      if (!this.cntusername) {
        alertify.alert('Alert', 'Name required.').set('closable', false);

        return false;
      }
      if (!this.cntemail) {
        alertify.alert('Alert', 'Email Id required.').set('closable', false);
        return false;
      }
      var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var matchArray = this.cntemail.match(emailPat);
      if (matchArray == null) {
        alertify.alert('Alert', 'Your email address seems incorrect.').set('closable', false);
        return false;
      }
      if (!this.cntcontact) {
        alertify.alert('Alert', 'Mobile number required.').set('closable', false);
        return false;
      }
      if (this.cntcontact.length < 8) {
        alertify.alert('Alert', 'Enter valid mobile number.').set('closable', false);
        return false;
      }
      if (!this.cntsubject) {
        alertify.alert('Alert', 'Subject required.').set('closable', false);
        return false;
      }
      if (!this.cntmessage) {
        alertify.alert('Alert', 'Message required.').set('closable', false);
        return false;
      } else {
        var frommail = this.ContactSection.From_Mail || this.commonStore.fallBackEmail;
        var custmail = {
          type: "UserAddedRequest",
          fromEmail: frommail,
          toEmails: Array.isArray(this.cntemail) ? this.cntemail : [this.cntemail],
          logo: this.emailSection.Logo_240_x_60px || "",
          agencyName: this.emailSection.Agency_Name,
          agencyAddress: this.ContactSection.Address || "",
          personName: this.cntusername || "",
          primaryColor:this.emailSection.Primary_Color,
          secondaryColor:this.emailSection.Secondary_Color
        };
        let agencyCode = this.agencyCode;
        let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
        let insertContactData = {
          type: "Contact Us",
          keyword1: this.cntusername,
          keyword2: this.cntemail,
          keyword3: this.cntcontact,
          keyword4: this.cntsubject,
          text1: this.cntmessage,
          date1: requestedDate,
          nodeCode: agencyCode
        };
        let responseObject = await this.cmsRequestData("POST", "cms/data", insertContactData, null);
        try {
          let insertID = Number(responseObject);
          var self = this;
          var emailApi = self.commonStore.hubUrls.emailServices.emailApi;
          sendMailService(emailApi, custmail);
          this.cntemail = '';
          this.cntusername = '';
          this.cntcontact = '';
          this.cntmessage = '';
          this.cntsubject = '';
          alertify.alert('Contact us', 'Thank you for contacting us.We shall get back to you.');
        } catch (e) {

        }
      }
    },
    async cmsRequestData(callMethod, urlParam, data, headerVal) {
      var huburl = HubServiceUrls.hubConnection.cmsUrl;
      var portno = HubServiceUrls.hubConnection.ipAddress;
      const url = huburl + portno + "/" + urlParam;
      if (data != null) {
        data = JSON.stringify(data);
      }
      const response = await fetch(url, {
        method: callMethod, // *GET, POST, PUT, DELETE, etc.
        credentials: "same-origin", // include, *same-origin, omit
        headers: { 'Content-Type': 'application/json' },
        body: data, // body data type must match "Content-Type" header
      });
      try {
        const myJson = await response.json();
        return myJson;
      } catch (error) {
        return object;
      }
    },
  },
  mounted: function () {
    this.getPagecontent();
  },
});
