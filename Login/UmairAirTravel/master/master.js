var HeaderComponent = Vue.component('headeritem', {
    template: `
  <div id="header" v-cloak>
    <div class="top">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-sm-12 col-md-12">
            <div class="contact-info">
              <div class="phone-number"> <span>{{HeaderDetails.Call_Label}} :</span> <a :href="'tel:'+HeaderDetails.Phone_Number">{{ContactDetails.Phone_Number}}</a> </div>
              <div class="email"> <span>{{HeaderDetails.Email_Label}} :</span> <a :href="'mailto:'+HeaderDetails.Email">{{ContactDetails.Email}}</a> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <nav id="navbar-main" class="navbar navbar-default">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
              <a class="navbar-brand" href="index.html"><img :src="HeaderDetails.Header_Logo" alt=""></a> </div>
            <div class="collapse navbar-collapse navbar-right" id="myNavbar">
              <ul class="nav navbar-nav">
                <li :class="activePageName=='Home' ? 'active' :''"><a href="index.html">{{HeaderDetails.Home_Label}}</a></li>
                <li :class="activePageName=='about' ? 'active' :''"><a href="about-us.html">{{HeaderDetails.About_Label}}</a></li>
                <li :class="activePageName=='contact' ? 'active' :''"><a href="contact-us.html">{{HeaderDetails.Contact_Label}}</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </nav>
  </div>
  `,
    data() {
        return {

            agencyCode: '',
            commonStore: vueCommonStore.state,
            HeaderDetails: {},
            Menu: {},
            activePageName: '',
            ContactDetails:{},

        }
    },
    methods: {
        headerData: function () {
            var self = this;
            var url = window.location.href;
            if (url.includes('index.html')) {
                this.activePageName = "Home";
            } else if (url.includes('about-us.html')) {
                this.activePageName = "about";
            } else if (url.includes('contact-us.html')) {
                this.activePageName = "contact";
            } else {
                this.activePageName = "Home";
            }
        },
        getPageheader: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {

                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Master Page/Master Page/Master Page.ftl';
                        var contact = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Contact Us/Contact Us/Contact Us.ftl';
                        var agencyCode = agyCode;
                        self.agencyCode = agencyCode;
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            if (response.data.area_List.length) {
                                var content = response.data;
                                var headerData = self.pluck('Header_Section', content.area_List);
                                if (headerData != undefined) {
                                    self.HeaderDetails = self.getAllMapData(headerData[0].component);
                                }
                                
                            }
                        }).catch(function (error) {
                            console.log('Error');
                            self.HeaderContent = [];
                            self.Menu = [];
                        });

                        axios.get(contact, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            if (response.data.area_List.length) {
                                var content = response.data;
                                var contactData = self.pluck('Page_Details', content.area_List);
                                if (contactData != undefined) {
                                    self.ContactDetails = self.getAllMapData(contactData[0].component);
                                }
                                
                            }
                        }).catch(function (error) {
                            console.log('Error');
                            self.HeaderContent = [];
                            self.Menu = [];
                        });

                    }

                });

            });

        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },
    },
    mounted: function () {
        this.getPageheader();
        this.headerData();
    },
})
var headerinstance = new Vue({
    el: 'header',
    name: 'headerArea',
    data() {
        return {
            key: '',
            content: null,
            getdata: true
        }

    },

});
Vue.component('footeritem', {
    template: `
    <div class="footer" v-cloak>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="ftr-cntc">
                    <h3>{{FooterContent.Get_In_Touch_Label}}</h3>
                    <p>{{FooterContent.Small_Description}}</p>
                    <ul>
                        <li>
                            <i class="fa fa-map-marker fnt-sz-25" aria-hidden="true"></i>
                            {{contact.Address}}</li>
                        <li>
                            <i class="fa fa-envelope" aria-hidden="true"></i>
                            <a :href="'mailto:'+contact.Email">{{contact.Email}}</a></li>
                        <li>
                            <i class="fa fa-phone fnt-sz-22" aria-hidden="true"></i>
                            <a :href="'tel:'+contact.Phone_Number">{{contact.Phone_Number}}</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="quick-links">
                    <h3>{{FooterContent.Quick_Link__Label_}}</h3>
                    <ul>
                        <li><i class="fa  fa-angle-double-right"></i><a href="index.html">{{HeaderContents.Home_Label}}</a></li>
                        <li><i class="fa  fa-angle-double-right"></i><a href="about-us.html">{{HeaderContents.About_Label}}</a></li>
                        <li><i class="fa  fa-angle-double-right"></i><a href="contact-us.html">{{HeaderContents.Contact_Label}}</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <div class="newsletter">
                    <h3>{{FooterContent.Newsletter_Label}}</h3>
                    <p>{{FooterContent.Newsletter_Description}}</p>
                    <div class="news-sub">
                        <input type="text" id="text" v-model="newsltremail"
                            :placeholder="FooterContent.Newsletter_Placeholder">
                        <button type="submit" v-on:click="sendnewsletter"><i class="fa  fa-paper-plane"></i></button>
                    </div>
                    <div class="foot-social">
                        <ul>
                            <li v-for ="item in FooterContent.Social_Media_List"><a :href="item.Url" target="_blank"><i :class="item.Icon"></i></a></li>
                            
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
    <div class="all-reserved">
    <div class="reserved">
        <p>{{FooterContent.Copyright_Content}}</p>
    </div>
   <!-- <div class="powered">
        <p>Powered by:</p>
        <a href="http://www.oneviewit.com/" target="_blank"><img src="images/oneview-logo.png" alt="logo"></a>
    </div> -->
    </div>
    </div>
    </div>


  `,
    data() {
        return {
            commonStore: vueCommonStore.state,
            agencyCode: '',
            ContactData: {},
            newsltremail: null,
            FooterContent: {},
            emailSection: {},
            HeaderContents:{},
            contact:{}
        }
    },
    methods: {
        getPagefooter: function () {
            var self = this;
            axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {

                //console.log(document.location.hostname.toLowerCase());

                var agencyFolderName = '';
                var agy;
                try {
                    response.data.forEach(function (agent, agentIndex) {
                        agent.domain.forEach(function (dom, domIndex) {
                            if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                                agy = agent;
                                agencyFolderName = agy.agencyFolderName;
                            }
                        });
                    });
                } catch (err) { }

                agy.registeredUsers.forEach(function (agyCode, domIndex) {
                    
                    if (domIndex == 0) {
                        var huburl = HubServiceUrls.hubConnection.cmsUrl;
                        var portno = HubServiceUrls.hubConnection.ipAddress;
                        var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
                        var contacturl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Contact Us/Contact Us/Contact Us.ftl';
                        // var Headerurl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Header/Header/Header.ftl';
                        var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Master Page/Master Page/Master Page.ftl';
                        self.agencyCode = agyCode;
                        axios.get(cmsPage, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            var content = response.data;
                            if (response.data.area_List.length) {
                                var Footers = self.pluck('Footer_Section', content.area_List);
                                if (Footers != undefined) {
                                    var FootersTemp = self.getAllMapData(Footers[0].component);
                                    self.FooterContent = FootersTemp;
                                }

                                var header = self.pluck('Header_Section', content.area_List);
                                if (header != undefined) {
                                    var HeaderTemp = self.getAllMapData(header[0].component);
                                    self.HeaderContents = HeaderTemp;
                                }
                            }
                        }).catch(function (error) {
                            console.log('Error');
                            self.footer = [];
                        });
                        axios.get(contacturl, {
                            headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
                        }).then(function (response) {
                            self.content = response.data;
                            if (response.data.area_List.length) {
                                var contact = self.pluck('Page_Details', self.content.area_List);
                                if (contact != undefined) {
                                    var contactTemp = self.getAllMapData(contact[0].component);
                                    self.contact = contactTemp;
                                }
                                var Email_Section = self.pluck('Email_Section', self.content.area_List);
                                    if (Email_Section != undefined) {
                                        self.emailSection = self.getAllMapData(Email_Section[0].component);
                                    }
                            }
                        }).catch(function (error) {
                            console.log('Error');
                            self.footer = [];
                        });
                    }

                });

            });

        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        async cmsRequestData(callMethod, urlParam, data, headerVal) {
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            const url = huburl + portno + "/" + urlParam;
            if (data != null) {
                data = JSON.stringify(data);
            }
            const response = await fetch(url, {
                method: callMethod, // *GET, POST, PUT, DELETE, etc.
                credentials: "same-origin", // include, *same-origin, omit
                headers: { 'Content-Type': 'application/json' },
                body: data, // body data type must match "Content-Type" header
            });
            try {
                const myJson = await response.json();
                return myJson;
            } catch (error) {
                return object;
            }
        },
        sendnewsletter: async function () {

            if (!this.newsltremail) {
                alertify.alert('Alert', 'Email Id required.');
                return false;;
            }
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = this.newsltremail.match(emailPat);
            if (matchArray == null) {
                alertify.alert('Alert', 'Your email address seems incorrect.');
                return false;
            } else {
                var agencyCode = this.agencyCode
                var filterValue = "type='Newsletter' AND keyword1='" + this.newsltremail + "'";
                var allDBData = await this.getDbData4Table(agencyCode, filterValue, "date1");

                if (allDBData != undefined && allDBData.length > 0) {
                    alertify.alert('Alert', 'Email address already enabled.').set('closable', false);
                    return false;
                }
                else {
                    var postData = {
                        type: "UserAddedRequest",
                        fromEmail: this.contact.From_Mail || this.commonStore.fallBackEmail,
                        toEmails: Array.isArray(this.newsltremail) ? this.newsltremail : [this.newsltremail],
                        logo: this.emailSection.Logo_240_x_60px || "",
                        agencyName: this.emailSection.Agency_Name,
                        agencyAddress: this.contact.Addres || "",
                        personName: this.newsltremail.split("@")[0],
                        primaryColor: this.emailSection.Primary_Color,
                        secondaryColor: this.emailSection.Secondary_Color
                    };
                    var agencyCode = this.agencyCode
                    let requestedDate = moment(String(new Date())).format('YYYY-MM-DDThh:mm:ss');
                    let insertSubscibeData = {
                        type: "Newsletter",
                        date1: requestedDate,
                        keyword1: this.newsltremail,
                        nodeCode: agencyCode
                    };
                    let responseObject = await this.cmsRequestData("POST", "cms/data", insertSubscibeData, null);
                    try {
                        let insertID = Number(responseObject);
                        var self = this;
                        mailUrl = self.commonStore.hubUrls.emailServices.emailApi;
                        sendMailService(mailUrl, postData);
                        this.newsltremail = '';
                        alertify.alert('Newsletter', 'Thank you for subscribing !');
                    } catch (e) {

                    }
                }

            }
        },
        async getDbData4Table(agencyCode, extraFilter, sortField) {

            var allDBData = [];
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            var cmsURL = huburl + portno + '/cms/data/search/byQuery';
            var queryStr = "select * from cms_forms_data where nodeCode = '" + agencyCode + "'";
            if (extraFilter != undefined && extraFilter != '') {
                queryStr = queryStr + " AND " + extraFilter;
            }
            var requestObject = {
                query: queryStr,
                sortField: sortField,
                from: 0,
                orderBy: "desc"
            };
            let responseObject = await this.cmsRequestData("POST", "cms/data/search/byQuery", requestObject, null);
            if (responseObject != undefined && responseObject.data != undefined) {
                allDBData = responseObject.data;
            }
            return allDBData;

        },
        getAllMapData: function (contentArry) {
            var tempDataObject = {};
            if (contentArry != undefined) {
                contentArry.map(function (item) {
                    let allKeys = Object.keys(item)
                    for (let j = 0; j < allKeys.length; j++) {
                        let key = allKeys[j];
                        let value = item[key];
                        if (key != 'name' && key != 'type') {
                            if (value == undefined || value == null) {
                                value = "";
                            }
                            tempDataObject[key] = value;
                        }
                    }
                });
            }
            return tempDataObject;
        },

    },
    mounted: function () {
        this.getPagefooter();
    },

})
var footerinstance = new Vue({
    el: 'footer',
    name: 'footerArea',
    data() {
        return {
            key: 0,
            content: null,
            getdata: true
        }

    },
});
