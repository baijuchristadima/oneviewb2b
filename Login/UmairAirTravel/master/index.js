var maininstance = new Vue({
  el: '#homepage',
  name: 'homepage',
  data: {
    commonStore: vueCommonStore.state,
    agencyCode: '',
    homeDetails: {},
    content: '',
    bannerImage: null,
    loginLabels:''
  },
  methods: {
    pluck(key, contentArry) {
      var Temparry = [];
      contentArry.map(function (item) {
        if (item[key] != undefined) {
          Temparry.push(item[key]);
        }
      });
      return Temparry;
    },
    getPagecontent: function () {
      var self = this;
      axios.get("/Resources/AgencyInformations/AgencyInformation.json").then(function (response) {
        var agencyFolderName = '';
        var agy;
        try {
          response.data.forEach(function (agent, agentIndex) {
            agent.domain.forEach(function (dom, domIndex) {
              if (dom.toLowerCase() == document.location.hostname.toLowerCase()) {
                agy = agent;
                agencyFolderName = agy.agencyFolderName;
              }
            });
          });
        } catch (err) { }

        agy.registeredUsers.forEach(function (agyCode, domIndex) {
          if (domIndex == 0) {
            var huburl = HubServiceUrls.hubConnection.cmsUrl;
            var portno = HubServiceUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            var homePageUrl = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + agyCode + '/Template/Home Page/Home Page/Home Page.ftl';
            var agencyCode = agyCode;
            self.agencyCode = agencyCode;
          
            axios.get(homePageUrl, {
                headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
              }).then(function (response) {
                self.content = response.data;
                if (self.content != undefined && self.content != null) {
                  var bannerData = self.pluck('Page_Details', self.content.area_List);
                  if (bannerData.length > 0) {
                      self.homeDetails = self.getAllMapData(bannerData[0].component);
                      self.bannerImage = self.homeDetails.Banner_Image;
                  }
                  var loginData = self.pluck('Log_In_Area', self.content.area_List);
                  if (loginData.length > 0) {
                      self.loginLabels = self.getAllMapData(loginData[0].component);
                    
                  }
                }
              });
          }

        });

      });

    },
    getAllMapData: function (contentArry) {
      var tempDataObject = {};
      if (contentArry != undefined) {
          contentArry.map(function (item) {
              let allKeys = Object.keys(item)
              for (let j = 0; j < allKeys.length; j++) {
                  let key = allKeys[j];
                  let value = item[key];
                  if (key != 'name' && key != 'type') {
                      if (value == undefined || value == null) {
                          value = "";
                      }
                      tempDataObject[key] = value;
                  }
              }
          });
      }
      return tempDataObject;
  }
  },
  mounted: function () {
    this.getPagecontent();
  },
});
