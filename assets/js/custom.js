/*owl carousel start */


/*owl carousel end */


/*-----------------------------------------*/

/*Smooth scrolling start */

$(document).ready(function () {
    // Select all links with hashes
    $('a[href*="#"]')
        // Remove links that don't actually link to anything
        .not('[href="#"]')
        .not('[href="#0"]')
        .click(function (event) {
            // On-page links
            if (
                location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
                location.hostname == this.hostname
            ) {
                // Figure out element to scroll to
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                // Does a scroll target exist?
                if (target.length) {
                    // Only prevent default if animation is actually gonna happen
                    event.preventDefault();
                    $('html, body').animate({
                        scrollTop: target.offset().top
                    }, 1000, function () {
                        // Callback after animation
                        // Must change focus!
                        var $target = $(target);
                        $target.focus();
                        if ($target.is(":focus")) { // Checking if the target was focused
                            return false;
                        } else {
                            $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                            $target.focus(); // Set focus again
                        };
                    });
                }
            }
        });
    $(".forgot_click").click(function () {
        $(".login_form").hide();
        $(".forgot_form").show();
    });
    $(".back").click(function () {
        $(".login_form").show();
        $(".forgot_form").hide();
    });
});
/*Smooth scrolling end */
$(window).load(function () {
    var viewportWidth = $(window).width();
    if (viewportWidth < 768) {
        $("body").removeClass("menuadd").addClass("pushmenu-push");
    }
});
/*-----------------------------------------*/


/*Wow js start */
$(function () {
    new WOW().init();

});

/*Wow js end */

