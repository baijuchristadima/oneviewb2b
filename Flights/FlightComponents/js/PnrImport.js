﻿Vue.component("pnrinfo-component", {
    mixins: [websocketMixin],
    data: function() {
        return {
            //get roles from common store
            commonStore: vueCommonStore.state,
            commonAgencyCode: 'AGY75',
            supplier: {
                selected: '',
                suppliers: []
            },
            PnrNumber: '',
            paxLastName: '',
            myInterval: '',
            timer: '',
            showLoader: true,
            showBtnPnr: false,
            Import_PNR: '',
            Supplier_Label: '',
            PNR_Label: '',
            Btn_Label: '',
            Alert_Heading_Warning: '',
            Alert_Heading_Fill: '',
            Alert_Msg_Results: '',
            Alert_Msg_Enter_Pnr: '',
            Ok_Label: '',
            pnrShowLoader: false,
            Alert_Error_Label: '',
            Tech_Diff_alert_Msg: '',
            allSupplierContents: {},
            selectedDescription: '',
            availableSuppliers: [],
            isSending: false
        };
    },
    created: function() {
        var self = this;
        self.timer = setInterval(function() {
            if (self.commonStore.commonRoles.hasSetPrivileges) {
                if (!self.commonStore.commonRoles.hasAirPnrImport) {
                    window.location.href = '/search.html';
                }
                clearInterval(self.timer);
            }
        }, 800);
        self.myInterval = setInterval(this.setSuppliers(), 2000);
        this.getPagecontent();
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function(item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        setSuppliers: function() {
            var self = this;
            try {
                var agencyNode = window.localStorage.getItem("agencyNode");
                if (agencyNode) {
                    agencyNode = JSON.parse(atob(agencyNode));
                    if (!isNullorUndefind(agencyNode)) {
                        try {
                            self.supplier.suppliers = agencyNode.loginNode.servicesList.find(x => x.name == 'Air').provider;
                            console.log(self.supplier.suppliers);
                            self.showLoader = false;
                            self.showBtnPnr = true;

                        } catch (err) {
                            console.log('end supplier list');
                        }
                    }
                }
                clearInterval(self.myInterval);
            } catch (err) {}
        },
        airPnrImport: function() {
            var self = this;
            if (self.PnrNumber == '') {
                alertify.alert(self.Alert_Heading_Fill, self.Alert_Msg_Enter_Pnr);
                return false;
            }
            if (self.paxLastName == '' && [60].indexOf(self.supplier.selected) != -1) {
                alertify.alert(self.Alert_Heading_Fill, "Last Name is required.");
                return false;
            }
            this.isSending = true;
            self.pnrShowLoader = true;
            var PostData = {
                request: {
                    service: "FlightRQ",
                    content: {
                        command: "FlightPnrImportRQ",
                        supplierSpecific: {},
                        tripDetailRQ: {
                            uniqueId: self.PnrNumber,
                            paxLastName: self.paxLastName ? self.paxLastName : undefined
                        }
                    },
                    supplierCodes: [self.supplier.selected]
                }
            }

            var vm = this;
            var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = vm.commonStore.hubUrls.hubConnection.hubServices.flights.pnrImport;
            console.log('Pnr Req: ', JSON.stringify(PostData));

            var config = {
                axiosConfig: {
                    method: "POST",
                    url: hubUrl + serviceUrl,
                    data: PostData
                },
                successCallback: function(response) {
                    console.log("Pnr Res: ", response);
                    try {
                        if (response.data.response.content.tripDetailRS != undefined &&
                            response.data.response.content.tripDetailRS.bookingRefId != undefined &&
                            response.data.response.selectCredential != undefined) {
                            localStorage.access_token = response.headers.access_token;
                            window.sessionStorage.setItem("flightbookingId", response.data.response.content.tripDetailRS.bookingRefId);
                            window.sessionStorage.setItem("selectCredential", JSON.stringify(response.data.response.selectCredential));
                            window.sessionStorage.setItem("sendbookmail", true);
                            window.location.href = '/Flights/bookinginfo.html';
                            self.isSending = false;
                        } else {
                            self.isSending = false;
                            self.pnrShowLoader = false;
                            alertify.alert(self.Alert_Error_Label, self.Tech_Diff_alert_Msg).set('label', self.Ok_Label);
                        }
                    } catch (err) {
                        self.pnrShowLoader = false;
                        self.isSending = false;
                        alertify.alert(self.Alert_Error_Label, self.Tech_Diff_alert_Msg).set('label', self.Ok_Label);
                    }
                },
                errorCallback: function(error) {
                    self.pnrShowLoader = false;
                    self.isSending = false;
                    //   try { console.log('PNR Error: ' + JSON.stringify(err.response.data)); } catch (err) { }
                    //   self.pnrShowLoader = false;
                    //   try {
                    //       if (err.response.data.code == "400") {
                    //           alertify.alert(self.Alert_Heading_Warning, err.response.data.message).set('label', self.Ok_Label);
                    //       } else {
                    //           alertify.alert(self.Alert_Heading_Warning, self.Alert_Msg_Results).set('label', self.Ok_Label);
                    //       }
                    //   } catch (error) {
                    //       alertify.alert(self.Alert_Heading_Warning, self.Alert_Msg_Results).set('label', self.Ok_Label);
                    //   }
                },
                showAlert: true
            }

            mainAxiosRequest(config);

        },
        changeSupplier: function() {
            this.selectedDescription = '';
            var self = this;
            if (this.allSupplierContents != undefined && this.supplier.selected != undefined) {
                var supplierID = self.supplier.selected;
                var supplierName = '';
                this.supplier.suppliers.forEach(function(item) {
                    if (item.id == self.supplier.selected) {
                        supplierName = item.name;
                    }
                });
                for (var index = 0; index < this.allSupplierContents.length; index++) {
                    const element = this.allSupplierContents[index];
                    if (element.Supplier_Code == supplierID || element.Name.toLowerCase() == supplierName.toLowerCase()) {
                        this.selectedDescription = element.Description;
                        break;
                    }

                }

            }
        },
        getPagecontent: function() {
            var self = this;
            var vm = this;
            var huburl = vm.commonStore.hubUrls.hubConnection.cmsUrl;
            var portno = vm.commonStore.hubUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            //var langauage = 'ar';
            self.dir = langauage == "ar" ? "rtl" : "ltr";

            var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + self.commonAgencyCode + '/Template/Flight PNR Import/Flight PNR Import/Flight PNR Import.ftl';
            axios.get(cmsPage, {
                headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
            }).then(function(response) {
                self.content = response.data;
                if (response.data.area_List.length > 0) {
                    var pnrComp = self.pluckcom('PNR_Import', self.content.area_List); // response.data.area_List[1].Trip_Section;
                    self.Import_PNR = self.pluckcom('Import_PNR', pnrComp.component);
                    self.Supplier_Label = self.pluckcom('Supplier_Label', pnrComp.component);
                    self.PNR_Label = self.pluckcom('PNR_Label', pnrComp.component);
                    self.Btn_Label = self.pluckcom('Btn_Label', pnrComp.component);
                    self.Alert_Heading_Warning = self.pluckcom('Alert_Heading_Warning', pnrComp.component);
                    self.Alert_Heading_Fill = self.pluckcom('Alert_Heading_Fill', pnrComp.component);
                    self.Alert_Msg_Results = self.pluckcom('Alert_Msg_Results', pnrComp.component);
                    self.Alert_Msg_Enter_Pnr = self.pluckcom('Alert_Msg_Enter_Pnr', pnrComp.component);
                    self.Ok_Label = self.pluckcom('Ok_Label', pnrComp.component);
                    self.Tech_Diff_alert_Msg = self.pluckcom('Tech_Diff_alert_Msg', pnrComp.component);
                    self.Alert_Error_Label = self.pluckcom('Alert_Error_Label', pnrComp.component);

                }
            }).catch(function(error) {
                console.log(error);
            });

            var cmsPagePnrImport = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/AGY' + self.commonStore.agencyNode.loginNode.solutionId + '/Template/Flight PNR Import/Flight PNR Import/Flight PNR Import.ftl';
            axios.get(cmsPagePnrImport, {
                headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
            }).then(function(response) {
                if (response.data.area_List.length > 0) {
                    var Supplier_Details = self.pluckcom('Supplier_Details', response.data.area_List);
                    self.allSupplierContents = self.pluckcom('Supplier_Description', Supplier_Details.component);
                    self.changeSupplier();

                }
            }).catch(function(error) {
                console.log(error);
            });
        }
    },
    mounted: function() {
        var tempSupplier = [];
        this.availableSuppliers = [{
                id: 1,
                name: "Amadeus"
            },
            {
                id: 45,
                name: "TravelPort"
            },
            {
                id: 55,
                name: "Sabre"
            },
            {
                id: 57,
                name: "Pia - Hitit"
            },
            {
                id: 58,
                name: "Airblue"
            },
            {
                id: 60,
                name: "SereneAir"
            },
            {
                id: 74,
                name: "AirSial"
            },
            {
                id: 87,
                name: "Airblue-WEB"
            },
        ];
        for (var i = 0; i < this.availableSuppliers.length; i++) {
            for (var index = 0; index < this.supplier.suppliers.length; index++) {
                if (this.supplier.suppliers[index].name.toLowerCase() == this.availableSuppliers[i].name.toLowerCase() &&
                    this.supplier.suppliers[index].supplierType.toLowerCase() != "no credentials added") {
                    tempSupplier.push(this.supplier.suppliers[index]);
                } else {}
            }
        }
        this.supplier.suppliers = _.intersection(this.supplier.suppliers, tempSupplier);
        this.supplier.selected = this.supplier.suppliers[0].id;

    }
});