﻿Vue.component("passportdetails-component", {
    data: function () {
        return {
            //get roles from common store
            commonStore: vueCommonStore.state,
            commonAgencyCode: 'AGY75',
            showLoader: true,
            country: [],
            pnr: '',
            bookingRefId: '',
            selectCredential: '',
            supplierCode: '',
            traveller: {},
            sendingRequest: false,
            sessionId: '',
            supplierSpecific: null
        };
    },
    created: function () {
        var self = this;
        self.getPagecontent();
    },
    methods: {
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            var huburl = self.commonStore.hubUrls.hubConnection.cmsUrl;
            var portno = self.commonStore.hubUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            //var langauage = 'ar';
            self.dir = langauage == "ar" ? "rtl" : "ltr";

            var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + self.commonAgencyCode + '/Template/Flight PNR Import/Flight PNR Import/Flight PNR Import.ftl';
            axios.get(cmsPage, {
                headers: {
                    'content-type': 'text/html',
                    'Accept': 'text/html',
                    'Accept-Language': langauage
                }
            }).then(function (response) {
                self.content = response.data;
                if (response.data.area_List.length > 0) {
                    var pnrComp = self.pluckcom('PNR_Import', self.content.area_List); // response.data.area_List[1].Trip_Section;
                    self.Import_PNR = self.pluckcom('Import_PNR', pnrComp.component);
                    self.Supplier_Label = self.pluckcom('Supplier_Label', pnrComp.component);
                    self.PNR_Label = self.pluckcom('PNR_Label', pnrComp.component);
                    self.Btn_Label = self.pluckcom('Btn_Label', pnrComp.component);
                    self.Alert_Heading_Warning = self.pluckcom('Alert_Heading_Warning', pnrComp.component);
                    self.Alert_Heading_Fill = self.pluckcom('Alert_Heading_Fill', pnrComp.component);
                    self.Alert_Msg_Results = self.pluckcom('Alert_Msg_Results', pnrComp.component);
                    self.Alert_Msg_Enter_Pnr = self.pluckcom('Alert_Msg_Enter_Pnr', pnrComp.component);
                    self.Ok_Label = self.pluckcom('Ok_Label', pnrComp.component);
                    self.Tech_Diff_alert_Msg = self.pluckcom('Tech_Diff_alert_Msg', pnrComp.component);
                    self.Alert_Error_Label = self.pluckcom('Alert_Error_Label', pnrComp.component);

                }
            }).catch(function (error) {
                console.log(error);
            });

        },
        getMaxDate: function (dateObject) {
            var dateObjectsplit = $(dateObject).attr('tag').split('_');
            var endDate = new Date();
            switch (dateObjectsplit[0]) {
                case 'dob':
                    adddays = 0;
                    try {
                        endDate = new Date(this.selectFlightFromSession.selectedFlightInfo.groupOfFlights[0].flightDetails[0].fInfo.dateTime.dateTimeFormatedDepDate1);
                    } catch (error) {
                        endDate = new Date();
                    }
                    endDate.setDate(endDate.getDate() + adddays);
                    switch (dateObjectsplit[1]) {
                        case 'ADT':
                            addyears = -12;
                            endDate.setFullYear(endDate.getFullYear() + addyears);
                            break;
                        case 'CHD':
                            // addyears = -2;
                            // endDate.setFullYear(endDate.getFullYear() + addyears);
                            endDate = new Date();
                            break;
                        case 'INF':
                            // endDate.setFullYear(endDate.getFullYear() + addyears);
                            endDate = new Date();
                            break;
                    }
                    $(dateObject).datepicker('option', 'maxDate', new Date(endDate));
                    break;
                case 'exd':
                    break;
                case 'isd':
                    adddays = -1;
                    endDate.setDate(endDate.getDate() + adddays);
                    $(dateObject).datepicker('option', 'maxDate', endDate);
                    break;
            }
            return endDate;
        },
        getMinDate: function (dateObject) {
            var dateObjectsplit = $(dateObject).attr('tag').split('_');
            var txtDob = $('#txtDob' + dateObjectsplit[1] + dateObjectsplit[2]);
            var startDate = new Date();
            switch (dateObjectsplit[0]) {
                case 'dob':
                    startDate = new Date();
                    switch (dateObjectsplit[1]) {
                        case 'ADT':
                            addyears = -120;
                            break;
                        case 'CHD':
                            addyears = -12;
                            break;
                        case 'INF':
                            addyears = -2;
                            break;
                    }
                    startDate.setFullYear(startDate.getFullYear() + addyears);
                    break;
                case 'exd':
                    switch (dateObjectsplit[1]) {
                        case 'ADT':
                            adddays = 1;
                            startDate.setDate(startDate.getDate() + adddays);
                            break;
                        case 'CHD':
                            adddays = 1;
                            startDate.setDate(startDate.getDate() + adddays);
                            break;
                        case 'INF':
                            adddays = 1;
                            startDate.setDate(startDate.getDate() + adddays);
                            break;
                    }
                    break;
                case 'isd':
                    switch (dateObjectsplit[1]) {
                        case 'ADT':
                            if (isNullorUndefined($(txtDob).val())) {
                                addyears = -100;
                                startDate.setFullYear(startDate.getFullYear() + addyears);
                            } else {
                                startDate = moment($(txtDob).val(), 'DD/MM/YYYY')._d;
                            }
                            break;
                        case 'CHD':
                            if (isNullorUndefined($(txtDob).val())) {
                                addyears = -12;
                                startDate.setFullYear(startDate.getFullYear() + addyears);
                            } else {
                                startDate = moment($(txtDob).val(), 'DD/MM/YYYY')._d;
                            }
                            break;
                        case 'INF':
                            if (isNullorUndefined($(txtDob).val())) {
                                addyears = -2;
                                startDate.setFullYear(startDate.getFullYear() + addyears);
                            } else {
                                startDate = moment($(txtDob).val(), 'DD/MM/YYYY')._d;
                            }
                            break;
                    }
                    break;
            }
            $(dateObject).datepicker('option', 'minDate', startDate);
            if (dateObjectsplit[0] == 'dob') {
                $(dateObject).datepicker('option', 'yearRange', "-82:+0");
            }
            if (dateObjectsplit[0] == 'isd') {
                $(dateObject).datepicker('option', 'yearRange', "-70:+0");
            }
            return startDate;
        },
        dateFormat: function (data) {
            if (data) {
                return moment(data).format('DD-MM-YYYY');
            }
        },
        setDate: function (dateObject, date) {
            var self = this;
            var dateObjectsplit = $(dateObject).attr('tag').split('_');
            var index = dateObjectsplit[2];
            switch (dateObjectsplit[0]) {
                case 'dob':
                    self.traveller[index].birthDate = moment(date, "DD/MM/YYYY").format('YYYY-MM-DD');
                    break;
                case 'exd':
                    self.traveller[index].expireDate = moment(date, "DD/MM/YYYY").format('YYYY-MM-DD');
                    break;
            }
        },
        modifyPassport: function () {
            var self = this;
            this.$validator.validate().then(function (result) {
                if (!result) {
                    alertMessage = "Please enter required fields.";
                    alertify.alert("Warning", alertMessage);
                    return false;
                }
                var travelerInfo = [];
                self.traveller.forEach(element => {
                    var gender = "";
                    var nationality = [];
                    nationality = self.country.filter((countryList) => (
                        countryList.twoLetter == element.nationality
                    )).map((con) => ({
                        twoLetter: con.twoLetter,
                        threeLetter: con.threeLetter
                    }))
                    var passenger = {
                        "passengerType": element.passengerType,
                        "gender": gender,
                        "givenName": element.givenName,
                        "namePrefix": element.namePrefix,
                        "surName": element.surName || element.surname,
                        "birthDate": element.birthDate ? moment(element.birthDate).format('DD-MM-YYYY') : null,
                        "docType": '1',
                        "docID": element.documentNumber,
                        "docIssueCountry": element.docIssueCountry,
                        "expireDate": element.expireDate ? moment(element.expireDate).format('DD-MM-YYYY') : null,
                        "nationalityData": nationality[0],
                    }
                    travelerInfo.push(passenger);
                });

                var modifyBookRequest = {
                    request: {
                        service: 'FlightRQ',
                        supplierCodes: [self.supplierCode],
                        content: {
                            command: 'FlightModifyBookRQ',
                            supplierSpecific: self.supplierSpecific,
                            pnr: self.supplierCode == "45" || self.supplierCode == "83" ? self.sessionId : self.pnr,
                            bookFlightRQ: {
                                bookingRefId: self.bookingRefId,
                                bookFlightEntity: {
                                    bookFlight: {
                                        travelerInfo: travelerInfo,
                                    }
                                },
                            }
                        },
                        selectCredential: self.selectCredential
                    }
                }

                var hubUrl = self.commonStore.hubUrls.hubConnection.baseUrl + self.commonStore.hubUrls.hubConnection.ipAddress;
                var ModifyBookRq = self.commonStore.hubUrls.hubConnection.hubServices.flights.updatePassportModifyBook;
                self.sendingRequest = true;
                var config = {
                    axiosConfig: {
                        method: "POST",
                        url: hubUrl + ModifyBookRq,
                        data: modifyBookRequest
                    },
                    successCallback: function (response) {
                        if (response.data.response.content.error) {
                            alertify.alert("Error", response.data.response.content.error).setting({ 'closable': false, onclose: function () {} }).set('label', "Ok");
                        } else {
                            alertify.alert("Success", "Successfully updated.").setting({ 'closable': false, onclose: function () { self.goBack(); } }).set('label', "Ok");
                        }
                        self.sendingRequest = false;
                    },
                    errorCallback: function (error) {
                        console.error(error);
                        alertify.alert("Error", "Error in updating passport information.").setting({ 'closable': false, onclose: function () {} }).set('label', "Ok");
                        self.sendingRequest = false;
                    },
                    showAlert: true
                }

                mainAxiosRequest(config)
            });
        },
        goBack: function (dateObject, date) {
            window.location.href = 'bookinginfo.html'
        },
    },
    mounted: function () {
        var self = this;
        self.country = countryList;
        var tempTraveller = JSON.parse(window.sessionStorage.getItem("Traveller_Info"));
        var temp = []
        tempTraveller.forEach(element => {
            if(element.docType == "1"){
                temp.push(element);    
            }
            else{
                let travellerObj = {
                    airTicketNo: null,
                    airTicketStatus: null,
                    birthDate: null,
                    contact: null,
                    docIssueCountry: null,
                    docType: null,
                    documentNumber: null,
                    expireDate: null,
                    extras: [],
                    givenName: element.givenName,
                    leadPax: null,
                    namePrefix: element.namePrefix,
                    nationality: null,
                    passengerType: null,
                    surname: element.surname,
                }
               temp.push(travellerObj); 
            }
            self.traveller = temp;
            
        });
        self.selectCredential = JSON.parse(window.sessionStorage.getItem("selectCredential"));
        self.pnr = window.sessionStorage.getItem("Flight_Pnr");
        self.sessionId = window.sessionStorage.getItem("Flight_sessionId");
        self.supplierCode = window.sessionStorage.getItem("supplier_Code");
        self.bookingRefId = window.sessionStorage.getItem("flightbookingId");
        self.supplierSpecific = JSON.parse(window.sessionStorage.getItem("supplierSpecific"));
        self.traveller.forEach(element => {
            element.docIssueCountry ? element.docIssueCountry = element.docIssueCountry : element.docIssueCountry = "";
            element.expireDate ? element.expireDate = element.expireDate : element.expireDate = "";
            element.nationality ? element.nationality = element.nationality : element.nationality = "";
            element.documentNumber ? element.documentNumber = element.documentNumber : element.documentNumber = "";
        });
        this.$nextTick(function () {
            var dateFormat = 'dd/mm/yy';
            var noOfMonths = 1;
            $(".cont_txt01_cldr").datepicker({
                numberOfMonths: parseInt(noOfMonths),
                changeMonth: true,
                changeYear: true,
                showButtonPanel: false,
                dateFormat: dateFormat,
                beforeShow: function () {
                    self.getMinDate(this);
                    self.getMaxDate(this);
                },
                onSelect: function (date) {
                    // var startDate = $(this).datepicker('getDate');
                    // sDate = startDate.getDate();
                    self.setDate(this, date);
                }
            });
        });
    }
});