var bookingRefId = '';
var provider = [];
var sendmailstatus = false;
Vue.component("bookinginfo-component", {
    //for websocket
    mixins: [websocketMixin],
    components: {
        'p-accordion': accordion,
        'p-accordion-tab': accordiontab
    },
    data: function () {
        return {
            //get roles from common store
            commonStore: vueCommonStore.state,
            commonAgencyCode: 'AGY75',
            bookinginfo: {
                "data": {
                    "response": {
                        "userID": 0,
                        "agencyCode": "",
                        "bookingRefId": "",
                        "bookingStatus": "",
                        "bookingStatusName": "",
                        "paymentStatus": false,
                        "pnrNum": "",
                        "bookingDate": "",
                        "ticketLimit": "",
                        "totalFareGroup": {
                            "totalBaseNet": "",
                            "totalTaxNet": "",
                            "markupValue": 0,
                            "sellAmount": "",
                            "sellCurrency": "",
                            "additionalServiceFee": "",
                            "cancellationAmount": "",
                            "paidAmount": "",
                            "totalCouponDiscount": 0
                        },
                        "travelerInfo": [{
                            "passengerType": "",
                            "givenName": "",
                            "namePrefix": "",
                            "surname": "",
                            "birthDate": "",
                            "docType": "",
                            "documentNumber": "",
                            "docIssueCountry": "",
                            "expireDate": "",
                            "airTicketNo": null,
                            "extras": []
                        }],
                        "adt": 0,
                        "chd": 0,
                        "inf": 0,
                        "costBreakuppax": [],
                        "miniRules": [],
                        "flLegGroup": [],
                        "paymentMode": ""
                    }
                },
            },
            repriceRBDResponse: '',
            repriceRBDResponseStatus: false,
            repriceLowestRBDResponse: '',
            repriceLowestRBDResponseStatus: false,
            //RepriceRBDfares:false,
            btnReprice: false,
            EditVoucher: true,
            btnRepricing: false,
            btnOfflineTkt: false,
            btnEmailVoucher: false,
            btnEditVoucher: false,
            btnEditPassportInfo: false,
            btnPrintVoucher: false,
            btnDownloadVoucher: false,
            tripDeRrespose: '',
            triplocations: '',
            baggageDetails: [],
            activeminirule: false,
            activefarerule: false,
            btnIssueTkt: false,
            btnCancelPnr: false,
            editorData: "<p></p>",
            downloadoption: false,
            agencyphonenumber: vueCommonStore.state.agencyNode.loginNode.phoneList.filter(function (tel) {
                return tel.type == "Telephone"
            })[0],
            voidActionStatus: false,
            voidAllActionStatus: false,
            voidAllButtonStatus: false,
            paxInfo: [],
            cancelPaxDet: {},
            PNR: '',
            BookingRef: '',
            PnrStatus: '',
            supplierCode: 0,
            segments: [],
            bookingStatusName: '',
            activeColor: "#006C00",
            miniRuleData: '',
            miniRuleDiv: '',
            fareRuleData: '',
            fareRuleDiv: '',
            showLoader: true,
            divBaggageAreaShow: false,
            leadPaxDetShow: false,
            leadPaxDetails: {
                phone: null,
                email: null
            },
            additionalContact: [],
            //CMS
            //Trip Details
            Email_Itinerary_Label: '',
            Fare_Rules_Label: '',
            Mini_Rules_Label: '',
            Download_Itinerary_Label: '',
            Print_Itinerary_Label: '',
            Edit_Voucher_Label: '',
            Email_Ticket_Label: '',
            Email_To_Label: '',
            Close_Button_Label: '',
            Send_Button_Label: '',
            Issue_Button_Label: '',
            Cancel_PNR_Button_Label: '',
            Issue_Ticket_Popup_Label: '',
            Issue_ticket_popup_Content: '',
            Issue_Offline_Ticket_Popup_Label: '',
            Issue_offline_ticket_popup_Content: '',
            Cancel_popup_Label: '',
            Cancel_PNR_popup_Content: '',
            Cancel_Button_Label: '',
            Trip_Header_Label: '',
            Trip_Label: '',
            Cancel_popup_Button_Label: '',
            Email_eTicket_Label: '',
            Download_eTicket_Label: '',
            Print_eTicket_Label: '',
            Email_btn_Label: '',
            Download_btn_Label: '',
            Print_btn_Label: '',
            Yes_Button_Label: '',
            No_Button_Label: '',
            Reprice_Same_RBD_Popup_Heading: '',
            Reprice_Lowest_RBD_Popup_Heading: '',
            Confirm_Button_Label: '',
            Reprice_Same_RBD_Button_Label: '',
            Reprice_Lowest_RBD_Button_Label: '',
            Segment_Heading: '',
            Issue_Offline_Ticket_Button_Label: '',

            //Booking Details
            Booking_Information_Label: '',
            Trip_ID_Label: '',
            Status_Label: '',
            PNR_Label: '',
            Total_Label: '',
            Payment_Mode_Label: '',
            Payment__Status_Label: '',
            Fare_Breakup_Label: '',
            Adult_Label: '',
            Child_Label: '',
            Infant_Label: '',
            Fare_Label: '',
            Taxes_and_Fees_Label: '',
            Discount_Label: '',
            Service_Fee_Label: '',
            Commission_Label: '',
            Close_Button_Label: '',
            Booking_Date_Label: '',
            Booking_Note_Label: '',
            Coupon_Discount_Label: '',
            Action_Available: true,
            Anclrtable: false,
            ancTableData: '',

            //Flight Details
            Flight_Details_Label: '',
            to_Label: '',
            Flight_Cabin_Label: '',
            RBD_Label: '',
            AirLine_Pnr_Label: '',
            summary: '',
            Detail: '',
            after: '',
            before: '',
            rule: '',

            //Passengers Details
            Passengers_Label: '',
            Pax_BreakUp_Label: '',
            Name_Label: '',
            Type_Label: '',
            e_Ticket_Number_Label: '',
            Status_Label: '',
            Action: '',
            Fare_Pax_Breakup_Label: '',
            Fare_Label: '',
            Base_Fare_Label: '',
            Taxes_and_Fees_Label: '',
            Taxes_Label: '',
            Discount_Label: '',
            Service_Fee_Label: '',
            Commission_Label: '',
            Toatl_Label: '',
            Close_Button_Label: '',
            Void_Button_Label: '',
            VoidAll_Button_Label: '',

            //Baggage Details
            Baggage_Details_Label: '',
            Cabin_Label: '',
            to_Label: '',
            Pax_Label: '',
            Check_in_Label: '',
            Baggage_Pax_Label: '',
            BaggDetAttchMail: [],

            //Extras Details
            Extras_Label: '',
            Pax_Label: '',
            Baggage_Label: '',
            Segments_Label: '',
            Seat_Label: '',
            Meal_Label: '',
            travAncillServ: [],

            //Help and Support
            Help_Label: '',
            Support_Label: '',
            Telephone_Label: '',
            Email_Label: '',

            //Alerts
            Alert_Error_Label: '',
            Alert_Success_Label: '',
            Alert_Processing_Label: '',
            Alert_Pending_Label: '',
            Alert_Cancelled_Label: '',
            Alert_TicketCancel_Label: '',
            Tech_Diff_alert_Msg: '',
            Ticket_Issued_Msg: '',
            Ticket_under_Process_Msg: '',
            Ticket_Pending_Msg: '',
            Ticket_not_issue_Msg: '',
            Booking_cancel_Msg: '',
            E_Ticket_Cancelled_Msg: '',
            Alert_Confirm_Label: '',
            Alert_Warning_Label: '',
            Void_Ticket_Alert_Msg: '',
            Void_error_Ticket_Msg: '',
            Cancellation_of_PNR_Msg: '',
            Avoid_Cancellation_Msg: '',
            Alert_Invalid_Label: '',
            Invalid_Email_Msg: '',
            Issue_Offline_Ticket_Msg: '',
            Ok_Label: '',
            Submit_Button_Label: '',
            Cancel_Button_Label: '',
            Passenger_Contact_Label: '',
            Phone_Label: '',
            PassEmail_Label: '',
            Reprice_Error_Alerts: '',
            Reprice_Successful_Alerts: '',
            Repricing_Error_Alerts: '',
            emailOption: 'yes',
            coutryCodeListMain: [],
            countryCodes: [
                ["Afghanistan (‫افغانستان‬‎)", "af", "93"],
                ["Albania (Shqipëri)", "al", "355"],
                ["Algeria (‫الجزائر‬‎)", "dz", "213"],
                ["American Samoa", "as", "1684"],
                ["Andorra", "ad", "376"],
                ["Angola", "ao", "244"],
                ["Anguilla", "ai", "1264"],
                ["Antigua and Barbuda", "ag", "1268"],
                ["Argentina", "ar", "54"],
                ["Armenia (Հայաստան)", "am", "374"],
                ["Aruba", "aw", "297"],
                ["Australia", "au", "61"],
                ["Austria (Österreich)", "at", "43"],
                ["Azerbaijan (Azərbaycan)", "az", "994"],
                ["Bahamas", "bs", "1242"],
                ["Bahrain (‫البحرين‬‎)", "bh", "973"],
                ["Bangladesh (বাংলাদেশ)", "bd", "880"],
                ["Barbados", "bb", "1246"],
                ["Belarus (Беларусь)", "by", "375"],
                ["Belgium (België)", "be", "32"],
                ["Belize", "bz", "501"],
                ["Benin (Bénin)", "bj", "229"],
                ["Bermuda", "bm", "1441"],
                ["Bhutan (འབྲུག)", "bt", "975"],
                ["Bolivia", "bo", "591"],
                ["Bosnia and Herzegovina (Босна и Херцеговина)", "ba", "387"],
                ["Botswana", "bw", "267"],
                ["Brazil (Brasil)", "br", "55"],
                ["British Indian Ocean Territory", "io", "246"],
                ["British Virgin Islands", "vg", "1284"],
                ["Brunei", "bn", "673"],
                ["Bulgaria (България)", "bg", "359"],
                ["Burkina Faso", "bf", "226"],
                ["Burundi (Uburundi)", "bi", "257"],
                ["Cambodia (កម្ពុជា)", "kh", "855"],
                ["Cameroon (Cameroun)", "cm", "237"],
                ["Canada", "ca", "1"],
                ["Cape Verde (Kabu Verdi)", "cv", "238"],
                ["Caribbean Netherlands", "bq", "599"],
                ["Cayman Islands", "ky", "1345"],
                ["Central African Republic (République centrafricaine)", "cf", "236"],
                ["Chad (Tchad)", "td", "235"],
                ["Chile", "cl", "56"],
                ["China (中国)", "cn", "86"],
                ["Colombia", "co", "57"],
                ["Comoros (‫جزر القمر‬‎)", "km", "269"],
                ["Congo (DRC) (Jamhuri ya Kidemokrasia ya Kongo)", "cd", "243"],
                ["Congo (Republic) (Congo-Brazzaville)", "cg", "242"],
                ["Cook Islands", "ck", "682"],
                ["Costa Rica", "cr", "506"],
                ["Côte d’Ivoire", "ci", "225"],
                ["Croatia (Hrvatska)", "hr", "385"],
                ["Cuba", "cu", "53"],
                ["Curaçao", "cw", "599"],
                ["Cyprus (Κύπρος)", "cy", "357"],
                ["Czech Republic (Česká republika)", "cz", "420"],
                ["Denmark (Danmark)", "dk", "45"],
                ["Djibouti", "dj", "253"],
                ["Dominica", "dm", "1767"],
                ["Dominican Republic (República Dominicana)", "do", "1"],
                ["Ecuador", "ec", "593"],
                ["Egypt (‫مصر‬‎)", "eg", "20"],
                ["El Salvador", "sv", "503"],
                ["Equatorial Guinea (Guinea Ecuatorial)", "gq", "240"],
                ["Eritrea", "er", "291"],
                ["Estonia (Eesti)", "ee", "372"],
                ["Ethiopia", "et", "251"],
                ["Falkland Islands (Islas Malvinas)", "fk", "500"],
                ["Faroe Islands (Føroyar)", "fo", "298"],
                ["Fiji", "fj", "679"],
                ["Finland (Suomi)", "fi", "358"],
                ["France", "fr", "33"],
                ["French Guiana (Guyane française)", "gf", "594"],
                ["French Polynesia (Polynésie française)", "pf", "689"],
                ["Gabon", "ga", "241"],
                ["Gambia", "gm", "220"],
                ["Georgia (საქართველო)", "ge", "995"],
                ["Germany (Deutschland)", "de", "49"],
                ["Ghana (Gaana)", "gh", "233"],
                ["Gibraltar", "gi", "350"],
                ["Greece (Ελλάδα)", "gr", "30"],
                ["Greenland (Kalaallit Nunaat)", "gl", "299"],
                ["Grenada", "gd", "1473"],
                ["Guadeloupe", "gp", "590"],
                ["Guam", "gu", "1671"],
                ["Guatemala", "gt", "502"],
                ["Guinea (Guinée)", "gn", "224"],
                ["Guinea-Bissau (Guiné Bissau)", "gw", "245"],
                ["Guyana", "gy", "592"],
                ["Haiti", "ht", "509"],
                ["Honduras", "hn", "504"],
                ["Hong Kong (香港)", "hk", "852"],
                ["Hungary (Magyarország)", "hu", "36"],
                ["Iceland (Ísland)", "is", "354"],
                ["India (भारत)", "in", "91"],
                ["Indonesia", "id", "62"],
                ["Iran (‫ایران‬‎)", "ir", "98"],
                ["Iraq (‫العراق‬‎)", "iq", "964"],
                ["Ireland", "ie", "353"],
                ["Israel (‫ישראל‬‎)", "il", "972"],
                ["Italy (Italia)", "it", "39"],
                ["Jamaica", "jm", "1876"],
                ["Japan (日本)", "jp", "81"],
                ["Jordan (‫الأردن‬‎)", "jo", "962"],
                ["Kazakhstan (Казахстан)", "kz", "7"],
                ["Kenya", "ke", "254"],
                ["Kiribati", "ki", "686"],
                ["Kuwait (‫الكويت‬‎)", "kw", "965"],
                ["Kyrgyzstan (Кыргызстан)", "kg", "996"],
                ["Laos (ລາວ)", "la", "856"],
                ["Latvia (Latvija)", "lv", "371"],
                ["Lebanon (‫لبنان‬‎)", "lb", "961"],
                ["Lesotho", "ls", "266"],
                ["Liberia", "lr", "231"],
                ["Libya (‫ليبيا‬‎)", "ly", "218"],
                ["Liechtenstein", "li", "423"],
                ["Lithuania (Lietuva)", "lt", "370"],
                ["Luxembourg", "lu", "352"],
                ["Macau (澳門)", "mo", "853"],
                ["Macedonia (FYROM) (Македонија)", "mk", "389"],
                ["Madagascar (Madagasikara)", "mg", "261"],
                ["Malawi", "mw", "265"],
                ["Malaysia", "my", "60"],
                ["Maldives", "mv", "960"],
                ["Mali", "ml", "223"],
                ["Malta", "mt", "356"],
                ["Marshall Islands", "mh", "692"],
                ["Martinique", "mq", "596"],
                ["Mauritania (‫موريتانيا‬‎)", "mr", "222"],
                ["Mauritius (Moris)", "mu", "230"],
                ["Mexico (México)", "mx", "52"],
                ["Micronesia", "fm", "691"],
                ["Moldova (Republica Moldova)", "md", "373"],
                ["Monaco", "mc", "377"],
                ["Mongolia (Монгол)", "mn", "976"],
                ["Montenegro (Crna Gora)", "me", "382"],
                ["Montserrat", "ms", "1664"],
                ["Morocco (‫المغرب‬‎)", "ma", "212"],
                ["Mozambique (Moçambique)", "mz", "258"],
                ["Myanmar (Burma) (မြန်မာ)", "mm", "95"],
                ["Namibia (Namibië)", "na", "264"],
                ["Nauru", "nr", "674"],
                ["Nepal (नेपाल)", "np", "977"],
                ["Netherlands (Nederland)", "nl", "31"],
                ["New Caledonia (Nouvelle-Calédonie)", "nc", "687"],
                ["New Zealand", "nz", "64"],
                ["Nicaragua", "ni", "505"],
                ["Niger (Nijar)", "ne", "227"],
                ["Nigeria", "ng", "234"],
                ["Niue", "nu", "683"],
                ["Norfolk Island", "nf", "672"],
                ["North Korea (조선 민주주의 인민 공화국)", "kp", "850"],
                ["Northern Mariana Islands", "mp", "1670"],
                ["Norway (Norge)", "no", "47"],
                ["Oman (‫عُمان‬‎)", "om", "968"],
                ["Pakistan (‫پاکستان‬‎)", "pk", "92"],
                ["Palau", "pw", "680"],
                ["Palestine (‫فلسطين‬‎)", "ps", "970"],
                ["Panama (Panamá)", "pa", "507"],
                ["Papua New Guinea", "pg", "675"],
                ["Paraguay", "py", "595"],
                ["Peru (Perú)", "pe", "51"],
                ["Philippines", "ph", "63"],
                ["Poland (Polska)", "pl", "48"],
                ["Portugal", "pt", "351"],
                ["Puerto Rico", "pr", "1"],
                ["Qatar (‫قطر‬‎)", "qa", "974"],
                ["Réunion (La Réunion)", "re", "262"],
                ["Romania (România)", "ro", "40"],
                ["Russia (Россия)", "ru", "7"],
                ["Rwanda", "rw", "250"],
                ["Saint Barthélemy (Saint-Barthélemy)", "bl", "590"],
                ["Saint Helena", "sh", "290"],
                ["Saint Kitts and Nevis", "kn", "1869"],
                ["Saint Lucia", "lc", "1758"],
                ["Saint Martin (Saint-Martin (partie française))", "mf", "590"],
                ["Saint Pierre and Miquelon (Saint-Pierre-et-Miquelon)", "pm", "508"],
                ["Saint Vincent and the Grenadines", "vc", "1784"],
                ["Samoa", "ws", "685"],
                ["San Marino", "sm", "378"],
                ["São Tomé and Príncipe (São Tomé e Príncipe)", "st", "239"],
                ["Saudi Arabia (‫المملكة العربية السعودية‬‎)", "sa", "966"],
                ["Senegal (Sénégal)", "sn", "221"],
                ["Serbia (Србија)", "rs", "381"],
                ["Seychelles", "sc", "248"],
                ["Sierra Leone", "sl", "232"],
                ["Singapore", "sg", "65"],
                ["Sint Maarten", "sx", "1721"],
                ["Slovakia (Slovensko)", "sk", "421"],
                ["Slovenia (Slovenija)", "si", "386"],
                ["Solomon Islands", "sb", "677"],
                ["Somalia (Soomaaliya)", "so", "252"],
                ["South Africa", "za", "27"],
                ["South Korea (대한민국)", "kr", "82"],
                ["South Sudan (‫جنوب السودان‬‎)", "ss", "211"],
                ["Spain (España)", "es", "34"],
                ["Sri Lanka (ශ්‍රී ලංකාව)", "lk", "94"],
                ["Sudan (‫السودان‬‎)", "sd", "249"],
                ["Suriname", "sr", "597"],
                ["Swaziland", "sz", "268"],
                ["Sweden (Sverige)", "se", "46"],
                ["Switzerland (Schweiz)", "ch", "41"],
                ["Syria (‫سوريا‬‎)", "sy", "963"],
                ["Taiwan (台灣)", "tw", "886"],
                ["Tajikistan", "tj", "992"],
                ["Tanzania", "tz", "255"],
                ["Thailand (ไทย)", "th", "66"],
                ["Timor-Leste", "tl", "670"],
                ["Togo", "tg", "228"],
                ["Tokelau", "tk", "690"],
                ["Tonga", "to", "676"],
                ["Trinidad and Tobago", "tt", "1868"],
                ["Tunisia (‫تونس‬‎)", "tn", "216"],
                ["Turkey (Türkiye)", "tr", "90"],
                ["Turkmenistan", "tm", "993"],
                ["Turks and Caicos Islands", "tc", "1649"],
                ["Tuvalu", "tv", "688"],
                ["U.S. Virgin Islands", "vi", "1340"],
                ["Uganda", "ug", "256"],
                ["Ukraine (Україна)", "ua", "380"],
                ["United Arab Emirates (‫الإمارات العربية المتحدة‬‎)", "ae", "971"],
                ["United Kingdom", "gb", "44"],
                ["United States", "us", "1"],
                ["Uruguay", "uy", "598"],
                ["Uzbekistan (Oʻzbekiston)", "uz", "998"],
                ["Vanuatu", "vu", "678"],
                ["Vatican City (Città del Vaticano)", "va", "39"],
                ["Venezuela", "ve", "58"],
                ["Vietnam (Việt Nam)", "vn", "84"],
                ["Wallis and Futuna", "wf", "681"],
                ["Yemen (‫اليمن‬‎)", "ye", "967"],
                ["Zambia", "zm", "260"],
                ["Zimbabwe", "zw", "263"]
            ],
            // showCancelPNR: true,
            bookingTicketLimit: "",
            paidServicesTotalPrice: 0,
            supplierRemarks: [],
            remarksToggleShowMore: true,
            spinnerHtml: 'Processing<span class="loader__dot">.</span><span class="loader__dot">.</span><span class="loader__dot">.</span>',
            processingPdf: false,
            processingEmail: false,
            processingPrint: false,
            contactError: false,
            showContact: false
        };
    },
    created: function () {
        var self = this;
        self.showLoader = true;
        this.getPagecontent();
        var agencyNode = window.localStorage.getItem("agencyNode");
        if (agencyNode) {
            agencyNode = JSON.parse(atob(agencyNode));
            var servicesList = agencyNode.loginNode.servicesList;
            var supDetails = servicesList.filter(function (service) {
                return service.name.toLowerCase().includes('air')
            });
            supDetails[0].provider.forEach(function (data) {
                provider.push(data.id);
            });
        }
        bookingRefId = window.sessionStorage.getItem("flightbookingId");
        sendmailstatus = JSON.parse(window.sessionStorage.getItem("sendbookmail"));
        this.getTripDetails('');

    },
    methods: {
        getTripDetails: function (service) {
            var vm = this;
            var self = this;
            var selectCred = null;
            try {
                selectCred = JSON.parse(window.sessionStorage.getItem("selectCredential"))
            } catch (err) {}
            sendmailstatus = JSON.parse(window.sessionStorage.getItem("sendbookmail"));
            var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = vm.commonStore.hubUrls.hubConnection.hubServices.flights.airTripDetails;
            var PostData = {
                request: {
                    service: "FlightRQ",
                    content: {
                        command: "FlightTripDetailsRQ",
                        supplierSpecific: {},
                        tripDetailRQ: {
                            bookingRefId: bookingRefId
                        }
                    },
                    selectCredential: selectCred,
                    supplierCodes: null,
                    hqCode:vm.commonStore.agencyNode.loginNode.solutionId ? vm.commonStore.agencyNode.loginNode.solutionId : "",

                }
            };
            console.log("TripDetailsRQ: ", JSON.stringify(PostData));


            var config = {
                axiosConfig: {
                    method: "POST",
                    url: hubUrl + serviceUrl,
                    data: PostData
                },
                successCallback: function (res) {
                    console.log("TripDetails RESPONSE RECEIVED: ", res);
                    vm.triplocations = '';
                    localStorage.accessToken = res.headers.access_token;
                    vm.tripDeRrespose = res;
                    vm.supplierRemarks = res.data.response.content.supplierSpecific ? res.data.response.content.supplierSpecific.remark || [] : [];
                    vm.bookinginfo.data.response = res.data.response.content.tripDetailRS.tripDetailsUiData.response;
                    vm.bookinginfo.data.response.flLegGroup.forEach(function (item, index) {
                        vm.triplocations += ' - ' + airportLocationFromAirportCode(item.from) + ' to ' + airportLocationFromAirportCode(item.to);
                    });
                    try {
                        if (vm.commonStore.selectedCurrency == '') {
                            if (vm.tripDeRrespose.data.response.selectCredential) {
                                vm.commonStore.selectedCurrency = vm.tripDeRrespose.data.response.selectCredential.officeIdList[0].currency;
                            } else {
                                vm.commonStore.selectedCurrency = JSON.parse(window.sessionStorage.getItem("selectCredential")).officeIdList[0].currency;
                            }
                            if (!vm.commonStore.selectedCurrency) {
                                vm.commonStore.selectedCurrency = vm.tripDeRrespose.data.response.content.tripDetailRS.tripDetailsUiData.response.totalFareGroup.sellCurrency;
                                vm.commonStore.currencyMultiplier = 1;
                            } else {
                                vm.commonStore.currencyMultiplier = _.filter(vm.commonStore.currencyRates, function (e) {
                                    return e.fromCurrencyCode == vm.commonStore.selectedCurrency;
                                })[0].sellRate;
                            }
                        }
                    } catch (error) {
                        if (vm.commonStore.selectedCurrency) {
                            vm.commonStore.selectedCurrency = vm.commonStore.agencyNode.loginNode.currency;
                        }
                        vm.commonStore.currencyMultiplier = 1;
                    }

                    vm.PNR = vm.bookinginfo.data.response.pnrNum;
                    vm.BookingRef = vm.bookinginfo.data.response.bookingRefId;
                    vm.PnrStatus = vm.bookinginfo.data.response.bookingStatus;
                    vm.bookingStatusName = vm.bookinginfo.data.response.bookingStatusName;
                    vm.supplierCode = parseInt(res.data.response.supplierCode);
                    if (vm.bookinginfo.data.response.miniRules != null && vm.bookinginfo.data.response.miniRules.length > 0) {
                        var miniRules = vm.bookinginfo.data.response.miniRules;
                        // vm.activeminirule = true;
                        var SummaryLists = [];
                        miniRules.forEach(rule => {
                            var SummaryList = _.groupBy(rule.category, "cat");
                            SummaryLists.push(SummaryList);
                        });
                        self.miniRuleData = SummaryLists;
                    }
                    if (vm.bookinginfo.data.response.fareruleSeg != null && vm.bookinginfo.data.response.fareruleSeg.length > 0) {
                        self.fareRuleData = vm.bookinginfo.data.response.fareruleSeg;
                        // vm.activefarerule = true;
                    }
                    // if (vm.PnrStatus == 'FE') {
                    // vm.btnReprice = true;
                    // vm.btnRepricing = true;
                    // vm.btnIssueTkt = false;
                    // vm.btnCancelPnr = true;
                    // vm.EditVoucher = false;
                    // vm.showLoader = false;

                    // }
                    // if (vm.PnrStatus != "XX" && vm.PnrStatus != "OK" && vm.PnrStatus != "RQ" && vm.PnrStatus != "RF" && vm.PnrStatus != "PD") {
                    //     vm.btnReprice = true;
                    //     vm.btnRepricing = true;
                    // }
                    // if (vm.PnrStatus == 'RQ' || vm.PnrStatus == 'HK') {
                    //     vm.btnReprice = true;
                    //     vm.btnRepricing = true;
                    // }
                    if (vm.PnrStatus == "XX" || vm.PnrStatus == "RF" || vm.PnrStatus == "TF") {
                        vm.activeColor = "#F44336";
                        vm.Action_Available = false;
                        // vm.btnReprice = false;

                    }
                    if (vm.PnrStatus == 'TV' && vm.setButtonsAndStatus("cancelPNR", vm.PnrStatus, vm.supplierCode, vm.commonStore.commonRoles.hasAirCancelPNR)) {
                        // alertify.alert(self.Alert_Warning_Label, self.Cancellation_of_PNR_Msg).set('label', self.Ok_Label);
                        // vm.btnCancelPnr = true;
                        vm.Action_Available = false;
                        alertify.confirm(self.Alert_Warning_Label, self.Cancellation_of_PNR_Msg, function () {
                            self.cancelPNR();
                        }, function () {}).set({
                            'closable': false,
                            'labels': {
                                ok: 'Yes',
                                cancel: 'No'
                            }
                        });
                    }
                    if (vm.PnrStatus == 'PV') {
                        alertify.alert(self.Alert_Warning_Label, self.Avoid_Cancellation_Msg).set('label', self.Ok_Label);
                    }
                    if (vm.PnrStatus == 'OK') {
                        vm.activeColor = "#006C00";
                        self.Email_btn_Label = self.Email_eTicket_Label;
                        self.Download_btn_Label = self.Download_eTicket_Label;
                        self.Print_btn_Label = self.Print_eTicket_Label;
                    } else {
                        if (vm.PnrStatus != 'FE') {
                            self.Email_btn_Label = self.Email_Itinerary_Label;
                            self.Download_btn_Label = self.Download_Itinerary_Label;
                            self.Print_btn_Label = self.Print_Itinerary_Label;
                        }
                    }
                    vm.getPaxDetails();
                    vm.getBaggageDeatails();
                    // vm.getbookinginfo(vm.PnrStatus);
                    vm.setEditVoucher();
                    vm.setsegments();
                    bindCreditLimit();
                    if (!sendmailstatus && vm.PnrStatus != 'RF' && vm.PnrStatus != 'RQ') {
                        vm.sendMail("BOOK");
                    }
                    if (service != "") {
                        if (service == 'TKTISSUE') {
                            // vm.showLoader = false;
                            alertify.alert(self.Alert_Success_Label, self.Ticket_Issued_Msg).set('label', self.Ok_Label);
                            vm.sendMail(service);
                        }
                        if (service == 'CANCELTKT') {
                            vm.sendMail(service);
                        }
                        if (service == 'CANCELBKNG') {
                            // vm.showLoader = false;
                            var ticketed = self.paxInfo.some(function (e) {
                                return e.ticketType.toLowerCase() == "ticketed"
                            });
                            var voidTicket = self.setButtonsAndStatus("voidTicket", ticketed ? "ticketed" : "cancelled", self.supplierCode, self.commonStore.commonRoles.hasAirCancelTicket);
                            if (voidTicket && ticketed) {
                                alertify.confirm("Message", "Do you want to void ticket?", function () {
                                    setTimeout(function () {
                                        document.getElementById("void-ticket").scrollIntoView();
                                        alertify.alert(self.Alert_Cancelled_Label, self.Booking_cancel_Msg).set('label', self.Ok_Label);
                                    }, 100);
                                }, function () {
                                    alertify.alert(self.Alert_Cancelled_Label, self.Booking_cancel_Msg).set('label', self.Ok_Label);
                                }).set({
                                    'closable': false,
                                    'labels': {
                                        ok: 'Continue',
                                        cancel: 'Cancel'
                                    }
                                });

                            } else {
                                alertify.alert(self.Alert_Cancelled_Label, self.Booking_cancel_Msg).set('label', self.Ok_Label);
                            }
                        }
                        if (service == 'reprice') {
                            // vm.showLoader = false;
                            // vm.btnReprice = false;
                            // vm.btnRepricing = false;
                            if (vm.PnrStatus == 'HK') {
                                alertify.alert(self.Alert_Success_Label, self.Reprice_Successful_Alerts).set('label', self.Ok_Label);
                            }

                        }
                    }

                    vm.bindPaxExtars(res.data.response.content.tripDetailRS.tripDetailsUiData.response.travelerInfo)
                    vm.showLoader = false;
                    // vm.showCancelPNR = globalConfigs.services.flights.supplpierWithoutCancelPNR.indexOf(vm.supplierCode) == -1;

                    if ((vm.supplierCode == "44" || vm.supplierCode == "49" || vm.supplierCode == "50" || vm.supplierCode == "71" || vm.supplierCode == "57" || vm.supplierCode == "73" || vm.supplierCode == "54" || vm.supplierCode == "58" || vm.supplierCode == "83" || vm.supplierCode == "60" || vm.supplierCode == "74" || vm.supplierCode == "86" || vm.supplierCode == "87") &&
                        vm.bookinginfo.data.response.ticketLimit && vm.PnrStatus == 'HK') {
                        self.bookingTicketLimit = moment(vm.bookinginfo.data.response.ticketLimit).format('DD MMM YYYY, HH:mm') + " GMT"; // 24 hrs
                    } else if (vm.PnrStatus == 'OK' || vm.PnrStatus == 'XX') {
                        self.bookingTicketLimit = "";
                    }

                    if (res.data.response.content.warning) {
                        alertify.alert("Please note", res.data.response.content.warning.message);
                    }

                    // if (vm.supplierCode == "64") {
                    //     vm.btnCancelPnr = false;
                    //     vm.btnReprice = false;
                    //     vm.btnRepricing = false;
                    // }

                    // if (vm.supplierCode == "44" || vm.supplierCode == "49" || vm.supplierCode == "50" || vm.supplierCode == "69" || vm.supplierCode == "71") {
                    //     vm.btnCancelPnr = false;
                    // }

                    // if (vm.supplierCode == "54") {
                    //     vm.btnIssueTkt = false;
                    //     vm.btnCancelPnr = false;
                    //     vm.btnReprice = false;
                    //     vm.btnRepricing = false;
                    // }

                    vm.btnCancelPnr = vm.setButtonsAndStatus("cancelPNR", vm.PnrStatus, vm.supplierCode, vm.commonStore.commonRoles.hasAirCancelPNR);
                    vm.btnIssueTkt = vm.setButtonsAndStatus("issueTicket", vm.PnrStatus, vm.supplierCode, vm.commonStore.commonRoles.hasAirIssueTicket);
                    vm.btnOfflineTkt = vm.setButtonsAndStatus("offlineTicket", vm.PnrStatus, vm.supplierCode, vm.commonStore.commonRoles.hasAirOfflineTicketAfterBooking);
                    vm.btnReprice = vm.setButtonsAndStatus("repricePNR", vm.PnrStatus, vm.supplierCode, vm.commonStore.commonRoles.hasAirSameRBD || vm.commonStore.commonRoles.hasAirLowestRBD);
                    vm.btnEmailVoucher = vm.setButtonsAndStatus("emailVoucher", vm.PnrStatus, vm.supplierCode, true);
                    vm.btnEditVoucher = vm.setButtonsAndStatus("editVoucher", vm.PnrStatus, vm.supplierCode, vm.commonStore.commonRoles.hasEditVoucher);
                    vm.btnPrintVoucher = vm.setButtonsAndStatus("printVoucher", vm.PnrStatus, vm.supplierCode, true);
                    vm.btnDownloadVoucher = vm.setButtonsAndStatus("downloadVoucher", vm.PnrStatus, vm.supplierCode, true);
                    vm.btnEditPassportInfo = vm.setButtonsAndStatus("editPassportInfo", vm.PnrStatus, vm.supplierCode, true);
                    // Show edit passport info button After Ticketed"
                    if (!vm.btnEditPassportInfo) {
                        vm.btnEditPassportInfo = vm.setButtonsAndStatus("editPassportInfoAfterTicketed", vm.PnrStatus, vm.supplierCode, true);
                    }

                    if (vm.bookinginfo.data.response.fareruleSeg && vm.bookinginfo.data.response.fareruleSeg.length > 0 && vm.bookinginfo.data.response.fareruleSeg[0].fareRules.length > 0) {
                        vm.activefarerule = vm.setButtonsAndStatus("fareRules", vm.PnrStatus, vm.supplierCode, true);
                    }
                    vm.activeminirule = vm.miniRuleData != "" && vm.setButtonsAndStatus("miniRules", vm.PnrStatus, vm.supplierCode, true);

                    if (vm.PnrStatus == "OK" && (vm.supplierCode == "57"||vm.supplierCode == "62")) {
                        vm.btnCancelPnr = false;
                    }
                    if (vm.getParameterByName("status") == 102) {
                        if (vm.PnrStatus == 'HK') {
                            self.cancelPNR();
                        }
                    } else if (vm.getParameterByName("status") == 101) {
                        if (vm.PnrStatus == 'HK') {
                            self.getIssueTicketRQ();
                        }
                    }
                },
                errorCallback: function (error) {
                    self.showLoader = false;

                },
                showAlert: true
            };

            mainAxiosRequest(config);

        },
        getParameterByName: function (name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, '\\$&');
            var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, ' '));
        },
        setButtonsAndStatus: function (button, status, supplier, role) {
            try {
                if (status.toLowerCase() == "cancelled") {
                    status = "XX";
                } else if (status.toLowerCase() == "ticketed") {
                    status = "OK";
                }
                return role && (globalConfigs.services.flights[button].supplier.indexOf(parseInt(supplier)) != -1 &&
                    globalConfigs.services.flights[button].status.indexOf(status) != -1);

            } catch (error) {
                return false;
            }
        },
        onSelectedCodeMain: _.debounce(function (event) {
            if (event.target.value.length > 0) {
                var newData = [];

                this.countryCodes.forEach(function (el) {
                    if (el[2].toLowerCase().match(RegExp('^' + event.target.value.toLowerCase()))) {
                        newData.push(el);
                    }
                });

                this.countryCodes.forEach(function (el) {
                    if (el[2].toLowerCase().indexOf(event.target.value.toLowerCase()) >= 0 &&
                        !(el[2].toLowerCase().match(RegExp('^' + event.target.value.toLowerCase())))) {
                        newData.push(el);
                    }
                });
                if (newData.length > 0) {
                    this.coutryCodeListMain = newData;
                } else {
                    this.coutryCodeListMain = [];
                }
            } else {
                this.coutryCodeListMain = [];
            }
        }, 100),
        onSelectedCountryMain: function (country) {
            $('#txtLeadPaxPhone2').val(country[2]);
            this.coutryCodeListMain = [];
        },
        checkBookingStatus: function () {
            if (this.bookinginfo.data.response.bookingStatus == "NF" &&
                this.bookinginfo.data.response.paymentMode == 6 &&
                this.bookinginfo.data.response.paymentStatus == false) {
                var returnURL = window.location.origin + "/Flights/bookinginfo.html";
                var paymentDetails = {
                    bookingReference: this.bookinginfo.data.response.bookingRefId,
                    totalAmount: this.bookinginfo.data.response.totalFareGroup.sellAmount,
                    currency: this.commonStore.agencyNode.loginNode.currency || 'AED',
                    currentPayGateways: this.commonStore.agencyNode.loginNode.paymentGateways,
                    customerEmail: JSON.parse(atob(window.localStorage.getItem("agencyNode"))).emailId
                }
                var status = paymentManager(paymentDetails, returnURL);
            } else {
                this.getIssueTicketRQ();
                $("#ticket_issue_popup").modal('hide');
            }
        },
        issueTicketContact: function() {
            this.contactError = false;
            if (this.commonStore.commonRoles.hasAirIssueContactNumber) {
                if ($('#txtLeadPaxPhone2').val() == "" && $('#txtLeadPaxPhone').val() == "") {
                    this.contactError = true;
                } else{
                    $("#ticket_issue_popup").modal('hide');
                    this.contactError = false;
                    this.checkBookingStatus();
                }
            } else {
                this.checkBookingStatus();
            }
        },
        getIssueTicketRQ: function () {
            var vm = this;
            var self = this;
            self.showLoader = true;
            var selectCred = null;
            try {
                selectCred = JSON.parse(window.sessionStorage.getItem("selectCredential"))
            } catch (err) {}
            var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = vm.commonStore.hubUrls.hubConnection.hubServices.flights.airIssueTicket;
            var leadPaxInfo = self.bookinginfo.data.response.travelerInfo.filter(function(e) {
                return e.leadPax == true;
            })[0];
            var smsNotification = {
                countryCode: $('#txtLeadPaxPhone2').val() ? "+" + $('#txtLeadPaxPhone2').val() : "",
                number: $('#txtLeadPaxPhone').val() || "",
                paxName: leadPaxInfo.givenName + " " + leadPaxInfo.surname
            };
            
            var PostData = {
                request: {
                    service: "FlightRQ",
                    content: {
                        command: "FlightIssueTicketRQ",
                        supplierSpecific: vm.tripDeRrespose.data.response.content.supplierSpecific ? vm.tripDeRrespose.data.response.content.supplierSpecific : {},
                        issueTicketRQ: {
                            bookingRefId: bookingRefId,
                            smsNotification: $('#txtLeadPaxPhone').val() && $('#txtLeadPaxPhone2').val() ? smsNotification : undefined,
                            passengerTypeQuantity: {
                                adt: vm.tripDeRrespose.data.response.content.tripDetailRS.tripDetailsUiData.response.adt,
                                chd: vm.tripDeRrespose.data.response.content.tripDetailRS.tripDetailsUiData.response.chd,
                                inf: vm.tripDeRrespose.data.response.content.tripDetailRS.tripDetailsUiData.response.inf
                            }
                        }

                    },
                    selectCredential: selectCred,
                    supplierCodes: [self.supplierCode],
                    hqCode:vm.commonStore.agencyNode.loginNode.solutionId ? vm.commonStore.agencyNode.loginNode.solutionId :""

                }
            };
            console.log("FlightIssueTicketRQ: ", JSON.stringify(PostData));


            var config = {
                axiosConfig: {
                    method: "POST",
                    url: hubUrl + serviceUrl,
                    data: PostData
                },
                successCallback: function (response) {
                    console.log("FlightIssueTicket RESPONSE RECEIVED: ", response);
                    localStorage.accessToken = response.headers.access_token;
                    try {
                        window.sessionStorage.setItem("selectCredential", JSON.stringify(response.data.response.selectCredential));
                        var Result = response.data.response;
                        var fareInformationWithoutPnrReply = Result.content.fareInformationWithoutPnrReply;
                        try {
                            selectCred = JSON.parse(window.sessionStorage.getItem("selectCredential"))
                        } catch (err) {}

                        if (fareInformationWithoutPnrReply
                            // && 
                            // parseFloat(fareInformationWithoutPnrReply.flightFareDetails.totalFare) != 
                            // parseFloat(self.bookinginfo.data.response.totalFareGroup.sellAmount)
                        ) {
                            alertify.confirm(self.Alert_Warning_Label,
                                "Your fare was increased from <b>" +
                                self.$n(self.bookinginfo.data.response.totalFareGroup.sellAmount / vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency) +
                                "</b> to <b>" +
                                self.$n(fareInformationWithoutPnrReply.flightFareDetails.totalFare / vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency) +
                                "</b>.<br /><br />Do you want to proceed issuance?",
                                function () {
                                    var newTktRq = {
                                        request: {
                                            service: "FlightRQ",
                                            content: {
                                                command: "FlightIssueTicketRQ",
                                                supplierSpecific: fareInformationWithoutPnrReply.supplierSpecific || {},
                                                issueTicketRQ: {
                                                    bookingRefId: bookingRefId,
                                                    isPnrTransfered: Result.content.issueTicketRS.isPnrTransfered,
                                                    passengerTypeQuantity: {
                                                        adt: fareInformationWithoutPnrReply.passengerTypeQuantity.adt,
                                                        chd: fareInformationWithoutPnrReply.passengerTypeQuantity.chd,
                                                        inf: fareInformationWithoutPnrReply.passengerTypeQuantity.inf
                                                    }
                                                },
                                                repricingRbdRQ: {
                                                    bookingRefId: Result.content.issueTicketRS.bookingRefId,
                                                    uniqueId: Result.content.issueTicketRS.uniqueId,
                                                    sealed: fareInformationWithoutPnrReply.sealed,
                                                    paymentModeId: vm.bookinginfo.data.response.paymentModeId,
                                                },
                                            },
                                            selectCredential: selectCred,
                                            supplierCodes: [self.supplierCode],
                                        },
                                    };
                                    self.showLoader = true;
                                    var config = {
                                        axiosConfig: {
                                            method: "POST",
                                            url: hubUrl + serviceUrl,
                                            data: newTktRq
                                        },
                                        successCallback: function (response) {
                                            console.log("FlightIssueTicket RESPONSE RECEIVED: ", response);
                                            localStorage.accessToken = response.headers.access_token;
                                            try {
                                                window.sessionStorage.setItem("selectCredential", JSON.stringify(response.data.response.selectCredential));
                                                try {
                                                    selectCred = JSON.parse(window.sessionStorage.getItem("selectCredential"))
                                                } catch (err) {}
                                                var Result = response.data.response;
                                                vm.PNR = Result.content.issueTicketRS.tripDetailsResult.uniqueID;
                                                vm.PnrStatus = Result.content.issueTicketRS.tripDetailsResult.bookingStatus;
                                                vm.bookingStatusName = Result.content.issueTicketRS.tripDetailsResult.ticketStatus;
                                                switch (vm.PnrStatus) {
                                                    case "OK":
                                                        vm.getTripDetails('TKTISSUE');
                                                        break;
                                                    case "TP":
                                                        vm.getTripDetails('');
                                                        self.showLoader = false;
                                                        alertify.alert(self.Alert_Processing_Label, self.Ticket_under_Process_Msg).set('label', self.Ok_Label);
                                                        break;
                                                    case "PD":
                                                        vm.getTripDetails('');
                                                        self.showLoader = false;
                                                        alertify.alert(self.Alert_Pending_Label, self.Ticket_Pending_Msg).set('label', self.Ok_Label);
                                                        break;
                                                    default:
                                                        vm.getTripDetails('');
                                                        self.showLoader = false;
                                                        alertify.alert(self.Alert_Error_Label, self.Ticket_not_issue_Msg).set('label', self.Ok_Label);
                                                }
                                            } catch (err) {
                                                self.showLoader = false;
                                                alertify.alert(self.Alert_Error_Label, self.Tech_Diff_alert_Msg).set('label', self.Ok_Label);
                                                console.log("Error in getIssueTicketRq-Rs" + err);
                                            }
                                        },
                                        errorCallback: function (error) {
                                            self.showLoader = false;
                                        },
                                        showAlert: true
                                    }

                                    mainAxiosRequest(config);
                                },
                                function () {
                                    // do nothing
                                }).set({
                                'closable': false,
                                'labels': {
                                    ok: 'Yes',
                                    cancel: 'No'
                                }
                            });
                            self.showLoader = false;
                        } else {
                            // var Travelinfo = Result.content.issueTicketRS.tripDetailsResult.itineraryInformation;
                            vm.PNR = Result.content.issueTicketRS.tripDetailsResult.uniqueID;
                            vm.PnrStatus = Result.content.issueTicketRS.tripDetailsResult.bookingStatus;
                            vm.bookingStatusName = Result.content.issueTicketRS.tripDetailsResult.ticketStatus;
                            switch (vm.PnrStatus) {
                                case "OK":
                                    vm.getTripDetails('TKTISSUE');
                                    break;
                                case "TP":
                                    vm.getTripDetails('');
                                    self.showLoader = false;
                                    alertify.alert(self.Alert_Processing_Label, self.Ticket_under_Process_Msg).set('label', self.Ok_Label);
                                    break;
                                case "PD":
                                    vm.getTripDetails('');
                                    self.showLoader = false;
                                    alertify.alert(self.Alert_Pending_Label, self.Ticket_Pending_Msg).set('label', self.Ok_Label);
                                    break;
                                default:
                                    vm.getTripDetails('');
                                    self.showLoader = false;
                                    alertify.alert(self.Alert_Error_Label, self.Ticket_not_issue_Msg).set('label', self.Ok_Label);
                            }
                        }
                    } catch (err) {
                        self.showLoader = false;
                        alertify.alert(self.Alert_Error_Label, self.Tech_Diff_alert_Msg).set('label', self.Ok_Label);
                        console.log("Error in getIssueTicketRq-Rs" + err);
                    }
                },
                errorCallback: function (error) {
                    self.showLoader = false;
                },
                showAlert: true
            }

            mainAxiosRequest(config);

        },
        offlineIssueTicket: function () {
            var vm = this;
            var flLegGroup = vm.bookinginfo.data.response.flLegGroup;
            var tripDetails = [];
            for (var i = 0; i < flLegGroup.length; i++) {
                // for (var j = 0; j < flLegGroup[i].segments.length; j++) {
                var fromAirport = airportFromAirportCode(flLegGroup[i].from);
                var toAirport = airportFromAirportCode(flLegGroup[i].to);

                tripDetails.push({
                    airportCodeSrc: {
                        code: flLegGroup[i].from,
                        name: fromAirport
                    },
                    airportCodeDst: {
                        code: flLegGroup[i].to,
                        name: toAirport
                    },
                    departureDate: moment(flLegGroup[i].segments[0].departureDate).format('YYYY-MM-DD'),
                    cabinClassCode: flLegGroup[i].segments[0].bookingClass,
                    rbd: flLegGroup[i].segments[0].cabin,
                    flightNum: flLegGroup[i].segments[0].flightNumber,
                })
                // }
            }


            var request = {
                requestTypeStatus: {
                    requestType: {
                        id: 2
                    },
                    requestStatus: {
                        id: 1
                    }
                },
                requestHST: [{
                    comments: null
                }],
                requestAirBook: {
                    pnrNumber: vm.bookinginfo.data.response.pnrNum,
                    airLine: {
                        code: vm.bookinginfo.data.response.flLegGroup[0].segments[0].validatingCompany,
                        name: vm.getAirLineName(vm.bookinginfo.data.response.flLegGroup[0].segments[0].validatingCompany)
                    },
                    journeyType: {
                        id: vm.bookinginfo.data.response.tripType.toLowerCase() == "o" ?
                            1 : vm.bookinginfo.data.response.tripType.toLowerCase() == "r" ? 2 : 3
                    },
                    leg: tripDetails,
                    node: {
                        id: vm.commonStore.agencyNode.loginNode.code.replace(/^\D+/g, '')
                    },
                    provider: {
                        id: vm.supplierCode
                    },
                    service: {
                        id: 1 // flights
                    },
                    requestAirBookPax: vm.bookinginfo.data.response.travelerInfo.map(function (e, index) {
                        // var costBreakUp = vm.bookinginfo.data.response.costBreakuppax[index];
                        var costBreakUp = vm.bookinginfo.data.response.costBreakuppax.filter(function (f) {
                            return e.passengerType == f.paxType
                        });
                        var debitData = {
                            debitSplitupDbList: [{
                                currencyFinalList: [{
                                        amount: costBreakUp[0].paxtotalBaseNet,
                                        fareType: {
                                            id: 1
                                        }
                                    },
                                    {
                                        amount: costBreakUp[0].paxtotalTaxNet,
                                        fareType: {
                                            id: 2
                                        }
                                    }
                                ]
                            }]
                        };

                        return {
                            pax: {
                                firstName: e.givenName,
                                surName: e.surname,
                                age: new Date().getFullYear() - new Date(e.birthDate).getFullYear(),
                                gender: ["mr", "master"].indexOf(e.namePrefix.toLowerCase()) != -1 ? "M" : "F",
                                nationality: {
                                    code: e.nationality
                                },
                                residence: {
                                    code: e.nationality
                                },
                                lead: e.leadPax,
                                paxType: {
                                    code: e.passengerType
                                },
                                title: {
                                    id: e.namePrefix.toLowerCase() == "mr" ?
                                        1 : e.namePrefix.toLowerCase() == "miss" ?
                                        2 : e.namePrefix.toLowerCase() == "ms" ?
                                        4 : e.namePrefix.toLowerCase() == "mrs" ?
                                        3 : e.namePrefix.toLowerCase() == "master" ? 5 : 1
                                },
                                flightPax: {
                                    passportNumber: e.documentNumber,
                                    dob: e.birthDate
                                }
                            },
                            debit: debitData
                        }
                    })
                },
                bookingReferenceID:vm.bookinginfo.data.response.bookingRefId ? vm.bookinginfo.data.response.bookingRefId :""
            }

            var axiosConfig = {
                headers: {
                    "Content-Type": "application/json",
                    Authorization: "Bearer " + window.localStorage.getItem("accessToken")
                }
            };

            var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl;
            var port = vm.commonStore.hubUrls.hubConnection.ipAddress;
            var offlineRequest = vm.commonStore.hubUrls.hubConnection.hubServices.flights.offlineRequest;
            var offlineRequestFare = vm.commonStore.hubUrls.hubConnection.hubServices.flights.offlineRequestFareWithBookingRef + vm.bookinginfo.data.response.bookingRefId;
            var offlineRequestTicketing = vm.commonStore.hubUrls.hubConnection.hubServices.flights.offlineRequestTicketing;
            axios.post(hubUrl + port + offlineRequestFare, request, axiosConfig).then(function (response) {
                var responseData = response.data.data;
                if (response.data.code == 200 && responseData.sealed != undefined && responseData.sealed != '') {
                    var insertReq = {
                        request: request,
                        sealed: responseData.sealed,
                        paymentMode: vm.bookinginfo.data.response.paymentModeId
                    };
                    axios.post(hubUrl + port + offlineRequestTicketing, insertReq, axiosConfig).then(function (response) {
                        if (response.data.code == 201 && response.data.message != undefined && response.data.message != '') {
                            alertify.alert(vm.Alert_Success_Label, vm.Issue_Offline_Ticket_Msg).set('label', vm.Ok_Label);
                        } else {
                            alertify.alert(vm.Alert_Error_Label, vm.Tech_Diff_alert_Msg).set('label', vm.Ok_Label);
                        }
                    }).catch(function (e) {
                        alertify.alert(vm.Alert_Error_Label, vm.Tech_Diff_alert_Msg).set('label', vm.Ok_Label);
                    });
                } else {
                    alertify.alert(vm.Alert_Error_Label, vm.Tech_Diff_alert_Msg).set('label', vm.Ok_Label);
                }
            }).catch(function (e) {
                alertify.alert(vm.Alert_Error_Label, vm.Tech_Diff_alert_Msg).set('label', vm.Ok_Label);
            });

            /* axios.post(hubUrl + port + offlineRequest, request, axiosConfig).then(function (response) {
                 if (response.data.message = "Created Successfully" && response.data.code == 201) {
                     alertify.alert(vm.Alert_Success_Label, vm.Issue_Offline_Ticket_Msg).set('label', vm.Ok_Label);
                     
                 } else {
                     alertify.alert(vm.Alert_Error_Label, vm.Tech_Diff_alert_Msg).set('label', vm.Ok_Label);
 
                 }
             }).catch(function (e) {
                     alertify.alert(vm.Alert_Error_Label, vm.Tech_Diff_alert_Msg).set('label', vm.Ok_Label);
             });*/
        },
        cancelPNR: function ($event, confirm) {
            var self = this;
            self.showLoader = true;
            var ticketed = self.paxInfo.some(function (e) {
                return e.ticketType.toLowerCase() == "ticketed"
            });
            if (ticketed && self.supplierCode != 73) {
                alertify.alert("Message", "Please void the ticket before cancel PNR", function () {
                    setTimeout(function () {
                        document.getElementById("void-ticket").scrollIntoView();
                        self.showLoader = false;
                    }, 100);
                });
                self.showLoader = false;
            } else {
                var vm = this;
                self.showLoader = true;
                var selectCred = null;
                try {
                    selectCred = JSON.parse(window.sessionStorage.getItem("selectCredential"))
                } catch (err) {}
                var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
                var serviceUrl = vm.commonStore.hubUrls.hubConnection.hubServices.flights.AircancelPnr;
                var PostData = {
                    request: {
                        service: "FlightRQ",
                        content: {
                            command: "FlightCancelPnrRQ",
                            supplierSpecific: vm.tripDeRrespose.data.response.content.supplierSpecific ? vm.tripDeRrespose.data.response.content.supplierSpecific : {},
                            cancelPnrRQ: {
                                bookingRefId: bookingRefId,
                                isUserConfirmed: confirm || false
                            }
                        },
                        selectCredential: selectCred,
                        supplierCodes: [self.supplierCode]
                    }
                };
                console.log("FlightCancelPnrRQ: ", JSON.stringify(PostData));

                var config = {
                    axiosConfig: {
                        method: "POST",
                        url: hubUrl + serviceUrl,
                        data: PostData
                    },
                    successCallback: function (response) {
                        console.log("FlightCancelPnr RESPONSE RECEIVED: ", response);
                        //call sendmail for cancelpnr
                        window.sessionStorage.setItem("selectCredential", JSON.stringify(response.data.response.selectCredential));
                        localStorage.accessToken = response.headers.access_token;
                        if ((self.supplierCode == "57") && response.data.response.content.error &&
                            response.data.response.content.error.message.trim().toLowerCase() == "the tickets will be refunded, do you want to proceed?") {
                            self.showLoader = false;
                            alertify.confirm(self.Alert_Warning_Label, response.data.response.content.error.message.trim(), function () {
                                self.showLoader = false;
                                self.cancelPNR(null, true);
                            }, function () {
                                self.getTripDetails('');
                            }).set({
                                'closable': false,
                                'labels': {
                                    ok: 'Yes',
                                    cancel: 'No'
                                }
                            });
                        } else if (response.data.response.content.error && response.data.response.content.error.message) {
                            self.showLoader = false;
                            alertify.alert(self.Alert_Error_Label, response.data.response.content.error.message).set('label', self.Ok_Label);
                        } else {
                            vm.getTripDetails("CANCELBKNG");
                            vm.sendMail("CANCELBKNG");
                        }
                    },
                    errorCallback: function (error) {
                        self.showLoader = false;
                    },
                    showAlert: true,
                    showAdditionalAlerts: true
                }

                mainAxiosRequest(config);
            }


        },
        getCancellationChangarge: function (pax) {
            var vm = this;
            var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = this.commonStore.hubUrls.hubConnection.hubServices.flights.cancelTicketCharge;
            var voidDocInfo = [];
            if (this.voidAllActionStatus) {
                for (let paxObj of this.paxInfo) {
                    var docRow = {
                        documentNumber: paxObj.airTicketNo
                    }
                    voidDocInfo.push(docRow)
                }
            } else {
                voidDocInfo = [{
                    documentNumber: pax.airTicketNo
                }];
            }
            var data = {
                airReservationLocatorCode: {
                    value: vm.PNR
                },
                voidDocumentInfo: voidDocInfo,
                bookingRefId: vm.BookingRef
            };
            var config = {
                axiosConfig: {
                    method: "POST",
                    url: hubUrl + serviceUrl,
                    data: data
                },
                successCallback: function (res) {
                    console.log("RESPONSE RECEIVED: ", res);
                    if (res.data.amount) {
                        alertify.confirm(vm.Alert_Warning_Label, vm.Void_Ticket_Alert_Msg + "<br><br><b>Void charges " + vm.$n(res.data.amount / vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency) + ".</b>", function () {
                            vm.cancelticket(pax, res.data);
                        }, function () {}).set({
                            'closable': false,
                            'labels': {
                                ok: 'Continue',
                                cancel: 'Cancel'
                            }
                        });
                    } else {
                        vm.cancelticket(pax, res.data);
                    }
                },
                errorCallback: function (error) {
                    vm.showLoader = false;
                },
                showAlert: true
            };
            alertify.confirm(vm.Alert_Confirm_Label, "Airline requires Booking to be cancelled. Shall we proceed?", function () {
                mainAxiosRequest(config);
            }, function () {
                //this.showLoader=true;
            }).set({
                'closable': false,
                'labels': {
                    ok: vm.Submit_Button_Label,
                    cancel: vm.Cancel_Button_Label
                }
            });
        },
        //void
        cancelticket: function (pax, cancelTicketChargeData) {
            var self = this;
            // self.showLoader=true;
            var selectCred = null;
            try {
                selectCred = JSON.parse(window.sessionStorage.getItem("selectCredential"))
            } catch (err) {}
            var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = this.commonStore.hubUrls.hubConnection.hubServices.flights.AircancelTicket;
            // alertify.confirm(self.Alert_Confirm_Label, self.Void_Ticket_Alert_Msg
            // , function () {
            var accessToken = window.localStorage.getItem("accessToken");
            self.showLoader = true;
            var voidDocInfo = [];
            if (self.voidAllActionStatus) {
                for (let paxObj of this.paxInfo) {
                    var docRow = {
                        documentNumber: paxObj.airTicketNo
                    }
                    voidDocInfo.push(docRow)
                }
            } else {
                voidDocInfo = [{
                    documentNumber: pax.airTicketNo
                }];
            }
            var PostData = {
                request: {
                    service: "FlightRQ",
                    token: accessToken,
                    supplierCodes: [self.supplierCode],
                    content: {
                        command: "TicketCancelRQ",
                        airVoidDocumentReq: {
                            airReservationLocatorCode: {
                                value: self.PNR
                            },
                            voidDocumentInfo: voidDocInfo,
                            bookingRefId: self.BookingRef,
                            voidCharge: cancelTicketChargeData.amount,
                            sealed: cancelTicketChargeData.sealed,
                            paymentModeId: 7
                        },
                        supplierSpecific: self.tripDeRrespose.data.response.content.supplierSpecific ? self.tripDeRrespose.data.response.content.supplierSpecific : {}
                    },
                    selectCredential: selectCred
                }
            };
            console.log("TicketCancel: ", JSON.stringify(PostData));

            var config = {
                axiosConfig: {
                    method: "POST",
                    url: hubUrl + serviceUrl,
                    data: PostData
                },
                successCallback: function (res) {
                    console.log("TicketCancelRQ RESPONSE RECEIVED: ", res);
                    try {
                        window.sessionStorage.setItem("selectCredential", JSON.stringify(res.data.response.selectCredential));
                         //for amedues
                        if (res.data.response.content.error != undefined) {
                            if (res.data.response.content.error.message) {
                                alertify.alert(self.Alert_Error_Label, res.data.response.content.error.message).set('label', self.Ok_Label);
                            } else {
                                //Server Error
                                alertify.alert(self.Alert_Error_Label, self.Void_error_Ticket_Msg).set('label', self.Ok_Label);
                            }
                            self.showLoader = false;
                        } 
                        //for travel port
                        else if (res.data.response.content.data.airVoidDocumentRsp.voidResultInfo[0].resultType != undefined &&
                            res.data.response.content.data.airVoidDocumentRsp.voidResultInfo[0].resultType == 'Failed') {
                            //Server Error
                            self.showLoader = false;
                            alertify.alert(self.Alert_Error_Label, self.Void_error_Ticket_Msg +
                                '<br/><br/>' + res.data.response.content.error.message +
                                '<br/>' + 'Please contact the Main Agent to process refund of the tickets.').set('label', self.Ok_Label);
                        } else {
                            self.cancelPaxDet = {
                                paxName: pax.namePrefix + " " + pax.givenName + " " + pax.surname,
                                ticketNumber: pax.airTicketNo == null ? 'N/A' : pax.airTicketNo
                            }
                            self.getTripDetails('CANCELTKT');
                        }
                    } catch (err) {
                        self.showLoader = false;
                        console.log('Void Response: ' + err);
                        alertify.alert(self.Alert_Error_Label, self.Tech_Diff_alert_Msg).set('label', self.Ok_Label);
                    }
                },
                errorCallback: function (error) {
                    self.showLoader = false;
                },
                showAlert: true,
                showAdditionalAlerts: true
            };

            mainAxiosRequest(config);

            // }
            // , function () {
            //     //this.showLoader=true;
            // }).set({ 'closable': false, 'labels': { ok: self.Submit_Button_Label, cancel: self.Cancel_Button_Label } });
        },
        GetFlightdurationtime: function (FromGo, ToGo, DepTimeGo, ArriTimeGo) {
            return GetFlightdurationtime(FromGo, ToGo, DepTimeGo, ArriTimeGo);
        },
        airportLocationFromAirportCode: function (airlinecode) {
            return airportLocationFromAirportCode(airlinecode);
        },
        getNoOfStop: function (segment) {
            return ((segment.length - 1 > 0) ? (segment.length - 1) + ' stop' : 'Nonstop');
        },
        getAirLineName: function (airlinecode) {
            return getAirLineName(airlinecode);
        },
        secondsToHms: function (d) {
            d = Number(d);
            var h = Math.floor(d / 3600);
            var m = Math.floor(d % 3600 / 60);
            var s = Math.floor(d % 3600 % 60);
            var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
            var mDisplay = m > 0 ? m + (m == 1 ? " minute " : " minutes ") : "";
            var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
            return hDisplay + mDisplay;
        },
        moment: function (date) {
            return moment(date);
        },
        getTerminal: function (terminal) {
            return isNullorUndefind(terminal) ? 'Terminal N/A' : 'Terminal ' + terminal;
        },
        getCabinName: function (cabin) {
            return getCabinName(cabin);
        },
        getBaggageDeatails: function () {
            var self = this;
            self.baggageDetails = [];
            var tooltip = '';
            this.bookinginfo.data.response.flLegGroup.forEach(function (item, index) {
                item.segments.forEach(function (leg, legindex) {
                    var id = index + '-' + legindex;
                    self.baggageDetails.push({
                        from: leg.departureFrom,
                        to: leg.departureTo,
                        id: id,
                        baggageArray: []
                    });
                    if (!stringIsNullorEmpty(leg.airBagDetails)) {
                        if (leg.airBagDetails.length > 0) {
                            self.divBaggageAreaShow = true;
                            leg.airBagDetails.forEach(function (bag, bagindex) {

                                if (bag.checkinBaggageUnit != undefined) {
                                    if ((bag.checkinBaggageUnit).toLowerCase() == 'piece' || (bag.checkinBaggageUnit).toLowerCase() == 'p' || (bag.checkinBaggageUnit).toLowerCase() == 'n') {
                                        tooltip = 'Airline typically permit 23kg baggage weight per piece';
                                    }
                                    checkinBaggageQuantity = self.cleanBagQuant(bag.checkinBaggageQuantity);
                                    checkinBaggageUnit = self.getBaggageUnit(bag.checkinBaggageUnit);
                                    if (bag.desc) {
                                        tooltip = bag.desc;
                                    }
                                    if (self.supplierCode == "69") {
                                        checkinBaggageUnit = bag.checkinBaggageUnit;
                                    }
                                    self.baggageDetails.some(function (el, i) {
                                        if (el.id === id) {
                                            var found = self.baggageDetails[i].baggageArray.some(function (bdi, bdindex) {
                                                if (bdi.paxType == bag.paxType) {
                                                    self.baggageDetails[i].baggageArray[bdindex].checkinBaggageQuantity = checkinBaggageQuantity;
                                                    self.baggageDetails[i].baggageArray[bdindex].checkinBaggageUnit = checkinBaggageUnit;
                                                    self.baggageDetails[i].baggageArray[bdindex].tooltip = bag.desc ? bag.desc : tooltip;
                                                    return true;
                                                }
                                            });
                                            if (!found) {
                                                self.baggageDetails[i].baggageArray.push({
                                                    checkinBaggageQuantity: checkinBaggageQuantity,
                                                    checkinBaggageUnit: checkinBaggageUnit,
                                                    tooltip: tooltip,
                                                    paxType: bag.paxType
                                                });
                                            }

                                        }

                                    });

                                }
                                if (bag.cabinBaggageUnit != null) {
                                    cabinBaggageQuantity = self.cleanBagQuant(bag.cabinBaggageQuantity);
                                    cabinBaggageUnit = self.getBaggageUnit(bag.cabinBaggageUnit);
                                    self.baggageDetails.some(function (el, i) {
                                        if (el.id === id) {
                                            var found = self.baggageDetails[i].baggageArray.some(function (bdi, bdindex) {
                                                if (bdi.paxType == bag.paxType) {
                                                    self.baggageDetails[i].baggageArray[bdindex].cabinBaggageQuantity = cabinBaggageQuantity;
                                                    self.baggageDetails[i].baggageArray[bdindex].cabinBaggageUnit = cabinBaggageUnit;
                                                    self.baggageDetails[i].baggageArray[bdindex].tooltipCabin = bag.desc ? bag.desc : "";
                                                    return true;
                                                }
                                            });
                                            if (!found) {
                                                self.baggageDetails[i].baggageArray.push({
                                                    cabinBaggageQuantity: cabinBaggageQuantity,
                                                    cabinBaggageUnit: cabinBaggageUnit,
                                                    tooltipCabin: bag.desc ? bag.desc : "",
                                                    paxType: bag.paxType
                                                });
                                            }


                                        }
                                    });
                                }
                            });
                        }
                    } else {
                        self.divBaggageAreaShow = false;
                    }
                });
            });
            setTimeout(() => {
                $('[data-toggle="tooltip"]').tooltip();
            }, 100);
        },
        getBaggageUnit: function (supUnit) {
            if (!stringIsNullorEmpty(supUnit)) {
                switch (supUnit.toLowerCase()) {
                    case "n":
                        return 'Piece';
                        break;
                    case "p":
                        return 'Piece';
                        break;
                    default:
                        return 'KG';
                }
            }
        },
        cleanBagQuant: function (quantity) {
            try {
                quantity = quantity.replace(".0", "");
            } catch (e) {
                quantity = parseInt(quantity);
            }
            return quantity;
        },
        // getbookinginfo: function (bookStatus) {
        //     if (bookStatus == 'OK' || bookStatus == 'TV') {
        //         this.btnIssueTkt = false;
        //         this.btnCancelPnr = true;
        //     }
        //     if (bookStatus == 'TP' || bookStatus == 'PD' || bookStatus == 'RF') {
        //         this.btnIssueTkt = false;
        //     }
        //     if (bookStatus == 'HK' || bookStatus == 'FC' || bookStatus == 'TF') {
        //         this.btnIssueTkt = true;
        //         this.btnCancelPnr = true;
        //     }
        //     if (bookStatus == 'PV') {
        //         this.btnIssueTkt = false;
        //         this.btnCancelPnr = false;
        //     }
        //     if (bookStatus == 'XX') {
        //         this.btnIssueTkt = false;
        //         this.btnCancelPnr = false;
        //     }
        // },
        setEditVoucher: function () {
            var self = this;
            var URL = "/Flights/FlightComponents/EmailTemplates/BookMailAttachment.html";
            var templatecontent = '';
            axios.get(URL)
                .then(response => {
                    templatecontent = self.generatepdftemplate(response.data);
                    templatecontent = templatecontent.replace("#FareDetailsAttachment#", ' ');
                    self.editorData = templatecontent;

                    // $('#voucherHtmlEdit').summernote({
                    //     toolbar: [
                    //         ['style', ['bold', 'italic', 'underline', 'clear']],
                    //         ['font', ['strikethrough', 'superscript', 'subscript']],
                    //         ['fontstyle', ['fontsize', 'fontname', 'color']],
                    //         ['para', ['ul', 'ol', 'paragraph', 'height']],
                    //         ['Edit', ['undo', 'redo']],
                    //         ['insert', ['table', 'hr']],
                    //         ['help', ['codeview', 'help']]
                    //     ]
                    // });
                    $('#voucherHtmlEdit').summernote('code', self.editorData);
                    // console.log(templatecontent);
                    //console.log(self.editorData);
                }).catch(function (error) {
                    console.log(error);
                    this.editorData = '';
                });
        },
        getAirCraftName: function (aireqpcode) {
            return getAirCraftName(aireqpcode);
        },
        getTemplate(template) {
            var url = this.commonStore.hubUrls.emailServices.getTemplate + "AGY75/" + template;
            // var url = this.commonStore.hubUrls.emailServices.getTemplate + vueCommonStore.state.agencyNode.loginNode.code + "/" + template;
            return axios.get(url);
        },
        convertTemplate: function (opt, advertiseImageCMS, type) {
            var self = this;

            var agencyNode = window.localStorage.getItem("agencyNode");
            if (agencyNode) {
                agencyNode = JSON.parse(atob(agencyNode));
            }
            var nodeAddress = agencyNode.loginNode.address;
            if (nodeAddress == undefined) {
                nodeAddress = "";
            }

            var travelerDetails = [];
            var itineraryInformation = [];
            self.bookinginfo.data.response.travelerInfo.forEach(function (pax, index) {
                travelerDetails.push({
                    id: index + 1,
                    givenName: pax.givenName.toUpperCase(),
                    surName: pax.surname.toUpperCase(),
                    frequentName: pax.frequentFlyerList ? pax.frequentFlyerList[0].airLine.code : "",
                    frequentCode: pax.frequentFlyerList ? pax.frequentFlyerList[0].frequentFlyerCode : ""
                });
            });

            self.bookinginfo.data.response.flLegGroup.forEach(function (leg) {
                leg.segments.forEach(function (seg, segIndex) {
                    var checkinBaggage = "";
                    var cabinBaggage = "";

                    var paxDetails = [];

                    self.bookinginfo.data.response.travelerInfo.forEach(function (pax) {

                        var paxType = "(Adult)";
                        if (pax.passengerType == "CHD") {
                            paxType = "(Child)"
                        } else if (pax.passengerType == "INF") {
                            paxType = "(Infant)"
                        }
                        var seatNo = undefined;
                        var addedBaggage = undefined;
                        var mealCount = 0;
                        var paxName = (pax.surname != undefined ? pax.surname : '') + " / " + (pax.givenName != undefined ? pax.givenName : '') + " " + paxType;
                        if (pax.extras != undefined) {
                            pax.extras.forEach(function (extras) {
                                if (extras.from == seg.departureFrom && extras.to == seg.departureTo) {
                                    if (extras.baggage != undefined) {
                                        extras.baggage.forEach(function (baggage) {
                                            var quantity = self.cleanBagQuant(baggage.quantity);
                                            var cabinUnit = self.getBaggageUnit(baggage.unit);
                                            var allBag = quantity + cabinUnit;
                                            addedBaggage = allBag;
                                        });
                                    }
                                    if (extras.meal != undefined) {
                                        extras.meal.forEach(function (meal, i) {
                                            mealCount = Number(mealCount) + 1;
                                        });
                                    }
                                    if (extras.seat != undefined) {
                                        extras.seat.forEach(function (seat) {
                                            if (seat.seatno != undefined) {
                                                seatNo = seat.seatno;
                                            }
                                        });
                                    }
                                }
                            });

                        }


                        checkinBaggage = "";
                        cabinBaggage = "";
                        if (seg.airBagDetails != undefined) {
                            seg.airBagDetails.forEach(function (airBagDetails) {
                                if (pax.passengerType == airBagDetails.paxType) {
                                    if (airBagDetails.checkinBaggageQuantity != undefined && airBagDetails.checkinBaggageUnit != undefined) {
                                        var quantity = self.cleanBagQuant(airBagDetails.checkinBaggageQuantity);
                                        var checkUnit = self.getBaggageUnit(airBagDetails.checkinBaggageUnit);
                                        checkinBaggage = quantity + " " + checkUnit;
                                    }
                                    if (airBagDetails.cabinBaggageQuantity != undefined && airBagDetails.cabinBaggageUnit != undefined) {
                                        var quantity = self.cleanBagQuant(airBagDetails.cabinBaggageQuantity);
                                        var cabinUnit = self.getBaggageUnit(airBagDetails.cabinBaggageUnit);
                                        cabinBaggage = quantity + " " + cabinUnit;
                                    }
                                }
                            });
                        }
                        if (checkinBaggage != '' && cabinBaggage != '') {
                            checkinBaggage = checkinBaggage + " + " + cabinBaggage;
                        } else if (cabinBaggage != '') {
                            checkinBaggage = cabinBaggage;
                        }

                        paxDetails.push({
                            paxName: paxName.toUpperCase(),
                            frequentFlyerList: pax.frequentFlyerList ? pax.frequentFlyerList[0].frequentFlyerCode : undefined,
                            seatNo: seatNo,
                            mealCount: mealCount > 0 ? mealCount : undefined,
                            addedBaggage: addedBaggage,
                            airTicketNo: pax.airTicketNo || undefined,
                            checkinBaggage: checkinBaggage
                        })
                    });

                    itineraryInformation.push({
                        fromCity: self.airportLocationFromAirportCode(seg.departureFrom) || "",
                        fromCityCode: seg.departureFrom,
                        toCity: self.airportLocationFromAirportCode(seg.departureTo) || "",
                        fromAirport: self.airportFromAirportCodeSimple(seg.departureFrom),
                        toAirport: self.airportFromAirportCodeSimple(seg.departureTo),
                        toCityCode: seg.departureTo,
                        terminalFrom: seg.terminalFrom ? (isNaN(seg.terminalFrom) ? seg.terminalFrom : "Terminal&nbsp;" + seg.terminalFrom) : '',
                        terminalTo: seg.terminalTo ? (isNaN(seg.terminalTo) ? seg.terminalTo : "Terminal&nbsp;" + seg.terminalTo) : '',
                        operatingAirline: self.getAirLineName(seg.operatingCompany) || "",
                        marketingAirline: self.getAirLineName(seg.marketingCompany) || "",
                        airlineLogo: window.location.origin + "/Flights/assets/images/AirLines/" + seg.operatingCompany + '.gif',
                        marketingAirlineLogo: window.location.origin + "/Flights/assets/images/AirLines/" + seg.marketingCompany + '.gif',
                        headerDate: self.moment(seg.departureDate).format("ddd DD MMM YY"),
                        departureDate: self.moment(seg.departureDate).format("DD MMMM HH:mm"),
                        departureDay: self.moment(seg.departureDate).format("dddd"),
                        arrivalDate: self.moment(seg.arrivalDate).format("DD MMMM HH:mm"),
                        arrivalDay: self.moment(seg.arrivalDate).format("dddd"),
                        flightDuration: self.GetFlightdurationtime(seg.departureFrom, seg.departureTo, seg.departureDate, seg.arrivalDate),
                        companyCode: seg.marketingCompany + " - " + seg.flightNumber,
                        aircraftName: self.getAirCraftName(seg.flightEquip) || "",
                        cabinClass: self.getCabinName(seg.bookingClass) + " (" + seg.bookingClass + ")",
                        ticketStatus: self.bookinginfo.data.response.bookingStatusName,
                        airlinePnr: seg.airlineLocator ? seg.airlineLocator : "",
                        // checkinBaggage: checkinBaggage,
                        paxDetails: paxDetails,
                    });
                });
            });
            var postData = {
                logoUrl: self.commonStore.agencyNode.loginNode.logo,
                advertImage: advertiseImageCMS,
                tripId: self.bookinginfo.data.response.bookingRefId,
                // travelers: travelerDetails,
                bookingRef: self.bookinginfo.data.response.pnrNum ? self.bookinginfo.data.response.pnrNum : "NA",
                itinerary: itineraryInformation,
                adtCount: self.bookinginfo.data.response.adt,
                chdCount: self.bookinginfo.data.response.chd || undefined,
                infCount: self.bookinginfo.data.response.inf || undefined,
                totalFare: self.commonStore.selectedCurrency + " " + parseFloat(self.bookinginfo.data.response.totalFareGroup.totalBaseNet).toFixed(2),
                totalTax: self.commonStore.selectedCurrency + " " + (parseFloat(self.bookinginfo.data.response.totalFareGroup.totalTaxNet).toFixed(2) - parseFloat(self.paidServicesTotalPrice).toFixed(2)),
                paidServices: self.commonStore.selectedCurrency + " " + parseFloat(self.paidServicesTotalPrice).toFixed(2),
                totalAmount: self.commonStore.selectedCurrency + " " + parseFloat(self.bookinginfo.data.response.totalFareGroup.sellAmount).toFixed(2),
                agentName: agencyNode.firstName + " " + agencyNode.lastName,
                agentContact: agencyNode.contactNumber || "",
                agentEmail: agencyNode.emailId || "",
                agencyName: agencyNode.loginNode.name || "",
                agencyContact: self.agencyphonenumber.number || "",
                agencyEmail: agencyNode.loginNode.email || "",
                agencyAddress: agencyNode.loginNode.address || "",
                cmsNotes: "<span>Ancillary Services: </span> Book In flight and ancillary services at a nominal price and enjoy your flight.",
                includeFare: opt == "Yes" ? true : false
            }
            return self.getTemplate("FlightPDFTemplate").then(function (response) {
                var data = response.data.data;
                var pdfTemplateId = "";
                var pdfFileName = "";
                var htmlTemplate = "";
                if (data.length > 0) {
                    for (var x = 0; x < data.length; x++) {
                        if (data[x].enabled == true && data[x].type == "FlightPDFTemplate") {
                            pdfTemplateId = data[x].id;
                            pdfFileName = data[x].nodeCode;
                            htmlTemplate = data[x].content;
                        }
                    }
                };
                if (type == "pdf") {
                    var generatePdf = self.commonStore.hubUrls.emailServices.generatePdf;
                    var pdfData = {
                        templateID: pdfTemplateId,
                        filename: pdfFileName,
                        content: {
                            trip: postData,
                        },
                    };
                    return axios
                        .post(generatePdf, pdfData)
                        .then(function (response) {
                            return response.data;
                        })
                        .catch(function (error) {
                            self.showLoader = false;
                            return false;
                        });
                } else {
                    var htmlGenerate = self.commonStore.hubUrls.emailServices.htmlGenerate;
                    var htmlData = {
                        template: htmlTemplate,
                        content: {
                            trip: postData
                        }
                    };
                    return axios
                        .post(htmlGenerate, htmlData)
                        .then(function (response) {
                            return response.data;
                        })
                        .catch(function (error) {
                            self.showLoader = false;
                            return false;
                        });
                }
            }).catch(function (error) {
                self.showLoader = false;
                return false;
            });

        },
        downloadPdf: function (response) {
            try {
                var link = document.createElement('a');
                link.href = response.data;
                link.download = response.data.split('/').pop();
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
                this.processingPdf = false;
            } catch (error) {
                alertify.alert(this.Alert_Warning_Label, "Error in generating pdf file.").set('label', this.Ok_Label);
                this.processingPdf = false;
            }
        },
        getPdf: function (opt, type) {
            var self = this;
            if (type == "pdf") {
                self.processingPdf = true;
            }
            var hubUrls = HubServiceUrls;
            var hubUrl = hubUrls.hubConnection.cmsUrl;
            var port = hubUrls.hubConnection.ipAddress;

            var baseUrlPerAgency = hubUrl + port + '/persons/source?path=/B2B/AdminPanel/CMS/AGY' + self.commonStore.agencyNode.loginNode.solutionId;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            var headers = {
                headers: {
                    'content-type': 'text/html',
                    'Accept': 'text/html',
                    'Accept-Language': langauage
                }
            };
            var bannerItinerary = '/Template/Booking Information/Booking Information/Booking Information.ftl';

            return axios.get(baseUrlPerAgency + bannerItinerary, headers).then(function (response) {
                var advertiseImageCMS = response.data.area_List[0].Itinerary.component[0].Banner_Image;
                return self.convertTemplate(opt, advertiseImageCMS, "pdf").then(function (response) {
                    if (type == "pdf") {
                        self.downloadPdf(response)
                    } else {
                        return response;
                    }
                });
            }).catch(function (error) {
                console.log('Error on getting CMS');
                self.convertTemplate(opt, "", "pdf").then(function (response) {
                    if (type == "pdf") {
                        self.downloadPdf(response)
                    } else {
                        return response;
                    }
                });
            });
        },
        printvoucher: function (opt) {
            this.processingPrint = true;
            // var self = this;
            // var URL = "/Flights/FlightComponents/EmailTemplates/BookMailAttachment.html";
            // var fareDetailsAttachment = '<div style="width:100%; float:left; font-family:Open Sans, sans-serif; font-size:13px; color:#000;  line-height: 18px; margin-top: 12px; margin-bottom: 5px;">'
            //     + '<div style = "font-family:Open Sans, sans-serif; font-size:15px; color:#195298; font-weight:700;  padding:0px; margin:0px; margin-bottom: 10px; padding-bottom: 7px; border-bottom: 1px dashed #adacac;">FARE BREAKUP</div>'
            //     + '<table width="100%" cellpadding="5" cellspacing="0" border="1" style="border:1px solid #c7c7c7;border-collapse: collapse;"><td style="font-weight:bold;color:#195298;">BaseFare</td><td style ="font-weight:bold;color:#195298;">Tax</td>'
            //     + (self.paidServicesTotalPrice != 0 ? '<td style="font-weight:bold;color:#195298;">Paid Services</td>' : '') + '<td style="font-weight:bold;color:#195298;">Total</td></tr>'
            //     + '<tr><td>' + self.bookinginfo.data.response.totalFareGroup.sellCurrency + ' ' + self.bookinginfo.data.response.totalFareGroup.totalBaseNet + '</td>'
            //     + '<td>' + self.bookinginfo.data.response.totalFareGroup.sellCurrency + ' ' + (parseFloat(self.bookinginfo.data.response.totalFareGroup.totalTaxNet) - parseFloat(self.paidServicesTotalPrice)) + '</td>'
            //     + (self.paidServicesTotalPrice != 0 ? '<td>' + self.bookinginfo.data.response.totalFareGroup.sellCurrency + ' ' + self.paidServicesTotalPrice + '</td>' : '')
            //     + '<td>' + self.bookinginfo.data.response.totalFareGroup.sellCurrency + ' ' + self.bookinginfo.data.response.totalFareGroup.sellAmount + '</td></tr></table></div>';
            // var templatecontent = '';
            // if (opt == 'No') {
            //     fareDetailsAttachment = "";
            // }
            // axios.get(URL)
            //     .then(response => {
            //         templatecontent = self.generatepdftemplate(response.data);
            //         templatecontent = templatecontent.replace("#FareDetailsAttachment#", fareDetailsAttachment);
            //         var id = (new Date()).getTime();
            //         var myWindow = window.open(window.location.href + '?printerFriendly=true', id, "toolbar=1,scrollbars=1,location=0,statusbar=0,menubar=1,resizable=1,width=800,height=600,left = 240,top = 212");
            //         myWindow.document.write(templatecontent);
            //         myWindow.focus();
            //         setTimeout(
            //             function () {
            //                 myWindow.print();
            //             }, 1000);
            //     }).catch(function (error) {
            //         console.log(error);
            //         this.content = [];
            //     });
            var self = this;
            var hubUrls = HubServiceUrls;
            var hubUrl = hubUrls.hubConnection.cmsUrl;
            var port = hubUrls.hubConnection.ipAddress;

            var baseUrlPerAgency = hubUrl + port + '/persons/source?path=/B2B/AdminPanel/CMS/AGY' + self.commonStore.agencyNode.loginNode.solutionId;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            var headers = {
                headers: {
                    'content-type': 'text/html',
                    'Accept': 'text/html',
                    'Accept-Language': langauage
                }
            };
            var bannerItinerary = '/Template/Booking Information/Booking Information/Booking Information.ftl';

            return axios.get(baseUrlPerAgency + bannerItinerary, headers).then(function (response) {
                var advertiseImageCMS = response.data.area_List[0].Itinerary.component[0].Banner_Image;
                return self.convertTemplate(opt, advertiseImageCMS, "html").then(function (response) {
                    // return response;
                    var id = (new Date()).getTime();
                    var myWindow = window.open(window.location.href + '?printerFriendly=true', id, "toolbar=1,scrollbars=1,location=0,statusbar=0,menubar=1,resizable=1,width=800,height=600,left = 240,top = 212");
                    myWindow.document.write(response.data.replaceAll('\"', ''));
                    myWindow.focus();
                    setTimeout(
                        function () {
                            myWindow.print();
                            self.processingPrint = false;
                        }, 1000);
                });
            }).catch(function (error) {
                console.log('Error on getting CMS');
                self.convertTemplate(opt, "", "template").then(function (response) {
                    // return response;
                    var id = (new Date()).getTime();
                    var myWindow = window.open(window.location.href + '?printerFriendly=true', id, "toolbar=1,scrollbars=1,location=0,statusbar=0,menubar=1,resizable=1,width=800,height=600,left = 240,top = 212");
                    myWindow.document.write(response.data);
                    myWindow.focus();
                    setTimeout(
                        function () {
                            myWindow.print();
                            self.processingPrint = false;
                        }, 1000);
                });
            });
        },
        generatepdftemplate: function (htmlstring) {
            var self = this;
            var logo = '';
            try {
                logo = vueCommonStore.state.agencyNode.loginNode.logo
            } catch (err) {}
            var NodeTelephone = this.agencyphonenumber;
            var paxDetailsAttach = '';
            this.bookinginfo.data.response.travelerInfo.forEach(function (pax) {
                paxDetailsAttach += '<tr> <td>' + pax.namePrefix + ' ' + pax.surname + ' ' + pax.givenName +
                    '</td> <td style="text-align:center;">' + pax.passengerType + '</td> <td style="text-align:center;">' + (pax.airTicketNo || 'N/A') + '</td> </tr>';
            });
            var baggageDetailsAttach = '';
            this.baggageDetails.forEach(function (bag) {
                baggageDetailsAttach += '<div style="width:100%;float:left;margin-bottom:20px;"> <div style="font-family:Open Sans, sans-serif; font-size:15px; color:#545454; font-weight:600; padding:0px; margin:0px; margin-bottom: 1px; padding-bottom: 5px;">' +
                    self.airportLocationFromAirportCode(bag.from) + ' to ' + self.airportLocationFromAirportCode(bag.to) +
                    '</div> <table width="100%" cellpadding="5" cellspacing="0" border="1" style="border:1px solid #c7c7c7;border-collapse: collapse;"> <tr> <td style="font-weight:bold; color:#397dc1;">PAX</td> <td style="font-weight:bold; color:#397dc1;">CHECK IN</td> <td style="font-weight:bold; color:#397dc1;">CABIN</td> </tr>';
                bag.baggageArray.forEach(function (paxbag) {
                    baggageDetailsAttach += '<tr><td><i class="fa fa-child"aria-hidden="true"></i>' + paxbag.paxType + '</td>' +
                        '<td><i class="fa fa-suitcase" aria-hidden="true"></i><span data-toggle="tooltip" data-placement="top" title="' + paxbag.tooltip + '">' + paxbag.checkinBaggageQuantity + '' + paxbag.checkinBaggageUnit + '</span></td>' +
                        '<td><i class="fa fa-suitcase" aria-hidden="true"></i>' + paxbag.cabinBaggageQuantity || '7' + ' ' + paxbag.cabinBaggageUnit || 'KG' + '</td></tr>';
                });
                baggageDetailsAttach += '</table></div>';

            });
            var momentFlDtFormat = "DD MMM' YY, ddd";
            var momentFlTmFormat = "HH:mm";
            var flightSegmentsAttach = '';
            this.bookinginfo.data.response.flLegGroup.forEach(function (leg) {
                flightSegmentsAttach += '<div style="width:100%; float:left; font-family:Open Sans, sans-serif; font-size:13px; color:#949396;line-height: 18px;"> <div style="font-family:Open Sans, sans-serif; font-size:17px; color:#195298; font-weight:700; padding:0px; margin:0px; margin-bottom: 8px; padding-bottom: 5px;">' + self.airportLocationFromAirportCode(leg.from) + ' to ' + self.airportLocationFromAirportCode(leg.to) + '</div>';
                leg.segments.forEach(function (seg) {
                    var airlineLocator = '';
                    var terminalFrom = '';
                    var terminalTo = '';
                    try {
                        airlineLocator = (!isNullorEmpty(seg.airlineLocator) ? '<p><span>Airline PNR :</span>' + seg.airlineLocator + '</p>' : '');
                    } catch (err) {}
                    try {
                        terminalFrom = (!isNullorEmpty(seg.terminalFrom) ? 'Terminal: ' + (seg.terminalFrom) : 'Terminal: N/A');
                    } catch (err) {}
                    try {
                        terminalTo = (!isNullorEmpty(seg.terminalTo) ? 'Terminal: ' + (seg.terminalTo) : 'Terminal: N/A');
                    } catch (err) {}
                    flightSegmentsAttach += '<div style="width:100%;float:left; padding-bottom: 5px; margin-bottom:10px;"> <div style="width:117px; float:left; text-align:left;"> <h1 style="text-align:left;margin:0px;margin-bottom:5px;"><img :src="' +
                        window.location.origin + '/Flights/assets/images/AirLines/' + seg.operatingCompany + '.gif" /></h1> <h2 style="font-family:Open Sans, sans-serif; font-size:14px; color:#000; font-weight:600;margin:0px;">' +
                        self.getAirLineName(seg.operatingCompany) + '</h2> <h2 style="font-family:Open Sans, sans-serif; font-size:12px; color:#000; font-weight:normal;margin:0px;">' +
                        seg.operatingCompany + ' - ' + seg.flightNumber + '</h2> </div> <div style="width:107px; float:left; text-align:right;"> <h2 style="font-family:Open Sans, sans-serif; font-size:14px; color:#000; font-weight:600;margin:0px;">' +
                        self.moment(seg.departureDate).format(momentFlTmFormat) + '</h2> <h2 style="font-family:Open Sans, sans-serif; font-size:14px; color:#000; font-weight:normal;margin:0px;">' +
                        airportFromAirportCode(seg.departureFrom) + '</h2> <h2 style="font-family:Open Sans, sans-serif; font-size:14px; color:#000; font-weight:normal;margin:0px;">' +
                        self.moment(seg.departureDate).format(momentFlDtFormat) + '</h2> <h2 style="font-family:Open Sans, sans-serif; font-size:14px; color:#000; font-weight:normal;margin:0px;">' +
                        terminalFrom + '</h2> </div> <div style="width:125px; float:left; text-align:center;"> <h2 style="font-family:Open Sans, sans-serif; font-size:14px; color:#000; font-weight:normal;margin:0px; padding-top: 26px;"></h2> </div> <div style="width:107px; float:left; text-align:left;"> <h2 style="font-family:Open Sans, sans-serif; font-size:14px; color:#000; font-weight:600;margin:0px;">' +
                        self.moment(seg.arrivalDate).format(momentFlTmFormat) + '</h2> <h2 style="font-family:Open Sans, sans-serif; font-size:14px; color:#000; font-weight:normal;margin:0px;">' +
                        airportFromAirportCode(seg.departureTo) + '</h2> <h2 style="font-family:Open Sans, sans-serif; font-size:14px; color:#000; font-weight:normal;margin:0px;">' +
                        self.moment(seg.arrivalDate).format(momentFlDtFormat) + '</h2> <h2 style="font-family:Open Sans, sans-serif; font-size:14px; color:#000; font-weight:normal;margin:0px;">' +
                        terminalTo + '</h2> </div> <div style="width:220px; float:left; text-align:left;"><h2 style="padding-left: 25px;font-family:Open Sans, sans-serif; font-size:14px; color:#000; font-weight:normal;margin:0px;">Cabin: ' +
                        self.getCabinName(seg.bookingClass) + '</h2>' +
                        '<h2 style="padding-left: 25px; font-family:Open Sans, sans-serif; font-size:14px; color:#000; font-weight:normal;margin:0px;">' + airlineLocator + '</h2>' +
                        '</div> </div>';
                });
            });
            htmlstring = htmlstring.replace("#DOMAIN#", logo);
            htmlstring = htmlstring.replace("#TripID#", this.bookinginfo.data.response.bookingRefId);
            htmlstring = htmlstring.replace("#PNR#", this.bookinginfo.data.response.pnrNum);
            htmlstring = htmlstring.replace("#YEAR#", new Date().getFullYear());
            htmlstring = htmlstring.replace("#AgencyWebsite#", "http://" + window.location.hostname);
            htmlstring = htmlstring.replace("#AgencyTitle#", this.commonStore.agencyNode.loginNode.name);
            htmlstring = htmlstring.replace("#NodeEmail#", this.commonStore.agencyNode.loginNode.email);
            htmlstring = htmlstring.replace("#PaxDetails#", paxDetailsAttach);
            htmlstring = htmlstring.replace("#BaggageDetailsAttach#", baggageDetailsAttach);
            htmlstring = htmlstring.replace("#FlightSegments#", flightSegmentsAttach);
            htmlstring = htmlstring.replace("#NodeTelephone#", NodeTelephone.number);
            //console.log(htmlstring);
            return htmlstring;
        },
        emailEditedVoucher: function () {
            var htmlVal = $('#voucherHtmlEdit').summernote('code');
            //htmlVal=$(htmlVal);
            var spechtml = $(htmlVal).filter(".maindiv")[0]; //$('.maindiv', htmlVal);

            console.log(spechtml.innerHTML);

        },
        emailTicket: function () {
            var vm = this;
            vm.sendMail("EMAILTKT");
        },
        getPaxDetails: function () {
            var self = this;
            // var isCancelled = false;
            // var cancelrol = self.commonStore.commonRoles.hasAirCancelTicket;
            var Travelinfo = self.bookinginfo.data.response.travelerInfo;
            var voidTicket = false;
            var voidAllTicket = false;
            var ticketVoidLimitNotExeeded = true;

            if (self.bookinginfo.data.response.ticketIssuedDate) {
                var timeOffset = self.bookinginfo.data.response.ticketIssuedDate.split('+')[1];
                var sign = self.bookinginfo.data.response.ticketIssuedDate.split('+').length == 2 ? "+" : "-";
                var midninghtDateAndTimeInBookingTimeZone = moment(new Date(self.bookinginfo.data.response.ticketIssuedDate).setHours(23, 0, 0, 0)).utcOffset(sign + timeOffset).format();
                var dateNowInBookingTimeZone = moment.utc(new Date()).utcOffset(sign + timeOffset).format();
                ticketVoidLimitNotExeeded = moment(dateNowInBookingTimeZone).isBefore(midninghtDateAndTimeInBookingTimeZone);
            }

            // var ticketVoidLimitNotExeeded = moment(dateNowInBookingTimeZone).isBefore(moment.parseZone(self.bookinginfo.data.response.ticketIssuedDate));

            if (self.PnrStatus.toLowerCase() == 'ok' || self.PnrStatus.toLowerCase() == 'pv' || self.PnrStatus.toLowerCase() == 'tv' || self.PnrStatus.toLowerCase() == 'xx') {
                // if (self.PnrStatus.toLowerCase() == 'xx') {
                Travelinfo.forEach(function (pax) {
                    if (pax.airTicketStatus == 'Cancelled' || pax.airTicketStatus == 'Ticketed' || pax.airTicketStatus.includes('Refunded')) {
                        // isCancelled = true;
                        var pnrCheckStatus = pax.airTicketStatus;
                        if (pax.airTicketStatus.includes('Refunded')) {
                            pnrCheckStatus = self.PnrStatus;
                        }
                        voidTicket = self.setButtonsAndStatus("voidTicket", pnrCheckStatus, self.supplierCode, self.commonStore.commonRoles.hasAirCancelTicket);
                        voidAllTicket = ticketVoidLimitNotExeeded && self.setButtonsAndStatus("voidAllTicket", pnrCheckStatus, self.supplierCode, self.commonStore.commonRoles.hasAirCancelTicket);
                        if (voidAllTicket) {
                            voidTicket = voidAllTicket;
                        }
                    }
                });
                // }
                // else {
                //     Travelinfo.forEach(function (pax) {
                //         if (pax.airTicketStatus == 'Cancelled' || pax.airTicketStatus == 'Ticketed') {
                //             isCancelled = true;
                //         }
                //     });
                // }
            }
            self.paxInfo = [];
            self.additionalContact = [];
            var isShowVoidAllButton = false;

            var paidServicesTotalPrice = 0;
            Travelinfo.forEach(function (pax, index) {
                var currentTicketStatus = isNullorEmptyToBlank(pax.airTicketStatus, '');
                var showCurrentTicketStatus = (currentTicketStatus.toLowerCase().includes('refunded') || currentTicketStatus.toLowerCase() == 'ticketed' || currentTicketStatus.toLowerCase() == 'cancelled' || currentTicketStatus.toLowerCase() == 'ok' || currentTicketStatus.toLowerCase() == 'xx') ? currentTicketStatus : '';
                showCurrentTicketStatus = (showCurrentTicketStatus.toLowerCase() == 'cancelled' || showCurrentTicketStatus.toLowerCase() == 'xx' ? 'Voided' : showCurrentTicketStatus);

                var paxExtra = pax.extras;
                var total = 0;
                for (var k = 0; k < paxExtra.length; k++) {
                    if (paxExtra[k].baggage) {
                        total += paxExtra[k].baggage.reduce(function (a, e) {
                            return parseFloat(a) + parseFloat(e.price)
                        }, 0);
                    }
                    if (paxExtra[k].seat) {
                        total += paxExtra[k].seat.reduce(function (a, e) {
                            return parseFloat(a) + parseFloat(e.price)
                        }, 0);
                    }
                    if (paxExtra[k].meal) {
                        total += paxExtra[k].meal.reduce(function (a, e) {
                            return parseFloat(a) + parseFloat(e.price)
                        }, 0);
                    }
                }
                paidServicesTotalPrice += total;
                try {
                    self.bookinginfo.data.response.costBreakuppax[index].totalPaidServices = total;
                } catch (error) {}

                try {
                    if (!isNullorEmpty(pax.contact.contactNumber)) {
                        self.leadPaxDetails.phone = pax.contact.contactNumber;
                        self.leadPaxDetShow = true;
                    } else {
                        var code = "";
                        if (pax.contact.phoneList[0].country && pax.contact.phoneList[0].country.code) {
                            code = _.find(self.countryCodes, function (code) {
                                return code[1].toUpperCase() == (pax.contact.phoneList[0].country.code || "").toUpperCase();
                            })
                        }
                        if (code) {
                            code = code[2];
                            self.leadPaxDetails.phone = code + " " + pax.contact.phoneList[0].number;
                            self.leadPaxDetShow = true;
                        } else {
                            self.leadPaxDetails.phone = pax.contact.phoneList[0].number;
                            if (self.leadPaxDetails.phone) {
                                self.leadPaxDetShow = true;
                            }
                        }
                        var data = pax.contact.phoneList.length > pax.contact.emailList.length ? pax.contact.phoneList : pax.contact.emailList;
                        for (var index = 1; index < data.length; index++) {
                            if (pax.contact.phoneList[index].country && pax.contact.phoneList[index].country.code) {
                                var code = _.find(self.countryCodes, function (code) {
                                    return code[1].toUpperCase() == pax.contact.phoneList[index].country.code.toUpperCase();
                                })[2];
                            } else {
                                code = "";
                            }

                            self.additionalContact.push({
                                phone: pax.contact.phoneList[index].number ? code + " " + pax.contact.phoneList[index].number : "",
                                email: pax.contact.emailList[index] ? pax.contact.emailList[index].emailId : "N/A"
                            })
                        }
                    }
                } catch (err) {}
                try {
                    if (!isNullorEmpty(pax.contact.emailId)) {
                        self.leadPaxDetails.email = pax.contact.emailId;
                        self.leadPaxDetShow = true;
                    } else {
                        self.leadPaxDetails.email = pax.contact.emailList[0].emailId;
                        if (self.leadPaxDetails.email) {
                            self.leadPaxDetShow = true;
                        }

                    }
                } catch (err) {}
                var ticketstatus = currentTicketStatus.toLowerCase() != 'ticketed' ? false : true;
                if (pax.airTicketStatus != "Cancelled") {
                    isShowVoidAllButton = true;
                }

                self.paxInfo.push({
                    namePrefix: pax.namePrefix,
                    givenName: pax.givenName,
                    surname: pax.surname,
                    passengerType: pax.passengerType,
                    airTicketNo: pax.airTicketNo,
                    ticketType: showCurrentTicketStatus,
                    frequentFlyerList: pax.frequentFlyerList,
                    // ticketStatus: ticketstatus
                    ticketStatus: pax.airTicketStatus == "Cancelled" ? false : true
                });
            });

            self.paidServicesTotalPrice = paidServicesTotalPrice;
            // (cancelrol && isCancelled && (self.supplierCode == 1 || self.supplierCode == 45 || self.supplierCode == 55)) ? self.voidActionStatus = true : self.voidActionStatus = false;

            self.voidActionStatus = voidTicket;
            self.voidAllActionStatus = voidAllTicket;
            self.voidAllButtonStatus = isShowVoidAllButton;
        },
        getPaxDetailsAfterIssueTicket: function (Travelinfo) {
            var self = this;
            var isCancelled = false;
            var cancelrol = this.commonStore.commonRoles.hasAirCancelTicket;
            if (this.PnrStatus.toLowerCase() == 'ok') {
                Travelinfo.forEach(function (pax) {
                    if (self.PnrStatus.toLowerCase() == 'ok' || self.PnrStatus.toLowerCase() == 'pv' || self.PnrStatus.toLowerCase() == 'tv') {

                        if (pax.airTicketStatus == 'Cancelled' || pax.airTicketStatus == 'Ticketed') {
                            isCancelled = true;
                        }
                    }
                });
            }
            self.paxInfo = [];
            Travelinfo.forEach(function (pax) {
                var currentTicketStatus = isNullorEmptyToBlank(pax.airTicketStatus, '');
                var showCurrentTicketStatus = (currentTicketStatus.toLowerCase().includes('refunded') || currentTicketStatus.toLowerCase() == 'ticketed' || currentTicketStatus.toLowerCase() == 'cancelled' || currentTicketStatus.toLowerCase() == 'ok' || currentTicketStatus.toLowerCase() == 'xx') ? currentTicketStatus : '';
                showCurrentTicketStatus = (showCurrentTicketStatus.toLowerCase() == 'cancelled' || showCurrentTicketStatus.toLowerCase() == 'xx' ? 'Voided' : showCurrentTicketStatus);
                var ticketstatus = currentTicketStatus.toLowerCase() != 'ticketed' ? false : true
                self.paxInfo.push({
                    namePrefix: "",
                    givenName: pax.customerInformation.paxdetails.passengerFirstName,
                    surname: pax.customerInformation.paxdetails.passengerLastName,
                    passengerType: pax.customerInformation.paxdetails.passengerType,
                    airTicketNo: pax.eTickets.eTicketNumber,
                    ticketType: showCurrentTicketStatus,
                    ticketStatus: ticketstatus
                });
            });

            (cancelrol && isCancelled && (this.supplierCode == 1 || this.supplierCode == 45 || self.supplierCode == 55)) ? this.voidActionStatus = true: this.voidActionStatus = false;
            // (cancelrol && isCancelled && (this.supplierCode == 1 || this.supplierCode == 45 || self.supplierCode == 55)) ? this.voidAllActionStatus = true : this.voidAllActionStatus = false;
        },
        sendMail: function (EmailType) {
            var self = this;
            var mailSubject = '';
            var emailStatus = '';
            var type = "VoidB2BRequest";
            if (self.bookinginfo.data.response.bookingStatus == "TF") {
                emailStatus = 'Ticket Failed'
            }
            if (self.bookinginfo.data.response.bookingStatus == "TV" || self.bookinginfo.data.response.bookingStatus == "PV") {
                emailStatus = 'Voided'
            }
            if (self.bookinginfo.data.response.bookingStatus == "HK" || self.bookinginfo.data.response.bookingStatus == "OK") {
                emailStatus = 'Just Confirmed'
            }
            var bookstatus = "Pleased to inform that your flight booking is confirmed.";

            switch (EmailType) {
                case "BOOK":
                    mailSubject = "Flight Confirmed - Trip ID " + bookingRefId + " - " + self.triplocations;
                    break;
                case "TKTISSUE":
                    mailSubject = "Your Ticket Issued - Trip ID " + bookingRefId + " - " + self.triplocations;
                    bookstatus = "Pleased to inform that your flight booking is ticketed.";
                    break;
                case "EMAILTKT":
                    mailSubject = "Your Flight Itinerary - Trip ID " + bookingRefId + " - " + self.triplocations;
                    break;
                case "CANCELBKNG":
                    mailSubject = "Your Booking Cancelled - Trip ID " + bookingRefId + " - " + self.triplocations;
                    emailStatus = 'Cancelled';
                    self.bookingStatusName = 'Booking Cancelled';
                    bookstatus = "Your booking has been cancelled.";
                    break;
                case "CANCELTKT":
                    mailSubject = "Your Ticket Cancelled - Trip ID " + bookingRefId + " - " + self.triplocations;
                    emailStatus = 'Voided';
                    bookstatus = "Your ticket has been voided.";
                    break;
                default:
                    mailSubject = "Your Flight Itinerary - Trip ID " + bookingRefId + " - " + self.triplocations;
                    break;
            }
            let paxList = [];
            if (EmailType == "CANCELTKT") {
                paxList.push({
                    paxName: self.cancelPaxDet.paxName,
                    ticketNumber: self.cancelPaxDet.ticketNumber
                });
            } else {
                // if (self.bookinginfo.data.response.bookingStatus != "TV") {
                self.paxInfo.forEach(function (pax) {
                    paxList.push({
                        paxName: pax.namePrefix + " " + pax.givenName + " " + pax.surname,
                        ticketNumber: pax.airTicketNo == null ? 'Ticket Pending' : pax.airTicketNo,
                        frequentFlyerList: pax.frequentFlyerList
                    });
                });
                // }
            }
            var toEMail = [];
            if (EmailType == "EMAILTKT") {
                if ((isNullorUndefined($('#txtEmailTo').val()) || !isValidEmail($('#txtEmailTo').val()))) {
                    alertify.alert(self.Alert_Invalid_Label, self.Invalid_Email_Msg).set('label', self.Ok_Label);
                    return;
                }
                toEMail.push($('#txtEmailTo').val());
            } else {
                toEMail.push(self.commonStore.agencyNode.emailId);
            }

            var fareBrkup = {};
            self.travAncillServ = [];
            self.BaggDetAttchMail = [];
            var bagdetAtchMail = null;
            var travelerAncilarys = null;
            var currency = self.bookinginfo.data.response.totalFareGroup.sellCurrency;
            if (EmailType != "CANCELTKT" && self.bookinginfo.data.response.bookingStatus != "TV") {
                type = "BookMailB2BRequest";
                fareBrkup = {
                    baseFare: currency + ' ' + self.bookinginfo.data.response.totalFareGroup.totalBaseNet,
                    taxFare: currency + ' ' + parseFloat(parseFloat(self.bookinginfo.data.response.totalFareGroup.totalTaxNet) - parseFloat(self.paidServicesTotalPrice)).toFixed(2),
                    paidServices: self.paidServicesTotalPrice != 0 ? currency + ' ' + parseFloat(self.paidServicesTotalPrice).toFixed(2) : "",
                    totalFare: currency + ' ' + self.bookinginfo.data.response.totalFareGroup.sellAmount
                };
                var travelInfo = self.bookinginfo.data.response.travelerInfo;
                var legInfo = self.bookinginfo.data.response.flLegGroup;
                try {
                    travelInfo.forEach(function (trvInfo) {
                        if (trvInfo.extras.length > 0) {
                            var segInfo = [];
                            var seatsInfo = [];
                            var mealsInfo = [];
                            var baggagesInfo = [];
                            var count = 0;
                            trvInfo.extras.forEach(function (exts) {
                                count++;
                                var sizeCnt = trvInfo.extras.length == 1 ? null : count != trvInfo.extras.length ? "1" : null;
                                var seg = {
                                    segment: exts.from + '-' + exts.to,
                                    size: sizeCnt
                                };
                                segInfo.push(seg);
                                if (exts.baggage != undefined && exts.baggage.length > 0) {
                                    exts.baggage.forEach(function (bagg) {
                                        var bag = {
                                            baggage: bagg.quantity + ' ' + bagg.unit,
                                            size: sizeCnt
                                        };
                                        baggagesInfo.push(bag);
                                    });
                                } else {
                                    var bag = {
                                        baggage: "None",
                                        size: sizeCnt
                                    };
                                    baggagesInfo.push(bag);
                                }
                                if (exts.meal != undefined && exts.meal.length > 0) {
                                    exts.meal.forEach(function (meaal) {
                                        var meal = {
                                            meal: meaal.meal,
                                            size: sizeCnt
                                        };
                                        mealsInfo.push(meal);
                                    });
                                } else {
                                    var meal = {
                                        meal: "None",
                                        size: sizeCnt
                                    };
                                    mealsInfo.push(meal);
                                }
                                if (exts.seat != undefined && exts.seat.length > 0) {
                                    exts.seat.forEach(function (seaat) {
                                        var seat = {
                                            seat: seaat.code,
                                            size: sizeCnt
                                        };
                                        seatsInfo.push(seat);
                                    });
                                } else {
                                    var seat = {
                                        seat: "None",
                                        size: sizeCnt
                                    };
                                    seatsInfo.push(seat);
                                }
                            });
                            var travAnc = {
                                traveller: trvInfo.namePrefix + ' ' + trvInfo.givenName + ' ' + trvInfo.surname,
                                segments: segInfo,
                                seats: seatsInfo,
                                meals: mealsInfo,
                                baggages: baggagesInfo,
                                eTicket: self.bookinginfo.data.response.pnrNum
                            }
                            self.travAncillServ.push(travAnc);
                        }
                    });
                    travelerAncilarys = self.travAncillServ;
                } catch (err) {}
                try {
                    legInfo.forEach(function (leg) {
                        leg.segments.forEach(function (seg) {
                            var paxs = [];
                            var checkIns = [];
                            var cabins = [];
                            var adtbag = adtbag = seg.airBagDetails.filter(function (bagg) {
                                return bagg.paxType.toLowerCase() == 'adt'
                            });
                            var chdbag = seg.airBagDetails.filter(function (bagg) {
                                return bagg.paxType.toLowerCase() == 'chd'
                            });
                            var infbag = infbag = seg.airBagDetails.filter(function (bagg) {
                                return bagg.paxType.toLowerCase() == 'inf'
                            });
                            var adtcabbag = [];
                            var adtchkbag = [];
                            var chdcabbag = [];
                            var chdchkbag = [];
                            var infcabbag = [];
                            var infchkbag = [];
                            try {
                                if (adtbag.length > 0) {
                                    var sizeCnt = chdbag.length > 0 ? "1" : infbag.length > 0 ? "1" : null;
                                    adtcabbag = adtbag.filter(function (cab) {
                                        return cab.cabinBaggageQuantity
                                    });
                                    adtchkbag = adtbag.filter(function (chk) {
                                        return chk.checkinBaggageQuantity
                                    });
                                    var paxx = {
                                        pax: 'ADT',
                                        size: sizeCnt
                                    };
                                    paxs.push(paxx);
                                    var checkinn = {
                                        checkIn: self.cleanBagQuant(adtchkbag[0].checkinBaggageQuantity) + ' ' + self.getBaggageUnit(adtchkbag[0].checkinBaggageUnit),
                                        size: sizeCnt
                                    };
                                    checkIns.push(checkinn);
                                    var cabinn = {
                                        cabin: self.cleanBagQuant(adtcabbag[0].cabinBaggageQuantity) + ' ' + self.getBaggageUnit(adtcabbag[0].cabinBaggageUnit),
                                        size: sizeCnt
                                    };
                                    cabins.push(cabinn);
                                }
                                if (chdbag.length > 0) {
                                    var sizeCnt = infbag.length > 0 ? "1" : null;
                                    chdcabbag = chdbag.filter(function (cab) {
                                        return cab.cabinBaggageQuantity
                                    });
                                    chdchkbag = chdbag.filter(function (chk) {
                                        return chk.checkinBaggageQuantity
                                    });
                                    var paxx = {
                                        pax: 'CHD',
                                        size: sizeCnt
                                    };
                                    paxs.push(paxx);
                                    var checkinn = {
                                        checkIn: self.cleanBagQuant(chdchkbag[0].checkinBaggageQuantity) + ' ' + self.getBaggageUnit(chdchkbag[0].checkinBaggageUnit),
                                        size: sizeCnt
                                    };
                                    checkIns.push(checkinn);
                                    var cabinn = {
                                        cabin: self.cleanBagQuant(chdcabbag[0].cabinBaggageQuantity) + ' ' + self.getBaggageUnit(chdcabbag[0].cabinBaggageUnit),
                                        size: sizeCnt
                                    };
                                    cabins.push(cabinn);
                                }
                                if (infbag.length > 0) {
                                    infcabbag = infbag.filter(function (cab) {
                                        return cab.cabinBaggageQuantity
                                    });
                                    infchkbag = infbag.filter(function (chk) {
                                        return chk.checkinBaggageQuantity
                                    });
                                    var paxx = {
                                        pax: 'INF',
                                        size: null
                                    };
                                    paxs.push(paxx);
                                    var checkinn = {
                                        checkIn: self.cleanBagQuant(infchkbag[0].checkinBaggageQuantity) + ' ' + self.getBaggageUnit(infchkbag[0].checkinBaggageUnit),
                                        size: null
                                    };
                                    checkIns.push(checkinn);
                                    var cabinn = {
                                        cabin: self.cleanBagQuant(infcabbag[0].cabinBaggageQuantity) + ' ' + self.getBaggageUnit(infcabbag[0].cabinBaggageUnit),
                                        size: null
                                    };
                                    cabins.push(cabinn);
                                }
                            } catch (err) {}
                            var bagDet = {
                                segment: seg.departureFrom + '-' + seg.departureTo,
                                paxs: paxs,
                                checkIns: checkIns,
                                cabins: cabins
                            }
                            self.BaggDetAttchMail.push(bagDet);
                        });
                    });
                    bagdetAtchMail = self.BaggDetAttchMail;
                } catch (err) {}
            }
            var leadPaxDet = null;
            if (self.leadPaxDetails.phone != null || self.leadPaxDetails.email != null) {
                self.leadPaxDetails.phone = self.leadPaxDetails.phone == null ? "N/A" : self.leadPaxDetails.phone;
                self.leadPaxDetails.email = self.leadPaxDetails.email == null ? "N/A" : self.leadPaxDetails.email;
                leadPaxDet = self.leadPaxDetails
            }
            var logoUrl = '';
            var fromEmail = [{
                emailId: ""
            }];

            var ccEmails = null;
            switch (EmailType) {
                case "BOOK":
                    ccEmails = _.filter(vueCommonStore.state.agencyNode.loginNode.emailList,
                        function (o) {
                            return o.emailId != undefined && o.emailTypeId == 4 && o.emailType.toLowerCase() == 'air confirmation';
                        });
                    break;
                case "TKTISSUE":
                    ccEmails = _.filter(vueCommonStore.state.agencyNode.loginNode.emailList,
                        function (o) {
                            return o.emailId != undefined && o.emailTypeId == 5 && o.emailType.toLowerCase() == 'air issuance';
                        });
                    break;
                case "CANCELBKNG":
                    ccEmails = _.filter(vueCommonStore.state.agencyNode.loginNode.emailList,
                        function (o) {
                            return o.emailId != undefined && o.emailTypeId == 6 && o.emailType.toLowerCase() == 'air cancellation';
                        });
                    break;
                default:
                    break;
            }

            ccEmails = _.map(ccEmails, function (o) {
                return o.emailId;
            });

            try {
                logoUrl = vueCommonStore.state.agencyNode.loginNode.logo;
                // fromEmail = vueCommonStore.state.agencyNode.loginNode.parentEmailId;
                fromEmail = _.filter(vueCommonStore.state.agencyNode.loginNode.emailList,
                    function (o) {
                        return o.emailTypeId == 3 && o.emailType.toLowerCase() == "support";
                    });
            } catch (err) {}
            var postData = {
                type: type,
                toEmails: Array.isArray(toEMail) ? toEMail : [toEMail],
                fromEmail: fromEmail[0].emailId || self.commonStore.fallBackEmail,
                ccEmails: ccEmails.length == 0 ? null : ccEmails,
                bccEmails: null,
                primaryColor: "#" + self.commonStore.agencyNode.loginNode.lookNfeel.primary,
                secondaryColor: "#" + self.commonStore.agencyNode.loginNode.lookNfeel.secondary,
                subject: mailSubject,
                logoUrl: logoUrl,
                agencyName: self.commonStore.agencyNode.loginNode.name,
                agencyAddress: self.commonStore.agencyNode.loginNode.cityName,
                year: new Date().getFullYear().toString(),
                agencyNumber: self.agencyphonenumber.number || "",
                agencyEmail: fromEmail[0].emailId || "",
                fullName: self.commonStore.agencyNode.firstName + " " + self.commonStore.agencyNode.lastName,
                tripName: self.triplocations,
                displayStatus: bookstatus,
                status: emailStatus,
                bookingRefId: bookingRefId,
                pnrNumber: self.PNR,
                totalFare: currency + ' ' + self.bookinginfo.data.response.totalFareGroup.sellAmount,
                segments: self.segments,
                paxList: paxList,
                leadPaxDetails: leadPaxDet,
                baggage: bagdetAtchMail,
                fareBreakup: self.emailOption == "no" ? null : fareBrkup,
                ticketInfo: "e-ticket",
                withAttachment: (EmailType == "TKTISSUE" || (EmailType == "EMAILTKT" && self.bookinginfo.data.response.bookingStatus == "OK")) ? true : false,

            };
            self.processingEmail = true;
            self.getTemplate("FlightEmailTemplate").then(function (templateResponse) {
                var data = templateResponse.data.data;
                var emailTemplate = "";
                if (data.length > 0) {
                    for (var x = 0; x < data.length; x++) {
                        if (data[x].enabled == true && data[x].type == "FlightEmailTemplate") {
                            emailTemplate = data[x].content;
                            break;
                        }
                    }
                };
                var htmlGenerate = self.commonStore.hubUrls.emailServices.htmlGenerate
                var emailData = {
                    template: emailTemplate,
                    content: postData
                };
                axios.post(htmlGenerate, emailData)
                    .then(function (htmlResponse) {
                        if (EmailType == "TKTISSUE" || EmailType == "EMAILTKT") {
                            self.getPdf(self.emailOption == "no" ? "No" : "Yes", 'email').then(function (response) {
                                var emailPostData = {
                                    type: "AttachmentRequest",
                                    toEmails: Array.isArray(toEMail) ? toEMail : [toEMail],
                                    fromEmail: fromEmail[0].emailId || self.commonStore.fallBackEmail,
                                    ccEmails: ccEmails.length == 0 ? null : ccEmails,
                                    bccEmails: null,
                                    subject: mailSubject,
                                    attachmentPath: response.data,
                                    html: htmlResponse.data.data
                                };
                                var mailUrl = self.commonStore.hubUrls.emailServices.emailApiWithAttachment;
                                sendMailService(mailUrl, emailPostData);
                                self.processingEmail = false;
                            });
                        } else {
                            var emailPostData = {
                                type: "AttachmentRequest",
                                toEmails: Array.isArray(toEMail) ? toEMail : [toEMail],
                                fromEmail: fromEmail[0].emailId || self.commonStore.fallBackEmail,
                                ccEmails: ccEmails.length == 0 ? null : ccEmails,
                                bccEmails: null,
                                subject: mailSubject,
                                attachmentPath: "",
                                html: htmlResponse.data.data
                            };
                            var mailUrl = self.commonStore.hubUrls.emailServices.emailApiWithAttachment;
                            sendMailService(mailUrl, emailPostData);
                            self.processingEmail = false;
                        }
                        window.sessionStorage.setItem('sendbookmail', true);
                    }).catch(function (error) {
                        self.showLoader = false;
                        return false;
                    })
            });
        },
        setsegments: function () {
            var self = this;
            self.segments = [];
            var Segmentinfo = self.bookinginfo.data.response.flLegGroup;
            Segmentinfo.forEach(function (leg) {
                leg.segments.forEach(function (seg) {
                    self.segments.push({
                        airlineName: self.getAirLineName(seg.operatingCompany),
                        airlineCode: seg.operatingCompany,
                        flightNumber: seg.flightNumber,
                        departureAirport: airportFromAirportCode(seg.departureFrom),
                        departureCity: self.airportLocationFromAirportCode(seg.departureFrom),
                        arrivalAirport: airportFromAirportCode(seg.departureTo),
                        arrivalCity: self.airportLocationFromAirportCode(seg.departureTo),
                        deparureTime: self.moment(seg.departureDate).format('HH:mm'),
                        arrivalTime: self.moment(seg.arrivalDate).format('HH:mm'),
                        arrivalDate: self.moment(seg.arrivalDate).format("DD MMM' YY, ddd"),
                        deparureDate: self.moment(seg.departureDate).format("DD MMM' YY, ddd"),
                        airlineLogoUrl: window.location.origin + "/Flights/assets/images/AirLines/" + seg.operatingCompany + ".gif",
                        // airlineLogoUrl: "https://www.oneviewitsolutions.com/Flights/images/airlines/" + seg.operatingCompany + ".gif",
                        bookingClass: self.getCabinName(seg.bookingClass) || "",
                        flightDuration: self.GetFlightdurationtime(seg.departureFrom, seg.departureTo, seg.departureDate, seg.arrivalDate),
                    });

                });
            });
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            var vm = this;
            var huburl = vm.commonStore.hubUrls.hubConnection.cmsUrl;
            var portno = vm.commonStore.hubUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            //var langauage = 'ar';
            self.dir = langauage == "ar" ? "rtl" : "ltr";

            var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + self.commonAgencyCode + '/Template/Flight Booking Info Page/Flight Booking Info Page/Flight Booking Info Page.ftl';
            axios.get(cmsPage, {
                headers: {
                    'content-type': 'text/html',
                    'Accept': 'text/html',
                    'Accept-Language': langauage
                }
            }).then(function (response) {
                self.content = response.data;
                if (response.data.area_List.length > 0) {
                    var tripComp = self.pluckcom('Trip_Section', self.content.area_List); // response.data.area_List[1].Trip_Section;
                    var bookingComp = self.pluckcom('Booking_Information', self.content.area_List); // response.data.area_List[2].Booking_Information;
                    var flightComp = self.pluckcom('Flight_Details', self.content.area_List); // response.data.area_List[3].Flight_Details;
                    var passengersComp = self.pluckcom('Passengers_Details', self.content.area_List); //response.data.area_List[4].Passengers_Details;
                    var baggageComp = self.pluckcom('Baggage_Details', self.content.area_List); // response.data.area_List[5].Baggage_Details;
                    var extrasComp = self.pluckcom('Extras_Details', self.content.area_List); // response.data.area_List[6].Extras_Details;
                    var helpComp = self.pluckcom('Help_and_Support', self.content.area_List); // response.data.area_List[7].Help_and_Support;
                    var alertComp = self.pluckcom('Alerts', self.content.area_List); // response.data.area_List[8].Alerts;
                    var PassengerContact = self.pluckcom('PassengerContactSection', self.content.area_List);

                    //Trip Section
                    self.Trip_Label = self.pluckcom('Trip_Label', tripComp.component);
                    self.Email_Itinerary_Label = self.pluckcom('Email_Itinerary_Label', tripComp.component);
                    self.Fare_Rules_Label = self.pluckcom('Fare_Rules_Label', tripComp.component);
                    self.Mini_Rules_Label = self.pluckcom('Mini_Rules_Label', tripComp.component);
                    self.Download_Itinerary_Label = self.pluckcom('Download_Itinerary_Label', tripComp.component);
                    self.Print_Itinerary_Label = self.pluckcom('Print_Itinerary_Label', tripComp.component);
                    self.Edit_Voucher_Label = self.pluckcom('Edit_Voucher_Label', tripComp.component);
                    self.Email_Ticket_Label = self.pluckcom('Email_Ticket_Label', tripComp.component);
                    self.Email_To_Label = self.pluckcom('Email_To_Label', tripComp.component);
                    self.Close_Button_Label = self.pluckcom('Close_Button_Label', tripComp.component);
                    self.Send_Button_Label = self.pluckcom('Send_Button_Label', tripComp.component);
                    self.Issue_Button_Label = self.pluckcom('Issue_Button_Label', tripComp.component);
                    self.Cancel_PNR_Button_Label = self.pluckcom('Cancel_PNR_Button_Label', tripComp.component);
                    self.Issue_Ticket_Popup_Label = self.pluckcom('Issue_Ticket_Popup_Label', tripComp.component);
                    self.Issue_ticket_popup_Content = self.pluckcom('Issue_ticket_popup_Content', tripComp.component);
                    self.Cancel_popup_Label = self.pluckcom('Cancel_popup_Label', tripComp.component);
                    self.Cancel_PNR_popup_Content = self.pluckcom('Cancel_PNR_popup_Content', tripComp.component);
                    self.Cancel_Button_Label = self.pluckcom('Cancel_Button_Label', tripComp.component);
                    self.Trip_Header_Label = self.pluckcom('Trip_Header_Label', tripComp.component);
                    self.Cancel_popup_Button_Label = self.pluckcom('Cancel_popup_Button_Label', tripComp.component);
                    self.Email_eTicket_Label = self.pluckcom('Email_eTicket_Label', tripComp.component);
                    self.Download_eTicket_Label = self.pluckcom('Download_eTicket_Label', tripComp.component);
                    self.Print_eTicket_Label = self.pluckcom('Print_eTicket_Label', tripComp.component);
                    self.Yes_Button_Label = self.pluckcom('Yes_Button_Label', tripComp.component);
                    self.No_Button_Label = self.pluckcom('No_Button_Label', tripComp.component);
                    self.Reprice_Same_RBD_Popup_Heading = self.pluckcom('Reprice_Same_RBD_Popup_Heading', tripComp.component);
                    self.Reprice_Lowest_RBD_Popup_Heading = self.pluckcom('Reprice_Lowest_RBD_Popup_Heading', tripComp.component);
                    self.Confirm_Button_Label = self.pluckcom('Confirm_Button_Label', tripComp.component);
                    self.Reprice_Same_RBD_Button_Label = self.pluckcom('Reprice_Same_RBD_Button_Label', tripComp.component);
                    self.Reprice_Lowest_RBD_Button_Label = self.pluckcom('Reprice_Lowest_RBD_Button_Label', tripComp.component);
                    self.Segment_Heading = self.pluckcom('Segment_Heading', tripComp.component);
                    self.Issue_Offline_Ticket_Button_Label = self.pluckcom('Issue_Offline_Ticket_Button_Label', tripComp.component);
                    self.Issue_Offline_Ticket_Popup_Label = self.pluckcom('Issue_Offline_Ticket_Popup_Label', tripComp.component);
                    self.Issue_offline_ticket_popup_Content = self.pluckcom('Issue_offline_ticket_popup_Content', tripComp.component);

                    //Booking Section
                    self.Booking_Information_Label = self.pluckcom('Booking_Information_Label', bookingComp.component);
                    self.Trip_ID_Label = self.pluckcom('Trip_ID_Label', bookingComp.component);
                    self.Status_Label = self.pluckcom('Status_Label', bookingComp.component);
                    self.PNR_Label = self.pluckcom('PNR_Label', bookingComp.component);
                    self.Total_Label = self.pluckcom('Total_Label', bookingComp.component);
                    self.Payment_Mode_Label = self.pluckcom('Payment_Mode_Label', bookingComp.component);
                    self.Payment__Status_Label = self.pluckcom('Payment__Status_Label', bookingComp.component);
                    self.Fare_Breakup_Label = self.pluckcom('Fare_Breakup_Label', bookingComp.component);
                    self.Adult_Label = self.pluckcom('Adult_Label', bookingComp.component);
                    self.Child_Label = self.pluckcom('Child_Label', bookingComp.component);
                    self.Infant_Label = self.pluckcom('Infant_Label', bookingComp.component);
                    self.Fare_Label = self.pluckcom('Fare_Label', bookingComp.component);
                    self.Taxes_and_Fees_Label = self.pluckcom('Taxes_and_Fees_Label', bookingComp.component);
                    self.Taxes_Label = self.pluckcom('Taxes_Label', bookingComp.component);
                    self.Discount_Label = self.pluckcom('Discount_Label', bookingComp.component);
                    self.Service_Fee_Label = self.pluckcom('Service_Fee_Label', bookingComp.component);
                    self.Commission_Label = self.pluckcom('Commission_Label', bookingComp.component);
                    self.Close_Button_Label = self.pluckcom('Close_Button_Label', bookingComp.component);
                    self.Booking_Date_Label = self.pluckcom('Booking_Date_Label', bookingComp.component);
                    self.Booking_Note_Label = self.pluckcom('Booking_Note_Label', bookingComp.component);
                    self.Coupon_Discount_Label = self.pluckcom('Coupon_Discount_Label', bookingComp.component);
                    self.Base_Fare_Label = self.pluckcom('Base_Fare_Label', bookingComp.component);
                    //Flight Section
                    self.Flight_Details_Label = self.pluckcom('Flight_Details_Label', flightComp.component);
                    self.to_Label = self.pluckcom('to_Label', flightComp.component);
                    self.Flight_Cabin_Label = self.pluckcom('Cabin_Label', flightComp.component);
                    self.RBD_Label = self.pluckcom('RBD_Label', flightComp.component);
                    self.AirLine_Pnr_Label = self.pluckcom('AirLine_Pnr_Label', flightComp.component);

                    self.summary = self.pluckcom('Summary_Label', flightComp.component);
                    self.Detail = self.pluckcom('Details_Label', flightComp.component);
                    self.after = self.pluckcom('After_Departure_Label', flightComp.component);
                    self.before = self.pluckcom('Before_Departure_Label', flightComp.component);
                    self.rule = self.pluckcom('Rules_Label', flightComp.component);
                    self.Fare_Rules = self.pluckcom('Fare_Rules_Label', flightComp.component);


                    //Passengers Section
                    self.Passengers_Label = self.pluckcom('Passengers_Label', passengersComp.component);
                    self.Pax_BreakUp_Label = self.pluckcom('Pax_BreakUp_Label', passengersComp.component);
                    self.Name_Label = self.pluckcom('Name_Label', passengersComp.component);
                    self.Type_Label = self.pluckcom('Type_Label', passengersComp.component);
                    self.e_Ticket_Number_Label = self.pluckcom('e_Ticket_Number_Label', passengersComp.component);
                    self.Status_Label = self.pluckcom('Status_Label', passengersComp.component);
                    self.Action = self.pluckcom('Action', passengersComp.component);
                    self.Fare_Pax_Breakup_Label = self.pluckcom('Fare_Pax_Breakup_Label', passengersComp.component);
                    self.Fare_Label = self.pluckcom('Fare_Label', passengersComp.component);
                    self.Taxes_and_Fees_Label = self.pluckcom('Taxes_and_Fees_Label', passengersComp.component);
                    self.Taxes_Label = self.pluckcom('Taxes_Label', passengersComp.component);
                    self.Discount_Label = self.pluckcom('Discount_Label', passengersComp.component);
                    self.Service_Fee_Label = self.pluckcom('Service_Fee_Label', passengersComp.component);
                    self.Commission_Label = self.pluckcom('Commission_Label', passengersComp.component);
                    self.Toatl_Label = self.pluckcom('Toatl_Label', passengersComp.component);
                    self.Close_Button_Label = self.pluckcom('Close_Button_Label', passengersComp.component);
                    self.Void_Button_Label = self.pluckcom('Void_Button_Label', passengersComp.component);
                    self.VoidAll_Button_Label = self.pluckcom('Void_All_Button_Label', passengersComp.component);

                    //Baggage Section
                    self.Baggage_Details_Label = self.pluckcom('Baggage_Details_Label', baggageComp.component);
                    self.Cabin_Label = self.pluckcom('Cabin_Label', baggageComp.component);
                    self.Baggage_Pax_Label = self.pluckcom('Pax_Label', baggageComp.component);
                    self.to_Label = self.pluckcom('to_Label', baggageComp.component);
                    self.Check_in_Label = self.pluckcom('Check_in_Label', baggageComp.component);

                    //Extras Section
                    self.Extras_Label = self.pluckcom('Extras_Label', extrasComp.component);
                    self.Pax_Label = self.pluckcom('Pax_Label', extrasComp.component);
                    self.Baggage_Label = self.pluckcom('Baggage_Label', extrasComp.component);
                    self.Segments_Label = self.pluckcom('Segments_Label', extrasComp.component);
                    self.Seat_Label = self.pluckcom('Seat_Label', extrasComp.component);
                    self.Meal_Label = self.pluckcom('Meal_Label', extrasComp.component);

                    //Help & Support Section
                    self.Help_Label = self.pluckcom('Help_Label', helpComp.component);
                    self.Support_Label = self.pluckcom('Support_Label', helpComp.component);
                    self.Telephone_Label = self.pluckcom('Telephone_Label', helpComp.component);
                    self.Email_Label = self.pluckcom('Email_Label', helpComp.component);

                    //Alerts
                    self.Alert_Error_Label = self.pluckcom('Alert_Error_Label', alertComp.component);
                    self.Alert_Success_Label = self.pluckcom('Alert_Success_Label', alertComp.component);
                    self.Alert_Processing_Label = self.pluckcom('Alert_Processing_Label', alertComp.component);
                    self.Alert_Pending_Label = self.pluckcom('Alert_Pending_Label', alertComp.component);
                    self.Alert_Cancelled_Label = self.pluckcom('Alert_Cancelled_Label', alertComp.component);
                    self.Alert_TicketCancel_Label = self.pluckcom('Alert_TicketCancel_Label', alertComp.component);
                    self.Tech_Diff_alert_Msg = self.pluckcom('Tech_Diff_alert_Msg', alertComp.component);
                    self.Ticket_Issued_Msg = self.pluckcom('Ticket_Issued_Msg', alertComp.component);
                    self.Ticket_under_Process_Msg = self.pluckcom('Ticket_under_Process_Msg', alertComp.component);
                    self.Ticket_Pending_Msg = self.pluckcom('Ticket_Pending_Msg', alertComp.component);
                    self.Ticket_not_issue_Msg = self.pluckcom('Ticket_not_issue_Msg', alertComp.component);
                    self.Booking_cancel_Msg = self.pluckcom('Booking_cancel_Msg', alertComp.component);
                    self.E_Ticket_Cancelled_Msg = self.pluckcom('E_Ticket_Cancelled_Msg', alertComp.component);
                    self.Alert_Confirm_Label = self.pluckcom('Alert_Confirm_Label', alertComp.component);
                    self.Alert_Warning_Label = self.pluckcom('Alert_Warning_Label', alertComp.component);
                    self.Void_Ticket_Alert_Msg = self.pluckcom('Void_Ticket_Alert_Msg', alertComp.component);
                    self.Void_error_Ticket_Msg = self.pluckcom('Void_error_Ticket_Msg', alertComp.component);
                    self.Cancellation_of_PNR_Msg = self.pluckcom('Cancellation_of_PNR_Msg', alertComp.component);
                    self.Avoid_Cancellation_Msg = self.pluckcom('Avoid_Cancellation_Msg', alertComp.component);
                    self.Ok_Label = self.pluckcom('Ok_Label', alertComp.component);
                    self.Submit_Button_Label = self.pluckcom('Submit_Button_Label', alertComp.component);
                    self.Cancel_Button_Label = self.pluckcom('Cancel_Button_Label', alertComp.component);
                    self.Alert_Invalid_Label = self.pluckcom('Alert_Invalid_Label', alertComp.component);
                    self.Invalid_Email_Msg = self.pluckcom('Invalid_Email_Msg', alertComp.component);
                    self.Issue_Offline_Ticket_Msg = self.pluckcom('Issue_Offline_Ticket_Msg', alertComp.component);
                    self.Reprice_Successful_Alerts = self.pluckcom('Reprice_Successful_Alerts', alertComp.component);
                    self.Reprice_Error_Alerts = self.pluckcom('Reprice_Error_Alerts', alertComp.component);
                    self.Repricing_Error_Alerts = self.pluckcom('Repricing_Error_Alerts', alertComp.component);

                    //PassengerContact
                    self.Passenger_Contact_Label = self.pluckcom('Passenger_Contact_Label', PassengerContact.component);
                    self.Phone_Label = self.pluckcom('Phone_Label', PassengerContact.component);
                    self.PassEmail_Label = self.pluckcom('Email_Label', PassengerContact.component);
                }
            }).catch(function (error) {
                console.log(error);
                self.content = [];
            });
        },
        roundAmount: function (amount, currency) {
            var decimalplace = 2;
            try {
                switch (currency.toUpperCase()) {
                    case 'BHD':
                    case 'IQD':
                    case 'JOD':
                    case 'KWD':
                    case 'OMR':
                    case 'TND':
                    case 'LYD':
                        decimalplace = 3;
                        break;
                }
            } catch (err) {
                console.log(err);
            }
            return parseFloat(Math.round(amount * 100) / 100).toFixed(decimalplace);
        },
        getminiRules: function () {
            var vm = this;
            var airMiniRules = this.miniRuleData;
            var miniRuleDiv = '';
            var miniRuleHeadDiv = '';
            $.each(airMiniRules, function (segIndex, miniRule) {
                if (!_.isEmpty(miniRule)) {
                    if (vm.commonStore.commonRoles.hasMiniRuleRole) {
                        if (isNullorEmpty(miniRuleHeadDiv)) {
                            miniRuleHeadDiv +=
                                '<div class="mini_rule">' +
                                '<ul>';
                        }
                        var titleHead = airportLocationFromAirportCode(miniRule.departure) + '(' + miniRule.departure.toUpperCase() + ') to ' + airportLocationFromAirportCode(miniRule.arrival) + '(' + miniRule.arrival.toUpperCase() + ')';
                        miniRuleHeadDiv +=
                            '<li class="mini_tablinks" onclick="openTab(event, \'tab_rule' + segIndex + '\', \'' + titleHead + '\')">' + miniRule.departure.toUpperCase() + ' to ' + miniRule.arrival.toUpperCase() + '</li>';
                    }
                }

            });
            if (!isNullorEmpty(miniRuleHeadDiv)) {
                miniRuleHeadDiv += '</ul>' + '</div>' + '<h3 class="mini_tab_head">First Tab</h3>';
            }

            var miniRuleCatDiv = '';
            var miniRuleCatTabDescDiv = '';
            $.each(airMiniRules, function (segIndex, miniRule) {
                if (!_.isEmpty(miniRule)) {
                    if (vm.commonStore.commonRoles.hasMiniRuleRole) {
                        miniRuleCatDiv += '<div class="tab_rule" id="tab_rule' + segIndex + '" style="display:none;">';
                        var miniRuleCatTabDiv = '';
                        try {
                            var totCat = miniRule.codes[0].category.length;
                            $.each(miniRule.codes[0].category, function (categoryIndex, catgry) {
                                if (
                                    (!isNullorEmpty(catgry.dateInfo) && !isNullorUndefind(catgry.dateInfo)) ||
                                    (!isNullorEmpty(catgry.monInfo) && !isNullorUndefind(catgry.monInfo)) ||
                                    (!isNullorEmpty(catgry.restriAppInfo) && !isNullorUndefind(catgry.restriAppInfo))
                                ) {
                                    if (categoryIndex == 0) {
                                        miniRuleCatDiv += '<div class="tabmini">';
                                    }
                                    miniRuleCatDiv += '<button class="tablinks" onclick="openSubTab(event, \'catgry_' + segIndex + '_' + catgry.cat + '\')"' + (categoryIndex == 0 ? ' id="defaultOpen"' : '') + '>' + catgry.desc + '</button>';

                                    miniRuleCatTabDiv += '<div id="catgry_' + segIndex + '_' + catgry.cat + '" class="tabcontent">';
                                    miniRuleCatTabDiv += '<div class="content_tab_sec">';

                                    // Restrictions Applicability Info
                                    if (!isNullorEmpty(catgry.restriAppInfo) && !isNullorUndefind(catgry.restriAppInfo)) {
                                        miniRuleCatTabDiv += '<h3>Restrictions Applicability Info</h3>';
                                        $.each(catgry.restriAppInfo, function (restriappinfoIndex, restriappinfo) {
                                            miniRuleCatTabDiv += '<p>';
                                            miniRuleCatTabDiv += isNullorEmptyToBlank(restriappinfo.desc);
                                            miniRuleCatTabDiv += ((isNullorEmpty(restriappinfo.situation) ? '' : ' ' + restriappinfo.situation.toLowerCase()));
                                            miniRuleCatTabDiv += ((isNullorEmpty(restriappinfo.value) ? '' : ' - ' + restriappinfo.value));
                                            miniRuleCatTabDiv += '</p>';
                                        });
                                    }

                                    // Monetary Info
                                    if (!isNullorEmpty(catgry.monInfo) && !isNullorUndefind(catgry.monInfo)) {
                                        miniRuleCatTabDiv += '<h3>Monetary Info</h3>';
                                        $.each(catgry.monInfo, function (moninfoIndex, moninfo) {
                                            miniRuleCatTabDiv += '<p>';
                                            miniRuleCatTabDiv += isNullorEmptyToBlank(moninfo.desc);
                                            miniRuleCatTabDiv += ((isNullorEmpty(moninfo.situation) ? '' : ' ' + moninfo.situation.toLowerCase()));
                                            miniRuleCatTabDiv += ((isNullorEmpty(moninfo.currency) && isNullorEmpty(moninfo.amount) ? '' : ' - '));
                                            miniRuleCatTabDiv += isNullorEmptyToBlank(moninfo.currency) + ' ' + isNullorEmptyToBlank(moninfo.amount);
                                            miniRuleCatTabDiv += '</p>';
                                        });
                                    }

                                    // Date Info
                                    if (!isNullorEmpty(catgry.dateInfo) && !isNullorUndefind(catgry.dateInfo)) {
                                        miniRuleCatTabDiv += '<h3>Date Info</h3>';
                                        $.each(catgry.dateInfo, function (dateinfoIndex, dateinfo) {
                                            miniRuleCatTabDiv += '<p>';
                                            miniRuleCatTabDiv += isNullorEmptyToBlank(dateinfo.desc);
                                            miniRuleCatTabDiv += ((isNullorEmpty(dateinfo.situation) ? '' : ' ' + dateinfo.situation.toLowerCase()));
                                            miniRuleCatTabDiv += ((isNullorEmpty(dateinfo.date) && isNullorEmpty(dateinfo.time) ? '' : ' - '));
                                            try {
                                                miniRuleCatTabDiv += dateinfo.date.substring(0, 2) + ' ' + dateinfo.date.substring(2, 5) + ', 20' + dateinfo.date.substring(5, 7);
                                            } catch (ex) {}
                                            try {
                                                miniRuleCatTabDiv += ' ' + dateinfo.time.substring(0, 2) + ':' + dateinfo.time.substring(2, 4);
                                            } catch (ex) {}
                                            miniRuleCatTabDiv += '</p>';
                                        });
                                    }

                                    miniRuleCatTabDiv += '</div>';
                                    miniRuleCatTabDiv += '</div>';

                                    if (categoryIndex + 1 == totCat) {
                                        miniRuleCatDiv += '</div>';
                                    }
                                }
                            });
                            miniRuleCatDiv += (miniRuleCatTabDiv + '</div>');
                        } catch (ex) {
                            var sampid = 'catgry_' + segIndex + '_nodata';
                            miniRuleCatDiv += '<div class="tabmini">';
                            miniRuleCatDiv += '<button class="tablinks" onclick="openSubTab(event, \'' + sampid + '\')" id="defaultOpen">Category</button>';
                            miniRuleCatDiv += '</div>';
                            miniRuleCatDiv += '<div id="' + sampid + '" class="tabcontent"><div class="content_tab_sec"><p><i class="fa fa-exclamation-circle"></i> No information available</p></div></div>';
                            miniRuleCatDiv += (miniRuleCatTabDiv + '</div>');
                            console.log(ex);
                        }
                    }
                }
            });

            miniRuleDiv = miniRuleHeadDiv + miniRuleCatDiv;

            if (miniRuleDiv != '') {
                this.miniRuleDiv = miniRuleDiv;
                try {
                    setTimeout(() => {
                        document.getElementsByClassName("mini_rule")[0].getElementsByTagName('ul')[0].getElementsByTagName('li')[0].click();
                    }, 100);
                } catch (ex) {}
            } else {
                this.miniRuleDiv = '<p>Mini rules not available for this trip !</p>';
            }
        },
        isNullorEmptyToBlank: function (value, optval) {
            return isNullorEmpty(value) ? (isNullorEmpty(optval) ? '' : optval) : value;
        },
        isValidEmail: function (emailID) {
            var status = false;
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = emailID.match(emailPat);
            if (matchArray != null) {
                status = true;
            }
            return status;
        },
        airportFromAirportCode: function (airPortCode) {
            return airportFromAirportCode(airPortCode);
        },
        airportFromAirportCodeSimple: function (airPortCode) {
            return airportFromAirportCodeSimple(airPortCode);
        },
        bindPaxExtars: function (travelinfo) {

            var self = this;
            if (travelinfo.length > 0) {
                var ancillerytable = '';
                var found = travelinfo.some(function (el, i) {
                    if (el.extras.length > 0) {
                        return true;
                    }
                });
                if (found) {
                    self.Anclrtable = true;
                    for (var ti = 0; ti < travelinfo.length; ti++) {
                        var Extras = travelinfo[ti].extras;
                        if (Extras.length > 0) {
                            var Name = travelinfo[ti].namePrefix + ' ' + travelinfo[ti].givenName + ' ' + travelinfo[ti].surname;
                            var Segmentsname = '<div class="pax_segmants"> <ul>';
                            var seatlist = '<div class="pax_segmants"> <ul>';
                            var baggagelis = '<div class="pax_segmants"> <ul>';
                            var Meallist = '<div class="pax_segmants"> <ul>';
                            for (var ei = 0; ei < Extras.length; ei++) {
                                var Listyle = Extras.length != ei + 1 ? '<div style="border-bottom: 1px dashed #adacac;">' : '<div>';
                                var From = Extras[ei].from;
                                var To = Extras[ei].to;
                                var Baggage = [];
                                var Meals = [];
                                var Seat = [];
                                if (Extras[ei].hasOwnProperty('baggage')) {
                                    Baggage = Extras[ei].baggage;
                                    if (Baggage.length > 0) {
                                        for (var bgl = 0; bgl < Baggage.length; bgl++) {
                                            if (Baggage[bgl].quantity != "0.0") {
                                                baggagelis = baggagelis + '<li>' + Baggage[bgl].quantity + ' ' + Baggage[bgl].unit + '</li>';
                                            } else {
                                                baggagelis = baggagelis + ' <li> None </li >';
                                            }
                                        }
                                    }
                                } else {
                                    baggagelis = baggagelis + ' <li> None </li >';
                                }
                                if (Extras[ei].hasOwnProperty('meal')) {
                                    Meals = Extras[ei].meal;
                                    Meallist = Meallist + '<li>';
                                    if (Meals.length > 0) {
                                        for (var mel = 0; mel < Meals.length; mel++) {
                                            Meallist = Meallist + '<span> ' + Meals[mel].meal + ' x ' + parseInt(Meals[mel].count) + '</span> ';
                                        }
                                    }
                                    Meallist = Meallist + '</li>';
                                } else {
                                    Meallist = Meallist + '<li> None </li>';
                                }
                                if (Extras[ei].hasOwnProperty('seat')) {
                                    Seat = Extras[ei].seat;
                                    if (Seat.length > 0) {
                                        for (var set = 0; set < Seat.length; set++) {
                                            seatlist = seatlist + '<li>' + Seat[set].seatno + '</li>';
                                        }
                                    }
                                } else {
                                    seatlist = seatlist + '<li> None</li>';
                                }
                                Segmentsname = Segmentsname + '<li>' + From + '-' + To + '</li>';
                            }
                            Segmentsname = Segmentsname + '</ul></div>';
                            seatlist = seatlist + '</ul></div>';
                            baggagelis = baggagelis + '</ul></div>';
                            Meallist = Meallist + '</ul></div>';

                            ancillerytable = ancillerytable + '<tr><td>' + Name + '</td>' +
                                '<td>' + Segmentsname + '</td><td>' + seatlist + '</td>' +
                                '<td>' + Meallist + '</td><td>' + baggagelis + '</td>' +
                                '</tr> ';
                        }
                    }
                    self.ancTableData = ancillerytable;
                } else {}
            }
        },
        getRepriceSameRBD: function () {

            var vm = this;
            var self = this;
            self.showLoader = true;
            var selectCred = null;
            try {
                selectCred = JSON.parse(window.sessionStorage.getItem("selectCredential"))
            } catch (err) {}
            var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = vm.commonStore.hubUrls.hubConnection.hubServices.flights.repriceRBD;
            var tripDetres = vm.tripDeRrespose;
            var booking = tripDetres.data.response.content.tripDetailRS.tripDetailsUiData.response;

            var PostData = {
                request: {
                    service: "FlightRQ",
                    content: {
                        command: "FlightRepriceRQ",
                        repriceRbdRQ: {
                            bookingRefId: booking.bookingRefId,
                            uniqueId: booking.pnrNum,
                            sessionId: booking.sessionId,
                            tripType: booking.tripType,
                            passengerTypeQuantity: {
                                adt: booking.adt,
                                chd: booking.chd,
                                inf: booking.inf
                            }
                        }
                    },
                    token: tripDetres.data.response.token,
                    hubUuid: tripDetres.data.response.hubUuid,
                    selectCredential: selectCred,
                    supplierCodes: [tripDetres.data.response.supplierCode]
                }
            };
            console.log("FlightRBDPriceRQ: ", JSON.stringify(PostData));

            var config = {
                axiosConfig: {
                    method: "POST",
                    url: hubUrl + serviceUrl,
                    data: PostData
                },
                successCallback: function (response) {
                    console.log("FlightRBDPriceRS RESPONSE RECEIVED: ", response);
                    if (response.data.response.content.error != undefined && response.data.response.content.error != null) {
                        self.showLoader = false;
                        alertify.alert(self.Alert_Warning_Label, self.Reprice_Error_Alerts).set('label', self.Ok_Label);
                    } else {
                        self.showLoader = false;
                        self.repriceRBDResponseStatus = true;
                        self.repriceRBDResponse = response.data.response;
                        $("#RepriceRBDfare").modal('show');
                        $("#reprice_rbd").modal('hide');
                        //self.RepriceRBDfares=true;
                    }
                },
                errorCallback: function (error) {
                    self.showLoader = false;
                },
                showAlert: true
            }

            mainAxiosRequest(config);
        },
        getRepricingSameRBD: function () {

            var vm = this;
            var self = this;
            self.showLoader = true;
            var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = vm.commonStore.hubUrls.hubConnection.hubServices.flights.repricingRBD;
            var selectCred = null;
            try {
                selectCred = JSON.parse(window.sessionStorage.getItem("selectCredential"))
            } catch (err) {}
            var tripDetres = vm.tripDeRrespose;
            var booking = tripDetres.data.response.content.tripDetailRS.tripDetailsUiData.response;
            var PostData = {
                request: {
                    service: "FlightRQ",
                    content: {
                        command: "FlightRepricingRQ",
                        supplierSpecific: self.repriceRBDResponse.content.supplierSpecific ? self.repriceRBDResponse.content.supplierSpecific : self.repriceRBDResponse.content.fareInformationWithoutPnrReply.supplierSpecific ? self.repriceRBDResponse.content.fareInformationWithoutPnrReply.supplierSpecific : {},
                        repricingRbdRQ: {
                            bookingRefId: booking.bookingRefId,
                            uniqueId: booking.pnrNum,
                            fareReprice: self.repriceRBDResponse.content.fareInformationWithoutPnrReply.fareReprice,
                            sealed: self.repriceRBDResponse.content.fareInformationWithoutPnrReply.sealed,
                            paymentModeId: booking.paymentModeId,
                            dealCode: self.repriceRBDResponse.content.fareInformationWithoutPnrReply.dealCode
                        }
                    },
                    token: self.repriceRBDResponse.token,
                    selectCredential: selectCred,
                    supplierCodes: [self.repriceRBDResponse.supplierCode]
                }

            };
            try {
                if (self.repriceRBDResponse.content.fareInformationWithoutPnrReply.dealCode) {
                    PostData.request.content.repricingRbdRQ.dealCode = self.repriceRBDResponse.content.fareInformationWithoutPnrReply.dealCode;
                }
            } catch (error) {
                console.log(error)
            }

            console.log("FlightRepricingRQ: ", JSON.stringify(PostData));

            var config = {
                axiosConfig: {
                    method: "POST",
                    url: hubUrl + serviceUrl,
                    data: PostData
                },
                successCallback: function (response) {
                    console.log("FlightRepricingRS RESPONSE RECEIVED: ", response);
                    //self.showLoader = false;
                    if (response.data.response.content.error != undefined && response.data.response.content.error != null) {
                        alertify.alert(self.Alert_Warning_Label, self.Repricing_Error_Alerts).set('label', self.Ok_Label);
                    } else {
                        self.getTripDetails('reprice');
                    }
                },
                errorCallback: function (error) {
                    self.showLoader = false;
                },
                showAlert: true
            }

            mainAxiosRequest(config);
        },
        getRepriceLowestRBD: function () {

            var vm = this;
            var self = this;
            self.showLoader = true;
            var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = vm.commonStore.hubUrls.hubConnection.hubServices.flights.repriceLowerfare;
            var selectCred = null;
            try {
                selectCred = JSON.parse(window.sessionStorage.getItem("selectCredential"))
            } catch (err) {}
            var tripDetres = vm.tripDeRrespose;
            var booking = tripDetres.data.response.content.tripDetailRS.tripDetailsUiData.response;
            var PostData = {
                request: {
                    service: "FlightRQ",
                    content: {
                        command: "FlightRepriceLowestFareRQ",
                        repriceToLowestRbdRQ: {
                            bookingRefId: booking.bookingRefId,
                            uniqueId: booking.pnrNum,
                            sessionId: booking.sessionId,
                            tripType: booking.tripType,
                            passengerTypeQuantity: {
                                adt: booking.adt,
                                chd: booking.chd,
                                inf: booking.inf
                            }
                        }
                    },
                    token: tripDetres.data.response.token,
                    hubUuid: tripDetres.data.response.hubUuid,
                    node: {
                        currency: tripDetres.data.response.node.currency,
                        agencyCode: tripDetres.data.response.node.agencyCode
                    },
                    selectCredential: selectCred,
                    supplierCodes: [tripDetres.data.response.supplierCode],
                }
            };
            console.log("FlightRepriceLowestFareRQ: ", JSON.stringify(PostData));

            var config = {
                axiosConfig: {
                    method: "POST",
                    url: hubUrl + serviceUrl,
                    data: PostData
                },
                successCallback: function (response) {
                    console.log("FlightRepriceLowestFareRS RESPONSE RECEIVED: ", response);

                    if (response.data.response.content.error != undefined && response.data.response.content.error != null) {
                        self.showLoader = false;
                        alertify.alert(self.Alert_Warning_Label, self.Reprice_Error_Alerts).set('label', self.Ok_Label);
                    } else {
                        self.showLoader = false;
                        self.repriceLowestRBDResponseStatus = true;
                        self.repriceLowestRBDResponse = response.data.response;
                        $("#RepricingLowestRBDfare").modal('show');
                        $("#reprice_rbd").modal('hide');
                    }
                },
                errorCallback: function (error) {
                    self.showLoader = false;
                },
                showAlert: true
            }

            mainAxiosRequest(config);
        },
        getRepricingLowestRBD: function () {

            var vm = this;
            var self = this;
            self.showLoader = true;
            var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = vm.commonStore.hubUrls.hubConnection.hubServices.flights.repricingLowerfare;
            var selectCred = null;
            try {
                selectCred = JSON.parse(window.sessionStorage.getItem("selectCredential"))
            } catch (err) {}
            var tripDetres = vm.tripDeRrespose;
            var booking = tripDetres.data.response.content.tripDetailRS.tripDetailsUiData.response;
            var PostData = {
                request: {
                    service: "FlightRQ",
                    content: {
                        command: "FlightRepricingLowestFareRQ",
                        repricingToLowestFareRQ: {
                            bookingRefId: booking.bookingRefId,
                            uniqueId: booking.pnrNum,
                            fareReprice: self.repriceLowestRBDResponse.content.fareInformationWithoutPnrReply.fareReprice,
                            sealed: self.repriceLowestRBDResponse.content.fareInformationWithoutPnrReply.sealed,
                            paymentModeId: booking.paymentModeId,
                            flLegGroup: self.repriceLowestRBDResponse.content.fareInformationWithoutPnrReply.airSegments
                        }
                    },
                    token: self.repriceLowestRBDResponse.token,
                    selectCredential: selectCred,
                    supplierCodes: [self.repriceLowestRBDResponse.supplierCode]
                }
            };
            console.log("FlightRepricingLowestFareRQ: ", JSON.stringify(PostData));

            var config = {
                axiosConfig: {
                    method: "POST",
                    url: hubUrl + serviceUrl,
                    data: PostData
                },
                successCallback: function (response) {
                    console.log("FlightRepricingLowestFareRS RESPONSE RECEIVED: ", response);
                    // self.showLoader = false;

                    if (response.data.response.content.error != undefined && response.data.response.content.error != null) {
                        alertify.alert(self.Alert_Warning_Label, self.Repricing_Error_Alerts).set('label', self.Ok_Label);
                    } else {

                        self.getTripDetails('reprice');
                    }
                },
                errorCallback: function (error) {
                    self.showLoader = false;
                },
                showAlert: true
            }

            mainAxiosRequest(config);
        },
        toggleRemarks: function (event) {
            var clickedElement = event.currentTarget;
            $("#supplierRemarks").toggle();
            $(clickedElement).find('i').toggleClass('fa-chevron-down fa-chevron-up');
        },
        updatePassport: function () {
            var self = this;
            var Travelinfo = self.bookinginfo.data.response.travelerInfo;
            var pnr = self.PNR;
            var supplierCode = self.tripDeRrespose.data.response.supplierCode;
            window.sessionStorage.setItem("Traveller_Info", JSON.stringify(Travelinfo));
            window.sessionStorage.setItem("Flight_Pnr", pnr);
            window.sessionStorage.setItem("Flight_sessionId", self.bookinginfo.data.response.sessionId);
            window.sessionStorage.setItem("supplier_Code", supplierCode);
            if (supplierCode == 62) {
                window.sessionStorage.setItem("supplierSpecific", JSON.stringify(self.tripDeRrespose.data.response.content.supplierSpecific));
            }
            if (Travelinfo && pnr) {
                window.location.href = "passportdetails.html";
            }
        },
        bindSummary: function (item) {
            var self = this;
            var rules;
            var before = item.find(x => x.situation.toLowerCase() == 'before departure');
            if (before && before.allowed) {
                if (before.amount > 0) {
                    // rules = '<p>' + before.cat + '</p><p class="allowed">Allowed @ ' + self.$n(parseFloat(before.amount / self.commonStore.currencyMultiplier), 'currency', self.commonStore.selectedCurrency) + '</p>'
                    rules = '<p>' + before.cat + '</p><p class="allowed">Allowed @ ' + before.currency + '&nbsp;' + before.amount + '</p>'

                } else {
                    rules = '<p>' + before.cat + '</p><p class="allowed">Allowed</p>';
                }

            } else {
                rules = '<p>' + before.cat + '</p><p class="not-allowed">Not Allowed</p>';
            }

            var after = item.find(x => x.situation.toLowerCase() == 'after departure');
            if (after && after.allowed) {
                if (after.amount > 0) {
                    // rules = rules + '<p class="allowed">Allowed @ ' + self.$n(parseFloat(after.amount / self.commonStore.currencyMultiplier), 'currency', self.commonStore.selectedCurrency) + '</p>'
                    rules = rules + '<p class="allowed">Allowed @ ' + after.currency + '&nbsp;' + after.amount + '</p>'

                } else {
                    rules = rules + '<p class="allowed">Allowed</p>';
                }

            } else {
                rules = rules + '<p class="not-allowed">Not Allowed</p>';
            }
            return rules;

        },
        getFareRules: function () {
            var fareRuleDiv = '';

            var airFareRules = this.fareRuleData;
            $('#popFareRule').show();
            $.each(airFareRules, function (segIndex, fareRule) {
                if (hasFareRule(fareRule)) {
                    fareRuleDiv += '<div class="itinerary_head">' +
                        '<h2 style="margin: 0">' +
                        '<i class="fa fa-plane summary_flight"></i>' +
                        airportLocationFromAirportCode(fareRule.from) + '(' + fareRule.from.toUpperCase() + ') to ' +
                        airportLocationFromAirportCode(fareRule.to) + '(' + fareRule.to + ')' +
                        '</h2>' +
                        '</div> ';
                    if (fareRule.fareRules && fareRule.fareRules.length > 0) {
                        $.each(fareRule.fareRules, function (ruleIndex, rule) {
                            if (!stringIsNullorEmpty(rule.fareRef)) {
                                fareRuleDiv += '<a href="#rule' + segIndex + ruleIndex + '" data-toggle="collapse">' + rule.fareRef + '<i class="chevron fa fa-fw" ></i></a><br>' +
                                    '<p id="rule' + segIndex + ruleIndex + '" class="collapse">' + rule.fareRule + '</p>';
                            } else {
                                fareRuleDiv += '<p style="padding-top: 16px;float: left;">No fare rule available for this segment</p>';
                            }
                        });
                    }
                }
            });

            if (fareRuleDiv != '') {
                this.fareRuleDiv = fareRuleDiv;
            } else {
                this.fareRuleDiv = '<p>Fare rules not available for this trip !</p>';
            }
        },
        // for set segment status text
        setSeegmentStatus: function (status) {
            var rStatusText = 'Failed'
            var confirmed = ['HK', 'TK', 'KL', 'KK', 'RR', 'SS', 'HS']
            var waitlisted = ['HL', 'HO', 'LL', 'NN', 'TL', 'TN', 'US', 'UU']
            var cancelled = ['HX']
            var failed = ['UN', 'UC', 'NO']
            try {
                status = status.toUpperCase();
                if (confirmed.includes(status)) {
                    rStatusText = "Confirmed"
                    return rStatusText
                }
                if (waitlisted.includes(status)) {
                    rStatusText = "Waitlisted"
                    return rStatusText
                }
                if (cancelled.includes(status)) {
                    rStatusText = "Cancelled"
                    return rStatusText
                }
                if (failed.includes(status)) {
                    rStatusText = "Failed"
                    return rStatusText
                } else {
                    return rStatusText
                }
            } catch (error) {
                return rStatusText
            }
        }
    },
    mounted: function () {
        $('#voucherHtmlEdit').summernote()
        $('[data-toggle="tooltip"]').tooltip();
    },
    watch: {
        "commonStore.currencyMultiplier": function () {
            var vm = this;
            try {
                if (vm.commonStore.selectedCurrency == '') {
                    if (vm.tripDeRrespose.data.response.selectCredential) {
                        vm.commonStore.selectedCurrency = vm.tripDeRrespose.data.response.selectCredential.officeIdList[0].currency;
                    } else {
                        vm.commonStore.selectedCurrency = JSON.parse(window.sessionStorage.getItem("selectCredential")).officeIdList[0].currency;
                    }
                    if (!vm.commonStore.selectedCurrency) {
                        vm.commonStore.selectedCurrency = vm.tripDeRrespose.data.response.content.tripDetailRS.tripDetailsUiData.response.totalFareGroup.sellCurrency;
                        vm.commonStore.currencyMultiplier = 1;
                    } else {
                        vm.commonStore.currencyMultiplier = _.filter(vm.commonStore.currencyRates, function (e) {
                            return e.fromCurrencyCode == vm.commonStore.selectedCurrency;
                        })[0].sellRate;
                    }
                }
            } catch (error) {
                if (vm.commonStore.selectedCurrency) {
                    vm.commonStore.selectedCurrency = vm.commonStore.agencyNode.loginNode.currency;
                }
                vm.commonStore.currencyMultiplier = 1;
            }
        }
    },
    computed: {}
});

function openSubTab(evt, subTab) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(subTab).style.display = "block";
    evt.currentTarget.className += " active";
}

function openTab(evt, mainTab, title) {
    var i, tab, currtab, tablinks;
    tab = document.getElementsByClassName("tab_rule");
    currtab = document.getElementById(mainTab);
    for (i = 0; i < tab.length; i++) {
        tab[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("mini_tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    currtab.style.display = "block";
    evt.currentTarget.className += " active";
    document.getElementsByClassName('mini_tab_head')[0].innerText = title;
    try {
        currtab.getElementsByClassName('tabmini')[0].getElementsByTagName('button')[0].click();
    } catch (ex) {}
}
$('#myMiniModal').on('load', function () {
    try {
        document.getElementsByClassName("mini_rule")[0].getElementsByTagName('ul')[0].getElementsByTagName('li')[0].click();
    } catch (ex) {}
});

function hasFareRule(airFareRules) {
    var hasFareRule = false;
    if (airFareRules != undefined && airFareRules != null && airFareRules != "" && airFareRules != "NA" && airFareRules != "NIL") {
        if (airFareRules.from != "" && airFareRules.from != "NA" && airFareRules.from != null && airFareRules.from != undefined && airFareRules.from != "NIL") {
            hasFareRule = true;
        }
    }
    return hasFareRule;
}


$('body').on('keypress', '.only_alpha', function (e) {
    if (e.ctrlKey || e.altKey) {
        e.preventDefault();
    } else {
        var key = e.keyCode;
        if (!((key == 32) || (key == 96) || (key >= 65 && key <= 90) || (key >= 97 && key <= 122))) {
            e.preventDefault();
        }
    }
});

$('body').on('keypress', '.only_number', function (e) {
    if (e.ctrlKey || e.altKey) {
        e.preventDefault();
    } else {
        var key = e.keyCode;
        if (!((key >= 48 && key <= 57))) {
            e.preventDefault();
        }
    }
});