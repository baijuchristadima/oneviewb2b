var index = 0;
var totalFare = 0;
var selectResponseObj = [];
var flightComponent = '';
Vue.component("flights-component", {
    //for websocket
    mixins: [websocketMixin],
    commonStore: vueCommonStore.state,
    data: function () {
        return {
            //get roles from common store
            commonStore: vueCommonStore.state,
            commonAgencyCode: 'AGY75',
            Account_NotAvailable: '',
            Account_ProcessLabel: '',
            Adult: '',
            Adults: '',
            After_Departure: '',
            Agency_Name: '',
            Agent_Details: '',
            Agency_Info: {
                loginNode: {
                    name: ''
                }
            },
            Agency_Username: '',
            Agree_Label1: '',
            Agree_Label2: '',
            Agree_Label3: '',
            AirlinesDatas: AirlinesDatas,
            AirlinesTimezone: [],
            ArlineSelected: {
                selected: ''
            },
            AirLineBaggages: [],
            ArlineSelectedLbl: '',
            Alert: '',
            Allowed_for: '',
            Available_Credit_limit: '',
            Baggage: '',
            Before_Departure: '',
            Booking: '',
            Bagg_Label: '',
            Booking_Failed: '',
            baggageShow: false,
            aribaggage_null: false,
            Cabin: '',
            RBD: '',
            Cabin_Baggage: '',
            Cancel: '',
            Checked_Baggage: '',
            Child: '',
            Children: '',
            Choose_Payment_Mode: '',
            Close: '',
            Confirm: '',
            Confirm_Booking_Button: '',
            Proceed_to_Issuance: '',
            Proceed_Payment_Button_Label: '',
            Booking_Button: '',
            bookType: '',
            Confirmation: '',
            Continue: '',
            Conditions_Label: '',
            Credit: '',
            Dob: '',
            Email: '',
            ExpireDate_Label: '',
            Expiry_Date: '',
            Fare: '',
            Fare_Rule: '',
            Fare_Rules: '',
            First_Name: '',
            IsFare_Rule_Available: false,
            Fare_Summary: '',
            FareChange_Alert: '',
            Fields_Mandatory: '',
            FirstName_Label: '',
            Flight_Booking: '',
            Flight_from: '',
            flightFareDetails: '',
            FlightTicket_RegisteredEmail: '',
            Free: '',
            Frequent_Flyer: '',
            Frequent_Flyer_Number: '',
            Frequentflyer_Program: '',
            FrequentNumber_Required: '',
            Infant: '',
            Infants: '',
            IssueCountry_Label: '',
            IssueDate_Label: '',
            LastName_Label: '',
            isLcc: false,
            Issue_Date: '',
            Issue_Country: '',
            IssueCntrySelected: {
                selected: ''
            },
            Layover_at: '',
            Last_Name: '',
            Max_TwentyChar: '',
            Message_UnexpectSupplierIssue: '',
            Mini_Rules: '',
            IsMini_Rule_Available: false,
            Multi_city: '',
            NationalIdPassport_Warning: '',
            Nationality_Label: '',
            Nationality: '',
            NationalitySelected: {
                selected: ''
            },
            Net_Fare: '',
            Base_Fare: '',
            No_Baggage: '',
            No_cancellation: '',
            No_Mini_Rules: '',
            Non_Refundable: '',
            No_baggage_available_label: '',
            No_Seg_baggAvail: '',
            Oneway: '',
            Original_Fare: '',
            OldTotalfare: '',
            PassportNumber_Label: '',
            Passport_Number: '',
            Password: '',
            Password_Required: '',
            PaxFareDet: '',
            Password_RequiredLabel: '',
            PassportOptional: false,
            Payment: '',
            Payment_Gateway: '',
            Phone_Number: '',
            PhoneNumber_Label: '',
            Proceed_to_issuance_button: '',
            Provide_Email: '',
            Provide_PhoneNumber: '',
            Provide_Traveller_Details: '',
            Privacy_Policy_Label: '',
            PlaceHolder_Date: '',
            PlaceHolder_Country: '',
            Refundable: '',
            Refundable_WithPenalty: '',
            Required_Field: '',
            Review_Flight: '',
            Review_your_flight: '',
            Round_Trip: '',
            Service_Fee: '',
            selectFlightFromSession: [],
            selectFlightFromSessionObj: '',
            selectedFlight: '',
            Select_Country: '',
            showAnimateDiv: true,
            Stop: '',
            Taxes_Fees: '',
            Taxes_Label: '',
            Terms_Label: '',
            Taxesfees_Included: '',
            Taxes_and_Fees: '',
            Thank_Registerwithus: '',
            Tickets: '',
            Title_Label: '',
            Title: '',
            to: '',
            Total: '',
            TotalPaxUnites: '',
            TripTypeText: '',
            Traveller: '',
            Traveller_Details: '',
            travellers: [],
            Trip_Total: '',
            TripSummary_Detail: '',
            Updated_Fare: '',
            User_Name: '',
            Username_Required: '',
            VAT: '',
            Welcome: '',
            miniRuleData: '',
            miniRuleDiv: '',
            fareRuleData: '',
            fareRuleDiv: '',
            summary: '',
            Detail: '',
            after: '',
            before: '',
            rule: '',
            //anxillery
            Baggage_Label: '',
            Select_Baggae_For_Your_Trip_Label: '',
            Skip_Baggage_Selection_Label: '',
            Confirm_Selection_Button_Label: '',
            To_Label: '',
            Edit_Button_Label: '',
            Close_Button_Label: '',
            No_Baggage_Label: '',
            Select_Label: '',
            Seat_Map_Label: '',
            Select_Seats_For_Your_Trip_Label: '',
            Fllight_Seat_Label: '',
            Seatmap_Legend_Label: '',
            Seat_Label: '',
            Available_Label: '',
            Occupied_Label: '',
            Selected_Label: '',
            Add_your_Meals_Label: '',
            Select_meals_for_your_Trip_Label: '',
            Meals_Label: '',
            Placeholder_Search: '',
            Placeholder_SelectAll: 'Select All',
            Placeholder_SortbyPrice: 'Sort By Price',
            Skip_Meal_Selection_Label: '',
            Add_this_Meal_Label: '',
            Skip_Seat_Selection_Label: '',
            Insurance_Label: '',
            Assistance_Label: '',
            //Alerts
            Alert_Label: '',
            Warning_Label: '',
            Ok: '',
            Please_Fill_Label: '',
            Terms_and_Conditions_Label: '',
            Please_agree: '',
            Message_UnexpectSupplierIssue: '',
            Duplicated_Names_alert_Msg: '',
            No_Booking_privillage_alert_Msg: '',
            Fields_Mandatory_alert_Msg: '',
            Payment_Gateway_Not_Available_alert: '',
            Booking_failed_alert_Msg: '',
            Booking_success_alert_Msg: '',
            Please_fill_first_Name_alert_msg: '',
            FirstName_Min_Two_Char_alert_msg: '',
            SecondName_Min_Two_Char_alert_msg: '',
            Please_fill_last_name_alert_msg: '',
            Please_select_date_of_birth_alert: '',
            Please_select_nationality_alert_msg: '',
            Please_fill_passport_number_alert: '',
            Please_select_expiry_date_alert: '',
            Invalid_passport_number_alert: '',
            Please_select_issue_date_alert: '',
            Please_fill_issue_country_alert: '',
            Fare_Increased_alert_Msg: '',
            Fare_Drop_alert_Msg: '',
            Continue_Btn_Label: '',
            Cancel_Btn_Label: '',
            Ins_Purch_Alert_Heading: '',
            Ins_Purch_Alert_Content: '',
            Choose_Payment_Mode_Alert: '',
            //ancillary services srf
            ShowExtrasDiv: false,
            ShowAncillerMeals: false,
            ShowAncillerBaggage: false,
            ShowAncillerSeats: false,
            ShowAncillerInsurence: false,
            ShowAncillerAssistnce: false,
            baggageDetailsResponses: [],
            mealDetailsResponses: [],
            seatmapInformation: [],
            BaggageInfo: [],
            Baggagepaxarr: [],
            SeatMapArry: [],
            seatIndex: 0,
            Choosepaxflag: false,
            choosepax: null,
            TotalPaxCount: 0,
            TotalselectedPax: 0,
            seatRequest: [],
            TotalseatPrice: 0,
            TotalmealPrice: 0,
            workingonseat: false,
            MealPaxArr: [],
            mealIndex: 0,
            mealsorttype: 'All',
            mealRequest: [],
            BagaggeExtra: [],
            SeatExtra: [],
            MealsExtra: [],
            showFarepricediv: true,
            showAncillaryFarePricediv: false,
            ancillaryRespones: {},
            //passenger detials
            Phone_Label: '',
            Phone_Placeholder: '',
            Email_Label: '',
            Email_Placeholder: '',
            Passenger_Details_Label: '',
            Discount_Label: '',
            Service_Fee_Label: '',
            Commission_Label: '',
            hasAncilleryResponse: false,
            SupplierId: null,
            AirLinehandBaggages: [],
            handbaggageShow: false,
            if_cabbinbaggage: false,
            prevBaggage: [],
            additionalContact: [],
            coutryCodeList: [],
            coutryCodeListMain: [],
            ancilleryLoadingBaggage: false,
            ancilleryLoadingSeat: false,
            ancilleryLoadingMeal: false,
            paidServices: 0,
            bookIsClicked: false,
            ancillaryServicesList: [{
                    title: "Seat",
                    show: false,
                    image: "/assets/images/extra_icon3.png"
                },
                {
                    title: "Baggage",
                    show: false,
                    image: "/assets/images/extra_icon2.png"
                },
                {
                    title: "Meals",
                    show: false,
                    image: "/assets/images/extra_icon1.png"
                },
                {
                    title: "Passenger Assistance",
                    show: false,
                    image: ""
                },
                {
                    title: "Inflight Entertainment",
                    show: false,
                    image: ""
                },
                {
                    title: "Lounge",
                    show: false,
                    image: ""
                },
                {
                    title: "Travel Services",
                    show: false,
                    image: ""
                },
                {
                    title: "Other Services",
                    show: false,
                    image: ""
                },
            ],
            selectedTab: "",
            paxAncillarySummary: [],
            showAncillaryFarePricedivPopUp: false,
            hideSeat: true,
            countryCodes: [
                ["Afghanistan", "af", "93"],
                ["Albania", "al", "355"],
                ["Algeria", "dz", "213"],
                ["American Samoa", "as", "1684"],
                ["Andorra", "ad", "376"],
                ["Angola", "ao", "244"],
                ["Anguilla", "ai", "1264"],
                ["Antigua and Barbuda", "ag", "1268"],
                ["Argentina", "ar", "54"],
                ["Armenia", "am", "374"],
                ["Aruba", "aw", "297"],
                ["Australia", "au", "61"],
                ["Austria", "at", "43"],
                ["Azerbaijan", "az", "994"],
                ["Bahamas", "bs", "1242"],
                ["Bahrain", "bh", "973"],
                ["Bangladesh", "bd", "880"],
                ["Barbados", "bb", "1246"],
                ["Belarus", "by", "375"],
                ["Belgium", "be", "32"],
                ["Belize", "bz", "501"],
                ["Benin", "bj", "229"],
                ["Bermuda", "bm", "1441"],
                ["Bhutan", "bt", "975"],
                ["Bolivia", "bo", "591"],
                ["Bosnia and Herzegovina", "ba", "387"],
                ["Botswana", "bw", "267"],
                ["Brazil", "br", "55"],
                ["British Indian Ocean Territory", "io", "246"],
                ["British Virgin Islands", "vg", "1284"],
                ["Brunei", "bn", "673"],
                ["Bulgaria", "bg", "359"],
                ["Burkina Faso", "bf", "226"],
                ["Burundi", "bi", "257"],
                ["Cambodia", "kh", "855"],
                ["Cameroon", "cm", "237"],
                ["Canada", "ca", "1"],
                ["Cape Verde", "cv", "238"],
                ["Caribbean Netherlands", "bq", "599"],
                ["Cayman Islands", "ky", "1345"],
                ["Central African Republic", "cf", "236"],
                ["Chad", "td", "235"],
                ["Chile", "cl", "56"],
                ["China", "cn", "86"],
                ["Colombia", "co", "57"],
                ["Comoros", "km", "269"],
                ["Congo", "cd", "243"],
                ["Congo (Republic)", "cg", "242"],
                ["Cook Islands", "ck", "682"],
                ["Costa Rica", "cr", "506"],
                ["Côte d’Ivoire", "ci", "225"],
                ["Croatia", "hr", "385"],
                ["Cuba", "cu", "53"],
                ["Curaçao", "cw", "599"],
                ["Cyprus", "cy", "357"],
                ["Czech Republic", "cz", "420"],
                ["Denmark", "dk", "45"],
                ["Djibouti", "dj", "253"],
                ["Dominica", "dm", "1767"],
                ["Dominican Republic", "do", "1"],
                ["Ecuador", "ec", "593"],
                ["Egypt", "eg", "20"],
                ["El Salvador", "sv", "503"],
                ["Equatorial Guinea", "gq", "240"],
                ["Eritrea", "er", "291"],
                ["Estonia", "ee", "372"],
                ["Ethiopia", "et", "251"],
                ["Falkland Islands", "fk", "500"],
                ["Faroe Islands", "fo", "298"],
                ["Fiji", "fj", "679"],
                ["Finland", "fi", "358"],
                ["France", "fr", "33"],
                ["French Guiana", "gf", "594"],
                ["French Polynesia", "pf", "689"],
                ["Gabon", "ga", "241"],
                ["Gambia", "gm", "220"],
                ["Georgia", "ge", "995"],
                ["Germany", "de", "49"],
                ["Ghana", "gh", "233"],
                ["Gibraltar", "gi", "350"],
                ["Greece", "gr", "30"],
                ["Greenland", "gl", "299"],
                ["Grenada", "gd", "1473"],
                ["Guadeloupe", "gp", "590"],
                ["Guam", "gu", "1671"],
                ["Guatemala", "gt", "502"],
                ["Guinea", "gn", "224"],
                ["Guinea-Bissau", "gw", "245"],
                ["Guyana", "gy", "592"],
                ["Haiti", "ht", "509"],
                ["Honduras", "hn", "504"],
                ["Hong Kong", "hk", "852"],
                ["Hungary", "hu", "36"],
                ["Iceland", "is", "354"],
                ["India", "in", "91"],
                ["Indonesia", "id", "62"],
                ["Iran", "ir", "98"],
                ["Iraq", "iq", "964"],
                ["Ireland", "ie", "353"],
                ["Israel", "il", "972"],
                ["Italy", "it", "39"],
                ["Jamaica", "jm", "1876"],
                ["Japan", "jp", "81"],
                ["Jordan", "jo", "962"],
                ["Kazakhstan", "kz", "7"],
                ["Kenya", "ke", "254"],
                ["Kiribati", "ki", "686"],
                ["Kuwait", "kw", "965"],
                ["Kyrgyzstan", "kg", "996"],
                ["Laos", "la", "856"],
                ["Latvia", "lv", "371"],
                ["Lebanon", "lb", "961"],
                ["Lesotho", "ls", "266"],
                ["Liberia", "lr", "231"],
                ["Libya", "ly", "218"],
                ["Liechtenstein", "li", "423"],
                ["Lithuania", "lt", "370"],
                ["Luxembourg", "lu", "352"],
                ["Macau", "mo", "853"],
                ["Macedonia", "mk", "389"],
                ["Madagascar", "mg", "261"],
                ["Malawi", "mw", "265"],
                ["Malaysia", "my", "60"],
                ["Maldives", "mv", "960"],
                ["Mali", "ml", "223"],
                ["Malta", "mt", "356"],
                ["Marshall Islands", "mh", "692"],
                ["Martinique", "mq", "596"],
                ["Mauritania", "mr", "222"],
                ["Mauritius", "mu", "230"],
                ["Mexico", "mx", "52"],
                ["Micronesia", "fm", "691"],
                ["Moldova", "md", "373"],
                ["Monaco", "mc", "377"],
                ["Mongolia", "mn", "976"],
                ["Montenegro", "me", "382"],
                ["Montserrat", "ms", "1664"],
                ["Morocco", "ma", "212"],
                ["Mozambique", "mz", "258"],
                ["Myanmar", "mm", "95"],
                ["Namibia", "na", "264"],
                ["Nauru", "nr", "674"],
                ["Nepal", "np", "977"],
                ["Netherlands", "nl", "31"],
                ["New Caledonia", "nc", "687"],
                ["New Zealand", "nz", "64"],
                ["Nicaragua", "ni", "505"],
                ["Niger", "ne", "227"],
                ["Nigeria", "ng", "234"],
                ["Niue", "nu", "683"],
                ["Norfolk Island", "nf", "672"],
                ["North Korea", "kp", "850"],
                ["Northern Mariana Islands", "mp", "1670"],
                ["Norway", "no", "47"],
                ["Oman", "om", "968"],
                ["Pakistan", "pk", "92"],
                ["Palau", "pw", "680"],
                ["Palestine", "ps", "970"],
                ["Panama", "pa", "507"],
                ["Papua New Guinea", "pg", "675"],
                ["Paraguay", "py", "595"],
                ["Peru", "pe", "51"],
                ["Philippines", "ph", "63"],
                ["Poland", "pl", "48"],
                ["Portugal", "pt", "351"],
                ["Puerto Rico", "pr", "1"],
                ["Qatar", "qa", "974"],
                ["Réunion", "re", "262"],
                ["Romania", "ro", "40"],
                ["Russia", "ru", "7"],
                ["Rwanda", "rw", "250"],
                ["Saint Barthélemy", "bl", "590"],
                ["Saint Helena", "sh", "290"],
                ["Saint Kitts and Nevis", "kn", "1869"],
                ["Saint Lucia", "lc", "1758"],
                ["Saint Martin", "mf", "590"],
                ["Saint Pierre and Miquelon", "pm", "508"],
                ["Saint Vincent and the Grenadines", "vc", "1784"],
                ["Samoa", "ws", "685"],
                ["San Marino", "sm", "378"],
                ["São Tomé and Príncipe", "st", "239"],
                ["Saudi Arabia", "sa", "966"],
                ["Senegal", "sn", "221"],
                ["Serbia", "rs", "381"],
                ["Seychelles", "sc", "248"],
                ["Sierra Leone", "sl", "232"],
                ["Singapore", "sg", "65"],
                ["Sint Maarten", "sx", "1721"],
                ["Slovakia", "sk", "421"],
                ["Slovenia", "si", "386"],
                ["Solomon Islands", "sb", "677"],
                ["Somalia", "so", "252"],
                ["South Africa", "za", "27"],
                ["South Korea", "kr", "82"],
                ["South Sudan", "ss", "211"],
                ["Spain", "es", "34"],
                ["Sri Lanka", "lk", "94"],
                ["Sudan", "sd", "249"],
                ["Suriname", "sr", "597"],
                ["Swaziland", "sz", "268"],
                ["Sweden", "se", "46"],
                ["Switzerland", "ch", "41"],
                ["Syria", "sy", "963"],
                ["Taiwan", "tw", "886"],
                ["Tajikistan", "tj", "992"],
                ["Tanzania", "tz", "255"],
                ["Thailand", "th", "66"],
                ["Timor-Leste", "tl", "670"],
                ["Togo", "tg", "228"],
                ["Tokelau", "tk", "690"],
                ["Tonga", "to", "676"],
                ["Trinidad and Tobago", "tt", "1868"],
                ["Tunisia", "tn", "216"],
                ["Turkey", "tr", "90"],
                ["Turkmenistan", "tm", "993"],
                ["Turks and Caicos Islands", "tc", "1649"],
                ["Tuvalu", "tv", "688"],
                ["U.S. Virgin Islands", "vi", "1340"],
                ["Uganda", "ug", "256"],
                ["Ukraine", "ua", "380"],
                ["United Arab Emirates", "ae", "971"],
                ["United Kingdom", "gb", "44"],
                ["United States", "us", "1"],
                ["Uruguay", "uy", "598"],
                ["Uzbekistan", "uz", "998"],
                ["Vanuatu", "vu", "678"],
                ["Vatican City", "va", "39"],
                ["Venezuela", "ve", "58"],
                ["Vietnam", "vn", "84"],
                ["Wallis and Futuna", "wf", "681"],
                ["Yemen", "ye", "967"],
                ["Zambia", "zm", "260"],
                ["Zimbabwe", "zw", "263"]
            ],
            oldBaggage: [],
            oldMeal: [],

            // minirules
            SummaryList: [],
            isSummaryAvailable: false,
            showCreditCardButton: false,
            handBaggageExist: true,
            supplierCode: '',
            responseHasZeroValueSeat: false,
            responseHasZeroValueBaggage: false,
            responseHasZeroValueMeal: false,
            baggageInit: false,
            hasbookOnHold: false,
            issueFlag: false,
            isSeatOk : false,
            isMealOk : false,
            contactError: false,
        };
    },

    created: function () {
        $('[data-toggle="tooltip"]').tooltip();
        var self = this;
        this.getPagecontent();
        this.getTermsnConditions();
        var defaultCountry = 'IN';
        var AirlinesDatasSorted = AirlinesDatas;
        AirlinesDatasSorted.sort(SortByName);
        var selectFlightFromSession = localStorage.selectedFlight;
        selectFlightFromSessionObj = JSON.parse(selectFlightFromSession);
        self.SupplierId = selectFlightFromSessionObj.selectedFlightInfo.segmentRef.supplier;
        if (selectFlightFromSessionObj.selectedFlightInfo.segmentRef.supplier == "44" ||
            selectFlightFromSessionObj.selectedFlightInfo.segmentRef.supplier == "49" ||
            selectFlightFromSessionObj.selectedFlightInfo.segmentRef.supplier == "50" ||
            selectFlightFromSessionObj.selectedFlightInfo.segmentRef.supplier == "54" ||
            selectFlightFromSessionObj.selectedFlightInfo.segmentRef.supplier == "69" ||
            selectFlightFromSessionObj.selectedFlightInfo.segmentRef.supplier == "71" ||
            selectFlightFromSessionObj.selectedFlightInfo.segmentRef.supplier == "86" ||
            selectFlightFromSessionObj.selectedFlightInfo.segmentRef.supplier == "1") {
            self.ShowExtrasDiv = true;
        }
        oldTotalfare = selectFlightFromSessionObj.selectedFlightInfo.fare.amount;
        $.each(selectFlightFromSessionObj.selectedFlightInfo.groupOfFlights, function (grpindex, groupOfFlight) {
            groupOfFlight['segDirectionID'] = grpindex;
            if (groupOfFlight.flightDetails.length > 1) {
                groupOfFlight['stopCount'] = groupOfFlight.flightDetails.length;
                groupOfFlight['stopCountText'] = groupOfFlight.flightDetails.length.toString() + ' ';
            } else {
                groupOfFlight['stopCount'] = 0;
                groupOfFlight['stopCountText'] = 'Non-';
            }
            var index = 0;
            $.each(groupOfFlight.flightDetails, function (fltdetindex, flightDetail) {
                var elapsedTime = "";
                // try {
                //     elapsedTime = self.GetFlightdurationtime(flightDetail.fInfo.location.locationFrom,
                //         flightDetail.fInfo.location.locationTo,
                //         getdatetimeformated(flightDetail.fInfo.dateTime.depDate, 'DDMMYY', 'YYYY-MM-DD') + ' ' +
                //         gettimeformated(flightDetail.fInfo.dateTime.depTime, 'HHmm', 'HH', ':', 'mm', ':00') + ".0 +0000",
                //         getdatetimeformated(flightDetail.fInfo.dateTime.arrDate, 'DDMMYY', 'YYYY-MM-DD') + ' ' +
                //         gettimeformated(flightDetail.fInfo.dateTime.arrTime, 'HHmm', 'HH', ':', 'mm', ':00') + ".0 +0000");
                // } catch (error) {}
                flightDetail.fInfo.location['origin'] = getAirport(flightDetail.fInfo.location.locationFrom);
                flightDetail.fInfo.location['fromTerminalValue'] = isNullorUndefind(flightDetail.fInfo.location.fromTerminal) ? 'Terminal N/A' : 'Terminal ' + flightDetail.fInfo.location.fromTerminal;
                flightDetail.fInfo.location['toTerminalValue'] = isNullorUndefind(flightDetail.fInfo.location.toTerminal) ? 'Terminal N/A' : 'Terminal ' + flightDetail.fInfo.location.toTerminal;
                flightDetail.fInfo.location['departure'] = getAirport(flightDetail.fInfo.location.locationTo);
                flightDetail.fInfo.dateTime['departureTime'] = gettimeformated(flightDetail.fInfo.dateTime.depTime, 'HHmm', 'HH', ': ', 'mm', '');
                flightDetail.fInfo.dateTime['arrivalTime'] = gettimeformated(flightDetail.fInfo.dateTime.arrTime, 'HHmm', 'HH', ': ', 'mm', '');
                flightDetail.fInfo.dateTime['dateTimeFormatedDepDate'] = getdatetimeformated(flightDetail.fInfo.dateTime.depDate, 'DDMMYY', 'DD MMM YYYY');
                flightDetail.fInfo.dateTime['dateTimeFormatedDepDate1'] = getdatetimeformated(flightDetail.fInfo.dateTime.depDate, 'DDMMYY', 'DD MMM YYYY');
                flightDetail.fInfo.dateTime['dateTimeFormatedDepTime'] = flightDetail.fInfo.elapsedTime ? self.getElapseTime(flightDetail.fInfo.elapsedTime, 'HHmm', 'HH', 'h ', 'mm', 'm') : elapsedTime;
                flightDetail.fInfo.dateTime['dateTimeFormatedDepDateTimeDisplay'] = getdatetimeformated(flightDetail.fInfo.dateTime.depTime + flightDetail.fInfo.dateTime.depDate, 'HHmmDDMMYY', 'DD MMM YYYY');
                flightDetail.fInfo.dateTime['dateTimeFormatedArrDate'] = getdatetimeformated(flightDetail.fInfo.dateTime.arrDate, 'DDMMYY', 'ddd, DD MMM YYYY');
                flightDetail.fInfo.dateTime['dateTimeFormatedArrTime'] = flightDetail.fInfo.elapsedTime ? self.getElapseTime(flightDetail.fInfo.elapsedTime, 'HHmm', 'HH', 'h ', 'mm', 'm') : elapsedTime;
                flightDetail.fInfo.dateTime['dateTimeFormatedArrDateTimeDisplay'] = getdatetimeformated(flightDetail.fInfo.dateTime.arrTime + flightDetail.fInfo.dateTime.arrDate, 'HHmmDDMMYY', 'DD MMM YYYY');
                flightDetail.fInfo.dateTime['calculateDatetimeDiff'] = calculateDatetimeDiff(flightDetail.fInfo.dateTime.depDate + flightDetail.fInfo.dateTime.depTime, 'DDMMYYHHmm', flightDetail.fInfo.dateTime.arrDate + flightDetail.fInfo.dateTime.arrTime, 'DDMMYYHHmm');
                flightDetail.fInfo.airCraftName = getAirCraftName(getAirCraftEquip(selectFlightFromSessionObj, flightDetail.fInfo.location.locationFrom, flightDetail.fInfo.location.locationTo));
                flightDetail.fInfo.cabinClass = getCabinClass(flightDetail.fInfo.bookingClass);
                flightDetail.fInfo.layOver = getLayover(index, groupOfFlight.flightDetails.length);
                flightDetail.fInfo.layOverTime = getLayoverTime(index, groupOfFlight.flightDetails);
                index++;
            });
        });
        selectFlightFromSessionObj.paxList = [];
        var totalPaxUnites = 0;
        var travllerIndex = 1;
        var airRevalidate = selectFlightFromSessionObj.selectFlightRq.request.content.commonRequestFarePricer.body.airRevalidate;
        var adtcount = airRevalidate.adt;
        var chdcount = airRevalidate.chd;
        var infcount = airRevalidate.inf;
        setPaxList('ADT', adtcount);
        setPaxList('CHD', chdcount);
        setPaxList('INF', infcount);

        function setPaxList(paxType, count) {
            if (selectFlightFromSessionObj.paxList == undefined) {
                selectFlightFromSessionObj.paxList = [];
            }
            var paxAvailbleTitles = paxTitles.filter(function (title) {
                return jQuery.inArray(paxType.toUpperCase(), title.paxType) != -1 ? true : false;
            });
            var titleOptions = getTitleOptions(paxAvailbleTitles);
            for (var paxIndex = 1; paxIndex <= count;) {
                selectFlightFromSessionObj.paxList.push({
                    paxType: getPaxType(paxType, 1),
                    paxTypeCode: paxType,
                    paxIndex: paxIndex++,
                    travllerIndex: travllerIndex++,
                    titleOptions: titleOptions,
                    selected: titleOptions[0],
                    nationalitycountry: {
                        //selected: defaultCountry,
                        countries: countryList
                    },
                    issuecountry: {
                        //selected: defaultCountry,
                        countries: countryList
                    },
                    AirlinesDatas: AirlinesDatasSorted,
                    airlineSelected: '',
                });
            }
        }

        try {
            var vm = this;
            var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = vm.commonStore.hubUrls.hubConnection.hubServices.flights.airSelect;
            this.selectFlightFromSession = selectFlightFromSessionObj;
            console.log('Selected Flight:', selectFlightFromSessionObj);
            var PostData = selectFlightFromSessionObj.selectFlightRq;
            console.log(JSON.stringify(PostData));


            var config = {
                axiosConfig: {
                    method: "POST",
                    url: hubUrl + serviceUrl,
                    data: PostData
                },
                successCallback: function (response) {
                    console.log("RESPONSE RECEIVED: ", response);
                    localStorage.accessToken = response.headers.access_token;
                    vm.checkPriceChange(parseFloat(response.data.response.content.fareInformationWithoutPnrReply.flightFareDetails.totalFare));
                    vm.initData(response.data);
                },
                errorCallback: function (error) {
                    //   if (self.Alert_Label == '') {
                    //       getPagecontent(function () {
                    //           alertify.alert(self.Alert_Label, self.Message_UnexpectSupplierIssue).setting({ 'closable': false, onclose: function () { window.location = '/search.html'; } }).set('label', self.Ok);
                    //       });
                    //   } else {
                    //       alertify.alert(self.Alert_Label, self.Message_UnexpectSupplierIssue).setting({ 'closable': false, onclose: function () { window.location = '/search.html'; } }).set('label', self.Ok);
                    //   }
                },
                showAlert: true
            };
            mainAxiosRequest(config);

        } catch (err) {
            if (self.Alert_Label == '') {
                self.getPagecontent(function () {
                    alertify.alert(self.Alert_Label, self.Message_UnexpectSupplierIssue).setting({
                        'closable': false,
                        onclose: function () {
                            window.location = '/search.html';
                        }
                    }.set('label', self.Ok));
                });
            } else {
                alertify.alert(self.Alert_Label, self.Message_UnexpectSupplierIssue).setting({
                    'closable': false,
                    onclose: function () {
                        window.location = '/search.html';
                    }
                }.set('label', self.Ok));
            }
        }
    },
    methods: {
        selectAncillaryTab: function (ancillary) {
            switch (ancillary) {
                case "Seat":
                    this.SetAncillerySeats();
                    break;
                case "Baggage":
                    this.SetAncillerBaggage();
                    break;
                case "Meals":
                    this.SetAncilleryMeals();
                    break;
                default:
                    break;
            }
            this.selectedTab = ancillary;
            var vm = this;
            if (vm.SupplierId == "71" || vm.SupplierId == "49" || vm.SupplierId == "50" || vm.SupplierId == "44") {
                if (!vm.baggageInit) {
                    setTimeout(() => {
                        const elem = vm.$refs.baggageRef;
                        for (var el = 0; el < elem.length; el++) {
                            elem[el].click();
                        }
                    }, 100);
                    vm.baggageInit = true;
                }
            }
        },
        selectTab() {
            this.selectedTab = this.tab.title;
        },
        openBaggageFunc: function () {
            document.getElementById('defaultOpenBaggage').click();
        },
        GetFlightdurationtime: function (FromGo, ToGo, DepTimeGo, ArriTimeGo) {
            return GetFlightdurationtime(FromGo, ToGo, DepTimeGo, ArriTimeGo);
        },
        onSelectedCodeMain: _.debounce(function (event) {
            if (event.target.value.length > 0) {
                var newData = [];

                this.countryCodes.forEach(function (el) {
                    if (el[2].toLowerCase().match(RegExp('^' + event.target.value.toLowerCase()))) {
                        newData.push(el);
                    }
                });

                this.countryCodes.forEach(function (el) {
                    if (el[2].toLowerCase().indexOf(event.target.value.toLowerCase()) >= 0 &&
                        !(el[2].toLowerCase().match(RegExp('^' + event.target.value.toLowerCase())))) {
                        newData.push(el);
                    }
                });
                if (newData.length > 0) {
                    this.coutryCodeListMain = newData;
                } else {
                    this.coutryCodeListMain = [];
                }
            } else {
                this.coutryCodeListMain = [];
            }
        }, 100),
        onSelectedCountryMain: function (country, code) {
            if (code == 2) {
                $('#txtLeadPaxPhone4').val(country[2]);
            } else {
                $('#txtLeadPaxPhone2').val(country[2]);
            }
            this.coutryCodeListMain = [];
        },
        onSelectedCode: _.debounce(function (value, index) {
            if (value.length > 0) {
                var newData = [];

                this.countryCodes.forEach(function (el) {
                    if (el[2].toLowerCase().match(RegExp('^' + value.toLowerCase()))) {
                        newData.push(el);
                    }
                });

                this.countryCodes.forEach(function (el) {
                    if (el[2].toLowerCase().indexOf(value.toLowerCase()) >= 0 &&
                        !(el[2].toLowerCase().match(RegExp('^' + value.toLowerCase())))) {
                        newData.push(el);
                    }
                });

                if (newData.length > 0) {
                    this.coutryCodeList = newData;
                    this.additionalContact[index].index = index;
                } else {
                    this.coutryCodeList = [];
                }
            } else {
                this.coutryCodeList = [];
            }
        }, 100),
        onSelectedCountry: function (country, index) {
            this.additionalContact[index].countryCode = country[1].toUpperCase();
            this.additionalContact[index].countryCodeNumber = country[2];
            this.additionalContact[index].index = null;
            this.coutryCodeList = [];
        },
        addAdditionalContact: function () {
            this.additionalContact.push({
                index: null,
                contactNumber: "",
                emailId: "",
                countryCode: "",
                countryCodeNumber: ""
            });
        },
        removeAdditionalContact: function (index) {
            if (index > -1) {
                this.additionalContact.splice(index, 1);
            }
        },
        paymentGatewayButton: function () {
            var self = this;
            if (self.commonStore.agencyNode.loginNode.nodetype != "HQ") {
                self.getHQPaymentGateway();
            } else {
                self.creditCardButton(self.commonStore.agencyNode.loginNode.paymentGateways);
            }
        },
        creditCardButton: function (paymetGateways) {
            var self = this;
            if (paymetGateways) {
                var paymentMethods = paymetGateways.filter(function (item) {
                    return item.id == 2;
                });
                if (self.commonStore.commonRoles.hasAirPaymentGateway && paymentMethods) {
                    self.showCreditCardButton = true;
                }
            } else {
                self.showCreditCardButton = false;
                self.bookType = 0;
            }
        },
        // getminiRules: function () {
        //     var airMiniRules = this.miniRuleData;
        //     var miniRuleDiv = '';
        //     var miniRuleHeadDiv = '';
        //     $.each(airMiniRules, function (segIndex, miniRule) {
        //         if (hasMiniRule(miniRule)) {
        //             if (isNullorEmpty(miniRuleHeadDiv)) {
        //                 miniRuleHeadDiv +=
        //                     '<div class="mini_rule">' +
        //                     '<ul>';
        //             }
        //             var titleHead = airportLocationFromAirportCode(miniRule.departure) + '(' + miniRule.departure.toUpperCase() + ') to ' + airportLocationFromAirportCode(miniRule.arrival) + '(' + miniRule.arrival.toUpperCase() + ')';
        //             miniRuleHeadDiv +=
        //                 '<li class="mini_tablinks" onclick="openTab(event, \'tab_rule' + segIndex + '\', \'' + titleHead + '\')">' + miniRule.departure.toUpperCase() + ' to ' + miniRule.arrival.toUpperCase() + '</li>';
        //         }
        //     });
        //     if (!isNullorEmpty(miniRuleHeadDiv)) {
        //         miniRuleHeadDiv += '</ul>' + '</div>' + '<h3 class="mini_tab_head">First Tab</h3>';
        //     }

        //     var miniRuleCatDiv = '';
        //     var miniRuleCatTabDescDiv = '';
        // $.each(airMiniRules, function (segIndex, miniRule) {
        //     if (hasMiniRule(miniRule)) {
        //         miniRuleCatDiv += '<div class="tab_rule" id="tab_rule' + segIndex + '" style="display:none;">';
        //         var miniRuleCatTabDiv = '';
        //         try {
        //             var totCat = miniRule.codes[0].category.length;
        //             $.each(miniRule.codes[0].category, function (categoryIndex, catgry) {
        //                 if (
        //                     (!isNullorEmpty(catgry.dateInfo) && !isNullorUndefind(catgry.dateInfo)) ||
        //                     (!isNullorEmpty(catgry.monInfo) && !isNullorUndefind(catgry.monInfo)) ||
        //                     (!isNullorEmpty(catgry.restriAppInfo) && !isNullorUndefind(catgry.restriAppInfo))
        //                 ) {
        //                     if (categoryIndex == 0) {
        //                         miniRuleCatDiv += '<div class="tabmini">';
        //                     }
        //                     miniRuleCatDiv += '<button class="tablinks" onclick="openSubTab(event, \'catgry_' + segIndex + '_' + catgry.cat + '\')"' + (categoryIndex == 0 ? ' id="defaultOpen"' : '') + '>' + catgry.desc + '</button>';

        //                     miniRuleCatTabDiv += '<div id="catgry_' + segIndex + '_' + catgry.cat + '" class="tabcontent">';
        //                     miniRuleCatTabDiv += '<div class="content_tab_sec">';

        //                     // Restrictions Applicability Info
        //                     if (!isNullorEmpty(catgry.restriAppInfo) && !isNullorUndefind(catgry.restriAppInfo)) {
        //                         miniRuleCatTabDiv += '<h3>Restrictions Applicability Info</h3>';
        //                         $.each(catgry.restriAppInfo, function (restriappinfoIndex, restriappinfo) {
        //                             miniRuleCatTabDiv += '<p>';
        //                             miniRuleCatTabDiv += isNullorEmptyToBlank(restriappinfo.desc);
        //                             miniRuleCatTabDiv += ((isNullorEmpty(restriappinfo.situation) ? '' : ' ' + restriappinfo.situation.toLowerCase()));
        //                             miniRuleCatTabDiv += ((isNullorEmpty(restriappinfo.value) ? '' : ' - ' + restriappinfo.value));
        //                             miniRuleCatTabDiv += '</p>';
        //                         });
        //                     }

        //                     // Monetary Info
        //                     if (!isNullorEmpty(catgry.monInfo) && !isNullorUndefind(catgry.monInfo)) {
        //                         miniRuleCatTabDiv += '<h3>Monetary Info</h3>';
        //                         $.each(catgry.monInfo, function (moninfoIndex, moninfo) {
        //                             miniRuleCatTabDiv += '<p>';
        //                             miniRuleCatTabDiv += isNullorEmptyToBlank(moninfo.desc);
        //                             miniRuleCatTabDiv += ((isNullorEmpty(moninfo.situation) ? '' : ' ' + moninfo.situation.toLowerCase()));
        //                             miniRuleCatTabDiv += ((isNullorEmpty(moninfo.currency) && isNullorEmpty(moninfo.amount) ? '' : ' - '));
        //                             miniRuleCatTabDiv += isNullorEmptyToBlank(moninfo.currency) + ' ' + isNullorEmptyToBlank(moninfo.amount);
        //                             miniRuleCatTabDiv += '</p>';
        //                         });
        //                     }

        //                     // Date Info
        //                     if (!isNullorEmpty(catgry.dateInfo) && !isNullorUndefind(catgry.dateInfo)) {
        //                         miniRuleCatTabDiv += '<h3>Date Info</h3>';
        //                         $.each(catgry.dateInfo, function (dateinfoIndex, dateinfo) {
        //                             miniRuleCatTabDiv += '<p>';
        //                             miniRuleCatTabDiv += isNullorEmptyToBlank(dateinfo.desc);
        //                             miniRuleCatTabDiv += ((isNullorEmpty(dateinfo.situation) ? '' : ' ' + dateinfo.situation.toLowerCase()));
        //                             miniRuleCatTabDiv += ((isNullorEmpty(dateinfo.date) && isNullorEmpty(dateinfo.time) ? '' : ' - '));
        //                             try { miniRuleCatTabDiv += dateinfo.date.substring(0, 2) + ' ' + dateinfo.date.substring(2, 5) + ', 20' + dateinfo.date.substring(5, 7); } catch (ex) { }
        //                             try { miniRuleCatTabDiv += ' ' + dateinfo.time.substring(0, 2) + ':' + dateinfo.time.substring(2, 4); } catch (ex) { }
        //                             miniRuleCatTabDiv += '</p>';
        //                         });
        //                     }

        //                     miniRuleCatTabDiv += '</div>';
        //                     miniRuleCatTabDiv += '</div>';

        //                     if (categoryIndex + 1 == totCat) {
        //                         miniRuleCatDiv += '</div>';
        //                     }
        //                 }
        //             });
        //             miniRuleCatDiv += (miniRuleCatTabDiv + '</div>');
        //         } catch (ex) {
        //             var sampid = 'catgry_' + segIndex + '_nodata';
        //             miniRuleCatDiv += '<div class="tabmini">';
        //             miniRuleCatDiv += '<button class="tablinks" onclick="openSubTab(event, \'' + sampid + '\')" id="defaultOpen">Category</button>';
        //             miniRuleCatDiv += '</div>';
        //             miniRuleCatDiv += '<div id="' + sampid + '" class="tabcontent"><div class="content_tab_sec"><p><i class="fa fa-exclamation-circle"></i> No information available</p></div></div>';
        //             miniRuleCatDiv += (miniRuleCatTabDiv + '</div>');
        //             console.log(ex);
        //         }
        //     }
        // });

        //     miniRuleDiv = miniRuleHeadDiv + miniRuleCatDiv;

        //     if (miniRuleDiv != '') {
        //         this.miniRuleDiv = miniRuleDiv;
        //     } else {
        //         this.miniRuleDiv = '<p>Mini rules not available for this trip !</p>';
        //     }

        //     try {
        //         setTimeout(() => {
        //             document.getElementsByClassName("mini_rule")[0].getElementsByTagName('ul')[0].getElementsByTagName('li')[0].click();
        //         }, 100);
        //     } catch (ex) { }

        // },
        getFareRules: function () {
            var fareRuleDiv = '';

            var airFareRules = this.fareRuleData;
            $('#popFareRule').show();
            $.each(airFareRules, function (segIndex, fareRule) {
                if (hasFareRule(fareRule)) {
                    fareRuleDiv += '<div class="itinerary_head">' +
                        '<h2>' +
                        '<i class="fa fa-plane summary_flight"></i>' +
                        airportLocationFromAirportCode(fareRule.departure) + '(' + fareRule.departure.toUpperCase() + ') to ' +
                        airportLocationFromAirportCode(fareRule.arrival) + '(' + fareRule.arrival + ')' +
                        '</h2>' +
                        '</div> ';
                    if (fareRule.fareRuleDetails && fareRule.fareRuleDetails.length > 0) {
                        $.each(fareRule.fareRuleDetails, function (ruleIndex, rule) {
                            if (!stringIsNullorEmpty(rule.ruleHead)) {
                                fareRuleDiv += '<a href="#rule' + segIndex + ruleIndex + '" data-toggle="collapse">' + rule.ruleHead + '<i class="chevron fa fa-fw" ></i></a><br>' +
                                    '<p id="rule' + segIndex + ruleIndex + '" class="collapse">' + rule.ruleBody + '</p>';
                            } else {
                                fareRuleDiv += '<p style="padding-top: 16px;float: left;">No fare rule available for this segment</p>';
                            }
                        });
                    }
                }
            });

            if (fareRuleDiv != '') {
                this.fareRuleDiv = fareRuleDiv;
            } else {
                this.fareRuleDiv = '<p>Fare rules not available for this trip !</p>';
            }
        },
        bindSummary: function (item) {
            var self = this;
            var rules;
            var before = item.find(x => x.situation.toLowerCase() == 'before departure');
            if (before && before.allowed) {
                if (before.amount > 0) {
                    // rules = '<p>' + before.cat + '</p><p class="allowed">Allowed @ ' + self.$n(parseFloat(before.amount / self.commonStore.currencyMultiplier), 'currency', self.commonStore.selectedCurrency) + '</p>'
                    rules = '<p>' + before.cat + '</p><p class="allowed">Allowed @ ' + before.currency + '&nbsp;' + before.amount + '</p>'

                } else {
                    rules = '<p>' + before.cat + '</p><p class="allowed">Allowed</p>';
                }

            } else {
                rules = '<p>' + before.cat + '</p><p class="not-allowed">Not Allowed</p>';
            }

            var after = item.find(x => x.situation.toLowerCase() == 'after departure');
            if (after && after.allowed) {
                if (after.amount > 0) {
                    // rules = rules + '<p class="allowed">Allowed @ ' + self.$n(parseFloat(after.amount / self.commonStore.currencyMultiplier), 'currency', self.commonStore.selectedCurrency) + '</p>'
                    rules = rules + '<p class="allowed">Allowed @ ' + after.currency + '&nbsp;' + after.amount + '</p>'

                } else {
                    rules = rules + '<p class="allowed">Allowed</p>';
                }

            } else {
                rules = rules + '<p class="not-allowed">Not Allowed</p>';
            }
            return rules;

        },
        getAirLineName: function (airlinecode) {
            return getAirLineName(airlinecode);
        },
        getAirLine: function (airlineCode) {
            var airline;
            try {
                airline = $.grep(this.AirlinesDatas, function (value) {
                    return (value.C.toUpperCase() == airlineCode.toUpperCase());
                });
            } catch (err) {}
            if (airline == null || airline == undefined || airline.length == 0) {
                airline = {
                    "C": airlineCode.toUpperCase(),
                    "A": airlineCode.toUpperCase(),
                    "T": airlineCode.toUpperCase(),
                    "S": "true"
                };
            } else {
                airline = airline[0];
            }
            return airline;
        },
        airportFromAirportCode: function (airport) {
            return airportFromAirportCode(airport);
        },
        roundAmount: function (amount) {
            return roundAmount(amount);
        },
        airportLocationFromAirportCode: function (airlinecode) {
            return airportLocationFromAirportCode(airlinecode);
        },
        getElapseTime: function (elapStr) {
            if (elapStr) {
                return elapStr.substring(0, 2) + 'h ' + elapStr.substring(2, 4) + 'm ';
            }
            return '';
        },
        momCommonFun: function (date, frmFormat, toFormat) {
            return moment(date, frmFormat).format(toFormat);
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            //getAgencycode(function (response) {
            var vm = this;
            var huburl = vm.commonStore.hubUrls.hubConnection.cmsUrl;
            var portno = vm.commonStore.hubUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            //var langauage = 'ar';
            self.dir = langauage == "ar" ? "rtl" : "ltr";

            var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + self.commonAgencyCode + '/Template/Flight Booking/Flight Booking/Flight Booking.ftl';
            axios.get(cmsPage, {
                headers: {
                    'content-type': 'text/html',
                    'Accept': 'text/html',
                    'Accept-Language': langauage
                }
            }).then(function (response) {
                self.content = response.data;
                // self.getdata = false;
                if (response.data.area_List.length > 0) {
                    var mainComp = self.pluckcom('main', self.content.area_List); // response.data.area_List[0].main;
                    var flightComp = self.pluckcom('FlightInfoSection', self.content.area_List); // response.data.area_List[1].FlightInfoSection;
                    var travellerComp = self.pluckcom('TravellerInfoSection', self.content.area_List); //  response.data.area_List[2].TravellerInfoSection;
                    var agntDetComp = self.pluckcom('AgentDetailsSection', self.content.area_List); //  response.data.area_List[3].AgentDetailsSection;
                    var paymentComp = self.pluckcom('PaymentModeSection', self.content.area_List); //  response.data.area_List[4].PaymentModeSection;
                    var fareComp = self.pluckcom('FareInfoSection', self.content.area_List); //  response.data.area_List[5].FareInfoSection;
                    var anxComp = self.pluckcom('AnxilleryInfoSection', self.content.area_List); //  response.data.area_List[6].AnxilleryInfoSection;
                    var alertsComp = self.pluckcom('Alerts', self.content.area_List); //  response.data.area_List[7].Alerts;
                    var PassengerComp = self.pluckcom('PassengerDetailsSection', self.content.area_List); // 

                    //Main Component
                    self.Booking = self.pluckcom('Booking_Label', mainComp.component);
                    self.Confirmation = self.pluckcom('Confirmation_Label', mainComp.component);
                    self.Available_Credit_limit = self.pluckcom('Available_Credit_Limit_Label', mainComp.component);
                    self.Agree_Label1 = self.pluckcom('Agree_Label1', mainComp.component);
                    self.Agree_Label2 = self.pluckcom('Agree_Label2', mainComp.component);
                    self.Agree_Label3 = self.pluckcom('Agree_Label3', mainComp.component);
                    self.Privacy_Policy_Label = self.pluckcom('Privacy_Policy_Label', mainComp.component);
                    self.Terms_Label = self.pluckcom('Terms_Label', mainComp.component);
                    self.Conditions_Label = self.pluckcom('Conditions_Label', mainComp.component);
                    self.Confirm_Booking_Button = self.pluckcom('Confirm_Booking_Button_Label', mainComp.component);
                    self.Proceed_Payment_Button_Label = self.pluckcom('Proceed_Payment_Button_Label', mainComp.component);
                    self.Proceed_to_Issuance = self.pluckcom('Proceed_to_Issuance', mainComp.component);
                    var temptype = '';
                    try {
                        temptype = selectFlightFromSessionObj.selectedFlightInfo.typeRef.type;
                        var currentSupplier = selectFlightFromSessionObj.selectedFlightInfo.segmentRef.supplier;
                        if (temptype == "WebFare" || (currentSupplier == '44' || currentSupplier == '49' || currentSupplier == '50' || currentSupplier == '71')) {
                            self.Confirm_Booking_Button = self.Proceed_to_Issuance;
                            self.isLcc = true;
                            self.issueFlag = true;
                        }
                    } catch (err) {}
                    self.Booking_Button = self.Confirm_Booking_Button;
                    //Flights section
                    self.Review_your_flight = self.pluckcom('Review_your_flight_Label', flightComp.component);
                    self.Cabin = self.pluckcom('Cabin_Label', flightComp.component);
                    self.RBD = self.pluckcom('RBD_Label', flightComp.component);
                    self.Baggage = self.pluckcom('Baggage_Label', flightComp.component);
                    self.Cabin_Baggage = self.pluckcom('Cabin_baggage_Label', flightComp.component);
                    self.Mini_Rules = self.pluckcom('Mini_Rules_Label', flightComp.component);
                    self.Fare_Rules = self.pluckcom('Fare_Rules_Label', flightComp.component);
                    self.from = self.pluckcom('From_Label', flightComp.component);
                    self.to = self.pluckcom('To_Label', flightComp.component);
                    self.Layover_at = self.pluckcom('Layover_at_Label', flightComp.component);
                    self.No_baggage_available_label = self.pluckcom('No_baggage_available_label', flightComp.component);
                    self.summary = self.pluckcom('Summary_Label', flightComp.component);
                    self.Detail = self.pluckcom('Details_Label', flightComp.component);
                    self.after = self.pluckcom('After_Departure_Label', flightComp.component);
                    self.before = self.pluckcom('Before_Departure_Label', flightComp.component);
                    self.rule = self.pluckcom('Rules_Label', flightComp.component);
                    //Traveller section
                    self.Traveller = self.pluckcom('Traveller_Label', travellerComp.component);
                    self.Required_Field = self.pluckcom('Required_field_Label', travellerComp.component);
                    self.Title = self.pluckcom('Title_Label', travellerComp.component);
                    self.First_Name = self.pluckcom('First_Name_Label', travellerComp.component);
                    self.Last_Name = self.pluckcom('Last_Name_Label', travellerComp.component);
                    self.Dob = self.pluckcom('Date_of_Birth_Label', travellerComp.component);
                    self.Nationality = self.pluckcom('Nationality_Label', travellerComp.component);
                    self.Passport_Number = self.pluckcom('Passport_Number_Label', travellerComp.component);
                    self.Expiry_Date = self.pluckcom('Expiry_Date_Label', travellerComp.component);
                    self.Issue_Date = self.pluckcom('Issue_Date_Label', travellerComp.component);
                    self.Issue_Country = self.pluckcom('Issue_Country_Label', travellerComp.component);
                    self.Frequent_Flyer = self.pluckcom('Frequent_Flyer_Label', travellerComp.component);
                    self.Frequent_Flyer_Number = self.pluckcom('Frequent_Flyer_Number_Label', travellerComp.component);
                    self.PlaceHolder_Date = self.pluckcom('PlaceHolder_Date', travellerComp.component);
                    self.PlaceHolder_Country = self.pluckcom('PlaceHolder_Country', travellerComp.component);
                    self.Select_Country = self.pluckcom('Select_Country', travellerComp.component);
                    self.ArlineSelectedLbl = self.pluckcom('Airline_Selected', travellerComp.component);
                    self.Provide_Traveller_Details = self.pluckcom('Provide_Traveller_Details_Label', travellerComp.component);

                    //Agency Section
                    self.Agent_Details = self.pluckcom('Agent_Details_Label', agntDetComp.component);
                    self.Agency_Name = self.pluckcom('Agency_Name_Label', agntDetComp.component);
                    self.User_Name = self.pluckcom('User_Name_Label', agntDetComp.component);
                    self.Email = self.pluckcom('Email_Label', agntDetComp.component);
                    self.Phone_Number = self.pluckcom('Phone_Number_Label', agntDetComp.component);

                    //Payment mode section
                    self.Choose_Payment_Mode = self.pluckcom('Choose_Payment_Mode_Label', paymentComp.component);
                    self.Credit = self.pluckcom('Credit_Label', paymentComp.component);
                    self.Payment_Gateway = self.pluckcom('Payment_Gateway_Label', paymentComp.component);

                    //Fare Section
                    self.Fare_Summary = self.pluckcom('Fare_Summary_Label', fareComp.component);
                    self.Tickets = self.pluckcom('Tickets_Label', fareComp.component);
                    self.Oneway = self.pluckcom('Oneway_Label', fareComp.component);
                    self.Round_Trip = self.pluckcom('Round_Trip_Label', fareComp.component);
                    self.Adult = self.pluckcom('Adult_Label', fareComp.component);
                    self.Child = self.pluckcom('Child_Label', fareComp.component);
                    self.Infant = self.pluckcom('Infant_Label', fareComp.component);
                    self.Net_Fare = self.pluckcom('Net_Fare_Label', fareComp.component);
                    self.Taxes_and_Fees = self.pluckcom('Taxes_and_Fees_Label', fareComp.component);
                    self.Taxes_Label = self.pluckcom('Taxes_Label', fareComp.component);
                    self.Discount_Label = self.pluckcom('Discount_Label', fareComp.component);
                    self.Service_Fee_Label = self.pluckcom('Service_Fee_Label', fareComp.component);
                    self.Commission_Label = self.pluckcom('Commission_Label', fareComp.component);
                    self.VAT = self.pluckcom('VAT_Label', fareComp.component);
                    self.Trip_Total = self.pluckcom('Trip_Total_Label', fareComp.component);
                    self.Taxesfees_Included = self.pluckcom('Taxes_and_fees_included_Label', fareComp.component);
                    self.Base_Fare = self.pluckcom('Base_Fare_Label', fareComp.component);

                    //Anxillery
                    self.Baggage_Label = self.pluckcom('Baggage_Label', anxComp.component);
                    self.Select_Baggae_For_Your_Trip_Label = self.pluckcom('Select_Baggae_For_Your_Trip_Label', anxComp.component);
                    self.Skip_Baggage_Selection_Label = self.pluckcom('Skip_Baggage_Selection_Label', anxComp.component);
                    self.Confirm_Selection_Button_Label = self.pluckcom('Confirm_Selection_Button_Label', anxComp.component);
                    self.To_Label = self.pluckcom('To_Label', anxComp.component);
                    self.Edit_Button_Label = self.pluckcom('Edit_Button_Label', anxComp.component);
                    self.Close_Button_Label = self.pluckcom('Close_Button_Label', anxComp.component);
                    self.No_Baggage_Label = self.pluckcom('No_Baggage_Label', anxComp.component);
                    self.Select_Label = self.pluckcom('Select_Label', anxComp.component);
                    self.Seat_Map_Label = self.pluckcom('Seat_Map_Label', anxComp.component);
                    self.Seat_Label = self.pluckcom('Seat_Label', anxComp.component);
                    self.Select_Seats_For_Your_Trip_Label = self.pluckcom('Select_Seats_For_Your_Trip_Label', anxComp.component);
                    self.Fllight_Seat_Label = self.pluckcom('Fllight_Seat_Label', anxComp.component);
                    self.Seatmap_Legend_Label = self.pluckcom('Seatmap_Legend_Label', anxComp.component);
                    self.Available_Label = self.pluckcom('Available_Label', anxComp.component);
                    self.Occupied_Label = self.pluckcom('Occupied_Label', anxComp.component);
                    self.Selected_Label = self.pluckcom('Selected_Label', anxComp.component);
                    self.Add_your_Meals_Label = self.pluckcom('Add_your_Meals_Label', anxComp.component);
                    self.Select_meals_for_your_Trip_Label = self.pluckcom('Select_meals_for_your_Trip_Label', anxComp.component);
                    self.Meals_Label = self.pluckcom('Meals_Label', anxComp.component);
                    self.Placeholder_Search = self.pluckcom('Placeholder_Search', anxComp.component);
                    self.Placeholder_SelectAll = self.pluckcom('Placeholder_SelectAll', anxComp.component);
                    self.Placeholder_SortbyPrice = self.pluckcom('Placeholder_SortbyPrice', anxComp.component);
                    self.Skip_Meal_Selection_Label = self.pluckcom('Skip_Meal_Selection_Label', anxComp.component);
                    self.Add_this_Meal_Label = self.pluckcom('Add_this_Meal_Label', anxComp.component);
                    self.Skip_Seat_Selection_Label = self.pluckcom('Skip_Seat_Selection_Label', anxComp.component);
                    self.Insurance_Label = self.pluckcom('Insurance_Label', anxComp.component);
                    self.Assistance_Label = self.pluckcom('Assistance_Label', anxComp.component);

                    //alerts
                    self.Alert_Label = self.pluckcom('Alert_Label', alertsComp.component);
                    self.Warning_Label = self.pluckcom('Warning_Label', alertsComp.component);
                    self.Ok = self.pluckcom('Ok', alertsComp.component);
                    self.Please_Fill_Label = self.pluckcom('Please_Fill_Label', alertsComp.component);
                    self.Terms_and_Conditions_Label = self.pluckcom('Terms_and_Conditions_Label', alertsComp.component);
                    self.Please_agree = self.pluckcom('Please_agree', alertsComp.component);
                    self.Message_UnexpectSupplierIssue = self.pluckcom('Message_UnexpectSupplierIssue', alertsComp.component);
                    self.Duplicated_Names_alert_Msg = self.pluckcom('Duplicated_Names_alert_Msg', alertsComp.component);
                    self.No_Booking_privillage_alert_Msg = self.pluckcom('No_Booking_privillage_alert_Msg', alertsComp.component);
                    self.Fields_Mandatory_alert_Msg = self.pluckcom('Fields_Mandatory_alert_Msg', alertsComp.component);
                    self.Payment_Gateway_Not_Available_alert = self.pluckcom('Payment_Gateway_Not_Available_alert', alertsComp.component);
                    self.Booking_failed_alert_Msg = self.pluckcom('Booking_failed_alert_Msg', alertsComp.component);
                    self.Booking_success_alert_Msg = self.pluckcom('Booking_success_alert_Msg', alertsComp.component);
                    self.Please_fill_first_Name_alert_msg = self.pluckcom('Please_fill_first_Name_alert_msg', alertsComp.component);
                    self.FirstName_Min_Two_Char_alert_msg = self.pluckcom('FirstName_Min_Two_Char_alert_msg', alertsComp.component);
                    self.SecondName_Min_Two_Char_alert_msg = self.pluckcom('SecondName_Min_Two_Char_alert_msg', alertsComp.component);
                    self.Please_fill_last_name_alert_msg = self.pluckcom('Please_fill_last_name_alert_msg', alertsComp.component);
                    self.Please_select_date_of_birth_alert = self.pluckcom('Please_select_date_of_birth_alert', alertsComp.component);
                    self.Please_select_nationality_alert_msg = self.pluckcom('Please_select_nationality_alert_msg', alertsComp.component);
                    self.Please_fill_passport_number_alert = self.pluckcom('Please_fill_passport_number_alert', alertsComp.component);
                    self.Please_select_expiry_date_alert = self.pluckcom('Please_select_expiry_date_alert', alertsComp.component);
                    self.Invalid_passport_number_alert = self.pluckcom('Invalid_passport_number_alert', alertsComp.component);
                    self.Please_select_issue_date_alert = self.pluckcom('Please_select_issue_date_alert', alertsComp.component);
                    self.Please_fill_issue_country_alert = self.pluckcom('Please_fill_issue_country_alert', alertsComp.component);
                    self.Fare_Increased_alert_Msg = self.pluckcom('Fare_Increased_alert_Msg', alertsComp.component);
                    self.Fare_Drop_alert_Msg = self.pluckcom('Fare_Drop_alert_Msg', alertsComp.component);
                    self.Continue_Btn_Label = self.pluckcom('Continue_Btn_Label', alertsComp.component);
                    self.Cancel_Btn_Label = self.pluckcom('Cancel_Btn_Label', alertsComp.component);
                    self.Ins_Purch_Alert_Heading = self.pluckcom('Ins_Purch_Alert_Heading', alertsComp.component);
                    self.Ins_Purch_Alert_Content = self.pluckcom('Ins_Purch_Alert_Content', alertsComp.component);
                    self.Choose_Payment_Mode_Alert = self.pluckcom('Choose_Payment_Mode_Alert', alertsComp.component);
                    // passenger
                    self.Phone_Label = self.pluckcom('Phone_Label', PassengerComp.component);
                    self.Phone_Placeholder = self.pluckcom('Phone_Placeholder', PassengerComp.component);
                    self.Email_Label = self.pluckcom('Email_Label', PassengerComp.component);
                    self.Email_Placeholder = self.pluckcom('Email_Placeholder', PassengerComp.component);
                    self.Passenger_Details_Label = self.pluckcom('Passenger_Details_Label', PassengerComp.component);
                    // self.Adult = self.pluckcom('adult', mainComp.component);
                    // self.Child = self.pluckcom('child', mainComp.component);
                    // self.Infant = self.pluckcom('infant', mainComp.component);
                    // self.Adults = self.pluckcom('adults', mainComp.component);
                    // self.Children = self.pluckcom('children', mainComp.component);
                    // self.Infants = self.pluckcom('infants', mainComp.component);                    
                    // self.Ticket = self.pluckcom('ticket', mainComp.component);   
                    // self.Travellers = self.pluckcom('travellers', mainComp.component);
                    // self.Proceed_to_Issuance_Button = self.pluckcom('proceed_to_issuance_button', mainComp.component);
                    // self.Proceed_to_Payment_Button = self.pluckcom('proceed_to_payment_button', mainComp.component);
                    // self.Multi_City = self.pluckcom('multi_city', mainComp.component);                    
                    // self.No_Mini_Rules = self.pluckcom('no_mini_rules', mainComp.component);     
                    flightComponent = self;
                }
            }).catch(function (error) {
                console.log('Error');
                self.content = [];
            });
            //});

        },
        getmoreinfo(url) {
            if (url != "") {
                url = url.split("/Template/")[1];
                url = url.split(' ').join('-');
                url = url.split('.').slice(0, -1).join('.')
                url = "/Nirvana/book-now.html?page=" + url;
            } else {
                url = "#";
            }
            return url;
        },
        getObjectId: function (name, paxType, number) {
            return name + paxType + number;
        },
        initData: function (response) {
            var self = this;
            try {
                selectResponseObj = response.response;
                totalFare = selectResponseObj.content.fareInformationWithoutPnrReply.flightFareDetails.totalFare;
                self.totalWithPaidServices = totalFare;
                var fsupplier = selectResponseObj.content.fareInformationWithoutPnrReply.fSupplier;
                // var fsupplier = selectResponseObj.supplierCode;
                if (fsupplier=="45"&&response.response.content.supplierSpecific
                &&response.response.content.supplierSpecific.ProviderCode == "ACH") {
                    self.hasbookOnHold = true;
                }
                if (fsupplier=="86"&&response.response.content.supplierSpecific
                &&response.response.content.supplierSpecific.hold == "1") {
                    self.hasbookOnHold = true;
                }
                var tempmsupplierSpecific = selectResponseObj.content.supplierSpecific;
                if (!isNullorUndefined(tempmsupplierSpecific)) {
                    localStorage.supplierSpecific = JSON.stringify({
                        "supplierSpecific": response.response.content.supplierSpecific
                    });
                } else {
                    localStorage.supplierSpecific = JSON.stringify({
                        "supplierSpecific": {}
                    });
                }

                if (isNullorUndefined(totalFare)) {
                    alertify.alert(self.Alert_Label, self.Message_UnexpectSupplierIssue).setting({
                        'closable': false,
                        onclose: function () {
                            window.location = '/search.html';
                        }
                    }).set('label', self.Ok);
                }
                if (parseFloat(oldTotalfare) != parseFloat(totalFare)) {
                    var updatedFare = parseFloat(totalFare);
                    var originalFare = parseFloat(oldTotalfare);
                    var difference = parseFloat(originalFare - updatedFare).toFixed(2);
                }
                if (fsupplier == "44" || fsupplier == "49" || fsupplier == "50" || fsupplier == "54" || fsupplier == "71" || fsupplier == "69" || fsupplier == "86") {
                    if (fsupplier != "54") {
                        if (self.commonStore.commonRoles.hasAirAncillarySeat) {
                            self.getSeatMapService();
                        }
                    }
                    // amadeus has only seatmap
                    if (fsupplier != "1") {
                        if (self.commonStore.commonRoles.hasAirAncillaryMeals) {
                            self.getAirMealsService();
                        }
                        if (self.commonStore.commonRoles.hasAirAncillaryBaggage) {
                            self.getAirBaggageService();
                        }
                    }

                }
                switch (selectFlightFromSessionObj.selectFlightRq.request.content.commonRequestFarePricer.body.airRevalidate.tripType.toLowerCase()) {
                    case 'o':
                        selectResponseObj.content.fareInformationWithoutPnrReply['tripTypeText'] = 'Oneway';
                        break;
                    case 'r':
                        selectResponseObj.content.fareInformationWithoutPnrReply['tripTypeText'] = 'Round Trip';
                        break;
                    case 'm':
                        selectResponseObj.content.fareInformationWithoutPnrReply['tripTypeText'] = 'Multicity';
                        break;
                }
                selectResponseObj.paxList = [];
                var totalPaxUnites = 0;
                var travllerIndex = 1;
                $.each(selectResponseObj.content.fareInformationWithoutPnrReply.paxFareDetails, function (index, paxFareDetail) {
                    totalPaxUnites += parseInt(paxFareDetail.totalPaxUnites);
                });
                var arlineBagges = [];
                $.each(selectResponseObj.content.fareInformationWithoutPnrReply.airSegments, function (index, airSegment) {
                    var fromToSeg = airportLocationFromAirportCode(airSegment.segInfo.flData.boardPoint) +
                        ' (' + airSegment.segInfo.flData.boardPoint + ') to ' +
                        airportLocationFromAirportCode(airSegment.segInfo.flData.offpoint) +
                        ' (' + airSegment.segInfo.flData.offpoint + ')';
                    var baggageinfo = [];
                    $.each(airSegment.baggageInformations, function (bagIndex, baggage) {
                        var paxType = baggage.bagPaxType;
                        var paxUnitQuantity = '';
                        var baggageTooltip = '';
                        if (paxType.toUpperCase() == 'ADT') {
                            paxType = 'Adult';
                        } else if (paxType.toUpperCase() == 'CHD') {
                            paxType = 'Child';
                        } else {
                            paxType = 'Infant';
                        }

                        var paxUnit = baggage.unit;
                        paxUnit = (paxUnit.toLowerCase() == 'n' || paxUnit.toLowerCase() == 'p' || paxUnit.toLowerCase() == 'pc' || paxUnit.toLowerCase() == 'pieces') ? baggage.baggage > 1 ? 'Pieces' : 'Piece' : 'Kg';
                        if (self.SupplierId == "69") {
                            paxUnit = baggage.unit;
                        }
                        paxUnitQuantity = baggage.baggage + ' ' + paxUnit;
                        if ((baggage.unit).toLowerCase() == "sb") {
                            baggageTooltip = 'Airline typically permit Standard baggage allowance.';
                            paxUnitQuantity = 'SB';
                        } else {
                            baggageTooltip = (paxUnit.toLowerCase() == 'piece' || paxUnit.toLowerCase() == 'pieces') ?
                                "Airline typically permit 23kg baggage weight per piece." :
                                '';
                        }
                        var baggage = {
                            paxType: paxType,
                            paxUnitQuantity: paxUnitQuantity,
                            tooltip: baggage.desc ? baggage.desc : baggageTooltip
                        }
                        var bagg = baggageinfo.filter(function (bag) {
                            return bag.paxType == baggage.paxType && bag.paxUnitQuantity == baggage.paxUnitQuantity
                        });
                        if (bagg.length == 0) {
                            baggageinfo.push(baggage);
                        }
                    });
                    var airLineBaggage = {
                        bagInfo: baggageinfo,
                        fromTo: fromToSeg,
                        segDirectionID: airSegment.segDirectionID,
                        isBaggageAddedFromAncillery: false,
                        fromToAncilleryMapping: airSegment.segInfo.flData.boardPoint + "-" + airSegment.segInfo.flData.offpoint,
                        totalBaggage: "0 KG"

                    }
                    arlineBagges.push(airLineBaggage);

                });
                this.AirLineBaggages = arlineBagges;
                this.aribaggage_null = true;
                this.AirLineBaggages.length > 0 ? self.baggageShow = true : falsecon();

                function falsecon() {
                    self.baggageShow = false
                    self.No_Seg_baggAvail = self.No_baggage_available_label;
                }


                // 

                // hand baggage
                var arlinehandBagges = [];
                var currentSupplier = selectFlightFromSessionObj.selectedFlightInfo.segmentRef.supplier;
                $.each(selectResponseObj.content.fareInformationWithoutPnrReply.airSegments, function (index, airSegment) {
                    var fromToSeg = airportLocationFromAirportCode(airSegment.segInfo.flData.boardPoint) +
                        ' (' + airSegment.segInfo.flData.boardPoint + ') to ' +
                        airportLocationFromAirportCode(airSegment.segInfo.flData.offpoint) +
                        ' (' + airSegment.segInfo.flData.offpoint + ')';
                    var handbaggageinfo = [];
                    $.each(airSegment.handBagInformations, function (bagIndex, baggage) {
                        var handpaxType = baggage.bagPaxType;
                        var paxUnithandQuantity = '';
                        var baggageTooltip = '';
                        if (handpaxType.toUpperCase() == 'ADT') {
                            handpaxType = 'Adult';
                        } else if (handpaxType.toUpperCase() == 'CHD') {
                            handpaxType = 'Child';
                        } else {
                            handpaxType = 'Infant';
                        }

                        var paxUnit = baggage.unit;
                        paxUnit = (paxUnit.toLowerCase() == 'pc(s)' || paxUnit.toLowerCase() == 'piece' || paxUnit.toLowerCase() == 'pieces' || paxUnit.toLowerCase() == 'n' || paxUnit.toLowerCase() == 'p' || paxUnit.toLowerCase() == 'pc') ? 'Piece' : 'Kg';
                        if (self.SupplierId == "69") {
                            paxUnit = baggage.unit;
                        }
                        paxUnithandQuantity = baggage.baggage + ' ' + paxUnit;
                        var undef = paxUnithandQuantity.includes("undefined" || "UNDEFINED");
                        if (undef == true) {
                            self.handbaggageShow = false
                            $('#hndbag').hide();
                        } else
                        if ((baggage.unit).toLowerCase() == "sb") {
                            baggageTooltip = 'Airline typically permit Standard baggage allowance.';
                            paxUnithandQuantity = 'SB';
                        } else {
                            baggageTooltip = (paxUnit.toLowerCase() == 'pc(s)' || paxUnit.toLowerCase() == 'piece' || paxUnit.toLowerCase() == 'pieces') ?
                                "Airline typically permit 7 kg baggage weight per piece." :
                                '';
                        }
                        var baggage = {
                            paxType: handpaxType,
                            paxUnithandQuantity: paxUnithandQuantity,
                            tooltip: baggage.desc ? baggage.desc : baggageTooltip
                        }
                        var handbagg = handbaggageinfo.filter(function (bag) {
                            return bag.handpaxType == baggage.paxType && bag.paxUnithandQuantity == baggage.paxUnithandQuantity
                        });
                        if (handbagg.length == 0) {
                            handbaggageinfo.push(baggage);
                        }
                    });
                    var airLinehandBaggage = {
                        bagInfo: handbaggageinfo,
                        fromTo: fromToSeg
                    }
                    arlinehandBagges.push(airLinehandBaggage);
                });
                this.AirLinehandBaggages = arlinehandBagges;
                self.handBaggageExist = _.some(self.AirLinehandBaggages, function (item) {
                    return item.bagInfo.length;
                });
                // this.handBaggageDet=this.AirLinehandBaggages[0].bagInfo;
                //  this.AirLinehandBaggages.length > 0 ? self.handbaggageShow = true : self.handbaggageShow = false;
                //  self.No_Seg_baggAvail = self.No_baggage_available_label;
                self.handBaggageExist ? truecon() : elsecon();

                function truecon() {
                    self.handbaggageShow = true
                    // $('#hide').hide();
                }

                function elsecon() {
                    self.handbaggageShow = false
                    $('#hndbag').hide();
                    //  self.No_Seg_baggAvail = self.No_baggage_available_label;

                }


                console.log(this.AirLinehandBaggages);
                // hand baggage ends here

                // var miniRuleLists = [];
                // $.each(selectResponseObj.content.fareInformationWithoutPnrReply.minirules, function (index, rule) {
                //     var miniRules = [];
                //     rule.codes[0].category.forEach(cat => {
                //         if (cat.cat == "31" || cat.cat == "33") {
                //             var before33Restriappinfo = { action: 0 };
                //             var before33Moninfo = { amount: 0, currency: '' };
                //             var before31Restriappinfo = { action: 0 };
                //             var before31Moninfo = { amount: 0, currency: '' };

                //             var after33Restriappinfo = { action: 0 };
                //             var after33Moninfo = { amount: 0, currency: '' };
                //             var after31Restriappinfo = { action: 0 };
                //             var after31Moninfo = { amount: 0, currency: '' };

                //             if (cat.cat == "31") {
                //                 try { before33Restriappinfo = cat.restriAppInfo.find(x => x.indicator == 'BDA'); } catch (err) { }
                //                 try { before33Moninfo = cat.monInfo.find(x => x.typeQualifier == 'BDX'); } catch (err) { }
                //                 try { after33Restriappinfo = cat.restriAppInfo.find(x => x.indicator == 'BDA'); } catch (err) { }
                //                 try { after33Moninfo = cat.monInfo.find(x => x.typeQualifier == 'BDX'); } catch (err) { }

                //                 miniRules.push({
                //                     type: 'Cancellation',
                //                     before: ((before33Restriappinfo.action == 1) ? '' : 'Not Allowed'),
                //                     after: ((before31Restriappinfo.action == 1) ? '' : 'Not Allowed'),
                //                     beforeAmount: before33Moninfo.amount,
                //                     afterAmount: before31Moninfo.amount
                //                 });
                //             }
                //             if (cat.cat == "33") {
                //                 try { before31Restriappinfo = cat.restriAppInfo.find(x => x.indicator == 'ADA'); } catch (err) { }
                //                 try { before31Moninfo = cat.monInfo.find(x => x.typeQualifier == 'ADX'); } catch (err) { }
                //                 try { after31Restriappinfo = cat.restriAppInfo.find(x => x.indicator == 'ADA'); } catch (err) { }
                //                 try { after31Moninfo = cat.monInfo.find(x => x.typeQualifier == 'ADX'); } catch (err) { }

                //                 miniRules.push({
                //                     type: 'Changes',
                //                     before: ((after33Restriappinfo.action == 1) ? '' : 'Not Allowed'),
                //                     after: ((after31Restriappinfo.action == 1) ? '' : 'Not Allowed'),
                //                     beforeAmount: after33Moninfo.amount,
                //                     afterAmount: after31Moninfo.amount
                //                 });
                //             }
                //         }
                //     });

                //     var miniRuleList = {
                //         "from": rule.departure,
                //         "to": rule.arrival,
                //         "fromAirport": getAirport(rule.departure),
                //         "toAirport": getAirport(rule.arrival),
                //         "miniRules": miniRules
                //     };

                //     miniRuleLists.push(miniRuleList);
                // });
                // selectResponseObj.content.fareInformationWithoutPnrReply.miniRuleList = miniRuleLists;
                // if (selectResponseObj.content.fareInformationWithoutPnrReply.minirules != null &&
                //     selectResponseObj.content.fareInformationWithoutPnrReply.minirules.length > 0) {
                //     self.miniRuleData = selectResponseObj.content.fareInformationWithoutPnrReply.minirules;
                //     this.IsMini_Rule_Available = true;
                // }
                if (selectResponseObj.content.fareInformationWithoutPnrReply.minirules != null &&
                    selectResponseObj.content.fareInformationWithoutPnrReply.minirules.length > 0) {
                    Summary = selectResponseObj.content.fareInformationWithoutPnrReply.minirules;
                    this.isSummaryAvailable = true;
                    this.IsMini_Rule_Available = true;
                    var SummaryLists = [];
                    Summary.forEach(rule => {
                        SummaryList = _.groupBy(rule.category, "cat");
                        SummaryLists.push(SummaryList);
                    });
                    self.SummaryList = SummaryLists;
                }

                if (selectResponseObj.content.fareInformationWithoutPnrReply.airFareRule != null &&
                    selectResponseObj.content.fareInformationWithoutPnrReply.airFareRule.length > 0) {
                    self.fareRuleData = selectResponseObj.content.fareInformationWithoutPnrReply.airFareRule;
                    this.IsFare_Rule_Available = true;
                }
                selectResponseObj.totalPaxUnites = totalPaxUnites;
                var airLegs = [];
                $.each(selectResponseObj.content.fareInformationWithoutPnrReply.airSegments, function (index, airSegment) {
                    var items = $.grep(selectResponseObj.content.fareInformationWithoutPnrReply.airSegments, function (key) {
                        return (key.segDirectionID == airSegment.segDirectionID);
                    });
                    if (items.length > 1) {
                        airSegment.stopCount = items.length;
                        airSegment.stopCountText = items.length.toString() + ' ';
                    } else {
                        airSegment.stopCount = 0;
                        airSegment.stopCountText = 'Non-';
                    }
                    airSegment.origin = getAirport(airSegment.originCity);
                    airSegment.departure = getAirport(airSegment.departureCity);
                    airSegment.boardAirport = getAirport(airSegment.segInfo.flData.boardPoint);
                    airSegment.offAirport = getAirport(airSegment.segInfo.flData.offpoint);
                    var airLegItemIndex = airLegs.findIndex(function (airLegItem) {
                        return airLegItem.legId == airSegment.segDirectionID;
                    });
                    if (airLegItemIndex > -1) {
                        airLegs[airLegItemIndex].departure = getAirport(airSegment.departureCity);
                    } else {
                        var legClass = airSegment.segDirectionID == 1 && selectResponseObj.content.fareInformationWithoutPnrReply.tripType == 'r' ? 'summary-departure summary-arrival' : 'summary-departure';
                        airLegs.push({
                            legClass: legClass,
                            legId: airSegment.segDirectionID,
                            origin: getAirport(airSegment.originCity),
                            departure: getAirport(airSegment.departureCity),
                            segInfo: airSegment.segInfo
                        });
                    }
                });
                selectResponseObj.content.fareInformationWithoutPnrReply.airLegs = airLegs;
                this.selectedFlight = selectResponseObj;
                this.TotalPaxUnites = totalPaxUnites;
                this.PaxFareDet = selectResponseObj.content.fareInformationWithoutPnrReply.paxFareDetails;
                this.flightFareDetails = selectResponseObj.content.fareInformationWithoutPnrReply.flightFareDetails;
                this.TripTypeText = selectResponseObj.content.fareInformationWithoutPnrReply.tripTypeText;
                // this.TripTypeText = selectResponseObj.content.fareInformationWithoutPnrReply.tripTypeText == 'Oneway' ? self.Oneway : self.Round_Trip;
                self.showAnimateDiv = false;
                self.Bagg_Label = self.Baggage;
                setTimeout(() => {
                    $('[data-toggle="tooltip"]').tooltip();
                }, 100);
            } catch (err) {
                alertify.alert(self.Alert_Label, self.Message_UnexpectSupplierIssue).setting({
                    'closable': false,
                    onclose: function () {
                        window.location = '/search.html';
                    }
                }).set('label', self.Ok);
            }
        },
        getTypePax: function (paxType) {
            var paxTypeText = 'Adult';
            switch (paxType) {
                case 'ADT':
                    // paxTypeText = 'Adult';
                    paxTypeText = this.Adult;
                    break;
                case 'CHD':
                    // paxTypeText = 'Child';
                    paxTypeText = this.Child;
                    break;
                case 'INF':
                    // paxTypeText = 'Infant';
                    paxTypeText = this.Infant;
                    break;
            }
            return paxTypeText;
        },
        getUserInfo: function () {
            this.Agency_Info = JSON.parse(atob(window.localStorage.getItem("agencyNode")));
            this.Agency_Username = this.Agency_Info.firstName + ' ' + this.Agency_Info.lastName;
        },
        getMaxDate: function (dateObject) {
            var dateObjectsplit = $(dateObject).attr('tag').split('_');
            var endDate = new Date();
            switch (dateObjectsplit[0]) {
                case 'dob':
                    adddays = 0;
                    try {
                        endDate = new Date(this.selectFlightFromSession.selectedFlightInfo.groupOfFlights[0].flightDetails[0].fInfo.dateTime.dateTimeFormatedDepDate1);
                    } catch (error) {
                        endDate = new Date();
                    }
                    endDate.setDate(endDate.getDate() + adddays);
                    switch (dateObjectsplit[1]) {
                        case 'ADT':
                            addyears = -12;
                            endDate.setFullYear(endDate.getFullYear() + addyears);
                            break;
                        case 'CHD':
                            // addyears = -2;
                            // endDate.setFullYear(endDate.getFullYear() + addyears);
                            endDate = new Date();
                            break;
                        case 'INF':
                            // endDate.setFullYear(endDate.getFullYear() + addyears);
                            endDate = new Date();
                            break;
                    }
                    $(dateObject).datepicker('option', 'maxDate', new Date(endDate));
                    break;
                case 'exd':
                    break;
                case 'isd':
                    adddays = -1;
                    endDate.setDate(endDate.getDate() + adddays);
                    $(dateObject).datepicker('option', 'maxDate', endDate);
                    break;
            }
            return endDate;
        },
        getMinDate: function (dateObject) {
            var dateObjectsplit = $(dateObject).attr('tag').split('_');
            var txtDob = $('#txtDob' + dateObjectsplit[1] + dateObjectsplit[2]);
            var txtExpireDate = $('#txtExpireDate' + dateObjectsplit[1] + dateObjectsplit[2]);
            var txtPassportIssue = $('#txtPassportIssue' + dateObjectsplit[1] + dateObjectsplit[2]);
            var startDate = new Date();
            switch (dateObjectsplit[0]) {
                case 'dob':
                    try {
                        var gLength = this.selectFlightFromSession.selectedFlightInfo.groupOfFlights.length;
                        var fLength = this.selectFlightFromSession.selectedFlightInfo.groupOfFlights[gLength - 1].flightDetails.length - 1;
                        startDate = new Date(this.selectFlightFromSession.selectedFlightInfo.groupOfFlights[gLength - 1].flightDetails[fLength].fInfo.dateTime.dateTimeFormatedArrDate);
                    } catch (error) {
                        startDate = new Date();
                    }
                    switch (dateObjectsplit[1]) {
                        case 'ADT':
                            addyears = -120;
                            break;
                        case 'CHD':
                            addyears = -12;
                            break;
                        case 'INF':
                            addyears = -2;
                            break;
                    }
                    startDate.setFullYear(startDate.getFullYear() + addyears);
                    break;
                case 'exd':
                    switch (dateObjectsplit[1]) {
                        case 'ADT':
                            adddays = 1;
                            startDate.setDate(startDate.getDate() + adddays);
                            break;
                        case 'CHD':
                            adddays = 1;
                            startDate.setDate(startDate.getDate() + adddays);
                            break;
                        case 'INF':
                            adddays = 1;
                            startDate.setDate(startDate.getDate() + adddays);
                            break;
                    }
                    break;
                case 'isd':
                    switch (dateObjectsplit[1]) {
                        case 'ADT':
                            if (isNullorUndefined($(txtDob).val())) {
                                addyears = -100;
                                startDate.setFullYear(startDate.getFullYear() + addyears);
                            } else {
                                startDate = moment($(txtDob).val(), 'DD/MM/YYYY')._d;
                            }
                            break;
                        case 'CHD':
                            if (isNullorUndefined($(txtDob).val())) {
                                addyears = -12;
                                startDate.setFullYear(startDate.getFullYear() + addyears);
                            } else {
                                startDate = moment($(txtDob).val(), 'DD/MM/YYYY')._d;
                            }
                            break;
                        case 'INF':
                            if (isNullorUndefined($(txtDob).val())) {
                                addyears = -2;
                                startDate.setFullYear(startDate.getFullYear() + addyears);
                            } else {
                                startDate = moment($(txtDob).val(), 'DD/MM/YYYY')._d;
                            }
                            break;
                    }
                    break;
            }
            $(dateObject).datepicker('option', 'minDate', startDate);
            if (dateObjectsplit[0] == 'dob') {
                $(dateObject).datepicker('option', 'yearRange', "-120:+0");
            }
            if (dateObjectsplit[0] == 'isd') {
                $(dateObject).datepicker('option', 'yearRange', "-70:+0");
            }
            return startDate;
        },
        getTermsnConditions: function () {

            try {
                var vm = this;
                var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
                var serviceUrl = vm.commonStore.hubUrls.hubConnection.hubServices.contact;
                axios.get(hubUrl + serviceUrl, {
                        headers: {
                            'Authorization': "Bearer " + localStorage.accessToken
                        }
                    })
                    .then(function (response) {
                        bindPrivacyPolicyNTerms(response.data.siteList);
                    }).catch(function (error) {
                        console.log('Error: Terms n Conditions');
                    });
            } catch (err) {
                console.log('Error: Terms n Conditions');
            }
        },
        checkPriceChange: function (fareOnSelect) {
            var fareOnSearch = '';
            var self = this;

            var multiplier = "";
            var currency = "";
            if (this.commonStore.selectedCurrency == '') {
                multiplier = self.selectFlightFromSession.selectedFlightInfo.selectCredential.officeIdList[0].multiplier;
                currency = self.selectFlightFromSession.selectedFlightInfo.selectCredential.officeIdList[0].currency;
            } else {
                multiplier = self.commonStore.currencyMultiplier;
                currency = self.commonStore.selectedCurrency;
            }

            selectFlightFromSessionObj.selectedFlightInfo.fare.amount
            if (selectFlightFromSessionObj.selectedFlightInfo.category != null) {
                if (isNullorUndefined(selectFlightFromSessionObj.selectedFlightInfo.CategoryId)) {
                    fareOnSearch = selectFlightFromSessionObj.selectedFlightInfo.fare.amount;
                } else {
                    fareOnSearch = selectFlightFromSessionObj.selectedFlightInfo.category[selectFlightFromSessionObj.selectedFlightInfo.CategoryId].amount;
                }

            } else {
                fareOnSearch = selectFlightFromSessionObj.selectedFlightInfo.fare.amount;
            }
            var fareCurrency = selectFlightFromSessionObj.selectedFlightInfo.fare.currency;
            if (parseFloat(parseFloat(fareOnSelect).toFixed(2)) > parseFloat(parseFloat(fareOnSearch).toFixed(2))) {
                // alertify.confirm(self.Alert_Label, self.Fare_Increased_alert_Msg + " " + fareCurrency + " " + roundAmount(fareOnSearch, fareCurrency) + ' ' + self.to + ' ' + fareCurrency + " " + roundAmount(fareOnSelect, fareCurrency),
                alertify.confirm(self.Alert_Label, self.Fare_Increased_alert_Msg + " " +
                        this.$n(fareOnSearch / multiplier, 'currency', currency) +
                        ' ' + self.to +
                        ' ' + this.$n(fareOnSelect / multiplier, 'currency', currency),
                        function () {},
                        function () {
                            window.history.back();
                        })
                    .set({
                        'closable': false,
                        'labels': {
                            ok: self.Ok,
                            cancel: self.Cancel_Btn_Label
                        }
                    });
            } else if (parseFloat(parseFloat(fareOnSelect).toFixed(2)) < parseFloat(parseFloat(fareOnSearch).toFixed(2))) {
                alertify.confirm(self.Alert_Label, self.Fare_Drop_alert_Msg + " " +
                        this.$n(fareOnSearch / multiplier, 'currency', currency) +
                        ' ' + self.to +
                        ' ' + this.$n(fareOnSelect / multiplier, 'currency', currency),
                        function () {},
                        function () {
                            window.history.back();
                        })
                    .set({
                        'closable': false,
                        'labels': {
                            ok: self.Ok,
                            cancel: self.Cancel_Btn_Label
                        }
                    });;
            }
        },
        bookTimeout: function (bookStatusType) {
            var self = this;

            if (self.ancilleryLoadingBaggage || self.ancilleryLoadingSeat || self.ancilleryLoadingMeal) {
                setTimeout(function () {
                    self.bookTimeout(bookStatusType)
                }, 500);
            } else {
                self.bookFlight(bookStatusType);
            }
        },
        bookFlight: function (bookStatusType) {
            var vm = this;
            var self = this;
            self.bookIsClicked = true;

            if (self.ancilleryLoadingBaggage || self.ancilleryLoadingSeat || self.ancilleryLoadingMeal) {
                setTimeout(function () {
                    self.bookTimeout(bookStatusType)
                }, 500);
            } else {
                self.bookIsClicked = false;
                if (vm.commonStore.commonRoles.hasAirBook) {
                    isDuplicateName = false;
                    DuplicateNames = [];
                    var isValidData = validateEntries();
                    setTimeout(function () {
                        if (isDuplicateName) {
                            alertify.alert(vm.Warning_Label, vm.Duplicated_Names_alert_Msg).set('closable', false).set('label', vm.Ok);
                        } else if (isValidData) {
                             if ((self.responseHasZeroValueMeal || self.responseHasZeroValueBaggage || self.responseHasZeroValueSeat) && (vm.SupplierId == "44" || vm.SupplierId == "49" || vm.SupplierId == "50" || vm.SupplierId == "71" || vm.SupplierId == "86")) {
                                 alertify.alert("Note", "Please select your free ancillary services before booking.", function () {
                                     $("#paid_services").modal({
                                         backdrop: 'static',
                                         keyboard: false,
                                         show: true
                                     })
                                 }).set('closable', false).set('label', vm.Ok);
                             } else {
                                if (!vm.showAncillaryFarePricediv && (vm.SupplierId == "44" || vm.SupplierId == "49" || vm.SupplierId == "50" || vm.SupplierId == "54" || vm.SupplierId == "71" || vm.SupplierId == "69" || vm.SupplierId == "86")) {
                                    alertify.confirm("Note", "Do you want to add additional services before booking?", function () {
                                        if (self.isLcc && bookStatusType == 0) {
                                            setTimeout(function () {
                                                alertify.confirm(self.Ins_Purch_Alert_Heading, self.Ins_Purch_Alert_Content, function () {
                                                    vm.bookFlightFun(bookStatusType);
                                                }, function () {
                                                    window.history.back();
                                                    //this.showLoader=true;
                                                }).set({
                                                    'closable': false,
                                                    'labels': {
                                                        ok: self.Continue_Btn_Label,
                                                        cancel: self.Cancel_Btn_Label
                                                    }
                                                });
                                            }, 200);
                                        } else {
                                            vm.bookFlightFun(bookStatusType);
                                        }
                                    }, function () {
                                        $("#paid_services").modal({
                                            backdrop: 'static',
                                            keyboard: false,
                                            show: true
                                        })
                                    }).set({
                                        'closable': false,
                                        'labels': {
                                            ok: "No",
                                            cancel: "Yes"
                                        }
                                    });
                                } else {
                                    if (self.isLcc && bookStatusType == 0) {
                                        alertify.confirm(self.Ins_Purch_Alert_Heading, self.Ins_Purch_Alert_Content, function () {
                                            vm.bookFlightFun(bookStatusType);
                                        }, function () {
                                            window.history.back();
                                            //this.showLoader=true;
                                        }).set({
                                            'closable': false,
                                            'labels': {
                                                ok: self.Continue_Btn_Label,
                                                cancel: self.Cancel_Btn_Label
                                            }
                                        });

                                    } else {
                                        vm.bookFlightFun(bookStatusType);
                                    }
                                }
                             }
                        } else {
                            //alertify.alert(flightComponent.Alert_Label, flightComponent.Fields_Mandatory_alert_Msg).set('closable', false).set('label', flightComponent.Ok);
                        }
                    }, 100);
                } else {
                    alertify.alert(vm.Warning_Label, vm.No_Booking_privillage_alert_Msg).set('closable', false).set('label', vm.Ok);
                }
            }

        },
        createBooking: function(bookStatusType) {
            $('#BookingLoader').show();
            var vm = this;
            var bookRq = vm.generatingAirBookingRequest(bookStatusType);
            console.log(JSON.stringify(bookRq));
            var paymentDetails = {
                "totalAmount": -188.88,
                "customerEmail": "test@test.com",
                "bookingReference": "testRef",
                "cartID": ""
            };
            var returnURL = window.location.origin + "/Flights/bookinginfo.html";
            var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = vm.commonStore.hubUrls.hubConnection.hubServices.flights.airBooking;


            var config = {
                axiosConfig: {
                    method: "POST",
                    url: hubUrl + serviceUrl,
                    data: bookRq
                },
                successCallback: function (response) {
                    response = response.data;
                    console.log(response);
                    if (response.response.content.bookFlightResult.success &&
                        response.response.content.bookFlightResult.status != undefined) {
                        try {
                            var proceedBooking = false;
                            var status = response.response.content.bookFlightResult.status;
                            var fsupplier = selectResponseObj.content.fareInformationWithoutPnrReply.fSupplier;
                            if ((fsupplier == '44' || fsupplier == '49' || fsupplier == '50' || fsupplier == '71') && status != 'RF') {
                                proceedBooking = true;
                            } else if ((fsupplier == '69' || fsupplier == '54' || fsupplier == '18') && (status == 'HK' || status == 'OK')) {
                                proceedBooking = true;
                            } else if (status == 'HK' || status == 'NF' || status == 'OK') {
                                proceedBooking = true;
                            }
                            if (proceedBooking) {
                                var bookData = {
                                    BkngRefID: response.response.content.bookFlightResult.bookingRefId,
                                    emailId: '',
                                    redirectFrom: 'booking',
                                    isMailsend: false
                                };
                                window.sessionStorage.setItem("flightbookingId", bookData.BkngRefID);
                                window.sessionStorage.setItem("sendbookmail", bookData.isMailsend);
                                window.sessionStorage.setItem("selectCredential", JSON.stringify(response.response.selectCredential));
                                if ($('#radio-btn-03').prop('checked')) {
                                    var status = '101';
                                    switch (response.response.content.bookFlightResult.status) {
                                        case 'HK':
                                        case 'OK':
                                            status = '101';
                                            break;
                                        case 'NF':
                                            status = '102';
                                            break;
                                        default:
                                            status = '100';
                                            break;
                                    }
                                    localStorage.bookData = JSON.stringify(bookData);
                                    localStorage.accessToken = response.response.token;
                                    console.log(JSON.stringify(response));
                                    if (status == '101') {
                                        paymentDetails.bookingReference = bookData.BkngRefID;
                                        paymentDetails.totalAmount = totalFare;
                                        paymentDetails.currency = vm.commonStore.agencyNode.loginNode.currency || 'AED';
                                        paymentDetails.currentPayGateways = vm.commonStore.agencyNode.loginNode.paymentGateways;
                                        paymentDetails.customerEmail = JSON.parse(atob(window.localStorage.getItem("agencyNode"))).emailId;

                                        //Un COmment for merchant start
                                        // if (currentPayGateways != undefined) {
                                        //     if (currentPayGateways.length > 0) {
                                        //         if (currentPayGateways[0].id == 9) {
                                        //             //Merch Integration
                                        //             localStorage.bkRefNo = bookData.BkngRefID;
                                        //             localStorage.cartEmailId = $('#emailtxt').val();
                                        //             window.location.href = '/pay.html';
                                        //         } else {
                                        //Redirection
                                        var status = paymentManager(paymentDetails, returnURL);
                                        //         }
                                        //     } else {
                                        //         alertify.alert(flightComponent.Alert_Label, flightComponent.Payment_Gateway_Not_Available_alert).setting({ 'closable': false, onclose: function () { window.location = '/Flights/flight-confirmation.html?status=102'; } }).set('label', flightComponent.Ok);
                                        //     }
                                        // }
                                        // else {
                                        //     alertify.alert(flightComponent.Alert_Label, flightComponent.Payment_Gateway_Not_Available_alert).setting({ 'closable': false, onclose: function () { window.location = '/Flights/flight-confirmation.html?status=102'; } }).set('label', flightComponent.Ok);
                                        // }
                                        //Un COmment for merchant End

                                    } else if (status == '102') {
                                        $('#BookingLoader').hide();
                                        alertify.alert(vm.Alert_Label, "Booking successfully created, pricing failed, please reprice PNR " + bookData.BkngRefID + " and issue ticket.").setting({
                                            'closable': false,
                                            onclose: function () {
                                                window.location = '/Flights/bookinginfo.html';
                                            }
                                        }).set('label', vm.Ok);
                                    } else {
                                        $('#BookingLoader').hide();
                                        //showHideConfirmLoader(false);
                                        alertify.alert(vm.Alert_Label, vm.Message_UnexpectSupplierIssue).setting({
                                            'closable': false,
                                            onclose: function () {
                                                window.location = '/search.html';
                                            }
                                        }).set('label', vm.Ok);
                                    }
                                } else {
                                    $('#BookingLoader').hide();
                                    if (status == 'NF') {
                                        alertify.alert(vm.Alert_Label, "Booking successfully created, pricing failed, please reprice PNR " + bookData.BkngRefID).setting({
                                            'closable': false,
                                            onclose: function () {
                                                window.location = '/Flights/bookinginfo.html';
                                            }
                                        }).set('label', vm.Ok);
                                    } else {
                                        alertify.alert(vm.Alert_Label, vm.Booking_success_alert_Msg + " " + bookData.BkngRefID).setting({
                                            'closable': false,
                                            onclose: function () {
                                                window.location = '/Flights/bookinginfo.html';
                                            }
                                        }).set('label', vm.Ok);
                                    }
                                    // window.location.href = 'bookinginfo.html';
                                }
                            } else {
                                $('#BookingLoader').hide();
                                //showHideConfirmLoader(false);
                                if (response.response.content.bookFlightResult.status == 'UN') {
                                    alertify.alert(vm.Alert_Label, "Booking was rejected by the airline.").setting({
                                        'closable': false,
                                        onclose: function () {
                                            window.location = '/search.html';
                                        }
                                    }).set('label', vm.Ok);
                                } else {
                                    alertify.alert(vm.Alert_Label, vm.Message_UnexpectSupplierIssue).setting({
                                        'closable': false,
                                        onclose: function () {
                                            window.location = '/search.html';
                                        }
                                    }).set('label', vm.Ok);
                                }
                            }
                        } catch (err) {
                            $('#BookingLoader').hide();
                            //showHideConfirmLoader(false);
                            alertify.alert(vm.Alert_Label, vm.Booking_failed_alert_Msg).setting({
                                'closable': false,
                                onclose: function () {
                                    window.location = '/search.html';
                                }
                            }).set('label', vm.Ok);
                        }
                    } else {
                        $('#BookingLoader').hide();
                        alertify.alert(vm.Alert_Label, vm.Booking_failed_alert_Msg).setting({
                            'closable': false,
                            onclose: function () {
                                window.location = '/search.html';
                            }
                        }).set('label', vm.Ok);
                    }
                },
                errorCallback: function (error) {
                    $('#BookingLoader').hide();
                },
                showAlert: true
            }

            mainAxiosRequest(config)
            
        },
        issueTicketContactBook: function(bookStatusType) {
            if ($('#txtLeadPaxPhone4').val() == "" && $('#txtLeadPaxPhone3').val() == "") {
                this.contactError = true;
            } else{
                $("#ticket_issue_popup").modal('hide');
                this.contactError = false;
                this.createBooking(bookStatusType);
            }
        },
        bookFlightFun: function (bookStatusType) {
            this.contactError = false;
            if (this.commonStore.commonRoles.hasAirIssueContactNumber && this.issueFlag && bookStatusType == 0) {
                $("#ticket_issue_popup").modal('show');
            } else {
                $("#ticket_issue_popup").modal('hide');
                this.createBooking(bookStatusType);
            }
        },
        generatingAirBookingRequest: function (bookStatusType) {
            var vm = this;
            var specialReqDetails = null;
            var airSupplierResponse = selectResponseObj.content;
            var dateFormatFrom = 'DD/MM/YYYY';
            var paxFares = airSupplierResponse.fareInformationWithoutPnrReply.paxFareDetails;
            var airResposneLegs = airSupplierResponse.fareInformationWithoutPnrReply.airSegments;
            var airSectoridies = _.pluck(airResposneLegs, 'segDirectionID');
            airSectoridies = _.uniq(airSectoridies);
            var fsupplier = selectResponseObj.content.fareInformationWithoutPnrReply.fSupplier;
            //var flagCountryCode = $("#txtPhoneNumber").intlTelInput("getSelectedCountryData").iso2;
            var flagCountryCode = '91'; //$('#phoneNumWithFlag .selected-flag>.iti-flag').attr('class').split(' ')[1];
            var telcode = flagCountryCode.toUpperCase();
            var phno = $('#txtPhoneNumber').val();
            var supplierSpecific = null;
            var flightid = "";
            if (fsupplier == "54" || fsupplier == "64" || fsupplier == "69" || fsupplier == "72" || fsupplier == "57" || fsupplier == "1" || fsupplier == "73" || fsupplier == "45" || fsupplier == "58" || fsupplier == "83" || fsupplier == "62" || fsupplier == "60" || fsupplier == "74" || fsupplier == "55" || fsupplier == "86" || fsupplier == "87") {

                if (localStorage.supplierSpecific) {
                    var tempsupplierSpecific = JSON.parse(localStorage.supplierSpecific);
                    supplierSpecific = tempsupplierSpecific.supplierSpecific;
                }
                if (fsupplier == "54" || fsupplier == "83" || fsupplier == "45") {
                    supplierSpecific.hold = bookStatusType;
                }
            } else if (fsupplier == "44" || fsupplier == "49" || fsupplier == "50" || fsupplier == "71") {
                supplierSpecific = {};
                var tempsupplierSpecific = JSON.parse(localStorage.supplierSpecific);
                if (Array.isArray(tempsupplierSpecific.supplierSpecific)) {
                    supplierSpecific.category = tempsupplierSpecific.supplierSpecific;
                } else {
                    supplierSpecific = tempsupplierSpecific.supplierSpecific;
                }
                supplierSpecific.hold = bookStatusType;
            }
            if (fsupplier == "18") {
                flightid = window.localStorage.getItem("flightID");
            }

            var passengerTypeQuantity = airSupplierResponse.fareInformationWithoutPnrReply.passengerTypeQuantity;
            var adult = passengerTypeQuantity.adt;
            var child = passengerTypeQuantity.chd;
            var infant = passengerTypeQuantity.inf;
            var totalPassengers = parseInt(adult) + parseInt(child) + parseInt(infant);
            var adultIndex = 1;
            var childIndex = 1;
            var infantIndex = 1;
            var currentIndex = 1;
            var isValidData = true;

            var airLineLegs = [];
            for (var i = 0; i < airSectoridies.length; i++) {
                var segments = airResposneLegs.filter(function (segment) {
                    return segment.segDirectionID == airSectoridies[i]
                });
                var airLineSegments = [];
                var flightDetails = '';
                try {
                    flightDetails = selectFlightFromSessionObj.selectedFlightInfo.groupOfFlights[i].flightDetails;
                } catch (err) {}
                $.each(segments, function (segIndex, segment) {
                    var terminalFrom = '';
                    var terminalTo = '';
                    try {
                        terminalFrom = isNullorUndefined(segment.segInfo.flData.boardTerminal) ? flightDetails[segIndex].fInfo.location.fromTerminal : segment.segInfo.flData.boardTerminal;
                    } catch (err) {}
                    try {
                        terminalTo = isNullorUndefined(segment.segInfo.flData.offTerminal) ? flightDetails[segIndex].fInfo.location.toTerminal : segment.segInfo.flData.offTerminal;
                    } catch (err) {}
                    var airLineSegment = {
                        fareBasis: stringIsNullorEmpty(segment.fareId) ? null : segment.fareId,
                        departureDate: moment(segment.segInfo.dateandTimeDetails.departureDate, 'DDMMYY').format('DD-MM-YYYY'),
                        departureTime: segment.segInfo.dateandTimeDetails.departureTime,
                        arrivalDate: moment(segment.segInfo.dateandTimeDetails.arrivalDate, 'DDMMYYYY').format('DD-MM-YYYY'),
                        arrivalTime: segment.segInfo.dateandTimeDetails.arrivalTime,
                        departureFrom: segment.segInfo.flData.boardPoint,
                        departureTo: segment.segInfo.flData.offpoint,
                        marketingCompany: segment.segInfo.flData.marketingCompany,
                        operatingCompany: segment.segInfo.flData.operatingCompany,
                        validatingCompany: segment.segInfo.flData.validatingCompany,
                        flightNumber: segment.segInfo.flData.flightNumber,
                        bookingClass: !stringIsNullorEmpty(segment.segInfo.flData.bookingClass) ? segment.segInfo.flData.bookingClass : segment.segInfo.flData.rbd,
                        rbd: !stringIsNullorEmpty(segment.segInfo.flData.rbd) ? segment.segInfo.flData.rbd : segment.segInfo.flData.bookingClass,
                        terminalTo: terminalTo,
                        terminalFrom: terminalFrom,
                        flightEquip: flightDetails[segIndex].fInfo.eqpType,
                        // flightEquip: segment.segInfo.flData.equiptype, 
                        paxRef: !stringIsNullorEmpty(segment.segInfo.flData.paxRef) ? segment.segInfo.flData.paxRef : null,
                        stopDetails: segment.stopDetails ? segment.stopDetails : undefined,
                        stopQuantity: segment.stopQuantity ? segment.stopQuantity : undefined
                    }
                    airLineSegments.push(airLineSegment);
                });

                var airLineLeg = {
                    elapsedTime: null,
                    from: segments[0].originCity,
                    to: segments[segments.length - 1].departureCity,
                    segments: airLineSegments
                }
                airLineLegs.push(airLineLeg);
            }

            var travellers = [];
            adultIndex = 1;
            childIndex = 1;
            infantIndex = 1;
            currentIndex = 1;
            for (var i = 1; i <= totalPassengers; i++) {
                var paxType = (i <= parseInt(adult) ? 'Adult' : (i > parseInt(adult) && (i <= parseInt(adult) + parseInt(child)) ? 'Child' : 'Infant'));
                var paxTypeCode = (i <= parseInt(adult) ? 'ADT' : (i > parseInt(adult) && (i <= parseInt(adult) + parseInt(child)) ? 'CHD' : 'INF'));
                switch (paxTypeCode) {
                    case 'ADT':
                        currentIndex = adultIndex;
                        adultIndex++;
                        break;
                    case 'CHD':
                        currentIndex = childIndex;
                        childIndex++;
                        break;
                    case 'INF':
                        currentIndex = infantIndex;
                        infantIndex++;
                        break;
                }
                var Extraservices = null;
                if (fsupplier == "44" || fsupplier == "49" || fsupplier == "50" || fsupplier == "54" || fsupplier == "71" || fsupplier == "69" || fsupplier == "86") {
                    var Extraservicesre = vm.getExtasfromAncillery(i, paxTypeCode, "booking");
                    if (Extraservicesre.length > 0) {
                        Extraservices = Extraservicesre;
                    }
                }
                var paxTypeIndex = paxTypeCode + currentIndex;
                var frequentFlayerNumber = $('#txtFreqFlyNumber' + paxTypeIndex).val();
                var frequentFlayerAirlineCode = $('#ddlFreqFlyAirLine' + paxTypeIndex).val();
                var paxTitle = $('#ddltitle' + paxTypeIndex).val();
                var paxGender = (paxTitle == 'Mr' || paxTitle == 'Mstr') ? 'Male' : 'Female';

                var paxDob = null;
                if ($('#txtDob' + paxTypeIndex).val() != '') {
                    paxDob = moment($('#txtDob' + paxTypeIndex).val(), dateFormatFrom).format('DD-MM-YYYY');
                }
                var passIsuDate = null;
                // if ($('#txtPassportIssue' + paxTypeIndex).val() != '') {
                //     passIsuDate = moment($('#txtPassportIssue' + paxTypeIndex).val(), dateFormatFrom).format('DD-MM-YYYY');
                // }
                var passExpDate = null;
                if ($('#txtExpireDate' + paxTypeIndex).val() != '') {
                    passExpDate = moment($('#txtExpireDate' + paxTypeIndex).val(), dateFormatFrom).format('DD-MM-YYYY');
                }
                var docIssueCountry = null;
                try {
                    if (!isNullorEmpty($('#ddlPassportIssue' + paxTypeIndex).val()) && $('#ddlPassportIssue' + paxTypeIndex).val() != 0) {
                        docIssueCountry = $('#ddlPassportIssue' + paxTypeIndex).val();
                    }
                } catch (err) {
                    /* empty */
                }
                var nationalityData = null;
                try {
                    if (!isNullorEmpty($('#txtnationality' + paxTypeIndex).val())) {
                        nationalityData = {
                            twoLetter: $('#txtnationality' + paxTypeIndex).val(),
                            threeLetter: $('#txtnationality' + paxTypeIndex + '>option:selected').data('letter3')
                        };
                    }
                } catch (err) {}

                var traveller = {
                    passengerType: paxTypeCode,
                    gender: paxGender,
                    givenName: $('#txtFname' + paxTypeIndex).val().trim().toLowerCase().replace(/(^.)/, m => m.toUpperCase()),
                    namePrefix: paxTitle,
                    surName: $('#txtSname' + paxTypeIndex).val().trim().toLowerCase().replace(/(^.)/, m => m.toUpperCase()),
                    quantity: i,
                    birthDate: paxDob,
                    docType: $('input:radio[name=doc'+ paxTypeIndex +']:checked').val(),
                    docID: $('#txtPassport' + paxTypeIndex).val().toUpperCase(),
                    issuanceDate: passIsuDate,
                    docIssueCountry: docIssueCountry,
                    expireDate: passExpDate,
                    nationalityData: nationalityData,
                    frequentFlyerNumber: (frequentFlayerNumber == undefined || frequentFlayerNumber == '') ? null : frequentFlayerNumber,
                    ffAirlineCode: (frequentFlayerAirlineCode == undefined || frequentFlayerAirlineCode == '') ? null : frequentFlayerAirlineCode,
                    extras: Extraservices
                };
                var contact = null;
                if (paxTypeIndex === 'ADT1') {
                    var phone = $('#txtLeadPaxPhone2').val();
                    try {
                        phone = (_.find(vm.countryCodes, function (code) {
                            return code[2].match($('#txtLeadPaxPhone2').val()) && $('#txtLeadPaxPhone2').val().match(code[2]);
                        })[1] || "").toUpperCase();
                    } catch (error) {
                        phone = "";
                    }
                    contact = {
                        phoneList: [{
                            number: $('#txtLeadPaxPhone').val().trim(),
                            country: {
                                code: $('#txtLeadPaxPhone2').val() ? phone.trim() : "",
                                telephonecode: $('#txtLeadPaxPhone2').val().trim()
                            },
                            phoneType: {
                                id: 1
                            }
                        }],
                        emailList: [{
                            emailId: $('#txtLeadPaxEmail').val().trim(),
                            emailType: {
                                id: 1
                            }
                        }]
                    };

                    contact.phoneList = contact.phoneList.concat(vm.additionalContact.map(function (e) {
                        return {
                            number: e.contactNumber,
                            country: {
                                code: e.countryCode,
                                telephonecode: e.countryCodeNumber
                            },
                            phoneType: {
                                id: 1
                            }
                        }
                    }))

                    contact.emailList = contact.emailList.concat(vm.additionalContact.map(function (e) {
                        return {
                            emailId: e.emailId,
                            emailType: {
                                id: 1
                            }
                        }
                    }))
                }


                traveller['contact'] = contact;
                travellers.push(traveller);
            }

            var airSegments = airSupplierResponse.fareInformationWithoutPnrReply.airSegments;
            var supplierIdForBook = airSupplierResponse.fareInformationWithoutPnrReply.fSupplier;
            var airLineBaggages = [];
            if (vm.SupplierId != "44" && vm.SupplierId && "49" && vm.SupplierId != "50" && vm.SupplierId != "71") {
                $.each(airSegments, function (index, airSegment) {
                    // if (supplierIdForBook == '45') {
                    //     $.each(airSegment.baggageInformations, function (bagIndex, baggage) {
                    //         var airLineBaggage = {
                    //             paxType: baggage.bagPaxType,
                    //             cabinBaggageQuantity: airSegment.handBagInformations[bagIndex].baggage,
                    //             cabinBaggageUnit: airSegment.handBagInformations[bagIndex].unit,
                    //             checkinBaggageQuantity: baggage.baggage,
                    //             checkinBaggageUnit: baggage.unit,
                    //             fromSeg: airSegment.segInfo.flData.boardPoint,
                    //             toSeg: airSegment.segInfo.flData.offpoint,
                    //             desc: baggage.desc
                    //         }
                    //         airLineBaggages.push(airLineBaggage);
                    //     });
                    // } else {
                        var tempBaggage = [];
                        $.each(airSegment.baggageInformations, function (bagIndex, baggage) {
                            let row = { 
                                       bagPaxType: baggage.bagPaxType,
                                       baggage: baggage.baggage,
                                       unit: baggage.unit
                                    }
                                    tempBaggage.push(row)
                        });
                        if(tempBaggage.length > 0){
                          if(airSegment.handBagInformations){
                          for (let hbi of airSegment.handBagInformations) {
                            let baggageRow = _.find(tempBaggage, function (tb) {
                                return tb.bagPaxType == hbi.bagPaxType
                            })
                            if(baggageRow) {
                                 baggageRow.handBaggage =  hbi.baggage;
                                 baggageRow.handUnit = hbi.unit;
                            }
                            else{
                                let row = { 
                                    bagPaxType: hbi.bagPaxType,
                                    handBaggage: hbi.baggage,
                                    handUnit: hbi.unit
                                 }
                                 tempBaggage.push(row)
                            }
                          }
                        }
                        }
                        else{
                            $.each(airSegment.handBagInformations, function (bagIndex, baggage) {
                                let row = { 
                                           bagPaxType: baggage.bagPaxType,
                                           handBaggage: baggage.baggage,
                                           handUnit: baggage.unit
                                        }
                                        tempBaggage.push(row)
                            });
                        }
                    $.each(tempBaggage, function (bagIndex, baggage) {
                        var airLineBaggage = {
                            paxType: baggage.bagPaxType,
                            cabinBaggageQuantity: baggage.handBaggage ? baggage.handBaggage : "7",
                            cabinBaggageUnit: baggage.handUnit ? baggage.handUnit : "KG",
                            checkinBaggageQuantity: baggage.baggage,
                            checkinBaggageUnit: baggage.unit,
                            fromSeg: airSegment.segInfo.flData.boardPoint,
                            toSeg: airSegment.segInfo.flData.offpoint,
                            cabinBaggageDesc: airSegment.handBagInformations ? airSegment.handBagInformations[bagIndex].desc : "",
                            checkinBaggageDesc: baggage.desc
                        }
                        airLineBaggages.push(airLineBaggage);
                    });
                    // }
                });
            }

            var airFareRules = [];
            var miniRules = [];
            try {
                miniRules = airSupplierResponse.fareInformationWithoutPnrReply.minirules
            } catch (err) {
                miniRules = [];
            }
            if (isNullorUndefined(miniRules)) {
                miniRules = [];
            }

            $.each(airSupplierResponse.fareInformationWithoutPnrReply.airFareRule, function (index, fareRule) {
                $.each(fareRule.fareRuleDetails, function (ruleIndex, rule) {
                    if (!stringIsNullorEmpty(rule.ruleBody) && !stringIsNullorEmpty(rule.ruleHead)) {
                        var airFareRule = {
                            fareRule: rule.ruleBody,
                            fareRef: rule.ruleHead,
                            segment: fareRule.departure + '-' + fareRule.arrival,
                            filingAirline: null,
                            marketingAirline: fareRule.airline
                        };
                        airFareRules.push(airFareRule);
                    }
                });

            });
            var paymentMode = 7;
            if ($('#radio-btn-03').prop('checked')) {
                paymentMode = 6;
            }
            var cookie = '';
            var selectCred = null;
            try {
                selectCred = selectResponseObj.selectCredential;
            } catch (err) {}
            try {
                cookie = airSupplierResponse.fareInformationWithoutPnrReply.airSegments[0].fareId;
            } catch (err) {};
            if (fsupplier == "54") {
                cookie = ""
            }
            var totalfaregroup = {};
            var sealedId = '';
            if (vm.hasAncilleryResponse) {
                totalfaregroup = {
                    totalBaseNet: parseFloat(vm.ancillaryRespones.flightFareDetails.basefare),
                    actualBaseNet: 0,
                    totalTaxNet: parseFloat(vm.ancillaryRespones.flightFareDetails.taxFare),
                    actualTaxNet: 0,
                    netCurrency: vm.ancillaryRespones.flightFareDetails.currency,
                    paidDate: null,
                    paidAmount: parseFloat(vm.ancillaryRespones.flightFareDetails.totalFare),
                    paymentTypeId: 1
                };
                sealedId = vm.ancillaryRespones.sealed;
            } else {
                totalfaregroup = {
                    totalBaseNet: parseFloat(airSupplierResponse.fareInformationWithoutPnrReply.flightFareDetails.basefare),
                    actualBaseNet: 0,
                    totalTaxNet: parseFloat(airSupplierResponse.fareInformationWithoutPnrReply.flightFareDetails.taxFare),
                    actualTaxNet: 0,
                    netCurrency: airSupplierResponse.fareInformationWithoutPnrReply.flightFareDetails.currency,
                    paidDate: null,
                    paidAmount: parseFloat(airSupplierResponse.fareInformationWithoutPnrReply.flightFareDetails.totalFare),
                    paymentTypeId: 1
                };
                sealedId = airSupplierResponse.fareInformationWithoutPnrReply.sealed;
            }

            var issueFlag = false;
            if (vm.issueFlag && bookStatusType == 0) {
                issueFlag = true;
            }

            var smsNotification = {
                countryCode: $('#txtLeadPaxPhone3').val() ? "+" + $('#txtLeadPaxPhone4').val() : "",
                number: $('#txtLeadPaxPhone3').val() || "",
                paxName: $('#txtFnameADT1').val().trim().toLowerCase().replace(/(^.)/, m => m.toUpperCase()) + " " + 
                $('#txtSnameADT1').val().trim().toLowerCase().replace(/(^.)/, m => m.toUpperCase())
            }
            var airBookingRequest = {
                request: {
                    service: 'FlightRQ',
                    supplierCodes: [airSupplierResponse.fareInformationWithoutPnrReply.fSupplier],
                    content: {
                        command: 'FlightBookRQ',
                        supplierSpecific: supplierSpecific,
                        bookFlightRQ: {
                            issue: issueFlag,
                            airBagDetails: airLineBaggages,
                            bookFlightEntity: {
                                bookingRefId: null,
                                bookFlight: {
                                    dealCode: airSupplierResponse.fareInformationWithoutPnrReply.dealCode,
                                    supplierId: airSupplierResponse.fareInformationWithoutPnrReply.fSupplier,
                                    supplierRemarks: airSupplierResponse.fareInformationWithoutPnrReply.fareFareSourceCode,
                                    otherDetails: null,
                                    reference: cookie,
                                    comments: airSupplierResponse.fareInformationWithoutPnrReply.fareAgencyCode,
                                    supplierSpecific: supplierSpecific,
                                    tripType: airSupplierResponse.fareInformationWithoutPnrReply.tripType.toUpperCase(),
                                    faresourceCode: airSupplierResponse.fareInformationWithoutPnrReply.fareFareSourceCode,
                                    sessionId: airSupplierResponse.fareInformationWithoutPnrReply.fareSessionId,
                                    abc: airSupplierResponse.fareInformationWithoutPnrReply.fareFareSourceCode,
                                    addressLine: [
                                        "Abc",
                                        "Xyz"
                                    ],
                                    fdetails: {
                                        flightId: '',
                                        clientIp: '',
                                        confirmedCurrency: vueCommonStore.state.agencyNode.loginNode.currency
                                    },
                                    travelerInfo: travellers,
                                    adt: airSupplierResponse.fareInformationWithoutPnrReply.passengerTypeQuantity.adt,
                                    chd: airSupplierResponse.fareInformationWithoutPnrReply.passengerTypeQuantity.chd,
                                    inf: airSupplierResponse.fareInformationWithoutPnrReply.passengerTypeQuantity.inf,
                                    flLegGroup: airLineLegs,
                                    specialReqDetails: specialReqDetails
                                }
                            },
                            smsNotification: $('#txtLeadPaxPhone3').val() && $('#txtLeadPaxPhone4').val() ? smsNotification : undefined,
                            paymentMode: paymentMode,
                            totalfaregroup: totalfaregroup,
                            fareRuleSeg: airFareRules,
                            miniRules: miniRules,
                            sealed: sealedId,
                            userContactDetails: {
                                countryCode: telcode,
                                contactNumber: phno
                            }
                        }
                    },
                    selectCredential: selectCred,
                    hqCode:vm.commonStore.agencyNode.loginNode.solutionId ? vm.commonStore.agencyNode.loginNode.solutionId :""
                }
            }

            return airBookingRequest;
        },
        //ancillery Service Start
        getAirBaggageService: function () {
            var vm = this;
            var airBaggageRequest = this.generateAirbaggagerequest();
            console.log(JSON.stringify(airBaggageRequest));
            var postDta = airBaggageRequest;
            var huburl = vm.commonStore.hubUrls.hubConnection.baseUrl;
            var portno = vm.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = vm.commonStore.hubUrls.hubConnection.hubServices.flights.airBaggage;
            var config = {
                axiosConfig: {
                    method: "POST",
                    url: huburl + portno + serviceUrl,
                    data: postDta
                },
                successCallback: function (response) {
                    console.log("AirBaggage RESPONSE RECEIVED: ", response);
                    localStorage.accessToken = response.headers.access_token;
                    try {
                        vm.baggageDetailsResponses = response.data.response.content.aaOTAAirBaggageDetailsRS.baggageDetailsResponses.onDBaggageDetailsResponse;
                        var responseHasZeroValue = false;
                        for (let k = 0; k < vm.baggageDetailsResponses.length; k++) {
                            vm.baggageDetailsResponses[k].baggage = _.sortBy(vm.baggageDetailsResponses[k].baggage, 'baggageCharge');
                            for (let l = 0; l < vm.baggageDetailsResponses[k].baggage.length; l++) {
                                var baggageCharge = vm.baggageDetailsResponses[k].baggage[l].baggageCharge;
                                var baggageCode = vm.baggageDetailsResponses[k].baggage[l].baggageCode;
                                if (baggageCharge == 0 && baggageCode.toLowerCase() != 'no bag') {
                                    responseHasZeroValue = true;
                                    break;
                                }
                            }
                        }
                        vm.responseHasZeroValueBaggage = responseHasZeroValue;
                        vm.oldBaggage = JSON.parse(JSON.stringify(vm.baggageDetailsResponses));
                        if (vm.baggageDetailsResponses.length > 0) {
                            vm.ShowExtrasDiv = false;
                            vm.ShowAncillerBaggage = true;
                            vm.ancillaryServicesList[1].show = true;
                        }
                    } catch (err) {
                        try {
                            console.log('Baggage details not available for requested carrier.');
                        } catch (err) {}
                    }
                    vm.ShowExtrasDiv = false;
                },
                errorCallback: function (error) {},
                showAlert: false
            };

            mainAxiosRequest(config);

        },
        generateAirbaggagerequest: function () {
            var airSupplierResponse = selectResponseObj.content;
            var cookie = null;
            var airResposneLegs = airSupplierResponse.fareInformationWithoutPnrReply.airSegments;
            if (selectResponseObj.supplierCode == "54" || selectResponseObj.supplierCode == "71" || selectResponseObj.supplierCode == "49" || selectResponseObj.supplierCode == "50" || selectResponseObj.supplierCode == "44") {
                cookie = selectResponseObj.content.supplierSpecific.cookie
            } else {
                cookie = airResposneLegs[0].fareId;
            }
            var airSectoridies = _.pluck(airResposneLegs, 'segDirectionID');
            airSectoridies = _.uniq(airSectoridies);
            var airLineSegments = [];
            for (var i = 0; i < airSectoridies.length; i++) {
                var segments = airResposneLegs.filter(function (segment) {
                    return segment.segDirectionID == airSectoridies[i]
                });
                $.each(segments, function (segIndex, segment) {
                    var airLineSegment = {
                        segmentInformation: {
                            flightDate: {
                                departureDate: segment.segInfo.dateandTimeDetails.departureDate,
                                departureTime: segment.segInfo.dateandTimeDetails.departureTime,
                                arrivalDate: segment.segInfo.dateandTimeDetails.arrivalDate,
                                arrivalTime: segment.segInfo.dateandTimeDetails.arrivalTime
                            },
                            segId: segment.fareSourceCode,
                            boardPointDetails: {
                                trueLocationId: segment.segInfo.flData.boardPoint
                            },
                            offPointDetails: {
                                trueLocationId: segment.segInfo.flData.offpoint
                            },
                            companyDetails: {
                                marketingCompany: segment.segInfo.flData.marketingCompany,
                                operatingCompany: segment.segInfo.flData.operatingCompany
                            },
                            flightIdentification: {
                                flightNumber: segment.segInfo.flData.flightNumber,
                                bookingClass: !stringIsNullorEmpty(segment.segInfo.flData.bookingClass) ? segment.segInfo.flData.bookingClass : segment.segInfo.flData.rbd,
                            },
                            flightTypeDetails: {
                                flightIndicator: 1,
                                itemNumber: 1
                            }
                        }

                    };
                    airLineSegments.push(airLineSegment);
                });

            }
            var selectCred = null;
            // var mappingDetails = undefined;
            try {
                selectCred = selectResponseObj.selectCredential;
                // if (selectResponseObj.supplierCode == "54") {
                // var tempsupplierSpecific = JSON.parse(localStorage.supplierSpecific);
                // if (tempsupplierSpecific.supplierSpecific.mappingDetails) {
                // mappingDetails = tempsupplierSpecific.supplierSpecific.mappingDetails;
                // }
                // }
            } catch (err) {}

            var supplierSpecific = JSON.parse(localStorage.supplierSpecific).supplierSpecific;
            supplierSpecific.cookie = cookie;

            var airBaggageRequest = {
                request: {
                    service: "FlightRQ",
                    content: {
                        command: "FlightAirBaggaeRQ",
                        airBaggageRequest: {
                            sessionId: airSupplierResponse.fareInformationWithoutPnrReply.fareSessionId,
                            segmentGroup: airLineSegments
                        },
                        supplierSpecific: supplierSpecific
                    },
                    selectCredential: selectCred,
                    supplierCodes: [airSupplierResponse.fareInformationWithoutPnrReply.fSupplier]
                }
            };
            return airBaggageRequest;
        },
        processMeals: function (meals, paxIndex, pax) {
            if (this.SupplierId == "69" || this.SupplierId == "86") {
                var mealPax = this.oldMeal.filter(function (e) {
                    return e.mealIdentifier.segmentCode == meals.RPH && e.mealIdentifier.paxCode == pax.paxcode;
                });
                try {
                    var index = this.MealPaxArr.findIndex(function (e) {
                        return e.RPH == meals.RPH;
                    })
                    $.each(this.MealPaxArr[index].travallers, function (chind, chpax) {
                        if (chpax.paxcode == pax.paxcode) {
                            chpax.mealsActive = true;
                        } else {
                            chpax.mealsActive = false
                        }
                    })
                    this.MealPaxArr[index].Meals = JSON.parse(JSON.stringify(mealPax[0].meal))
                } catch (error) {}
            }
        },
        getAirMealsService: function () {
            var vm = this;
            var airMealsRequest = this.generateAirMealsRequest();
            console.log("airMealsRequest " + JSON.stringify(airMealsRequest));
            var postDta = airMealsRequest;
            var huburl = vm.commonStore.hubUrls.hubConnection.baseUrl;
            var portno = vm.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = vm.commonStore.hubUrls.hubConnection.hubServices.flights.airmeals;

            var config = {
                axiosConfig: {
                    method: "POST",
                    url: huburl + portno + serviceUrl,
                    data: postDta
                },
                successCallback: function (response) {
                    console.log("AirMeals RESPONSE RECEIVED: ", response);
                    localStorage.accessToken = response.headers.access_token;

                    try {
                        vm.mealDetailsResponses = response.data.response.content.aaOTAAirMealDetailsRS.mealDetailsResponses.mealDetailsResponse;
                        var responseHasZeroValue = false;
                        for (let k = 0; k < vm.mealDetailsResponses.length; k++) {
                            for (let l = 0; l < vm.mealDetailsResponses[k].meal.length; l++) {
                                var mealCharge = vm.mealDetailsResponses[k].meal[l].mealCharge;
                                if (mealCharge == 0) {
                                    responseHasZeroValue = true;
                                    break;
                                }
                            }
                        }
                        vm.responseHasZeroValueMeal = responseHasZeroValue;
                        if (vm.mealDetailsResponses.length > 0) {
                            vm.ShowExtrasDiv = false;
                            vm.ShowAncillerMeals = true;
                            vm.ancillaryServicesList[2].show = true;
                        }
                    } catch (err) {
                        try {
                            console.log(response.data.response.content.aaOTAAirMealDetailsRS.errors.error[0].shortText);
                        } catch (err) {}
                    }
                    vm.ShowExtrasDiv = false;
                },
                errorCallback: function (error) {},
                showAlert: false
            }

            mainAxiosRequest(config);
        },
        generateAirMealsRequest: function () {
            var airSupplierResponse = selectResponseObj.content;
            var cookie = null;
            var airResposneLegs = airSupplierResponse.fareInformationWithoutPnrReply.airSegments;
            if (selectResponseObj.supplierCode == "54" || selectResponseObj.supplierCode == "71" || selectResponseObj.supplierCode == "49" || selectResponseObj.supplierCode == "50" || selectResponseObj.supplierCode == "44") {
                cookie = selectResponseObj.content.supplierSpecific.cookie
            } else {
                cookie = airResposneLegs[0].fareId;
            }
            var airSectoridies = _.pluck(airResposneLegs, 'segDirectionID');
            airSectoridies = _.uniq(airSectoridies);
            var airLineSegments = [];
            for (var i = 0; i < airSectoridies.length; i++) {
                var segments = airResposneLegs.filter(function (segment) {
                    return segment.segDirectionID == airSectoridies[i]
                });
                $.each(segments, function (segIndex, segment) {
                    var airLineSegment = {
                        segmentInformation: {
                            flightDate: {
                                departureDate: segment.segInfo.dateandTimeDetails.departureDate,
                                departureTime: segment.segInfo.dateandTimeDetails.departureTime,
                                arrivalDate: segment.segInfo.dateandTimeDetails.arrivalDate,
                                arrivalTime: segment.segInfo.dateandTimeDetails.arrivalTime
                            },
                            segId: segment.fareSourceCode,
                            boardPointDetails: {
                                trueLocationId: segment.segInfo.flData.boardPoint
                            },
                            offPointDetails: {
                                trueLocationId: segment.segInfo.flData.offpoint
                            },
                            companyDetails: {
                                marketingCompany: segment.segInfo.flData.marketingCompany,
                                operatingCompany: segment.segInfo.flData.operatingCompany
                            },
                            flightIdentification: {
                                flightNumber: segment.segInfo.flData.flightNumber,
                                bookingClass: !stringIsNullorEmpty(segment.segInfo.flData.bookingClass) ? segment.segInfo.flData.bookingClass : segment.segInfo.flData.rbd,
                            },
                            flightTypeDetails: {
                                flightIndicator: 1,
                                itemNumber: 1
                            }
                        }

                    };
                    airLineSegments.push(airLineSegment);
                });

            }
            var selectCred = null;
            // var mappingDetails = undefined;
            try {
                selectCred = selectResponseObj.selectCredential;
                // if (selectResponseObj.supplierCode == "54") {
                // var tempsupplierSpecific = JSON.parse(localStorage.supplierSpecific);
                // if (tempsupplierSpecific.supplierSpecific.mappingDetails) {
                // mappingDetails = tempsupplierSpecific.supplierSpecific.mappingDetails;
                // }
                // }
            } catch (err) {}

            var supplierSpecific = JSON.parse(localStorage.supplierSpecific).supplierSpecific;
            supplierSpecific.cookie = cookie;

            var airmealsRequest = {
                request: {
                    service: "FlightRQ",
                    content: {
                        command: "AirMealRQ",
                        airMealRequest: {
                            sessionId: airSupplierResponse.fareInformationWithoutPnrReply.fareSessionId,
                            segmentGroup: airLineSegments
                        },
                        supplierSpecific: supplierSpecific
                    },
                    selectCredential: selectCred,
                    supplierCodes: [airSupplierResponse.fareInformationWithoutPnrReply.fSupplier]
                }
            };
            return airmealsRequest;
        },
        getSeatMapService: function () {
            var vm = this;
            var airSeatMapRequest = this.generateAirSeatMapRequest();
            console.log("airSeatMapRequest " + JSON.stringify(airSeatMapRequest));
            var postDta = airSeatMapRequest;
            var huburl = vm.commonStore.hubUrls.hubConnection.baseUrl;
            var portno = vm.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = vm.commonStore.hubUrls.hubConnection.hubServices.flights.airSeatMap;

            var config = {
                axiosConfig: {
                    method: "POST",
                    url: huburl + portno + serviceUrl,
                    data: postDta
                },
                successCallback: function (response) {
                    console.log("AirSeatMap RESPONSE RECEIVED: ", response);
                    localStorage.accessToken = response.headers.access_token;
                    try {
                        vm.seatmapInformation = response.data.response.content.seatMapData.airRetrieveSeatMapReply[0].seatmapInformation;
                        var responseHasZeroValue = false;
                        for (let j = 0; j < vm.seatmapInformation.length; j++) {
                            for (let k = 0; k < vm.seatmapInformation[j].row.length; k++) {
                                for (let l = 0; l < vm.seatmapInformation[j].row[k].rowDetails.seatOccupationDetails.length; l++) {
                                    for (let m = 0; m < vm.seatmapInformation[j].row[k].rowDetails.seatOccupationDetails[l].seatCharacteristicDetails.length; m++) {
                                        var totalFare = vm.seatmapInformation[j].row[k].rowDetails.seatOccupationDetails[l].seatCharacteristicDetails[m].totalFare;
                                        let occpied = vm.seatmapInformation[j].row[k].rowDetails.seatOccupationDetails[l].seatOccupation;
                                        if (totalFare == 0 && occpied !='O') {
                                            responseHasZeroValue = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        vm.responseHasZeroValueSeat = responseHasZeroValue;
                        if (vm.seatmapInformation.length > 0) {
                            vm.ShowExtrasDiv = false;
                            vm.ShowAncillerSeats = true;
                            vm.ancillaryServicesList[0].show = true; // seat
                        }
                    } catch (err) {
                        try {
                            console.log('Seat details not available for requested carrier.');
                        } catch (err) {}
                    }
                    vm.ShowExtrasDiv = false;
                },
                errorCallback: function (error) {},
                showAlert: false
            }

            mainAxiosRequest(config)
        },
        generateAirSeatMapRequest: function () {
            var cookie = null;
            var airSupplierResponse = selectResponseObj.content;
            var airResposneLegs = airSupplierResponse.fareInformationWithoutPnrReply.airSegments;
            if (selectResponseObj.supplierCode == "54" || selectResponseObj.supplierCode == "71" || selectResponseObj.supplierCode == "49" || selectResponseObj.supplierCode == "50" || selectResponseObj.supplierCode == "44") {
                cookie = selectResponseObj.content.supplierSpecific.cookie;
            } else {
                cookie = airResposneLegs[0].fareId;
            }
            var airSectoridies = _.pluck(airResposneLegs, 'segDirectionID');
            airSectoridies = _.uniq(airSectoridies);
            var airLineSegments = [];
            for (var i = 0; i < airSectoridies.length; i++) {
                var segments = airResposneLegs.filter(function (segment) {
                    return segment.segDirectionID == airSectoridies[i]
                });
                $.each(segments, function (segIndex, segment) {
                    var airLineSegment = {

                        flightDate: {
                            departureDate: segment.segInfo.dateandTimeDetails.departureDate,
                            departureTime: segment.segInfo.dateandTimeDetails.departureTime,
                            arrivalDate: segment.segInfo.dateandTimeDetails.arrivalDate,
                            arrivalTime: segment.segInfo.dateandTimeDetails.arrivalTime
                        },

                        boardPointDetails: {
                            trueLocationId: segment.segInfo.flData.boardPoint
                        },
                        offPointDetails: {
                            trueLocationId: segment.segInfo.flData.offpoint
                        },
                        companyDetails: {
                            marketingCompany: segment.segInfo.flData.marketingCompany,
                            operatingCompany: segment.segInfo.flData.operatingCompany
                        },
                        flightIdentification: {
                            flightNumber: segment.segInfo.flData.flightNumber,
                            bookingClass: !stringIsNullorEmpty(segment.segInfo.flData.bookingClass) ? segment.segInfo.flData.bookingClass : segment.segInfo.flData.rbd,
                        },
                        segInfo: {
                            segId: segment.fareSourceCode,
                            transId: airSupplierResponse.fareInformationWithoutPnrReply.fareSessionId
                        }


                    };
                    airLineSegments.push(airLineSegment);
                });

            }
            var selectCred = null;
            // var mappingDetails = undefined;
            try {
                selectCred = selectResponseObj.selectCredential;
                // if (selectResponseObj.supplierCode == "54") {
                // var tempsupplierSpecific = JSON.parse(localStorage.supplierSpecific);
                // if (tempsupplierSpecific.supplierSpecific.mappingDetails) {
                // mappingDetails = tempsupplierSpecific.supplierSpecific.mappingDetails;
                // }
                // }
            } catch (err) {}

            var supplierSpecific = JSON.parse(localStorage.supplierSpecific).supplierSpecific;
            supplierSpecific.cookie = cookie;

            var airSeatRequest = {
                request: {
                    service: "FlightRQ",
                    content: {
                        command: "AirSeatRQ",
                        seatMapRequest: {
                            seatRequestParameters: {
                                processingIndicator: "FT"
                            },
                            travelProductIdent: airLineSegments
                        },
                        supplierSpecific: supplierSpecific
                    },
                    selectCredential: selectCred,
                    supplierCodes: [airSupplierResponse.fareInformationWithoutPnrReply.fSupplier]
                }
            };
            return airSeatRequest;
        },
        openPaidServices: function () {
            var isValidate = false;
            if (self.SupplierId == "54") {
                isValidate = validateEntries('seat');
            } else {
                isValidate = validateEntriesforancillery();
            }
            if (isValidate) {
                $("#paid_services").modal({
                    backdrop: 'static',
                    keyboard: false,
                    show: true
                })
            }
        },
        SetAncillerBaggage: function () {
            var self = this;
            var isValidate = false;
            var baggageDetails = ["BAGB", "BAGL", "BAGX"];
            if (self.SupplierId == "54") {
                isValidate = validateEntries('BAG');
                // baggageDetails = [
                //     {code: "BAGB",value: 20},
                //     {code: "BAGL",value: 30},
                //     {code: "BAGX",value: 40},
                //     {code: "BUPL",value: 10},
                //     {code: "BUPZ",value: 10},
                //     {code: "BUPX",value: 20}
                // ];
            } else {
                isValidate = validateEntriesforancillery();
            }
            self.prevBaggage = self.BaggageInfo.slice();
            // if (self.BaggageInfo.length != self.travellers.length) {
            self.BaggageInfo = [];
            // }
            if (isValidate) {
                if (self.travellers.length == 0) {
                    self.travellers = self.getpaxinfo();
                }

                if (self.SupplierId == "69" || self.SupplierId == "86") {

                    // create a map for al jazeera
                    var newBaggageResponse = [];
                    var tempLeg = [];
                    for (var x = 0; x < self.baggageDetailsResponses.length; x++) {
                        var baggageExist = tempLeg.some(function (e) {
                            return e == self.baggageDetailsResponses[x].baggageIdentifier.segmentCode;
                        });
                        if (!baggageExist) {
                            newBaggageResponse.push(self.baggageDetailsResponses[x]);
                            tempLeg.push(self.baggageDetailsResponses[x].baggageIdentifier.segmentCode);
                        }
                    }
                    self.baggageDetailsResponses = newBaggageResponse;
                }

                for (var i = 0; i < self.baggageDetailsResponses.length; i++) {
                    var paxbaggage = [];
                    var Baggages = [];
                    var FlightSegmentInfo = self.baggageDetailsResponses[i].onDFlightSegmentInfo;
                    var rph = '';
                    var bgseg = i;
                    var flightNumber = '';
                    var bgdepartureDate = '';
                    var bgdeparturetime = '';
                    var destinations = '';
                    if (FlightSegmentInfo.length > 0) {
                        for (var fls = 0; fls < FlightSegmentInfo.length; fls++) {
                            if (destinations != '') {
                                destinations = destinations + '#';
                            }
                            destinations = destinations + FlightSegmentInfo[fls].departureAirport.locationCode + '-' + FlightSegmentInfo[fls].arrivalAirport.locationCode;
                            if ((self.SupplierId == "69" || this.SupplierId == "86") && FlightSegmentInfo[fls].rph == undefined) {
                                rph = rph + FlightSegmentInfo[fls].segmentCode + ',';
                            } else {
                                rph = rph + FlightSegmentInfo[fls].rph + ',';
                            }
                            flightNumber = flightNumber + FlightSegmentInfo[fls].flightNumber + ',';
                            bgdepartureDate = bgdepartureDate + moment(FlightSegmentInfo[fls].departureDate, 'DDMMYY').format('DDMMYY') + ',';
                            bgdeparturetime = bgdeparturetime + FlightSegmentInfo[fls].departureTime + ',';
                        }
                        rph = rph.slice(0, -1);
                        flightNumber = flightNumber.slice(0, -1);
                        bgdepartureDate = bgdepartureDate.slice(0, -1);
                        bgdeparturetime = bgdeparturetime.slice(0, -1);
                        var from = FlightSegmentInfo[0].departureAirport.locationCode;
                        var to = FlightSegmentInfo[FlightSegmentInfo.length - 1].arrivalAirport.locationCode;
                    }
                    if (self.travellers.length > 0) {
                        for (var paxi = 0; paxi < self.travellers.length; paxi++) {
                            if (self.SupplierId == "54") {
                                var quantity = "";
                                var bagInfoPerLeg = _.filter(self.AirLineBaggages, function (e) {
                                    return e.segDirectionID == i;
                                });
                                if (bagInfoPerLeg[0].bagInfo.length > 0) {
                                    quantity = bagInfoPerLeg[0].bagInfo.filter(function (f) {
                                        var pTypeCode = "INF";
                                        if (f.paxType.toLowerCase() == "adult") {
                                            pTypeCode = "ADT"
                                        } else if (f.paxType.toLowerCase() == "child") {
                                            pTypeCode = "CHD"
                                        }
                                        return self.travellers[paxi].passengerType == pTypeCode;

                                    })[0].paxUnitQuantity.split(" ")[0];
                                    switch (quantity) {
                                        case "20":
                                            baggageDetails = ["BUPL", "BUPX"];
                                            break;
                                        case "30":
                                            baggageDetails = ["BUPZ"];
                                            break;
                                        case "40":
                                            baggageDetails = [];
                                            break;
                                        default:
                                            baggageDetails = ["BAGB", "BAGL", "BAGX"];
                                            break;
                                    }
                                    Baggages = self.baggageDetailsResponses[i].baggage.filter(function (e) {
                                        return baggageDetails.indexOf(e.baggageCode) != -1;
                                    });
                                } else {
                                    baggageDetails = ["BAGB", "BAGL", "BAGX"];
                                    Baggages = self.baggageDetailsResponses[i].baggage.filter(function (e) {
                                        return baggageDetails.indexOf(e.baggageCode) != -1;
                                    });
                                }               
                            } else if (self.SupplierId == "69" || self.SupplierId == "86") {
                                //filter same paxcode and segment code
                                try {
                                    Baggages = self.oldBaggage.filter(function (e) {
                                        return e.baggageIdentifier.segmentCode == self.baggageDetailsResponses[i].baggageIdentifier.segmentCode &&
                                            e.baggageIdentifier.paxCode == self.travellers[paxi].paxcode;
                                    })[0].baggage;
                                } catch (error) {
                                    Baggages = [];
                                }
                            } else {
                                Baggages = self.baggageDetailsResponses[i].baggage;
                            }
                            console.log('Baggages',Baggages);
                            var BAG = [];
                            var user = []
                            user = self.travellers[paxi];

                            if (self.SupplierId == "54") {
                                if (bagInfoPerLeg[0].bagInfo.length > 0) {
                                    if (bagInfoPerLeg[0].bagInfo.length > 0) {
                                        var info = bagInfoPerLeg[0].bagInfo[0].paxUnitQuantity;
                                        user.bagInf = info.toUpperCase();
                                        if (info.split(" ")[0] == "40") {
                                            user.isBaggageAdded = true;
                                        }
                                    } else {
                                        user.isBaggageAdded = false;
                                    }
                                } else {
                                    user.bagInf = "NO BAGGAGE";
                                    user.isBaggageAdded = false;
                                }
                            }
                            if (self.prevBaggage.length != 0) {
                                user.bagInf = self.prevBaggage[i].Pax[paxi].bagInf;
                                user.bagCharge = self.prevBaggage[i].Pax[paxi].bagCharge;
                            }

                            if (self.SupplierId=='44'||self.SupplierId=='49'||self.SupplierId =='50'||self.SupplierId =='71') {
                                var tmpBg = _.filter(self.AirLineBaggages, function (e) {
                                    return e.segDirectionID == i;
                                })[0].bagInfo[0];
                                user.bagInfOrig = tmpBg? tmpBg.paxUnitQuantity : '';
                            } else {
                                user.bagInfOrig = '';
                            }
                            if (Baggages && Baggages.length > 0) {
                                BAG = Baggages;
                            } else {
                                BAG = [];
                            }
                            user = Object.assign({}, user, {
                                "BAG": BAG
                            })
                            // user = { ...user, BAG };
                            paxbaggage.push(user);
                        }
                    }
                    // if (self.BaggageInfo.length != self.travellers.length) {
                    self.BaggageInfo.push({
                        index: i,
                        From: from,
                        destinations: destinations,
                        To: to,
                        RPH: rph,
                        FlNo: flightNumber,
                        DepDate: bgdepartureDate,
                        DepTime: bgdeparturetime,
                        Pax: paxbaggage
                    });
                    // }
                }
                // $("#baggage_popup").modal({ backdrop: 'static', keyboard: false, show: true })


            }

        },
        getpaxinfo: function () {
            var self = this;
            var Travellers = [];
            var airSupplierResponse = selectResponseObj.content;
            var passengertypequantity = airSupplierResponse.fareInformationWithoutPnrReply.passengerTypeQuantity;
            var adult = passengertypequantity.adt;
            var child = passengertypequantity.chd;
            var adultIndex = 1;
            var childIndex = 1;
            var infantIndex = 1;
            var currentIndex = 1;
            // var infant = passengertypequantity.inf;
            var totalPassengers = parseInt(adult) + parseInt(child); // + parseInt(infant);
            self.TotalPaxCount = parseInt(totalPassengers);
            for (var i = 1; i <= totalPassengers; i++) {
                var paxType = 'Adult';
                var paxTypeCode = 'ADT';
                if (i > adult) {
                    paxType = 'Child';
                    paxTypeCode = 'CHD';
                }
                if (i > parseInt(adult) + parseInt(child)) {
                    paxType = 'Infant';
                    paxTypeCode = 'INF';
                }
                switch (paxTypeCode) {
                    case 'ADT':
                        currentIndex = adultIndex;
                        adultIndex++;
                        break;
                    case 'CHD':
                        currentIndex = childIndex;
                        childIndex++;
                        break;
                    case 'INF':
                        currentIndex = infantIndex;
                        infantIndex++;
                        break;
                }
                var paxTypeIndex = paxTypeCode + currentIndex;
                var traveller = {
                    passengerType: paxTypeCode,
                    givenName: $('#txtFname' + paxTypeIndex).val().trim(),
                    namePrefix: $('#ddltitle' + paxTypeIndex).val(),
                    surName: $('#txtSname' + paxTypeIndex).val().trim(),
                    paxType: (paxTypeCode == 'ADT' ? 'A' : 'C'),
                    paxcode: (paxTypeCode == 'ADT' ? 'A' + i : 'C' + i),
                    bagInf: self.No_Baggage_Label,
                    bagCharge: 0,
                    SeatCode: '',
                    SeatCharge: 0,
                    active: (i == 1) ? true : false,
                    index: (i - 1),
                    Meals: [],
                    isBaggageAdded: false,
                    mealsActive: (self.SupplierId == "69" || self.SupplierId == "86") ? (i == 1) ? true : false : false
                };
                Travellers.push(traveller);
            }
            return Travellers;
        },
        openpaxbaggage: function (event, id) {
            var self = this;
            var clickedElement = event.currentTarget;
            $(clickedElement).find('p').text(($(clickedElement).find('p').text() == self.Close_Button_Label) ? self.Edit_Button_Label : self.Close_Button_Label).fadeIn();
            $(clickedElement).find('i').toggleClass('fa-close fa-edit');
            $("#" + id).slideToggle("slow");
        },
        chooseBaggage: function (event, bag, pax, paxbag) {
            var self = this;
            var hashCode = null;
            var ruleId = null;
            if (paxbag.hasOwnProperty('hashCode')) {
                hashCode = paxbag.hashCode;
            }
            if (paxbag.hasOwnProperty('ruleId')) {
                ruleId = paxbag.ruleId;
            }
            var clickedElement = event.currentTarget;
            pax.bagInf = paxbag.baggageCode;
            pax.bagCharge = paxbag.baggageCharge;
            $(clickedElement).addClass('active');
            $(clickedElement).siblings().removeClass('active');
            var dataLegIndex = bag.index;
            var sgg = dataLegIndex;
            var dataDestinations = bag.destinations.split('#');
            var price = paxbag.baggageCharge;
            var paxName = pax.namePrefix + ' ' + pax.givenName + '' + pax.surName;
            var code = paxbag.baggageCode;
            var desc = paxbag.baggageDescription;
            var paxCode = pax.paxcode;
            var temppaxno = paxCode.match(/\d/g);
            temppaxno = parseInt(temppaxno.join(""));
            var flight = bag.FlNo.split(',');
            var depdate = bag.DepDate.split(',');
            var deptime = bag.DepTime.split(',');
            var rph = bag.RPH.split(',');
            var quantity = code.split(" ")[0] == 'No' ? '0' : code.split(" ")[0];
            var unit = code.split(" ")[1];
            if (self.SupplierId == "54" || self.SupplierId == "86") {
                quantity = desc.replace(/\D/g, '') == "" ? '0' : desc.replace(/\D/g, '');
                unit = "KG";
            }
            if (self.SupplierId == "69") {
                quantity = desc.split(" ")[0] == 'No' ? '0' : desc.split(" ")[0];
                unit = desc.split(" ")[1];
                var uniqueRph = [];
                $.each(rph, function (i, el) {
                    if ($.inArray(el, uniqueRph) === -1) uniqueRph.push(el);
                });

                rph = uniqueRph;
            }
            if (rph.length > 0) {
                for (var rpii = 0; rpii < rph.length; rpii++) {
                    var tempsg = rpii;
                    if (rph.length == 1) {
                        tempsg = sgg;
                    } else {
                        if (sgg > 0) {
                            tempsg = parseInt(sgg + sgg) + rpii;
                        } else {
                            tempsg = rpii;
                        }
                    }
                    var RphNo = rph[rpii];
                    var Flno = flight[rpii];
                    var dtime = deptime[rpii];
                    var ddate = depdate[rpii];
                    var index;
                    var found = self.Baggagepaxarr.some(function (el, i) {
                        if (el.flightRefNumberRPHList === RphNo) {
                            index = i;
                            return true;
                        }
                    });
                    var destinations = dataDestinations[rpii].split('-');
                    if (!found) {
                        self.Baggagepaxarr.push({
                            segment: tempsg,
                            departureDate: ddate,
                            departureTime: dtime,
                            flightNumber: Flno,
                            baggageData: [{
                                paxCode: paxCode,
                                paxCode1: temppaxno,
                                paxName: paxName,
                                bagCode: code,
                                unit: '',
                                unit1: unit,
                                quantity: quantity,
                                Price: price,
                                currency: paxbag.currencyCode,
                                code: code,
                                sealedCode: paxbag.sealedCode,
                                hashCode: paxbag.hashCode,
                                ruleId: paxbag.ruleId
                            }],
                            flightRefNumberRPHList: RphNo,
                            legIndex: dataLegIndex,
                            fromAP: destinations[0],
                            toAP: destinations[1]
                        });
                        //self.BagaggeExtra.push({ segment: tempsg, segRph: RphNo, baggageData: [{ paxCode: paxCode.match(/\d/g).join(""), quantity: quantity, unit: unit, Price: price.toString(), currency: paxbag.currencyCode, code: code, ruleId: ruleId, hashCode: hashCode }] });
                        self.BagaggeExtra.push({
                            segment: tempsg,
                            segRph: RphNo,
                            baggageData: [{
                                sealedCode: paxbag.sealedCode,
                                paxCode: paxCode.match(/\d/g).join(""),
                                paxType: pax.paxType,
                                quantity: quantity,
                                unit: unit,
                                Price: price.toString(),
                                currency: paxbag.currencyCode,
                                code: code,
                                ruleId: ruleId,
                                hashCode: hashCode
                            }]
                        });

                    } else {
                        var bagggearr = self.Baggagepaxarr[index].baggageData;
                        var baggagefound = bagggearr.some(function (el, i) {
                            if (el.paxCode === paxCode) {
                                if (el.quantity == quantity) {
                                    if (self.SupplierId == "71" || self.SupplierId == "49" || self.SupplierId == "50" || self.SupplierId == "44") {
                                        // var noBag = pax.BAG.filter(function (e) {
                                        //     return e.baggageCode.toLowerCase() == "no bag";
                                        // })[0];
                                        // if (noBag) {
                                        //     el.bagCode = noBag.baggageCode;
                                        //     self.BagaggeExtra[index].baggageData[i].quantity = 0;
                                        //     self.BagaggeExtra[index].baggageData[i].unit = "KG";
                                        //     self.BagaggeExtra[index].baggageData[i].Price = noBag.baggageCharge;
                                        //     self.BagaggeExtra[index].baggageData[i].code = noBag.baggageCode;
                                        //     self.Baggagepaxarr[index].baggageData[i].quantity = 0;
                                        //     self.Baggagepaxarr[index].baggageData[i].unit = "KG";
                                        //     self.Baggagepaxarr[index].baggageData[i].Price = noBag.baggageCharge;
                                        //     self.Baggagepaxarr[index].baggageData[i].code = noBag.baggageCode;
                                        //     self.Baggagepaxarr[index].baggageData[i].fromAP = destinations[0];
                                        //     self.Baggagepaxarr[index].baggageData[i].toAP = destinations[1];

                                        //     self.BagaggeExtra[index].baggageData[i].ruleId = ruleId;
                                        //     self.BagaggeExtra[index].baggageData[i].hashCode = hashCode;

                                        //     self.BagaggeExtra[index].baggageData[i].sealedCode = noBag.sealedCode;
                                        //     self.Baggagepaxarr[index].baggageData[i].sealedCode = noBag.sealedCode;
                                        // } else {
                                        self.Baggagepaxarr[index].baggageData.splice(i, 1);
                                        if (self.Baggagepaxarr[index].baggageData.length == 0) {
                                            self.Baggagepaxarr.splice(index, 1);
                                        }
                                        self.BagaggeExtra[index].baggageData.splice(i, 1);
                                        if (self.BagaggeExtra[index].baggageData.length == 0) {
                                            self.BagaggeExtra.splice(index, 1);
                                        }
                                        // }
                                    } else {
                                        self.Baggagepaxarr[index].baggageData.splice(i, 1);
                                        if (self.Baggagepaxarr[index].baggageData.length == 0) {
                                            self.Baggagepaxarr.splice(index, 1);
                                        }
                                        self.BagaggeExtra[index].baggageData.splice(i, 1);
                                        if (self.BagaggeExtra[index].baggageData.length == 0) {
                                            self.BagaggeExtra.splice(index, 1);
                                        }
                                    }
                                    $(clickedElement).removeClass('active');
                                } else {
                                    el.bagCode = code;
                                    self.BagaggeExtra[index].baggageData[i].quantity = quantity
                                    self.BagaggeExtra[index].baggageData[i].unit = unit
                                    self.BagaggeExtra[index].baggageData[i].Price = price.toString();
                                    self.BagaggeExtra[index].baggageData[i].code = code;
                                    self.Baggagepaxarr[index].baggageData[i].quantity = quantity
                                    self.Baggagepaxarr[index].baggageData[i].unit = unit
                                    self.Baggagepaxarr[index].baggageData[i].Price = price;
                                    self.Baggagepaxarr[index].baggageData[i].code = code;
                                    self.Baggagepaxarr[index].baggageData[i].fromAP = destinations[0];
                                    self.Baggagepaxarr[index].baggageData[i].toAP = destinations[1];

                                    self.BagaggeExtra[index].baggageData[i].ruleId = ruleId;
                                    self.BagaggeExtra[index].baggageData[i].hashCode = hashCode;

                                    self.BagaggeExtra[index].baggageData[i].sealedCode = paxbag.sealedCode;
                                    self.Baggagepaxarr[index].baggageData[i].sealedCode = paxbag.sealedCode;
                                }
                                return true;
                            }
                        });
                        if (!baggagefound) {
                            self.Baggagepaxarr[index].baggageData.push({
                                hashCode: paxbag.hashCode,
                                sealedCode: paxbag.sealedCode,
                                paxCode: paxCode,
                                paxName: paxName,
                                bagCode: code,
                                unit: '',
                                paxCode1: temppaxno,
                                quantity: quantity,
                                unit1: unit,
                                Price: price,
                                currency: paxbag.currencyCode,
                                code: code,
                                fromAP: destinations[0],
                                toAP: destinations[1]
                            });
                            //self.BagaggeExtra[index].baggageData.push({ paxCode: paxCode.match(/\d/g).join(""), quantity: quantity, unit: unit, Price: price.toString(), currency: paxbag.currencyCode, code: code, ruleId: ruleId, hashCode: hashCode });
                            self.BagaggeExtra[index].baggageData.push({
                                sealedCode: paxbag.sealedCode,
                                paxCode: paxCode.match(/\d/g).join(""),
                                paxType: pax.paxType,
                                quantity: quantity,
                                unit: unit,
                                Price: price.toString(),
                                currency: paxbag.currencyCode,
                                code: code,
                                ruleId: ruleId,
                                hashCode: hashCode
                            });

                        }

                    }
                }
            }
            this.setPaxAncillaryPriceSummary();
            //console.log(JSON.stringify(self.Baggagepaxarr));
        },
        setPaxAncillaryPriceSummary: function (type) {
            var paxFareDetails = [];
            var self = this;
            var newPaxFareDet = [];
            for (var ik = 0; ik < this.PaxFareDet.length; ik++) {
                for (let ij = 0; ij < this.PaxFareDet[ik].totalPaxUnites; ij++) {
                    var travelerRefNumber = this.PaxFareDet[ik].travelerRefNumber;
                    if (this.PaxFareDet[ik].totalPaxUnites > 1 && this.PaxFareDet[ik].travelerRefNumber.length == 2) {
                        travelerRefNumber = this.PaxFareDet[ik].travelerRefNumber.split("")[0] + (ij + 1);
                    }
                    newPaxFareDet.push({
                        totalPax: this.PaxFareDet[ik].totalPax,
                        totalPaxUnites: "1",
                        paxType: this.PaxFareDet[ik].paxType,
                        totalFare: this.PaxFareDet[ik].totalFare,
                        basefare: this.PaxFareDet[ik].basefare,
                        travelerRefNumber: travelerRefNumber,
                        taxFare: this.PaxFareDet[ik].taxFare,
                        currency: this.PaxFareDet[ik].currency,
                        surcharges: this.PaxFareDet[ik].surcharges,
                        taxbreakup: this.PaxFareDet[ik].taxbreakup,
                        feesbreakup: this.PaxFareDet[ik].feesbreakup
                    })

                }

            }

            $.each(newPaxFareDet, function (paxindex, PaxFareData) {
                var paxfareinf = {};
                var currentpax = self.travellers.filter(obj => (obj.paxcode === PaxFareData.travelerRefNumber) || (obj.paxType + obj.paxcode.match(/\d/g)[0] === PaxFareData.travelerRefNumber) || (obj.passengerType + "-" + obj.paxcode.match(/\d/g)[0] === PaxFareData.travelerRefNumber))[0];

                if (!stringIsNullorEmpty(currentpax)) {
                    paxfareinf.givenName = currentpax.givenName;
                    paxfareinf.surName = currentpax.surName;
                    paxfareinf.namePrefix = currentpax.namePrefix;
                    paxfareinf.paxbag = [];
                    paxfareinf.paxseat = [];
                    paxfareinf.paxmeal = [];
                    paxfareinf.paxcode = currentpax.paxcode;
                    paxfareinf.seatTotal = 0;
                    paxfareinf.bagTotal = 0;
                    paxfareinf.mealTotal = 0;

                    $.each(self.SeatExtra, function (seatArryIndex, seatArryItem) {
                        try {
                            var tempseat = seatArryItem.seatinfo.filter(obj => obj.paxcode === PaxFareData.travelerRefNumber)[0];
                            if (!stringIsNullorEmpty(tempseat)) {

                                paxfareinf.haveSeatMap = true;
                                var newsegmentno = seatArryItem.segRph;
                                var tempsegments = selectResponseObj.content.fareInformationWithoutPnrReply.airSegments.filter(seg => seg.fareSourceCode === newsegmentno)[0];
                                $.each(seatArryItem.seatinfo, function (seatDataIndex, seatDataItem) {
                                    if (seatDataIndex === 0) {
                                        paxfareinf.paxseat.push({
                                            seat: tempseat,
                                            from: tempsegments.segInfo.flData.boardPoint,
                                            to: tempsegments.segInfo.flData.offpoint
                                        });
                                    }
                                });
                                paxfareinf.seatTotal = paxfareinf.paxseat.reduce(function (accum, arr) {
                                    return parseFloat(accum) + parseFloat(arr.seat.seat.Price);
                                }, parseFloat(0))
                            }
                        } catch (err) {

                        }
                    });
                    $.each(self.MealsExtra, function (mealArryIndex, mealArryItem) {
                        try {
                            var tempmeal = mealArryItem.mealData.filter(obj => (obj.paxCode === PaxFareData.travelerRefNumber) || (obj.paxType + obj.paxCode.match(/\d/g)[0] === PaxFareData.travelerRefNumber) || (obj.passengerType + "-" + obj.paxCode.match(/\d/g)[0] === PaxFareData.travelerRefNumber))[0];

                            if (!stringIsNullorEmpty(tempmeal)) {
                                tempmeal = JSON.parse(JSON.stringify(tempmeal));
                                paxfareinf.havemeals = true;
                                var newsegmentno = mealArryItem.segRph;
                                var tempsegments = selectResponseObj.content.fareInformationWithoutPnrReply.airSegments.filter(seg => seg.fareSourceCode === newsegmentno)[0];
                                if (self.SupplierId == "69" || self.SupplierId == "86") {
                                    tempsegments = selectResponseObj.content.fareInformationWithoutPnrReply.airSegments.filter(obj => (obj.segInfo.flData.boardPoint + "/" + obj.segInfo.flData.offpoint) == newsegmentno)[0];
                                }
                                $.each(mealArryItem.mealData, function (mealDataIndex, mealDataItem) {
                                    if (mealDataIndex === 0) {
                                        paxfareinf.paxmeal.push({
                                            meal: tempmeal,
                                            from: tempsegments.segInfo.flData.boardPoint,
                                            to: tempsegments.segInfo.flData.offpoint
                                        });
                                    }
                                });
                                paxfareinf.mealTotal = paxfareinf.paxmeal.reduce(function (accum, cur) {
                                    return parseFloat(accum) + parseFloat(cur.meal.meal.reduce(function (a, e) {
                                        return parseFloat(a) + parseFloat(e.price * e.count);
                                    }, parseFloat(0)));
                                }, parseFloat(0))
                            }
                        } catch (err) {

                        }
                    });
                    $.each(self.Baggagepaxarr, function (baggArryIndex, baggArryItem) {
                        try {
                            var tempbag = baggArryItem.baggageData.filter(obj => (obj.paxCode === PaxFareData.travelerRefNumber) || (obj.paxType + obj.paxCode.match(/\d/g)[0] === PaxFareData.travelerRefNumber) || (obj.passengerType + "-" + obj.paxCode.match(/\d/g)[0] === PaxFareData.travelerRefNumber))[0];
                            if (!stringIsNullorEmpty(tempbag)) {
                                paxfareinf.havebaggage = true;
                                var tempsegments = selectResponseObj.content.fareInformationWithoutPnrReply.airSegments.filter(obj => obj.segInfo.flData.boardPoint == baggArryItem.fromAP && obj.segInfo.flData.offpoint == baggArryItem.toAP)[0];
                                $.each(baggArryItem.baggageData, function (bagDataIndex, bagDataItem) {
                                    if (bagDataIndex === 0) {
                                        paxfareinf.paxbag.push({
                                            legIndex: baggArryItem.legIndex,
                                            Bag: tempbag,
                                            from: tempsegments.segInfo.flData.boardPoint,
                                            to: tempsegments.segInfo.flData.offpoint
                                        });
                                    }
                                });
                                var paxbagNew = [];
                                for (var m = 0; m < self.BaggageInfo.length; m++) {
                                    var paxInf = paxfareinf.paxbag.filter(function (e) {
                                        return self.BaggageInfo[m].index == e.legIndex;
                                    })[0];
                                    if (paxInf) {
                                        paxbagNew.push({
                                            from: self.BaggageInfo[m].From,
                                            to: self.BaggageInfo[m].To,
                                            Bag: paxInf.Bag
                                        });
                                    }
                                }
                                paxfareinf.paxbagNew = paxbagNew;
                                paxfareinf.bagTotal = paxfareinf.paxbagNew.reduce(function (accum, arr) {
                                    return parseFloat(accum) + parseFloat(arr.Bag.Price);
                                }, parseFloat(0))
                            }
                        } catch (err) {

                        }
                    });

                    paxfareinf.paxTotalAncillary = parseFloat(paxfareinf.seatTotal) + parseFloat(paxfareinf.bagTotal) + parseFloat(paxfareinf.mealTotal);
                    paxFareDetails.push({
                        paxfareinf
                    });

                }

            });
            self.paxAncillarySummary = JSON.parse(JSON.stringify(paxFareDetails));
            // var totalAncillary = self.paxAncillarySummary.reduce(function (a, c) { return a + c.paxfareinf.paxTotalAncillary }, parseFloat(0));
            // if (self.showAncillaryFarePricediv == false) {
            // if (totalAncillary == 0 && self.showAncillaryFarePricediv == false) {
            // self.showAncillaryFarePricedivPopUp = false;
            // } else {
            self.showAncillaryFarePricedivPopUp = true;
            // }
        },
        clearextraServices: function (item) {
            var self = this;
            switch (item) {
                case "Baggage":
                    self.Baggagepaxarr = [];
                    self.BagaggeExtra = [];
                    self.prevBaggage = [];
                    self.BaggageInfo = [];
                    $('#baggage_popup').modal('hide');
                    break;
                case "Seat":
                    self.seatRequest = [];
                    self.TotalselectedPax = 0;
                    self.SeatMapArry = [];
                    self.SeatExtra = [];
                    $('#seatmap').modal('hide');
                    break;
                case "Meal":
                    self.MealPaxArr = [];
                    self.mealRequest = [];
                    self.MealsExtra = [];
                    $('#addmeals').modal('hide');
                    break;

            }
        },
        sendAncillaryPriceQuate: function (itemid) {
            var vm = this;
            if (vm.SupplierId == "86") {
                let seatTravellerCount  = 0;
                let mealTravellerCount  = 0;
                let seatCount = 0 ;
                let mealCount = 0 ;
                let seat = [] ;
                let meal = [] ;
                var validationSeat = vm.setValidationSeat();
                var validationMeals = vm.setValidationMeals();
                validationSeat.forEach(vSeat => {
                    seatTravellerCount += vSeat.travallers.length
                    for (let obj of vm.SeatExtra) {
                        if(obj.segRph == vSeat.RPH){
                            seat.push(obj);
                        }
                    }
                });
                validationMeals.forEach(vMeals => {
                    mealTravellerCount += vMeals.travallers.length
                    for (let obj of vm.MealsExtra) {
                        if(obj.segRph == vMeals.RPH){
                            meal.push(obj);
                        }
                    }
                });
                seat.forEach(seatItem => {
                    seatCount += seatItem.seatinfo.length
                })
                meal.forEach(mealItem => {
                    mealCount += mealItem.mealData.length
                })
                if(seatCount != seatTravellerCount && vm.ancillaryServicesList[0].show == true && vm.isSeatOk == false){
                    alertify.confirm('Confirm', 'You have not selected free seats for all passengers.Do you want to Continue?', function(){ 
                        vm.responseHasZeroValueSeat = false;
                        vm.isSeatOk = true;
                        vm.sendAncillary(itemid);    
                    }
                    ,function(){  $("#paid_services").modal({
                        backdrop: 'static',
                        keyboard: false,
                        show: true
                    }) });
                    
                }
                else if(mealCount != mealTravellerCount && vm.ancillaryServicesList[2].show == true && vm.isMealOk == false){
                    alertify.confirm('Confirm', 'You have not selected free meals for all passengers.Do you want to Continue?', function(){ 
                        vm.responseHasZeroValueMeal = false;
                        vm.isMealOk = true;
                        vm.sendAncillary(itemid);
                    }
                    ,function(){  $("#paid_services").modal({
                        backdrop: 'static',
                        keyboard: false,
                        show: true
                    }) });
                    
                }
                else{
                    vm.responseHasZeroValueMeal = false;
                    vm.responseHasZeroValueSeat = false;
                    vm.sendAncillary(itemid);
                }
            }
            else{
                vm.sendAncillary(itemid);
            }
        },
        sendAncillary:function(itemid){
            var vm = this;
            if (itemid == "baggage_popup") {
                vm.ancilleryLoadingBaggage = true;
            }
            if (itemid == "seatmap") {
                vm.ancilleryLoadingSeat = true;
            }
            if (itemid == "addmeals") {
                vm.ancilleryLoadingMeal = true;
            }
            // if (vm.SupplierId != "54") {
            var AncilleryRequest = vm.generateAncillaryPriceQuateReq();
            console.log(JSON.stringify(AncilleryRequest));
            var postDta = AncilleryRequest;
            var huburl = vm.commonStore.hubUrls.hubConnection.baseUrl;
            var portno = vm.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = vm.commonStore.hubUrls.hubConnection.hubServices.flights.ancillaryPriceQuote;

            var config = {
                axiosConfig: {
                    method: "POST",
                    url: huburl + portno + serviceUrl,
                    data: postDta
                },
                successCallback: function (response) {
                    console.log("AncillaryPriceQuote RESPONSE RECEIVED: ", response);
                    vm.setAncillaryPriceQuate(response);
                    localStorage.accessToken = response.headers.access_token;
                    vm.hasAncilleryResponse = true;
                    if (itemid == "baggage_popup") {
                        vm.ancilleryLoadingBaggage = false;
                    }
                    if (itemid == "seatmap") {
                        vm.ancilleryLoadingSeat = false;
                    }
                    if (itemid == "addmeals") {
                        vm.ancilleryLoadingMeal = false;
                    }
                    if (vm.SupplierId == "54") {
                        localStorage.supplierSpecific = JSON.stringify({
                            "supplierSpecific": response.data.response.content.supplierSpecific
                        });
                    }
                    vm.responseHasZeroValueBaggage = false;
                    if (vm.SupplierId != "86") {
                        vm.responseHasZeroValueMeal = false;
                        vm.responseHasZeroValueSeat = false;
                    }
                    // vm.responseHasZeroValueMeal = false;
                    // vm.responseHasZeroValueSeat = false;
                    $("#" + itemid).modal('hide');
                    $("#paid_services").modal('hide');

                },
                errorCallback: function (error) {
                    if (itemid == "baggage_popup") {
                        vm.ancilleryLoadingBaggage = false;
                    }
                    if (itemid == "seatmap") {
                        vm.ancilleryLoadingSeat = false;
                    }
                    if (itemid == "addmeals") {
                        vm.ancilleryLoadingMeal = false;
                    }
                },
                showAlert: true
            }

            mainAxiosRequest(config)

            // }
            // else {
            //     vm.GetpaxwithAncillery();
            // }
            $("#" + itemid).modal('hide');
            $("#paid_services").modal('hide');
        },
        setValidationSeat(){
            var vm = this;
            if (vm.SeatMapArry.length == 0) {
                vm.SetAncillerySeats();
            }
            let seatObj;
            for (let j = 0; j < vm.SeatMapArry.length; j++) {
                for (let k = 0; k < vm.SeatMapArry[j].Row.length; k++) {
                    for (let l = 0; l < vm.SeatMapArry[j].Row[k].rowDetails.seatOccupationDetails.length; l++) {
                        for (let m = 0; m < vm.SeatMapArry[j].Row[k].rowDetails.seatOccupationDetails[l].seatCharacteristicDetails.length; m++) {
                            seatObj = vm.SeatMapArry.filter(e => e.Row[k].rowDetails.seatOccupationDetails[l].seatCharacteristicDetails[m].totalFare == 0 );
                        }
                        
                    }
                }
            }
            return seatObj;
        },
        setValidationMeals(){
            var vm = this;
            if (vm.MealPaxArr.length == 0) {
                vm.SetAncilleryMeals();
            }
            let mealsObj;
            mealsObj = vm.MealPaxArr.filter(function (mealPaxArrItem) {
                return mealPaxArrItem.Meals.some( function (mealsItem) {
                    return mealsItem.mealCharge === 0
                });
            });
            return mealsObj;
        },
        generateAncillaryPriceQuateReq: function () {
            var self = this;
            var airSupplierResponse = selectResponseObj.content;
            var airResposneLegs = airSupplierResponse.fareInformationWithoutPnrReply.airSegments;
            var airSectoridies = _.pluck(airResposneLegs, 'segDirectionID');
            airSectoridies = _.uniq(airSectoridies);
            var airLineSegments = [];
            for (var i = 0; i < airSectoridies.length; i++) {
                var segments = airResposneLegs.filter(function (segment) {
                    return segment.segDirectionID == airSectoridies[i]
                });
                $.each(segments, function (segIndex, segment) {
                    var airLineSegment = {
                        segmentInformation: {
                            flightDate: {
                                departureDate: segment.segInfo.dateandTimeDetails.departureDate,
                                departureTime: segment.segInfo.dateandTimeDetails.departureTime,
                                arrivalDate: segment.segInfo.dateandTimeDetails.arrivalDate,
                                arrivalTime: segment.segInfo.dateandTimeDetails.arrivalTime
                            },
                            fareId: segment.fareId,
                            boardPointDetails: {
                                trueLocationId: segment.segInfo.flData.boardPoint
                            },
                            offPointDetails: {
                                trueLocationId: segment.segInfo.flData.offpoint
                            },
                            companyDetails: {
                                marketingCompany: segment.segInfo.flData.marketingCompany,
                                operatingCompany: segment.segInfo.flData.operatingCompany
                            },
                            flightIdentification: {
                                flightNumber: segment.segInfo.flData.flightNumber,
                                bookingClass: !stringIsNullorEmpty(segment.segInfo.flData.bookingClass) ? segment.segInfo.flData.bookingClass : segment.segInfo.flData.rbd,
                            },
                            flightTypeDetails: {
                                // flightIndicator: 1,
                                // itemNumber: 1

                                "flightIndicator": String(i + 1),
                                "itemNumber": segIndex + 1
                            }
                        }

                    };
                    airLineSegments.push(airLineSegment);
                });

            }

            var selectCred = null;
            try {
                selectCred = selectResponseObj.selectCredential;
            } catch (err) {}
            var supplierSpecific = {
                cookie: airResposneLegs[0].fareId
            };
            var sessionId = airSupplierResponse.fareInformationWithoutPnrReply.fareSessionId;
            if (self.SupplierId == "54" || self.SupplierId == "69" || self.SupplierId == "86") {
                var tempsupplierSpecific = JSON.parse(localStorage.supplierSpecific);
                supplierSpecific = tempsupplierSpecific.supplierSpecific;
                sessionId = self.GetpaxwithAncillery();
            }
            if (self.SupplierId == "71" || self.SupplierId == "49" || self.SupplierId == "50" || self.SupplierId == "44") {
                var tempsupplierSpecific = JSON.parse(localStorage.supplierSpecific);
                supplierSpecific = tempsupplierSpecific.supplierSpecific;
            }
            var AncillaryPriceQuateReq = {
                request: {
                    service: "FlightRQ",
                    token: localStorage.accessToken,
                    supplierCodes: [airSupplierResponse.fareInformationWithoutPnrReply.fSupplier],
                    selectCredential: selectCred,
                    content: {
                        command: "FlightSelectAncillaryRQ",
                        supplierSpecific: supplierSpecific,
                        commonRequestFarePricer: {
                            body: {
                                airRevalidate: {
                                    agencyCode: airSupplierResponse.fareInformationWithoutPnrReply.fareAgencyCode,
                                    fareSourceCode: airSupplierResponse.fareInformationWithoutPnrReply.fareFareSourceCode,
                                    sessionId: sessionId,
                                    adt: airSupplierResponse.fareInformationWithoutPnrReply.passengerTypeQuantity.adt,
                                    chd: airSupplierResponse.fareInformationWithoutPnrReply.passengerTypeQuantity.chd,
                                    inf: airSupplierResponse.fareInformationWithoutPnrReply.passengerTypeQuantity.inf,
                                    segmentGroup: airLineSegments,
                                    specialReqDetails: {
                                        baggageRequests: (self.Baggagepaxarr.length > 0) ? {
                                            baggageRequest: self.Baggagepaxarr
                                        } : null,
                                        mealRequests: (self.mealRequest.length > 0) ? {
                                            mealRequest: self.mealRequest
                                        } : null,
                                        seatRequests: (self.seatRequest.length > 0) ? {
                                            seatRequest: self.seatRequest
                                        } : null
                                    },
                                    tripType: airSupplierResponse.fareInformationWithoutPnrReply.tripType
                                }
                            }
                        }

                    },

                }
            };
            return AncillaryPriceQuateReq;
        },
        SetAncillerySeats: function () {
            var self = this;
            var isValidate = false;
            if (self.SupplierId == "54") {
                isValidate = validateEntries('seat');
            } else {
                isValidate = validateEntriesforancillery();
            }
            var tempSeatMapArray = [];
            if (isValidate) {
                if (self.travellers.length == 0) {
                    self.travellers = self.getpaxinfo();
                }

                for (var i = 0; i < self.seatmapInformation.length; i++) {
                    var index = self.seatmapInformation[i].index;
                    var from = self.seatmapInformation[i].flightDateInformation.boardPointDetails.trueLocationId;
                    var to = self.seatmapInformation[i].flightDateInformation.offpointDetails.trueLocationId;
                    var Company = self.seatmapInformation[i].flightDateInformation.companyDetails.marketingCompany;
                    var flno = self.seatmapInformation[i].flightDateInformation.flightIdentification.flightNumber;
                    var rph = self.seatmapInformation[i].equipmentInformation.iataAircraftTypeCode;
                    var status = false;
                    var amount = 0;
                    var Row = [];
                    if (self.seatmapInformation[i].hasOwnProperty('row')) {
                        status = true;
                        Row = self.seatmapInformation[i].row
                    }
                    var travallers = JSON.parse(JSON.stringify(self.travellers));
                    for (var k = 0; k < self.SeatMapArry.length; k++) {
                        var oldSeatItem = self.SeatMapArry[k];
                        if (oldSeatItem.from == from && oldSeatItem.to == to && oldSeatItem.flno == flno) {
                            amount = oldSeatItem.amount;
                            travallers = JSON.parse(JSON.stringify(oldSeatItem.travallers));
                            break;
                        }
                    }
                    for (let rowObj of Row) {
                        if(rowObj.rowDetails.seatOccupationDetails.length < 6){
                            rowObj.rowDetails.seatOccupationDetails = self.processSeatRow(rowObj.rowDetails.seatOccupationDetails);
                        }
                    }
                    tempSeatMapArray.push({
                        index: index,
                        from: from,
                        to: to,
                        Company: Company,
                        flno: flno,
                        status: status,
                        Row: Row,
                        amount: amount,
                        RPH: rph,
                        travallers: travallers
                    });
                }

                // $("#seatmap").modal({ backdrop: 'static', keyboard: false, show: true })

                setTimeout(function () {
                    $('[data-toggle="tooltip"]').tooltip();
                }, 1000);


            }
            self.SeatMapArry = tempSeatMapArray;
            this.hideSeat = this.SeatMapArry[0].status;

        },
        processSeatRow(row){
            let newSeatColumnMapping =[];
            let seatColumns = ['A','B','C','D','E','F']
            let emptySeatColumn ={
                seatColumn: "",
                seatOccupation: "",
                seatCharacteristic: [],
                seatCharacteristicDetails: [
                    {
                        totalFare: "0.00",
                        sealedCode: "",
                        baseFare: "0.00",
                        tax: "0.00"
                    }
                ],
                currencyCode: ""
            }
            for (let columnLabel of seatColumns) {
                let columnObj = _.find(row, function (sc) {
                    return sc.seatColumn == columnLabel;
                })
                if (columnObj) {
                    newSeatColumnMapping.push(columnObj);
                } else{
                    newSeatColumnMapping.push(emptySeatColumn);
                }
            }
            return newSeatColumnMapping;
        },
        ChooseSeat: function (indexno, rownumber, Seat, seatOccupation, RPH) {
            var self = this;
            var hashCode = "";
            if (Seat.hasOwnProperty('seatCharacteristic')) {
                if (Seat.seatCharacteristic.length > 0) {
                    hashCode = Seat.seatCharacteristic[0];
                }
            }

            if (seatOccupation == 'F') {
                if (self.TotalselectedPax < self.TotalPaxCount) {
                    var temppaxno = 0
                    if (self.Choosepaxflag) {
                        temppaxno = self.choosepax.index;
                        self.choosepax.SeatCharge = Seat.seatCharacteristicDetails[0].totalFare;
                        self.Choosepaxflag = false;
                    } else {
                        if (self.seatRequest.length > 0) {
                            self.seatRequest.some(function (el, i) {
                                if (el.flightRefNumberRPHList === RPH) {
                                    $.each(self.travellers, function (index, pax) {
                                        if (el.seatData.find(x => x.paxCode === pax.paxcode)) {} else {
                                            temppaxno = pax.index;
                                            return false;
                                        }
                                    })

                                }
                            });
                        }

                    }
                    var temPaxcode = self.travellers[temppaxno].paxcode;

                    $.each(self.SeatMapArry[indexno].travallers, function (chind, chpax) {
                        if (chpax.paxcode == temPaxcode) {
                            chpax.active = true;
                            chpax.SeatCharge = Seat.seatCharacteristicDetails[0].totalFare;
                            chpax.SeatCode = rownumber + Seat.seatColumn;
                        } else {
                            chpax.active = false
                        }

                    });
                    self.SeatMapArry[indexno].amount = self.SeatMapArry[indexno].travallers.map(amount => parseFloat(amount.SeatCharge)).reduce((acc, amount) => amount + acc);
                    if (self.seatRequest.length > 0) {
                        var foundleg = self.seatRequest.some(function (el, i) {
                            if (el.flightRefNumberRPHList === RPH) {
                                var foundSeat = el.seatData.some(function (seatItem, ind) {
                                    if (seatItem.paxCode === temPaxcode) {
                                        self.seatRequest[i].seatData[ind].seatCode = rownumber + Seat.seatColumn;
                                        self.SeatExtra[i].seatinfo[ind].seat.seatno = rownumber + Seat.seatColumn;
                                        self.SeatExtra[i].seatinfo[ind].seat.Price = Seat.seatCharacteristicDetails[0].totalFare;
                                        self.SeatExtra[i].seatinfo[ind].seat.currency = Seat.currencyCode;
                                        self.SeatExtra[i].seatinfo[ind].seat.code = rownumber + Seat.seatColumn;
                                        self.SeatExtra[i].seatinfo[ind].seat.hashCode = hashCode;
                                        self.SeatExtra[i].seatinfo[ind].seat.sealedCode = Seat.seatCharacteristicDetails[0].sealedCode;
                                        self.seatRequest[i].seatData[ind].sealedCode = Seat.seatCharacteristicDetails[0].sealedCode;
                                        return true;
                                    }
                                });
                                if (!foundSeat) {
                                    self.seatRequest[i].seatData.push({
                                        sealedCode: Seat.seatCharacteristicDetails[0].sealedCode,
                                        seatCode: rownumber + Seat.seatColumn,
                                        paxCode: temPaxcode,
                                        hashCode: hashCode
                                    });
                                    self.SeatExtra[i].seatinfo.push({
                                        paxid: temppaxno,
                                        paxcode: temPaxcode,
                                        seat: {
                                            sealedCode: Seat.seatCharacteristicDetails[0].sealedCode,
                                            seatno: rownumber + Seat.seatColumn,
                                            Price: Seat.seatCharacteristicDetails[0].totalFare,
                                            currency: Seat.currencyCode,
                                            code: rownumber + Seat.seatColumn,
                                            hashCode: hashCode
                                        }
                                    });
                                }
                                return true
                            }

                        });
                        if (!foundleg) {
                            self.seatRequest.push({
                                flightRefNumberRPHList: RPH,
                                seatData: [{
                                    sealedCode: Seat.seatCharacteristicDetails[0].sealedCode,
                                    seatCode: rownumber + Seat.seatColumn,
                                    paxCode: temPaxcode,
                                    hashCode: hashCode
                                }]
                            });
                            self.SeatExtra.push({
                                segment: indexno,
                                segRph: RPH,
                                seatinfo: [{
                                    paxid: temppaxno,
                                    paxcode: temPaxcode,
                                    seat: {
                                        sealedCode: Seat.seatCharacteristicDetails[0].sealedCode,
                                        seatno: rownumber + Seat.seatColumn,
                                        Price: Seat.seatCharacteristicDetails[0].totalFare,
                                        currency: Seat.currencyCode,
                                        code: rownumber + Seat.seatColumn,
                                        hashCode: hashCode
                                    }
                                }]
                            });
                        }
                    } else {
                        self.seatRequest.push({
                            flightRefNumberRPHList: RPH,
                            seatData: [{
                                sealedCode: Seat.seatCharacteristicDetails[0].sealedCode,
                                seatCode: rownumber + Seat.seatColumn,
                                paxCode: temPaxcode,
                                hashCode: hashCode
                            }]
                        });
                        self.SeatExtra.push({
                            segment: indexno,
                            segRph: RPH,
                            seatinfo: [{
                                paxid: temppaxno,
                                paxcode: temPaxcode,
                                seat: {
                                    sealedCode: Seat.seatCharacteristicDetails[0].sealedCode,
                                    seatno: rownumber + Seat.seatColumn,
                                    Price: Seat.seatCharacteristicDetails[0].totalFare,
                                    currency: Seat.currencyCode,
                                    code: rownumber + Seat.seatColumn,
                                    hashCode: hashCode
                                }
                            }]
                        });
                    }
                    Seat.seatOccupation = 'S';
                    self.TotalselectedPax++;
                } else {
                    var temppaxcode = null;
                    if (self.Choosepaxflag) {
                        temppaxcode = self.choosepax.paxcode;
                        self.choosepax.SeatCharge = Seat.seatCharacteristicDetails[0].totalFare;
                        self.Choosepaxflag = false;
                    } else {
                        $.each(self.SeatMapArry[indexno].travallers, function (chind, chpax) {
                            //   chpax.active?chind==self.TotalPaxCount - 1?:
                            if (chpax.active) {
                                if (chind != (self.TotalPaxCount - 1)) {
                                    temppaxcode = self.SeatMapArry[indexno].travallers[chind + 1].paxcode;
                                    self.SeatMapArry[indexno].travallers[chind + 1].active = true;
                                    return false;
                                } else {
                                    temppaxcode = self.SeatMapArry[indexno].travallers[0].paxcode;
                                    self.SeatMapArry[indexno].travallers[0].active = true;
                                }
                                return false
                            }


                        });
                    }

                    $.each(self.SeatMapArry[indexno].travallers, function (chind, chpax) {
                        if (chpax.paxcode == temppaxcode) {
                            chpax.active = true;
                            chpax.SeatCharge = Seat.seatCharacteristicDetails[0].totalFare;
                            chpax.SeatCode = rownumber + Seat.seatColumn;
                        } else {
                            chpax.active = false
                        }

                    });
                    self.SeatMapArry[indexno].amount = self.SeatMapArry[indexno].travallers.map(amount => parseFloat(amount.SeatCharge)).reduce((acc, amount) => amount + acc);
                    self.seatRequest.some(function (el, i) {
                        if (el.flightRefNumberRPHList === RPH) {
                            var setind = null;
                            var foundSeat = el.seatData.some(function (seatItem, ind) {
                                if (seatItem.paxCode === temppaxcode) {
                                    var seatCode = self.seatRequest[i].seatData[ind].seatCode;
                                    var temprownumber = seatCode.match(/\d+/)[0];
                                    var setpos = seatCode.replace(/[0-9]/g, '');
                                    self.SeatMapArry[indexno].Row.some(function (rowirtem, indexroe) {
                                        if (rowirtem.rowDetails.seatRowNumber == temprownumber) {
                                            $.each(rowirtem.rowDetails.seatOccupationDetails, function (sindex, rowset) {
                                                if (rowset.seatColumn == setpos) {
                                                    self.SeatMapArry[indexno].Row[indexroe].rowDetails.seatOccupationDetails[sindex].seatOccupation = "F";
                                                    return false;
                                                }
                                            });
                                        }
                                    });
                                    setind = ind;
                                    return true;
                                }
                            });
                            if (foundSeat) {
                                self.seatRequest[i].seatData[setind].seatCode = rownumber + Seat.seatColumn;
                                self.SeatExtra[i].seatinfo[setind].seat.seatno = rownumber + Seat.seatColumn;
                                self.SeatExtra[i].seatinfo[setind].seat.Price = Seat.seatCharacteristicDetails[0].totalFare;
                                self.SeatExtra[i].seatinfo[setind].seat.currency = Seat.currencyCode;
                                self.SeatExtra[i].seatinfo[setind].seat.code = rownumber + Seat.seatColumn;
                                self.SeatExtra[i].seatinfo[setind].seat.hashCode = hashCode;
                                self.SeatExtra[i].seatinfo[setind].seat.sealedCode = Seat.seatCharacteristicDetails[0].sealedCode;
                                self.seatRequest[i].seatData[setind].sealedCode = Seat.seatCharacteristicDetails[0].sealedCode;
                            }

                        }

                    });
                    Seat.seatOccupation = 'S';
                }
            } else if (seatOccupation == 'S') {
                self.seatRequest.some(function (el, i) {
                    if (el.flightRefNumberRPHList === RPH) {
                        var tempseatcode = rownumber + Seat.seatColumn;
                        var seatindex;
                        var foundSeat = el.seatData.some(function (seatItem, ind) {
                            if (seatItem.seatCode === tempseatcode) {
                                var temPaxcode = seatItem.paxCode;
                                self.SeatMapArry[indexno].travallers.some(function (paxitem, paxi) {
                                    if (paxitem.paxcode === temPaxcode) {
                                        self.TotalseatPrice = Number(self.TotalseatPrice) - Number(paxitem.SeatCharge);
                                        self.SeatMapArry[indexno].amount = Number(self.SeatMapArry[indexno].amount) - Number(paxitem.SeatCharge);
                                        paxitem.SeatCharge = 0;
                                        paxitem.SeatCode = '';
                                        paxitem.active = false;

                                    }
                                });
                                seatindex = ind;
                                return true
                            }
                        });
                        if (foundSeat) {
                            el.seatData.splice(seatindex, 1);
                            self.SeatExtra[i].seatinfo.splice(seatindex, 1);
                        }
                        if (el.seatData.length == 0) {
                            self.seatRequest.splice(i, 1);
                            self.SeatExtra.splice(i, 1);
                        }

                    }
                    Seat.seatOccupation = 'F';
                    self.TotalselectedPax--;
                });
            } else {

            }
            if (seatOccupation == 'F' || seatOccupation == 'S') {
                self.workingonseat = !self.workingonseat;
                this.setPaxAncillaryPriceSummary();
            }
        },
        SelectPaxforSeat: function (event, Pax, status, seatindex) {
            if (status) {
                var self = this;
                Pax.active = true;
                self.Choosepaxflag = true;
                self.choosepax = Pax;
                var temPaxcode = Pax.paxcode;
                $.each(self.SeatMapArry[seatindex].travallers, function (chind, chpax) {
                    if (chpax.paxcode == temPaxcode) {

                    } else {
                        chpax.active = false
                    }

                });
            }
        },
        chooseseatleg: function (RPH) {
            var self = this;
            if (self.seatRequest.length > 0) {
                self.seatRequest.some(function (el, i) {
                    if (el.flightRefNumberRPHList === RPH) {
                        if (el.seatData.length > 0) {
                            self.TotalselectedPax = el.seatData.length;
                        } else {
                            self.TotalselectedPax = 0;
                        }

                    } else {
                        self.TotalselectedPax = 0;
                    }


                });
            }
        },
        deletePaxSeat: function (RPH, pax, indexno) {
            var self = this;
            self.seatRequest.some(function (el, i) {
                if (el.flightRefNumberRPHList === RPH) {
                    var tempseatcode = pax.SeatCode;
                    var seatindex;
                    var foundSeat = el.seatData.some(function (seatItem, ind) {
                        if (seatItem.seatCode === tempseatcode) {
                            var temPaxcode = seatItem.paxCode;
                            self.SeatMapArry[indexno].travallers.some(function (paxitem, paxi) {
                                if (paxitem.paxcode === temPaxcode) {
                                    self.TotalseatPrice = Number(self.TotalseatPrice) - Number(paxitem.SeatCharge);
                                    self.SeatMapArry[indexno].amount = Number(self.SeatMapArry[indexno].amount) - Number(paxitem.SeatCharge);
                                    paxitem.SeatCharge = 0;
                                    paxitem.SeatCode = '';
                                    paxitem.active = false;

                                }
                            });
                            var temprownumber = tempseatcode.match(/\d+/)[0];
                            var setpos = tempseatcode.replace(/[0-9]/g, '');
                            self.SeatMapArry[indexno].Row.some(function (rowirtem, indexroe) {
                                if (rowirtem.rowDetails.seatRowNumber == temprownumber) {
                                    $.each(rowirtem.rowDetails.seatOccupationDetails, function (sindex, rowset) {
                                        if (rowset.seatColumn == setpos) {
                                            self.SeatMapArry[indexno].Row[indexroe].rowDetails.seatOccupationDetails[sindex].seatOccupation = "F";
                                            return false;
                                        }
                                    });
                                }
                            });
                            seatindex = ind;
                            return true
                        }
                    });
                    if (foundSeat) {
                        el.seatData.splice(seatindex, 1);
                    }
                    if (el.seatData.length == 0) {
                        self.seatRequest.splice(i, 1);
                    }

                }
                // Seat.seatOccupation = 'F';
                self.TotalselectedPax--;
            });
        },
        SetAncilleryMeals: function () {
            var self = this;
            var isValidate = false;
            if (self.SupplierId == "54") {
                isValidate = validateEntries('meal');
            } else {
                isValidate = validateEntriesforancillery();
            }

            var segments = []
            var groupOfFlights = self.selectFlightFromSession.selectedFlightInfo.groupOfFlights;
            for (var i = 0; i < groupOfFlights.length; i++) {
                for (var j = 0; j < groupOfFlights[i].flightDetails.length; j++) {
                    segments.push({
                        segDirectionID: groupOfFlights[i].segDirectionID,
                        category: groupOfFlights[i].flightDetails[j].fInfo.selectedCategory
                    })
                }
            }

            //self.MealPaxArr = [];
            if (isValidate) {
                if (self.travellers.length == 0) {
                    self.travellers = self.getpaxinfo();
                }

                if (self.SupplierId == "69" || self.SupplierId == "86") {

                    // create a map for al jazeera
                    var newMealResponse = [];
                    var tempLeg = [];
                    for (var x = 0; x < self.mealDetailsResponses.length; x++) {
                        var mealExist = tempLeg.some(function (e) {
                            return e == self.mealDetailsResponses[x].mealIdentifier.segmentCode;
                        });
                        if (!mealExist) {
                            newMealResponse.push(self.mealDetailsResponses[x]);
                            tempLeg.push(self.mealDetailsResponses[x].mealIdentifier.segmentCode);
                        }
                    }
                    self.oldMeal = JSON.parse(JSON.stringify(self.mealDetailsResponses));
                    self.mealDetailsResponses = newMealResponse;
                }
                if (self.MealPaxArr.length == 0) {
                    $.each(self.mealDetailsResponses, function (mlindex, Meal) {
                        var index = Meal.index;
                        var from = Meal.flightSegmentInfo.departureAirport.locationCode;
                        var to = Meal.flightSegmentInfo.arrivalAirport.locationCode;
                        var flno = Meal.flightSegmentInfo.flightNumber;
                        var rph = Meal.flightSegmentInfo.rph;
                        if (rph == undefined && self.SupplierId == "69" || self.SupplierId == "86") {
                            rph = Meal.flightSegmentInfo.segmentCode;
                        }
                        var departureDate = Meal.flightSegmentInfo.departureDate;
                        var departureTime = Meal.flightSegmentInfo.departureTime;
                        var status = false;
                        var amount = 0;
                        var TempMeals = [];
                        var MealCategories = [];
                        if (Meal.hasOwnProperty('meal')) {
                            if (Meal.meal.length > 0) {
                                status = true;
                                MealCategories = Meal.meal.map(item => item.mealCategoryCode)
                                    .filter((value, index, selfi) => selfi.indexOf(value) === index);

                                if (self.SupplierId == "54") {
                                    TempMeals = Meal.meal.filter(function (item) {
                                        return item.mealCode != "IFPP";
                                    });
                                } else {
                                    TempMeals = Meal.meal;
                                }
                            }
                        }
                        var travallers = JSON.parse(JSON.stringify(self.travellers));
                        self.MealPaxArr.push({
                            index: index,
                            from: from,
                            to: to,
                            flno: flno,
                            status: status,
                            Meals: TempMeals,
                            amount: amount,
                            RPH: rph,
                            departureDate: departureDate,
                            departureTime: departureTime,
                            travallers: travallers,
                            MealCategories: MealCategories,
                            categoryCode: segments[mlindex].category ? segments[mlindex].category.fbCode : ""
                        });
                    });
                }
                // $("#addmeals").modal({ backdrop: 'static', keyboard: false, show: true })  

                setTimeout(function () {
                    SelectstyleOption("sort");
                    SelectstyleOption("customselect");
                }, 1000);
            }
        },

        addthismeal: function (item, classItem) {
            var clickedElement = item.currentTarget;
            var tempid = $(clickedElement).attr("data-id");
            if(this.SupplierId !='86'){
                $(".meal_count").not('#' + tempid).hide();
            }
            $("#" + tempid).toggle();
            var valueArray = $(".count_meal-" + classItem).map(function () {
                return parseInt(this.value);
            }).get().reduce(function (a, c) {
                return a + c;
            }, 0);
            if (valueArray == 0 && $(".div-" + classItem).is(":visible")) {
                $(".div-" + classItem).click();
            }
        },
        selectMeal: function (item, paxinfo, index, Selectmealitem, departureDate, departureTime, RPH, flno, type) {
            var self = this;
            var tempcount = 0;
            var hashCode = null;
            var ruleId = null;
            if (Selectmealitem.hasOwnProperty('hashCode')) {
                hashCode = Selectmealitem.hashCode;
            }
            if (Selectmealitem.hasOwnProperty('ruleId')) {
                ruleId = Selectmealitem.ruleId;
            }
            var mealPaxItem = self.MealPaxArr[index];

            var clickedElement = item.currentTarget;
            if (type == 1) {
                var continueAdd = true;
                var freePaxMeal = false
                try {
                    var selectedPaxMeal = mealPaxItem.travallers.find(element =>
                        (element.index == paxinfo.index));
                    if (selectedPaxMeal && selectedPaxMeal.Meals.length) {
                        var item = selectedPaxMeal.Meals.find(element => (element.mealCharge == 0));
                        if (item) {
                            freePaxMeal = true
                        }
                    }
                } catch (error) {
                    console.log(error)
                }
                if (Selectmealitem.mealCharge == 0 && freePaxMeal) {
                    // if (Selectmealitem.mealCharge == 0 && Selectmealitem.mealQuantity == 1) {
                    // one free meal max
                    continueAdd = false;
                }
                if(self.SupplierId == "86" && paxinfo.Meals.length > 0){
                    continueAdd = false;

                }
                // else if (self.mealRequest[index] && self.mealRequest[index].mealData.length > 0 && freePaxMeal) {
                //     continueAdd = false;
                // }
                if (continueAdd) {
                    tempcount = 1;
                    if (self.mealRequest.length > 0) {
                        self.mealRequest.some(function (el, i) {
                            if (el.flightRefNumberRPHList === RPH) {
                                var foundpax = el.mealData.some(function (Paxitem, ind) {
                                    if (Paxitem.paxCode === paxinfo.paxcode) {
                                        var fuondMeal = Paxitem.selectedMeals.some(function (tempmeal, mealind) {
                                            if (tempmeal.mealCode == Selectmealitem.mealCode) {
                                                var tempmealcount = tempmeal.mealQuantity;
                                                if (tempmealcount < Selectmealitem.availableMeals) {
                                                    self.mealRequest[i].mealData[ind].selectedMeals[mealind].mealQuantity = tempmealcount + 1;
                                                    paxinfo.Meals.some(function (paxm, indmpx) {
                                                        if (paxm.mealCode == Selectmealitem.mealCode) {
                                                            paxm.mealQuantity = tempmealcount + 1;
                                                        }
                                                    });
                                                    tempcount = tempmealcount + 1;
                                                    self.MealsExtra[i].mealData[ind].meal[mealind].count = tempcount;
                                                    self.MealsExtra[i].mealData[ind].meal[mealind].sealedCode = Selectmealitem.sealedCode;
                                                    self.MealsExtra[i].mealData[ind].meal[mealind].hashCode = Selectmealitem.hashCode;
                                                    self.mealRequest[i].mealData[ind].selectedMeals[mealind].sealedCode = Selectmealitem.sealedCode;
                                                    self.mealRequest[i].mealData[ind].selectedMeals[mealind].hashCode = Selectmealitem.hashCode;
                                                }
                                                return true;
                                            }

                                        });
                                        if (!fuondMeal) {
                                            if (Selectmealitem.availableMeals > 1 || self.SupplierId == "54") {
                                                self.mealRequest[i].mealData[ind].selectedMeals.push({
                                                    hashCode: Selectmealitem.hashCode,
                                                    sealedCode: Selectmealitem.sealedCode,
                                                    mealCode: Selectmealitem.mealCode,
                                                    mealQuantity: 1
                                                });
                                                paxinfo.Meals.push({
                                                    hashCode: Selectmealitem.hashCode,
                                                    mealCode: Selectmealitem.mealCode,
                                                    mealName: Selectmealitem.mealName,
                                                    mealQuantity: 1,
                                                    mealCharge: Selectmealitem.mealCharge,
                                                    currencyCode: Selectmealitem.currencyCode
                                                });
                                                self.MealsExtra[i].mealData[ind].meal.push({
                                                    hashCode: Selectmealitem.hashCode,
                                                    sealedCode: Selectmealitem.sealedCode,
                                                    meal: Selectmealitem.mealName,
                                                    count: 1,
                                                    price: Selectmealitem.mealCharge.toString(),
                                                    currency: Selectmealitem.currencyCode,
                                                    code: Selectmealitem.mealCode,
                                                    hashCode: hashCode,
                                                    ruleId: ruleId
                                                });
                                            }
                                        }
                                        return true;
                                    }
                                });
                                if (!foundpax) {
                                    self.mealRequest[i].mealData.push({
                                        paxCode: paxinfo.paxcode,
                                        selectedMeals: [{
                                            hashCode: Selectmealitem.hashCode,
                                            sealedCode: Selectmealitem.sealedCode,
                                            mealCode: Selectmealitem.mealCode,
                                            mealQuantity: 1
                                        }]
                                    });
                                    paxinfo.Meals.push({
                                        hashCode: Selectmealitem.hashCode,
                                        mealCode: Selectmealitem.mealCode,
                                        mealName: Selectmealitem.mealName,
                                        mealQuantity: 1,
                                        mealCharge: Selectmealitem.mealCharge,
                                        currencyCode: Selectmealitem.currencyCode
                                    });
                                    self.MealsExtra[i].mealData.push({
                                        paxType: paxinfo.paxType,
                                        paxCode: paxinfo.paxcode.match(/\d/g).join(""),
                                        meal: [{
                                            hashCode: Selectmealitem.hashCode,
                                            sealedCode: Selectmealitem.sealedCode,
                                            meal: Selectmealitem.mealName,
                                            count: 1,
                                            price: Selectmealitem.mealCharge.toString(),
                                            currency: Selectmealitem.currencyCode,
                                            code: Selectmealitem.mealCode,
                                            hashCode: hashCode,
                                            ruleId: ruleId
                                        }]
                                    });
                                }
                            } else {
                                if (!_.findWhere(self.mealRequest, {
                                        flightRefNumberRPHList: RPH
                                    })) {
                                    self.mealRequest.push({
                                        flightRefNumberRPHList: RPH,
                                        departureDate: departureDate,
                                        departureTime: departureTime,
                                        FlightNumber: flno,
                                        mealData: [{
                                            paxCode: paxinfo.paxcode,
                                            selectedMeals: [{
                                                hashCode: Selectmealitem.hashCode,
                                                sealedCode: Selectmealitem.sealedCode,
                                                mealCode: Selectmealitem.mealCode,
                                                mealQuantity: 1
                                            }]
                                        }]
                                    });
                                    paxinfo.Meals.push({
                                        hashCode: Selectmealitem.hashCode,
                                        mealCode: Selectmealitem.mealCode,
                                        mealName: Selectmealitem.mealName,
                                        mealQuantity: 1,
                                        mealCharge: Selectmealitem.mealCharge,
                                        currencyCode: Selectmealitem.currencyCode
                                    });
                                    self.MealsExtra.push({
                                        segement: index,
                                        segRph: RPH,
                                        mealData: [{
                                            paxType: paxinfo.paxType,
                                            paxCode: paxinfo.paxcode.match(/\d/g).join(""),
                                            meal: [{
                                                hashCode: Selectmealitem.hashCode,
                                                sealedCode: Selectmealitem.sealedCode,
                                                meal: Selectmealitem.mealName,
                                                count: 1,
                                                price: Selectmealitem.mealCharge.toString(),
                                                currency: Selectmealitem.currencyCode,
                                                code: Selectmealitem.mealCode,
                                                hashCode: hashCode,
                                                ruleId: ruleId
                                            }]
                                        }]
                                    });
                                }
                            }
                        });
                    } else {
                        self.mealRequest.push({
                            flightRefNumberRPHList: RPH,
                            departureDate: departureDate,
                            departureTime: departureTime,
                            FlightNumber: flno,
                            mealData: [{
                                paxCode: paxinfo.paxcode,
                                selectedMeals: [{
                                    hashCode: Selectmealitem.hashCode,
                                    sealedCode: Selectmealitem.sealedCode,
                                    mealCode: Selectmealitem.mealCode,
                                    mealQuantity: 1
                                }]
                            }]
                        });
                        paxinfo.Meals.push({
                            hashCode: Selectmealitem.hashCode,
                            mealCode: Selectmealitem.mealCode,
                            mealName: Selectmealitem.mealName,
                            mealQuantity: 1,
                            mealCharge: Selectmealitem.mealCharge,
                            currencyCode: Selectmealitem.currencyCode
                        });
                        self.MealsExtra.push({
                            segement: index,
                            segRph: RPH,
                            mealData: [{
                                paxType: paxinfo.paxType,
                                paxCode: paxinfo.paxcode.match(/\d/g).join(""),
                                meal: [{
                                    hashCode: Selectmealitem.hashCode,
                                    sealedCode: Selectmealitem.sealedCode,
                                    meal: Selectmealitem.mealName,
                                    count: 1,
                                    price: Selectmealitem.mealCharge.toString(),
                                    currency: Selectmealitem.currencyCode,
                                    code: Selectmealitem.mealCode,
                                    hashCode: hashCode,
                                    ruleId: ruleId
                                }]
                            }]
                        });

                    }
                    $(clickedElement).prev('input').val(tempcount);
                    Selectmealitem.mealQuantity = tempcount;
                }

            } else {
                if (self.mealRequest.length > 0) {
                    self.mealRequest.some(function (el, i) {
                        if (el.flightRefNumberRPHList === RPH) {
                            var foundpax = el.mealData.some(function (Paxitem, ind) {
                                if (Paxitem.paxCode === paxinfo.paxcode) {
                                    var fuondMeal = Paxitem.selectedMeals.some(function (tempmeal, mealind) {
                                        if (tempmeal.mealCode == Selectmealitem.mealCode) {
                                            var tempmealcount = tempmeal.mealQuantity;
                                            if (tempmealcount > 0) {
                                                tempmealcount = tempmealcount - 1;
                                                if (tempmealcount > 0) {
                                                    self.mealRequest[i].mealData[ind].selectedMeals[mealind].mealQuantity = tempmealcount;
                                                    paxinfo.Meals.some(function (paxm, indmpx) {
                                                        if (paxm.mealCode == Selectmealitem.mealCode) {
                                                            paxm.mealQuantity = tempmealcount;
                                                            self.MealsExtra[i].mealData[ind].meal[mealind].count = tempmealcount;
                                                        }
                                                        self.MealsExtra[i].mealData[ind].meal[mealind].sealedCode = Selectmealitem.sealedCode;
                                                        self.mealRequest[i].mealData[ind].selectedMeals[mealind].sealedCode = Selectmealitem.sealedCode;
                                                        self.mealRequest[i].mealData[ind].selectedMeals[mealind].hashCode = Selectmealitem.hashCode;

                                                    });
                                                } else {
                                                    self.mealRequest[i].mealData[ind].selectedMeals.splice(mealind, 1);
                                                    self.MealsExtra[i].mealData[ind].meal.splice(mealind, 1);
                                                    paxinfo.Meals.some(function (paxm, indmpx) {
                                                        if (paxm.mealCode == Selectmealitem.mealCode) {
                                                            paxinfo.Meals.splice(indmpx, 1);
                                                        }
                                                    });
                                                    if (self.mealRequest[i].mealData[ind].selectedMeals.length == 0) {
                                                        self.mealRequest[i].mealData.splice(ind, 1);
                                                        self.MealsExtra[i].mealData.splice(ind, 1);
                                                        if (self.mealRequest[i].mealData.length == 0) {
                                                            self.mealRequest.splice(i, 1);
                                                            self.MealsExtra.splice(i, 1);
                                                        }
                                                    }
                                                }
                                                tempcount = tempmealcount;

                                            }
                                            return true;
                                        }

                                    });

                                    return true;
                                }
                            });

                        }

                    });
                }
                $(clickedElement).next('input').val(tempcount);
                Selectmealitem.mealQuantity = tempcount;

            }
            if (mealPaxItem != undefined) {
                var totalAmount = 0;
                if (mealPaxItem.travallers != undefined) {
                    for (let travelindex = 0; travelindex < mealPaxItem.travallers.length; travelindex++) {
                        const element = mealPaxItem.travallers[travelindex];
                        if (element.Meals != undefined) {
                            for (var mealIndex = 0; mealIndex < element.Meals.length; mealIndex++) {
                                const mealElement = element.Meals[mealIndex];
                                var total = Number(mealElement.mealCharge) * Number(mealElement.mealQuantity);
                                totalAmount = Number(total) + Number(totalAmount);
                            }
                        }


                    }
                }
                mealPaxItem.amount = totalAmount;
            }

            if (this.SupplierId == "69") {
                var index = self.oldMeal.findIndex(function (e) {
                    return e.mealIdentifier.segmentCode == RPH && e.mealIdentifier.paxCode == paxinfo.paxcode;
                })

                var indexMeal = self.oldMeal[index].meal.findIndex(function (e) {
                    return e.hashCode == Selectmealitem.hashCode;
                })

                self.oldMeal[index].meal[indexMeal].mealQuantity = tempcount;
            }
            var totalMealAmount = 0;
            if (self.MealPaxArr != undefined) {
                for (var mealIndex = 0; mealIndex < self.MealPaxArr.length; mealIndex++) {
                    const mealElement = self.MealPaxArr[mealIndex];
                    totalMealAmount = Number(mealElement.amount) + Number(totalMealAmount);
                }
            }
            self.TotalmealPrice = totalMealAmount;
            this.setPaxAncillaryPriceSummary();
        },
        getExtasfromAncillery: function (paxno, paxtype, type) {
            var self = this;
            var Extas = [];
            var flage = false;
            var haveSeatMap = true,
                havebaggage = true,
                havemeals = true;

            if (type == "booking") {
                try {
                    havebaggage = this.ancillaryRespones.paxFareDetails[paxno - 1].paxfareinf.havebaggage;
                    haveSeatMap = this.ancillaryRespones.paxFareDetails[paxno - 1].paxfareinf.haveSeatMap;
                    havemeals = this.ancillaryRespones.paxFareDetails[paxno - 1].paxfareinf.havemeals;
                } catch (error) {
                    haveSeatMap = havebaggage = havemeals = false;
                }
            }
            if (paxtype !== 'INF') {
                var airSupplierResponse = selectResponseObj.content;
                var airResposneLegs = airSupplierResponse.fareInformationWithoutPnrReply.airSegments;
                var airSectoridies = _.pluck(airResposneLegs, 'segDirectionID');
                airSectoridies = _.uniq(airSectoridies);
                var airLineSegments = [];
                for (var i = 0; i < airSectoridies.length; i++) {
                    var segments = airResposneLegs.filter(function (segment) {
                        return segment.segDirectionID == airSectoridies[i]
                    });
                    $.each(segments, function (segIndex, segment) {

                        if (self.SeatExtra.length > 0 && haveSeatMap) {
                            $.each(self.SeatExtra, function (key, value) {
                                if (value.seatinfo.length > 0) {
                                    flage = true;
                                    if (value.segRph === segment.fareSourceCode) {
                                        var seatinfo = value.seatinfo;
                                        var tempseet = [];
                                        var found = seatinfo.some(function (el, i) {
                                            if (parseInt(el.paxid) + 1 === parseInt(paxno)) {
                                                tempseet = el.seat;
                                                return true;
                                            }
                                        });

                                        var confirmed = true;

                                        if (type == "booking") {
                                            confirmed = seatinfo.some(function (e) {
                                                var paxseat = self.ancillaryRespones.paxFareDetails[paxno - 1].paxfareinf.paxseat;
                                                return paxseat.some(function (k) {
                                                    return e.seat.sealedCode == k.seat.seat.sealedCode;
                                                });
                                            });
                                        }

                                        if (found && confirmed) {
                                            if (typeof Extas[parseInt(value.segement)] === 'undefined') {
                                                Extas.push({
                                                    seat: [tempseet],
                                                    from: segment.segInfo.flData.boardPoint,
                                                    to: segment.segInfo.flData.offpoint,
                                                    flightRefNumberRPHList: segment.fareSourceCode
                                                });

                                            } else {
                                                Extas[parseInt(value.segement)].seat = [tempseet];
                                                Extas[parseInt(value.segement)].from = segment.segInfo.flData.boardPoint;
                                                Extas[parseInt(value.segement)].to = segment.segInfo.flData.offpoint;
                                                Extas[parseInt(value.segment)].flightRefNumberRPHList = segment.fareSourceCode;
                                            }

                                        }
                                    }

                                }
                            });
                        }
                        if (self.MealsExtra.length > 0 && havemeals) {
                            $.each(self.MealsExtra, function (key, value) {
                                if (value.mealData.length > 0) {
                                    flage = true;
                                    if (value.segRph === segment.fareSourceCode || value.segRph === (segment.segInfo.flData.boardPoint + "/" + segment.segInfo.flData.offpoint)) {
                                        var mealinfo = value.mealData;
                                        var tempmeals = [];
                                        var found = mealinfo.some(function (el, i) {
                                            if (parseInt(el.paxCode) === parseInt(paxno)) {
                                                tempmeals = el.meal;
                                                return true;
                                            }
                                        });

                                        var confirmedMeal = JSON.parse(JSON.stringify(tempmeals));

                                        if (type == "booking") {
                                            confirmedMeal = tempmeals.filter(function (segment) {
                                                var paxmeal = self.ancillaryRespones.paxFareDetails[paxno - 1].paxfareinf.paxmeal;
                                                return paxmeal.some(function (k) {
                                                    return k.meal.meal.some(function (m) {
                                                        return m.sealedCode == segment.sealedCode;
                                                    });
                                                });
                                            });
                                        }

                                        if (found) {
                                            if (typeof Extas[parseInt(value.segement)] === 'undefined') {
                                                Extas.push({
                                                    meal: type == "booking" ? confirmedMeal : tempmeals,
                                                    from: segment.segInfo.flData.boardPoint,
                                                    to: segment.segInfo.flData.offpoint,
                                                    flightRefNumberRPHList: segment.fareSourceCode
                                                });
                                            } else {
                                                Extas[parseInt(value.segement)].meal = type == "booking" ? confirmedMeal : tempmeals;
                                                Extas[parseInt(value.segement)].from = segment.segInfo.flData.boardPoint;
                                                Extas[parseInt(value.segement)].to = segment.segInfo.flData.offpoint;
                                                Extas[parseInt(value.segement)].flightRefNumberRPHList = segment.fareSourceCode;
                                            }

                                        }
                                    }
                                }

                            });
                        }
                        if (self.BagaggeExtra.length > 0 && havebaggage) {
                            $.each(self.BagaggeExtra, function (key, value) {
                                if (value.baggageData.length > 0) {
                                    flage = true;
                                    if (value.segRph === segment.fareSourceCode || value.segRph === (segment.segInfo.flData.boardPoint + "/" + segment.segInfo.flData.offpoint)) {
                                        var bagginfo = value.baggageData;
                                        var tempbaggage = [];
                                        var found = bagginfo.some(function (el, i) {
                                            if (parseInt(el.paxCode) === parseInt(paxno)) {
                                                // delete el.paxCode;
                                                tempbaggage = el;
                                                return true;
                                            }
                                        });

                                        var confirmed = true;

                                        if (type == "booking") {
                                            confirmed = bagginfo.some(function (e) {
                                                var paxbag = self.ancillaryRespones.paxFareDetails[paxno - 1].paxfareinf.paxbag;
                                                return paxbag.some(function (k) {
                                                    return e.sealedCode == k.Bag.sealedCode;
                                                });
                                            });
                                        }

                                        if (found && confirmed) {
                                            if (typeof Extas[parseInt(value.segment)] === 'undefined') {
                                                Extas.push({
                                                    baggage: [tempbaggage],
                                                    from: segment.segInfo.flData.boardPoint,
                                                    to: segment.segInfo.flData.offpoint,
                                                    flightRefNumberRPHList: segment.fareSourceCode || value.segRph
                                                });
                                            } else {
                                                Extas[parseInt(value.segment)].baggage = [tempbaggage];
                                                Extas[parseInt(value.segment)].from = segment.segInfo.flData.boardPoint;
                                                Extas[parseInt(value.segment)].to = segment.segInfo.flData.offpoint;
                                                Extas[parseInt(value.segment)].flightRefNumberRPHList = segment.fareSourceCode || value.segRph;
                                            }

                                        }
                                    }
                                }

                            });
                        }

                    });
                }
            }

            return Extas;
        },
        setAncillaryPriceQuate: function (result) {
            var self = this;
            try {
                self.showAncillaryFarePricediv = false;
                // self.showFarepricediv = false;
                var needPaxFare = false;
                if (!stringIsNullorEmpty(result.data)) {

                    if (result.data.response.content.hasOwnProperty('error')) {

                    } else {

                        var Result = result.data.response.content.fareInformationWithoutPnrReply;
                        //    self.ancillaryRespones=Result;
                        //    var anc={}
                        self.ancillaryRespones['flightFareDetails'] = Result.flightFareDetails;
                        self.ancillaryRespones['sealed'] = Result.sealed;
                        var paxFareDetails = [];
                        // self.flightFareDetails.totalFare = Result.flightFareDetails.totalFare;

                        if (Result.paxFareDetails.length > 0) {

                            $.each(Result.paxFareDetails, function (paxindex, PaxFareData) {
                                var paxfareinf = {};
                                var currentpax = self.travellers.filter(obj => (obj.paxcode === PaxFareData.travelerRefNumber) || (obj.paxType + obj.paxcode.match(/\d/g)[0] === PaxFareData.travelerRefNumber) || (obj.passengerType + "-" + obj.paxcode.match(/\d/g)[0] === PaxFareData.travelerRefNumber))[0];

                                if (!stringIsNullorEmpty(currentpax)) {
                                    paxfareinf.givenName = currentpax.givenName;
                                    paxfareinf.surName = currentpax.surName;
                                    paxfareinf.namePrefix = currentpax.namePrefix;
                                    paxfareinf.paxcode = currentpax.paxcode;
                                    paxfareinf.haveSeatMap = false;
                                    paxfareinf.havebaggage = false;
                                    paxfareinf.havemeals = false;
                                    paxfareinf.basefare = PaxFareData.basefare;
                                    paxfareinf.taxFare = PaxFareData.taxFare;
                                    paxfareinf.totalFare = PaxFareData.totalFare;
                                    paxfareinf.currency = PaxFareData.currency;
                                    paxfareinf.showhidediv = false;
                                    var mealsAmount = 0;
                                    var seatAmount = 0;
                                    $.each(PaxFareData.feesbreakup, function (fareindex, FareDta) {
                                        if (FareDta.feeCode.split('/')[0] === 'SM') {
                                            paxfareinf.paxseat = [];
                                            $.each(self.SeatExtra, function (seatArryIndex, seatArryItem) {
                                                try {
                                                    var tempseat = seatArryItem.seatinfo.filter(obj => obj.paxcode === PaxFareData.travelerRefNumber)[0];
                                                    if (!stringIsNullorEmpty(tempseat)) {

                                                        paxfareinf.haveSeatMap = true;
                                                        var newsegmentno = seatArryItem.segRph;
                                                        var tempsegments = Result.airSegments.filter(seg => seg.fareSourceCode === newsegmentno)[0];
                                                        $.each(seatArryItem.seatinfo, function (seatDataIndex, seatDataItem) {
                                                            if (seatDataIndex === 0) {
                                                                paxfareinf.paxseat.push({
                                                                    seat: tempseat,
                                                                    from: tempsegments.segInfo.flData.boardPoint,
                                                                    to: tempsegments.segInfo.flData.offpoint
                                                                });
                                                            }
                                                        });

                                                    }
                                                } catch (err) {

                                                }
                                            });
                                            paxfareinf.seatcurrency = FareDta.currencyCode;
                                            if (self.SupplierId == "86") {
                                                seatAmount += Number(FareDta.amount);
                                                paxfareinf.seatfare = seatAmount;
                                            }
                                            else{
                                                paxfareinf.seatfare = FareDta.amount;
                                            }
                                            
                                        }
                                        if (FareDta.feeCode.split('/')[0] === 'ML' || (FareDta.feeCode == "IFPP" && self.SupplierId == "54")) {
                                            paxfareinf.paxmeal = [];
                                            $.each(self.MealsExtra, function (mealArryIndex, mealArryItem) {
                                                try {
                                                    var tempmeal = mealArryItem.mealData.filter(obj => (obj.paxCode === PaxFareData.travelerRefNumber) || (obj.paxType + obj.paxCode.match(/\d/g)[0] === PaxFareData.travelerRefNumber) || (obj.passengerType + "-" + obj.paxCode.match(/\d/g)[0] === PaxFareData.travelerRefNumber))[0];

                                                    if (!stringIsNullorEmpty(tempmeal)) {
                                                        tempmeal = JSON.parse(JSON.stringify(tempmeal));
                                                        paxfareinf.havemeals = true;
                                                        var newsegmentno = mealArryItem.segRph;
                                                        var tempsegments = Result.airSegments.filter(seg => seg.fareSourceCode === newsegmentno)[0];
                                                        if (self.SupplierId == "69" || self.SupplierId == "86") {
                                                            tempsegments = Result.airSegments.filter(obj => (obj.segInfo.flData.boardPoint + "/" + obj.segInfo.flData.offpoint) == newsegmentno)[0];
                                                        }
                                                        $.each(mealArryItem.mealData, function (mealDataIndex, mealDataItem) {
                                                            if (mealDataIndex === 0) {
                                                                paxfareinf.paxmeal.push({
                                                                    meal: tempmeal,
                                                                    from: tempsegments.segInfo.flData.boardPoint,
                                                                    to: tempsegments.segInfo.flData.offpoint
                                                                });
                                                            }
                                                        });

                                                    }
                                                } catch (err) {

                                                }
                                            });
                                            paxfareinf.mealcurrency = FareDta.currencyCode;
                                            if (self.SupplierId == "54" || self.SupplierId == "86") {
                                                mealsAmount += Number(FareDta.amount);
                                                paxfareinf.mealfare = mealsAmount;
                                            }
                                            else{
                                                paxfareinf.mealfare = FareDta.amount;
                                            }

                                        }
                                        if (FareDta.feeCode.split('/')[0] === 'BG') {
                                            paxfareinf.paxbag = [];
                                            $.each(self.Baggagepaxarr, function (baggArryIndex, baggArryItem) {
                                                try {

                                                    var tempbag = baggArryItem.baggageData.filter(obj => (obj.paxCode === PaxFareData.travelerRefNumber) || (obj.paxType + obj.paxCode.match(/\d/g)[0] === PaxFareData.travelerRefNumber) || (obj.passengerType + "-" + obj.paxCode.match(/\d/g)[0] === PaxFareData.travelerRefNumber))[0];
                                                    if (!stringIsNullorEmpty(tempbag)) {

                                                        paxfareinf.havebaggage = true;
                                                        // var newsegmentno = baggArryItem.segment;
                                                        var tempsegments = Result.airSegments.filter(obj => obj.segInfo.flData.boardPoint == baggArryItem.fromAP && obj.segInfo.flData.offpoint == baggArryItem.toAP)[0];
                                                        // var tempsegments = Result.airSegments.filter(seg => seg.segDirectionID === newsegmentno)[0];
                                                        $.each(baggArryItem.baggageData, function (bagDataIndex, bagDataItem) {
                                                            if (bagDataIndex === 0) {
                                                                paxfareinf.paxbag.push({
                                                                    legIndex: baggArryItem.legIndex,
                                                                    Bag: tempbag,
                                                                    from: tempsegments.segInfo.flData.boardPoint,
                                                                    to: tempsegments.segInfo.flData.offpoint
                                                                });
                                                            }
                                                        });

                                                    }
                                                } catch (err) {

                                                }
                                            });
                                            paxfareinf.bagcurrency = FareDta.currencyCode;
                                            paxfareinf.bagfare = FareDta.amount;

                                            var paxbagNew = [];
                                            for (var m = 0; m < self.BaggageInfo.length; m++) {
                                                var paxInf = paxfareinf.paxbag.filter(function (e) {
                                                    return self.BaggageInfo[m].index == e.legIndex;
                                                })[0];
                                                if (paxInf) {
                                                    paxbagNew.push({
                                                        from: self.BaggageInfo[m].From,
                                                        to: self.BaggageInfo[m].To,
                                                        Bag: paxInf.Bag
                                                    });
                                                }
                                            }
                                            paxfareinf.paxbagNew = paxbagNew;
                                        }
                                    });
                                    if (paxfareinf.haveSeatMap == true || paxfareinf.havemeals == true || paxfareinf.havebaggage == true) {
                                        needPaxFare = true;
                                    }
                                    if (self.SeatExtra.length > 0 || self.MealsExtra.length > 0 || self.BagaggeExtra.length > 0) {
                                        needPaxFare = true;
                                    }
                                    paxFareDetails.push({
                                        paxfareinf
                                    });


                                }
                            });

                            self.ancillaryRespones['paxFareDetails'] = JSON.parse(JSON.stringify(paxFareDetails));

                            var total = 0;
                            for (var index = 0; index < paxFareDetails.length; index++) {

                                total += (parseFloat(paxFareDetails[index].paxfareinf.seatfare) || 0) +
                                    (parseFloat(paxFareDetails[index].paxfareinf.mealfare) || 0) +
                                    (parseFloat(paxFareDetails[index].paxfareinf.bagfare) || 0);
                            }

                            self.paidServices = total;

                            if (parseFloat(Result.flightFareDetails.totalFare) != parseFloat(self.flightFareDetails.totalFare) || parseFloat(self.totalWithPaidServices) != parseFloat(Result.flightFareDetails.totalFare)) {
                                // if (parseFloat(Result.flightFareDetails.totalFare) - parseFloat(self.flightFareDetails.totalFare) >= total) {
                                var priceMovement = parseFloat(Result.flightFareDetails.totalFare) > parseFloat(self.totalWithPaidServices) ? "increased" : "decreased";
                                var currency, multiplier;
                                if (self.commonStore.selectedCurrency == '') {
                                    multiplier = self.selectFlightFromSession.selectedFlightInfo.selectCredential.officeIdList[0].multiplier;
                                    currency = self.selectFlightFromSession.selectedFlightInfo.selectCredential.officeIdList[0].currency;
                                } else {
                                    multiplier = self.commonStore.currencyMultiplier;
                                    currency = self.commonStore.selectedCurrency;
                                }
                                alertify.alert("Please note",
                                    "Additional paid services has been added. Your fare was " + priceMovement + " from <b>" +
                                    self.$n(priceMovement == "decreased" ? self.totalWithPaidServices : self.flightFareDetails.totalFare / multiplier, 'currency', currency) +
                                    "</b> to <b>" +
                                    self.$n(self.ancillaryRespones.flightFareDetails.totalFare / multiplier, 'currency', currency) +
                                    "</b>.").set('onok', function(closeEvent){
                                    $("#paid_services").modal('hide');} );
                                    // .set('closable', false).set('label', self.Ok);

                                self.totalWithPaidServices = Result.flightFareDetails.totalFare;
                            }

                            var paxbag = []
                            for (var z = 0; z < self.ancillaryRespones.paxFareDetails.length; z++) {
                                if (self.ancillaryRespones.paxFareDetails[z].paxfareinf.paxbag) {
                                    paxbag = paxbag.concat(self.ancillaryRespones.paxFareDetails[z].paxfareinf.paxbag);
                                }
                            }

                            for (var k = 0; k < self.AirLineBaggages.length; k++) {
                                var total = 0;
                                for (var j = 0; j < paxbag.length; j++) {
                                    var segment = paxbag[j].from + "-" + paxbag[j].to;
                                    if (self.AirLineBaggages[k].fromToAncilleryMapping == segment) {
                                        total += parseInt(paxbag[j].Bag.quantity);
                                        self.AirLineBaggages[k].isBaggageAddedFromAncillery = true;
                                        self.AirLineBaggages[k].totalBaggage = total + " " + (paxbag[j].Bag.unit == "" ? paxbag[j].Bag.unit1 : paxbag[j].Bag.unit) + " (Total)";
                                    }
                                }
                            }
                        }
                    }
                }
                self.showAncillaryFarePricediv = (needPaxFare == true) ? true : false;
                // self.showFarepricediv = (needPaxFare == true) ? false : true;

            } catch (err) {
                console.log(err)
            }
        },
        isNullorEmptyToBlank: function (value, optval) {
            return isNullorEmpty(value) ? (isNullorEmpty(optval) ? '' : optval) : value;
        },
        showhideancillery: function (item, paxitem, id) {
            var clickedElement = item.currentTarget;
            $(id + paxitem).toggle();
            $(clickedElement).find('i').toggleClass('fa-chevron-down fa-chevron-up');
        },
        bindCreditLimit: function () {
            bindCreditLimit();
        },
        GetpaxwithAncillery: function () {
            var vm = this;
            var dateFormatFrom = 'DD/MM/YYYY';
            var passinfo = null;
            var leginfo = null;
            var airSupplierResponse = selectResponseObj.content;
            var airResposneLegs = airSupplierResponse.fareInformationWithoutPnrReply.airSegments;
            var airSectoridies = _.pluck(airResposneLegs, 'segDirectionID');
            airSectoridies = _.uniq(airSectoridies);
            var passengerTypeQuantity = airSupplierResponse.fareInformationWithoutPnrReply.passengerTypeQuantity;
            var adult = passengerTypeQuantity.adt;
            var child = passengerTypeQuantity.chd;
            var infant = passengerTypeQuantity.inf;
            var totalPassengers = parseInt(adult) + parseInt(child) + parseInt(infant);
            var adultIndex = 1;
            var childIndex = 1;
            var infantIndex = 1;
            var currentIndex = 1;
            var travelerInfo = [];
            for (var i = 1; i <= totalPassengers; i++) {
                var paxType = (i <= parseInt(adult) ? 'Adult' : (i > parseInt(adult) && (i <= parseInt(adult) + parseInt(child)) ? 'Child' : 'Infant'));
                var paxTypeCode = (i <= parseInt(adult) ? 'ADT' : (i > parseInt(adult) && (i <= parseInt(adult) + parseInt(child)) ? 'CHD' : 'INF'));
                switch (paxTypeCode) {
                    case 'ADT':
                        currentIndex = adultIndex;
                        adultIndex++;
                        break;
                    case 'CHD':
                        currentIndex = childIndex;
                        childIndex++;
                        break;
                    case 'INF':
                        currentIndex = infantIndex;
                        infantIndex++;
                        break;
                }
                var Extraservices = null;
                if (vm.SupplierId == "44" || vm.SupplierId == "49" || vm.SupplierId == "50" || vm.SupplierId == "54" || vm.SupplierId == "71" || vm.SupplierId == "69" || vm.SupplierId == "86") {
                    var Extraservicesre = vm.getExtasfromAncillery(i, paxTypeCode, "priceQuote");
                    if (Extraservicesre.length > 0) {
                        Extraservices = Extraservicesre;
                    }
                }
                var paxTypeIndex = paxTypeCode + currentIndex;
                var frequentFlayerNumber = $('#txtFreqFlyNumber' + paxTypeIndex).val();
                var frequentFlayerAirlineCode = $('#ddlFreqFlyAirLine' + paxTypeIndex).val();
                var paxTitle = $('#ddltitle' + paxTypeIndex).val();
                var paxGender = (paxTitle == 'Mr' || paxTitle == 'Mstr') ? 'Male' : 'Female';

                var paxDob = null;
                if ($('#txtDob' + paxTypeIndex).val() != '') {
                    paxDob = moment($('#txtDob' + paxTypeIndex).val(), dateFormatFrom).format('DD-MM-YYYY');
                }
                var passIsuDate = null;
                // if ($('#txtPassportIssue' + paxTypeIndex).val() != '') {
                //     passIsuDate = moment($('#txtPassportIssue' + paxTypeIndex).val(), dateFormatFrom).format('DD-MM-YYYY');
                // }
                var passExpDate = null;
                if ($('#txtExpireDate' + paxTypeIndex).val() != '') {
                    passExpDate = moment($('#txtExpireDate' + paxTypeIndex).val(), dateFormatFrom).format('DD-MM-YYYY');
                }
                var docIssueCountry = null;
                try {
                    if (!isNullorEmpty($('#ddlPassportIssue' + paxTypeIndex).val()) && $('#ddlPassportIssue' + paxTypeIndex).val() != 0) {
                        docIssueCountry = $('#ddlPassportIssue' + paxTypeIndex).val();
                    }
                } catch (err) {
                    /* empty */
                }
                var nationalityData = null;
                try {
                    if (!isNullorEmpty($('#txtnationality' + paxTypeIndex).val())) {
                        nationalityData = {
                            twoLetter: $('#txtnationality' + paxTypeIndex).val(),
                            threeLetter: $('#txtnationality' + paxTypeIndex + '>option:selected').data('letter3')
                        };
                    }
                } catch (err) {}

                var traveller = {
                    passengerType: paxTypeCode,
                    gender: paxGender,
                    givenName: $('#txtFname' + paxTypeIndex).val().trim(),
                    namePrefix: paxTitle,
                    surName: $('#txtSname' + paxTypeIndex).val().trim(),
                    quantity: i,
                    birthDate: paxDob,
                    docType: '1',
                    docID: $('#txtPassport' + paxTypeIndex).val(),
                    issuanceDate: passIsuDate,
                    docIssueCountry: docIssueCountry,
                    expireDate: passExpDate,
                    nationalityData: nationalityData,
                    frequentFlyerNumber: (frequentFlayerNumber == undefined || frequentFlayerNumber == '') ? null : frequentFlayerNumber,
                    ffAirlineCode: (frequentFlayerAirlineCode == undefined || frequentFlayerAirlineCode == '') ? null : frequentFlayerAirlineCode,
                    extras: Extraservices
                };
                var contact = null;
                if (paxTypeIndex === 'ADT1') {
                    var phone = $('#txtLeadPaxPhone2').val();
                    try {
                        phone = (_.find(vm.countryCodes, function (code) {
                            return code[2].match($('#txtLeadPaxPhone2').val()) && $('#txtLeadPaxPhone2').val().match(code[2]);
                        })[1] || "").toUpperCase();
                    } catch (error) {
                        phone = "";
                    }
                    contact = {
                        phoneList: [{
                            number: $('#txtLeadPaxPhone').val().trim(),
                            country: {
                                code: $('#txtLeadPaxPhone2').val() ? phone.trim() : "",
                                telephonecode: $('#txtLeadPaxPhone2').val().trim()
                            },
                            phoneType: {
                                id: 1
                            }
                        }],
                        emailList: [{
                            emailId: $('#txtLeadPaxEmail').val().trim(),
                            emailType: {
                                id: 1
                            }
                        }]
                    };

                    contact.phoneList = contact.phoneList.concat(vm.additionalContact.map(function (e) {
                        return {
                            number: e.contactNumber,
                            country: {
                                code: e.countryCode,
                                telephonecode: e.countryCodeNumber
                            },
                            phoneType: {
                                id: 1
                            }
                        }
                    }))

                    contact.emailList = contact.emailList.concat(vm.additionalContact.map(function (e) {
                        return {
                            emailId: e.emailId,
                            emailType: {
                                id: 1
                            }
                        }
                    }))
                }



                traveller['contact'] = contact;

                travelerInfo.push(traveller);
            }
            var flLegGroup = [];
            var customerInfo = {
                firstName: vm.Agency_Info.firstName,
                lastName: vm.Agency_Info.lastName,
                phoneNumber: vm.commonStore.agencyNode.loginNode.phoneList[0].number,
                email: vm.commonStore.agencyNode.loginNode.email
            };
            for (var i = 0; i < airSectoridies.length; i++) {
                var segments = airResposneLegs.filter(function (segment) {
                    return segment.segDirectionID == airSectoridies[i]
                });
                var airLineSegments = [];
                var flightDetails = '';
                try {
                    flightDetails = selectFlightFromSessionObj.selectedFlightInfo.groupOfFlights[i].flightDetails;
                } catch (err) {}
                $.each(segments, function (segIndex, segment) {
                    var terminalFrom = '';
                    var terminalTo = '';
                    try {
                        terminalFrom = isNullorUndefined(segment.segInfo.flData.boardTerminal) ? flightDetails[segIndex].fInfo.location.fromTerminal : segment.segInfo.flData.boardTerminal;
                    } catch (err) {}
                    try {
                        terminalTo = isNullorUndefined(segment.segInfo.flData.offTerminal) ? flightDetails[segIndex].fInfo.location.toTerminal : segment.segInfo.flData.offTerminal;
                    } catch (err) {}
                    var airLineSegment = {
                        fareBasis: stringIsNullorEmpty(segment.fareId) ? null : segment.fareId,
                        departureDate: moment(segment.segInfo.dateandTimeDetails.departureDate, 'DDMMYY').format('DD-MM-YYYY'),
                        departureTime: segment.segInfo.dateandTimeDetails.departureTime,
                        arrivalDate: moment(segment.segInfo.dateandTimeDetails.arrivalDate, 'DDMMYYYY').format('DD-MM-YYYY'),
                        arrivalTime: segment.segInfo.dateandTimeDetails.arrivalTime,
                        departureFrom: segment.segInfo.flData.boardPoint,
                        departureTo: segment.segInfo.flData.offpoint,
                        marketingCompany: segment.segInfo.flData.marketingCompany,
                        operatingCompany: segment.segInfo.flData.operatingCompany,
                        validatingCompany: segment.segInfo.flData.validatingCompany,
                        flightNumber: segment.segInfo.flData.flightNumber,
                        bookingClass: !stringIsNullorEmpty(segment.segInfo.flData.bookingClass) ? segment.segInfo.flData.bookingClass : segment.segInfo.flData.rbd,
                        rbd: !stringIsNullorEmpty(segment.segInfo.flData.rbd) ? segment.segInfo.flData.rbd : segment.segInfo.flData.bookingClass,
                        terminalTo: terminalTo,
                        terminalFrom: terminalFrom,
                        flightEquip: segment.segInfo.flData.equiptype,
                        paxRef: !stringIsNullorEmpty(segment.segInfo.flData.paxRef) ? segment.segInfo.flData.paxRef : null,
                    }
                    airLineSegments.push(airLineSegment);
                });

                var airLineLeg = {
                    elapsedTime: null,
                    from: segments[0].originCity,
                    to: segments[segments.length - 1].departureCity,
                    segments: airLineSegments
                }
                flLegGroup.push(airLineLeg);
            }
            if (travelerInfo.length > 0 && flLegGroup.length > 0) {
                var travelinfo = {
                    travelerInfo,
                    flLegGroup,
                    customerInfo
                };
                passinfo = JSON.stringify(travelinfo);

            }
            console.log(passinfo)

            passinfo = btoa(passinfo);

            return passinfo;
        },

        // get HQ node payment gateway privilege because payment gateway not getting in subnodes
        getHQPaymentGateway: function () {
            var vm = this;
            var hqNodeId = vm.commonStore.agencyNode.loginNode.solutionId;
            if (hqNodeId) {

                var huburl = vm.commonStore.hubUrls.hubConnection.baseUrl;
                var portno = vm.commonStore.hubUrls.hubConnection.ipAddress;
                var serviceUrl = vm.commonStore.hubUrls.hubConnection.hubServices.paymentGatewayHQ;

                var config = {
                    axiosConfig: {
                        method: "GET",
                        url: huburl + portno + serviceUrl + hqNodeId
                    },
                    successCallback: function (response) {
                        try {

                            if (response.data) {
                                console.log("HQ payment gateways : ", response.data);
                                vm.creditCardButton(response.data);
                            }

                        } catch (err) {
                            console.log('no payment gateway found in hqnode');
                        }
                    },
                    errorCallback: function (error) {},
                    showAlert: false
                };

                mainAxiosRequest(config);
            }
        }

    },
    mounted: function () {
        var currentSupplier = selectFlightFromSessionObj.selectedFlightInfo.segmentRef.supplier;
        if (currentSupplier != 45) {
            this.if_cabbinbaggage = true;
        }


        var self = this;
        self.supplierCode = selectFlightFromSessionObj.selectedFlightInfo.segmentRef.supplier;
        self.ArlineSelected.selected = 0;
        self.NationalitySelected.selected = 0;
        self.IssueCntrySelected.selected = 0;
        self.getUserInfo();
        var dateFormat = 'dd/mm/yy';
        var noOfMonths = 1;
        $(".cont_txt01_cldr").datepicker({
            numberOfMonths: parseInt(noOfMonths),
            changeMonth: true,
            changeYear: true,
            showButtonPanel: false,
            dateFormat: dateFormat,
            beforeShow: function () {
                self.getMinDate(this);
                self.getMaxDate(this);
            }
            /*,onSelect: function (date) {
                var startDate = $(this).datepicker('getDate');
                sDate = startDate.getTime();
            }*/
        });
        this.AirlinesTimezone = AirlinesTimezone;

        var toAirport = this.selectFlightFromSession.selectFlightRq.request.content.commonRequestFarePricer.body.airRevalidate.to;
        var fromAirport = this.selectFlightFromSession.selectFlightRq.request.content.commonRequestFarePricer.body.airRevalidate.from;

        if (currentSupplier == 54) {

            var isForUmrahNotice = false;
            $.each(this.selectFlightFromSession.selectedFlightInfo.groupOfFlights, function (index, airLeg) {
                $.each(airLeg.flightDetails, function (index, flDetails) {
                    isForUmrahNotice = (["JED", "MED", "TIF"].indexOf(flDetails.fInfo.location.locationFrom.toUpperCase()) != -1) ||
                        (["JED", "MED", "TIF"].indexOf(flDetails.fInfo.location.locationTo.toUpperCase()) != -1)
                });
            });

            if ((["JED", "MED", "TIF"].indexOf(toAirport) != -1) || (["JED", "MED", "TIF"].indexOf(fromAirport) != -1) || isForUmrahNotice) {
                $("#fly-dubai-notice").modal({
                    backdrop: "static",
                    keyboard: false,
                    show: true
                })
            }
        }
        setTimeout(() => {
            self.paymentGatewayButton();
        }, 1000);
    },
    filters: {
        momCommon: function (date, frmFormat, toFormat) {
            return moment(date, frmFormat).format(toFormat);
        }
    },
    watch: {
        workingonseat: function (val) {
            if (this.SeatMapArry.length > 0) {
                this.TotalseatPrice = this.SeatMapArry.map(amou => parseFloat(amou.amount)).reduce((acc, amou) => amou + acc);
            }
        },
        bookType: function (val) {
            try {
                var temptype = selectFlightFromSessionObj.selectedFlightInfo.typeRef.type;
                this.Booking_Button = this.bookType == 0 ? this.Confirm_Booking_Button : this.Proceed_Payment_Button_Label;
            } catch (err) {}

        },
        seatIndex: function () {
            this.hideSeat = this.SeatMapArry[this.seatIndex].status;
        }
    },
    computed: {}
});

function getdatetimeformated(datetime, inputFormat, outputFormat) {
    return moment(datetime, inputFormat).format(outputFormat);
}

function gettimeformated(time, inputFormat, houroutputFormat, houraddl, minoutputFormat, minaddl) {
    var hour = moment(time, inputFormat).format(houroutputFormat).toString();
    var min = moment(time, inputFormat).format(minoutputFormat).toString();
    var formattedtime = hour + houraddl + min + minaddl;
    return formattedtime;
}

function calculateDatetimeDiff(startDate, startDateFormat, endDate, endDateFormat) {
    var start_date = moment(startDate, startDateFormat);
    var end_date = moment(endDate, endDateFormat);
    var duration = moment.duration(end_date.diff(start_date));
    var hours = duration.hours();
    var mins = duration.minutes();
    return hours + 'h ' + mins + 'm';
}

function stringIsNullorEmpty(value) {
    var status = false;
    if (value == undefined || value == null || value == '') {
        status = true;
    }
    return status;
}

function isValidEmail(emailID) {
    var status = false;
    var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    var matchArray = emailID.match(emailPat);
    if (matchArray != null) {
        status = true;
    }
    return status;
}

function isNullorUndefined(value) {
    var status = false;
    if (value == '' || value == null || value == undefined || value == "undefined" || value == [] || value == NaN) {
        status = true;
    }
    return status;
}

function isNumber(value) {
    var status = false;
    if (Math.floor(value) == value && $.isNumeric(value)) {
        status = true
    }
    return status;
}

function isNullorEmpty(value) {
    var status = false;
    if (value == null || value == undefined || value == "undefined") {
        status = true;
    }
    if (!status && $.trim(value) == '') {
        status = true;
    }
    return status;
}

function isNullorEmptyToBlank(value, optval) {
    return isNullorEmpty(value) ? (isNullorEmpty(optval) ? '' : optval) : value;
}

function getAirport(airportCode) {
    var airport = null;
    try {
        airport = $.grep(this.AirlinesTimezone, function (value) {
            return (value.I.toUpperCase() == airportCode.toUpperCase());
        });
    } catch (err) {}
    if (isNullorUndefined(airport)) {
        airport = {
            label: airportCode.toUpperCase(),
            N: airportCode.toUpperCase(),
            C: airportCode.toUpperCase(),
            CN: airportCode.toUpperCase(),
            I: airportCode.toUpperCase(),
            T: "A",
            TZ: 0
        };
    } else {
        airport = airport[0];
    }
    return airport;
}

function getPaxType(paxType, paxCount) {
    var paxTypeText = 'Adult';
    switch (paxType) {
        case 'ADT':
            paxTypeText = 'Adult' + ((paxCount > 1) ? 's' : '');
            break;
        case 'CHD':
            paxTypeText = 'Child' + ((paxCount > 1) ? 'ren' : '');
            break;
        case 'INF':
            paxTypeText = 'Infant' + ((paxCount > 1) ? 's' : '');
            break;
    }
    return paxTypeText;
}

function getUnit(unit) {
    unit = unit.toLowerCase();
    var unitTypeText = 'KG';
    switch (unit) {
        case 'k':
        case 'kg':
        case 'w':
        case '700':
            unitTypeText = 'KG';
            break;
        case 'pc':
        case 'p':
        case 'n':
            unitTypeText = 'Piece';
            break;
        default:
            unitTypeText = 'KG';
            break;
    }
    return unitTypeText;
}

function getTitleOptions(paxAvailbleTitles) {
    var divOptions = [];
    $.each(paxAvailbleTitles, function (index, title) {
        divOptions.push(title.name);
    });
    return divOptions;
}

function SortByName(a, b) {
    var aName = a.A.toLowerCase();
    var bName = b.A.toLowerCase();
    return ((aName < bName) ? -1 : ((aName > bName) ? 1 : 0));
}

function getAirCraftEquip(selectedFlight, departureCity, arrivalCity) {
    var airSelectedFlightseg = [];
    $.each(selectedFlight.selectedFlightInfo.groupOfFlights, function (index, airLeg) {
        if (airSelectedFlightseg.length == 0) {
            airSelectedFlightseg = airLeg.flightDetails.filter(function (airSeg) {
                return airSeg.fInfo.location.locationFrom.toUpperCase().split(' ').join('') == departureCity.toUpperCase().split(' ').join('') &&
                    airSeg.fInfo.location.locationTo.toUpperCase().split(' ').join('') == arrivalCity.toUpperCase().split(' ').join('');
            });
        }
    });
    return hasArrayData(airSelectedFlightseg) ? airEqType = airSelectedFlightseg[0].fInfo.eqpType : airEqType = '';
}

function getCabinClass(cabinCode) {
    var cabinClass = 'Economy';
    if (cabinCode == "F") {
        cabinClass = "First Class";
    } else if (cabinCode == "C") {
        cabinClass = "Business";
    } else {
        try {
            cabinClass = getCabinClassObject(cabinCode).BasicClass;
        } catch (err) {}
    }
    return cabinClass;
}

function getLayover(index, flightLength) {
    if (index + 1 == flightLength) {
        return false;
    } else {
        return true;
    }
}

function getLayoverTime(index, airSegments) {
    var LayoverTimee = '';
    if (index + 1 != airSegments.length) {
        var arrivalDate1 = airSegments[index].fInfo.dateTime.arrDate;
        var departureDate1 = airSegments[index + 1].fInfo.dateTime.depDate;
        var arrivalTime1 = airSegments[index].fInfo.dateTime.arrTime;
        arrivalTime1 = arrivalTime1.substring(0, 2) + ':' + arrivalTime1.substring(2, 4);
        var departureTime1 = airSegments[index + 1].fInfo.dateTime.depTime;

        departureTime1 = departureTime1.substring(0, 2) + ':' + departureTime1.substring(2, 4);
        var lastyeararrlay = arrivalDate1.substr(arrivalDate1.length - 2);
        var lastmontharrlay = arrivalDate1.substring(2, 4);
        var lastdayarrlay = arrivalDate1.substring(0, 2);
        var dataarrdate = "20" + lastyeararrlay + '-' + lastmontharrlay + '-' + lastdayarrlay + 'T' + arrivalTime1 + ':00';
        var lastyeardeplay = departureDate1.substr(departureDate1.length - 2);
        var lastmonthdeplay = departureDate1.substring(2, 4);
        var lastdaydeplay = departureDate1.substring(0, 2);
        var datadeptdate = "20" + lastyeardeplay + '-' + lastmonthdeplay + '-' + lastdaydeplay + 'T' + departureTime1 + ':00';

        var FirstArrivalTime = moment(dataarrdate);
        var NextDeptTime = moment(datadeptdate);

        var DifferenceMilli = NextDeptTime - FirstArrivalTime;
        var absDifferenceMilli = DifferenceMilli > 0 ? DifferenceMilli : Math.abs(DifferenceMilli);
        DifferenceMilliDuration = moment.duration(absDifferenceMilli);

        LayoverTimee = DifferenceMilliDuration.hours() + 'h ' + DifferenceMilliDuration.minutes() + 'm';
        console.log(LayoverTimee);
        return LayoverTimee;

    }
}

$('body').on('keypress', '.only_alpha', function (e) {
    if (e.ctrlKey || e.altKey) {
        e.preventDefault();
    } else {
        var key = e.keyCode;
        if (!((key == 32) || (key == 96) || (key >= 65 && key <= 90) || (key >= 97 && key <= 122))) {
            e.preventDefault();
        }
    }
});

$('body').on('keypress', '.only_number', function (e) {
    if (e.ctrlKey || e.altKey) {
        e.preventDefault();
    } else {
        var key = e.keyCode;
        if (!((key >= 48 && key <= 57))) {
            e.preventDefault();
        }
    }
});

function disablePaste(e) {
    var keyText = (e.type === "paste" ? e.clipboardData : e.dataTransfer).getData('text');
    var key = !keyText.match(/^[a-z ]+$/i);
        if (key) {
            e.preventDefault();
        }
}

$('.only_alpha, .only_number, .sel_co_trvlr').bind('copy paste cut', function (e) {
    e.preventDefault(); //disable cut,copy,paste
});

function openSubTab(evt, subTab) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(subTab).style.display = "block";
    evt.currentTarget.className += " active";
}

function openTab(evt, mainTab, title) {
    var i, tab, currtab, tablinks;
    tab = document.getElementsByClassName("tab_rule");
    currtab = document.getElementById(mainTab);
    for (i = 0; i < tab.length; i++) {
        tab[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("mini_tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    currtab.style.display = "block";
    evt.currentTarget.className += " active";
    document.getElementsByClassName('mini_tab_head')[0].innerText = title;
    try {
        currtab.getElementsByClassName('tabmini')[0].getElementsByTagName('button')[0].click();
    } catch (ex) {}
}

$('#myMiniModal').on('load', function () {
    try {
        document.getElementsByClassName("mini_rule")[0].getElementsByTagName('ul')[0].getElementsByTagName('li')[0].click();
    } catch (ex) {}
});

function hasMiniRule(airMiniRules) {
    var hasMiniRule = false;
    if (airMiniRules != undefined && airMiniRules != null && airMiniRules != "" && airMiniRules != "NA" && airMiniRules != "NIL") {
        if (airMiniRules.departure != "" && airMiniRules.departure != "NA" && airMiniRules.departure != null && airMiniRules.departure != undefined && airMiniRules.departure != "NIL") {
            hasMiniRule = true;
        }
    }
    return hasMiniRule;
}

function hasFareRule(airFareRules) {
    var hasFareRule = false;
    if (airFareRules != undefined && airFareRules != null && airFareRules != "" && airFareRules != "NA" && airFareRules != "NIL") {
        if (airFareRules.departure != "" && airFareRules.departure != "NA" && airFareRules.departure != null && airFareRules.departure != undefined && airFareRules.departure != "NIL") {
            hasFareRule = true;
        }
    }
    return hasFareRule;
}
var isDuplicateName = false;
var DuplicateNames = [];



function validateEntries(item) {
    var vm = this;
    var isValidData = true;
    var arrname = [];
    $("input.fname").each(function () {
        var value = $(this).val();
        var id = this.id;
        value = isNullorEmpty(value) ? makeid(6, 2) : value.toLowerCase();
        arrname.push({
            fname: value,
            fnameid: '#' + id
        });
    });
    var lnameindex = 0;
    $("input.lname").each(function () {
        var value = $(this).val();
        var id = this.id;
        value = isNullorEmpty(value) ? makeid(6, 2) : value.trim().toLowerCase();
        arrname[lnameindex]['lname'] = value;
        arrname[lnameindex]['lnameid'] = '#' + id;
        lnameindex++;
    });
    for (i = 0; i < arrname.length - 1; i++) {
        for (j = i + 1; j < arrname.length; j++) {
            if ((arrname[i]['fname'] == arrname[j]['fname']) && (arrname[i]['lname'] == arrname[j]['lname'])) {
                isDuplicateName = true;
                DuplicateNames.push(i + 1);
                DuplicateNames.push(j + 1);
            }
        }
    }
    if (isDuplicateName) {
        return;
    }
    /*if (!$("#chkAgree").is(':checked')) {
        toggleClassShow(('[name="checkbox-list-3"]'), 'border_danger');
        isValidData = false;
    }*/
    if (isValidData) {
        var airSupplierResponse = selectResponseObj.content;
        var passengerTypeQuantity = airSupplierResponse.fareInformationWithoutPnrReply.passengerTypeQuantity;
        var adult = passengerTypeQuantity.adt;
        var child = passengerTypeQuantity.chd;
        var infant = passengerTypeQuantity.inf;
        var totalPassengers = parseInt(adult) + parseInt(child) + parseInt(infant);
        var adultIndex = 1;
        var childIndex = 1;
        var infantIndex = 1;
        var currentIndex = 1;
        for (var i = 1; i <= totalPassengers; i++) {
            var paxType = (i <= parseInt(adult) ? 'Adult' : (i > parseInt(adult) && (i <= parseInt(adult) + parseInt(child)) ? 'Child' : 'Infant'));
            var paxTypeCode = (i <= parseInt(adult) ? 'ADT' : (i > parseInt(adult) && (i <= parseInt(adult) + parseInt(child)) ? 'CHD' : 'INF'));
            switch (paxTypeCode) {
                case 'ADT':
                    currentIndex = adultIndex;
                    adultIndex++;
                    break;
                case 'CHD':
                    currentIndex = childIndex;
                    childIndex++;
                    break;
                case 'INF':
                    currentIndex = infantIndex;
                    infantIndex++;
                    break;
            }
            var paxTypeIndex = paxTypeCode + currentIndex;
            console.log($('#txtFname' + paxTypeIndex).val());
            if ($('#txtFname' + paxTypeIndex).val() == '') {
                alertify.alert(flightComponent.Please_Fill_Label, flightComponent.Please_fill_first_Name_alert_msg, function () {
                    setValidateFocus('#txtFname' + paxTypeIndex);
                });
                return false;
            }
            var txtfname = $('#txtFname' + paxTypeIndex).val();
            if (txtfname.length < 1) {
                alertify.alert(flightComponent.Please_Fill_Label, flightComponent.FirstName_Min_Two_Char_alert_msg, function () {
                    setValidateFocus('#txtFname' + paxTypeIndex);
                });
                return false;
            }
            if (/\s\s+/gi.test(txtfname)) {
                alertify.alert("Invalid", "Please enter single space character for first name", function () {
                    setValidateFocus("#txtFname" + paxTypeIndex);
                });
                return false;
            }
            if (isNullorEmptyToBlank($('#txtSname' + paxTypeIndex).val()) == '') {
                alertify.alert(flightComponent.Please_Fill_Label, flightComponent.Please_fill_last_name_alert_msg, function () {
                    setValidateFocus('#txtSname' + paxTypeIndex);
                });
                return false;
            }
            var txtSname = $('#txtSname' + paxTypeIndex).val();
            if (txtSname.length < 2) {
                alertify.alert(flightComponent.Please_Fill_Label, flightComponent.SecondName_Min_Two_Char_alert_msg, function () {
                    setValidateFocus('#txtSname' + paxTypeIndex);
                });
                return false;
            }
            if (/\s\s+/gi.test(txtSname)) {
                alertify.alert("Invalid", "Please enter single space character for last name", function () {
                    setValidateFocus("#txtFname" + paxTypeIndex);
                });
                return false;
            }
            //  if (paxTypeCode != 'ADT') {
            if (isNullorEmptyToBlank($('#txtDob' + paxTypeIndex).val()) == '') {
                alertify.alert(flightComponent.Please_Fill_Label, flightComponent.Please_select_date_of_birth_alert, function () {
                    setValidateFocus('#txtDob' + paxTypeIndex);
                });
                return false;
            }
            // }
            if (stringIsNullorEmpty($('#txtnationality' + paxTypeIndex).val()) || $('#txtnationality' + paxTypeIndex).val() == "0") {
                alertify.alert(flightComponent.Please_Fill_Label, flightComponent.Please_select_nationality_alert_msg, function () {
                    setValidateFocus('#txtnationality' + paxTypeIndex);
                });
                return false;
            }
            if (!vm.vueCommonStore.state.commonRoles.hasPassportOptional) {

                if (isNullorEmptyToBlank($('#txtPassport' + paxTypeIndex).val()) == '') {
                    alertify.alert(flightComponent.Please_Fill_Label, flightComponent.Please_fill_passport_number_alert, function () {
                        setValidateFocus('#txtPassport' + paxTypeIndex);
                    });
                    return false;
                }
                var txtPn = $('#txtPassport' + paxTypeIndex).val();
                if (txtPn.length > 20) {
                    alertify.alert(flightComponent.Please_Fill_Label, flightComponent.Invalid_passport_number_alert, function () {
                        setValidateFocus('#txtPassport' + paxTypeIndex);
                    });
                    return false;
                }
                if (isNullorEmptyToBlank($('#txtExpireDate' + paxTypeIndex).val()) == '') {
                    alertify.alert(flightComponent.Please_Fill_Label, flightComponent.Please_select_expiry_date_alert, function () {
                        setValidateFocus('#txtExpireDate' + paxTypeIndex);
                    });
                    return false;
                }
                // if (isNullorEmptyToBlank($('#txtPassportIssue' + paxTypeIndex).val()) == '') {
                //     alertify.alert(flightComponent.Please_Fill_Label, flightComponent.Please_select_issue_date_alert, function () {
                //         setValidateFocus('#txtPassportIssue' + paxTypeIndex);
                //     });
                //     return false;
                // }
                if (stringIsNullorEmpty($('#ddlPassportIssue' + paxTypeIndex).val()) || $('#ddlPassportIssue' + paxTypeIndex).val() == "0") {
                    alertify.alert(flightComponent.Please_Fill_Label, flightComponent.Please_fill_issue_country_alert, function () {
                        setValidateFocus('#ddlPassportIssue' + paxTypeIndex);
                    });
                    return false;
                }
            }

        }
    }
    if (typeof item == "undefined") {
        if ($('input[name="credit"]:checked').length == 0) {
            alertify.alert(flightComponent.Terms_and_Conditions_Label, flightComponent.Choose_Payment_Mode_Alert, function () {});
            return false;
        }
    }
    if (typeof item == "undefined") {
        if ($("#chkAgree").prop('checked') == false) {
            alertify.alert(flightComponent.Terms_and_Conditions_Label, flightComponent.Please_agree, function () {});
            return false;
        }
    }
    var email=$('#txtLeadPaxEmail').val().trim();
    if (email) {
        var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        var matchArray = email.match(emailPat);
        if (matchArray == null) {
          alertify.alert('warning', 'Please enter a valid email address', function () {});
          return false;
        }
      }
    return isValidData;
}

function makeid(length, idtype) {
    var text = "";
    var possibleAlpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    var possibleNumeric = "0123456789";
    var possible = '';
    switch (idtype) {
        case 1:
            possible = possibleAlpha + possibleNumeric;
            break;
        case 0:
        case 2:
            possible = possibleAlpha;
            break;
        case 3:
            possible = possibleNumeric;
            break;
    }

    for (var i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

function getSuppliers(serviceName) {
    var suppliers = [];
    if (!isNullorUndefined(serviceName)) {
        $.each(vueCommonStore.state.agencyNode.loginNode.servicesList.filter(function (service) {
            return service.name === serviceName;
        })[0].provider, function (providerindex, provider) {
            suppliers.push(provider.id);
        });
    }
    return suppliers;
}

function bindPrivacyPolicyNTerms(siteList) {
    var termsNconditions = getContentTypeFromKey('termsnconditions', 'flightbookingtermsnconditions', siteList);
    termsNconditions = !stringIsNullorEmpty(termsNconditions) ? termsNconditions : 'No contents available';
    $('#termsNConditionsContents').append(termsNconditions);

    var privacyPolicy = getContentTypeFromKey('privacypolicy', 'flightbookingprivacypolicy', siteList);
    privacyPolicy = !stringIsNullorEmpty(privacyPolicy) ? privacyPolicy : 'No contents available';
    $('#privacypolicyContents').append(privacyPolicy);
}

function getContentTypeFromKey(contentType, keyValue, siteList) {
    var value = '';
    var siteContents = siteList;
    if (hasArrayData(siteContents)) {
        var contentTypes = siteContents[0].contentType.filter(function (type) {
            return type.description.toLowerCase().split(' ').join('') == contentType.toLowerCase() &&
                type.name.toLowerCase().split(' ').join('') == keyValue.toLowerCase()
        });
        if (hasArrayData(contentTypes)) {
            value = contentTypes[0].value;
        }
    }
    return value;
}

function validateEntriesforancillery() {

    var isValidData = true;

    if (isValidData) {
        var airSupplierResponse = selectResponseObj.content;
        var passengerTypeQuantity = airSupplierResponse.fareInformationWithoutPnrReply.passengerTypeQuantity;
        var adult = passengerTypeQuantity.adt;
        var child = passengerTypeQuantity.chd;
        var infant = passengerTypeQuantity.inf;
        var totalPassengers = parseInt(adult) + parseInt(child) + parseInt(infant);
        var adultIndex = 1;
        var childIndex = 1;
        var infantIndex = 1;
        var currentIndex = 1;
        for (var i = 1; i <= totalPassengers; i++) {
            var paxType = (i <= parseInt(adult) ? 'Adult' : (i > parseInt(adult) && (i <= parseInt(adult) + parseInt(child)) ? 'Child' : 'Infant'));
            var paxTypeCode = (i <= parseInt(adult) ? 'ADT' : (i > parseInt(adult) && (i <= parseInt(adult) + parseInt(child)) ? 'CHD' : 'INF'));
            switch (paxTypeCode) {
                case 'ADT':
                    currentIndex = adultIndex;
                    adultIndex++;
                    break;
                case 'CHD':
                    currentIndex = childIndex;
                    childIndex++;
                    break;
                case 'INF':
                    currentIndex = infantIndex;
                    infantIndex++;
                    break;
            }
            var paxTypeIndex = paxTypeCode + currentIndex;
            if (isNullorEmptyToBlank($('#txtFname' + paxTypeIndex).val()) == '') {
                alertify.alert(flightComponent.Please_Fill_Label, flightComponent.Please_fill_first_Name_alert_msg, function () {
                    setValidateFocus('#txtFname' + paxTypeIndex);
                });
                return false;
            }
            var txtfname = $('#txtFname' + paxTypeIndex).val();
            if (txtfname.length < 2) {
                alertify.alert(flightComponent.Please_Fill_Label, flightComponent.FirstName_Min_Two_Char_alert_msg, function () {
                    setValidateFocus('#txtFname' + paxTypeIndex);
                });
                return false;
            }
            if (isNullorEmptyToBlank($('#txtSname' + paxTypeIndex).val()) == '') {
                alertify.alert(flightComponent.Please_Fill_Label, flightComponent.Please_fill_last_name_alert_msg, function () {
                    setValidateFocus('#txtSname' + paxTypeIndex);
                });
                return false;
            }
            var txtSname = $('#txtSname' + paxTypeIndex).val();
            if (txtSname.length < 2) {
                alertify.alert(flightComponent.Please_Fill_Label, flightComponent.SecondName_Min_Two_Char_alert_msg, function () {
                    setValidateFocus('#txtSname' + paxTypeIndex);
                });
                return false;
            }


        }
    }

    return isValidData;
}

function setValidateFocus(id) {
    setTimeout(function () {
        $(id).focus();
    }, 500);
}

function Mealsfilter(type, Mealsitem) {
    if (type == "All") {
        $("#Mealslist-" + Mealsitem + " li").each(function () {
            $(this).show();
        });
    } else {
        $("#Mealslist-" + Mealsitem + " li").each(function () {
            if ($(this).attr("data-catogory") == type) {
                $(this).show();
            } else {
                $(this).hide();
            }

        });
    }

}

function mealSearch(key) {
    var keyvalue = $(key).val().toLowerCase();
    $("#meals_listall ul li").each(function () {
        var mealsname = $(this).attr("data-name").toLowerCase();
        if (mealsname.match(keyvalue)) {
            $(this).show();
        } else {
            $(this).hide();
        }
    });
}

function sortMeal(sortkey) {
    var chkid = $('input[type=radio][name=radio-btns-meals]:checked').attr('id');
    chkid = chkid.split('-')[1];
    if (sortkey === "1") {
        tinysort('ul#Mealslist-' + chkid + '>li', {
            order: (this.isAsc = !this.isAsc) ? 'asc' : 'desc',
            attr: 'data-name'
        });
    }
    if (sortkey === "0") {
        tinysort('ul#Mealslist-' + chkid + '>li', {
            order: (this.isAsc = !this.isAsc) ? 'asc' : 'desc',
            attr: 'data-price'
        });
    }
}

function SelectstyleOption(type) {
    var x, i, j, selElmnt, ax, b, c;
    /*look for any elements with the class "custom-select":*/
    if (type == "customselect") {
        x = document.getElementsByClassName("catogorie-custom-select");
    } else if (type == "sort") {
        x = document.getElementsByClassName("sort-custom-select");
    } else {
        x = document.getElementsByClassName("custom-select");
    }
    for (i = 0; i < x.length; i++) {
        selElmnt = x[i].getElementsByTagName("select")[0];
        if (x[i].childNodes.length > 1) {
            continue;
        }
        /*for each element, create a new DIV that will act as the selected item:*/
        ax = document.createElement("DIV");
        ax.setAttribute("class", "select-selected");
        ax.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
        x[i].appendChild(ax);

        /*for each element, create a new DIV that will contain the option list:*/
        b = document.createElement("DIV");
        b.setAttribute("class", "select-items select-hide");
        for (j = 0; j < selElmnt.length; j++) {
            /*for each option in the original select element,
            create a new DIV that will act as an option item:*/
            c = document.createElement("DIV");
            c.innerHTML = selElmnt.options[j].innerHTML;
            c.addEventListener("click", function (e) {
                /*when an item is clicked, update the original select box,
                and the selected item:*/
                var y, i, k, s, h;
                s = this.parentNode.parentNode.getElementsByTagName("select")[0];
                var itemindex = $(s).attr('data-item');
                h = this.parentNode.previousSibling;
                for (i = 0; i < s.length; i++) {
                    if (s.options[i].innerHTML == this.innerHTML) {
                        s.selectedIndex = i;
                        h.innerHTML = this.innerHTML;
                        y = this.parentNode.getElementsByClassName("same-as-selected");
                        for (k = 0; k < y.length; k++) {
                            y[k].removeAttribute("class");
                        }
                        if (type == "customselect") {
                            Mealsfilter(s.options[i].value, itemindex);
                        } else if (type == "sort") {
                            sortMeal(s.options[i].value);
                        }
                        this.setAttribute("class", "same-as-selected");
                        break;
                    }
                }
                h.click();
            });
            b.appendChild(c);
        }
        x[i].appendChild(b);
        ax.addEventListener("click", function (e) {
            /*when the select box is clicked, close any other select boxes,
            and open/close the current select box:*/
            e.stopPropagation();
            closeAllSelect(this);
            this.nextSibling.classList.toggle("select-hide");
            this.classList.toggle("select-arrow-active");
        });
    }

    function closeAllSelect(elmnt) {
        /*a function that will close all select boxes in the document,
        except the current select box:*/
        var x, y, i, arrNo = [];
        x = document.getElementsByClassName("select-items");
        y = document.getElementsByClassName("select-selected");
        for (i = 0; i < y.length; i++) {
            if (elmnt == y[i]) {
                arrNo.push(i)
            } else {
                y[i].classList.remove("select-arrow-active");
            }
        }
        for (i = 0; i < x.length; i++) {
            if (arrNo.indexOf(i)) {
                x[i].classList.add("select-hide");
            }
        }
    }
    /*if the user clicks anywhere outside the select box,
    then close all select boxes:*/
    document.addEventListener("click", closeAllSelect);
}