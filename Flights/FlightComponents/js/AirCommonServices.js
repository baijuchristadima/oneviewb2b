﻿function getAirportCodeFromName(qfrom) {
    var Tfrom = qfrom.substr(qfrom.lastIndexOf("(") + 1);
    var Farr = Tfrom.split(')');
    Tfrom = Farr[0];
    return Tfrom;
}

function airportFromAirportCodeSimple(airportCode) {
    var airportName = _.where(AirlinesTimezone, { I: airportCode.toUpperCase() });
    if (airportName == "") {
        airportName = airportCode;
    }
    else {
        airportName = airportName[0].N;
    }
    return airportName;
}


function airportFromAirportCode(airportCode) {
    var airportName = _.where(AirlinesTimezone, { I: airportCode.toUpperCase() });
    if (airportName == "") {
        airportName = airportCode;
    }
    else {
        airportName = airportName[0].N + ' (' + airportCode.toUpperCase() + ')';
    }
    return airportName;
}


function airportLocationFromAirportCode(airportCode) {
    var airportName = _.where(AirlinesTimezone, { I: airportCode.toUpperCase() });
    if (airportName == "") {
        airportName = airportCode;
    }
    else {
        airportName = airportName[0].C
    }
    return airportName;
}

function getAirLineName(airlineCode) {
    var airLineName = _.where(AirlinesDatas, { C: airlineCode.toUpperCase() });
    if (airLineName == "") {
        airLineName = airlineCode;
    }
    else {
        airLineName = airLineName[0].A;
    }
    return airLineName;
}

function getAirLineTimeZone(airlinecode) {
    var AirLineTimeZone = _.where(AirlinesTimezone, { I: airlinecode });
    if (AirLineTimeZone == "") {
        AirLineTimeZone = "--h:--m";
    }
    else {
        AirLineTimeZone = AirLineTimeZone[0].TZ;
    }
    return AirLineTimeZone;
}

function GetFlightdurationtime(FromGo, ToGo, DepTimeGo, ArriTimeGo) {
    var DepartureAirlineTimezone = getAirLineTimeZone(FromGo);
    var ArrivalAirlineTimezone = getAirLineTimeZone(ToGo);
    var DurationTimezone = DepartureAirlineTimezone - ArrivalAirlineTimezone;
    var DurationTimezoneMinutes = parseFloat(DurationTimezone) * 60;
    DurationTimezoneMilliseconds = parseFloat(DurationTimezoneMinutes) * 60000;
    var depariduration = moment(ArriTimeGo) - moment(DepTimeGo);
    var GrandDurationTimezone = depariduration + DurationTimezoneMilliseconds;
    GrandDurationTimezone = GrandDurationTimezone / 60000;
    var temphours = 0;
    var tempminutes = 0;
    var DTime = '';
    if (GrandDurationTimezone > 59) {
        temphours = Math.floor(GrandDurationTimezone / 60);
        tempminutes = GrandDurationTimezone - (temphours * 60);
        if (tempminutes == 0) {
            DTime = temphours + 'h ';
        }
        else {
            DTime = temphours + 'h ' + tempminutes + 'm';
        }

    }
    else {
        tempminutes = GrandDurationTimezone;
        DTime = tempminutes + 'm';
    }
    if (DTime.indexOf("N") > -1) {
        DTime = "";
    }

    return DTime;
}

function getGenderFromTitle(input) {
    var gender = paxTitles.filter(function (title) { return title.name.toUpperCase() == input.toUpperCase() });
    if (hasArrayData(gender)) { try { return gender[0].gender.toUpperCase(); } catch (err) { return ""; } } else { return ''; }
}

function getCabinName(cabinCode) {
    var cabinClass = 'Economy';
    if (cabinCode == "F") {
        cabinClass = "First Class";
    }
    else if (cabinCode == "C") {
        cabinClass = "Business";
    } else {
        try { cabinClass = getCabinClassObject(cabinCode).BasicClass; } catch (err) { }
    }
    return cabinClass;
}

function getAirCraftName(aireqpcode) {
    var AirCraftName = _.where(AirplanesData, { C: aireqpcode });
    if (AirCraftName == "") {
        AirCraftName = aireqpcode;
    }
    else {
        AirCraftName = AirCraftName[0].N;
    }
    return AirCraftName;
}

function get3letterCountryCode(input) {
    var countries = countryList.filter(function (country) { return country.twoLetter.toUpperCase() == input.toUpperCase() });
    if (hasArrayData(countries)) { try { return countries[0].threeLetter; } catch (err) { return ""; } } else { return ""; }
}

function getBaggageUnitString(baggageValue) {
    if (baggageValue.toLowerCase() == "pc(s)" || baggageValue.toLowerCase() == "piece" || baggageValue.toLowerCase() == "pieces" || baggageValue == "PC" || baggageValue == "P" || baggageValue == "N") {
        return { bagunitVal: "Piece", bagunitDesc: "Airline typically permit 23kg baggage weight per piece" }
    }
    else {
        return { bagunitVal: "Kg", bagunitDesc: "" }
    }
}

function calcLayoverTime(arrDate, arrTime, depDate, depTime) {
    var arrDateTime = moment(arrDate + arrTime, 'DDMMYYHHmm').format('YYYY-MM-DDTHH:mm');
    var depDateTime = moment(depDate + depTime, 'DDMMYYHHmm').format('YYYY-MM-DDTHH:mm');
    var DifferenceMilli = moment(depDateTime) - moment(arrDateTime);
    var duration = moment.duration(DifferenceMilli); return duration.hours() + 'h ' + duration.minutes() + 'm'
}

function isRefundableFlight(refundString) {
    if (refundString == "Yes") {
        return 'REFUNDABLE';
    }
    else if (refundString == "TICKETS ARE NON-REFUNDABLE" || refundString == "No") {
        // return 'NON-REFUNDABLE';
        return 'NO';
    }
    else if (refundString == "PENALTY APPLIES" || refundString == "PENALTY APPLIES - CHECK RULES" || refundString == "SUBJ TO CANCELLATION/CHANGE PENALTY") {
        // return 'PENALTY APPLIES';
        return 'REFUNDABLE';
    }
    else {
        return refundString;
    }
}