var oDestination = [];

Vue.component("flights-component", {
    //for websocket
    mixins: [websocketMixin],
    components: {
        'p-button': button
    },
    data: function () {
        return {
            //get roles from common store
            commonStore: vueCommonStore.state,
            commonAgencyCode: 'AGY75',
            //Result Page
            showLoaders: true,
            hasError: false,
            tripType: 'o',
            adtCount: '',
            chdCount: '',
            infCount: '',
            bookClass: '',
            dirFlight: false,
            selSuppliers: [],
            allFlightRes: [],
            filteredFlightRes: [],
            flightResBind: [],
            currentScrollLimit: 15,
            forCurrencyUpdatesOnly: false,
            DateFormate: 'dd/mm/yy',
            DateFormateConv: 'DD/MM/YYYY',
            //Sort
            sortValues: [{
                    isActive: true,
                    sortBy: "Price",
                    isAscending: false
                },
                {
                    isActive: false,
                    sortBy: "Duration",
                    isAscending: false
                },
                {
                    isActive: false,
                    sortBy: "Departure time",
                    isAscending: false
                },
                {
                    isActive: false,
                    sortBy: "Arrival time",
                    isAscending: false
                }
            ],
            //Filters
            showFilterLd: true,
            // minPrice: 0,
            // maxPrice: 1,
            lowestPrice: 0,
            highestPrice: 1,
            checkStops: [],
            resAirlines: [],
            resStops: [],
            checkAirlines: [],
            resDepLoc: [],
            selDeptLoc: [],
            resArrLoc: [],
            selArrtLoc: [],
            flSerList: [],
            selFlSerList: [],
            supplierConfig: globalConfigs.services.flights.upSellPlan.supplier,
            supplierConfigAvailability: globalConfigs.services.flights.availability.supplier,
            //Others
            cdnDomain: 'https://www.oneviewitsolutions.com',
            // summDepcity:'',
            // summArrcity:'',
            // summDatePax:'',
            summDepArrDatePax: '',
            mobileSort: {
                isAscending: true,
                name: "Price"
            },

            //Supplier Count
            // ameduesCount: 0,
            // mystiflyCount: 0,
            // salamairCount: 0,
            // flycreativeCount: 0,
            // airarabiaG9Count: 0,
            // airarabia3OCount: 0,
            // airarabiaE5Count: 0,
            // airarabia3LCount: 0,
            // piaHititCount: 0,
            // travelportgalileoCount: 0,
            // fludybaiCount: 0,
            // sabreCount: 0,
            // techmasterCount: 0,
            // aljazeeraCount: 0,
            //Main Comp Labels
            Sort_Results_by_Label: '',
            Price_Label: '',
            Duration_Label: '',
            From_Label: '',
            To_Label: '',
            Arrival_Label: '',
            Arrival_Date_Label: '',
            Adult_option_Label: '',
            Child_Option_Label: '',
            Infant_Option_Label: '',
            Trip_Label: '',
            Travel_Date_Label: '',
            Add_up_to_Label1: '',
            Add_up_to_Label2: '',
            Select_all_Label: '',
            Quotations_Label: '',
            Remove_Label: '',
            Check_all_Label: '',
            Quote_List_Label: '',
            Filters_Label: '',
            Reset_Label: '',
            Stop_Label: '',
            Only_Label: '',
            Airlines_Label: '',
            Departure_Time_Label: '',
            Timings_Label: '',
            Early_Morning_Label: '',
            Morning_Label: '',
            Mid_Day_Label: '',
            Night_Label: '',
            Arrival_Time_Label: '',
            Close_Filter_Label: '',
            Apply_Filters_Label: '',
            Return_Date_Label: '',
            Suppliers_Label: '',
            Sort_by_Label: '',
            Departure_Label: '',
            Lowest_Label: '',
            Highest_Label: '',
            Shortest_Label: '',
            Longest_Label: '',
            Earliest_Label: '',
            Latest_Label: '',
            Send_Label: '',
            Fare_info_Label: '',
            errorMessage: "Didn't find result for matching criteria. Please do change the criteria and search again.",

            //Flight Comp Labels
            Returning_Label: '',
            Non_stop_Label: '',
            Book_Now_Label: '',
            Flight_details_Label: '',
            Free_cabin_Label: '',
            checked_baggage_Label: '',
            Refund_policy_Label: '',
            Itinerary_Label: '',
            Baggage_Label: '',
            Amenities_Label: '',
            Category_Label: '',
            Baggage_Information_Label: '',
            Flight_From_Label: '',
            No_amenities_to_display_Label: '',
            Category_Information_Label: '',
            Add_Quote_Label: '',
            Net_Fare_Label: '',
            Base_Fare_Label: '',
            Taxes_Label: '',
            Fees_Label: '',
            Total_Label: '',
            Cabin_baggage_Label: '',
            Cabin_Label: '',
            Layover_at_Label: '',
            CheckIn_baggage_Label: '',
            RBD_Label: '',
            Non_Refundable_Label: '',
            Refundable_Label: '',
            MiniRule_Label: '',
            FareRule_Label: '',
            has_Mini_Rule_Role: false,
            has_Fare_Rule_Role: false,
            Discount_Label: '',
            Service_Fee_Label: '',
            Commission_Label: '',
            catogorySelected: false,
            catogoryFlightIndex1: null,
            catogoryFlightIndex2: null,
            catogoryFareID1: null,
            catogoryFareID2: null,
            You_Earn_Commission: null,
            Seat_Left_label: null,
            Upsell_label: null,
            Upsell_Tab_Label: null,
            Upsell_Details_Label: null,
            summary: '',
            Detail: '',
            after: '',
            before: '',
            rule: '',

            // Progress Bar
            loadingProgress: 0,
            percentWidth: null,
            progressBarStatus: true,
            // compinationDetails:{},
            // baggageInfoHT:{},
            filterType: 1,
            multipleCarrier: "",
            currentIndex: 0,
            countValidSupplierResponse: 0,
            requestCount: 0,
            isSummaryAvailable: true,
            calendarDepartureDate: null,
            calendarReturnDate: null,
            selectedCalendarDepartureDate: null,
            selectedCalendarReturnDate: null,
            groupedRetDates: {},
            groupedDepDates: [],
            calendarClicked: false,
            cellNum: null,
            calendarSearch: false,
            spinnerHtml: 'Fetching more flights<span class="loader__dot">.</span><span class="loader__dot">.</span><span class="loader__dot">.</span>',
            searchingNewDates: false,
            showPricingCalendar: true,
            searchForMore: false,

            // airArabia
            categoryFlag: false,
            orginalcategories: [],
            responseDetailCache: []
        };
    },
    created: function () {
        var self = this;

        this.getPagecontent();
        var urldata = this.getUrlVars().flight;
        if (urldata == undefined) {
            window.location.href = 'index.html';
        }
        this.connect(); //open websocket connection           

        var urldata = decodeURIComponent(urldata);
        urldata = urldata.substring(1, urldata.length).split('/');
        var calendarSearch = parseInt(urldata[urldata.length - 1].split('-')[11]);
        var searchRequest = this.getSearchData(0);
        setTimeout(() => {
            if (this.commonStore.commonRoles.hasCalendarSearch && parseInt(calendarSearch) == 1) {
                searchRequest = this.getSearchData(1);
                this.sendMessage(JSON.stringify(searchRequest)); //send request to web socket server
                setTimeout(() => {
                    searchRequest = this.getSearchData(0);
                    this.sendMessage(JSON.stringify(searchRequest)); //send request to web socket server
                }, 500);
            } else {
                searchRequest = this.getSearchData(0);
                this.sendMessage(JSON.stringify(searchRequest)); //send request to web socket server
            }
        }, 1000);
        this.Showsearchsummary(searchRequest);

        setTimeout(function () {
            if (self.allFlightRes.length == 0) {
                $('#divNoFlight').html(self.errorMessage);
                $('#divNoFlight').show();
                $('.airline_metrix').hide();
                self.showLoaders = false;
                percentWidth = 50;
                self.progressBarClose();
                self.hasError = true;
                self.disconnect();
                self.countValidSupplierResponse = self.requestCount;

            }
        }, 180000);
    },
    methods: {
        getCalendarSearch(flight) {

            var index = this.allFlightRes.findIndex(function (e) {
                if (e.groupOfFlights[1]) {
                    return e.fare.amount == flight.fare.amount &&
                        e.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime == flight.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime &&
                        e.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate == flight.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate &&
                        e.groupOfFlights[1].flightDetails[0].fInfo.dateTime.arrTime == flight.groupOfFlights[1].flightDetails[0].fInfo.dateTime.arrTime &&
                        e.groupOfFlights[1].flightDetails[0].fInfo.dateTime.arrDate == flight.groupOfFlights[1].flightDetails[0].fInfo.dateTime.arrDate &&
                        e.groupOfFlights[0].flightDetails[0].fInfo.flightNo == flight.groupOfFlights[0].flightDetails[0].fInfo.flightNo;
                } else {
                    return e.fare.amount == flight.fare.amount &&
                        e.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime == flight.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime &&
                        e.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate == flight.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate &&
                        e.groupOfFlights[0].flightDetails[0].fInfo.flightNo == flight.groupOfFlights[0].flightDetails[0].fInfo.flightNo;
                }

            });

            this.allFlightRes.splice(index, 1);

            this.searchingNewDates = true;
            var searchRequest = this.getSearchData(0);
            // this.Showsearchsummary(searchRequest);    
            // console.log(searchRequest);
            if (this.webSocketStatus == "Disconnected") {
                this.connect(); //open websocket connection           
            }

            this.sendMessage(JSON.stringify(searchRequest)); //send request to web socket server
        },
        moment: function (date, format) {
            return moment(date, format);
        },
        // progress Bar hide 
        progressBarClose: function () {

            var self = this;
            setTimeout(function () {
                self.progressBarStatus = false;
            }, 700);
        },
        getSearchData: function (type) {
            var urldata = this.getUrlVars().flight;
            if (urldata == undefined) {
                window.location.href = 'index.html';
            }
            urldata = decodeURIComponent(urldata);
            return this.createFlightSearchRequest(urldata.substring(1, urldata.length).split('/'), type);
        },
        getUrlVars: function () {
            var vars = [],
                hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        },
        createFlightSearchRequest: function (tripLeg, type) {
            var originDestinationInformationArr = [];
            for (var i = 0; i < tripLeg.length - 1; i++) {
                var Trip = tripLeg[i];
                var TripFrom = Trip.split('-')[0].toUpperCase();
                var TripTo = Trip.split('-')[1].toUpperCase();
                var TripDate = Trip.split('-')[2];
                if (i == 0) {
                    TripDate = this.selectedCalendarDepartureDate ? this.selectedCalendarDepartureDate : Trip.split('-')[2];
                } else if (i == 1) {
                    TripDate = this.selectedCalendarReturnDate ? this.selectedCalendarReturnDate : Trip.split('-')[2];
                } else {
                    TripDate = Trip.split('-')[2];
                }
                var TripArray = {
                    departureDate: moment(TripDate, 'DD|MM|YYYY').format("DD-MM-YYYY"),
                    originLocation: TripFrom,
                    destinationLocation: TripTo,
                    radiusInformation: {
                        _FromValue: '0',
                        _ToValue: '250'
                    }
                }
                originDestinationInformationArr.push(TripArray);
            }
            oDestination = originDestinationInformationArr;
            var triptypes = tripLeg[tripLeg.length - 1];
            var totalADT = triptypes.split('-')[0];
            var totalCHD = triptypes.split('-')[1];
            var totalINF = triptypes.split('-')[2];
            this.adtCount = parseInt(totalADT);
            this.chdCount = parseInt(totalCHD);
            this.infCount = parseInt(totalINF);

            var tripClass = triptypes.split('-')[3];
            if (tripClass.toLowerCase() == 'all') {
                tripClass = 'All';
            } else {
                tripClass = tripClass.toUpperCase();
            }
            this.bookClass = tripClass;
            var tripTyppee = triptypes.split('-')[7];
            this.tripType = tripTyppee.toLowerCase();
            var totalpax = parseInt(totalADT) + parseInt(totalCHD) + parseInt(totalINF);
            var supplierCode = triptypes.split('-')[4];
            if (supplierCode == 'all') {
                supplierCode = [];
            } else {
                supplierCode = supplierCode.split('|');
            }
            this.selSuppliers = supplierCode;
            nmberOfRec = triptypes.split('-')[5];
            var filter = false;
            var isfiltered = triptypes.split('-')[6];
            if (isfiltered == 'T') {
                filter = true;
            }
            var stopQuantity = triptypes.split('-')[10];
            if (stopQuantity.toLowerCase() == "df") {
                stopQuantity = "Direct";
                this.dirFlight = true;
            } else {
                stopQuantity = "All";
                this.dirFlight = false;
            }
            var preferAirline = [triptypes.split('-')[9]];
            if (preferAirline == "" || preferAirline == undefined) {
                preferAirline = [];
            }

            this.calendarSearch = parseInt(triptypes.split('-')[11]);
            this.calendarDepartureDate = tripLeg[0].split('-')[2];

            var date = this.calendarDepartureDate.split("|");
            var year = date[2].slice(2);
            var completeDate = date[0] + date[1] + year;
            if (this.selectedCalendarDepartureDate == null) {
                this.selectedCalendarDepartureDate = completeDate;
            }
            if (tripTyppee.toLowerCase() == "r") {
                this.calendarReturnDate = tripLeg[1].split('-')[2];

                var date1 = this.calendarReturnDate.split("|");
                var year1 = date1[2].slice(2);
                var completeDate1 = date1[0] + date1[1] + year1;
                if (this.selectedCalendarReturnDate == null) {
                    this.selectedCalendarReturnDate = completeDate1;
                }
            }
            var flightSearchRQ = {
                "request": {
                    "service": "FlightRQ",
                    "token": localStorage.accessToken,
                    "supplierCodes": this.selSuppliers,
                     "hqCode":this.commonStore.agencyNode.loginNode.solutionId ? this.commonStore.agencyNode.loginNode.solutionId :"",
                    "content": {
                        "command": type == 1 ? "CalendarSearchRQ" : "FlightSearchRQ",
                        "criteria": {
                            "criteriaType": "Air",
                            "commonRequestSearch": {
                                "numberOfUnits": parseInt(totalpax),
                                "typeOfUnit": "PX",
                                "numberOfRec": nmberOfRec,
                                "isFiltered": filter
                            },
                            "originDestinationInformation": originDestinationInformationArr,
                            "preferredAirline": preferAirline,
                            "nonStop": false,
                            cabin: tripClass,
                            "isRefundable": false,
                            "maxStopQuantity": stopQuantity,
                            "tripType": tripTyppee.toUpperCase(),
                            "preferenceLevel": "Preferred",
                            "target": "Test",
                            "passengerTypeQuantity": {
                                adt: parseInt(totalADT),
                                chd: parseInt(totalCHD),
                                inf: parseInt(totalINF)
                            },
                            "requestOption": null,
                            "pricingSource": null,
                            "dealcode": null,
                            "rangeOfDates": type == 1 ? 3 : undefined
                        },
                        "sort": [{
                            "field": "price",
                            "order": "asc",
                            "sequence": 1
                        }],
                        "filter": {
                            "noOfResults": "250",
                            "from": 1,
                            "to": 100,
                            "sequence": 1
                        },
                        "supplierSpecific": {}
                    }
                }
            }

            return flightSearchRQ;
        },
        getSeatCount: function (flightDet) {
            var svgIc = '<svg xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:cc="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" width="44.119675mm" height="48.810017mm" viewBox="0 0 44.119675 48.810017" version="1.1" id="svg8" inkscape:version="0.92.3 (2405546, 2018-03-11)"> <defs id="defs2" /> <sodipodi:namedview id="base" bordercolor="#666666" borderopacity="1.0" inkscape:pageopacity="0.0" inkscape:pageshadow="2" inkscape:zoom="2.8" inkscape:cx="73.414515" inkscape:cy="109.95125" inkscape:document-units="mm" inkscape:current-layer="layer1" showgrid="false" inkscape:window-width="1920" inkscape:window-height="1017" inkscape:window-x="-8" inkscape:window-y="-8" inkscape:window-maximized="1" fit-margin-top="0" fit-margin-left="0" fit-margin-right="0" fit-margin-bottom="0" /> <metadata id="metadata5"> <rdf:RDF> <cc:Work rdf:about=""> <dc:format>image/svg+xml</dc:format> <dc:type rdf:resource="http://purl.org/dc/dcmitype/StillImage" /> <dc:title></dc:title> </cc:Work> </rdf:RDF> </metadata> <g inkscape:label="Layer 1" inkscape:groupmode="layer" id="layer1" transform="translate(-81.116032,-108.43448)"> <path style="opacity:1;fill-opacity:1;stroke:none;stroke-width:0.99999994;stroke-opacity:1" d="m 331.50195,409.83398 c -3.38257,-0.0635 -12.32813,0.93359 -19.35937,1.79297 -8.03572,0.98215 -5,8.03516 -5,8.03516 l 19.28515,57.85742 c 0,0 1.34097,3.39202 4.64454,12.05274 3.30355,8.66071 3.39257,15.98242 3.39257,15.98242 0,0 -0.71455,44.19782 -0.35742,57.41211 C 334.46457,576.18108 345,575.55469 345,575.55469 c 0,0 112.50055,-0.0879 121.33984,-0.0879 8.8393,0 6.78516,-8.92969 6.78516,-8.92969 0,0 -2.58927,-17.14177 -3.75,-25.44531 -1.16069,-8.30355 -8.21484,-8.66211 -8.21484,-8.66211 0,0 -6.33958,-1.24889 -12.23243,-2.32031 -5.89285,-1.07146 -8.30273,-7.32227 -8.30273,-7.32227 0,0 -7.41014,-15.89174 -12.41016,-27.32031 -4.99997,-11.42858 -14.82226,-10.89453 -14.82226,-10.89453 0,0 -29.99916,0.002 -39.55274,0.26953 -9.55356,0.26785 -12.76757,-9.55469 -12.76757,-9.55469 0,0 -21.42941,-54.99971 -22.94727,-59.01758 -1.51786,-4.01786 -3.66016,-5.8934 -5.53516,-6.33984 -0.23437,-0.0558 -0.60466,-0.0866 -1.08789,-0.0957 z m 19.02149,1.91797 c -5.93464,0.37879 -6.94532,0.25196 -6.94532,0.25196 l 16.91993,43.18359 6.1875,-2.9043 c 0,0 4.92372,-0.37834 2.01953,-8.20703 -2.90419,-7.82868 -9.5957,-26.26367 -9.5957,-26.26367 0,0 -2.65129,-6.43937 -8.58594,-6.06055 z m 86.61914,86.61914 6.0625,13.63672 h 16.28711 c 0,0 2.65294,0.50691 2.40039,-2.90234 -0.25247,-3.40929 -0.25196,-8.20899 -0.25196,-8.20899 0,0 0.25165,-2.27216 -3.03124,-2.39843 -3.28302,-0.12624 -21.46485,-0.12696 -21.46485,-0.12696 z m 28.30469,82.8086 -119.375,0.53711 -0.35743,-0.35742 v 7.41015 c 0,6.07143 5.35743,5.44727 5.35743,5.44727 0,0 103.48104,-0.0905 109.19531,0.0879 5.7143,0.17858 5.17969,-4.28516 5.17969,-4.28516 z" transform="scale(0.26458333)" id="path826" inkscape:connector-curvature="0" /> </g> </svg>';
            var numSeats = Math.min.apply(Math, flightDet.map(function (o) {
                return parseInt(o.fInfo.avlStatus);
            }))
            if (numSeats > 0) {
                if (numSeats <= 2) {
                    return ' <p class="red_svg">' + svgIc + numSeats + " " + this.Seat_Left_label + '</p>';
                }
                if (numSeats > 2 && numSeats <= 4) {
                    return ' <p class="orange_svg">' + svgIc + numSeats + " " + this.Seat_Left_label + '</p>';
                } else {
                    return ' <p class="green_svg">' + svgIc + numSeats + " " + this.Seat_Left_label + '</p>';
                }
            }
        },
        splitOfficeId: function (officeIdList) {
            var newofficeIdList = officeIdList;
            try {
                if (newofficeIdList != undefined) {
                    newofficeIdList.forEach(office => {
                        try {
                            office.splittedOfficeId = office.officeId.split(':')[0];
                        } catch (err) {
                            office.splittedOfficeId = office.officeId;
                        }
                        if (office.splittedOfficeId == undefined) {
                            office.splittedOfficeId = office.officeId;
                        }
                        try {
                            office.pcc = office.officeId.split(':')[1];
                        } catch (err) {
                            office.pcc = office.officeId;
                        }
                        if (office.pcc == undefined) {
                            office.pcc = office.officeId;
                        }
                    });
                }
            } catch (err) {
                newofficeIdList = [{
                    officeId: '',
                    splittedOfficeId: '',
                    pcc: ''
                }];
            }
            return newofficeIdList;
        },
        gettingCategoryValue: function (type, fareid, fareCode, flRes) {
            var returnValue = "At a charge";
            if (flRes.baggageInfoHT != undefined && flRes.baggageInfoHT[fareid] != undefined) {
                // console.log(" fareid : "+fareid);
                var allBaggages = flRes.baggageInfoHT[fareid];
                if (allBaggages.length > 0) {
                    allBaggages = allBaggages[0];
                    if (allBaggages != undefined && allBaggages.baggage != undefined && allBaggages.baggage != '' && allBaggages.unit != undefined && allBaggages.unit != '') {
                        returnValue = allBaggages.baggage + " " + allBaggages.unit;
                    }
                }
            } else {
                if (fareCode != undefined && fareCode.toLowerCase() == "lite") {
                    returnValue = "At a charge";
                } else if (fareCode != undefined && fareCode.toLowerCase() == "value") {
                    returnValue = "20 Kg";
                } else if (fareCode != undefined && fareCode.toLowerCase() == "flex") {
                    returnValue = "30 Kg";
                } else if (fareCode != undefined && fareCode.toLowerCase() == "business") {
                    returnValue = "40 Kg";
                }
            }
            return returnValue;

        },
        processFlightResult: function (response) {
            var self = this;
            if (response.requestDetails) {
                this.requestCount += parseInt(response.requestDetails.count);
            } else {
                if(response.response.content.totalPages) {
                    var responseKey = response.response.supplierCode + "|" + JSON.stringify(response.response.selectCredential);
                    if (self.responseDetailCache.indexOf(responseKey) == -1) {
                        self.responseDetailCache.push(responseKey);
                        self.requestCount = parseInt(self.requestCount) + parseInt(response.response.content.totalPages) - 1;
                        // console.log(self.requestCount);
                    }
                }
                // console.log({"A": self.countValidSupplierResponse});
                var now = moment(new Date());
                //responseObj.response.token=response.response.token;
                //response= responseObj;
                //this.allFlightRes=[];

                // console.log(response);
                // console.log('Supplier : ' + response.response.supplierCode);
                localStorage.accessToken = response.response.token;

                $('#divNoFlight').hide();
                $('#divNoFlight').html('');
                this.countValidSupplierResponse = this.countValidSupplierResponse + 1;

                if (isNullorUndefined(response) || isNullorUndefined(response.response) || isNullorUndefined(response.response.content) || isNullorUndefined(response.response.content.fareMasterPricerTravelBoardSearchReply) || isNullorUndefined(response.response.content.fareMasterPricerTravelBoardSearchReply.flightIndex)) {
                    if (!isNullorUndefined(response) && !isNullorUndefined(response.response) && !isNullorUndefined(response.response.content) && !isNullorUndefined(response.response.content.error) && !isNullorUndefined(response.response.content.error)) {
                        self.errorMessage = response.response.content.error.message;
                        if (this.selSuppliers.length == 1 && (this.selSuppliers == "44" || this.selSuppliers == "49" || this.selSuppliers == "50" || this.selSuppliers == "54" || this.selSuppliers == "71")) {
                            this.percentWidth = 50;
                            $('#divNoFlight').html(self.errorMessage || "Didn't find result for matching criteria. Please do change the criteria and search again.");
                            $('#divNoFlight').show();
                            $('.airline_metrix').hide();
                            self.progressBarClose();
                            self.showLoaders = false;
                        }
                    }
                } else if (response.response.content.fareMasterPricerTravelBoardSearchReply.flightIndex != null) {
                    this.hasError = false;
                    // localStorage.supplierSpecific = JSON.stringify({ "supplierSpecific": response.response.content.supplierSpecific });
                    // localStorage.selectCredential = JSON.stringify({ "selectCredential":  response.response.selectCredential });
                    var selectCredential = JSON.parse(JSON.stringify(response.response.selectCredential));
                    var currency = [];
                    try {
                        currency = _.filter(self.commonStore.currencyRates, function (e) {
                            return response.response.selectCredential.officeIdList[0].currency == e.fromCurrencyCode;
                        });
                    } catch (error) {
                        currency = [];
                        selectCredential.officeIdList = [{
                            multiplier: 1,
                            currency: self.commonStore.agencyNode.loginNode.currency
                        }];
                    }
                    if (currency.length > 0) {
                        selectCredential.officeIdList[0].multiplier = currency[0].sellRate;
                    } else {
                        selectCredential.officeIdList[0].multiplier = 1;
                    }

                    response.response.content.fareMasterPricerTravelBoardSearchReply.flightIndex.forEach(function (fi) {

                        if (fi.segmentRef.supplier == 54 || fi.segmentRef.supplier == 57 || fi.segmentRef.supplier == 62 || fi.segmentRef.supplier == 72 || fi.segmentRef.supplier == 44 || fi.segmentRef.supplier == 49 || fi.segmentRef.supplier == 50 || fi.segmentRef.supplier == 71) {
                            if (response.response.content.supplierSpecific != undefined && response.response.content.supplierSpecific.compinationDetails != undefined) {
                                if (fi.segmentRef.supplier == 72) {
                                    fi.compinationDetails = response.response.content.supplierSpecific[fi.segmentRef.supplier].compinationDetails
                                } else {
                                    fi.compinationDetails = response.response.content.supplierSpecific.compinationDetails;
                                }

                                if (fi.compinationDetails == undefined) {
                                    fi.compinationDetails = {};
                                }
                                // delete response.response.content.supplierSpecific.compinationDetails; 
                            } else {
                                fi.compinationDetails = {};
                            }
                            var mappedDetails = [];
                            if (response.response.content.supplierSpecific != undefined && response.response.content.supplierSpecific.mappingDetails != undefined) {
                                if (fi.segmentRef.supplier == 72) {
                                    mappedDetails = response.response.content.supplierSpecific[fi.segmentRef.supplier].mappingDetails;
                                } else {
                                    mappedDetails = response.response.content.supplierSpecific.mappingDetails;
                                }

                                if (fi.mappedDetails == undefined) {
                                    fi.mappedDetails = [];
                                }
                                //delete response.response.content.supplierSpecific.mappingDetails; 
                            }
                            fi.baggageInfoHT = mappedDetails.Baggage_Info;
                            fi.filghtFareIndexHT = mappedDetails.Segment_Info;

                        } else {
                            fi.compinationDetails = {};
                        }

                        fi.selectCredential = selectCredential;
                        fi.selectCredentialBooking = JSON.parse(JSON.stringify(response.response.selectCredential));
                        fi.supplierSpecificContent = response.response.content.supplierSpecific;
                    });

                    var flights = response.response.content.fareMasterPricerTravelBoardSearchReply.flightIndex;

                    // progress bar
                    this.percentWidth = (1 / flights.length) * 100;
                    //validation prevents progress bar to move backwards
                    if (this.loadingProgress < this.percentWidth * 100) {
                        this.loadingProgress = (this.percentWidth * 100);
                    }
                    this.progressBarClose();

                    for (let i = 0; i < flights.length; i++) {
                        var flresponse = flights[i];
                        flights[i].noAvailabilityResponse = false;

                        // flights[i].need2ShowCategory=false;
                        // flights[i].selFareID1="1";
                        // flights[i].selFareID2="";
                        if (this.commonStore.commonRoles.hasShowMarkupComponent) {
                            var total = (flights[i].fare.serviceFee || 0) - (flights[i].fare.discount || 0);
                            if (total < 0) {
                                flights[i].serviceFee = "Discount " + i18n.n(Math.abs(total) / this.commonStore.currencyMultiplier, 'currency', this.commonStore.selectedCurrency) + " included";
                            } else if (total > 0) {
                                flights[i].serviceFee = "Service fee " + i18n.n(Math.abs(total) / this.commonStore.currencyMultiplier, 'currency', this.commonStore.selectedCurrency) + " included";
                            }
                        }
                        flights[i].refundpolicy = this.getRefundDetails(flights[i].description.pricingMsg, flights[i].segmentRef.supplier);
                        for (var k = 0; k < flights[i].groupOfFlights.length; k++) {
                            if (k == 0) {
                                if (flights[i].segmentRef.supplier == 54) {
                                    var categoryFZ = flights[i].category.filter(function (el) {
                                        return el.tripId == 0;
                                    });
                                    var tmpDepCat =[];
                                    categoryFZ = _.groupBy(categoryFZ, "fbCode");
                                    $.each(categoryFZ, function (key, value) {
                                        var minFare = _.min(value, function (e) {
                                            return e.amount;
                                        });
                                        tmpDepCat.push(minFare);
                                    });
                                    categoryFZ = tmpDepCat.sort((a, b) => parseFloat(a.amount) - parseFloat(b.amount));;
                                    flights[i]["selFareID" + (k + 1)] = categoryFZ[0].fareId;
                                } else {
                                    flights[i]["selFareID" + (k + 1)] = "1";
                                }
                            } else {
                                flights[i]["selFareID" + (k + 1)] = "";
                            }
                        }

                        if (flresponse.category != undefined) {
                            // var category=flresponse.category.filter(function (el) { return el.tripId == 0;});
                            // if(category.length>0){
                            //     flights[i].selFareID1=category[0].fareId;
                            // }
                            // category=flresponse.category.filter(function (el) { return el.tripId == 1;});
                            // if(category.length>0){
                            //     flights[i].selFareID2=category[0].fareId;
                            // }
                            for (var m = 0; m < flights[i].groupOfFlights.length; m++) {
                                var category = flresponse.category.filter(function (el) {
                                    return el.tripId == m;
                                });
                                if (category.length > 0) {
                                    if (m == 0) {
                                        if (flights[i].segmentRef.supplier == 57 || flights[i].segmentRef.supplier == 62) {
                                            if (category.length > 1) {
                                                try {
                                                    amount = Math.min.apply(Math, category.map(function (o) {
                                                        return o.amount;
                                                    }))
                                                    var id = category.filter(function (el) {
                                                        return el.amount == amount;
                                                    })
                                                    flights[i]["selFareID" + (m + 1)] = id[0].fareId;
                                                } catch (error) {
                                                    flights[i]["selFareID" + (m + 1)] = category[0].fareId;
                                                }
                                            } else {
                                                flights[i]["selFareID" + (m + 1)] = category[0].fareId;
                                            }
                                        } else {
                                            if (flights[i].segmentRef.supplier == 54) {
                                                var categoryFZ = flights[i].category.filter(function (el) {
                                                    return el.tripId == 0;
                                                });
                                                var tmpDepCat =[];
                                                categoryFZ = _.groupBy(categoryFZ, "fbCode");
                                                $.each(categoryFZ, function (key, value) {
                                                    var minFare = _.min(value, function (e) {
                                                        return e.amount;
                                                    });
                                                    tmpDepCat.push(minFare);
                                                });
                                                categoryFZ = tmpDepCat.sort((a, b) => parseFloat(a.amount) - parseFloat(b.amount));;
                                                flights[i]["selFareID" + (m + 1)] = categoryFZ[0].fareId;
                                            } else {
                                                flights[i]["selFareID" + (m + 1)] = category[0].fareId;
                                            }
                                        }
                                    } else {
                                        if (_.isEmpty(flights[i].compinationDetails)) {
                                            flights[i]["selFareID" + (m + 1)] = category[0].fareId;
                                        } else {
                                            // flights[i]["selFareID" + (m + 1)] = self.compinationDetails[flights[i]['selFareID'+m]][0];
                                            var allSecCategory = self.getPairList(flights[i].category.filter(function (el) {
                                                return el.tripId == m;
                                            }), flights[i], m);
                                            if (allSecCategory.length > 0) {
                                                allSecCategory = allSecCategory[0];
                                                if (flights[i].segmentRef.supplier == 54) {
                                                    Vue.set(flights[i], "selFareIDtmp", allSecCategory.newfareId);
                                                    // flights[i]["selFareIDtmp"] = allSecCategory.newfareId;
                                                }
                                                flights[i]["selFareID" + (m + 1)] = allSecCategory.fareId;
                                            }
                                        }
                                    }
                                }
                            }

                            // var recID = Number(i) + 1;
                            // if (flights[i].filghtFareIndexHT != undefined && flights[i].filghtFareIndexHT[recID] != undefined && parseInt(flights[i].segmentRef.supplier) == 54) {
                            //     // var segInfo=filghtFareIndexHT[recID];
                            //     // if(segInfo!=undefined&&segInfo[0]!=undefined&&segInfo[1]!=undefined){
                            //     //     flights[i].selFareID1=segInfo[0];
                            //     //     flights[i].selFareID2=segInfo[1];
                            //     // }
                            //     for (var e = 0; e < flights[i].groupOfFlights.length; e++) {
                            //         var segInfo = flights[i].filghtFareIndexHT[recID];
                            //         if (segInfo != undefined && segInfo[e] != undefined) {
                            //             flights[i]["selFareID" + (e + 1)] = segInfo[e];
                            //         }
                            //     }
                            // }
                            self.categoryChangeOption(flresponse);
                            // if (flights[i].segmentRef.supplier != 54) {
                            self.changeBaggageAndCabin(flresponse, i);
                            // }
                        }
                        this.allFlightRes.push(flights[i]);
                        // switch (parseInt(flights[i].segmentRef.supplier)) {
                        //     case 1:
                        //         this.ameduesCount++;
                        //         break;
                        //     case 3:
                        //         this.mystiflyCount++;
                        //         break;
                        //     case 18:
                        //         this.salamairCount++;
                        //         break;
                        //     case 42:
                        //         this.flycreativeCount++;
                        //         break;
                        //     case 44:
                        //         this.airarabiaG9Count++;
                        //         break;
                        //     case 49:
                        //         this.airarabia3OCount++;
                        //         break;
                        //     case 50:
                        //         this.airarabiaE5Count++;
                        //         break;
                        //     case 45:
                        //         this.travelportgalileoCount++;
                        //         break;
                        //     case 54:
                        //         this.fludybaiCount++;
                        //         break;
                        //     case 55:
                        //         this.sabreCount++;
                        //         break;
                        //     case 64:
                        //         this.techmasterCount++;
                        //         break;
                        //     case 69:
                        //         this.aljazeeraCount++;
                        //         break;
                        //     case 71:
                        //         this.airarabia3LCount++;
                        //         break;
                        //     case 57:
                        //         this.piaHititCount++;
                        //         break;
                        // }
                    }
                    var vm = this;

                    this.allFlightRes = this.allFlightRes.sort((a, b) => a.fare.amount - b.fare.amount);
                    if (this.tripType.toLowerCase() == 'r') {
                        this.filteredFlightRes = _.filter(this.allFlightRes, function (e) {
                            return e.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate == vm.selectedCalendarDepartureDate &&
                                e.groupOfFlights[1].flightDetails[0].fInfo.dateTime.depDate == vm.selectedCalendarReturnDate;
                        });
                    } else {
                        this.filteredFlightRes = _.filter(this.allFlightRes, function (e) {
                            return e.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate == vm.selectedCalendarDepartureDate;
                        });
                    }
                    this.flightResBind = this.filteredFlightRes.slice(0, this.currentScrollLimit);

                    //CSS Scripts

                    this.$nextTick(function () {
                        if (vm.allFlightRes.length > 0) {
                            //Get Filter Data
                            var lowestPrice = Math.min.apply(Math, vm.allFlightRes.map(function (o) {
                                return o.fare.amount;
                            }))
                            var highestPrice = Math.max.apply(Math, vm.allFlightRes.map(function (o) {
                                return o.fare.amount;
                            }))

                            if (highestPrice == lowestPrice) {
                                highestPrice = parseFloat(highestPrice) + 1;
                            }

                            try {
                                lowestPrice = i18n.n(lowestPrice / vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency).match(/\d+(\.\d+)?/g).join('');
                                highestPrice = i18n.n(highestPrice / vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency).match(/\d+(\.\d+)?/g).join('');
                            } catch (error) {}
                            // vm.minPrice = lowestPrice;
                            // vm.maxPrice = highestPrice;



                            document.getElementById("property-price-lower").innerHTML = lowestPrice;
                            document.getElementById("property-price-upper").innerHTML = highestPrice;

                            var slider = document.getElementById("property-price-range");

                            if (slider.noUiSlider) {
                                slider.noUiSlider.updateOptions({
                                    tooltips: [true, true],
                                    start: [parseFloat(lowestPrice), parseFloat(highestPrice)],
                                    range: {
                                        'min': parseFloat(lowestPrice),
                                        'max': parseFloat(highestPrice)
                                    }
                                });
                            } else {
                                noUiSlider.create(slider, {
                                    start: [parseFloat(lowestPrice), parseFloat(highestPrice)],
                                    connect: true,
                                    tooltips: [true, true],
                                    range: {
                                        'min': [parseFloat(lowestPrice)],
                                        'max': [parseFloat(highestPrice)]
                                    }
                                })

                                slider.noUiSlider.on('update', function (values) {
                                    vm.lowestPrice = values[0];
                                    vm.highestPrice = values[1];
                                });

                            }
                        }
                        $('[data-toggle="tooltip"]').tooltip();
                        this.showLoaders = false;
                        setTimeout(function () {
                            metrixAir()
                        }, 100);
                        $('a[data-toggle="tab"]').click(function (e) {
                            var display = "block";
                            if (e.target.text != undefined && e.target.text == 'Category') {
                                display = "none";
                            }

                            $(".bookDetails").each(function () {
                                $(this).css("display", display);
                            });
                            //just for demo

                        });
                    });

                    //Gen Filter Data
                    var end = moment(new Date());
                    // console.log(moment.duration(end.diff(now)).asSeconds());
                    var calendarDepartureDates = [];

                    this.allFlightRes.forEach(fl => {
                        fl.groupOfFlights.forEach(leg => {
                            var calendarData = {};
                            if (fl.groupOfFlights[1]) {
                                calendarData = {
                                    depDate: fl.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate,
                                    depDateRet: fl.groupOfFlights[1].flightDetails[0].fInfo.dateTime.depDate,
                                    fare: fl.fare.amount,
                                    description: fl.description,
                                    airline: this.getAirLineName(fl.groupOfFlights[1].flightDetails[0].fInfo.companyId.mCarrier)
                                }
                            } else {
                                calendarData = {
                                    depDate: fl.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate,
                                    fare: fl.fare.amount,
                                    description: fl.description,
                                    airline: this.getAirLineName(fl.groupOfFlights[0].flightDetails[0].fInfo.companyId.mCarrier)
                                }
                            }
                            calendarDepartureDates.push(calendarData);
                        });
                    });

                    var calendarDepartureDatesGrouped = _.groupBy(calendarDepartureDates, "depDate");
                    var tmpDepCal = [];
                    $.each(calendarDepartureDatesGrouped, function (key, value) {
                        var minFare = _.min(value, function (e) {
                            return e.fare;
                        });
                        tmpDepCal.push({
                            depDate: key,
                            fare: minFare.fare,
                            description: minFare.description,
                            airline: minFare.airline,
                        });
                    });

                    this.processMetrix();

                    $(".highlight").removeClass("highlighted");
                    $(".t-header").removeClass("h-highlighted");
                    $(".owl-item .item").removeClass("active");
                    if (this.tripType.toLowerCase() == 'r') {
                        var date = this.selectedCalendarDepartureDate;
                        var date1 = this.selectedCalendarReturnDate;

                        var newObj = _.groupBy(_.sortBy(calendarDepartureDates, function (a) {
                            return moment(a.depDate, 'DDMMYY').valueOf();
                        }), "depDateRet");
                        var tempArr = [];
                        Object.keys(newObj)
                            .sort(function (a, b) {
                                return moment(a, 'DDMMYY').valueOf() - moment(b, 'DDMMYY').valueOf();
                            }).forEach((a) => {
                                tempArr.push({
                                    name: a,
                                    value: newObj[a]
                                })
                            });
                        if (tempArr.length == 7) {
                            this.groupedRetDates = tempArr;
                        } else {
                            var newArr = [];
                            for (var x = 0; x < 7; x++) {
                                var data = [];
                                try {
                                    var dt = moment(vm.calendarReturnDate, 'DD|MM|YYYY').subtract(3, 'days').add(x, 'days').format('DDMMYY');
                                    var data = _.filter(tempArr, function (e) {
                                        return e.name == dt;
                                    });
                                } catch (error) {}

                                if (data.length == 0) {
                                    newArr.push({
                                        name: dt,
                                        value: []
                                    })
                                } else {
                                    newArr.push(data[0]);
                                }
                                this.groupedRetDates = newArr;
                            }
                        }


                        setTimeout(() => {
                            $("#cell-" + date1 + "-" + date).addClass("highlighted");
                            // console.log({
                            //     date1
                            // });
                            // console.log({
                            //     date
                            // });
                            $("#r-header-" + date1).addClass("h-highlighted");
                            $("#header-" + date).addClass("h-highlighted");
                        }, 200);
                    }
                    var date = this.selectedCalendarDepartureDate;
                    this.groupedDepDates = tmpDepCal.sort(function (a, b) {
                        return moment(a.depDate, 'DDMMYY').valueOf() - moment(b.depDate, 'DDMMYY').valueOf();
                    });

                    if (tmpDepCal.length == 7) {
                        this.groupedDepDates = tmpDepCal;
                    } else {
                        var newArr = [];
                        for (var x = 0; x < 7; x++) {
                            var data = [];
                            try {
                                var dt = moment(vm.calendarDepartureDate, 'DD|MM|YYYY').subtract(3, 'days').add(x, 'days').format('DDMMYY');
                                var data = _.filter(tmpDepCal, function (e) {
                                    return e.depDate == dt;
                                });
                            } catch (error) {}

                            if (data.length == 0) {
                                newArr.push({
                                    depDate: "",
                                    fare: "",
                                    description: "",
                                    airline: "NA",
                                })
                            } else {
                                newArr.push(data[0]);
                            }
                            this.groupedDepDates = newArr;
                        }
                    }

                    // this.groupedDepDates = _.sortBy(tmpDepCal, function(e){ return e.depDate; });
                    setTimeout(() => {
                        $("#cell-" + date).addClass("highlighted");
                        $("#header-" + date).addClass("h-highlighted");
                        // console.log({
                        //     date
                        // });
                    }, 200);

                    this.searchForMore = false;

                    this.flSerList = _.where(this.commonStore.agencyNode.loginNode.servicesList, {
                        name: "Air"
                    })[0];

                    this.flSerList.provider = this.flSerList.provider.filter(supplier => supplier.supplierType == 'Test' || supplier.supplierType == 'Production').
                    sort(function (a, b) {
                        if (a.name.toLowerCase() < b.name.toLowerCase()) {
                            return -1;
                        }
                        if (a.name.toLowerCase() > b.name.toLowerCase()) {
                            return 1;
                        }
                        return 0;
                    })
                }
                this.showFilterLd = false;
                var end = moment(new Date());
                // console.log(moment.duration(end.diff(now)).asSeconds());
                // setTimeout(function () { metrixAir() }, 100);
                this.searchingNewDates = false;
                if (this.requestCount == this.countValidSupplierResponse) {
                    this.disconnect();
                    if (this.allFlightRes.length == 0) {
                        this.showLoaders = false;
                        this.progressBarClose();
                        $('#divNoFlight').html(self.errorMessage || "Didn't find result for matching criteria. Please do change the criteria and search again.");
                        $('#divNoFlight').show();
                        $('.airline_metrix').hide();
                    }
                    // console.log(this.webSocketStatus)
                    this.countValidSupplierResponse = this.requestCount;
                }
            }
        },
        processMetrix: function () {
            var vm = this;
            var tmp = this.allFlightRes;
            if (this.tripType.toLowerCase() == 'r') {
                tmp = _.filter(this.allFlightRes, function (e) {
                    return e.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate == vm.selectedCalendarDepartureDate &&
                        e.groupOfFlights[1].flightDetails[0].fInfo.dateTime.depDate == vm.selectedCalendarReturnDate;
                });
            } else {
                tmp = _.filter(this.allFlightRes, function (e) {
                    return e.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate == vm.selectedCalendarDepartureDate;
                });
            }

            var tempAirlines = [];
            var tempStops = [];
            var tempDepLoc = [];
            var tempArrLoc = [];
            var multipleCarrier = [];
            tmp.forEach(fl => {
                fl.groupOfFlights.forEach(leg => {
                    tempStops.push(leg.flightDetails.length - 1)
                    tempDepLoc.push(leg.flightDetails[0].fInfo.location.locationFrom)
                    tempArrLoc.push(leg.flightDetails[leg.flightDetails.length - 1].fInfo.location.locationTo)
                    if (!leg.flightDetails.every((val, i, arr) => val.fInfo.companyId.mCarrier === arr[0].fInfo.companyId.mCarrier)) {
                        leg.flightDetails.forEach(seg => {
                            multipleCarrier.push(fl.fare.amount);
                        });
                    }
                    leg.flightDetails.forEach(seg => {
                        if (seg.fInfo.stopQuantity > 0) {
                            tempStops.push(99);
                        }
                    });
                    // leg.flightDetails.forEach(seg => {
                    //     tempAirlines.push({
                    //         air: seg.fInfo.companyId.mCarrier,
                    //         price: fl.fare.amount,
                    //         multiplier: fl.selectCredential.officeIdList[0].multiplier,
                    //         currency: fl.selectCredential.officeIdList[0].currency
                    //     });
                    // });
                    if (leg.flightDetails.every((val, i, arr) => val.fInfo.companyId.mCarrier === arr[0].fInfo.companyId.mCarrier)) {
                        tempAirlines.push({
                            air: leg.flightDetails[0].fInfo.companyId.mCarrier,
                            price: fl.fare.amount,
                            multiplier: fl.selectCredential.officeIdList[0].multiplier,
                            currency: fl.selectCredential.officeIdList[0].currency
                        });
                    }
                });
            });

            this.multipleCarrier = multipleCarrier;
            tempAirLine = [];
            airPrice = [];
            var resAirlines1 = _.groupBy(tempAirlines, "air");

            $.each(resAirlines1, function (key, value) {
                var airname = key;
                $.each(value, function (key1, value1) {
                    airPrice.push(value1.price);
                });
                if (vm.commonStore.selectedCurrency == '') {
                    tempAirLine.push({
                        air: airname,
                        price: airPrice,
                        multiplier: value[0].multiplier,
                        currency: value[0].currency
                    });
                } else {
                    tempAirLine.push({
                        air: airname,
                        price: airPrice,
                        multiplier: vm.commonStore.currencyMultiplier,
                        currency: vm.commonStore.selectedCurrency
                    });
                }
                airname = null;
                airPrice = [];

            });
            var airMaxMin = tempAirLine.map(function (air) {
                return {
                    air: air.air,
                    priceMax: Math.max.apply(Math, air.price),
                    price: Math.min.apply(Math, air.price),
                    multiplier: air.multiplier,
                    currency: air.currency
                }
            });
            // console.log(airMaxMin);


            this.resStops = _.sortBy(_.uniq(tempStops));
            this.resAirlines = airMaxMin.sort(function (a, b) {
                return a.price - b.price;
            })

            if (this.multipleCarrier.length > 0) {
                airMaxMin.push({
                    air: "Multi-Airline",
                    priceMax: Math.max.apply(Math, this.multipleCarrier),
                    price: Math.min.apply(Math, this.multipleCarrier),
                    multiplier: vm.commonStore.currencyMultiplier,
                    currency: vm.commonStore.selectedCurrency
                });
            }

            this.resDepLoc = _.uniq(tempDepLoc);
            this.resArrLoc = _.uniq(tempArrLoc);

            tempAirlines = [];
            tempStops = [];
            tempDepLoc = [];
            tempArrLoc = [];
            setTimeout(function () {
                metrixAir()
            }, 10);

        },
        getCalendarRows: function (dates) {
            var vm = this;
            var compressedDates = [];
            var calendarDepartureDatesGrouped = _.groupBy(dates, "depDate");
            $.each(calendarDepartureDatesGrouped, function (key, value) {
                var minFare = _.min(value, function (e) {
                    return e.fare;
                });
                compressedDates.push({
                    depDate: key,
                    fare: minFare.fare,
                    description: minFare.description,
                    airline: minFare.airline,
                });
            });
            var sorted = compressedDates.sort(function (a, b) {
                return moment(a.depDate, 'DDMMYY').valueOf() - moment(b.depDate, 'DDMMYY').valueOf();
            });

            if (sorted.length == 7) {
                return sorted;
            }
            var newArr = [];
            for (var x = 0; x < 7; x++) {
                var data = [];
                try {
                    var data = _.filter(sorted, function (e) {
                        return e.depDate == vm.groupedDepDates[x].depDate;
                    });
                } catch (error) {}

                if (data.length == 0) {
                    newArr.push({
                        airline: "NA",
                        depDate: "",
                        description: "",
                        fare: 0
                    })
                } else {
                    newArr.push(data[0]);
                }
            }

            return newArr;
        },
        setCalendarFilter: function (itemDep, itemRet) {
            var vm = this;
            $(".highlight").removeClass("highlighted");
            $(".t-header").removeClass("h-highlighted");
            $(".owl-item .item").removeClass("active");
            if (itemRet) {
                $("#cell-" + itemRet + "-" + itemDep.depDate).addClass("highlighted");
                $("#r-header-" + itemRet).addClass("h-highlighted");
                $("#header-" + itemDep.depDate).addClass("h-highlighted");
            } else {
                $("#cell-" + itemDep.depDate).addClass("highlighted");
                $("#header-" + itemDep.depDate).addClass("h-highlighted");
            }
            if (this.allFlightRes.length > 0) {
                var temp = [];
                if (this.tripType.toLowerCase() == 'r') {
                    temp = _.filter(this.allFlightRes, function (e) {
                        return e.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate == itemDep.depDate &&
                            e.groupOfFlights[1].flightDetails[0].fInfo.dateTime.depDate == itemRet;
                    });
                } else {
                    temp = _.filter(this.allFlightRes, function (e) {
                        return e.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate == itemDep.depDate;
                    });
                }
                temp = temp.filter(function (res) {
                    var sameFareDep = res.fare.amount >= itemDep.fare;
                    var sameDateDep = res.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate == itemDep.depDate;
                    if (itemRet) {
                        var sameFareRet = res.fare.amount >= itemDep.fare;
                        var sameDateRet = res.groupOfFlights[1].flightDetails[0].fInfo.dateTime.depDate == itemRet;
                        return sameFareDep && sameDateDep && sameFareRet && sameDateRet;
                    }
                    return sameFareDep && sameDateDep;
                })
                vm.filteredFlightRes = temp;
                vm.currentScrollLimit = 15;
                vm.flightResBind = vm.filteredFlightRes.slice(0, vm.currentScrollLimit);
                this.searchForMore = temp.length == 1;

            }
            vm.cellNum = itemDep.depDate + itemRet;
            vm.calendarClicked = !vm.calendarClicked;
            this.selectedCalendarDepartureDate = itemDep.depDate;
            this.selectedCalendarReturnDate = itemRet;
            this.selFlSerList = [];
            this.processMetrix();
        },
        airFilterBanner: function () {
            var vm = this;

            var tempAirlines = [];

            this.allFlightRes.forEach(fl => {
                fl.groupOfFlights.forEach(leg => {
                    // leg.flightDetails.forEach(seg => {
                    //     tempAirlines.push({
                    //         air: seg.fInfo.companyId.mCarrier,
                    //         price: fl.fare.amount,
                    //         multiplier: fl.selectCredential.officeIdList[0].multiplier,
                    //         currency: fl.selectCredential.officeIdList[0].currency
                    //     });
                    // });
                    if (leg.flightDetails.every((val, i, arr) => val.fInfo.companyId.mCarrier === arr[0].fInfo.companyId.mCarrier)) {
                        tempAirlines.push({
                            air: leg.flightDetails[0].fInfo.companyId.mCarrier,
                            price: fl.fare.amount,
                            multiplier: fl.selectCredential.officeIdList[0].multiplier,
                            currency: fl.selectCredential.officeIdList[0].currency
                        });
                    }
                });
            });


            tempAirLine = [];
            airPrice = [];
            var resAirlines1 = _.groupBy(tempAirlines, "air");

            $.each(resAirlines1, function (key, value) {
                var airname = key;
                $.each(value, function (key1, value1) {
                    airPrice.push(value1.price);
                });
                if (vm.commonStore.selectedCurrency == '') {
                    tempAirLine.push({
                        air: airname,
                        price: airPrice,
                        multiplier: value[0].multiplier,
                        currency: value[0].currency
                    });
                } else {
                    tempAirLine.push({
                        air: airname,
                        price: airPrice,
                        multiplier: vm.commonStore.currencyMultiplier,
                        currency: vm.commonStore.selectedCurrency
                    });
                }
                airname = null;
                airPrice = [];

            });

            this.resAirlines = tempAirLine.map(function (air) {
                return {
                    air: air.air,
                    priceMax: Math.max.apply(Math, air.price),
                    price: Math.min.apply(Math, air.price),
                    multiplier: vm.commonStore.selectedCurrency == '' ? air.multiplier : vm.commonStore.currencyMultiplier,
                    currency: vm.commonStore.selectedCurrency == '' ? air.currency : vm.commonStore.selectedCurrency
                }
            });

            this.resAirlines = this.resAirlines.sort(function (a, b) {
                return a.price - b.price;
            })

            if (this.multipleCarrier.length > 0) {
                this.resAirlines.push({
                    air: "Multi-Airline",
                    priceMax: Math.max.apply(Math, this.multipleCarrier),
                    price: Math.min.apply(Math, this.multipleCarrier),
                    multiplier: vm.commonStore.selectedCurrency == '' ? air.multiplier : vm.commonStore.currencyMultiplier,
                    currency: vm.commonStore.selectedCurrency == '' ? air.currency : vm.commonStore.selectedCurrency
                });
            }
        },
        scroll: function () {
            window.onscroll = () => {
                var bottomOfWindow = false;
                // var bottomOfWindow = Math.ceil(document.documentElement.scrollTop) + window.innerHeight >= document.documentElement.offsetHeight - 10;
                var docElement = $(document)[0].documentElement;
                var winElement = $(window)[0];

                if ((docElement.scrollHeight - winElement.innerHeight) <= winElement.pageYOffset + 100) {
                    bottomOfWindow = true;
                }

                if (bottomOfWindow) {
                    if (this.filteredFlightRes.length > 10 && this.filteredFlightRes.length > this.flightResBind.length) {
                        for (var index = this.currentScrollLimit; index < this.currentScrollLimit + 10; index++) {
                            if (this.filteredFlightRes[index]) {
                                this.flightResBind.push(this.filteredFlightRes[index]);
                            }
                        }
                        this.currentScrollLimit += 10;
                    }
                }
                $('[data-toggle="tooltip"]').tooltip();
            };
        },
        getAirLineName: function (airlinecode) {
            return getAirLineName(airlinecode);
        },
        airportFromAirportCode: function (airport) {
            return airportFromAirportCode(airport);
        },
        airportLocationFromAirportCode: function (airlinecode) {
            return airportLocationFromAirportCode(airlinecode);
        },
        getElapseTime: function (elapStr) {
            if (elapStr) {
                return elapStr.substring(0, 2) + 'h ' + elapStr.substring(2, 4) + 'm ';
            }
            return '';
        },
        getAirCraftName: function (aireqpcode) {
            return getAirCraftName(aireqpcode);
        },
        isRefundableFlight: function (refundString) {
            return isRefundableFlight(refundString);
        },
        openFlightDetails: function (event, fIndex, flRes, action) {
            var vm = this;
            if (action != 'ffr') {
                this.bindFlightDetails(flRes, fIndex);
            }
            $("#Flight-Details_" + fIndex).show();
            $(".simplePopupBackground").show();
            $("#Search-Details_" + fIndex).addClass("zindex");
            if (action != 'category' && action != 'fd' && action != 'ffr' && action != 'upsell' && action != 'mo') {
                $("#Search-Details_" + fIndex + ' .tab-links a:eq(2)').click();
                $("#Search-Details_" + fIndex + ' .tab-links li').removeClass("active");
                $('.Baggaget').addClass("active");
            } else if (action != 'category' && action != 'fd' && action == 'ffr' && action != 'upsell' && action != 'mo') {
                this.bindFlightDetails(flRes, fIndex);
                $("#tab1_" + fIndex + '_1').css("display", "none");
                $("#tab1_" + fIndex + '_8').css("display", "none");
                $("#tab1_" + fIndex + '_5').css("display", "none");
                $("#tab1_" + fIndex + '_3').css("display", "none");
                $("#tab1_" + fIndex + '_7').css("display", "block");
                $("#Search-Details_" + fIndex + ' .tab-links li').removeClass("active");
                $('.Fareruleget').addClass("active");
                setTimeout(function () {
                    vm.popFareRule(flRes, fIndex);
                }, 100);
            } else if (action != 'fd' && action != 'ffr' && action != 'category' && action == 'upsell' && action != 'mo') {
                this.bindFlightDetails(flRes, fIndex);
                $("#tab1_" + fIndex + '_1').css("display", "none");
                $("#tab1_" + fIndex + '_7').css("display", "none");
                $("#tab1_" + fIndex + '_3').css("display", "none");
                $("#tab1_" + fIndex + '_5').css("display", "none");
                $("#tab1_" + fIndex + '_8').css("display", "block");
                $("#Search-Details_" + fIndex + ' .tab-links li').removeClass("active");
                $('.upselltab').addClass("active");
                setTimeout(function () {
                    vm.popFareRule(flRes, fIndex);
                }, 100);
            } else if (action != 'fd' && action != 'ffr' && action != 'upsell' && action == 'category' && action != 'mo') {
                this.bindFlightDetails(flRes, fIndex);
                $("#tab1_" + fIndex + '_1').css("display", "none");
                $("#tab1_" + fIndex + '_7').css("display", "none");
                $("#tab1_" + fIndex + '_3').css("display", "none");
                $("#tab1_" + fIndex + '_8').css("display", "none");
                $("#tab1_" + fIndex + '_5').css("display", "block");
                $("#Search-Details_" + fIndex + ' .tab-links li').removeClass("active");
                $('.categorytab').addClass("active");
                setTimeout(function () {
                    vm.popFareRule(flRes, fIndex);
                }, 100);
                $(".bookDetails").each(function () {
                    $(this).css("display", "none");
                });
            } else if (action == 'mo') {
                $("#Search-Details_" + fIndex + ' .tab-links .options a').first().click();
                $("#Search-Details_" + fIndex + ' .tab-links li').removeClass("active");
                $("#tab1_" + fIndex + '_9').css("display", "block");
                $('.options').addClass("active");
                setTimeout(function () {
                    vm.checkAvailabilty(flRes, fIndex);
                }, 100);
            } else {
                $("#Search-Details_" + fIndex + ' .tab-links a').first().click();
                $("#Search-Details_" + fIndex + ' .tab-links li').removeClass("active");
                $('.Itinerary').addClass("active");
            }
            this.currentIndex = 0;
            if (flRes.segmentRef.supplier == "69") {
                listHeight();
            }
        },
        closeFlightDetails: function (event, fIndex) {
            $("#Flight-Details_" + fIndex).hide();
            $(".simplePopupBackground").hide();
            $("#Search-Details_" + fIndex).removeClass("zindex");
        },
        sortResults: function (event, isMobile, sortType) {
            var currentId = isMobile ? sortType.name : event.currentTarget.id;
            var index = _.findIndex(this.sortValues, function (e) {
                return e.sortBy == currentId
            })
            var item = this.sortValues[index];
            this.sortValues = [{
                    isActive: false,
                    sortBy: "Price",
                    isAscending: true
                },
                {
                    isActive: false,
                    sortBy: "Duration",
                    isAscending: true
                },
                {
                    isActive: false,
                    sortBy: "Departure time",
                    isAscending: true
                },
                {
                    isActive: false,
                    sortBy: "Arrival time",
                    isAscending: true
                }
            ];
            this.$set(this.sortValues, index, {
                isActive: true,
                sortBy: item.sortBy,
                isAscending: !item.isAscending
            });

            switch (currentId) {
                case 'Price':
                    this.filteredFlightRes.sort(function (a, b) {
                        return item.isAscending ? parseFloat(a.fare.amount) - parseFloat(b.fare.amount) : parseFloat(b.fare.amount) - parseFloat(a.fare.amount);
                    });
                    break;
                case 'Duration':
                    this.filteredFlightRes.sort(function (a, b) {
                        return item.isAscending ? parseInt(a.groupOfFlights[0].flightProposal.elapse) - parseInt(b.groupOfFlights[0].flightProposal.elapse) : parseInt(b.groupOfFlights[0].flightProposal.elapse) - parseInt(a.groupOfFlights[0].flightProposal.elapse);
                    });
                    break;
                case 'Departure time':
                    this.filteredFlightRes.sort(function (a, b) {
                        return item.isAscending ? moment(a.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime, 'HHmm').format('HHmm') : moment(b.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime, 'HHmm').format('HHmm') - new moment(a.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depTime, 'HHmm').format('HHmm');
                    });
                    break;
                case 'Arrival time':
                    this.filteredFlightRes.sort(function (a, b) {
                        return item.isAscending ? moment(a.groupOfFlights[0].flightDetails[a.groupOfFlights[0].flightDetails.length - 1].fInfo.dateTime.arrTime, 'HHmm').format('HHmm') - new moment(b.groupOfFlights[0].flightDetails[b.groupOfFlights[0].flightDetails.length - 1].fInfo.dateTime.arrTime, 'HHmm').format('HHmm') : moment(b.groupOfFlights[0].flightDetails[b.groupOfFlights[0].flightDetails.length - 1].fInfo.dateTime.arrTime, 'HHmm').format('HHmm') - new moment(a.groupOfFlights[0].flightDetails[a.groupOfFlights[0].flightDetails.length - 1].fInfo.dateTime.arrTime, 'HHmm').format('HHmm');
                    });
                    break;

                default:
                    break;
            }

            this.flightResBind = this.filteredFlightRes.slice(0, this.currentScrollLimit);
        },
        resetPrice: function () {
            this.lowestPrice = this.minPrice;
            this.highestPrice = this.maxPrice;
        },
        bindFlightDetails: function (flRes, fIndex) {
            var vm = this;
            var self = this;
            var itinerary = `<div class="itinerary_details">`;
            var baggageTab = '';

            var multiplier = "";
            var currency = "";
            if (this.commonStore.selectedCurrency == '') {
                multiplier = flRes.selectCredential.officeIdList[0].multiplier;
                currency = flRes.selectCredential.officeIdList[0].currency;
            } else {
                multiplier = this.commonStore.currencyMultiplier;
                currency = this.commonStore.selectedCurrency;
            }

            if (vm.commonStore.commonRoles.hasMiniRuleRole && flRes.segmentRef.supplier == "1") {
                self.has_Fare_Rule_Role = true;
                self.has_Mini_Rule_Role = false;
            }
            if (vm.commonStore.commonRoles.hasFareRuleRole && (flRes.segmentRef.supplier == "45" || flRes.segmentRef.supplier == "64" || flRes.segmentRef.supplier == "55" || flRes.segmentRef.supplier == "72" || flRes.segmentRef.supplier == "83" || flRes.segmentRef.supplier == "60" || flRes.segmentRef.supplier == "62")) {
                self.has_Mini_Rule_Role = false;
                self.has_Fare_Rule_Role = true;
            }
            var weight = '';
            if (flRes.bagDetails != undefined) {
                if (flRes.bagDetails.qCode != undefined) {
                    weight = flRes.bagDetails.qCode;
                } else {
                    weight = flRes.bagDetails.unit;
                }
            }
            flRes.groupOfFlights.forEach(function (flleg2) {
                itinerary += `<div class="itinerary_details">
                <div class="itinerary_head">
                    <h2><i class="fa fa-plane" aria-hidden="true"></i>` + vm.airportLocationFromAirportCode(flleg2.flightDetails[0].fInfo.location.locationFrom) + ' to ' + vm.airportLocationFromAirportCode(flleg2.flightDetails[flleg2.flightDetails.length - 1].fInfo.location.locationTo) + `</h2>
                    <h6><i class="fa fa-clock-o" aria-hidden="true"></i>` + vm.getElapseTime(flleg2.flightProposal.elapse) + `</h6>
                    <h6><i class="fa fa-calendar" aria-hidden="true"></i>` + vm.momCommonFun(flleg2.flightDetails[0].fInfo.dateTime.depDate, 'DDMMYY', 'ddd, DD MMM YY') + `</h6>
                </div>`;

                flleg2.flightDetails.forEach(function (segment, segindex) {
                    var oprater = (segment.fInfo.companyId.mCarrier == segment.fInfo.companyId.oCarrier) ? '' : '<p>Operated by ' + vm.getAirLineName(segment.fInfo.companyId.oCarrier) + '</p>';
                    var bagunitDesc = '';
                    var bagunitDesCabin = '';
                    var BagInfoSec = '';
                    var baggage = '';
                    var paxType = '';
                    var bagunitVal = '';
                    var bagUnit = '';
                    var paxBaginfo = '';
                    var checkInBaggageAdt = '';
                    var cabinBaggageAdt = '';
                    var Check_in_baggage = '';
                    try {
                        segment.fInfo.bags.forEach(function (bag, index) {
                            if (typeof bag.bagPaxType != "undefined") {
                                try {
                                    baggage = bag.baggage;
                                } catch (err) {
                                    /* empty */
                                }
                                try {
                                    if (typeof bag.bagPaxType != "undefined") {
                                        paxType = bag.bagPaxType == 'ADT' ? 'Adult' : bag.bagPaxType == 'CHD' ? 'Child' : 'Infant';
                                    } else {
                                        paxType = 'Adult';
                                    }
                                } catch (err) {
                                    /* empty */
                                }
                                if (bag.bagPaxType == 'ADT' && bag.desc == undefined) {
                                    try {
                                        bagunitDesc = getBaggageUnitString(bag.unit).bagunitDesc;
                                    } catch (err) {
                                        /* empty */
                                    }
                                }
                                try {
                                    bagUnit = getBaggageUnitString(bag.unit).bagunitVal;
                                } catch (err) {
                                    /* empty */
                                }
                                if (baggage == "" || baggage == "NA" || baggage == undefined) {
                                    BagInfoSec = 'Not available ';
                                } else if (bagUnit == "SB") {
                                    Check_in_baggage = "Airline typically permit standard baggage allowance";
                                    BagInfoSec = '<span data-toggle="tooltip" data-placement="top" title="' + Check_in_baggage + '">Standard Baggage</span>';
                                } else {
                                    if ((weight == "N" || weight == "P" || weight == "Pieces" || weight == "Piece" || (weight || "").toLowerCase() == "pc(s)") && (bagUnit == "N" || bagUnit == "P" || bagUnit == "Piece" || bagUnit == "Pieces" || bagUnit.toLowerCase() == "pc(s)")) {
                                        bagunitVal = baggage > 1 ? "Pieces" : "Piece";
                                        Check_in_baggage = "Airline typically permit 23kg baggage weight per piece";
                                    } else if (bagUnit.toUpperCase() == "KG" || bagUnit.toUpperCase() == "K") {
                                        bagunitVal = "Kg";
                                    } else {
                                        bagunitVal = baggage > 1 ? "Pieces" : "Piece";
                                    }

                                    if (flRes.segmentRef.supplier == "69") {
                                        bagunitVal = "Kg";
                                    }

                                    if (flRes.segmentRef.supplier == "69" && bag.baggage.toUpperCase() == "NO") {
                                        baggage = "0";
                                    }
                                    Check_in_baggage = bag.desc ? bag.desc : Check_in_baggage;
                                    BagInfoSec = isNullorUndefind(bagUnit) ?
                                        "" :
                                        '<span data-toggle="tooltip" data-placement="top" title="' + Check_in_baggage + '">' + baggage + " " + bagunitVal + "</span>";
                                }
                                paxBaginfo += '<div class="col-lg-6 col-sm-3 col-xs-6"><h1>' + paxType + '</h1></div> <div class="col-lg-6 col-sm-3 col-xs-6"><h2>' + BagInfoSec + '</h2></div>';
                                if (bag.bagPaxType == 'ADT') {
                                    checkInBaggageAdt = baggage;
                                }
                                cabinBaggageAdt = bag.cabinBaggage;
                                bagunitDesCabin = bag.cabinBaggageDesc;
                            }
                        });
                    } catch (err) {}
                    var checkInBaggage = checkInBaggageAdt != undefined && checkInBaggageAdt != "" ? '<p><span data-toggle="tooltip" data-placement="top" title="' + (bagunitDesc ? bagunitDesc : Check_in_baggage) + '"><span> ' + self.CheckIn_baggage_Label + ' :</span>' + checkInBaggageAdt + ' ' + bagunitVal + '</span></p>' : '';
                    var cabinInBaggage = cabinBaggageAdt != undefined && cabinBaggageAdt != "" ? '<p><span data-toggle="tooltip" data-placement="top" title="' + bagunitDesCabin + '">' + self.Cabin_baggage_Label + ': ' + cabinBaggageAdt + '</span></p>' : '';

                    var cabinClass = segment.fInfo.bookingClass ? getCabinClassObject(segment.fInfo.bookingClass).BasicClass : "";

                    if ((flRes.segmentRef.supplier == "57" || flRes.segmentRef.supplier == "62" || flRes.segmentRef.supplier == "83" || flRes.segmentRef.supplier == "60" || flRes.segmentRef.supplier == "86" || flRes.segmentRef.supplier == "87") && segment.fInfo.extraVariables != undefined) {
                        segment.fInfo.rbd = segment.fInfo.extraVariables[segindex].rbd;
                        segment.fInfo.bookingClass = segment.fInfo.extraVariables[segindex].cabinClass;
                        segment.fInfo.avlStatus = segment.fInfo.extraVariables[0].availableSeats;
                        cabinClass = segment.fInfo.bookingClass ? getCabinClassObject(segment.fInfo.bookingClass).BasicClass : "";
                    }

                    itinerary += `<div class="flight_itinerary">
                        <div class="col-sm-2 col-xs-12 text-left no_padding_left itinerary01">` +
                        `<img src="/Flights/assets/images/AirLines/` + segment.fInfo.companyId.mCarrier + `.gif" class="img_itinerary">
                            <h1>` + vm.getAirLineName(segment.fInfo.companyId.mCarrier) + `</h1>
                            <h1>` + segment.fInfo.companyId.mCarrier + `-` + segment.fInfo.flightNo + `</h1>
                            ` + oprater + `
                        </div>

                        <div class="col-xs-12 col-sm-7 no_padding_left mrg_btm">
                        <div class="flight_from left_txt">
                            <p><span data-toggle="tooltip" data-placement="top" title="` + vm.airportFromAirportCode(segment.fInfo.location.locationFrom) + `">` + segment.fInfo.location.locationFrom + `</span></p>
                            <p>` + vm.momCommonFun(segment.fInfo.dateTime.depTime, 'HHmm', 'HH:mm') + `</p>
                            <h4>` + vm.momCommonFun(segment.fInfo.dateTime.depDate, 'DDMMYY', 'DD MMM YY') + `</h4>
                            <h5>` + (segment.fInfo.location.fromTerminal == null ? 'Terminal: N/A' : 'Terminal:' + segment.fInfo.location.fromTerminal) + `</h5>            
                        </div>

                        <div class="flight_from sp08">
                            <div class="filghtImg02 split">
                            <p>` + vm.airportFromAirportCode(segment.fInfo.location.locationFrom) + `</p>
                            <div class="line_flight01">` +
                        line_flight01 +
                        `</div>
                            <p>` + vm.getAirCraftName(segment.fInfo.eqpType) + `</p>` + 
                            ((segment.fInfo.stopQuantity && segment.fInfo.stopQuantity !=0) ? (`<p style="margin-top: 15px;" data-toggle="tooltip" data-html="true" data-placement="bottom" data-title="` + segment.fInfo.stopDetails.map((e)=>"<div>"+vm.airportFromAirportCode(e.airportCode)+"</div>").join('')  + `">` + "En Route " + (segment.fInfo.stopQuantity > 1 ? 'Stops' : 'Stop') + "</p>"):'') +
                            `</div>
                        </div>

                        <div class="flight_from txt_right01">
                            <p><span data-toggle="tooltip" data-placement="top" title="` + vm.airportFromAirportCode(segment.fInfo.location.locationTo) + `">` + segment.fInfo.location.locationTo + `</span></p>            
                            <p>` + vm.momCommonFun(segment.fInfo.dateTime.arrTime, 'HHmm', 'HH:mm') + `</p>            
                            <h4>` + vm.momCommonFun(segment.fInfo.dateTime.arrDate, 'DDMMYY', 'DD MMM YY') + `</h4>            
                            <h5>` + (segment.fInfo.location.toTerminal == null ? 'Terminal: N/A' : 'Terminal:' + segment.fInfo.location.toTerminal) + `</h5>
                        </div>

                        </div>

                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 left_padding01">
                        <div class="itinerary_flight_details">
                            <p><span>` + self.Cabin_Label + `:</span>` + cabinClass + `</p>
                            ` + (segment.fInfo.rbd ? `<p><span>` + self.RBD_Label + `:</span>` + segment.fInfo.rbd + `</p>` : '') +
                        checkInBaggage + (cabinInBaggage ? cabinInBaggage : `<p><span data-toggle="tooltip" data-placement="top" title="Standard Hand Baggage Policy is 1PC/7Kg.This can vary based on the Airline/Route/Cabin">` + self.Cabin_baggage_Label + `: Info not available</span></p>`) +
                        `</div>
                        </div>
                        
                    </div>
                    </div>`;

                    if (flleg2.flightDetails.length > 1 && segindex != flleg2.flightDetails.length - 1) {
                        itinerary += '<div class="new_layover"> <h2 data-toggle="tooltip" data-placement="top" title="' + vm.airportFromAirportCode(segment.fInfo.location.locationTo) + '">' + self.Layover_at_Label + ' ' + segment.fInfo.location.locationTo + ' | ' + calcLayoverTime(segment.fInfo.dateTime.arrDate, segment.fInfo.dateTime.arrTime, flleg2.flightDetails[segindex + 1].fInfo.dateTime.depDate, flleg2.flightDetails[segindex + 1].fInfo.dateTime.depTime) + '</h2> </div>';
                    }
                    baggageTab += '<li><div class="col-lg-12 col-sm-12 col-xs-12">' +
                        '<h1>' + vm.airportLocationFromAirportCode(segment.fInfo.location.locationFrom) + ' To ' + vm.airportLocationFromAirportCode(segment.fInfo.location.locationTo) + '</h1></div>' +
                        (paxBaginfo ? paxBaginfo : '<div class="col-lg-6 col-sm-3 col-xs-6"><h1>Adult</h1></div> <div class="col-lg-6 col-sm-3 col-xs-6"><h2>NOT AVAILABLE</h2></div>') + '</li>';

                });
            });

            $("#tab1_" + fIndex + '_1').html(itinerary);

            var discountDet = '';
            if (!isNullorUndefind(flRes.fare.discount) && flRes.fare.discount != "" && flRes.fare.discount != "0.00") {
                discountDet = ` <li>
              <div class="col-lg-6 col-sm-6 col-xs-6">` +
                    '<h1>' + this.Discount_Label + '</h1>' +
                    `</div>
              <div class="col-lg-6 col-sm-6 col-xs-6">` +
                    '<h2>' + this.$n(isNullorEmptyToBlank(flRes.fare.discount, 0) / multiplier, 'currency', currency) + '</h2>' +
                    `</div>
            </li>`;
            }
            var serviceFeeDet = '';
            if (!isNullorUndefind(flRes.fare.serviceFee) && flRes.fare.serviceFee != "" && flRes.fare.serviceFee != "0.00") {
                serviceFeeDet = `<li>
              <div class="col-lg-6 col-sm-6 col-xs-6">` +
                    '<h1>' + this.Service_Fee_Label + '</h1>' +
                    `</div>
              <div class="col-lg-6 col-sm-6 col-xs-6">` +
                    '<h2>' + this.$n(isNullorEmptyToBlank(flRes.fare.serviceFee, 0) / multiplier, 'currency', currency) + '</h2>' +
                    `</div>
            </li>`
            }
            var commisionDet = '';
            if (!isNullorUndefind(flRes.fare.commission) && flRes.fare.commission != "" && flRes.fare.commission != "0.00") {
                commisionDet = `<li>
                <div class="col-lg-6 col-sm-6 col-xs-6">` +
                    '<h1>' + this.Commission_Label + '</h1>' +
                    `</div>
                <div class="col-lg-6 col-sm-6 col-xs-6">` +
                    '<h2>' + this.$n(isNullorEmptyToBlank(flRes.fare.commission, 0) / multiplier, 'currency', currency) + '</h2>' +
                    `</div>
                </li>`;
            }

            //Fare Info
            var fareInfo = `<div class="fare_details">
      <div class="baggage_head"><i class="fa fa-money" aria-hidden="true"></i>Fare Breakup</div>
      <div class="baggage-info">` + vm.adtCount + ' ' + self.Adult_option_Label + ', ' + vm.chdCount + ' ' + self.Child_Option_Label + ', ' + vm.infCount + ' ' + self.Infant_Option_Label + ` </div>
      <div class="fare_cont">
        <div class="left_fare result_listSec">
          <ul>
            <li>
              <div class="col-lg-6 col-sm-6 col-xs-6">` +
                '<h1>' + this.Base_Fare_Label + '</h1>' +
                `</div>
              <div class="col-lg-6 col-sm-6 col-xs-6">` +
                '<h2>' + this.$n(flRes.fare.baseFare / multiplier, 'currency', currency) + '</h2>' +
                `</div>
            </li>
            <li>
              <div class="col-lg-6 col-sm-6 col-xs-6">` +
                '<h1>' + this.Taxes_Label + '</h1>' +
                `</div>
              <div class="col-lg-6 col-sm-6 col-xs-6">` +
                '<h2>' + this.$n(flRes.fare.taxfare / multiplier, 'currency', currency) + '</h2>' +
                `</div>
            </li>` + discountDet + serviceFeeDet + commisionDet +
                `<li>
            <div class="col-lg-6 col-sm-6 col-xs-6">` +
                '<h1>' + this.Total_Label + '</h1>' +
                `</div>
              <div class="col-lg-6 col-sm-6 col-xs-6">` +
                '<h2>' + this.$n(flRes.fare.amount / multiplier, 'currency', currency) + '</h2>' +
                `</div>
            </li>
          </ul>
        </div>
      </div>
    </div>`;
            $("#tab1_" + fIndex + '_2').html(fareInfo);
            $("#tab1_" + fIndex + '_3 .bagBindHere').html(baggageTab);

            setTimeout(() => {
                $('[data-toggle="tooltip"]').tooltip();
            }, 100);
        },
        momCommonFun: function (date, frmFormat, toFormat) {
            return moment(date, frmFormat).format(toFormat);
        },
        //Filters
        getStop: function (segLen) {
            if (segLen - 1 === 0) {
                return 'Non-stop';
            } else {
                return segLen - 1 + ' stop';
            }
        },
        getCategoryObject4Soln: function (selFlightOption, solnID, index) {
            var cabinClass = "Y";
            if (selFlightOption != undefined && selFlightOption.category != undefined) {
                for (var i = 0; i < selFlightOption.category.length; i++) {
                    if (selFlightOption.category[i].extraVariables != undefined) {
                        if (selFlightOption.category[i].fareId == solnID && selFlightOption.category[i].extraVariables[index]) {
                            cabinClass = selFlightOption.category[i].extraVariables[index].cabinClass;
                        } else if (selFlightOption.category[i].fareId == solnID && selFlightOption.category[i].extraVariables[index]) {
                            cabinClass = selFlightOption.category[i].extraVariables[index].cabinClass;
                        }
                    } else {
                        if (selFlightOption.category[i].fareId == solnID && selFlightOption.category[i].fbCode != undefined && selFlightOption.category[i].fbCode.toLowerCase().includes('business')) {
                            cabinClass = 'C';
                        } else if (selFlightOption.category[i].fareId == solnID && selFlightOption.category[i].fbCode != undefined && selFlightOption.category[i].fbCode.toLowerCase().includes('first')) {
                            cabinClass = 'F';
                        }
                    }
                }
            }
            return cabinClass;
        },
        createSelRqMinRq: function (type, flIndex, type, fInfoData) {

            var self = this;
            var searchData = this.getSearchData(0);
            var selFlightOption = type == "updateFare" ? fInfoData : this.flightResBind[flIndex];
            var supplierSpecific = {};
            var segGroups = [];
            var farebasicode = '';
            var fareCode = '';
            var fareKey = [];
            var suppSpecific = {};
            var suppSpecificArr = [];
            if (selFlightOption.category != undefined) {
                selFlightOption = this.categoryChangeOption(selFlightOption);
            }
            selFlightOption.groupOfFlights.forEach(function (leg, legind) {
                leg.flightDetails.forEach(function (seg, segind) {
                    var fareid = "12";
                    var Mcarrier = seg.fInfo.companyId.mCarrier;
                    var Ocarrier = seg.fInfo.companyId.oCarrier;
                    if (selFlightOption.segmentRef.supplier == "PYT001" || selFlightOption.segmentRef.supplier == "42") {
                        try {
                            farebasicode = selFlightOption.fareDetails.fareBasis;
                        } catch (err) {}
                        // } else if (selFlightOption.segmentRef.supplier == "18") {
                        //     try {
                        //         fareid = selFlightOption.groupOfFlights[legind].flightProposal.unitQualifier;
                        //         farebasicode = selFlightOption.category[cat].fareID;
                        //     } catch (err) { }
                    } else if (selFlightOption.segmentRef.supplier == "1") {
                        try {
                            supplierSpecific = selFlightOption.supplierSpecific;
                            farebasicode = selFlightOption.typeRef.type;
                        } catch (err) {}
                    }
                    // else if (selFlightOption.segmentRef.supplier == "44" || selFlightOption.segmentRef.supplier == "49" || selFlightOption.segmentRef.supplier == "50" || selFlightOption.segmentRef.supplier == "71") {
                    //     try {
                    //         farebasicode = selFlightOption.segmentRef.segRef;
                    //         fareid = selFlightOption.typeRef.type;
                    //     } catch (err) {}
                    // } 
                    else if (selFlightOption.segmentRef.supplier == "45") {
                        try {
                            supplierSpecific = selFlightOption.supplierSpecific;
                            farebasicode = selFlightOption.segmentRef.segRef;
                            fareid = selFlightOption.groupOfFlights[legind].flightDetails[segind].fInfo.eqpType;
                            fareCode = selFlightOption.fareDetails.fareBasis;
                        } catch (err) {}
                    } else if (selFlightOption.segmentRef.supplier == "3" || selFlightOption.segmentRef.supplier == "64") {
                        try {
                            farebasicode = selFlightOption.segmentRef.segRef;
                        } catch (err) {}
                    } else if (selFlightOption.segmentRef.supplier == "54" ||
                        selFlightOption.segmentRef.supplier == "69" ||
                        selFlightOption.segmentRef.supplier == "57" ||
                        selFlightOption.segmentRef.supplier == "62" ||
                        selFlightOption.segmentRef.supplier == "18" ||
                        selFlightOption.segmentRef.supplier == "58" ||
                        selFlightOption.segmentRef.supplier == "83" ||
                        selFlightOption.segmentRef.supplier == "44" ||
                        selFlightOption.segmentRef.supplier == "49" ||
                        selFlightOption.segmentRef.supplier == "50" ||
                        selFlightOption.segmentRef.supplier == "71" ||
                        selFlightOption.segmentRef.supplier == "60" ||
                        selFlightOption.segmentRef.supplier == "74" ||
                        selFlightOption.segmentRef.supplier == "86" ||
                        selFlightOption.segmentRef.supplier == "87"
                    ) {
                        try {
                            farebasicode = selFlightOption.segmentRef.segRef;
                            supplierSpecific = selFlightOption.supplierSpecificContent;
                            if (selFlightOption.segmentRef.supplier == "54" && selFlightOption["selFareIDtmp"]) {
                                fareid = selFlightOption["selFareIDtmp"][legind];
                            } else {
                                fareid = selFlightOption["selFareID" + (legind + 1)];
                            }
                            // for round trip and multicity category issue fix- Suppplier 62 
                            // if (selFlightOption.segmentRef.supplier == "62" && legind > 0 && fareid == '') {
                            //     fareid = selFlightOption.selFareID1 || '';
                            // }
                            var typeTest = selFlightOption.baggageInfoHT ? selFlightOption.baggageInfoHT[1] : undefined;
                            if (typeTest != undefined && typeTest.length > 0) {
                                seg.fInfo.bags = selFlightOption.baggageInfoHT[fareid];
                            }
                            var cabinClass = self.getCategoryObject4Soln(selFlightOption, fareid, segind);
                            if (cabinClass != undefined && cabinClass != '') {
                                seg.fInfo.bookingClass = cabinClass;
                            }
                            var selectedCategoryFromFlights = _.find(selFlightOption.category, function (e) {
                                return e.fareId == fareid;
                            });
                            if (selectedCategoryFromFlights) {
                                seg.fInfo.selectedCategory = selectedCategoryFromFlights;
                                var isFareExists = false;
                                if (fareKey.length >= 1) {
                                    isFareExists = fareKey.some(function (e) {
                                        return e == seg.fInfo.selectedCategory.fareKey
                                    });
                                }
                                if (!isFareExists) {
                                    fareKey.push(seg.fInfo.selectedCategory.fareKey)
                                }
                            }

                            if ((selFlightOption.segmentRef.supplier == "58" || selFlightOption.segmentRef.supplier == "83" || selFlightOption.segmentRef.supplier == "71" || selFlightOption.segmentRef.supplier == "49" || selFlightOption.segmentRef.supplier == "50" || selFlightOption.segmentRef.supplier == "44" || selFlightOption.segmentRef.supplier == "60" || selFlightOption.segmentRef.supplier == "74" ||selFlightOption.segmentRef.supplier == "86"||selFlightOption.segmentRef.supplier == "87" || selFlightOption.segmentRef.supplier == "62") && segind == 0) {
                                if (selectedCategoryFromFlights) {
                                    if (legind == 0 && (selFlightOption.segmentRef.supplier == "71" || selFlightOption.segmentRef.supplier == "49" || selFlightOption.segmentRef.supplier == "50" || selFlightOption.segmentRef.supplier == "44")) {
                                        suppSpecific = Object.assign({}, supplierSpecific, seg.fInfo.selectedCategory.supplierSpecific);
                                    } else if (selFlightOption.segmentRef.supplier == "71" || selFlightOption.segmentRef.supplier == "49" || selFlightOption.segmentRef.supplier == "50" || selFlightOption.segmentRef.supplier == "44") {
                                        suppSpecific = seg.fInfo.selectedCategory.supplierSpecific;
                                    } else {
                                        suppSpecific = Object.assign({}, supplierSpecific, seg.fInfo.selectedCategory.supplierSpecific);
                                    }
                                    suppSpecificArr.push(suppSpecific);
                                } else {
                                    if (suppSpecificArr.length == 0) {
                                        suppSpecificArr.push(Object.assign({}, supplierSpecific, selFlightOption.supplierSpecific));
                                    }
                                }

                            } else {
                                if (selectedCategoryFromFlights) {
                                    suppSpecific = Object.assign({}, supplierSpecific, seg.fInfo.selectedCategory.supplierSpecific);
                                } else {
                                    suppSpecific = Object.assign({}, supplierSpecific, selFlightOption.supplierSpecific);
                                }
                            }

                        } catch (err) {}
                    } else if (selFlightOption.segmentRef.supplier == "55") {
                        try {
                            farebasicode = selFlightOption.fareDetails.fareBasis;
                            supplierSpecific = selFlightOption.supplierSpecific;
                        } catch (err) {}
                    } else if (selFlightOption.segmentRef.supplier == "72") {
                        // farebasicode = selFlightOption.segmentRef.segRef;

                        // ---------
                        var suppCode = selFlightOption.supplierSpecific.supplierCode;


                        if (suppCode == "PYT001" || suppCode == "42") {
                            try {
                                farebasicode = selFlightOption.fareDetails.fareBasis;
                            } catch (err) {}
                            // } else if (selFlightOption.segmentRef.supplier == "18") {
                            //     try {
                            //         fareid = selFlightOption.groupOfFlights[legind].flightProposal.unitQualifier;
                            //         farebasicode = selFlightOption.category[cat].fareID;
                            //     } catch (err) { }
                        } else if (suppCode == "1") {
                            try {
                                supplierSpecific = selFlightOption.supplierSpecific;
                                farebasicode = selFlightOption.typeRef.type;
                            } catch (err) {}
                        }
                        // else if (suppCode == "44" || suppCode== "49" || suppCode == "50" || suppCode == "71") {
                        //     try {
                        //         farebasicode = selFlightOption.segmentRef.segRef;
                        //         fareid = selFlightOption.typeRef.type;
                        //     } catch (err) {}
                        // } 
                        else if (suppCode == "45") {
                            try {
                                supplierSpecific = selFlightOption.supplierSpecific;
                                farebasicode = selFlightOption.segmentRef.segRef;
                                fareid = selFlightOption.groupOfFlights[legind].flightDetails[segind].fInfo.eqpType;
                                fareCode = selFlightOption.fareDetails.fareBasis;
                            } catch (err) {}
                        } else if (suppCode == "3" || suppCode == "64") {
                            try {
                                farebasicode = selFlightOption.segmentRef.segRef;
                            } catch (err) {}
                        } else if (suppCode == "54" ||
                            suppCode == "69" ||
                            suppCode == "57" ||
                            suppCode == "62" ||
                            suppCode == "18" ||
                            suppCode == "58" ||
                            suppCode == "83" ||
                            suppCode == "44" ||
                            suppCode == "49" ||
                            suppCode == "50" ||
                            suppCode == "71" ||
                            suppCode == "60" ||
                            suppCode == "74"
                        ) {
                            try {
                                farebasicode = selFlightOption.segmentRef.segRef;

                                supplierSpecific = selFlightOption.supplierSpecificContent;
                                if (selFlightOption.segmentRef.supplier == "54" && selFlightOption["selFareIDtmp"]) {
                                    fareid = selFlightOption["selFareIDtmp"][legind];
                                } else {
                                    fareid = selFlightOption["selFareID" + (legind + 1)];
                                }
                                var typeTest = selFlightOption.baggageInfoHT ? selFlightOption.baggageInfoHT[1] : undefined;
                                if (typeTest != undefined && typeTest.length > 0) {
                                    seg.fInfo.bags = selFlightOption.baggageInfoHT[fareid];
                                }

                                var cabinClass = self.getCategoryObject4Soln(selFlightOption, fareid, segind);
                                if (cabinClass != undefined && cabinClass != '') {
                                    seg.fInfo.bookingClass = cabinClass;
                                }
                                seg.fInfo.selectedCategory = _.find(selFlightOption.category, function (e) {
                                    return e.fareId == fareid;
                                });
                                var isFareExists = false;
                                if (fareKey.length >= 1) {
                                    isFareExists = fareKey.some(function (e) {
                                        return e == seg.fInfo.selectedCategory.fareKey
                                    });
                                }
                                if (!isFareExists) {
                                    fareKey.push(seg.fInfo.selectedCategory.fareKey)
                                }
                                suppSpecific = seg.fInfo.selectedCategory.supplierSpecific;

                            } catch (err) {}
                        } else if (suppCode == "55") {
                            try {
                                farebasicode = selFlightOption.fareDetails.fareBasis;
                                supplierSpecific = selFlightOption.supplierSpecific;
                            } catch (err) {}
                        }
                        //----


                        // farebasicode = selFlightOption.supplierSpecific.supplierCode;
                        fareid = selFlightOption.groupOfFlights[legind].flightDetails[segind].fInfo.eqpType;
                        fareCode = selFlightOption.fareDetails.fareBasis;
                        if (selFlightOption.supplierSpecificContent) {
                            Object.assign(supplierSpecific, selFlightOption.supplierSpecificContent[selFlightOption.supplierSpecific.supplierCode]);
                        }
                        if (selFlightOption.supplierSpecific) {
                            Object.assign(supplierSpecific, selFlightOption.supplierSpecific);
                        }

                        var fareidLocal = selFlightOption["selFareID" + selFlightOption.groupOfFlights.length];
                        var selectedCategory = _.find(selFlightOption.category, function (e) {
                            return e.fareId == fareidLocal;
                        });
                        if (selectedCategory && selectedCategory.supplierSpecific) {
                            Object.assign(supplierSpecific, selectedCategory.supplierSpecific);
                        }
                    }
                    var vAirline = leg.flightProposal.vAirline;
                    var apisRequirementsRef = null;
                    try {
                        apisRequirementsRef = seg.apisRequirementsRef;
                    } catch (ex) {}
                    var hostTokenRef = null;
                    try {
                        hostTokenRef = seg.hostTokenRef;
                    } catch (ex) {}
                    var Seginfo = {
                        "segmentInformation": {
                            "flightDate": {
                                "departureDate": seg.fInfo.dateTime.depDate,
                                "departureTime": seg.fInfo.dateTime.depTime,
                                "arrivalDate": seg.fInfo.dateTime.arrDate,
                                "arrivalTime": seg.fInfo.dateTime.arrTime
                            },
                            "fareId": fareid,
                            "boardPointDetails": {
                                "trueLocationId": seg.fInfo.location.locationFrom
                            },
                            "offPointDetails": {
                                "trueLocationId": seg.fInfo.location.locationTo
                            },
                            "companyDetails": {
                                "marketingCompany": Mcarrier,
                                "operatingCompany": Ocarrier,
                                "validatingCompany": vAirline,
                            },
                            "flightIdentification": {
                                "flightNumber": seg.fInfo.flightNo,
                                "bookingClass": seg.fInfo.rbd
                            },
                            "flightTypeDetails": {
                                "flightIndicator": String(legind + 1),
                                "itemNumber": segind + 1
                            },
                            "apisRequirementsRef": apisRequirementsRef,
                            "hostTokenRef": hostTokenRef,
                            "stopDetails": seg.fInfo.stopDetails ? seg.fInfo.stopDetails : undefined,
                            "stopQuantity": seg.fInfo.stopQuantity ? seg.fInfo.stopQuantity : undefined
                        }
                    }
                    segGroups.push(Seginfo);

                });
            });

            if (selFlightOption.segmentRef.supplier == "69") {
                if (supplierSpecific) {
                    supplierSpecific.fareKeys = fareKey;
                } else {
                    supplierSpecific = {};
                    supplierSpecific.fareKeys = fareKey;
                }
            }

            if (selFlightOption.segmentRef.supplier == "57" || selFlightOption.segmentRef.supplier == "62") {
                supplierSpecific = suppSpecific;
            }

            if (selFlightOption.segmentRef.supplier == "58" || selFlightOption.segmentRef.supplier == "83" || selFlightOption.segmentRef.supplier == "71" || selFlightOption.segmentRef.supplier == "49" || selFlightOption.segmentRef.supplier == "50" || selFlightOption.segmentRef.supplier == "44" || selFlightOption.segmentRef.supplier == "60" || selFlightOption.segmentRef.supplier == "74" || selFlightOption.segmentRef.supplier == "86" || selFlightOption.segmentRef.supplier == "87"|| selFlightOption.segmentRef.supplier == "62") {
                supplierSpecific = suppSpecificArr;
            }


            if (selFlightOption.segmentRef.supplier == "73") {
                supplierSpecific = selFlightOption.supplierSpecific;
                // supplierSpecific["segmentRef"] = selFlightOption.segmentRef;
            }

            // if (selFlightOption.segmentRef.supplier == "71" || selFlightOption.segmentRef.supplier == "49" || selFlightOption.segmentRef.supplier == "50" || selFlightOption.segmentRef.supplier == "44") {
            //     supplierSpecific = selFlightOption.supplierSpecificContent;
            // }

            var Com = "FlightSelectRQ";
            if (type === "minirule") {
                Com = "FlightMiniRulesRQ";
            }
            var selectCred = null;
            // try { selectCred = JSON.parse(localStorage.selectCredential).selectCredential; } catch (err) { }
            try {
                selectCred = selFlightOption.selectCredentialBooking;
            } catch (err) {}
            var suppCode = null;
            try {
                suppCode = selFlightOption.segmentRef.supplier;
            } catch (err) {}
            // debugger;
            var rq = {
                "request": {
                    "service": "FlightRQ",
                    "supplierCodes": [suppCode],
                    "content": {
                        "command": Com,
                        "commonRequestFarePricer": {
                            "body": {
                                "airRevalidate": {
                                    "fareSourceCode": farebasicode,
                                    "sessionId": selFlightOption.segmentRef.pSessionId,
                                    "target": "Test",
                                    "adt": searchData.request.content.criteria.passengerTypeQuantity.adt,
                                    "chd": searchData.request.content.criteria.passengerTypeQuantity.chd,
                                    "inf": searchData.request.content.criteria.passengerTypeQuantity.inf,
                                    "paymentCardType": fareCode,
                                    "segmentGroup": segGroups,
                                    "tripType": searchData.request.content.criteria.tripType,
                                    "from": selFlightOption.groupOfFlights[0].flightDetails[0].fInfo.location.locationFrom,
                                    "to": searchData.request.content.criteria.tripType.toLowerCase() == 'r' ? selFlightOption.groupOfFlights[selFlightOption.groupOfFlights.length - 1].flightDetails[0].fInfo.location.locationFrom : selFlightOption.groupOfFlights[selFlightOption.groupOfFlights.length - 1].flightDetails[selFlightOption.groupOfFlights[selFlightOption.groupOfFlights.length - 1].flightDetails.length - 1].fInfo.location.locationTo,
                                    "dealCode": selFlightOption.dealCode
                                }
                            },
                            "supplierAgencyDetails": null
                        },
                        "supplierSpecific": supplierSpecific
                    },
                    "selectCredential": selectCred
                }
            }
            return rq;
        },
        // categorySel: function(flRes){
        //     flRes.need2ShowCategory=false;
        // },
        categoryContinueSel: function (flRes, event, index) {
            // if(flRes.selFareID1!=undefined){
            //     flRes.need2ShowCategory=true;
            // }
            this.currentIndex = (index + 1);
        },
        categoryBackSel: function (flRes, event, index) {
            // flRes.need2ShowCategory=false;
            this.currentIndex = (index - 1);

        },
        // getPairList4LegOne: function(cateogoryList,flRes){
        //     cateogoryList = cateogoryList.sort((a, b) => a.amount - b.amount);
        //     return cateogoryList;
        // },
        getPairList: function (cateogoryList, flRes, index) {
            if (index == 0) {
                if (flRes.segmentRef.supplier != 83) {
                    cateogoryList = cateogoryList.sort((a, b) => parseFloat(a.amount) - parseFloat(b.amount));
                }
                if (flRes.segmentRef.supplier == 54) {
                    var tmpDepCat =[];
                    cateogoryList = _.groupBy(cateogoryList, "fbCode");
                    $.each(cateogoryList, function (key, value) {
                        var minFare = _.min(value, function (e) {
                            return e.amount;
                        });
                        tmpDepCat.push(minFare);
                    });
                    cateogoryList = tmpDepCat;
                    // cateogoryList = cateogoryList.sort((a, b) => parseFloat(a.amount) - parseFloat(b.amount));
                }
                return cateogoryList
            }
            // this.need2ShowCategory=true;
            var combineValues = flRes.compinationDetails[flRes['selFareID' + index]];
            if (!Array.isArray(combineValues) && !_.isEmpty(combineValues)) {
                combineValues = Object.keys(combineValues);
            }
            if (index >= 2 && combineValues) {
                combineValues = flRes.compinationDetails[flRes['selFareID' + (index - 1)]][flRes['selFareID' + index]];
            }
            var newList = [];
            for (var i = 0; i < cateogoryList.length; i++) {
                var categryObject = cateogoryList[i];
                var need2Add = false;
                if (combineValues == undefined || combineValues.length == 0) {
                    need2Add = true;
                } else {
                    if (flRes.segmentRef.supplier == 54) {
                        need2Add = true;
                    } else {
                        for (var k = 0; k < combineValues.length; k++) {
                            if (categryObject.fareId == combineValues[k]) {
                                need2Add = true;
                                break;
                            }
                        }
                    }
                }
                if (need2Add) {
                    newList.forEach(element => {
                        if (element.fbCode == categryObject.fbCode && element.amount == categryObject.amount) {
                            need2Add = false;
                        }
                    });
                    if (need2Add) {
                        categryObject.newAmount = 0;
                        categryObject.newfareId = null;
                        newList.push(JSON.parse(JSON.stringify(categryObject)));
                    }
                }
            }
            if (flRes.segmentRef.supplier != 83) {
                newList = newList.sort((a, b) => parseFloat(a.amount) - parseFloat(b.amount));
            }

            if (flRes.segmentRef.supplier == 54) {

                var selectedFare = flRes.category.filter(function (el) {
                    return el.fareId == flRes['selFareID' + index];
                });
                var catgryListTmp = flRes.category.filter(function (el) {
                    return el.tripId == index - 1;
                });
                catgryListTmp = _.groupBy(catgryListTmp, "fbCode");

                var selectedfbCode = catgryListTmp[selectedFare[0].fbCode];
                // var tmpDepombi =[];

                $.each(selectedfbCode, function (key, value) {
                    $.each(flRes.compinationDetails[value.fareId], function (key1, value1) {
                        var ind = newList.findIndex(function (e) {
                            return e.fareId == value1;
                        })
                        if(ind != -1) {
                            newList[ind].newfareId = [value.fareId, newList[ind].fareId];
                            newList[ind].newAmount = parseFloat(value.amount) + parseFloat(newList[ind].amount) - parseFloat(selectedFare[0].amount);
                        }
                    });
                });

                // tmpDepombi = tmpDepombi.flat();
                // flRes.category = tmpDepCat;
                // for (var li = 0; li < cateogoryList.length; li++) {
                //     Vue.set(cateogoryList, li, newList[li]);
                // }
            }

            if (flRes.selFareID2 == '' && newList.length > 0) {
                flRes.selFareID2 = newList[0].fareId;
            }

            // if(newList.length==0){
            //      flRes.need2ShowCategory=false;
            // }
            return newList;
        },
        categoryBookNow: function (flRes, event, index) {
            this.selectFlight(index)

        },
        categoryChangeOption(flRes) {
            try {
                // debugger;
                var categories = [];
                var cancellation = [];
                for (var e = 0; e < flRes.groupOfFlights.length; e++) {
                    if (flRes["selFareID" + (e + 1)] != undefined && flRes["selFareID" + (e + 1)] != "" && flRes.category != undefined) {
                        var tmp = flRes["selFareID" + (e + 1)];
                        if (flRes["selFareIDtmp"] && flRes["selFareIDtmp"].length > 0) {
                            tmp = flRes["selFareIDtmp"][e];
                        }
                        var category = flRes.category.filter(function (category) {
                            return category.fareId == tmp;
                        });
                        if (category != undefined && category.length > 0) {
                            categories.push(category[0]);
                            if (category[0].classDetails && category[0].classDetails.cancellation) {
                                cancellation.push(category[0].classDetails.cancellation);
                            } else if (category[0].extraVariables && category[0].extraVariables[0].pricingMsg) {
                                var valueMsg = category[0].extraVariables.some(function (e) {
                                    return e.pricingMsg.toLowerCase() == "no" || e.pricingMsg.toLowerCase() == "false" || e.pricingMsg.toLowerCase() == false;
                                });
                                cancellation.push(valueMsg ? "no" : "yes");
                            } else {
                                cancellation.push("");
                            }
                        }
                    }
                }

                var nonRefundable = cancellation.some(function (e) {
                    return e.toLowerCase() == "non refundable" || e.toLowerCase() == "no";
                });

                if (nonRefundable) {
                    flRes.refundpolicy = this.getRefundDetails("no", flRes.segmentRef.supplier);
                    setTimeout(() => {
                        $('[data-toggle="tooltip"]').tooltip();
                    }, 50);
                } else {
                    flRes.refundpolicy = this.getRefundDetails(flRes.description.pricingMsg, flRes.segmentRef.supplier);
                }

                flRes.fare.amount = (categories.reduce(function (a, c) {
                    return a + parseFloat(c.amount)
                }, 0)).toString();
                flRes.fare.taxfare = (categories.reduce(function (a, c) {
                    return a + parseFloat(c.taxfare)
                }, 0)).toString();
                flRes.fare.baseFare = (categories.reduce(function (a, c) {
                    return a + parseFloat(c.baseFare)
                }, 0)).toString();
                if (flRes.fare.serviceFee) {
                    flRes.fare.serviceFee = (categories.reduce(function (a, c) {
                        return a + parseFloat(c.serviceFee)
                    }, 0)).toString();
                }
                if (flRes.fare.discount) {
                    flRes.fare.discount = (categories.reduce(function (a, c) {
                        return a + parseFloat(c.discount)
                    }, 0)).toString();
                }
                if (flRes.fare.commission) {
                    flRes.fare.commission = (categories.reduce(function (a, c) {
                        return a + parseFloat(c.commission)
                    }, 0)).toString();
                }

                var newPaxBreakUp = [];
                for (var i = 0; i < categories[0].paxBreakups.length; i++) {
                    var pax = JSON.parse(JSON.stringify(categories[0].paxBreakups[i]));

                    pax.baseFare = (categories.reduce(function (a, c) {
                        return a + parseFloat(c.paxBreakups[i].baseFare)
                    }, 0)).toString();
                    pax.fare = (categories.reduce(function (a, c) {
                        return a + parseFloat(c.paxBreakups[i].fare)
                    }, 0)).toString();
                    pax.tax = (categories.reduce(function (a, c) {
                        return a + parseFloat(c.paxBreakups[i].tax)
                    }, 0)).toString();
                    newPaxBreakUp.push(pax);
                }

                flRes.fareDetails.paxBreakups = newPaxBreakUp;

                // if(flRes.selFareID1!=undefined&&flRes.selFareID1!=''&&flRes.selFareID2!=undefined&&flRes.selFareID2&&flRes.category!=undefined){

                //     var categoryOne=flRes.category.filter(function (category) { return category.fareId== flRes.selFareID1 });
                //     var categoryTwo=flRes.category.filter(function (category) { return category.fareId== flRes.selFareID2 });
                //     if(categoryOne!=undefined&&categoryTwo!=undefined&&categoryOne.length>0&&categoryTwo.length>0){
                //         categoryOne=categoryOne[0];
                //         categoryTwo=categoryTwo[0];
                //         flRes.fare.amount= Number(categoryOne.amount)+ Number(categoryTwo.amount);
                //         flRes.fare.taxfare= Number(categoryOne.taxfare)+ Number(categoryTwo.taxfare);
                //         flRes.fare.baseFare	= Number(categoryOne.baseFare	)+ Number(categoryTwo.baseFare);
                //         var newPaxBreakUp=[];
                //         for(var i=0;i<categoryOne.paxBreakups.length;i++){
                //             var pax=JSON.parse(JSON.stringify(categoryOne.paxBreakups[i]));
                //             if(categoryTwo.paxBreakups.length>i){
                //                 var pax2=categoryTwo.paxBreakups[i];
                //                 pax.baseFare=Number(pax.baseFare)+Number(pax2.baseFare);
                //                 pax.fare=Number(pax.fare)+Number(pax2.fare);
                //                 pax.tax=Number(pax.tax)+Number(pax2.tax);
                //             }
                //             newPaxBreakUp.push(pax);
                //         }

                //         flRes.fareDetails.paxBreakups=newPaxBreakUp;
                //     }
                // }
            } catch (error) {
                //do nothing
            }
            return flRes;
        },
        changeBaggageAndCabin: function (flRes, index) {
            var self = this;
            var categories = [];
            var need2Bind = false;

            for (var e = 0; e < flRes.groupOfFlights.length; e++) {
                if (flRes["selFareID" + (e + 1)] != undefined && flRes["selFareID" + (e + 1)] != "" && flRes.category != undefined) {
                    var category = flRes.category.filter(function (category) {
                        return category.fareId == flRes["selFareID" + (e + 1)]
                    });
                    if (category != undefined && category.length > 0) {
                        categories.push(category[0]);
                    }
                }
            }

            for (var m = 0; m < categories.length; m++) {
                if (categories[m] != undefined || categories[m].length > 0) {
                    for (let z = 0; z < flRes.groupOfFlights[m].flightDetails.length; z++) {
                        var segment = flRes.groupOfFlights[m].flightDetails[z];
                        var bags = [];
                        var childBaggage = {};
                        try {
                            segment.fInfo.bags.filter(function (e) {
                                return e.bagPaxType == "INF";
                            })[0];
                        } catch (error) {
                            childBaggage = {};
                        }
                        var cabinClass = self.getCategoryObject4Soln(flRes, flRes["selFareID" + (m + 1)], z);
                        var typeTest = flRes.baggageInfoHT ? flRes.baggageInfoHT[1] : undefined;
                        if (typeTest != undefined && typeTest.length > 0) {
                            bags = flRes.baggageInfoHT[flRes["selFareID" + (m + 1)]];
                        } else {
                            for (let k = 0; k < categories[m].paxBreakups.length; k++) {
                                if (categories[m].paxBreakups[k].paxType != "INF") {
                                    var cabinBagagge = categories[m].classDetails ? categories[m].classDetails.handBaggage : undefined;
                                    var cabinBaggageDesc = categories[m].extraVariables ? categories[m].extraVariables[0].handBaggageDesc : "";
                                    if (categories[m].classDetails && cabinBagagge == undefined) {
                                        cabinBagagge = categories[m].classDetails.cabinBaggage ? categories[m].classDetails.cabinBaggage : undefined
                                    }
                                    if (categories[m].extraVariables && (cabinBaggageDesc == "" || cabinBaggageDesc == undefined)) {
                                        cabinBaggageDesc = categories[m].extraVariables[0].cabinBaggageDesc ? categories[m].extraVariables[0].cabinBaggageDesc : "";
                                    }
                                    var bagValue = categories[m].classDetails ? categories[m].classDetails.checkedBaggage.split(" ")[0] : undefined;

                                    if (isNaN(bagValue)) {
                                        bagValue = "0"
                                    }
                                    bags.push({
                                        bagPaxType: categories[m].paxBreakups[k].paxType,
                                        baggage: bagValue,
                                        unit: categories[m].classDetails ? categories[m].classDetails.checkedBaggage.split(" ").slice(1).join(" ") : undefined,
                                        desc: categories[m].extraVariables ? categories[m].extraVariables[0].checkedBaggageDesc : "",
                                        cabinBaggage: cabinBagagge,
                                        cabinBaggageDesc: cabinBaggageDesc
                                    })
                                } else {
                                    bags.push({
                                        bagPaxType: childBaggage.bagPaxType,
                                        baggage: childBaggage.baggage,
                                        unit: childBaggage.unit,
                                        desc: childBaggage.desc,
                                        cabinBaggage: childBaggage.handBaggage,
                                        cabinBaggageDesc: childBaggage.handBaggageDesc
                                    })
                                }
                            }
                        }
                        if (categories[m].extraVariables) {
                            segment.fInfo.extraVariables = categories[m].extraVariables;
                        }
                        if (segment != undefined) {
                            segment.fInfo.bags = bags;
                            need2Bind = true;
                            if (cabinClass != undefined && cabinClass != '') {
                                segment.fInfo.bookingClass = cabinClass;
                            }
                        }
                    }
                }
            }

            //     var solnID1=flRes.selFareID1;
            //     var solnID2=flRes.selFareID2;

            //     var category1= flRes.category.filter(function (el) { return el.tripId == 0 && el.fareId==solnID1 ;});
            //     var category2= flRes.category.filter(function (el) { return el.tripId == 1 && el.fareId==solnID2 ;});
            //     var need2Bind=false;

            //    if(category1!=undefined&&category1.length>0){
            //         category1=category1[0];
            //         var segment=flRes.groupOfFlights[0].flightDetails[0];
            //         var bags=[];
            //         var cabinClass= self.getCategoryObject4Soln(flRes,solnID1);
            //         if(self.baggageInfoHT!=undefined){
            //             bags= self.baggageInfoHT[solnID1];

            //         }
            //         if(segment!=undefined){
            //             segment.fInfo.bags=bags;
            //             need2Bind=true;
            //             if(cabinClass!=undefined&&cabinClass!=''){
            //                 segment.fInfo.bookingClass=cabinClass;
            //             }
            //         }
            //    }
            //    if(category2!=undefined&&category2.length>0&&flRes.groupOfFlights.length>1){
            //         category2=category2[0];
            //         var segment=flRes.groupOfFlights[1].flightDetails[0];
            //         var bags=[];
            //         var cabinClass= self.getCategoryObject4Soln(flRes,solnID2);
            //         if(self.baggageInfoHT!=undefined){
            //             bags= self.baggageInfoHT[solnID2];
            //         }
            //         if(segment!=undefined){
            //             segment.fInfo.bags=bags;
            //             need2Bind=true;
            //             if(cabinClass!=undefined&&cabinClass!=''){
            //                 segment.fInfo.bookingClass=cabinClass;
            //             }
            //         }
            //    }
            if (need2Bind == true) {
                self.bindFlightDetails(flRes, index);
            }
        },
        //category selection 
        categorySelection: function (index, catogoryFareID, catindex, event, flRes, category, fIndex) {
            var self = this;
            //change fare for fly dubai
            // flRes.fare.amount = category.amount;
            //flRes.fare.baseFare = category.baseFare;
            // flRes.fare.taxfare = category.taxfare;
            // flRes.fareDetails.paxBreakups = category.paxBreakups;

            // self.catogorySelected = true;
            // flRes['selFareIDtmp'] = category.newfareId;
            
            //console.log(catindex);
            if (flRes.segmentRef.supplier == 54 || flRes.segmentRef.supplier == 57 || flRes.segmentRef.supplier == 62 || flRes.segmentRef.supplier == 18 || flRes.segmentRef.supplier == 58 || flRes.segmentRef.supplier == 83 || flRes.segmentRef.supplier == 60 || flRes.segmentRef.supplier == 44 || flRes.segmentRef.supplier == 49 || flRes.segmentRef.supplier == 50 || flRes.segmentRef.supplier == 71 || flRes.segmentRef.supplier == 74) {
                if (catindex == 0) {
                    // debugger
                    //self.catogoryFareID1 = catogoryFareID;
                    //self.catogoryFlightIndex1 = index;
                    flRes.selFareID1 = catogoryFareID;
                    // flRes.selFareID2="";
                    if (flRes.category.filter(function (el) {
                            return el.tripId == 1;
                        }).length == 0) {
                        flRes.fare.amount = Number(category.amount);
                        flRes.fare.taxfare = Number(category.taxfare);
                        flRes.fare.baseFare = Number(category.baseFare);
                        var newPaxBreakUp = JSON.parse(JSON.stringify(category.paxBreakups));
                        flRes.fareDetails.paxBreakups = newPaxBreakUp;
                    } else {
                        for (var d = 1; d < flRes.groupOfFlights.length; d++) {
                            var allSecCategory = self.getPairList(flRes.category.filter(function (el) {
                                return el.tripId == d;
                            }), flRes, d);

                            if (flRes.segmentRef.supplier == 44 || flRes.segmentRef.supplier == 49 || flRes.segmentRef.supplier == 50 || flRes.segmentRef.supplier == 71) {
                                if (!self.categoryFlag) {
                                    self.orginalcategories = JSON.parse(JSON.stringify(allSecCategory))
                                    self.categoryFlag = true;
                                }
                                for (let k = 0; k < allSecCategory.length; k++) {
                                    // allSecCategory[k].amount = parseFloat(category.categoryFare) + parseFloat(allSecCategory[k].amount);
                                    allSecCategory[k].amount = parseFloat(category.categoryFare) + parseFloat(self.orginalcategories[k].amount);
                                }
                                var allCategory = self.getPairList(flRes.category.filter(function (el) {
                                    return el.tripId != d;
                                }), flRes, d);
                                let changedCategory =  allCategory.concat(allSecCategory);
                                Vue.set(flRes, "category", changedCategory)
            
                            }
                            if (allSecCategory.length > 0) {
                                allSecCategory = allSecCategory[0];
                                flRes['selFareID' + (d + 1)] = allSecCategory.fareId;
                                if (flRes.segmentRef.supplier == 54) {
                                    Vue.set(flRes, "selFareIDtmp", allSecCategory.newfareId);
                                }
                            }
                        }
                    }
                } else {
                    // self.catogoryFareID2 = catogoryFareID;
                    //self.catogoryFlightIndex2 = index;
                    if (flRes.segmentRef.supplier == 54) {
                        Vue.set(flRes, "selFareIDtmp", category.newfareId);
                    }
                    if (catindex >= 2) {
                        flRes['selFareID' + (catindex + 1)] = catogoryFareID;
                    } else {
                        for (var x = catindex; x < flRes.groupOfFlights.length; x++) {
                            flRes['selFareID' + (x + 1)] = catogoryFareID;
                            if (x >= 2) {
                                // var nextfareID = self.compinationDetails[flRes['selFareID'+(x-1)]][catogoryFareID];
                                // if (nextfareID) {
                                //     flRes['selFareID'+(x+1)]=nextfareID[0];
                                // }
                                var allSecCategory = self.getPairList(flRes.category.filter(function (el) {
                                    return el.tripId == x;
                                }), flRes, x);
                                if (allSecCategory.length > 0) {
                                    allSecCategory = allSecCategory[0];
                                    if (flRes.segmentRef.supplier == 54) {
                                        flRes["selFareID" + (x + 1)] = allSecCategory.newfareId[x];
                                    } else {
                                        flRes['selFareID' + (x + 1)] = allSecCategory.fareId;
                                    }
                                }
                            }

                        }
                    }

                }
            } else {
                flRes["selFareID" + (index + 1)] = catogoryFareID;
            }
            self.categoryChangeOption(flRes);
            self.changeBaggageAndCabin(flRes, fIndex);
            var clickedElement = event.currentTarget;
            //$(clickedElement).addClass('active');
            // $(clickedElement).siblings().removeClass('active');
            this.currentIndex = index;
        },
        updateFare(flIndex) {
            var selectedFlightInfo = __.cloneDeep(this.flightResBind[flIndex]);
            selectedFlightInfo.groupOfFlights = this.flightResBind[flIndex].availabilityTmp.groupOfFlights;
            var selectFlightRq = this.createSelRqMinRq('select', flIndex, "updateFare", selectedFlightInfo);
            var vm = this;
            var hubUrl = vm.commonStore.hubUrls.hubConnection.baseUrl + vm.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = vm.commonStore.hubUrls.hubConnection.hubServices.flights.airSelect;
            this.flightResBind[flIndex].sendAvailability = true;
            var config = {
                axiosConfig: {
                    method: "POST",
                    url: hubUrl + serviceUrl,
                    data: selectFlightRq
                },
                successCallback: function (response) {
                    // console.log("RESPONSE RECEIVED: ", response);
                    var data = response.data.response.content.fareInformationWithoutPnrReply;
                    if (data) {
                        if (vm.flightResBind[flIndex].bagDetails) {
                            vm.flightResBind[flIndex].bagDetails.unit = data.airSegments[0].baggageInformations[0].unit;
                        }
                        vm.flightResBind[flIndex].fare.amount = data.flightFareDetails.totalFare;
                        vm.flightResBind[flIndex].fare.baseFare = data.flightFareDetails.basefare;
                        vm.flightResBind[flIndex].fare.currency = data.flightFareDetails.currency;
                        vm.flightResBind[flIndex].fare.discount = data.flightFareDetails.discount;
                        vm.flightResBind[flIndex].fare.serviceFee = data.flightFareDetails.serviceFee;
                        vm.flightResBind[flIndex].fare.taxFare = data.flightFareDetails.taxFare;

                        for (var i = 0; i < vm.flightResBind[flIndex].groupOfFlights.length; i++) {
                            var gFlight = vm.flightResBind[flIndex].groupOfFlights;
                            var segments = data.airSegments.filter(function (e) {
                                return e.segDirectionID == i;
                            })
                            for (var x = 0; x < gFlight[i].flightDetails.length; x++) {
                                var fDet = gFlight[i].flightDetails;
                                fDet[x].fInfo.bags = segments[x].baggageInformations;
                                fDet[x].fInfo.bookingClass = segments[x].segInfo.flData.bookingClass;
                                fDet[x].fInfo.rbd = segments[x].segInfo.flData.rbd;
                            }
                        }
                        vm.bindFlightDetails(vm.flightResBind[flIndex], flIndex);
                    } else {
                        alertify.alert("Error", response.data.response.content.error.message);
                    }
                    vm.flightResBind[flIndex].sendAvailability = false;
                },
                errorCallback: function (error) {
                    vm.flightResBind[flIndex].sendAvailability = false;
                    alertify.alert("Error", error.message);
                },
                showAlert: true
            };
            mainAxiosRequest(config);
        },
        updateClass: function (index, gOfIndex, fIndex, classListIndex, newClass) {
            var fDetails = this.flightResBind[index].availabilityTmp.groupOfFlights[gOfIndex].flightDetails;
            var availability = this.flightResBind[index].availability.groupOfFlights[gOfIndex].flightDetails
            if (fIndex == 0) {
                for (var i = 0; i < fDetails.length; i++) {
                    availability[i].fInfo.bookingClassList.forEach(function (e) {
                        delete e.selected;
                    })
                    var classList = availability[i].fInfo.bookingClassList;
                    if (i == 0) {
                        fDetails[i].fInfo.bookingClass = newClass.cabin;
                        fDetails[i].fInfo.avlStatus = newClass.avlStatus;
                        fDetails[i].fInfo.rbd = newClass.rbd;
                        classList[classListIndex].selected = true;
                    } else {
                        var ind = classList.findIndex(function (e) {
                            return e.rbd == newClass.rbd;
                        })
                        if (ind == -1) {
                            var newBookingClass = classList[classList.length - 1];
                            fDetails[i].fInfo.bookingClass = newBookingClass.cabin;
                            fDetails[i].fInfo.avlStatus = newBookingClass.avlStatus;
                            fDetails[i].fInfo.rbd = newBookingClass.rbd;
                            classList[classList.length - 1].selected = true;
                        } else {
                            fDetails[i].fInfo.bookingClass = classList[ind].cabin;
                            fDetails[i].fInfo.avlStatus = classList[ind].avlStatus;
                            fDetails[i].fInfo.rbd = classList[ind].rbd;
                            classList[ind].selected = true;
                        }
                    }
                    delete fDetails[i].fInfo.bookingClassList;
                }
            } else {
                fDetails[fIndex].fInfo.bookingClass = newClass.cabin;
                fDetails[fIndex].fInfo.avlStatus = newClass.avlStatus;
                fDetails[fIndex].fInfo.rbd = newClass.rbd;
                this.flightResBind[index].availability.groupOfFlights[gOfIndex].flightDetails[fIndex].fInfo.bookingClassList.forEach(function (e) {
                    delete e.selected;
                })
                this.flightResBind[index].availability.groupOfFlights[gOfIndex].flightDetails[fIndex].fInfo.bookingClassList[classListIndex].selected = true;
                delete fDetails[fIndex].fInfo.bookingClassList;
            }

            this.$forceUpdate();
        },
        selectFlight: function (flIndex) {
            var selectFlightRq = this.createSelRqMinRq('select', flIndex, "", {});
            var selectedFlightInfo = this.flightResBind[flIndex];
            // selectedFlightInfo.newfare="90100";//fly dubai
            localStorage.selectedFlight = JSON.stringify({
                "selectFlightRq": selectFlightRq,
                "selectedFlightInfo": selectedFlightInfo
            });
            window.location.href = '/Flights/travellerdetails.html';
        },
        showAll: function (type) {
            switch (type) {
                case 1:
                    this.checkStops = [];
                    break;
                case 2:
                    this.checkAirlines = [];
                    break;
                case 3:
                    this.selDeptLoc = [];
                    break;
                case 4:
                    this.selArrtLoc = [];
                    break;
                case 5:
                    this.selFlSerList = [];
                    break;

                default:
                    break;
            }
        },
        showOnly: function (type, val) {
            switch (type) {
                case 1:
                    this.checkStops = [val];
                    break;
                case 2:
                    this.checkAirlines = [val];
                    if (val.air == "Multi-Airline") {
                        this.filterType = 2;
                    } else {
                        this.filterType = 1;
                    }
                    break;
                case 3:
                    this.selDeptLoc = [val];
                    break;
                case 4:
                    this.selArrtLoc = [val];
                    break;
                case 5:
                    this.selFlSerList = [val];
                    break;

                default:
                    break;
            }
        },
        showFilterMob: function () {
            $('.search_result_left').show();
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            //getAgencycode(function (response) {
            var vm = this;
            var huburl = vm.commonStore.hubUrls.hubConnection.cmsUrl;
            var portno = vm.commonStore.hubUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            //var langauage = 'ar';
            self.dir = langauage == "ar" ? "rtl" : "ltr";

            var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + self.commonAgencyCode + '/Template/Flight Results Page/Flight Results Page/Flight Results Page.ftl';
            axios.get(cmsPage, {
                headers: {
                    'content-type': 'text/html',
                    'Accept': 'text/html',
                    'Accept-Language': langauage
                }
            }).then(function (response) {
                self.content = response.data;
                if (response.data.area_List.length > 0) {
                    var mainComp = response.data.area_List[0].Main_Area;
                    var flightComp = response.data.area_List[1].Flight_Area;
                    //Main Section
                    self.Sort_Results_by_Label = self.pluckcom('Sort_Results_by_Label', mainComp.component);
                    self.Duration_Label = self.pluckcom('Duration_Label', mainComp.component);
                    self.Arrival_Label = self.pluckcom('Arrival_Label', mainComp.component);
                    self.From_Label = self.pluckcom('From_Label', mainComp.component);
                    self.To_Label = self.pluckcom('To_Label', mainComp.component);
                    self.Arrival_Date_Label = self.pluckcom('Arrival_Date_Label', mainComp.component);
                    self.Trip_Label = self.pluckcom('Trip_Label', mainComp.component);
                    self.Travel_Date_Label = self.pluckcom('Travel_Date_Label', mainComp.component);
                    self.Add_up_to_Label1 = self.pluckcom('Add_up_to_Label1', mainComp.component);
                    self.Add_up_to_Label2 = self.pluckcom('Add_up_to_Label2', mainComp.component);
                    self.Adult_Label = self.pluckcom('Adult_Label', mainComp.component);
                    self.Adult_option_Label = self.pluckcom('Adult_option_Label', mainComp.component);
                    self.Adults_option_Label = self.pluckcom('Adults_option_Label', mainComp.component);
                    self.Child_Label = self.pluckcom('Child_Label', mainComp.component);
                    self.Child_Option_Label = self.pluckcom('Child_Option_Label', mainComp.component);
                    self.Children_Option_Label = self.pluckcom('Children_Option_Label', mainComp.component);
                    self.Infant_Label = self.pluckcom('Infant_Label', mainComp.component);
                    self.Infant_Option_Label = self.pluckcom('Infant_Option_Label', mainComp.component);
                    self.Infants_Option_Label = self.pluckcom('Infants_Option_Label', mainComp.component);
                    self.Suppliers_Label = self.pluckcom('Suppliers_Label', mainComp.component);
                    self.Select_all_Label = self.pluckcom('Select_all_Label', mainComp.component);
                    self.Airline_Label = self.pluckcom('Airline_Label', mainComp.component);
                    self.Direct_flights_Label = self.pluckcom('Direct_flights_Label', mainComp.component);
                    self.Search_Flights_Label = self.pluckcom('Search_Flights_Label', mainComp.component);
                    self.Quotations_Label = self.pluckcom('Quotations_Label', mainComp.component);
                    self.Remove_Label = self.pluckcom('Remove_Label', mainComp.component);
                    self.Check_all_Label = self.pluckcom('Check_all_Label', mainComp.component);
                    self.Quote_List_Label = self.pluckcom('Quote_List_Label', mainComp.component);
                    self.Filters_Label = self.pluckcom('Filters_Label', mainComp.component);
                    self.Reset_Label = self.pluckcom('Reset_Label', mainComp.component);
                    self.Stop_Label = self.pluckcom('Stop_Label', mainComp.component);
                    self.Only_Label = self.pluckcom('Only_Label', mainComp.component);
                    self.Airlines_Label = self.pluckcom('Airlines_Label', mainComp.component);
                    self.Departure_Time_Label = self.pluckcom('Departure_Time_Label', mainComp.component);
                    self.Timings_Label = self.pluckcom('Timings_Label', mainComp.component);
                    self.Early_Morning_Label = self.pluckcom('Early_Morning_Label', mainComp.component);
                    self.Morning_Label = self.pluckcom('Morning_Label', mainComp.component);
                    self.Mid_Day_Label = self.pluckcom('Mid_Day_Label', mainComp.component);
                    self.Night_Label = self.pluckcom('Night_Label', mainComp.component);
                    self.Arrival_Time_Label = self.pluckcom('Arrival_Time_Label', mainComp.component);
                    self.Close_Filter_Label = self.pluckcom('Close_Filter_Label', mainComp.component);
                    self.Apply_Filters_Label = self.pluckcom('Apply_Filters_Label', mainComp.component);
                    self.Sort_by_Label = self.pluckcom('Sort_by_Label', mainComp.component);
                    self.Departure_Label = self.pluckcom('Departure_Label', mainComp.component);
                    self.Price_Label = self.pluckcom('Price_Label', mainComp.component);
                    self.Lowest_Label = self.pluckcom('Lowest_Label', mainComp.component);
                    self.Highest_Label = self.pluckcom('Highest_Label', mainComp.component);
                    self.Shortest_Label = self.pluckcom('Shortest_Label', mainComp.component);
                    self.Longest_Label = self.pluckcom('Longest_Label', mainComp.component);
                    self.Earliest_Label = self.pluckcom('Earliest_Label', mainComp.component);
                    self.Latest_Label = self.pluckcom('Latest_Label', mainComp.component);
                    self.Send_Label = self.pluckcom('Send_Label', mainComp.component);
                    self.Fare_info_Label = self.pluckcom('Fare_info_Label', mainComp.component);

                    //Flight Section
                    self.Trip_Label = self.pluckcom('Trip_Label', flightComp.component);
                    self.Departure_Label = self.pluckcom('Departure_Label', flightComp.component);
                    self.Returning_Label = self.pluckcom('Returning_Label', flightComp.component);
                    self.Arrival_Label = self.pluckcom('Arrival_Label', flightComp.component);
                    self.Non_stop_Label = self.pluckcom('Non_stop_Label', flightComp.component);
                    self.Book_Now_Label = self.pluckcom('Book_Now_Label', flightComp.component);
                    self.Flight_details_Label = self.pluckcom('Flight_details_Label', flightComp.component);
                    self.Free_cabin_Label = self.pluckcom('Free_cabin_Label', flightComp.component);
                    self.checked_baggage_Label = self.pluckcom('checked_baggage_Label', flightComp.component);
                    self.Refund_policy_Label = self.pluckcom('Refund_policy_Label', flightComp.component);
                    self.Itinerary_Label = self.pluckcom('Itinerary_Label', flightComp.component);
                    self.Baggage_Label = self.pluckcom('Baggage_Label', flightComp.component);
                    self.Amenities_Label = self.pluckcom('Amenities_Label', flightComp.component);
                    self.Category_Label = self.pluckcom('Category_Label', flightComp.component);
                    self.Baggage_Information_Label = self.pluckcom('Baggage_Information_Label', flightComp.component);
                    self.Flight_From_Label = self.pluckcom('Flight_From_Label', flightComp.component);
                    self.No_amenities_to_display_Label = self.pluckcom('No_amenities_to_display_Label', flightComp.component);
                    self.Category_Information_Label = self.pluckcom('Category_Information_Label', flightComp.component);
                    self.Stop_Label = self.pluckcom('Stop_Label', flightComp.component);
                    self.Net_Fare_Label = self.pluckcom('Net_Fare_Label', flightComp.component);
                    self.Taxes_Label = self.pluckcom('Taxes_Label', flightComp.component);
                    self.Fees_Label = self.pluckcom('Fees_Label', flightComp.component);
                    self.Discount_Label = self.pluckcom('Discount_Label', flightComp.component);
                    self.Service_Fee_Label = self.pluckcom('Service_Fee_Label', flightComp.component);
                    self.Commission_Label = self.pluckcom('Commission_Label', flightComp.component);
                    self.Total_Label = self.pluckcom('Total_Label', flightComp.component);
                    self.Add_Quote_Label = self.pluckcom('Add_Quote_Label', flightComp.component);
                    self.Cabin_baggage_Label = self.pluckcom('Cabin_baggage_Label', flightComp.component);
                    self.Cabin_Label = self.pluckcom('Cabin_Label', flightComp.component);
                    self.Layover_at_Label = self.pluckcom('Layover_at_Label', flightComp.component);
                    self.CheckIn_baggage_Label = self.pluckcom('CheckIn_baggage_Label', flightComp.component);
                    self.RBD_Label = self.pluckcom('RBD_Label', flightComp.component);
                    self.Refundable_Label = self.pluckcom('Refundable_Label', flightComp.component);
                    self.Non_Refundable_Label = self.pluckcom('Non_Refundable_Label', flightComp.component);
                    self.MiniRule_Label = self.pluckcom('MiniRule_Label', flightComp.component);
                    self.FareRule_Label = self.pluckcom('FareRule_Label', flightComp.component);
                    self.Base_Fare_Label = self.pluckcom('Base_Fare_Label', flightComp.component);

                    self.You_Earn_Commission = self.pluckcom('You_Earn_Commission', flightComp.component);
                    self.Seat_Left_label = self.pluckcom('Seat_Left_label', flightComp.component);
                    self.Upsell_label = self.pluckcom('Upsell_label', flightComp.component);
                    self.Upsell_Tab_Label = self.pluckcom('Upsell_Tab_Label', flightComp.component);
                    self.summary = self.pluckcom('Summary_Label', flightComp.component);
                    self.Detail = self.pluckcom('Details_Label', flightComp.component);
                    self.after = self.pluckcom('After_Departure_Label', flightComp.component);
                    self.before = self.pluckcom('Before_Departure_Label', flightComp.component);
                    self.rule = self.pluckcom('Rules_Label', flightComp.component);

                }
            }).catch(function (error) {
                console.log(error);
            });
            //});

        },
        // getSuppNameandCount: function (flSer) {
        //     var res = '';
        //     switch (flSer.id) {
        //         case 1:
        //             res = flSer.name + '(' + this.ameduesCount + ')'
        //             break;
        //         case 3:
        //             res = flSer.name + '(' + this.mystiflyCount + ')'
        //             break;
        //         case 18:
        //             res = flSer.name + '(' + this.salamairCount + ')'
        //             break;
        //         case 42:
        //             res = flSer.name + '(' + this.flycreativeCount + ')'
        //             break;
        //         case 44:
        //             res = flSer.name + '(' + this.airarabiaG9Count + ')'
        //             break;
        //         case 49:
        //             res = flSer.name + '(' + this.airarabia3OCount + ')'
        //             break;
        //         case 50:
        //             res = flSer.name + '(' + this.airarabiaE5Count + ')'
        //             break;
        //         case 45:
        //             res = flSer.name + '(' + this.travelportgalileoCount + ')'
        //             break;
        //         case 54:
        //             res = flSer.name + '(' + this.fludybaiCount + ')'
        //             break;
        //         case 55:
        //             res = flSer.name + '(' + this.sabreCount + ')'
        //             break;
        //         case 64:
        //             res = flSer.name + '(' + this.techmasterCount + ')'
        //             break;
        //         case 69:
        //             res = flSer.name + '(' + this.aljazeeraCount + ')'
        //             break;
        //         case 71:
        //             res = flSer.name + '(' + this.airarabia3LCount + ')'
        //             break;
        //         case 57:
        //             res = flSer.name + '(' + this.piaHititCount + ')'
        //             break;
        //     }
        //     return res;
        // },
        getRefundDetails: function (refund, supplier) {
            var self = this;
            var res = '';
            refund = (isNullorUndefined(refund) ? '' : refund);
            refund = this.isRefundableFlight(refund)
            if (refund.toLowerCase() == 'yes' || refund.toLowerCase() == 'refundable') {
                res = '<span data-toggle="tooltip" data-placement="top" style="color: #15d015;"' +
                    ' v-bind:title=""><i class="fa fa-dollar"></i>' +
                    self.Refundable_Label + '</span>'
            } else if (refund.toLowerCase() == 'no') {
                res = '<span data-toggle="tooltip" data-placement="top" style="color: red;"' +
                    ' v-bind:title=""><i class="fa fa-dollar"></i>' +
                    self.Non_Refundable_Label + '</span>'
            } else {
                // res = '<span data-toggle="tooltip" data-placement="top" style="cursor: pointer;" ' +
                //     'data-toggle="tooltip" data-placement="top" title="" data-original-title="' + this.isRefundableFlight(refund) + '"><i class="fa fa-dollar"></i>' +
                //     self.Refund_policy_Label + '</span>'
            }
            return res;
        },
        Showsearchsummary: function (Req) {
            var Request = Req.request.content.criteria;
            var from = '';
            var to = '';
            var depdatee = '';
            var retdate = '';
            if (Request.originDestinationInformation.length == 1) {
                from = Request.originDestinationInformation[0].originLocation;
                to = Request.originDestinationInformation[0].destinationLocation;
                depdatee = Request.originDestinationInformation[0].departureDate;
            } else if (Request.originDestinationInformation.length == 2) {
                depdatee = Request.originDestinationInformation[0].departureDate;
                retdate = Request.originDestinationInformation[1].departureDate
                from = Request.originDestinationInformation[0].originLocation;
                to = Request.originDestinationInformation[1].originLocation;
            }
            if (Request.tripType.toLowerCase() == 'm') {
                from = Request.originDestinationInformation[0].originLocation;
                to = Request.originDestinationInformation[Request.originDestinationInformation.length - 1].destinationLocation;
                depdatee = Request.originDestinationInformation[0].departureDate;
                retdate = Request.originDestinationInformation[Request.originDestinationInformation.length - 1].departureDate
            }
            Request.cabin = Request.cabin.toUpperCase();
            depdatee = moment(depdatee, 'DD-MM-YYYY').format('DD/MM/YYYY');
            retdate = moment(retdate, 'DD-MM-YYYY').format('DD/MM/YYYY');

            var newdepdate = depdatee;
            var newretdate = retdate;
            //Oneway
            if (Request.tripType.toLowerCase() == 'o' || Request.tripType.toLowerCase() == 'r' || Request.tripType.toLowerCase() == 'm') {
                $('#h5MulLab').hide();
                if (this.DateFormate == 'dd/mm/yy') {
                    depdatee = new Date(depdatee.split('/')[1] + '-' + depdatee.split('/')[0] + '-' + depdatee.split('/')[2]);
                    retdate = new Date(retdate.split('/')[1] + '-' + retdate.split('/')[0] + '-' + retdate.split('/')[2]);
                    if (depdatee == 'Invalid Date') {
                        depdatee = newdepdate;
                    }
                    if (retdate == 'Invalid Date') {
                        retdate = newretdate;
                    }
                } else {
                    depdatee = new Date(depdatee);
                    retdate = new Date(retdate);
                }
                var datepax = '';
                var onward = moment(depdatee, this.DateFormateConv).format("DD MMM YY");
                datepax += ' <i class="fa fa-calendar" aria-hidden="true"></i> Onward <span>' + onward + '</span>';

                if (Request.tripType.toLowerCase() == 'r') { //Return
                    var returndate = moment(retdate, this.DateFormateConv).format("DD MMM YY");
                    datepax += ' , Return <span>' + returndate + '</span>';
                }
                if (Request.tripType.toLowerCase() == 'm') { //Multicity
                    var returndate = moment(retdate, this.DateFormateConv).format("DD MMM YY");
                    datepax += ' , Last Trip <span>' + returndate + '</span>';
                    $('#h5MulLab').show();
                }
                var totpaxcount = 'ADT : ' + parseInt(Request.passengerTypeQuantity.adt) + '';
                if (parseInt(Request.passengerTypeQuantity.chd) > 0) {
                    totpaxcount += '  CHD : ' + parseInt(Request.passengerTypeQuantity.chd);
                }
                if (parseInt(Request.passengerTypeQuantity.inf) > 0) {
                    totpaxcount += ' INF : ' + parseInt(Request.passengerTypeQuantity.inf);
                }
                datepax += '<i class="fa fa-users icn_htl" style="margin-right: 6px; margin-left: 8px;" aria-hidden="true"></i> <span>' + totpaxcount + '</span>';
                this.summDepArrDatePax = '<h3> <span class="deptCity"><span>' + this.getCityNameUTCOnly(from) + ' (' + from + ')</span></span>' +
                    '<span>_ _ _ _ _<i class="fa fa-plane"></i>_ _ _ _ _</span>' +
                    '<span class="arriCity"></span><span>' + this.getCityNameUTCOnly(to) + ' (' + to + ')</span></h3>' +
                    '<h4><span class="datepax">' + datepax + '</span></h4>'

            } else {
                $('.modify_details_left').hide();
            }
        },
        setSearchHistory: function () {
            try {
                var flightUrl = window.location.href;
                flightUrl = flightUrl.split('flight=/')[1];
                if (!stringIsNullorEmpty(flightUrl)) {
                    var airSearchHistories = [];
                    var previousSearchesJson = $.cookie("AirSearchHistory");
                    if (!stringIsNullorEmpty(previousSearchesJson)) {
                        airSearchHistories = JSON.parse(previousSearchesJson);
                        airSearchHistories = airSearchHistories.filter(function (history) {
                            return history.searchKey.toLowerCase() != flightUrl.toLowerCase()
                        });
                    }
                    var searchTime = moment().format('DD-MM-YYYY HH:mm:ss');
                    var searchKey = this.generateKey(flightUrl);
                    var airSearchHistory = {
                        searchKey: flightUrl,
                        searchTime: searchTime,
                        key: searchKey
                    }
                    airSearchHistories.push(airSearchHistory);
                    if (airSearchHistories.length > 0) {
                        $.cookie("AirSearchHistory", JSON.stringify(airSearchHistories), {
                            expires: 50,
                            path: '/'
                        });
                    }
                }
            } catch (err) {}
        },
        getCityNameUTCOnly: function (airportcode) {
            var CityName = _.where(AirlinesTimezone, {
                I: airportcode
            });
            if (CityName == "") {
                CityName = airportcode;
            } else {
                CityName = CityName[0].C;
            }
            return CityName;
        },
        generateKey: function (flightUrl) {
            var airSearchKey = flightUrl.split('/').slice(0, -1).join('-') + '-' + flightUrl.split('/').pop().split('-')[0] + flightUrl.split('/').pop().split('-')[1] +
                flightUrl.split('/').pop().split('-')[2];
            return airSearchKey;
        },
        popMiniRule: function (flight, index) {
            var isupdated = $('#divflightminirules_' + index).data("isupdated");
            if (isupdated == 'false' || isupdated == false) {
                $('#divflightminirules_' + index).html('<div class="resultprogress_load animated-background" style="height:47px"></div>');
                this.selectFlightForMiniRule(flight, index, 0);
            }
            $('#divflightminirules_' + index).data('isupdated', 'true');
            try {
                document.getElementById('tabmini_' + index).getElementsByTagName('button')[0].click();
            } catch (ex) {}
        },
        selectFlightForMiniRule: function (flight, IDNo, cat) {
            var JsonSelectedFL = flight;
            JsonSelectedFL.CategoryId = cat;
            var totalleg = JsonSelectedFL.groupOfFlights.length;
            var depfrom1 = "";
            var arrto1 = "";
            if (this.tripType.toLowerCase() == "o") {
                var totalflightdt = JsonSelectedFL.groupOfFlights[totalleg - 1].flightDetails.length;
                var JsonSeg1 = JsonSelectedFL.groupOfFlights[0].flightDetails[0];
                var JsonSeg2 = JsonSelectedFL.groupOfFlights[totalleg - 1].flightDetails[totalflightdt - 1];
                depfrom1 = JsonSeg1.fInfo.location.locationFrom;
                arrto1 = JsonSeg2.fInfo.location.locationTo;
            } else if (this.tripType.toLowerCase() == "r") {
                var totalflightdt = JsonSelectedFL.groupOfFlights[totalleg - 1].flightDetails.length;
                var JsonSeg1 = JsonSelectedFL.groupOfFlights[0].flightDetails[0];
                if (totalflightdt > 1) {
                    var JsonSeg2 = JsonSelectedFL.groupOfFlights[totalleg - 1].flightDetails[totalflightdt - 2];
                    depfrom1 = JsonSeg1.fInfo.location.locationFrom;
                    arrto1 = JsonSeg2.fInfo.location.locationFrom;
                } else {
                    var JsonSeg2 = JsonSelectedFL.groupOfFlights[totalleg - 1].flightDetails[totalflightdt - 1];
                    depfrom1 = JsonSeg1.fInfo.location.locationFrom;
                    arrto1 = JsonSeg2.fInfo.location.locationFrom;
                }
            } else {
                var currtotalleg = totalleg;
                if (currtotalleg == 1 || JsonSelectedFL.segmentRef.supplier == '3') currtotalleg = 2;
                else if (currtotalleg == 2) currtotalleg = 3;
                var totalflightdt = JsonSelectedFL.groupOfFlights[currtotalleg - 2].flightDetails.length;
                var JsonSeg1 = JsonSelectedFL.groupOfFlights[0].flightDetails[0];
                if (totalflightdt < 1) totalflightdt = 1;
                var JsonSeg2 = JsonSelectedFL.groupOfFlights[currtotalleg - 2].flightDetails[totalflightdt - 1];
                depfrom1 = JsonSeg1.fInfo.location.locationFrom;
                arrto1 = JsonSeg2.fInfo.location.locationTo;
            }

            var segmentGroup = [];
            for (var FLleg = 0; FLleg < totalleg; FLleg++) {
                var totalseg = JsonSelectedFL.groupOfFlights[FLleg].flightDetails.length;
                for (var FLseg = 0; FLseg < totalseg; FLseg++) {
                    var JsonSeg = JsonSelectedFL.groupOfFlights[FLleg].flightDetails[FLseg];
                    var DepDate = JsonSeg.fInfo.dateTime.depDate;
                    var ArrDate = JsonSeg.fInfo.dateTime.arrDate;
                    var DepTime = JsonSeg.fInfo.dateTime.depTime;
                    var ArrTime = JsonSeg.fInfo.dateTime.arrTime;
                    var DepFrom = JsonSeg.fInfo.location.locationFrom;
                    var ArrTo = JsonSeg.fInfo.location.locationTo;
                    var Mcarrier = JsonSeg.fInfo.companyId.mCarrier;
                    var Ocarrier = JsonSeg.fInfo.companyId.oCarrier;
                    var Vcarrier = JsonSelectedFL.groupOfFlights[FLleg].flightProposal.vAirline;
                    var FLNo = JsonSeg.fInfo.flightNo;
                    var bookingclas = JsonSeg.fInfo.rbd;
                    var farebasicode = '';
                    var fareid = 12;
                    if (JsonSelectedFL.segmentRef.supplier == "PYT001" || JsonSelectedFL.segmentRef.supplier == "42") {
                        try {
                            farebasicode = JsonSelectedFL.fareDetails.fareBasis;
                        } catch (err) {}
                    } else if (JsonSelectedFL.segmentRef.supplier == "18") {
                        try {
                            fareid = JsonSelectedFL.groupOfFlights[FLleg].flightProposal.unitQualifier;
                            farebasicode = JsonSelectedFL.category[cat].fareID;
                        } catch (err) {}
                    } else if (JsonSelectedFL.segmentRef.supplier == 1) {
                        try {
                            farebasicode = JsonSelectedFL.typeRef.type;
                        } catch (err) {}

                    }
                    // else if (JsonSelectedFL.segmentRef.supplier == "44" || JsonSelectedFL.segmentRef.supplier == "49" || JsonSelectedFL.segmentRef.supplier == "50" || JsonSelectedFL.segmentRef.supplier == "71") {
                    //     try { farebasicode = JsonSelectedFL.segmentRef.segRef; } catch (err) {}
                    // } 
                    else if (JsonSelectedFL.segmentRef.supplier == "45") {
                        try {
                            farebasicode = JsonSelectedFL.segmentRef.segRef;
                            fareid = JsonSelectedFL.groupOfFlights[FLleg].flightDetails[FLseg].fInfo.eqpType;
                            var tpgsessionid = JsonSelectedFL.segmentRef.pSessionId;
                            Mcarrier = tpgsessionid;
                            Ocarrier = tpgsessionid;
                        } catch (err) {}
                    } else {
                        farebasicode = JsonSelectedFL.segmentRef.segRef;
                    }
                    var Seginfo = {
                        segmentInformation: {
                            flightDate: {
                                departureDate: DepDate,
                                departureTime: DepTime,
                                arrivalDate: ArrDate,
                                arrivalTime: ArrTime
                            },
                            fareId: fareid,
                            boardPointDetails: {
                                trueLocationId: DepFrom
                            },
                            offPointDetails: {
                                trueLocationId: ArrTo
                            },
                            companyDetails: {
                                marketingCompany: Mcarrier,
                                operatingCompany: Ocarrier,
                                validatingCompany: Vcarrier
                            },
                            flightIdentification: {
                                flightNumber: FLNo,
                                bookingClass: bookingclas
                            },
                            flightTypeDetails: {
                                flightIndicator: String(FLleg + 1),
                                itemNumber: FLseg + 1
                            }
                        }
                    }
                    segmentGroup.push(Seginfo);
                }
            }

            var searchData = this.getSearchData(0);

            // var data = {
            //     request: {
            //         service: "FlightRQ",
            //         supplierCodes: [JsonSelectedFL.segmentRef.supplier],
            //         content: {
            //             command: "FlightMiniRulesRQ",
            //             commonRequestFarePricer: {
            //                 body: {
            //                     airRevalidate: {
            //                         supplierId: JsonSelectedFL.segmentRef.supplier,
            //                         fareSourceCode: farebasicode,
            //                         adt: searchData.request.content.criteria.passengerTypeQuantity.adt,
            //                         chd: searchData.request.content.criteria.passengerTypeQuantity.chd,
            //                         inf: searchData.request.content.criteria.passengerTypeQuantity.inf,
            //                         tripType: this.tripType,
            //                         from: depfrom1,
            //                         to: arrto1,
            //                         paymentCardType: null,
            //                         segmentGroup: segmentGroup
            //                     }
            //                 }
            //             },
            //             supplierSpecific: {}
            //         }
            //     }
            // }
            var data = this.createSelRqMinRq('select', IDNo, "", {});
            var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = this.commonStore.hubUrls.hubConnection.hubServices.flights.airSelect;
            var vm = this;
            var config = {
                axiosConfig: {
                    method: "post",
                    url: hubUrl + serviceUrl,
                    data: data
                },
                successCallback: function (response) {
                    // var response = '{"data":{"minirules":[{"fc":"1","departure":"AUH","arrival":"LON","codes":[{"category":[{"cat":"5","desc":"Advance Reservation/Ticketing","dateinfo":[{"qualifier":"LTR","date":"05SEP19","time":"2359","desc":"Latest ticketing date after reservation","alias":"","situation":""}]},{"cat":"31","desc":"Changes/Reissue","dateinfo":[{"qualifier":"BDV","date":"02SEP20","time":"0000","desc":"Ticket validity date","alias":"","situation":"Before departure"},{"qualifier":"BNV","date":"02SEP20","time":"0000","desc":"Ticket validity date for no show","alias":"","situation":"Before departure"},{"qualifier":"ADV","date":"17OCT20","time":"0000","desc":"Ticket validity date","alias":"","situation":"After departure"},{"qualifier":"ANV","date":"17OCT20","time":"0000","desc":"Ticket validity date when no show","alias":"","situation":"After departure"}],"moninfo":[{"typeQualifier":"BDM","amount":"0","currency":"AED","desc":"Reissue minimum Penalty amount with sale currency","alias":"","situation":"Before departure"},{"typeQualifier":"BDX","amount":"240","currency":"AED","desc":"Reissue maximum penalty amount with sale currency","alias":"","situation":"Before departure"},{"typeQualifier":"BDF","amount":"0","currency":"AED","desc":"Reissue minimum Penalty amount with filing currenc","alias":"","situation":"Before departure"},{"typeQualifier":"BDG","amount":"240","currency":"AED","desc":"Reissue maximum penalty amount with filing currency","alias":"","situation":"Before departure"},{"typeQualifier":"BDT","amount":"240","currency":"AED","desc":"Reissue maximum penalty amount for the ticket","alias":"","situation":"Before departure"},{"typeQualifier":"BDI","amount":"0","currency":"AED","desc":"Revalidation minimum Penalty amount with sale currency","alias":"","situation":"Before departure"},{"typeQualifier":"BDU","amount":"0","currency":"AED","desc":"Revalidation maximum penalty amount with sale currency","alias":"","situation":"Before departure"},{"typeQualifier":"BDH","amount":"0","currency":"AED","desc":"Revalidation minimum Penalty amount with filing currency","alias":"","situation":"Before departure"},{"typeQualifier":"BDL","amount":"0","currency":"AED","desc":"Revalidation maximum penalty amount with filing currency","alias":"","situation":"Before departure"},{"typeQualifier":"BDC","amount":"0","currency":"AED","desc":"Revalidation maximum penalty amount for the ticket","alias":"","situation":"Before departure"},{"typeQualifier":"BNM","amount":"720","currency":"AED","desc":"Reissue minimum Penalty amount with sale currency when no show","alias":"","situation":"Before departure"},{"typeQualifier":"BNX","amount":"720","currency":"AED","desc":"Reissue maximum penalty amount with sale currency when no show","alias":"","situation":"Before departure"},{"typeQualifier":"BNF","amount":"720","currency":"AED","desc":"Reissue minimum Penalty amount with filing currency when no show","alias":"","situation":"Before departure"},{"typeQualifier":"BNG","amount":"720","currency":"AED","desc":"Reissue maximum penalty amount with filing currency when no show","alias":"","situation":"Before departure"},{"typeQualifier":"BNT","amount":"720","currency":"AED","desc":"Reissue maximum penalty amount for the ticket when no show","alias":"","situation":"Before departure"},{"typeQualifier":"BNI","amount":"0","currency":"AED","desc":"Revalidation minimum Penalty amount with sale currency when no show","alias":"","situation":"Before departure"},{"typeQualifier":"BNU","amount":"0","currency":"AED","desc":"Revalidation maximum penalty amount with sale currency when no show","alias":"","situation":"Before departure"},{"typeQualifier":"BNH","amount":"0","currency":"AED","desc":"Revalidation minimum penalty amount with sale currency when no show","alias":"","situation":"Before departure"},{"typeQualifier":"BNL","amount":"0","currency":"AED","desc":"Revalidation maximum penalty amount with filing currency when no show","alias":"","situation":"Before departure"},{"typeQualifier":"BNC","amount":"0","currency":"AED","desc":"Revalidation maximum penalty amount for the ticket when no show","alias":"","situation":"Before departure"},{"typeQualifier":"ADM","amount":"0","currency":"AED","desc":"Reissue minimum Penalty amount with sale currency","alias":"","situation":"After departure"},{"typeQualifier":"ADX","amount":"240","currency":"AED","desc":"Reissue maximum penalty amount with filing currency","alias":"","situation":"After departure"},{"typeQualifier":"ADF","amount":"0","currency":"AED","desc":"Reissue minimum Penalty amount with filing currency","alias":"","situation":"After departure"},{"typeQualifier":"ADG","amount":"240","currency":"AED","desc":"Reissue maximum penalty amount with filing currency","alias":"","situation":"After departure"},{"typeQualifier":"ADT","amount":"240","currency":"AED","desc":"Reissue maximum penalty amount for the ticket","alias":"","situation":"After departure"},{"typeQualifier":"ADI","amount":"0","currency":"AED","desc":"Revalidation minimum Penalty amount with sale currency ","alias":"","situation":"After departure"},{"typeQualifier":"ADU","amount":"0","currency":"AED","desc":"Revalidation Maximum penalty amount with sale currency","alias":"","situation":"After departure"},{"typeQualifier":"ADH","amount":"0","currency":"AED","desc":"Revalidation Minimum Penalty amount with filing currency","alias":"","situation":"After departure"},{"typeQualifier":"ADL","amount":"0","currency":"AED","desc":"Revalidation Maximum penalty amount with filing currency","alias":"","situation":"After departure"},{"typeQualifier":"ADC","amount":"0","currency":"AED","desc":"Revalidation Maximum penalty amount for the ticket","alias":"","situation":"After departure"},{"typeQualifier":"ANM","amount":"720","currency":"AED","desc":"Reissue minimum Penalty amount with sale currency when no show","alias":"","situation":"After departure"},{"typeQualifier":"ANX","amount":"720","currency":"AED","desc":"Reissue maximum penalty amount with sale currency when no show","alias":"","situation":"After departure"},{"typeQualifier":"ANF","amount":"720","currency":"AED","desc":"Reissue minimum Penalty amount with filing currency when no show","alias":"","situation":"After departure"},{"typeQualifier":"ANG","amount":"720","currency":"AED","desc":"Reissue maximum penalty amount with filing currency when no show","alias":"","situation":"After departure"},{"typeQualifier":"ANT","amount":"720","currency":"AED","desc":"Reissue maximum penalty amount for the ticket when no show","alias":"","situation":"After departure"},{"typeQualifier":"ANI","amount":"0","currency":"AED","desc":"Revalidation minimum Penalty amount with sale currency when no show","alias":"","situation":"After departure"},{"typeQualifier":"ANU","amount":"0","currency":"AED","desc":"Revalidation maximum penalty amount with sale currency when no show","alias":"","situation":"After departure"},{"typeQualifier":"ANH","amount":"0","currency":"AED","desc":"Revalidation minimum Penalty amount with filing currency when no show","alias":"","situation":"After departure"},{"typeQualifier":"ANL","amount":"0","currency":"AED","desc":"Revalidation maximum penalty amount with filing currency when no show","alias":"","situation":"After departure"},{"typeQualifier":"ANC","amount":"0","currency":"AED","desc":"Revalidation maximum penalty amount for the ticket when no show","alias":"","situation":"After departure"}],"restriappinfo":[{"indicator":"FFT","action":"0","value":"No free form text from Cat16","desc":"Cat 16 Free Text","alias":"","situation":""},{"indicator":"WAI","action":"0","value":"No waiver","desc":"Penalties can be waived for passenger and family death/illness","alias":"","situation":"Before departure"},{"indicator":"RVA","action":"0","value":"Reval not allowed","desc":"Revalidation","alias":"","situation":"Before departure"},{"indicator":"BDA","action":"1","value":"Allowed with restrictions","desc":"Reissue","alias":"","situation":"Before departure"},{"indicator":"BNW","action":"0","desc":"Penalties can be waived for passenger and family death/illness when no show","alias":"","situation":"Before departure"},{"indicator":"BNR","action":"0","value":"Reval not allowed","desc":"Revalidation when no show","alias":"","situation":"Before departure"},{"indicator":"BNA","action":"1","value":"Allowed with restrictions","desc":"Reissue when no show","alias":"","situation":"Before departure"},{"indicator":"ADW","action":"0","desc":"Penalties can be waived for passenger and family death/illness","alias":"","situation":"After departure"},{"indicator":"ADR","action":"0","desc":"Revalidation","alias":"","situation":"After departure"},{"indicator":"ADA","action":"1","desc":"Reissue","alias":"","situation":"After departure"},{"indicator":"ANW","action":"0","desc":"Penalties can be waived for passenger and family death/illness when no show","alias":"","situation":"After departure"},{"indicator":"ANR","action":"0","value":"Reval not allowed","desc":"Revalidation when no show","alias":"","situation":"After departure"},{"indicator":"ANA","action":"1","value":"Allowed with restrictions","desc":"Reissue when no show","alias":"","situation":"After departure"},{"indicator":"GUA","action":"1","value":"Guaranteed","desc":"Amout garanteed flag for cat 31/33","alias":"","situation":""}]},{"cat":"33","desc":"Refund","moninfo":[{"typeQualifier":"BDM","amount":"480","currency":"AED","desc":"Minimum Penalty amount with sale currency","alias":"","situation":"Before departure"},{"typeQualifier":"BDX","amount":"480","currency":"AED","desc":"Maximum penalty amount with sale currency","alias":"","situation":"Before departure"},{"typeQualifier":"BDF","amount":"480","currency":"AED","desc":"Minimum Penalty amount with filing currency","alias":"","situation":"Before departure"},{"typeQualifier":"BDG","amount":"480","currency":"AED","desc":"Maximum penalty amount with filing currency","alias":"","situation":"Before departure"},{"typeQualifier":"BDT","amount":"480","currency":"AED","desc":"Maximum penality amount for the ticket","alias":"","situation":"Before departure"},{"typeQualifier":"BNM","amount":"0","currency":"AED","desc":"Minimum Penalty amount with sale currency when no show","alias":"","situation":"Before departure"},{"typeQualifier":"BNX","amount":"0","currency":"AED","desc":"Maximum penalty amount with sale currency when no show","alias":"","situation":"Before departure"},{"typeQualifier":"BNF","amount":"0","currency":"AED","desc":"Minimum Penalty amount with filing currency when no show","alias":"","situation":"Before departure"},{"typeQualifier":"BNG","amount":"0","currency":"AED","desc":"Maximum penalty amount with filing currency when no show","alias":"","situation":"Before departure"},{"typeQualifier":"BNT","amount":"0","currency":"AED","desc":"Maximum penalty amount for the ticket when no show","alias":"","situation":"Before departure"},{"typeQualifier":"ADM","amount":"0","currency":"AED","desc":"Minimum Penalty amount with sale currency","alias":"","situation":"After departure"},{"typeQualifier":"ADX","amount":"0","currency":"AED","desc":"Maximum penalty amount with sale currency","alias":"","situation":"After departure"},{"typeQualifier":"ADF","amount":"0","currency":"AED","desc":"Minimum Penalty amount with filing currency","alias":"","situation":"After departure"},{"typeQualifier":"ADG","amount":"0","currency":"AED","desc":"Maximum penalty amount with filing currency","alias":"","situation":"After departure"},{"typeQualifier":"ADT","amount":"0","currency":"AED","desc":"Maximum penalty amount for the ticket","alias":"","situation":"After departure"},{"typeQualifier":"ANM","amount":"0","currency":"AED","desc":"Minimum Penalty amount with sale currency when no show","alias":"","situation":"After departure"},{"typeQualifier":"ANX","amount":"0","currency":"AED","desc":"Maximum penalty amount with sale currency when no show","alias":"","situation":"After departure"},{"typeQualifier":"ANF","amount":"0","currency":"AED","desc":"Minimum Penalty amount with filing currency when no show","alias":"","situation":"After departure"},{"typeQualifier":"ANG","amount":"0","currency":"AED","desc":"Maximum penalty amount with filing currency when no show","alias":"","situation":"After departure"},{"typeQualifier":"ANT","amount":"0","currency":"AED","desc":"Maximum penalty amount for the ticket when no show","alias":"","situation":"After departure"}],"restriappinfo":[{"indicator":"FFT","action":"0","value":"No free form text from Cat16","desc":"Cat 16 Free Text","alias":"","situation":""},{"indicator":"BDA","action":"1","value":"Allowed with restrictions","desc":"Refund","alias":"","situation":"Before departure"},{"indicator":"BNA","action":"0","value":"Not allowed","desc":"Refund when no show","alias":"","situation":"Before departure"},{"indicator":"ADA","action":"1","value":"Allowed with restrictions","desc":"Refund","alias":"","situation":"After departure"},{"indicator":"GUA","action":"1","value":"Guaranteed","desc":"Amout garanteed for cat 31/33","alias":"","situation":""}]}],"rateclass":"LS2PXOW"}]}]},"message":"Data retrieved successfully","responseMessage":{"version":{"major":1,"minor":1,"build":-1,"revision":-1,"majorRevision":-1,"minorRevision":-1},"statusCode":200,"reasonPhrase":"OK","isSuccessStatusCode":true},"isSuccessful":true}';
                    if (response) {
                        try {
                            vm.bindMiniRules(response.data.response.content.fareInformationWithoutPnrReply.minirules, IDNo);
                        } catch (err) {
                            $('#divflightminirules_' + IDNo).html('<p>Mini rules not available for this trip !</p>');
                        }
                    }
                },
                errorCallback: function (error) {
                    $('#divflightminirules_' + IDNo).html('<p>Mini rules not available for this trip !</p>');
                },
                showAlert: false
            };

            mainAxiosRequest(config);
        },
        bindMiniRules: function (airMiniRules, divindex) {
            var vm = this;
            var miniRuleDiv = '';
            var miniRuleHeadDiv = '';
            // console.log(airMiniRules);
            $.each(airMiniRules, function (segIndex, miniRule) {
                if (vm.hasMiniRule(miniRule)) {
                    if (isNullorEmpty(miniRuleHeadDiv)) {
                        miniRuleHeadDiv +=
                            '<div class="mini_rule" id="mini_rule_' + divindex + '">' +
                            '<ul>';
                    }
                    var titleHead = airportLocationFromAirportCode(miniRule.departure) + '(' + miniRule.departure.toUpperCase() + ') to ' + airportLocationFromAirportCode(miniRule.arrival) + '(' + miniRule.arrival.toUpperCase() + ')';
                    miniRuleHeadDiv +=
                        '<li class="mini_tablinks mini_tablinks_' + divindex + '" id="mini_tablinks_' + divindex + '" onclick="openTab(event, ' + segIndex + ', ' + divindex + ', \'' + titleHead + '\')">' + miniRule.departure.toUpperCase() + ' to ' + miniRule.arrival.toUpperCase() + '</li>';
                }
            });
            if (isNullorEmpty(miniRuleHeadDiv)) {
                miniRuleHeadDiv += '</ul>' + '</div>' + '<h3 class="mini_tab_head" id="mini_tab_head_' + divindex + '">No mini rules</h3>';
            }

            var miniRuleCatDiv = '';
            var miniRuleCatTabDescDiv = '';
            $.each(airMiniRules, function (segIndex, miniRule) {
                if (vm.hasMiniRule(miniRule)) {
                    miniRuleCatDiv += '<div class="tab_rule tab_rule_' + segIndex + '" id="tab_rule_' + segIndex + '_' + divindex + '" style="display:none;">';
                    var miniRuleCatTabDiv = '';
                    try {
                        var totCat = miniRule.codes[0].category.length;
                        $.each(miniRule.codes[0].category, function (categoryIndex, catgry) {
                            if (
                                (!isNullorEmpty(catgry.dateInfo) && !isNullorUndefind(catgry.dateInfo)) ||
                                (!isNullorEmpty(catgry.monInfo) && !isNullorUndefind(catgry.monInfo)) ||
                                (!isNullorEmpty(catgry.restriAppInfo) && !isNullorUndefind(catgry.restriAppInfo))
                            ) {
                                if (categoryIndex == 0) {
                                    miniRuleCatDiv += '<div class="tabmini" id="tabmini_' + divindex + '">';
                                }
                                miniRuleCatDiv += '<button class="tablinks" onclick="openSubTab(event, ' + segIndex + ', ' + catgry.cat + ', ' + divindex + ')"' + (categoryIndex == 0 ? ' id="defaultOpen"' : '') + '>' + catgry.desc + '</button>';

                                miniRuleCatTabDiv += '<div id="catgry_' + segIndex + '_' + catgry.cat + '_' + divindex + '" class="tabcontent tabcontent_' + divindex + '"' + (categoryIndex != 0 ? ' style="display:none;"' : '') + '>';
                                miniRuleCatTabDiv += '<div class="content_tab_sec">';

                                // Restrictions Applicability Info
                                if (!isNullorEmpty(catgry.restriAppInfo) && !isNullorUndefind(catgry.restriAppInfo)) {
                                    miniRuleCatTabDiv += '<h3>Restrictions Applicability Info</h3>';
                                    $.each(catgry.restriAppInfo, function (restriappinfoIndex, restriappinfo) {
                                        miniRuleCatTabDiv += '<p>';
                                        miniRuleCatTabDiv += isNullorEmptyToBlank(restriappinfo.desc);
                                        miniRuleCatTabDiv += ((isNullorEmpty(restriappinfo.situation) ? '' : ' ' + restriappinfo.situation.toLowerCase()));
                                        miniRuleCatTabDiv += ((isNullorEmpty(restriappinfo.value) ? '' : ' - ' + restriappinfo.value));
                                        miniRuleCatTabDiv += '</p>';
                                    });
                                }

                                // Monetary Info
                                if (!isNullorEmpty(catgry.monInfo) && !isNullorUndefind(catgry.monInfo)) {
                                    miniRuleCatTabDiv += '<h3>Monetary Info</h3>';
                                    $.each(catgry.monInfo, function (moninfoIndex, moninfo) {
                                        miniRuleCatTabDiv += '<p>';
                                        miniRuleCatTabDiv += isNullorEmptyToBlank(moninfo.desc);
                                        miniRuleCatTabDiv += ((isNullorEmpty(moninfo.situation) ? '' : ' ' + moninfo.situation.toLowerCase()));
                                        miniRuleCatTabDiv += ((isNullorEmpty(moninfo.currency) && isNullorEmpty(moninfo.amount) ? '' : ' - '));
                                        miniRuleCatTabDiv += isNullorEmptyToBlank(moninfo.currency) + ' ' + isNullorEmptyToBlank(moninfo.amount);
                                        miniRuleCatTabDiv += '</p>';
                                    });
                                }

                                // Date Info
                                if (!isNullorEmpty(catgry.dateInfo) && !isNullorUndefind(catgry.dateInfo)) {
                                    miniRuleCatTabDiv += '<h3>Date Info</h3>';
                                    $.each(catgry.dateInfo, function (dateinfoIndex, dateinfo) {
                                        miniRuleCatTabDiv += '<p>';
                                        miniRuleCatTabDiv += isNullorEmptyToBlank(dateinfo.desc);
                                        miniRuleCatTabDiv += ((isNullorEmpty(dateinfo.situation) ? '' : ' ' + dateinfo.situation.toLowerCase()));
                                        miniRuleCatTabDiv += ((isNullorEmpty(dateinfo.date) && isNullorEmpty(dateinfo.time) ? '' : ' - '));
                                        try {
                                            miniRuleCatTabDiv += dateinfo.date.substring(0, 2) + ' ' + dateinfo.date.substring(2, 5) + ', 20' + dateinfo.date.substring(5, 7);
                                        } catch (ex) {}
                                        try {
                                            miniRuleCatTabDiv += ' ' + dateinfo.time.substring(0, 2) + ':' + dateinfo.time.substring(2, 4);
                                        } catch (ex) {}
                                        miniRuleCatTabDiv += '</p>';
                                    });
                                }

                                miniRuleCatTabDiv += '</div>';
                                miniRuleCatTabDiv += '</div>';

                                if (categoryIndex + 1 == totCat) {
                                    miniRuleCatDiv += '</div>';
                                }
                            }
                        });
                        miniRuleCatDiv += (miniRuleCatTabDiv + '</div>');
                    } catch (ex) {
                        var sampid = 'catgry_' + segIndex + '_nodata_' + divindex + '';
                        miniRuleCatDiv += '<div class="tabmini">';
                        miniRuleCatDiv += '<button class="tablinks" onclick="openSubTab(event, \'' + sampid + '\')" id="defaultOpen">Category</button>';
                        miniRuleCatDiv += '</div>';
                        miniRuleCatDiv += '<div id="' + sampid + '" class="tabcontent"><div class="content_tab_sec"><p><i class="fa fa-exclamation-circle"></i> No information available</p></div></div>';
                        miniRuleCatDiv += (miniRuleCatTabDiv + '</div>');
                        // console.log(ex);
                    }
                }
            });

            miniRuleDiv = miniRuleHeadDiv + miniRuleCatDiv;
            if (miniRuleDiv != '') {
                $('#divflightminirules_' + divindex).html(miniRuleDiv);
            } else {
                $('#divflightminirules_' + divindex).html('<p>Mini rules not available for this trip !</p>');
            }
            setTimeout(function () {
                try {
                    $('#mini_rule_' + divindex + ' ul li')[0].click()
                    //  document.getElementsByClassName("mini_rule")[0].getElementsByTagName('ul')[0].getElementsByTagName('li')[0]
                } catch (ex) {}
            }, 100)
        },
        hasMiniRule: function (airMiniRules) {
            var hasMiniRule = false;
            if (airMiniRules != undefined && airMiniRules != null && airMiniRules != "" && airMiniRules != "NA" && airMiniRules != "NIL") {
                if (airMiniRules.departure != "" && airMiniRules.departure != "NA" && airMiniRules.departure != null && airMiniRules.departure != undefined && airMiniRules.departure != "NIL") {
                    hasMiniRule = true;
                }
            }
            return hasMiniRule;
        },
        updateTripType: function (triptype) {
            this.tripType = triptype;
            if (triptype.toUpperCase() == 'O') {
                $("#retDate").val("");
            }
        },
        checkAvailabilty: function (flight, index) {
            // groupOfFlights = flight.groupOfFlights.map(function(e) {
            //     return 
            // })
            if (_.isEmpty(flight.availability)) {
                var rq = {
                    request: {
                        service: "FlightRQ",
                        supplierCodes: [flight.segmentRef.supplier],
                        content: {
                            command: "FlightAvailabilityRQ",
                            availability: {
                                groupOfFlights: flight.groupOfFlights
                            },
                            supplierSpecific: {}
                        },
                        selectCredential: flight.selectCredential
                    }
                }

                var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;
                var serviceUrl = this.commonStore.hubUrls.hubConnection.hubServices.flights.airAvailability;
                var vm = this;
                var config = {
                    axiosConfig: {
                        method: "post",
                        url: hubUrl + serviceUrl,
                        data: rq
                    },
                    successCallback: function (response) {
                        if (response && !_.isEmpty(response.data.response.content.availabilityReply)) {
                            flight.availability = response.data.response.content.availabilityReply;
                            flight.availabilityTmp = __.cloneDeep(response.data.response.content.availabilityReply);
                            flight.sendAvailability = false;

                            var groupOfFlights = flight.availability.groupOfFlights;
                            for (var j = 0; j < groupOfFlights.length; j++) {
                                var flightDetails = groupOfFlights[j].flightDetails;
                                for (var i = 0; i < flightDetails.length; i++) {
                                    var classList = flightDetails[i].fInfo.bookingClassList;

                                    var ind = classList.findIndex(function (e) {
                                        return e.rbd == flight.groupOfFlights[j].flightDetails[i].fInfo.rbd;
                                    });
                                    if (classList[ind] != undefined) {
                                        classList[ind].selected = true;
                                    }
                                }
                            }

                            vm.$forceUpdate();
                        } else {
                            flight.noAvailabilityResponse = true;
                            // if (response.data.response.content.error) {
                            //     alertify.alert("Error", response.data.response.content.error.message);
                            // } else {
                            //     alertify.alert("Error", "No availability found.");
                            // }
                        }
                    },
                    errorCallback: function (error) {
                        flight.noAvailabilityResponse = true;
                        alertify.alert("Error", error.message);
                    },
                    showAlert: true
                }

                mainAxiosRequest(config);
            }

        },
        popFareRule: function (flight, index) {
            var isupdated = $('#divflighfarerules_' + index).data("isupdated");
            if (isupdated == 'false' || isupdated == false) {
                $('#divflighfarerules_' + index).html('<div class="resultprogress_load animated-background" style="height:47px"></div>');
                this.selectFlightForFareRule(flight, index, 0);
            }
            $('#divflighfarerules_' + index).data('isupdated', 'true');
            try {
                document.getElementById('tabmini_' + index).getElementsByTagName('button')[0].click();
            } catch (ex) {}
        },
        popUpsellDetail: function (index) {
            try {
                document.getElementById('tabmini_' + index).getElementsByTagName('button')[0].click();
            } catch (ex) {}
        },
        selectFlightForFareRule: function (flight, IDNo, cat) {
            var data = this.createSelRqMinRq('select', IDNo, "", {});
            var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = this.commonStore.hubUrls.hubConnection.hubServices.flights.airSelect;
            var vm = this;
            var config = {
                axiosConfig: {
                    method: "post",
                    url: hubUrl + serviceUrl,
                    data: data
                },
                successCallback: function (response) {
                    if (response) {
                        try {
                            vm.bindFareRules(response.data.response.content.fareInformationWithoutPnrReply.airFareRule, IDNo);
                        } catch (err) {
                            $('#divflighfarerules_' + IDNo).html('<p>Fare rules not available for this trip !</p>');
                        }
                        try {
                            var summary = response.data.response.content.fareInformationWithoutPnrReply.minirules;
                            if (summary) {
                                var SummaryLists = [];
                                summary.forEach(rule => {
                                    SummaryList = _.groupBy(rule.category, "cat");
                                    SummaryLists.push(SummaryList);
                                });
                                Vue.set(vm.flightResBind[IDNo], 'summary', SummaryLists);
                            } else {
                                vm.isSummaryAvailable = false;
                            }
                        } catch (err) {
                            vm.isSummaryAvailable = false;
                        }
                    }
                },
                errorCallback: function (error) {
                    $('#divflighfarerules_' + IDNo).html('<p>Fare rules not available for this trip !</p>');
                },
                showAlert: false
            }

            mainAxiosRequest(config);
        },
        bindFareRules: function (airFareRules, IDNo) {
            var tabHead = '';
            var tabBody = '';
            try {
                tabHead = '<ul class="nav nav-tabs" id="myTab_' + IDNo + '" role="tablist">';
                tabBody = '<div class="tab-content tab_Body" id="myTabContent_' + IDNo + '">';
                $.each(airFareRules, function (fareRuleindex, fareRule) {
                    var tabHeadId = 'myTabHead_' + IDNo + '_' + fareRuleindex;
                    var tabBodyId = 'myTabBody_' + IDNo + '_' + fareRuleindex;
                    tabHead +=
                        '<li class="nav-item ' + (fareRuleindex == 0 ? 'active' : '') + '">' +
                        '<a class="nav-link ' + (fareRuleindex == 0 ? 'active' : '') + '" id="' + tabHeadId + '" data-toggle="tab" href="#' + tabBodyId + '" role="tab" aria-controls="' + tabBodyId + '" aria-selected="true">' + fareRule.departure + ' to ' + fareRule.arrival + '</a>' +
                        '</li>';
                    tabBody += '<div class="tab-pane fade ' + (fareRuleindex == 0 ? 'active in' : '') + '" id="' + tabBodyId + '" role="tabpanel" aria-labelledby="' + tabHeadId + '">';
                    $.each(fareRule.fareRuleDetails, function (fareRuleDetindex, fareRuleDet) {
                        tabBody +=
                            '<a href="#myItemHead_' + IDNo + '_' + fareRuleindex + '_' + fareRuleDetindex + '" class="tab_Body_Head" data-toggle="collapse" >' + fareRuleDet.ruleHead + '<i class="chevron fa fa-fw" ></i></a></br>' +
                            '<p id="myItemHead_' + IDNo + '_' + fareRuleindex + '_' + fareRuleDetindex + '"class="tab_Body_Body collapse">' + fareRuleDet.ruleBody + '</p>' +
                            '<hl/>';
                    });
                    tabBody += '</div>';
                });
                tabHead += '</ul>';
                tabBody += '</div>';
            } catch (err) {
                tabHead = '';
                tabBody = '';
            }

            if (!stringIsNullorEmpty(tabHead) && !stringIsNullorEmpty(tabBody)) {
                $('#divflighfarerules_' + IDNo).html(tabHead + tabBody);
            } else {
                $('#divflighfarerules_' + IDNo).html('<p>Fare rules not available for this trip !</p>');
            }
        },
        bindSummary: function (item) {
            var self = this;
            var rules;
            var before = item.find(x => x.situation.toLowerCase() == 'before departure');
            if (before && before.allowed) {
                if (before.amount > 0) {
                    // rules = '<p>' + before.cat + '</p><p class="allowed">Allowed @ ' + self.$n(parseFloat(before.amount / self.commonStore.currencyMultiplier), 'currency', self.commonStore.selectedCurrency) + '</p>'
                    rules = '<p>' + before.cat + '</p><p class="allowed">Allowed @ ' + before.currency + '&nbsp;' + before.amount + '</p>'

                } else {
                    rules = '<p>' + before.cat + '</p><p class="allowed">Allowed</p>';
                }

            } else {
                rules = '<p>' + before.cat + '</p><p class="not-allowed">Not Allowed</p>';
            }

            var after = item.find(x => x.situation.toLowerCase() == 'after departure');
            if (after && after.allowed) {
                if (after.amount > 0) {
                    rules = rules + '<p class="allowed">Allowed @ ' + after.currency + '&nbsp;' + after.amount + '</p>'
                    // rules = rules + '<p class="allowed">Allowed @ ' + self.$n(parseFloat(after.amount / self.commonStore.currencyMultiplier), 'currency', self.commonStore.selectedCurrency) + '</p>'

                } else {
                    rules = rules + '<p class="allowed">Allowed</p>';
                }

            } else {
                rules = rules + '<p class="not-allowed">Not Allowed</p>';
            }
            return rules;

        },
        onClickUpsell: function (upselref) {
            try {
                var link = $('[data-upsellkey="' + upselref + '"]')[0];
                var fltDiv = $('#' + $(link).data('id'))[0];
                //$($(link).parent().parent().parent())[0].scrollIntoView();
                if (fltDiv != undefined) {
                    fltDiv.scrollIntoView();
                    link.click();
                } else {
                    this.allResultBind(upselref);

                }

            } catch (err) {
                console.log(err);
            }
        },
        //To find all flight results to UI=>for Flight upsell
        allResultBind: function (upsellref) {
            this.$nextTick(function () {
                try {
                    if (this.filteredFlightRes.length > this.flightResBind.length) {
                        for (var index = this.flightResBind.length; index < this.filteredFlightRes.length; index++) {
                            this.flightResBind.push(this.filteredFlightRes[index]);
                        }
                    };
                    if (this.filteredFlightRes.length == this.flightResBind.length) {
                        this.onClickUpsell(upsellref);
                    }
                } catch (err) {

                }


            });



        },
        addQuotationToCurrentCart: function (flRes, index) {
            var vm = this;
            // console.log(flRes)
            var uuid = flRes.segmentRef.key + "|" + flRes.segmentRef.segRef + "|" + flRes.segmentRef.supplier + "|" + index + "|flights";
            var flightQuotation = this.commonStore.selectedCart.services.flights;

            if (flightQuotation.findIndex(function (x) {
                    return x.serialNumber == uuid;
                }) == -1) {
                vm.commonStore.selectedCart.services.flights.push({
                    serialNumber: uuid,
                    tripType: this.tripType,
                    adt: this.adtCount,
                    chd: this.chdCount,
                    inf: this.infCount,
                    bagDetails: flRes.bagDetails,
                    description: flRes.description,
                    fare: flRes.fare,
                    fareDetails: flRes.fareDetails,
                    groupOfFlights: flRes.groupOfFlights,
                    segmentRef: flRes.segmentRef
                });

                for (var k = 0; k < vm.commonStore.carts.length; k++) {
                    if (vm.commonStore.carts[k].id == vm.commonStore.selectedCart.id) {
                        vm.commonStore.carts[k].services.flights = vm.commonStore.selectedCart.services.flights;
                        break;
                    }
                }
                var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;
                var serviceUrl = this.commonStore.hubUrls.hubConnection.hubServices.addQuote;

                var config = {
                    axiosConfig: {
                        method: "post",
                        url: hubUrl + serviceUrl,
                        data: {
                            content: JSON.stringify(vm.commonStore.carts)
                        }
                    },
                    successCallback: function (response) {
                        if (response) {
                            vm.getAllquotationsOnServer();
                            // alertify.alert("Success", "Added to quotaion " + vm.commonStore.selectedCart.name);
                        }
                    },
                    errorCallback: function (error) {
                        alertify.alert("Error", "Error unable to add quotation.");
                    },
                    showAlert: true
                };

                mainAxiosRequest(config);
            } else {
                alertify.alert("Please note", "Quotation already added.");
            }


        },
        getAllquotationsOnServer: function () {
            var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = this.commonStore.hubUrls.hubConnection.hubServices.pendingQuoteDetails;
            var vm = this;

            var config = {
                axiosConfig: {
                    method: "get",
                    url: hubUrl + serviceUrl
                },
                successCallback: function (response) {
                    if (response && response.data.data[0].content) {
                        vm.commonStore.carts = JSON.parse(response.data.data[0].content);
                    } else {
                        vm.commonStore.carts = [{
                            id: uuidv4(),
                            name: "Default",
                            services: {
                                flights: [],
                                hotels: [],
                                insuranceTP: [],
                                insuranceWIS: []
                            }
                        }];
                    }
                },
                errorCallback: function (error) {
                    vm.commonStore.carts = [{
                        id: uuidv4(),
                        name: "Default",
                        services: {
                            flights: [],
                            hotels: [],
                            insuranceTP: [],
                            insuranceWIS: []
                        }
                    }];
                },
                showAlert: false
            };

            mainAxiosRequest(config);
        },
        supplierCount: function (supplier) {
            var vm = this;
            var tmp = this.allFlightRes;
            try {
                if (this.tripType.toLowerCase() == 'r') {
                    tmp = _.filter(this.allFlightRes, function (e) {
                        return e.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate == vm.selectedCalendarDepartureDate &&
                            e.groupOfFlights[1].flightDetails[0].fInfo.dateTime.depDate == vm.selectedCalendarReturnDate;
                    });
                } else {
                    tmp = _.filter(this.allFlightRes, function (e) {
                        return e.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate == vm.selectedCalendarDepartureDate;
                    });
                }
            } catch (error) {}
            return tmp.filter(function (flight) {
                return parseInt(flight.segmentRef.supplier) == parseInt(supplier.id);
            }).length;
        },
        findCategoryClassLength: function (category) {
            var length = 0;
            try {
                category.forEach(element => {
                    if (Object.keys(element.classDetails).length > length) {
                        length = Object.keys(element.classDetails).length;
                    }
                });
                return Number(length)
            } catch (error) {
                return Number(length)
            }
        }
    },

    mounted: function () {
        //Initial Scripts
        var vm = this;
        this.$nextTick(function () {
            $(".simplePopupBackground").click(function () {
                $(".more_info_searchresultpopup").hide();
                $(".simplePopupBackground").hide();
                $(".search_details").removeClass("zindex");
            });
            vm.scroll();

            try {
                vm.setSearchHistory();
            } catch (err) {
                $.removeCookie('AirSearchHistory');
            }

            // ProgressBar
            this.percentWidth = 0.02;
            var loader = setInterval(() => {
                this.loadingProgress = this.loadingProgress + this.percentWidth * 100;
                if (this.loadingProgress >= 100) {
                    clearInterval(loader);
                }
            }, 3600);
        });

        this.$watch(function (vmWatch) {
                return vmWatch.lowestPrice, vmWatch.highestPrice, vmWatch.checkStops, vmWatch.checkAirlines, vmWatch.selDeptLoc, vmWatch.selArrtLoc, vmWatch.selFlSerList, Date.now();
            },
            function () {
                var vm = this;
                var temp = [];
                try {
                    if (this.allFlightRes.length > 0) {
                        if (this.tripType.toLowerCase() == 'r') {
                            temp = _.filter(this.allFlightRes, function (e) {
                                return e.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate == vm.selectedCalendarDepartureDate &&
                                    e.groupOfFlights[1].flightDetails[0].fInfo.dateTime.depDate == vm.selectedCalendarReturnDate;
                            });
                        } else {
                            temp = _.filter(this.allFlightRes, function (e) {
                                return e.groupOfFlights[0].flightDetails[0].fInfo.dateTime.depDate == vm.selectedCalendarDepartureDate;
                            });
                        }
                        try {
                            temp = temp.filter(function (res) {
                                return parseFloat(res.fare.amount) + 1 >= parseFloat(i18n.n(vm.lowestPrice * vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency).match(/\d+(\.\d+)?/g).join('')) &&
                                    parseFloat(res.fare.amount) <= parseFloat(i18n.n(vm.highestPrice * vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency).match(/\d+(\.\d+)?/g).join('')) + 1
                            });
                        } catch (error) {}


                        if (this.checkStops.length > 0) {
                            temp = temp.filter(function (res) {
                                var takeLeg = true;
                                res.groupOfFlights.forEach(leg => {
                                    if (vm.checkStops.length == 1 && vm.checkStops[0] != 99) {
                                        leg.flightDetails.forEach(seg => {
                                            if (vm.checkStops.indexOf(leg.flightDetails.length - 1) == -1 || seg.fInfo.stopQuantity > 0) {
                                                takeLeg = false;
                                            }
                                        });
                                    }else if (vm.checkStops.length == 1 && vm.checkStops[0] == 99) {
                                        leg.flightDetails.forEach(seg => {
                                            if (seg.fInfo.stopQuantity == 0) {
                                                takeLeg = false;
                                            }
                                        });
                                    } else if (vm.checkStops.length > 1) {
                                        leg.flightDetails.forEach(seg => {
                                            if (vm.checkStops.indexOf(leg.flightDetails.length - 1) == -1 || 
                                            (vm.checkStops.indexOf(99) == -1 && seg.fInfo.stopQuantity)
                                            ) {
                                                takeLeg = false;
                                            }
                                        });
                                    }
                                });
                                return takeLeg;
                            })
                        }

                        if (this.checkAirlines.length > 0) {
                            if (this.checkAirlines[0].air == "Multi-Airline") {
                                this.filterType = 2;
                            } else {
                                this.filterType = 1;
                            }
                            var selAirlines = this.checkAirlines;
                            temp = temp.filter(function (res) {
                                var takeLeg = false;
                                res.groupOfFlights.forEach(leg => {
                                    if (vm.filterType == 1) {
                                        selAirlines.forEach(air => {
                                            if (leg.flightDetails.every((val, i, arr) => val.fInfo.companyId.mCarrier === arr[0].fInfo.companyId.mCarrier)) {
                                                if (air.air === leg.flightDetails[0].fInfo.companyId.mCarrier) {
                                                    takeLeg = true;
                                                }
                                            }
                                        });
                                    } else {
                                        if (!leg.flightDetails.every((val, i, arr) => val.fInfo.companyId.mCarrier === arr[0].fInfo.companyId.mCarrier)) {
                                            takeLeg = true;
                                        }
                                    }
                                });
                                return takeLeg;
                            })
                        }

                        if (this.selDeptLoc.length > 0) {
                            var selDeptLoc = this.selDeptLoc;
                            temp = temp.filter(function (res) {
                                var takeLeg = false;
                                res.groupOfFlights.forEach(leg => {
                                    selDeptLoc.forEach(deploc => {
                                        var depLocSplit = deploc.split('-');
                                        if (depLocSplit[0] === leg.flightDetails[0].fInfo.location.locationFrom) {
                                            var dTime = parseInt(leg.flightDetails[0].fInfo.dateTime.depTime.substring(0, 2))
                                            if (dTime >= parseInt(depLocSplit[1]) && dTime <= parseInt(depLocSplit[2])) {
                                                takeLeg = true;
                                            }
                                        }
                                    });
                                });
                                return takeLeg;
                            })
                        }

                        if (this.selArrtLoc.length > 0) {
                            var selArrtLoc = this.selArrtLoc;
                            temp = temp.filter(function (res) {
                                var takeLeg = false;
                                res.groupOfFlights.forEach(leg => {
                                    selArrtLoc.forEach(arrloc => {
                                        var arrLocSplit = arrloc.split('-');
                                        if (arrLocSplit[0] === leg.flightDetails[leg.flightDetails.length - 1].fInfo.location.locationTo) {
                                            var arrTime = parseInt(leg.flightDetails[leg.flightDetails.length - 1].fInfo.dateTime.arrTime.substring(0, 2))
                                            if (arrTime >= parseInt(arrLocSplit[1]) && arrTime <= parseInt(arrLocSplit[2])) {
                                                takeLeg = true;
                                            }
                                        }
                                    });
                                });
                                return takeLeg;
                            })
                        }

                        if (this.selFlSerList.length > 0) {
                            temp = temp.filter(function (res) {
                                return vm.selFlSerList.indexOf(parseInt(res.segmentRef.supplier)) > -1
                            })
                        }

                    }
                } catch (err) {
                    console.log(err)

                }
                vm.filteredFlightRes = temp;
                vm.flightResBind = vm.filteredFlightRes.slice(0, vm.currentScrollLimit);
            }
        )
    },
    filters: {
        momCommon: function (date, frmFormat, toFormat) {
            return moment(date, frmFormat).format(toFormat);
        }
    },
    watch: {
        "commonStore.currencyMultiplier": function () {
            var vm = this;
            vm.forCurrencyUpdatesOnly = true;

            var slider = document.getElementById("property-price-range");

            // var filterAppliedRes = vm.allFlightRes.filter(function (res) {
            //   return res.fare.amount >= vm.lowestPrice && res.fare.amount <= vm.highestPrice
            // });

            var lowestPrice = Math.min.apply(Math, vm.allFlightRes.map(function (o) {
                return o.fare.amount;
            }))
            var highestPrice = Math.max.apply(Math, vm.allFlightRes.map(function (o) {
                return o.fare.amount;
            }))

            if (highestPrice == lowestPrice) {
                highestPrice = parseFloat(highestPrice) + 1;
            }

            try {
                lowestPrice = (i18n.n(lowestPrice / this.commonStore.currencyMultiplier, 'currency', this.commonStore.selectedCurrency).match(/\d+(\.\d+)?/g) || [0]).join('');
                highestPrice = (i18n.n(highestPrice / this.commonStore.currencyMultiplier, 'currency', this.commonStore.selectedCurrency).match(/\d+(\.\d+)?/g) || [highestPrice]).join('');
            } catch (error) {}

            vm.filteredFlightRes = vm.allFlightRes;
            vm.flightResBind = vm.filteredFlightRes;

            document.getElementById("property-price-lower").innerHTML = lowestPrice;
            document.getElementById("property-price-upper").innerHTML = highestPrice;

            slider.noUiSlider.updateOptions({
                tooltips: [true, true],
                start: [parseFloat(lowestPrice), parseFloat(highestPrice)],
                range: {
                    'min': parseFloat(lowestPrice),
                    'max': parseFloat(highestPrice)
                }
            });

            $(".more_info_searchresultpopup").hide();
            $(".simplePopupBackground").hide();
            $(".search_details").removeClass("zindex");
            this.airFilterBanner();
        },
    },
    computed: {}
});

$(document).ready(function () {
    $(function () {
        var Accordion = function (el, multiple) {
            this.el = el || {};
            this.multiple = multiple || false;

            // Variables privadas
            var links = this.el.find('.link');
            // Evento
            links.on('click', {
                el: this.el,
                multiple: this.multiple
            }, this.dropdown)
        }

        Accordion.prototype.dropdown = function (e) {
            var $el = e.data.el;
            $this = $(this),
                $next = $this.next();

            $next.slideToggle();
            $this.parent().toggleClass('open');

            if (!e.data.multiple) {
                $el.find($(this) + '.submenu').not($next).slideUp().parent().removeClass('open');
            };
        }

        var accordion = new Accordion($(this), false);
    });

    // Animated Fade
    jQuery('body').on('click', '.tabs.animated-fade .tab-links a', function (e) {
        var currentAttrValue = jQuery(this).attr('href');

        // Show/Hide Tabs
        jQuery('.tabs ' + currentAttrValue).fadeIn(400).siblings().hide();

        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

        e.preventDefault();
    });
});

//Modify Search

Vue.component('flight-autocomplete', {
    data() {
        return {
            KeywordSearch: this.returnValue,
            resultItemsarr: [],
            autoCompleteProgress: false,
            loading: false,
            highlightIndex: 0
        }
    },
    props: {
        itemText: String,
        itemId: String,
        status: Boolean,
        placeHolderText: String,
        returnValue: String,
        tripType: String,
        id: {
            type: String,
            default: '',
            required: false
        },

    },
    template: `<div class="autocomplete flightauto"  v-click-outside="hideautocompelte">
      <input type="text" :placeholder="placeHolderText" :id="id" :data-aircode="itemId" autocomplete="off"  
      :value="KeywordSearch" class="frm_from" :class="{ 'loading-circle' : (KeywordSearch && KeywordSearch.length > 2), 'hide-loading-circle': resultItemsarr.length > 0 || resultItemsarr.length == 0 && !loading  }" 
      @input="function(event){onSelectedAutoCompleteEvent(event.target.value,event)}"   
      @keydown.down="down"
      @keydown.up="up"
      @keydown.tab="autoCompleteProgress=false"
      @keydown.esc="autoCompleteProgress=false"
      @keydown.enter="onSelected(resultItemsarr[highlightIndex].code,resultItemsarr[highlightIndex].label)"
      @click="autocomplete"/>
      <ul ref="searchautocomplete" class="autocomplete-results" v-if="autoCompleteProgress&&resultItemsarr.length > 0">
          <li ref="options" :class="{'autocomplete-result-active' : i == highlightIndex}" class="autocomplete-result" v-for="(item,i) in resultItemsarr" :key="i" @click="onSelected(item.code, item.label)">
              {{ item.label }}
          </li>
      </ul>
  </div>`,
    methods: {
        up: function () {
            if (this.autoCompleteProgress) {
                if (this.highlightIndex > 0) {
                    this.highlightIndex--
                }
            } else {
                this.autoCompleteProgress = true;
            }
            this.fixScrolling();
        },

        down: function () {
            if (this.autoCompleteProgress) {
                if (this.highlightIndex < this.resultItemsarr.length - 1) {
                    this.highlightIndex++
                } else if (this.highlightIndex == this.resultItemsarr.length - 1) {
                    this.highlightIndex = 0;
                }
            } else {
                this.autoCompleteProgress = true;
            }
            this.fixScrolling();
        },
        fixScrolling: function () {
            if (this.$refs.options[this.highlightIndex]) {
                var liH = this.$refs.options[this.highlightIndex].clientHeight;
                if (liH == 50) {
                    liH = 32;
                }
                if (liH * this.highlightIndex >= 32 || this.highlightIndex == 0) {
                    this.$refs.searchautocomplete.scrollTop = liH * this.highlightIndex;
                }
            }

        },
        onSelectedAutoCompleteEvent: _.debounce(function (keywordEntered) {
            var self = this;
            if (keywordEntered.length > 2) {
                this.autoCompleteProgress = true;
                this.loading = true;
                var cityName = keywordEntered;
                var cityarray = cityName.split(' ');
                var uppercaseFirstLetter = "";
                for (var k = 0; k < cityarray.length; k++) {
                    uppercaseFirstLetter += cityarray[k].charAt(0).toUpperCase() + cityarray[k].slice(1).toLowerCase()
                    if (k < cityarray.length - 1) {
                        uppercaseFirstLetter += " ";
                    }
                }
                uppercaseFirstLetter = "*" + uppercaseFirstLetter + "*";
                var lowercaseLetter = "*" + cityName.toLowerCase() + "*";
                var uppercaseLetter = "*" + cityName.toUpperCase() + "*";
                var query = {
                    query: {
                        bool: {
                            should: [{
                                bool: {
                                    should: [{
                                        wildcard: {
                                            iata_code: uppercaseFirstLetter
                                        }
                                    }, {
                                        wildcard: {
                                            iata_code: uppercaseLetter
                                        }
                                    }, {
                                        wildcard: {
                                            iata_code: lowercaseLetter
                                        }
                                    }]
                                }
                            }, {
                                bool: {
                                    should: [{
                                        wildcard: {
                                            municipality: uppercaseFirstLetter
                                        }
                                    }, {
                                        wildcard: {
                                            municipality: uppercaseLetter
                                        }
                                    }, {
                                        wildcard: {
                                            municipality: lowercaseLetter
                                        }
                                    }]
                                }
                            }, {
                                bool: {
                                    should: [{
                                        wildcard: {
                                            name: uppercaseFirstLetter
                                        }
                                    }, {
                                        wildcard: {
                                            name: uppercaseLetter
                                        }
                                    }, {
                                        wildcard: {
                                            name: lowercaseLetter
                                        }
                                    }]
                                }
                            }],
                            must: [{
                                "exists": {
                                    "field": "iata_code"
                                }
                            }]
                        }
                    }
                };
                var config = {
                    axiosConfig: {
                        headers: {
                            "Content-Type": "application/json"
                        },
                        method: "post",
                        url: HubServiceUrls.elasticSearch.url,
                        data: query
                    },
                    successCallback: function (resp) {
                        finalResult = [];
                        var hits = resp.data.hits.hits;
                        var Citymap = new Map();
                        for (var i = 0; i < hits.length; i++) {
                            Citymap.set(hits[i]._source.iata_code, hits[i]._source);
                        }
                        var get_values = Citymap.values();
                        var Cityvalues = [];
                        for (var ele of get_values) {
                            Cityvalues.push(ele);
                        }
                        var results = SortInputFirstFlight(cityName, Cityvalues);
                        for (var i = 0; i < results.length; i++) {
                            finalResult.push({
                                "code": results[i].iata_code,
                                "label": results[i].name + ", " + results[i].iso_country + '(' + results[i].iata_code + ')',
                                "municipality": results[i].municipality
                            });
                        }
                        var newData = [];
                        finalResult.forEach(function (item, index) {
                            if (item.label.toLowerCase().indexOf(keywordEntered.toLowerCase()) >= 0 || item.code.toLowerCase().includes(keywordEntered.toLowerCase()) || item.municipality.toLowerCase().indexOf(keywordEntered.toLowerCase()) >= 0) {
                                newData.push(item);
                            }
                        });
                        self.resultItemsarr = newData;
                        self.autoCompleteProgress = true;
                        self.loading = false;
                    },
                    errorCallback: function (error) { },
                    showAlert: false
                };

                mainAxiosRequest(config);
                // var client = new elasticsearch.Client({
                //     host: [{
                //         host: HubServiceUrls.elasticSearch.elasticsearchHost,
                //         auth: HubServiceUrls.elasticSearch.auth,
                //         protocol: HubServiceUrls.elasticSearch.protocol,
                //         port: HubServiceUrls.elasticSearch.port,
                //         requestTimeout: 60000
                //     }],
                //     log: 'trace'
                // });
                // client.search({
                //     index: 'airport_info',
                //     size: 150,
                //     timeout: "3000ms",
                //     body: query
                // }).then(function (resp) {
                    
                // })
            } else {
                this.autoCompleteProgress = false;
                this.resultItemsarr = [];
                this.loading = false;
            }
            this.KeywordSearch = keywordEntered;
        }, 500),
        onSelected: function (code, label) {
            this.autoCompleteProgress = true;
            this.resultItemsarr = [];
            this.KeywordSearch = label;
            if (this.tripType.toLowerCase() != 'm') {
                if (this.id == 'txtFrom') {
                    $("#txtTo").focus();
                } else if (this.id == 'txtTo') {
                    $("#deptDate01").focus();
                }

            } else {
                if (this.id == 'txtFrom') {
                    $("#txtTo").focus();
                } else if (this.id == 'txtTo') {
                    $("#deptDate01").focus();
                } else if (this.id == 'txtLeg2From') {
                    $("#txtLeg2To").focus();
                } else if (this.id == 'txtLeg2To') {
                    $("#txtLeg2Date").focus();
                } else if (this.id == 'txtLeg3From') {
                    $("#txtLeg3To").focus();
                } else if (this.id == 'txtLeg3To') {
                    $("#txtLeg3Date").focus();
                } else if (this.id == 'txtLeg4From') {
                    $("#txtLeg4To").focus();
                } else if (this.id == 'txtLeg4To') {
                    $("#txtLeg4Date").focus();
                } else if (this.id == 'txtLeg5From') {
                    $("#txtLeg5To").focus();
                } else if (this.id == 'txtLeg5To') {
                    $("#txtLeg5Date").focus();
                } else if (this.id == 'txtLeg6From') {
                    $("#txtLeg6To").focus();
                } else if (this.id == 'txtLeg6To') {
                    $("#txtLeg6Date").focus();
                }

            }
            this.$emit('air-search-completed', code, label, this.itemId, this.itemText, this.id);

        },
        hideautocompelte: function () {
            this.autoCompleteProgress = false;
        },
        autocomplete: function () {
            this.autoCompleteProgress = true;
        }

    },
    watch: {
        status: function () {
            this.KeywordSearch = this.returnValue;
        },
        returnValue: function () {
            this.KeywordSearch = this.returnValue;
        }
    }

});

Vue.component("flight-search", {
    data: function () {
        return {
            commonStore: vueCommonStore.state,
            commonAgencyCode: 'AGY75',
            showSupplierList: false,
            supplierList: [],
            selectedSuppliers: [],
            supplierSearchLabel: "Select All",
            totalAllowdPax: 9,
            status: true,
            Totaltravaller: "1 Passenger, Economy",
            flightSearchCities: {
                cityFrom0: '',
                cityTo0: '',
                cityFrom1: '',
                cityTo1: '',
                cityFrom2: '',
                cityTo2: '',
                cityFrom3: '',
                cityTo3: '',
                cityFrom4: '',
                cityTo4: '',
                cityFrom5: '',
                cityTo5: ''
            },
            flightSearchCityName: {
                cityFrom0: '',
                cityTo0: '',
                cityFrom1: '',
                cityTo1: '',
                cityFrom2: '',
                cityTo2: '',
                cityFrom3: '',
                cityTo3: '',
                cityFrom4: '',
                cityTo4: '',
                cityFrom5: '',
                cityTo5: ''
            },
            legcount: 1,
            placeholders: {
                From: 'Enter Departure Airport or City',
                To: 'Enter Destination Airport or City',
                MCityFrom: 'From',
                MCityTo: 'TO',
            },
            preferAirline: "",
            airlineList: AirlinesDatas,
            airlineserch: "",
            Airlineresults: [],
            airlineautoCompleteProgress: false,
            airlineloading: false,
            FlightItemtext: {
                from: 'from',
                to: 'to'
            },
            FlightItemtID: {
                cityFrom0: 'cityFrom0',
                cityTo0: 'cityTo0',
                cityFrom1: 'cityFrom1',
                cityTo1: 'cityTo1',
                cityFrom2: 'cityFrom2',
                cityTo2: 'cityTo2',
                cityFrom3: 'cityFrom3',
                cityTo3: 'cityTo3',
                cityFrom4: 'cityFrom4',
                cityTo4: 'cityTo4',
                cityFrom5: 'cityFrom5',
                cityTo5: 'cityTo5'
            },

            //Labels
            Modify_Search_Label: '',
            OneWay_Label: '',
            RoundTrip_Label: '',
            MultiCity_Label: '',
            From_Label: '',
            To_Label: '',
            Trip_Label: '',
            Travel_Date_Label: '',
            Departure_Date_Label: '',
            Add_up_to_Label1: '',
            Add_up_to_Label2: '',
            Return_Date_Label: '',
            Adult_Label: '',
            Adult_option_Label: '',
            Adults_option_Label: '',
            Child_Label: '',
            Child_Option_Label: '',
            Children_Option_Label: '',
            Infant_Label: '',
            Infant_Option_Label: '',
            Infants_Option_Label: '',
            Class_Label: '',
            Economy_Label: '',
            Business_Label: '',
            First: '',
            Suppliers_Label: '',
            Airline_Label: '',
            Direct_flights_Label: '',
            Search_Flights_Label: '',

            //Alert Comp Labels
            Alert_Validation_Label: '',
            Departure_and_arrival__Alert_Msg: '',
            Please_fill_origin_Alert_Msg: '',
            Please_fill_destination_Alert_Msg: '',
            Please_fill_dept_date_Alert_Ms: '',
            Please_Arriv_Date_Alert_Msg: '',
            Please_fill_Trip_Alert_Msg: '',
            Fields_Alert_Msg: '',
            tripTypeData: 'o',
            oldRetDate: "",
            selected_adultsData: this.selected_adults,
            selected_childrenData: this.selected_children,
            selected_infantData: this.selected_infant,
            selected_cabinData: this.selected_cabin,
            direct_flightData: this.direct_flight,
            calendarSearch: 0,
            isSending: false
        };
    },
    props: {
        tripType: String,
        selected_adults: Number,
        selected_children: Number,
        selected_infant: Number,
        selected_cabin: String,
        direct_flight: Boolean,
        sel_supps: Array,
        calendar_search_prop: Number,
    },
    created: function () {
        this.tripTypeData = this.tripType.toLowerCase();
        this.getPagecontent();
        var agencyNode = window.localStorage.getItem("agencyNode");
        if (agencyNode) {
            agencyNode = JSON.parse(atob(agencyNode));
            var servicesList = agencyNode.loginNode.servicesList;
            for (var i = 0; i < servicesList.length; i++) {
                var provider = servicesList[i].provider;
                if (servicesList[i].name == "Air" && provider.length > 0) {
                    provider = provider.filter(supplier => supplier.supplierType == 'Test' || supplier.supplierType == 'Production').
                    sort(function (a, b) {
                        if (a.name.toLowerCase() < b.name.toLowerCase()) {
                            return -1;
                        }
                        if (a.name.toLowerCase() > b.name.toLowerCase()) {
                            return 1;
                        }
                        return 0;
                    });
                    for (var j = 0; j < provider.length; j++) {
                        this.supplierList.push({
                            id: provider[j].id,
                            name: provider[j].name.toUpperCase(),
                            supplierType: provider[j].supplierType
                        });
                        // this.selectedSuppliers = this.supplierList.map(function (supplier) { return supplier.id });
                        this.supplierSearchLabel = this.supplierList.map(function (supplier) {
                            return supplier.name;
                        }).join(",");
                    }
                    try {
                        this.selectedSuppliers = this.sel_supps.split('|');
                    } catch (err) {
                        this.selectedSuppliers = this.sel_supps;
                    }
                    break;
                } else if (servicesList[i].name == "Air" && provider.length == 0) {
                    setLoginPage("login");
                }
            }
        }
    },
    computed: {
        selectAll: {
            get: function () {
                return this.supplierList ? this.selectedSuppliers.length == this.supplierList.length : false;
            },
            set: function (value) {
                var selectedSuppliers = [];

                if (value) {
                    this.supplierList.forEach(function (supplier) {
                        selectedSuppliers.push(supplier.id);
                    });
                }

                this.selectedSuppliers = selectedSuppliers;
            }
        },
        showCalendarSearch() {
            // Amadeus for now
            var calsendarSearchSuppliers = [1];
            var hasCalendarSearch = true;

            for (var i = 0; i < this.selectedSuppliers.length; i++) {
                if (calsendarSearchSuppliers.indexOf(parseInt(this.selectedSuppliers[i])) >= 0) {
                    hasCalendarSearch = true;
                }
            }
            this.calendarSearch = this.calendar_search_prop;
            return this.selectedSuppliers.length > 0 && hasCalendarSearch;
        }

    },
    watch: {
        selectedSuppliers: function () {
            if (this.selectedSuppliers.length == 0) {
                this.supplierSearchLabel = 'No supplier selected';
            } else {
                var vm = this;
                this.supplierSearchLabel = this.selectedSuppliers.map(function (id) {
                    return vm.supplierList.filter(function (supplier) {
                        return id == supplier.id;
                    })[0].name;
                }).join(",");
            }
            this.autoCompleteResult = [];
            this.keywordSearch = "";
        },
        tripTypeData: function () {
            this.$emit('update-trip-type', this.tripTypeData);
        }
    },
    mounted: function () {
        var vm = this;
        vm.setCalender();
        vm.$nextTick(function () {

            var vr;
            var sl;
            sl = "0";
            $(".class_view").click(function () {
                sl = "1";
            });
            $(".class_type").click(function () {
                if (sl == "0") {
                    $(".class_view").fadeToggle();
                    vr = "1";
                } else {
                    vr = "1";
                    sl = "0";
                }

            });
            $(".wrapper").click(function () {
                if (sl == "0") {
                    if (vr == "0") {

                        $(".class_view").hide();
                    } else {
                        vr = "0";
                        sl = "0";
                    }
                } else {
                    vr = "0";
                    sl = "0";
                }
            });
            $(".multi_box").click(function (event) {
                event.stopPropagation();
                $(".multi_dropBox").slideToggle();
            });
            $(".multi_dropBox").on("click", function (event) {
                event.stopPropagation();
            });

            $(document).mouseup(function (e) {
                var container = $(".multi_dropBox");
                if (!container.is(e.target) && container.has(e.target).length === 0) {
                    container.hide();
                }
            });

        });
        this.setModifySearch();
    },
    methods: {
        showSupplierListDropdown: function () {
            if (this.supplierList.length != 1) {
                this.showSupplierList = !this.showSupplierList;
            }
            this.isCompleted = false;
        },
        flightSearchcomplete(AirportCode, AirportName, item, direction, id) {
            var self = this;
            var leg = item.replace(/^\D+/g, '');
            var from = 'cityFrom' + leg;
            var to = 'cityTo' + leg;
            if (direction == 'from') {
                if (this.flightSearchCities[to] != AirportCode) {
                    this.selectedSuppliers = this.supplierList.map(function(supplier) { return supplier.id });
                }
                if (this.flightSearchCities[to] == AirportCode) {
                    this.status = false;
                    this.flightSearchCityName[item] = '';
                    this.flightSearchCities[item] = '';
                    $("#" + id).focus();
                    alertify.alert(self.Alert_Validation_Label, self.Departure_and_arrival__Alert_Msg);

                } else {
                    this.flightSearchCities[item] = AirportCode;
                    this.flightSearchCityName[item] = AirportName;
                    this.status = true;
                }
            } else {
                if (this.flightSearchCities[from] != AirportCode) {
                    this.selectedSuppliers = this.supplierList.map(function(supplier) { return supplier.id });
                }
                if (this.flightSearchCities[from] == AirportCode) {
                    this.status = false;
                    this.flightSearchCityName[item] = '';
                    this.flightSearchCities[item] = '';
                    $("#" + id).focus();
                    alertify.alert(self.Alert_Validation_Label, self.Departure_and_arrival__Alert_Msg);
                } else {
                    this.flightSearchCities[item] = AirportCode;
                    this.flightSearchCityName[item] = AirportName;
                    this.status = true;
                }

            }


        },
        AddNewLeg() {
            if (this.legcount <= 5) {
                ++this.legcount;

            }
        },
        DeleteLeg(leg) {
            var legs = this.legcount;
            if (legs > 1) {
                --this.legcount;
                var from = 'cityFrom' + leg;
                var to = 'cityTo' + leg;
                this.flightSearchCities[from] = "";
                this.flightSearchCities[to] = "";

            }
        },
        setpaxcount(item, action) {
            if (item == 'adt') {
                if (action == 'plus') {
                    if (this.selected_adultsData < this.totalAllowdPax) {
                        ++this.selected_adultsData;
                    }
                } else {
                    if (this.selected_adultsData > 1) {
                        --this.selected_adultsData;
                        if (this.selected_adultsData < this.selected_infantData) {
                            this.selected_infantData = this.selected_adultsData;
                        }

                    }
                }
            } else if (item == 'chd') {
                if (action == 'plus') {
                    if (this.selected_adultsData + this.selected_childrenData < this.totalAllowdPax) {
                        ++this.selected_childrenData;
                    }
                } else {
                    if (this.selected_childrenData > 0) {
                        --this.selected_childrenData;
                    }
                }
            } else if (item == 'inf') {
                if (action == 'plus') {
                    if (this.selected_adultsData + this.selected_childrenData + this.selected_infantData < this.totalAllowdPax) {
                        if (this.selected_infantData < this.selected_adultsData) {
                            ++this.selected_infantData
                        }

                    }
                } else {
                    if (this.selected_infantData > 0) {
                        --this.selected_infantData
                    }
                }
            } else {}
            if (this.selected_adultsData + this.selected_childrenData > this.totalAllowdPax) {
                this.selected_childrenData = 0;
            }

            if (this.selected_adultsData + this.selected_childrenData + this.selected_infantData > this.totalAllowdPax) {
                this.selected_infantData = 0;

            }
            var cabin = getCabinName(this.selected_cabinData);
            var totalpax = this.selected_adultsData + this.selected_childrenData + this.selected_infantData;
            if (parseInt(totalpax) > 1) {
                totalpax = totalpax + ' Passengers';
            } else {
                totalpax = totalpax + ' Passenger'
            }
            this.Totaltravaller = totalpax + ',' + cabin;
        },
        onSelectedAirlineAutoComplete: _.debounce(function (keywordEntered) {
            if (keywordEntered.length > 2) {
                this.airlineautoCompleteProgress = true;
                this.airlineloading = true;
                var newData = [];
                this.airlineList.filter(function (el) {
                    if (el.A.toLowerCase().indexOf(keywordEntered.toLowerCase()) >= 0) {
                        newData.push({
                            "code": el.C,
                            "label": el.A
                        });
                    }
                });
                if (newData.length > 0) {
                    this.Airlineresults = newData;
                    this.airlineautoCompleteProgress = true;
                    this.airlineloading = false;
                } else {
                    this.Airlineresults = [];
                    this.airlineautoCompleteProgress = false;
                    this.airlineloading = false;
                }

            } else {
                this.Airlineresults = [];
                this.airlineautoCompleteProgress = false;
                this.airlineloading = false;
            }
        }, 100),
        onSelectedAirline: function (code, label) {
            this.airlineserch = label;
            this.Airlineresults = [];
            this.preferAirline = code;

        },
        setCalender() {
            var systemDateFormat = 'dd/mm/yy'; // generalInformation.systemSettings.systemDateFormat;
            var numberofmonths = generalInformation.systemSettings.calendarDisplay;
            var vm = this;
            var tempnumberofmonths = numberofmonths;
            if (parseInt($(window).width()) > 500 && parseInt($(window).width()) <= 999) {
                tempnumberofmonths = 2;
            } else if (parseInt($(window).width()) > 999) {
                tempnumberofmonths = 3;
            }
            $("#deptDate01").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: tempnumberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                onSelect: function (selectedDate) {
                    vm.oldRetDate = selectedDate;
                    $("#retDate").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);

                }
            });

            $("#retDate").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: tempnumberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#deptDate01").val();
                    $("#retDate").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    if ($('#deptDate01').val() != '') {
                        vm.tripTypeData = 'r';
                        // document.getElementById("radio-btn-2").checked = true;
                    } else {
                        $("#retDate").val("");
                        alertify.alert('Please fill !', 'Please fill departure date !');
                    }
                }
            });

            $("#txtLeg2Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: tempnumberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#deptDate01").val();
                    $("#retDate").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg2Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg3Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: tempnumberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg2Date").val();
                    $("#txtLeg3Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg4Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: tempnumberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg3Date").val();
                    $("#txtLeg4Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg5Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: tempnumberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg4Date").val();
                    $("#txtLeg5Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                }
            });

            $("#txtLeg6Date").datepicker({
                minDate: "0d",
                maxDate: "360d",
                numberOfMonths: tempnumberofmonths,
                changeMonth: true,
                showButtonPanel: false,
                dateFormat: systemDateFormat,
                beforeShow: function (event, ui) {
                    var selectedDate = $("#txtLeg5Date").val();
                    $("#txtLeg6Date").datepicker("option", "minDate", selectedDate);
                },
                onSelect: function (selectedDate) {}
            });
        },
        serchflight: function () {
            var self = this;
            var Departuredate = $('#deptDate01').val() == "" ? "" : $('#deptDate01').datepicker('getDate');
            if (!this.flightSearchCities.cityFrom0) {
                alertify.alert(self.Alert_Validation_Label, self.Please_fill_origin_Alert_Msg);
                return false;
            }
            if (!this.flightSearchCities.cityTo0) {
                alertify.alert(self.Alert_Validation_Label, self.Please_fill_destination_Alert_Msg);
                return false;
            }
            if (!Departuredate) {
                alertify.alert(self.Alert_Validation_Label, self.Please_fill_dept_date_Alert_Ms);
            }

            var tripType = this.tripType;

            var sec1TravelDate = moment(Departuredate).format('DD|MM|YYYY');
            var sectors = this.flightSearchCities.cityFrom0 + '-' + this.flightSearchCities.cityTo0 + '-' + sec1TravelDate;
            if (this.tripType.toLowerCase() == 'r') {
                var ArrivalDate = $('#retDate').val() == "" ? "" : $('#retDate').datepicker('getDate');
                if (!isNullorUndefined(ArrivalDate)) {
                    ArrivalDate = moment(ArrivalDate).format('DD|MM|YYYY');
                    if (!ArrivalDate) {
                        // alertify.alert(self.Alert_Validation_Label, self.Please_Arriv_Date_Alert_Msg);
                        // return false;
                        tripType = 'o';
                    } else {
                        sectors += '/' + this.flightSearchCities.cityTo0 + '-' + this.flightSearchCities.cityFrom0 + '-' + ArrivalDate;
                    }
                } else {
                    // alertify.alert(self.Alert_Validation_Label, self.Please_Arriv_Date_Alert_Msg);
                    // return false;
                    tripType = 'o';
                }
            }
            if (this.tripType.toLowerCase() == 'm') {
                for (var legValue = 1; legValue <= this.legcount; legValue++) {
                    var temDeparturedate = $('#txtLeg' + (legValue + 1) + 'Date').val() == "" ? "" : $('#txtLeg' + (legValue + 1) + 'Date').datepicker('getDate');
                    var from = 'cityFrom' + legValue;
                    var to = 'cityTo' + legValue;
                    if (temDeparturedate != "" && this.flightSearchCities[from] != "" && this.flightSearchCities[to] != "") {
                        var departureFrom = this.flightSearchCities[from];
                        var arrivalTo = this.flightSearchCities[to];
                        var travelDate = moment(temDeparturedate).format('DD|MM|YYYY');
                        sectors += '/' + departureFrom + '-' + arrivalTo + '-' + travelDate;
                    } else {
                        alertify.alert(self.Alert_Validation_Label, self.Please_fill_Trip_Alert_Msg + ' ' + (legValue + 1) + ' ' + this.Fields_Alert_Msg);
                        return false;
                    }
                }
            }
            var directFlight = this.direct_flightData ? 'DF' : 'AF';
            var calendarSearch = this.calendarSearch ? '1' : '0';
            var adult = this.selected_adultsData;
            var child = this.selected_childrenData;
            var infant = this.selected_infantData;
            var cabin = this.selected_cabinData;
            var preferAirline = this.preferAirline;
            var airsupplier = 'all';
            self.isSending = true;
            
            var legDetails = [];
            if (this.triptype == 'M') {
                for (var legValue = 1; legValue <= this.legcount; legValue++) {
                    let temDeparturedate = $('#txtLeg' + (legValue + 1) + 'Date').val() == "" ? "" : $('#txtLeg' + (legValue + 1) + 'Date').datepicker('getDate');
                    let from = 'cityFrom' + legValue;
                    let to = 'cityTo' + legValue;
                    if (temDeparturedate != "" && this.flightSearchCities[from] != "" && this.flightSearchCities[to] != "") {
                        legDetails.push(this.flightSearchCities[from] + '|' + this.flightSearchCities[to])
                    }
                }
            } else {
                legDetails.push(this.flightSearchCities.cityFrom0 + '|' + this.flightSearchCities.cityTo0);
            }
            
            var PostData = {
                Leg: legDetails,
                SupplierCode: this.selectedSuppliers.filter(function(e) { return ([1, 45, 55, 64, 42].indexOf(e) == -1) || (["1", "45", "55", "64", "42"].indexOf(e) == -1)}).map(function(e){return e.toString()})
              }

            var config = {
                axiosConfig: {
                    method: "POST",
                    url: HubServiceUrls.elasticSearch.airFilter,
                    data: PostData
                },
                successCallback: function (response) {
                    if (response.data) {
                        var tempSupp = [];
                        for (let index = 0; index < response.data.data.length; index++) {
                            tempSupp.push(response.data.data[index].SupplierCode);
                        }
                        var tmpArr = self.selectedSuppliers.filter(function(e) { return ([1, 45, 55, 64, 42].indexOf(e) != -1) || (["1", "45", "55", "64", "42"].indexOf(e) != -1)});
                        self.selectedSuppliers = tempSupp.concat(tmpArr);
                    }

                    if (self.selectedSuppliers.length == 0 && response.data.data.length == 0) {
                        alertify.alert(self.Alert_Validation_Label, "Supplier does not support route.");
                        self.isSending = false;
                        return false;
                    }
                    
                    // var hasFlyDubai = this.selectedSuppliers.indexOf("54") != -1 || this.selectedSuppliers.indexOf(54) != -1;
                    // var removeFZSupplier = false;
                    // if (hasFlyDubai) {
                    //     for (var key of Object.keys(this.flightSearchCities)) {
                    //         if (self.flightSearchCities[key]) {
                    //             var validAirport = _.find(fzAirports, function(e){ return e.airportCode == self.flightSearchCities[key]; });
                    //             if (validAirport==undefined) {
                    //                 removeFZSupplier = true;
                    //             }
                    //         }
                    //     }
                    //     if (removeFZSupplier) {
                    //         var index = this.selectedSuppliers.indexOf("54");
                    //         index = index == -1 ? this.selectedSuppliers.indexOf(54) : index;
                    //         if (index > -1) {
                    //             this.selectedSuppliers.splice(index, 1);
                    //         }
                    //     }
                    // }
                    if (self.selectedSuppliers.length == 0) {
                        alertify.alert(self.Alert_Validation_Label, "No supplier selected.");
                        self.isSending = false;
                        return false;
                    }
                    airsupplier = self.selectedSuppliers.length > 0 ? self.selectedSuppliers.join('|') : 'all';
                    var searchUrl = '/Flights/flightresults.html?flight=/' + sectors + '/' + adult + '-' + child + '-' + infant + '-' + cabin + '-' + airsupplier + '-50-F-' + tripType + '-F-' + preferAirline + '-' + directFlight + '-' + calendarSearch;
                    //searchUrl = searchUrl.toLocaleLowerCase();
                    window.location.href = searchUrl;
                },
                errorCallback: function (error) {
                    self.showLoader = false;
                },
                showAlert: true
            }
            mainAxiosRequest(config);
        },
        hideautocompelteair: function () {
            this.airlineautoCompleteProgress = false;
        },
        setModifySearch: function () {
            var self = this;
            if (oDestination.length > 0) {
                oDestination.forEach(function (item, index) {
                    var from = 'cityFrom' + index;
                    var to = 'cityTo' + index;
                    self.flightSearchCities[from] = item.originLocation;
                    self.flightSearchCities[to] = item.destinationLocation;
                    self.getairportfromelasticserch(item.originLocation, function (params) {
                        self.flightSearchCityName[from] = params;
                        self.status = true;
                    });
                    self.getairportfromelasticserch(item.destinationLocation, function (params) {
                        self.flightSearchCityName[to] = params;
                        self.status = false;
                    });
                });
                $("#deptDate01").val(moment(oDestination[0].departureDate, 'DD-MM-YYYY').format('DD/MM/YYYY'));
                if (self.tripType.toLowerCase() == 'r') {
                    $("#retDate").val(moment(oDestination[1].departureDate, 'DD-MM-YYYY').format('DD/MM/YYYY'));
                } else if (self.tripType.toLowerCase() == 'm') {
                    this.legcount = oDestination.length - 1;
                    var mulLeg = 2;
                    for (let index = 1; index < oDestination.length; index++) {
                        $('#txtLeg' + (mulLeg) + 'Date').val(moment(oDestination[index].departureDate, 'DD-MM-YYYY').format('DD/MM/YYYY'));
                        mulLeg++;
                    }
                }
                this.status = true;
            }
        },
        getairportfromelasticserch: function (airportcode, callBack) {
            var self = this;

            var uppercaseFirstLetter = airportcode.charAt(0).toUpperCase() + airportcode.slice(1).toLowerCase();
            uppercaseFirstLetter = "*" + uppercaseFirstLetter + "*";
            var lowercaseLetter = "*" + airportcode.toLowerCase() + "*";
            var uppercaseLetter = "*" + airportcode.toUpperCase() + "*";

            var query = {
                query: {
                    bool: {
                        should: [{
                            bool: {
                                should: [{
                                    wildcard: {
                                        iata_code: {
                                            value: uppercaseFirstLetter,
                                            boost: 3.0
                                        }
                                    }
                                }, {
                                    wildcard: {
                                        iata_code: {
                                            value: uppercaseLetter,
                                            boost: 3.0
                                        }
                                    }
                                }, {
                                    wildcard: {
                                        iata_code: {
                                            value: lowercaseLetter,
                                            boost: 3.0
                                        }
                                    }
                                }]
                            }
                        }, {
                            bool: {
                                should: [{
                                    wildcard: {
                                        municipality: uppercaseFirstLetter
                                    }
                                }, {
                                    wildcard: {
                                        municipality: uppercaseLetter
                                    }
                                }, {
                                    wildcard: {
                                        municipality: lowercaseLetter
                                    }
                                }]
                            }
                        }, {
                            bool: {
                                should: [{
                                    wildcard: {
                                        name: uppercaseFirstLetter
                                    }
                                }, {
                                    wildcard: {
                                        name: uppercaseLetter
                                    }
                                }, {
                                    wildcard: {
                                        name: lowercaseLetter
                                    }
                                }]
                            }
                        }],
                        must: [{
                            "exists": {
                                "field": "iata_code"
                            }
                        }]
                    }
                }
            };
            var config = {
                axiosConfig: {
                    headers: {
                        "Content-Type": "application/json"
                    },
                    method: "post",
                    url: HubServiceUrls.elasticSearch.url,
                    data: query
                },
                successCallback: function (resp) {
                    finalResult = [];
                    var hits = resp.data.hits.hits;
                    var Citymap = new Map();
                    for (var i = 0; i < hits.length; i++) {
                        Citymap.set(hits[i]._source.iata_code, hits[i]._source);
                    }
                    var get_values = Citymap.values();
                    var Cityvalues = [];
                    for (var ele of get_values) {
                        Cityvalues.push(ele);
                    }
                    if (Cityvalues.length > 0) {
                        var city = Cityvalues[0].name + ", " + Cityvalues[0].iso_country + '(' + Cityvalues[0].iata_code + ')';
                        callBack(city);
                        //return city;
                    }
                },
                errorCallback: function (error) { },
                showAlert: false
            };

            mainAxiosRequest(config);
            // var client = new elasticsearch.Client({
            //     host: [{
            //         host: HubServiceUrls.elasticSearch.elasticsearchHost,
            //         auth: HubServiceUrls.elasticSearch.auth,
            //         protocol: HubServiceUrls.elasticSearch.protocol,
            //         port: HubServiceUrls.elasticSearch.port,
            //         requestTimeout: 60000
            //     }],
            //     log: 'trace'
            // });
            // client.search({
            //     index: 'airport_info',
            //     size: 1,
            //     timeout: "3000ms",
            //     body: query
            // }).then(function (resp) {
                
            // })
        },
        pluck(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry.push(item[key]);
                }
            });
            return Temparry;
        },
        pluckcom(key, contentArry) {
            var Temparry = [];
            contentArry.map(function (item) {
                if (item[key] != undefined) {
                    Temparry = item[key];
                }
            });
            return Temparry;
        },
        getPagecontent: function () {
            var self = this;
            //getAgencycode(function (response) {
            var vm = this;
            var huburl = vm.commonStore.hubUrls.hubConnection.cmsUrl;
            var portno = vm.commonStore.hubUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            //var langauage = 'ar';
            self.dir = langauage == "ar" ? "rtl" : "ltr";

            var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + self.commonAgencyCode + '/Template/Flight Results Page/Flight Results Page/Flight Results Page.ftl';
            axios.get(cmsPage, {
                headers: {
                    'content-type': 'text/html',
                    'Accept': 'text/html',
                    'Accept-Language': langauage
                }
            }).then(function (response) {
                self.content = response.data;
                if (response.data.area_List.length > 0) {
                    var mainComp = response.data.area_List[0].Main_Area;
                    var alertComp = response.data.area_List[2].Alerts;
                    //Main Section
                    self.Modify_Search_Label = self.pluckcom('Modify_Search_Label', mainComp.component);
                    self.OneWay_Label = self.pluckcom('OneWay_Label', mainComp.component);
                    self.RoundTrip_Label = self.pluckcom('RoundTrip_Label', mainComp.component);
                    self.MultiCity_Label = self.pluckcom('MultiCity_Label', mainComp.component);
                    self.From_Label = self.pluckcom('From_Label', mainComp.component);
                    self.To_Label = self.pluckcom('To_Label', mainComp.component);
                    self.Departure_Date_Label = self.pluckcom('Departure_Date_Label', mainComp.component);
                    self.Arrival_Date_Label = self.pluckcom('Arrival_Date_Label', mainComp.component);
                    self.Trip_Label = self.pluckcom('Trip_Label', mainComp.component);
                    self.Travel_Date_Label = self.pluckcom('Travel_Date_Label', mainComp.component);
                    self.Add_up_to_Label1 = self.pluckcom('Add_up_to_Label1', mainComp.component);
                    self.Add_up_to_Label2 = self.pluckcom('Add_up_to_Label2', mainComp.component);
                    self.Adult_Label = self.pluckcom('Adult_Label', mainComp.component);
                    self.Adult_option_Label = self.pluckcom('Adult_option_Label', mainComp.component);
                    self.Adults_option_Label = self.pluckcom('Adults_option_Label', mainComp.component);
                    self.Child_Label = self.pluckcom('Child_Label', mainComp.component);
                    self.Child_Option_Label = self.pluckcom('Child_Option_Label', mainComp.component);
                    self.Children_Option_Label = self.pluckcom('Children_Option_Label', mainComp.component);
                    self.Infant_Label = self.pluckcom('Infant_Label', mainComp.component);
                    self.Infant_Option_Label = self.pluckcom('Infant_Option_Label', mainComp.component);
                    self.Infants_Option_Label = self.pluckcom('Infants_Option_Label', mainComp.component);
                    self.Suppliers_Label = self.pluckcom('Suppliers_Label', mainComp.component);
                    self.Select_all_Label = self.pluckcom('Select_all_Label', mainComp.component);
                    self.Airline_Label = self.pluckcom('Airline_Label', mainComp.component);
                    self.Direct_flights_Label = self.pluckcom('Direct_flights_Label', mainComp.component);
                    self.Search_Flights_Label = self.pluckcom('Search_Flights_Label', mainComp.component);
                    self.Return_Date_Label = self.pluckcom('Return_Date_Label', mainComp.component);
                    self.Class_Label = self.pluckcom('Class_Label', mainComp.component);
                    self.Economy_Label = self.pluckcom('Economy_Label', mainComp.component);
                    self.Business_Label = self.pluckcom('Business_Label', mainComp.component);
                    self.First = self.pluckcom('First', mainComp.component);
                    //Alerts
                    self.Alert_Validation_Label = self.pluckcom('Alert_Validation_Label', alertComp.component);
                    self.Departure_and_arrival__Alert_Msg = self.pluckcom('Departure_and_arrival__Alert_Msg', alertComp.component);
                    self.Please_fill_origin_Alert_Msg = self.pluckcom('Please_fill_origin_Alert_Msg', alertComp.component);
                    self.Please_fill_destination_Alert_Msg = self.pluckcom('Please_fill_destination_Alert_Msg', alertComp.component);
                    self.Please_fill_dept_date_Alert_Ms = self.pluckcom('Please_fill_dept_date_Alert_Ms', alertComp.component);
                    self.Please_Arriv_Date_Alert_Msg = self.pluckcom('Please_Arriv_Date_Alert_Msg', alertComp.component);
                    self.Please_fill_Trip_Alert_Msg = self.pluckcom('Please_fill_Trip_Alert_Msg', alertComp.component);
                    self.Fields_Alert_Msg = self.pluckcom('Fields_Alert_Msg', alertComp.component);
                }
            }).catch(function (error) {
                console.log(error);
            });
            //});

        },
        setCHDINFTravellers: function () {
            var self = this;
            var selectedADT = $('#ddladult').val();
            var selectedChild = $('#ddlchild').val();
            var selectedInfant = $('#ddlinfant').val();
            var totalAllowdPax = 9;
            var Child = parseInt(totalAllowdPax) - parseInt(selectedADT);
            $('#ddlchild').empty();
            for (var i = 0; i <= Child; i++) {
                var Childtext = self.Child_Option_Label;
                if (i > 1) {
                    Childtext = self.Children_Option_Label;
                }
                $('#ddlchild').append('<option value="' + i + '">' + i + ' ' + Childtext + '</option>');
                this.selected_childrenData = 0;
            }
            if (selectedChild > 0 && selectedChild <= Child) {
                $('#ddlchild').val(selectedChild);
            }
            $('#ddlinfant').empty();
            var infant = parseInt(selectedADT);
            if (infant + parseInt(selectedChild) + parseInt(selectedADT) > 9) {
                infant = parseInt(totalAllowdPax) - (parseInt(selectedADT) + parseInt(selectedChild));
            }
            infant = ((parseInt(infant) < 0) ? 0 : infant);
            if (infant == 0) selectedInfant = 0;
            for (var i = 0; i <= parseInt(infant); i++) {
                var Infanttext = self.Infant_Option_Label;
                if (i > 1) {
                    Infanttext = self.Infants_Option_Label;
                }
                $('#ddlinfant').append('<option value="' + i + '">' + i + ' ' + Infanttext + '</option>');
                this.selected_infantData = 0;
            }
            if (selectedInfant > 0) {
                $('#ddlinfant').val((selectedInfant <= infant) ? selectedInfant : infant);
            }
        },
        SetInfantTravellers: function () {
            var self = this;
            var selectedADT = $('#ddladult').val();
            var selectedChild = $('#ddlchild').val();
            var selectedInfant = $('#ddlinfant').val();
            var totalAllowdPax = 9;
            var remiaingpax = parseInt(selectedADT);
            if (remiaingpax + parseInt(selectedChild) + parseInt(selectedADT) > 9) {
                remiaingpax = totalAllowdPax - (parseInt(selectedADT) + parseInt(selectedChild));
            }
            $('#ddlinfant').empty();
            remiaingpax = ((parseInt(remiaingpax) < 0) ? 0 : remiaingpax);
            for (var i = 0; i <= parseInt(remiaingpax); i++) {
                var inftext = self.Infant_Option_Label;
                if (i > 1) {
                    inftext = self.Infants_Option_Label;
                }
                $('#ddlinfant').append('<option value="' + i + '">' + i + ' ' + inftext + '</option>');
                this.selected_infantData = 0;
            }
            if (selectedInfant > 0) {
                $('#ddlinfant').val((selectedInfant <= remiaingpax) ? selectedInfant : remiaingpax);
            }
        },
        retDateClose: function () {
            this.tripTypeData = 'o';
        },
        tripTypeReturn: function () {
            if ($("#deptDate01").val() == "") {
                $("#retDate").datepicker("setDate", 'today');
            } else {
                $("#retDate").datepicker("setDate", new Date(moment($("#deptDate01").val(), "DD/MM/YYYY")));
            }
        }
    },
    updated: function () {
        this.setCalender();
    }
});

function openTab(evt, segIndex, divindex, title) {
    var i, tab, currtab, tablinks;
    currtab = document.getElementById('tab_rule_' + segIndex + '_' + divindex);
    tab = document.getElementsByClassName('tab_rule');
    for (i = 0; i < tab.length; i++) {
        tab[i].style.display = 'none';
    }
    tablinks = document.getElementsByClassName('mini_tablinks_' + divindex);
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(' active', '');
    }
    currtab.style.display = 'block';
    evt.currentTarget.className += ' active';

    try {
        currtab.childNodes[0].childNodes[0].click();
        // document.getElementById('tabmini_' + divindex).getElementsByTagName('button')[0].click(); 
        document.getElementById('mini_tab_head_' + divindex).innerText = title;
    } catch (ex) {}
}

function openSubTab(evt, segIndex, cat, divindex) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName('tabcontent_' + divindex);
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = 'none';
    }
    tablinks = document.getElementsByClassName('tablinks');
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(' active', '');
    }
    document.getElementById('catgry_' + segIndex + '_' + cat + '_' + divindex).style.display = 'block';
    evt.currentTarget.className += ' active';
}

function metrixAir() {
    var owl = $("#slider-carousel");
    var owlInstance = owl.data('owlCarousel');
    // if instance is existing
    if (owlInstance != null) {
        owlInstance.reinit();
    } else {
        owl.owlCarousel({
            items: 5,
            itemsDesktop: [1024, 4],
            itemsDesktopSmall: [768, 4],
            itemsTablet: [480, 2],
            itemsMobile: false,
            pagination: false
        });
        $(".next").click(function () {
            owl.trigger('owl.next');
        })
        $(".prev").click(function () {
            owl.trigger('owl.prev');
        })
    }


};

function listHeight() {
    var listHeight = 0;
    $(".catgry_sec ul").each(function () {
        var newHeight = 0;

        $("li", this).each(function () {
            var el = $(this);
            el.css('height', 'auto');
            newHeight = el.height();

            if (newHeight > listHeight) {
                listHeight = newHeight;
            }
        });

        $("li", this).height(listHeight);
    });
    $(".first_title ul").each(function () {
        $("li", this).height(listHeight);
    });
}