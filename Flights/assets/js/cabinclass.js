function getCabinClassObject(cabin) {
    try { return $.grep(airlineCabinClass, function (n, i) { return (n.AirlineCode.toLowerCase() == cabin.toLowerCase()); })[0] } catch (err) { return null; }
}
var airlineCabinClass = [{
    AirlineCode: "A",
    AirlineClass: "First Class Discounted",
    BasicClasscode: "F",
    BasicClass: "First Class"
},
{
    AirlineCode: "B",
    AirlineClass: "Economy/Coach � Usually an upgradable fare to Business",
    BasicClasscode: "Y",
    BasicClass: "Economy"
},
{
    AirlineCode: "C",
    AirlineClass: "Business Class",
    BasicClasscode: "C",
    BasicClass: "Business"
},
{
    AirlineCode: "D",
    AirlineClass: "Business Class Discounted",
    BasicClasscode: "C",
    BasicClass: "Business"
},
{
    AirlineCode: "E",
    AirlineClass: "Shuttle Service (no reservation allowed) or Economy/Coach Discounted",
    BasicClasscode: "Y",
    BasicClass: "Economy"
},
{
    AirlineCode: "F",
    AirlineClass: "First Class",
    BasicClasscode: "F",
    BasicClass: "First Class"
},
{
    AirlineCode: "G",
    AirlineClass: "Conditional Reservation",
    BasicClasscode: "Y",
    BasicClass: "Economy"
},
{
    AirlineCode: "H",
    AirlineClass: "Economy/Coach Discounted � Usually an upgradable fare to Business",
    BasicClasscode: "Y",
    BasicClass: "Economy"
},
{
    AirlineCode: "I",
    AirlineClass: "Economy",
    BasicClasscode: "Y",
    BasicClass: "Economy"
},
{
    AirlineCode: "J",
    AirlineClass: "Business Class Premium",
    BasicClasscode: "C",
    BasicClass: "Business"
},
{
    AirlineCode: "K",
    AirlineClass: "Economy/Coach Discounted",
    BasicClasscode: "Y",
    BasicClass: "Economy"
},
{
    AirlineCode: "L",
    AirlineClass: "Economy/Coach Discounted",
    BasicClasscode: "Y",
    BasicClass: "Economy"
},
{
    AirlineCode: "L",
    AirlineClass: "Economy/Coach Discounted",
    BasicClasscode: "W",
    BasicClass: "PremiumEconomy"
},
{
    AirlineCode: "L",
    AirlineClass: "Economy/Coach Discounted",
    BasicClasscode: "M",
    BasicClass: "RegularEconomy"
},
{
    AirlineCode: "M",
    AirlineClass: "Economy/Coach Discounted � Usually an upgradable fare to Business",
    BasicClasscode: "Y",
    BasicClass: "Economy"
},
{
    AirlineCode: "N",
    AirlineClass: "Economy/Coach Discounted",
    BasicClasscode: "Y",
    BasicClass: "Economy"
},
{
    AirlineCode: "O",
    AirlineClass: "Economy",
    BasicClasscode: "Y",
    BasicClass: "Economy"
},
{
    AirlineCode: "P",
    AirlineClass: "First Class Premium",
    BasicClasscode: "F",
    BasicClass: "First Class"
},
{
    AirlineCode: "Q",
    AirlineClass: "Economy/Coach Discounted",
    BasicClasscode: "Y",
    BasicClass: "Economy"
},
{
    AirlineCode: "R",
    AirlineClass: "First Class Suite or Supersonic (discontinued)",
    BasicClasscode: "F",
    BasicClass: "First Class"
},
{
    AirlineCode: "S",
    AirlineClass: "Economy/Coach",
    BasicClasscode: "Y",
    BasicClass: "Economy"
},
{
    AirlineCode: "T",
    AirlineClass: "Economy/Coach Discounted",
    BasicClasscode: "Y",
    BasicClass: "Economy"
},
{
    AirlineCode: "U",
    AirlineClass: "Shuttle Service (no reservation needed/seat guaranteed)",
    BasicClasscode: "Y",
    BasicClass: "Economy"
},
{
    AirlineCode: "V",
    AirlineClass: "Economy/Coach Discounted",
    BasicClasscode: "Y",
    BasicClass: "Economy"
},
{
    AirlineCode: "W",
    AirlineClass: "Economy/Coach Premium",
    BasicClasscode: "Y",
    BasicClass: "Economy"
},
{
    AirlineCode: "X",
    AirlineClass: "Economy/Coach Discounted",
    BasicClasscode: "Y",
    BasicClass: "Economy"
},
{
    AirlineCode: "Y",
    AirlineClass: "Economy/Coach",
    BasicClasscode: "Y",
    BasicClass: "Economy"
},
{
    AirlineCode: "Z",
    AirlineClass: "Business Class Discounted",
    BasicClasscode: "C",
    BasicClass: "Business"
}
];
