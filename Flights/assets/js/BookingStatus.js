﻿var BookingStatus = [
    {
        StatusCode: "RQ",
        Status: "Status Requested"
    },
    {
        StatusCode: "HK",
        Status: "Confirmed - Not Ticketed"
    },
    {
        StatusCode: "OK",
        Status: "Ticketed"
    },
    {
        StatusCode: "PD",
        Status: "Pending"
    },
    {
        StatusCode: "RJ",
        Status: "Rejected"
    },
    {
        StatusCode: "TP",
        Status: "Ticket in progress"
    },
    {
        StatusCode: "HN",
        Status: "Status Requested"
    },
    {
        StatusCode: "RC",
        Status: "Reconfirmed"
    },
    {
        StatusCode: "RR",
        Status: "Confirmation Pending"
    },
    {
        StatusCode: "XM",
        Status: "Cancellation Request Mail"
    },
    {
        StatusCode: "XR",
        Status: "Cancellation Request"
    },
    {
        StatusCode: "XX",
        Status: "Cancelled"
    },

]