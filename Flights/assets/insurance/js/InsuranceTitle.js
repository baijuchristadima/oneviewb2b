﻿var paxTitles = [
    { id: 1, name: 'Mr', paxType: ['ADT', 'SR1', 'SR2'], gender: 'M' },
    { id: 2, name: 'Miss', paxType: ['ADT', 'SR1', 'SR2'], gender: 'F' },
    { id: 3, name: 'Mrs', paxType: ['ADT', 'SR1', 'SR2'], gender: 'F' },
    { id: 4, name: 'Ms', paxType: ['CHD', 'INF'], gender: 'F' },
    { id: 5, name: 'Master', paxType: ['CHD', 'INF'], gender: 'M' }
]