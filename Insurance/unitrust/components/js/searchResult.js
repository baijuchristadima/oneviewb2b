Vue.component("unitrust-insurance", {
  mixins: [websocketMixin],
  data() {
    return {
      commonStore: vueCommonStore.state,
      pageObject: {
        Banner_Title: "Unitrust",
        Banner_Image: "/Insurance/InsuranceComponents/Images/tune-protect-banner.jpg",
      },
      showLoaders: true,
      insuranceList: [],
    };
  },
  methods: {
    getUrlVars: function () {
      var vars = [],
        hash;
      var hashes = window.location.href.slice(window.location.href.indexOf("?") + 1).split("&");
      for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split("=");
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
      }
      return vars;
    },
    loadAllResults: function () {
      var urldata = this.getUrlVars().insurance;
      if (urldata == undefined) {
        window.location.href = "index.html";
      } else {
        var allDatas = urldata.split("--");
        if (allDatas.length > 2) {
          var countryDetails = allDatas[0];
          var dateDetails = allDatas[1];
          var paxDetails = allDatas[2];
          var departureCountry = countryDetails.split("-")[0];
          var departureCity = countryDetails.split("-")[1];
          var arrivalCountry = countryDetails.split("-")[2];
          var arrivalCity = countryDetails.split("-")[3];
          var departureDate = dateDetails.split("-")[0];
          var returnDate = dateDetails.split("-")[1] || "NA";
          var senior = paxDetails.split("|")[3];
          var adult = paxDetails.split("|")[2];
          var children = paxDetails.split("|")[1];
          var infant = paxDetails.split("|")[0];
          var duration = moment(returnDate, "DD|MM|YYYY").diff(moment(departureDate, "DD|MM|YYYY"), "days");
          var travellerList = JSON.parse(sessionStorage.travellerList);

          var travelpaxDetails = { adult: adult, children: children, infant: infant };
          var traveldateDetails = { Start_Date: departureDate, Return_Date: returnDate };
          var stationDetails = { Dept_Country: departureCountry, Dept_City: departureCity, Arrival_Country: arrivalCountry, Arrival_City: arrivalCity };
          this.Search_Details = { Pax_Details: travelpaxDetails, Date_Details: traveldateDetails, Station_Details: stationDetails };
         
          this.connect(); //open websocket connection

          var searchRQ = {
            request: {
              service: "InsuranceUniTrustRQ",
              content: {
                command: "UniTrustSearchRQ",
                premiumRQ: {
                  from: departureCity,
                  to: arrivalCity,
                  fromCountryCode: departureCountry,
                  toCountryCode: arrivalCountry,
                  duration: duration.toString(),
                  ageList: travellerList.map(e=>e.age),
                },
              },
              token: window.localStorage.getItem("accessToken"),
              supplierCodes: [76],
            },
          };

          this.sendMessage(JSON.stringify(searchRQ));
          var vm = this;
          setTimeout(function () {
              vm.disconnect();
          }, 90000);
        } else {
          window.location.href = "index.html";
        }
      }
    },
    processInsuranceResult: function (response) {
      this.sealedCode = response.response.content.premiumRS.sealed;
      this.selectCredential = response.response.selectCredential;
      this.responseData = response.response.content.premiumRS;
      this.insuranceList.push({
        country: response.response.content.premiumRS.country,
        premium: response.response.content.premiumRS.premium,
      });
      this.showLoaders = false;
    },
    issuePolicy: function () {
        var selectInsuranceRequest = { Search_Details: this.Search_Details, Plan_Details: this.responseData, sealedCode: this.sealedCode, credential: this.selectCredential};
        sessionStorage.selectedInsurance = JSON.stringify(selectInsuranceRequest);
        window.location.href = '/Insurance/unitrust/IssuePolicy.html'; 
      }
    },
  mounted() {
    this.loadAllResults();
  },
});
