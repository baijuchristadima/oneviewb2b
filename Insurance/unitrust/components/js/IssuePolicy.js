﻿var countryList = [
  { code: "AF", name: "Afghanistan" },
  { code: "AL", name: "Albania" },
  { code: "DZ", name: "Algeria" },
  { code: "AS", name: "American Samoa" },
  { code: "AD", name: "Andorra" },
  { code: "AO", name: "Angola" },
  { code: "AI", name: "Anguilla" },
  { code: "AQ", name: "Antartica" },
  { code: "AG", name: "Antigua And Barbuda" },
  { code: "AR", name: "Argentina" },
  { code: "AM", name: "Armenia" },
  { code: "AW", name: "Aruba" },
  { code: "AU", name: "Australia" },
  { code: "AT", name: "Austria" },
  { code: "AZ", name: "Azerbaijan" },
  { code: "BS", name: "Bahamas" },
  { code: "BH", name: "Bahrain" },
  { code: "BD", name: "Bangladesh" },
  { code: "BB", name: "Barbados" },
  { code: "BY", name: "Belarus" },
  { code: "BE", name: "Belgium" },
  { code: "BZ", name: "Belize" },
  { code: "BJ", name: "Benin" },
  { code: "BM", name: "Bermuda" },
  { code: "BT", name: "Bhutan" },
  { code: "BO", name: "Bolivia" },
  { code: "BQ", name: "Bonaire St Eustatius And Saba " },
  { code: "BA", name: "Bosnia-Herzegovina" },
  { code: "BW", name: "Botswana" },
  { code: "BR", name: "Brazil" },
  { code: "IO", name: "British Indian Ocean Territory" },
  { code: "BN", name: "Brunei Darussalam" },
  { code: "BG", name: "Bulgaria" },
  { code: "BF", name: "Burkina Faso" },
  { code: "BI", name: "Burundi" },
  { code: "KH", name: "Cambodia" },
  { code: "CM", name: "Cameroon-Republic Of" },
  { code: "CB", name: "Canada Buffer" },
  { code: "CA", name: "Canada" },
  { code: "CV", name: "Cape Verde-Republic Of" },
  { code: "KY", name: "Cayman Islands" },
  { code: "CF", name: "Central African Republic" },
  { code: "TD", name: "Chad" },
  { code: "CL", name: "Chile" },
  { code: "CN", name: "China" },
  { code: "CX", name: "Christmas Island" },
  { code: "CC", name: "Cocos Islands" },
  { code: "CO", name: "Colombia" },
  { code: "KM", name: "Comoros" },
  { code: "CG", name: "Congo Brazzaville" },
  { code: "CD", name: "Congo The Democratic Rep Of" },
  { code: "CK", name: "Cook Islands" },
  { code: "CR", name: "Costa Rica" },
  { code: "CI", name: "Cote D Ivoire" },
  { code: "HR", name: "Croatia" },
  { code: "CU", name: "Cuba" },
  { code: "CW", name: "Curacao" },
  { code: "CY", name: "Cyprus" },
  { code: "CZ", name: "Czech Republic" },
  { code: "DK", name: "Denmark" },
  { code: "DJ", name: "Djibouti" },
  { code: "DM", name: "Dominica" },
  { code: "DO", name: "Dominican Republic" },
  { code: "TP", name: "East Timor Former Code)" },
  { code: "EC", name: "Ecuador" },
  { code: "EG", name: "Egypt" },
  { code: "SV", name: "El Salvador" },
  { code: "EU", name: "Emu European Monetary Union" },
  { code: "GQ", name: "Equatorial Guinea" },
  { code: "ER", name: "Eritrea" },
  { code: "EE", name: "Estonia" },
  { code: "ET", name: "Ethiopia" },
  { code: "FK", name: "Falkland Islands" },
  { code: "FO", name: "Faroe Islands" },
  { code: "ZZ", name: "Fictitious Points" },
  { code: "FJ", name: "Fiji" },
  { code: "FI", name: "Finland" },
  { code: "FR", name: "France" },
  { code: "GF", name: "French Guiana" },
  { code: "PF", name: "French Polynesia" },
  { code: "GA", name: "Gabon" },
  { code: "GM", name: "Gambia" },
  { code: "GE", name: "Georgia" },
  { code: "DE", name: "Germany" },
  { code: "GH", name: "Ghana" },
  { code: "GI", name: "Gibraltar" },
  { code: "GR", name: "Greece" },
  { code: "GL", name: "Greenland" },
  { code: "GD", name: "Grenada" },
  { code: "GP", name: "Guadeloupe" },
  { code: "GU", name: "Guam" },
  { code: "GT", name: "Guatemala" },
  { code: "GW", name: "Guinea Bissau" },
  { code: "GN", name: "Guinea" },
  { code: "GY", name: "Guyana" },
  { code: "HT", name: "Haiti" },
  { code: "HN", name: "Honduras" },
  { code: "HK", name: "Hong Kong" },
  { code: "HU", name: "Hungary" },
  { code: "IS", name: "Iceland" },
  { code: "IN", name: "India" },
  { code: "ID", name: "Indonesia" },
  { code: "IR", name: "Iran" },
  { code: "IQ", name: "Iraq" },
  { code: "IE", name: "Ireland-Republic Of" },
  { code: "IL", name: "Israel" },
  { code: "IT", name: "Italy" },
  { code: "JM", name: "Jamaica" },
  { code: "JP", name: "Japan" },
  { code: "JO", name: "Jordan" },
  { code: "KZ", name: "Kazakhstan" },
  { code: "KE", name: "Kenya" },
  { code: "KI", name: "Kiribati" },
  { code: "KP", name: "Korea Dem Peoples Rep Of" },
  { code: "KR", name: "Korea Republic Of" },
  { code: "KW", name: "Kuwait" },
  { code: "KG", name: "Kyrgyzstan" },
  { code: "LA", name: "Lao Peoples Dem Republic" },
  { code: "LV", name: "Latvia" },
  { code: "LB", name: "Lebanon" },
  { code: "LS", name: "Lesotho" },
  { code: "LR", name: "Liberia" },
  { code: "LY", name: "Libya" },
  { code: "LI", name: "Liechtenstein" },
  { code: "LT", name: "Lithuania" },
  { code: "LU", name: "Luxembourg" },
  { code: "MO", name: "Macao -Sar Of China-" },
  { code: "MK", name: "Macedonia -Fyrom-" },
  { code: "MG", name: "Madagascar" },
  { code: "MW", name: "Malawi" },
  { code: "MY", name: "Malaysia" },
  { code: "MV", name: "Maldives Island" },
  { code: "ML", name: "Mali" },
  { code: "MT", name: "Malta" },
  { code: "MH", name: "Marshall Islands" },
  { code: "MQ", name: "Martinique" },
  { code: "MR", name: "Mauritania" },
  { code: "MU", name: "Mauritius Island" },
  { code: "YT", name: "Mayotte" },
  { code: "MB", name: "Mexico Buffer" },
  { code: "MX", name: "Mexico" },
  { code: "FM", name: "Micronesia" },
  { code: "MD", name: "Moldova" },
  { code: "MC", name: "Monaco" },
  { code: "MN", name: "Mongolia" },
  { code: "ME", name: "Montenegro" },
  { code: "MS", name: "Montserrat" },
  { code: "MA", name: "Morocco" },
  { code: "MZ", name: "Mozambique" },
  { code: "MM", name: "Myanmar" },
  { code: "NA", name: "Namibia" },
  { code: "NR", name: "Nauru" },
  { code: "NP", name: "Nepal" },
  { code: "AN", name: "Netherlands Antilles" },
  { code: "NL", name: "Netherlands" },
  { code: "NC", name: "New Caledonia" },
  { code: "NZ", name: "New Zealand" },
  { code: "NI", name: "Nicaragua" },
  { code: "NE", name: "Niger" },
  { code: "NG", name: "Nigeria" },
  { code: "NU", name: "Niue" },
  { code: "NF", name: "Norfolk Island" },
  { code: "MP", name: "Northern Mariana Islands" },
  { code: "NO", name: "Norway" },
  { code: "OM", name: "Oman" },
  { code: "PK", name: "Pakistan" },
  { code: "PW", name: "Palau Islands" },
  { code: "PS", name: "Palestine - State Of" },
  { code: "PA", name: "Panama" },
  { code: "PG", name: "Papua New Guinea" },
  { code: "PY", name: "Paraguay" },
  { code: "PE", name: "Peru" },
  { code: "PH", name: "Philippines" },
  { code: "PL", name: "Poland" },
  { code: "PT", name: "Portugal" },
  { code: "PR", name: "Puerto Rico" },
  { code: "QA", name: "Qatar" },
  { code: "RE", name: "Reunion Island" },
  { code: "RO", name: "Romania" },
  { code: "RU", name: "Russia" },
  { code: "XU", name: "Russia" },
  { code: "RW", name: "Rwanda" },
  { code: "WS", name: "Samoa-Independent State Of" },
  { code: "SM", name: "San Marino" },
  { code: "ST", name: "Sao Tome And Principe Islands " },
  { code: "SA", name: "Saudi Arabia" },
  { code: "SN", name: "Senegal" },
  { code: "RS", name: "Serbia" },
  { code: "SC", name: "Seychelles Islands" },
  { code: "SL", name: "Sierra Leone" },
  { code: "SG", name: "Singapore" },
  { code: "SX", name: "Sint Maarten" },
  { code: "SK", name: "Slovakia" },
  { code: "SI", name: "Slovenia" },
  { code: "SB", name: "Solomon Islands" },
  { code: "SO", name: "Somalia" },
  { code: "ZA", name: "South Africa" },
  { code: "SS", name: "South Sudan" },
  { code: "ES", name: "Spain" },
  { code: "LK", name: "Sri Lanka" },
  { code: "SH", name: "St. Helena Island" },
  { code: "KN", name: "St. Kitts" },
  { code: "LC", name: "St. Lucia" },
  { code: "PM", name: "St. Pierre And Miquelon" },
  { code: "VC", name: "St. Vincent" },
  { code: "SD", name: "Sudan" },
  { code: "SR", name: "Suriname" },
  { code: "SZ", name: "Swaziland" },
  { code: "SE", name: "Sweden" },
  { code: "CH", name: "Switzerland" },
  { code: "SY", name: "Syrian Arab Republic" },
  { code: "TW", name: "Taiwan" },
  { code: "TJ", name: "Tajikistan" },
  { code: "TZ", name: "Tanzania-United Republic" },
  { code: "TH", name: "Thailand" },
  { code: "TL", name: "Timor Leste" },
  { code: "TG", name: "Togo" },
  { code: "TK", name: "Tokelau" },
  { code: "TO", name: "Tonga" },
  { code: "TT", name: "Trinidad And Tobago" },
  { code: "TN", name: "Tunisia" },
  { code: "TR", name: "Turkey" },
  { code: "TM", name: "Turkmenistan" },
  { code: "TC", name: "Turks And Caicos Islands" },
  { code: "TV", name: "Tuvalu" },
  { code: "UM", name: "U.S. Minor Outlying Islands" },
  { code: "UG", name: "Uganda" },
  { code: "UA", name: "Ukraine" },
  { code: "AE", name: "United Arab Emirates" },
  { code: "GB", name: "United Kingdom" },
  { code: "US", name: "United States Of America" },
  { code: "UY", name: "Uruguay" },
  { code: "UZ", name: "Uzbekistan" },
  { code: "VU", name: "Vanuatu" },
  { code: "VA", name: "Vatican" },
  { code: "VE", name: "Venezuela" },
  { code: "VN", name: "Vietnam" },
  { code: "VG", name: "Virgin Islands-British" },
  { code: "VI", name: "Virgin Islands-United States" },
  { code: "WF", name: "Wallis And Futuna Islands" },
  { code: "EH", name: "Western Sahara" },
  { code: "YE", name: "Yemen Republic" },
  { code: "ZM", name: "Zambia" },
  { code: "ZW", name: "Zimbabwe" },
];
Vue.component("unitrust-insurance", {
  data() {
    return {
      commonStore: vueCommonStore.state,
      selectedPlan: {},
      titleList: [
        { Name: "Mr", value: "Mr" },
        { Name: "Mrs", value: "Mrs" },
        { Name: "Ms", value: "Ms" },
      ],
      genderList: [
        { Name: "Male", value: "M" },
        { Name: "Female", value: "F" },
      ],
      countryList: countryList,
      paymentType: "C",
      Agency_Info: null,
      traveller_Details: [],
      paxDetails: {},
      Station_Details: {},
      Date_Details: {},
      sealedCode: "",
      selectCredential: {},
      pageObject: {
        Banner_Title: "Issue Policy",
        Banner_Image: "/Insurance/InsuranceComponents/Images/tune-protect-banner.jpg",
        Page_Title: "Available Plan",
        Traveller_Label: "Pax Information",
        Adult_Label: "Adult (17-75)",
        Child_Label: "Child (3-16)",
        Infant_Label: "Infant (0-2)",
        First_Name_Place_Holder: "First Name",
        Last_Name_Place_Holder: "Last Name",
        Date_Of_Birth_Place_Holder: "Date Of Birth",
        Passport_Label: "Passport No:",
        Passport_Place_Holder: "Passport Number",
        Address_Place_Holder: "Address",
        Occupation_Place_Holder: "Occupation",
        Purpose_Place_Holder: "Purpose of travel",
        MedicalCondition_Place_Holder: "Medical Condition",
        KinPhone_Place_Holder: "Contact number of Kin",
        KinAddress_Place_Holder: "Address of Kin",
        KinName_Place_Holder: "Name of Kin",
        Other_Identity_Label: "Identity No:",
        Other_Identity_Place_Holder: "Identity Number",
        Title_Place_Holder: "Title",
        Gender_Place_Holder: "Gender",
        Nationality_Place_Holder: "Nationality",
        Agency_Name_Label: "Agency Name",
        User_Name_Label: "User Name",
        Agency_Email_Label: "Email",
        Agency_Phone_Label: "Phone",
        Payment_Mode_Title: "",
        Credit_Label: "",
        Payment_Gateway_Label: "",
        Title_Alert: "Please choose title to proceed.",
        First_Name_Alert: "Please enter first name to proceed.",
        Last_Name_Alert: "Please enter last name to proceed.",
        Date_Of_Birth_Alert: "Please choose date of birth to proceed.",
        Nationality_Alert: "Please choose nationality to proceed.",
        Passport_Alert: "Please enter passport number to proceed.",
        Terms_And_Condition_Alert: "Please agree terms and condition to proceed.",
        Address_Identity_Alert: "Please enter address to proceed.",
        Occupation_Identity_Alert: "Please enter occupation to proceed.",
        Purpose_Identity_Alert: "Please enter purpose to proceed.",
        Condition_Identity_Alert: "Please enter medical condition to proceed.",
        beneficiaryDetailsName_Alert: "Please enter kin name to proceed.",
        beneficiaryDetailsAddress_Alert: "Please enter kin address to proceed.",
        beneficiaryDetailsPhone_Alert: "Please enter kin phone number to proceed.",
      },
      beneficiaryDetails: {
        name: "",
        address: "",
        phone: "",
      },
      isBooking: false,
    };
  },
  methods: {
    loadAllTravellerDetails: function (count, type) {
      for (var i = 0; i < parseInt(count); i++) {
        var travellerObject = {
          Object_Label: "",
          Object_Type: type,
          title: "",
          firstName: "",
          lastName: "",
          dateOfBirth: "",
          nationality: "",
          passportNo: "",
          address: "",
          occupation: "",
          purposeOfTravel: "",
          medicalCondition: "",
        };
        if (type == "Adult") {
          travellerObject.Object_Label = this.pageObject.Adult_Label;
        } else if (type == "Child") {
          travellerObject.Object_Label = this.pageObject.Child_Label;
        } else {
          travellerObject.Object_Label = this.pageObject.Infant_Label;
        }
        this.traveller_Details.push(travellerObject);
      }
    },
    setCalendar: function () {
      var vm = this;
      vm.$nextTick(function () {
        var tempnumberofmonths = 1;
        var dateFormat = generalInformation.systemSettings.systemDateFormat;
        if (parseInt($(window).width()) > 500 && parseInt($(window).width()) <= 999) {
          tempnumberofmonths = 2;
        } else if (parseInt($(window).width()) > 999) {
          tempnumberofmonths = 3;
        }
        for (var i = 0; i < vm.traveller_Details.length; i++) {
          $("#date" + i).datepicker({
            minDate: "0",
            maxDate: "0",
            numberOfMonths: 1,
            changeMonth: true,
            changeYear: true,
            dateFormat: dateFormat,
            showButtonPanel: false,
            onSelect: function (selectedDate) {
              var dateID = parseInt(this.id.replace("date", ""));
              var selectedObject = vm.traveller_Details[dateID];
              selectedObject.dateOfBirth = $("#" + this.id).val();
              var age = moment().diff(moment($("#" + this.id).val(), "DD MMM YY, DDD"), "years");
            },
            beforeShow: function () {
              var dateID = parseInt(this.id.replace("date", ""));
              var selectedObject = vm.traveller_Details[dateID];
              var Object_Type = selectedObject.Object_Type;
              var startDate = new Date();
              var addyears = 1;
              var addEndYears = 0;
              if (Object_Type == "Adult") {
                addyears = -75;
                addEndYears = -17;
              } else if (Object_Type == "Child") {
                addyears = -16;
                addEndYears = -3;
              } else if (Object_Type == "Infant") {
                addyears = -2;
              }
              startDate.setFullYear(startDate.getFullYear() + addyears);
              $(this).datepicker("option", "minDate", startDate);
              $(this).datepicker("option", "maxDate", addEndYears + "y");
              console.log(selectedObject);
            },
          });
        }
      });
    },
    purchase: function () {
      this.isBooking = true;
      var vm = this;
      var focusID = "";
      var divID = "";
      var alertMsg = "";
      for (var i = 0; i < vm.traveller_Details.length; i++) {
        var traveller = vm.traveller_Details[i];
        divID = "div" + i;
        if (traveller.title == undefined || traveller.title == "") {
          focusID = "title" + i;
          alertMsg = this.pageObject.Title_Alert;
          break;
        } else if (traveller.firstName == undefined || traveller.firstName == "") {
          focusID = "fname" + i;
          alertMsg = this.pageObject.First_Name_Alert;
          break;
        } else if (traveller.lastName == undefined || traveller.lastName == "") {
          focusID = "lname" + i;
          alertMsg = this.pageObject.Last_Name_Alert;
          break;
        } else if (traveller.dateOfBirth == undefined || traveller.dateOfBirth == "") {
          focusID = "date" + i;
          alertMsg = this.pageObject.Date_Of_Birth_Alert;
          break;
        } else if (traveller.nationality == undefined || traveller.nationality == "") {
          focusID = "nationality" + i;
          alertMsg = this.pageObject.Nationality_Alert;
          break;
        } else if (traveller.passportNo == undefined || traveller.passportNo == "") {
          focusID = "passport" + i;
          alertMsg = this.pageObject.Passport_Alert;
          break;
        } else if (traveller.address == undefined || traveller.address == "") {
          focusID = "address" + i;
          alertMsg = this.pageObject.Address_Identity_Alert;
          break;
        } else if (traveller.occupation == undefined || traveller.occupation == "") {
          focusID = "occupation" + i;
          alertMsg = this.pageObject.Occupation_Identity_Alert;
          break;
        } else if (traveller.purposeOfTravel == undefined || traveller.purposeOfTravel == "") {
          focusID = "purpose" + i;
          alertMsg = this.pageObject.Purpose_Identity_Alert;
          break;
        } else if (traveller.medicalCondition == undefined || traveller.medicalCondition == "") {
          focusID = "condition" + i;
          alertMsg = this.pageObject.Condition_Identity_Alert;
          break;
        } else if (beneficiaryDetailsName == undefined || beneficiaryDetailsName == "") {
          focusID = "beneficiaryDetailsName" + i;
          alertMsg = this.pageObject.beneficiaryDetailsName_Alert;
          break;
        } else if (beneficiaryDetailsAddress == undefined || beneficiaryDetailsAddress == "") {
          focusID = "beneficiaryDetailsAddress" + i;
          alertMsg = this.pageObject.beneficiaryDetailsAddress_Alert;
          break;
        } else if (beneficiaryDetailsPhone == undefined || beneficiaryDetailsPhone == "") {
          focusID = "beneficiaryDetailsPhone" + i;
          alertMsg = this.pageObject.beneficiaryDetailsPhone_Alert;
          break;
        }
      }
      if (focusID != undefined && focusID != "" && alertMsg != undefined && alertMsg != "") {
        this.isBooking = false;
        alertify.alert("Warning", alertMsg, function () {
          $("#" + focusID).focus();
          $("html, body").animate({
            scrollTop: $("#" + divID).position().top,
          });
        });
        return false;
      } else {
        if ($("#chkAgree").prop("checked") == false) {
          alertify.alert("Warning", this.pageObject.Terms_And_Condition_Alert, function () {});
          this.isBooking = false;
          return false;
        }
      }

      var hubUrls = HubServiceUrls;
      var hubUrl = hubUrls.hubConnection.baseUrl;
      var port = hubUrls.hubConnection.ipAddress;
      var singleTripInsurance = hubUrls.hubConnection.hubServices.unitrust.singleTripInsurance;

      var data = {
        request: {
          service: "InsuranceUniTrustRQ",
          content: {
            command: "UniTrustFinaliseRQ",
            singleTripInsuranceRQ: {
              travelerInfo: {
                passportNumber: this.traveller_Details[0].passportNo,
                address: this.traveller_Details[0].address,
                gender: this.traveller_Details[0].title == "Mr" ? "M" : "F",
                givenName: this.traveller_Details[0].firstName,
                surName: this.traveller_Details[0].lastName,
                title: this.traveller_Details[0].title,
                birthDate: moment(this.traveller_Details[0].dateOfBirth, "DD MMM YY, DDD").format("DD/MM/YYYY"),
                nationality: this.traveller_Details[0].nationality.code,
                paxType: "ADT",
                isLead: true,
                contact: {
                  emailList: [{emailId: this.Agency_Info.emailId}],
                  phoneList: [{number: this.Agency_Info.contactNumber}],
                },
              },
              policyDetails: {
                from: moment(this.Date_Details.Start_Date, "DD|MM|YYYY").format("DD/MM/YYYY"),
                to: moment(this.Date_Details.Return_Date, "DD|MM|YYYY").format("DD/MM/YYYY"),
                policyType: "Single",
                type: "Single",
              },
              otherInfo: {
                destination: this.selectedPlan.country.name,
                purpose: this.traveller_Details[0].purposeOfTravel,
                subdate: "19/02/2021",
                occupation: this.traveller_Details[0].occupation,
                medical: this.traveller_Details[0].medicalCondition,
                medicalCond: this.traveller_Details[0].medicalCondition,
                nextOfKin: this.beneficiaryDetails.name,
                nokAddress: this.beneficiaryDetails.address,
                nokPhoneNumber: { number: this.beneficiaryDetails.phone },
              },
              fare: this.selectedPlan.totalPremiumCost,
              paymentMode: 7,
              sealed: this.sealedCode,
            },
          },
          token: window.localStorage.getItem("accessToken"),
          supplierCodes: [76],
        },
      };

      axios
        .post(hubUrl + port + singleTripInsurance, data, {
          headers: { Authorization: "Bearer " + window.localStorage.getItem("accessToken") },
        })
        .then(function (response) {
          this.isBooking = false;
          window.sessionStorage.setItem("Insurance_Info", JSON.stringify({ referenceNumber: response.data.response.content.bookingRef }));
          window.location.href = "/Insurance/unitrust/policyinfo.html";
        })
        .catch(function (error) {
          console.log(error);
        });
    },
  },
  created: function () {
    this.Agency_Info = JSON.parse(atob(window.localStorage.getItem("agencyNode")));
    this.Agency_Username = this.Agency_Info.firstName + ' ' + this.Agency_Info.lastName;
    selectInsuranceFromSession = JSON.parse(sessionStorage.selectedInsurance);
    this.selectedPlan = selectInsuranceFromSession.Plan_Details;
    var paxDetails = selectInsuranceFromSession.Search_Details.Pax_Details;
    this.paxDetails = paxDetails;
    this.Station_Details = selectInsuranceFromSession.Search_Details.Station_Details;
    this.Date_Details = selectInsuranceFromSession.Search_Details.Date_Details;
    this.sealedCode = selectInsuranceFromSession.sealedCode;
    this.selectCredential = selectInsuranceFromSession.credential;
    window.sessionStorage.setItem("selectCredential", JSON.stringify(this.selectCredential));
    this.loadAllTravellerDetails(paxDetails.adult, 'Adult');
    this.loadAllTravellerDetails(paxDetails.children, 'Child');
    this.loadAllTravellerDetails(paxDetails.infant, 'Infant');
    this.setCalendar();
  },
});
