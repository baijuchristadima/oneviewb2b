Vue.component("tune-protect-booking", {
  data() {
    return {
      commonStore: vueCommonStore.state,
      commonAgencyCode: "AGY75",
      toEmail: "",
      policyNumber: "",
      pageObject: {
        Banner_Title: "Policy Informations",
        Banner_Image: "/Insurance/InsuranceComponents/Images/tune-protect-banner.jpg",
        Policy_Detail_Title: "Policy",
        Email_Label: "Email Insurance",
        Email_Title: "Email Ticket",
        Download_Label: "Download Insurance",
        Cancel_Label: "Cancel Policy",
        Booking_History_Title: "Booking History",
        Price_Breakup_Title: "Price Breakup",
        To_Email_Label: "EMAIL TO",
        Booking_Info_Title: "BOOKING INFORMATION",
        Ref_Number_Label: "Our Ref No:",
        Booking_Date_Label: "Booking Date:",
        Start_Date_Label: "Start Date:",
        Status_Label: "Status:",
        Policy_Number_Label: "Policy Number:",
        Destination_Label: "Destination:",
        Total_Label: "Total:",
        Payment_Mode_Label: "Payment Mode :",
        Policy_Detail_Label: "POLICY DETAILS",
        Passenger_Detail_Title: "PASSENGERS",
        Help_Support_Title: "HELP & SUPPORT",
        Help_Support_Desc: "FOR ANY QUERIES, REFER OUR FAQS",
      },
      Plan_Object: {},
      Plan_Hub_Response: { insuranceItem: { startDate: "", fare: "", schemeName: "", travelerInfoList: [] } },
    };
  },
  methods: {
    sendEmailVoucher: function () {
      var vm = this;
      if (this.toEmail == undefined || this.toEmail == "") {
        alertify.alert("Warning", "Please enter valid email to proceed.");
        return;
      }
      var status = vm.isValidEmail(this.toEmail);
      if (!status) {
        alertify.alert("Warning", "Please enter valid email to proceed.");
        return;
      }

      var emailParams = {
        fullName: vm.commonStore.agencyNode.firstName + " " + vm.commonStore.agencyNode.lastName,
        logoUrl: vm.commonStore.agencyNode.loginNode.logo,
        policyName: vm.Plan_Hub_Response.insuranceItem.schemeName,
        ourRefNo: vm.Plan_Hub_Response.bookingRef,
        bookingDate: moment(vm.Plan_Hub_Response.bookDate).format("ddd, DD MMM YY"),
        startDate: moment(vm.Plan_Hub_Response.insuranceItem.startDate).format("ddd, DD MMM YY"),
        endDate: vm.Plan_Hub_Response.insuranceItem.endDate ? moment(vm.Plan_Hub_Response.insuranceItem.endDate).format("ddd, DD MMM YY") : undefined,
        policyNumber: vm.policyNumber,
        destination: vm.Plan_Hub_Response.insuranceItem.airportCode && vm.Plan_Hub_Response.insuranceItem.airportCode.name.split(",")[1],
        total: vm.$n(vm.Plan_Hub_Response.totalFare / vm.commonStore.currencyMultiplier, "currency", vm.commonStore.selectedCurrency),
        addOns: vm.Plan_Hub_Response.insuranceItem.info,
        fullAmount: vm.$n(vm.Plan_Hub_Response.totalFare / vm.commonStore.currencyMultiplier, "currency", vm.commonStore.selectedCurrency),
        paxDetails: $("#tblBody").html(),
        voucherUrl: vm.Plan_Hub_Response.insuranceItem.travelerInfoList[0].policyUrlLink,
        agencyTitle: vm.commonStore.agencyNode.loginNode.name,
        agencyPhone:
          vm.commonStore.agencyNode.loginNode.phoneList.filter(function (tel) {
            return tel.type == "Telephone";
          })[0].number || "",
        agencyEmail: vm.commonStore.agencyNode.loginNode.email || "",
      };

      vm.getTemplate("TPInsuranceVoucherNew").then(function (templateResponse) {
        var data = templateResponse.data.data;
        var emailTemplate = "";
        if (data.length > 0) {
          for (var x = 0; x < data.length; x++) {
            if (data[x].enabled == true && data[x].type == "TPInsuranceVoucherNew") {
              emailTemplate = data[x].content;
              break;
            }
          }
        }

        var fromEmail = [{ emailId: "" }];
        try {
          fromEmail = _.filter(vueCommonStore.state.agencyNode.loginNode.emailList, function (o) {
            return o.emailTypeId == 3 && o.emailType.toLowerCase() == "support";
          });
        } catch (err) {}

        var htmlGenerate = vm.commonStore.hubUrls.emailServices.htmlGenerate;
        var emailData = {
          template: emailTemplate,
          content: emailParams,
        };
        axios
          .post(htmlGenerate, emailData)
          .then(function (htmlResponse) {
            var emailPostData = {
              type: "AttachmentRequest",
              toEmails: Array.isArray(vm.toEmail) ? vm.toEmail : [vm.toEmail],
              fromEmail: fromEmail[0].emailId || vm.commonStore.fallBackEmail,
              ccEmails: null,
              bccEmails: null,
              subject: "Your insurance policy - " + vm.policyNumber,
              attachmentPath: "",
              html: htmlResponse.data.data,
            };
            var mailUrl = vm.commonStore.hubUrls.emailServices.emailApiWithAttachment;
            sendMailService(mailUrl, emailPostData);
            $("#insuranceMail").modal("hide");
          })
          .catch(function (error) {
            alertify.alert("Error", "We have found some technical difficulties. Please contact admin!");
          });
      });
    },
    getTemplate(template) {
      var url = this.commonStore.hubUrls.emailServices.getTemplate + "AGY75/" + template;
      // var url = this.commonStore.hubUrls.emailServices.getTemplate + vueCommonStore.state.agencyNode.loginNode.code + "/" + template;
      return axios.get(url);
    },
    isValidEmail: function (emailID) {
      var status = false;
      var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
      var matchArray = emailID.match(emailPat);
      if (matchArray != null) {
        status = true;
      }
      return status;
    },
    getUrlVars: function () {
      var vars = [],
        hash;
      var hashes = window.location.href.slice(window.location.href.indexOf("?") + 1).split("&");
      for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split("=");
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
      }
      return vars;
    },
    getPagecontent: function () {
      var self = this;
      // var huburl = self.commonStore.hubUrls.hubConnection.cmsUrl;
      // var portno = self.commonStore.hubUrls.hubConnection.ipAddress;
      // var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
      // self.dir = langauage == "ar" ? "rtl" : "ltr";
      // var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + self.commonAgencyCode + '/Template/Insurance Tune Protect/Insurance Tune Protect/Insurance Tune Protect.ftl';
      var hubUrl = self.commonStore.hubUrls.hubConnection.baseUrl + self.commonStore.hubUrls.hubConnection.ipAddress;
      var serviceUrl = self.commonStore.hubUrls.hubConnection.hubServices.unitrust.insuranceDetails;

      // axios.get(cmsPage, {
      //     headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
      // }).then(function (response) {
      //     self.content = response.data;
      //     if (response.data.area_List.length > 2) {
      //         var mainComp = response.data.area_List[2].Policy_Info_Page;
      //         self.getAllMapData(mainComp.component);

      //     }
      // }).catch(function (error) {
      //     console.log(error);
      // });
      // var selectCredential = JSON.parse(window.sessionStorage.getItem('selectCredential'));
      var item = JSON.parse(window.sessionStorage.getItem("Insurance_Info"));
      self.policyNumber = item.referenceNumber;
      // window.sessionStorage.setItem("Insurance_Info", JSON.stringify(item));
      var dataRequest = {
        request: {
          service: "InsuranceUniTrustRQ",
          content: {
            command: "UniTrustDeailRQ",
            detailsRQ: {
              bookingRef: self.policyNumber,
            },
          },
          token: window.localStorage.getItem("accessToken"),
          supplierCodes: [76],
        },
      };
      axios({
        method: "POST",
        url: hubUrl + serviceUrl,
        data: dataRequest,
        headers: {
          "Content-Type": "application/json",
          Authorization: "Bearer " + window.localStorage.getItem("accessToken"),
        },
      })
        .then(response => {
          console.log("RESPONSE RECEIVED: ", response);
          self.Plan_Hub_Response = response.data.response.content.data;
        })
        .catch(function () {
          alertify.alert("Error", "We have found some technical difficulties. Please contact admin!");
        });
    },
    moment: function (date) {
      return moment(date);
    },
    async getAllMapData(contentArry) {
      var tempDataObject = {};
      contentArry.map(function (item) {
        let allKeys = Object.keys(item);
        for (let j = 0; j < allKeys.length; j++) {
          let key = allKeys[j];
          let value = item[key];

          if (key != "name" && key != "type") {
            if (value == undefined || value == null) {
              value = "";
            }
            tempDataObject[key] = value;
          }
        }
      });
      this.pageObject = tempDataObject;
    },
  },
  created: function () {
    this.getPagecontent();
  },
});
