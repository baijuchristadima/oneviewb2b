﻿var countryList = [{
        code: "AF",
        name: "Afghanistan"
    },
    {
        code: "AL",
        name: "Albania"
    },
    {
        code: "DZ",
        name: "Algeria"
    },
    {
        code: "AS",
        name: "American Samoa"
    },
    {
        code: "AD",
        name: "Andorra"
    },
    {
        code: "AO",
        name: "Angola"
    },
    {
        code: "AI",
        name: "Anguilla"
    },
    {
        code: "AQ",
        name: "Antartica"
    },
    {
        code: "AG",
        name: "Antigua And Barbuda"
    },
    {
        code: "AR",
        name: "Argentina"
    },
    {
        code: "AM",
        name: "Armenia"
    },
    {
        code: "AW",
        name: "Aruba"
    },
    {
        code: "AU",
        name: "Australia"
    },
    {
        code: "AT",
        name: "Austria"
    },
    {
        code: "AZ",
        name: "Azerbaijan"
    },
    {
        code: "BS",
        name: "Bahamas"
    },
    {
        code: "BH",
        name: "Bahrain"
    },
    {
        code: "BD",
        name: "Bangladesh"
    },
    {
        code: "BB",
        name: "Barbados"
    },
    {
        code: "BY",
        name: "Belarus"
    },
    {
        code: "BE",
        name: "Belgium"
    },
    {
        code: "BZ",
        name: "Belize"
    },
    {
        code: "BJ",
        name: "Benin"
    },
    {
        code: "BM",
        name: "Bermuda"
    },
    {
        code: "BT",
        name: "Bhutan"
    },
    {
        code: "BO",
        name: "Bolivia"
    },
    {
        code: "BQ",
        name: "Bonaire St Eustatius And Saba "
    },
    {
        code: "BA",
        name: "Bosnia-Herzegovina"
    },
    {
        code: "BW",
        name: "Botswana"
    },
    {
        code: "BR",
        name: "Brazil"
    },
    {
        code: "IO",
        name: "British Indian Ocean Territory"
    },
    {
        code: "BN",
        name: "Brunei Darussalam"
    },
    {
        code: "BG",
        name: "Bulgaria"
    },
    {
        code: "BF",
        name: "Burkina Faso"
    },
    {
        code: "BI",
        name: "Burundi"
    },
    {
        code: "KH",
        name: "Cambodia"
    },
    {
        code: "CM",
        name: "Cameroon-Republic Of"
    },
    {
        code: "CB",
        name: "Canada Buffer"
    },
    {
        code: "CA",
        name: "Canada"
    },
    {
        code: "CV",
        name: "Cape Verde-Republic Of"
    },
    {
        code: "KY",
        name: "Cayman Islands"
    },
    {
        code: "CF",
        name: "Central African Republic"
    },
    {
        code: "TD",
        name: "Chad"
    },
    {
        code: "CL",
        name: "Chile"
    },
    {
        code: "CN",
        name: "China"
    },
    {
        code: "CX",
        name: "Christmas Island"
    },
    {
        code: "CC",
        name: "Cocos Islands"
    },
    {
        code: "CO",
        name: "Colombia"
    },
    {
        code: "KM",
        name: "Comoros"
    },
    {
        code: "CG",
        name: "Congo Brazzaville"
    },
    {
        code: "CD",
        name: "Congo The Democratic Rep Of"
    },
    {
        code: "CK",
        name: "Cook Islands"
    },
    {
        code: "CR",
        name: "Costa Rica"
    },
    {
        code: "CI",
        name: "Cote D Ivoire"
    },
    {
        code: "HR",
        name: "Croatia"
    },
    {
        code: "CU",
        name: "Cuba"
    },
    {
        code: "CW",
        name: "Curacao"
    },
    {
        code: "CY",
        name: "Cyprus"
    },
    {
        code: "CZ",
        name: "Czech Republic"
    },
    {
        code: "DK",
        name: "Denmark"
    },
    {
        code: "DJ",
        name: "Djibouti"
    },
    {
        code: "DM",
        name: "Dominica"
    },
    {
        code: "DO",
        name: "Dominican Republic"
    },
    {
        code: "TP",
        name: "East Timor Former Code)"
    },
    {
        code: "EC",
        name: "Ecuador"
    },
    {
        code: "EG",
        name: "Egypt"
    },
    {
        code: "SV",
        name: "El Salvador"
    },
    {
        code: "EU",
        name: "Emu European Monetary Union"
    },
    {
        code: "GQ",
        name: "Equatorial Guinea"
    },
    {
        code: "ER",
        name: "Eritrea"
    },
    {
        code: "EE",
        name: "Estonia"
    },
    {
        code: "ET",
        name: "Ethiopia"
    },
    {
        code: "FK",
        name: "Falkland Islands"
    },
    {
        code: "FO",
        name: "Faroe Islands"
    },
    {
        code: "ZZ",
        name: "Fictitious Points"
    },
    {
        code: "FJ",
        name: "Fiji"
    },
    {
        code: "FI",
        name: "Finland"
    },
    {
        code: "FR",
        name: "France"
    },
    {
        code: "GF",
        name: "French Guiana"
    },
    {
        code: "PF",
        name: "French Polynesia"
    },
    {
        code: "GA",
        name: "Gabon"
    },
    {
        code: "GM",
        name: "Gambia"
    },
    {
        code: "GE",
        name: "Georgia"
    },
    {
        code: "DE",
        name: "Germany"
    },
    {
        code: "GH",
        name: "Ghana"
    },
    {
        code: "GI",
        name: "Gibraltar"
    },
    {
        code: "GR",
        name: "Greece"
    },
    {
        code: "GL",
        name: "Greenland"
    },
    {
        code: "GD",
        name: "Grenada"
    },
    {
        code: "GP",
        name: "Guadeloupe"
    },
    {
        code: "GU",
        name: "Guam"
    },
    {
        code: "GT",
        name: "Guatemala"
    },
    {
        code: "GW",
        name: "Guinea Bissau"
    },
    {
        code: "GN",
        name: "Guinea"
    },
    {
        code: "GY",
        name: "Guyana"
    },
    {
        code: "HT",
        name: "Haiti"
    },
    {
        code: "HN",
        name: "Honduras"
    },
    {
        code: "HK",
        name: "Hong Kong"
    },
    {
        code: "HU",
        name: "Hungary"
    },
    {
        code: "IS",
        name: "Iceland"
    },
    {
        code: "IN",
        name: "India"
    },
    {
        code: "ID",
        name: "Indonesia"
    },
    {
        code: "IR",
        name: "Iran"
    },
    {
        code: "IQ",
        name: "Iraq"
    },
    {
        code: "IE",
        name: "Ireland-Republic Of"
    },
    {
        code: "IL",
        name: "Israel"
    },
    {
        code: "IT",
        name: "Italy"
    },
    {
        code: "JM",
        name: "Jamaica"
    },
    {
        code: "JP",
        name: "Japan"
    },
    {
        code: "JO",
        name: "Jordan"
    },
    {
        code: "KZ",
        name: "Kazakhstan"
    },
    {
        code: "KE",
        name: "Kenya"
    },
    {
        code: "KI",
        name: "Kiribati"
    },
    {
        code: "KP",
        name: "Korea Dem Peoples Rep Of"
    },
    {
        code: "KR",
        name: "Korea Republic Of"
    },
    {
        code: "KW",
        name: "Kuwait"
    },
    {
        code: "KG",
        name: "Kyrgyzstan"
    },
    {
        code: "LA",
        name: "Lao Peoples Dem Republic"
    },
    {
        code: "LV",
        name: "Latvia"
    },
    {
        code: "LB",
        name: "Lebanon"
    },
    {
        code: "LS",
        name: "Lesotho"
    },
    {
        code: "LR",
        name: "Liberia"
    },
    {
        code: "LY",
        name: "Libya"
    },
    {
        code: "LI",
        name: "Liechtenstein"
    },
    {
        code: "LT",
        name: "Lithuania"
    },
    {
        code: "LU",
        name: "Luxembourg"
    },
    {
        code: "MO",
        name: "Macao -Sar Of China-"
    },
    {
        code: "MK",
        name: "Macedonia -Fyrom-"
    },
    {
        code: "MG",
        name: "Madagascar"
    },
    {
        code: "MW",
        name: "Malawi"
    },
    {
        code: "MY",
        name: "Malaysia"
    },
    {
        code: "MV",
        name: "Maldives Island"
    },
    {
        code: "ML",
        name: "Mali"
    },
    {
        code: "MT",
        name: "Malta"
    },
    {
        code: "MH",
        name: "Marshall Islands"
    },
    {
        code: "MQ",
        name: "Martinique"
    },
    {
        code: "MR",
        name: "Mauritania"
    },
    {
        code: "MU",
        name: "Mauritius Island"
    },
    {
        code: "YT",
        name: "Mayotte"
    },
    {
        code: "MB",
        name: "Mexico Buffer"
    },
    {
        code: "MX",
        name: "Mexico"
    },
    {
        code: "FM",
        name: "Micronesia"
    },
    {
        code: "MD",
        name: "Moldova"
    },
    {
        code: "MC",
        name: "Monaco"
    },
    {
        code: "MN",
        name: "Mongolia"
    },
    {
        code: "ME",
        name: "Montenegro"
    },
    {
        code: "MS",
        name: "Montserrat"
    },
    {
        code: "MA",
        name: "Morocco"
    },
    {
        code: "MZ",
        name: "Mozambique"
    },
    {
        code: "MM",
        name: "Myanmar"
    },
    {
        code: "NA",
        name: "Namibia"
    },
    {
        code: "NR",
        name: "Nauru"
    },
    {
        code: "NP",
        name: "Nepal"
    },
    {
        code: "AN",
        name: "Netherlands Antilles"
    },
    {
        code: "NL",
        name: "Netherlands"
    },
    {
        code: "NC",
        name: "New Caledonia"
    },
    {
        code: "NZ",
        name: "New Zealand"
    },
    {
        code: "NI",
        name: "Nicaragua"
    },
    {
        code: "NE",
        name: "Niger"
    },
    {
        code: "NG",
        name: "Nigeria"
    },
    {
        code: "NU",
        name: "Niue"
    },
    {
        code: "NF",
        name: "Norfolk Island"
    },
    {
        code: "MP",
        name: "Northern Mariana Islands"
    },
    {
        code: "NO",
        name: "Norway"
    },
    {
        code: "OM",
        name: "Oman"
    },
    {
        code: "PK",
        name: "Pakistan"
    },
    {
        code: "PW",
        name: "Palau Islands"
    },
    {
        code: "PS",
        name: "Palestine - State Of"
    },
    {
        code: "PA",
        name: "Panama"
    },
    {
        code: "PG",
        name: "Papua New Guinea"
    },
    {
        code: "PY",
        name: "Paraguay"
    },
    {
        code: "PE",
        name: "Peru"
    },
    {
        code: "PH",
        name: "Philippines"
    },
    {
        code: "PL",
        name: "Poland"
    },
    {
        code: "PT",
        name: "Portugal"
    },
    {
        code: "PR",
        name: "Puerto Rico"
    },
    {
        code: "QA",
        name: "Qatar"
    },
    {
        code: "RE",
        name: "Reunion Island"
    },
    {
        code: "RO",
        name: "Romania"
    },
    {
        code: "RU",
        name: "Russia"
    },
    {
        code: "XU",
        name: "Russia"
    },
    {
        code: "RW",
        name: "Rwanda"
    },
    {
        code: "WS",
        name: "Samoa-Independent State Of"
    },
    {
        code: "SM",
        name: "San Marino"
    },
    {
        code: "ST",
        name: "Sao Tome And Principe Islands "
    },
    {
        code: "SA",
        name: "Saudi Arabia"
    },
    {
        code: "SN",
        name: "Senegal"
    },
    {
        code: "RS",
        name: "Serbia"
    },
    {
        code: "SC",
        name: "Seychelles Islands"
    },
    {
        code: "SL",
        name: "Sierra Leone"
    },
    {
        code: "SG",
        name: "Singapore"
    },
    {
        code: "SX",
        name: "Sint Maarten"
    },
    {
        code: "SK",
        name: "Slovakia"
    },
    {
        code: "SI",
        name: "Slovenia"
    },
    {
        code: "SB",
        name: "Solomon Islands"
    },
    {
        code: "SO",
        name: "Somalia"
    },
    {
        code: "ZA",
        name: "South Africa"
    },
    {
        code: "SS",
        name: "South Sudan"
    },
    {
        code: "ES",
        name: "Spain"
    },
    {
        code: "LK",
        name: "Sri Lanka"
    },
    {
        code: "SH",
        name: "St. Helena Island"
    },
    {
        code: "KN",
        name: "St. Kitts"
    },
    {
        code: "LC",
        name: "St. Lucia"
    },
    {
        code: "PM",
        name: "St. Pierre And Miquelon"
    },
    {
        code: "VC",
        name: "St. Vincent"
    },
    {
        code: "SD",
        name: "Sudan"
    },
    {
        code: "SR",
        name: "Suriname"
    },
    {
        code: "SZ",
        name: "Swaziland"
    },
    {
        code: "SE",
        name: "Sweden"
    },
    {
        code: "CH",
        name: "Switzerland"
    },
    {
        code: "SY",
        name: "Syrian Arab Republic"
    },
    {
        code: "TW",
        name: "Taiwan"
    },
    {
        code: "TJ",
        name: "Tajikistan"
    },
    {
        code: "TZ",
        name: "Tanzania-United Republic"
    },
    {
        code: "TH",
        name: "Thailand"
    },
    {
        code: "TL",
        name: "Timor Leste"
    },
    {
        code: "TG",
        name: "Togo"
    },
    {
        code: "TK",
        name: "Tokelau"
    },
    {
        code: "TO",
        name: "Tonga"
    },
    {
        code: "TT",
        name: "Trinidad And Tobago"
    },
    {
        code: "TN",
        name: "Tunisia"
    },
    {
        code: "TR",
        name: "Turkey"
    },
    {
        code: "TM",
        name: "Turkmenistan"
    },
    {
        code: "TC",
        name: "Turks And Caicos Islands"
    },
    {
        code: "TV",
        name: "Tuvalu"
    },
    {
        code: "UM",
        name: "U.S. Minor Outlying Islands"
    },
    {
        code: "UG",
        name: "Uganda"
    },
    {
        code: "UA",
        name: "Ukraine"
    },
    {
        code: "AE",
        name: "United Arab Emirates"
    },
    {
        code: "GB",
        name: "United Kingdom"
    },
    {
        code: "US",
        name: "United States Of America"
    },
    {
        code: "UY",
        name: "Uruguay"
    },
    {
        code: "UZ",
        name: "Uzbekistan"
    },
    {
        code: "VU",
        name: "Vanuatu"
    },
    {
        code: "VA",
        name: "Vatican"
    },
    {
        code: "VE",
        name: "Venezuela"
    },
    {
        code: "VN",
        name: "Vietnam"
    },
    {
        code: "VG",
        name: "Virgin Islands-British"
    },
    {
        code: "VI",
        name: "Virgin Islands-United States"
    },
    {
        code: "WF",
        name: "Wallis And Futuna Islands"
    },
    {
        code: "EH",
        name: "Western Sahara"
    },
    {
        code: "YE",
        name: "Yemen Republic"
    },
    {
        code: "ZM",
        name: "Zambia"
    },
    {
        code: "ZW",
        name: "Zimbabwe"
    },
]
Vue.component('tune-protect-issue', {
    data() {
        return {
            commonStore: vueCommonStore.state,
            Agency_Info: {},
            Agency_Username: '',
            commonAgencyCode: 'AGY75',
            pageObject: {
                Banner_Title: 'Issue Policy',
                Banner_Image: '/Insurance/InsuranceComponents/Images/tune-protect-banner.jpg',
                Page_Title: 'Available Plan',
                Traveller_Label: 'Pax Information',
                Adult_Label: 'Adult (17-75)',
                Child_Label: 'Child (3-16)',
                Infant_Label: 'Infant (0-2)',
                First_Name_Place_Holder: 'First Name',
                Last_Name_Place_Holder: 'Last Name',
                Date_Of_Birth_Place_Holder: 'Date Of Birth',
                Passport_Label: 'Passport No:',
                Passport_Place_Holder: 'Passport Number',
                Other_Identity_Label: 'Identity No:',
                Other_Identity_Place_Holder: 'Identity Number',
                Title_Place_Holder: 'Title',
                Gender_Place_Holder: 'Gender',
                Nationality_Place_Holder: 'Nationality',
                Agency_Name_Label: 'Agency Name',
                User_Name_Label: 'User Name',
                Agency_Email_Label: 'Email',
                Agency_Phone_Label: 'Phone',
                Payment_Mode_Title: '',
                Credit_Label: '',
                Payment_Gateway_Label: '',
                Title_Alert: '',
                First_Name_Alert: '',
                Last_Name_Alert: '',
                Gender_Alert: '',
                Date_Of_Birth_Alert: '',
                Nationality_Alert: '',
                Passport_Alert: '',
                Other_Identity_Alert: '',
                Terms_And_Condition_Alert: '',
            },
            selectedPlan: {},
            titleList: [{
                Name: "Mr",
                value: "Mr"
            }, {
                Name: "Mrs",
                value: "Mrs"
            }, {
                Name: "Ms",
                value: "Ms"
            }],
            genderList: [{
                Name: "Male",
                value: "M"
            }, {
                Name: "Female",
                value: "F"
            }],
            countryList: countryList,
            traveller_Details: [],
            paymentType: '',
            paxDetails: {},
            Station_Details: {},
            Date_Details: {},
            sealedCode: "",
            selectCredential: {},
            isBooking: false,
            showPaymentgateway: false
        }
    },
    methods: {
        purchase: function () {
            this.isBooking = true;
            var vm = this;
            var focusID = "";
            var divID = "";
            var alertMsg = "";
            for (var i = 0; i < vm.traveller_Details.length; i++) {
                var traveller = vm.traveller_Details[i];
                divID = "div" + i;
                if (traveller.title == undefined || traveller.title == '') {
                    focusID = "title" + i;
                    alertMsg = this.pageObject.Title_Alert;
                    break;
                } else if (traveller.firstName == undefined || traveller.firstName == '') {
                    focusID = "fname" + i;
                    alertMsg = this.pageObject.First_Name_Alert;
                    break;
                } else if (traveller.lastName == undefined || traveller.lastName == '') {
                    focusID = "lname" + i;
                    alertMsg = this.pageObject.Last_Name_Alert;
                    break;
                } else if (traveller.gender == undefined || traveller.gender == '') {
                    focusID = "gender" + i;
                    alertMsg = this.pageObject.Gender_Alert;
                    break;
                } else if (traveller.dateOfBirth == undefined || traveller.dateOfBirth == '') {
                    focusID = "date" + i;
                    alertMsg = this.pageObject.Date_Of_Birth_Alert;
                    break;
                } else if (traveller.country == undefined || traveller.country == '') {
                    focusID = "nationality" + i;
                    alertMsg = this.pageObject.Nationality_Alert;
                    break;
                } else if (traveller.type == "P" && (traveller.passportNo == undefined || traveller.passportNo == '')) {
                    focusID = "passport" + i;
                    alertMsg = this.pageObject.Passport_Alert;
                    break;
                } else if (traveller.type == "O" && (traveller.otherNo == undefined || traveller.otherNo == '')) {
                    focusID = "other" + i;
                    alertMsg = this.pageObject.Other_Identity_Alert;
                    break;
                }
            }
            if (focusID != undefined && focusID != '' && alertMsg != undefined && alertMsg != '') {
                this.isBooking = false;
                alertify.alert("Warning", alertMsg, function () {
                    $('#' + focusID).focus();
                    $('html, body').animate({
                        'scrollTop': $("#" + divID).position().top
                    });
                });
                return false;
            } else {
                if (!vm.paymentType) {
                    alertify.alert("Warning", "Please choose the payment mode.", function () {});
                    this.isBooking = false;
                    return false;
                }
                else if ($("#chkAgree").prop('checked') == false) {
                    alertify.alert("Warning", this.pageObject.Terms_And_Condition_Alert, function () {});
                    this.isBooking = false;
                    return false;
                }
            }

            var hubUrls = HubServiceUrls;
            var hubUrl = hubUrls.hubConnection.baseUrl;
            var port = hubUrls.hubConnection.ipAddress;
            var tpGetCurrencyCode = hubUrls.hubConnection.hubServices.insurance.purchase;
            var data = {
                request: {
                    service: "InsuranceRQ",
                    supplierCodes: [24],
                    content: {
                        command: "TuneprotectConfirmRQ",
                        isHubOnly: vm.paymentType == 6 ? true : false,
                        purchaseInsuranceRQ: {
                            userContactDetails: {
                                countryCode: "AE"
                            },
                            insurancePurchaseRequest: {
                                pnr: moment(new Date()).format("YYYYMMDDHHSSMM"),
                                purchaseDate: moment(new Date()).format("YYYY-MM-DD HH:SS:MM"),
                                startDate: moment(this.Date_Details.Start_Date, "DD|MM|YYYY").format("YYYY-MM-DDT00:00:00"),
                                returnDate: this.Date_Details.Return_Date != "NA" ? moment(this.Date_Details.Return_Date, "DD|MM|YYYY").format("YYYY-MM-DDT00:00:00") : undefined,
                                planCode: this.selectedPlan.planCode,
                                policyName: this.selectedPlan.planTitle,
                                planContent: this.selectedPlan.planContent,
                                totalAdults: this.paxDetails.adult,
                                totalChild: this.paxDetails.children,
                                totalInfants: this.paxDetails.infant,
                                ssrFeeCode: this.selectedPlan.ssrFeeCode,
                                currency: this.selectedPlan.currencyCode,
                                totalPremium: this.selectedPlan.totalPremiumAmount,
                                contactDetails: {
                                    contactPerson: this.Agency_Username,
                                    emailAddress: this.Agency_Info.emailId
                                },
                                flights: [{
                                    departCountryCode: this.Station_Details.Dept_Country,
                                    departStationCode: this.Station_Details.Dept_City,
                                    arrivalCountryCode: this.Station_Details.Arrival_Country,
                                    arrivalStationCode: this.Station_Details.Arrival_City,
                                    departAirlineCode: this.Station_Details.airline,
                                    departDateTime: moment(this.Date_Details.Start_Date, "DD|MM|YYYY").format("YYYY-MM-DD 00:00:00"),
                                    returnDateTime: this.Date_Details.Return_Date != "NA" ? moment(this.Date_Details.Return_Date, "DD|MM|YYYY").format("YYYY-MM-DD 00:00:00") : undefined
                                }],
                                passengers: this.traveller_Details.map(function (e, i) {
                                    var passengerPremiumAmount = vm.selectedPlan.planPricingBreakdown[0].premiumAmount;

                                    if (vm.selectedPlan.planPremiumChargeType.toLowerCase() == "perbooking" && i > 0) {
                                        passengerPremiumAmount = 0
                                    }

                                    return {
                                        title: e.title,
                                        paxType: e.Object_Type == "Adult" ? "ADT" : "CHD",
                                        isInfant: e.Object_Type == "Infant" ? "1" : "0",
                                        firstName: e.firstName,
                                        lastName: e.lastName,
                                        gender: e.gender == "M" ? "MALE" : "FEMALE",
                                        dob: moment(e.dateOfBirth, "DD MMM YYYY, DDD").format("YYYY-MM-DD"),
                                        age: moment().diff(moment(e.dateOfBirth, "DD MMM YYYY, DDD"), 'years'),
                                        identityType: e.type == "P" ? "PASSPORT" : "IDENTIFICATIONCARD",
                                        identityNo: e.type == "P" ? e.passportNo : e.otherNo,
                                        isQualified: "false",
                                        nationality: e.country.code,
                                        countryOfResidence: e.country.code,
                                        selectedPlanCode: vm.selectedPlan.planCode,
                                        selectedSSRFeeCode: vm.selectedPlan.ssrFeeCode,
                                        currencyCode: vm.selectedPlan.currencyCode,
                                        passengerPremiumAmount: e.Object_Type == "Infant" ? 0 : passengerPremiumAmount
                                    }
                                })
                            },
                            sealed: vm.sealedCode,
                            paymentMode: vm.paymentType
                        }
                    },
                    selectCredential: vm.selectCredential
                }
            }
            window.sessionStorage.setItem('Insurance_Rq', JSON.stringify(data));
            axios.post(hubUrl + port + tpGetCurrencyCode, data, {
                headers: {
                    Authorization: "Bearer " + window.localStorage.getItem("accessToken")
                }
            }).then(function (response) {
                this.isBooking = false;
                window.sessionStorage.setItem('Insurance_Info', JSON.stringify({
                    referenceNumber: response.data.response.content.bookingRef
                }));
                // window.location.href = '/Insurance/TuneProtect/policyinfo.html';
                if (vm.paymentType == 7) {
                    window.location.href = '/Insurance/TuneProtect/policyinfo.html';
                } else {
                    //pg 
                    var paymentDetails = {
                        bookingReference: response.data.response.content.bookingRef,
                        cartID: "",
                        totalAmount: vm.selectedPlan.totalPremiumAmount,
                        customerEmail: JSON.parse(atob(window.localStorage.getItem("agencyNode"))).emailId,
                        currentPayGateways:vm.commonStore.agencyNode.loginNode.paymentGateways,
                    }

                    console.log("before pg", window.localStorage.getItem("accessToken"));
                    vm.paymentForm = paymentManager(paymentDetails, window.location.origin + "/Insurance/TuneProtect/policyinfo.html");
                    console.log("before pg form", window.localStorage.getItem("accessToken"));
                }
            }).catch(function (error) {
                vm.isBooking = false;
                try {
                    if (error.response.data.code == 402 && error.response.data.message) {
                        alertify.alert("Warning", error.response.data.message);
                    } else {
                        alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");
                    }
                } catch (error) {
                    alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");
                }
                console.log(error);
            });

        },
        setCalendar: function () {
            var vm = this;
            vm.$nextTick(function () {

                var tempnumberofmonths = 1;
                var dateFormat = "dd M yy, D";
                // dd M y,D

                if (parseInt($(window).width()) > 500 && parseInt($(window).width()) <= 999) {
                    tempnumberofmonths = 2;
                } else if (parseInt($(window).width()) > 999) {
                    tempnumberofmonths = 3;
                }
                for (var i = 0; i < vm.traveller_Details.length; i++) {

                    $("#date" + i).datepicker({
                        minDate: "0",
                        maxDate: "0",
                        numberOfMonths: 1,
                        changeMonth: true,
                        changeYear: true,
                        dateFormat: dateFormat,
                        showButtonPanel: false,
                        onSelect: function (selectedDate) {
                            var dateID = parseInt(this.id.replace('date', ''));
                            var selectedObject = vm.traveller_Details[dateID];
                            selectedObject.dateOfBirth = $("#" + this.id).val();
                            var age = moment().diff(moment($("#" + this.id).val(), "DD MMM YYYY, DDD"), 'years');
                            if ((age < vm.selectedPlan.planPricingBreakdown[0].minAge ||
                                    age > vm.selectedPlan.planPricingBreakdown[0].maxAge) && selectedObject.Object_Type != "Infant") {
                                alertify.alert("Please note", "The policy allows passenger age between " + vm.selectedPlan.planPricingBreakdown[0].minAge + " years to " + vm.selectedPlan.planPricingBreakdown[0].maxAge + " years only.")
                            }
                        },
                        beforeShow: function () {
                            var dateID = parseInt(this.id.replace('date', ''));
                            var selectedObject = vm.traveller_Details[dateID];
                            var Object_Type = selectedObject.Object_Type;
                            var startDate = new Date();
                            var addyears = 1;
                            var addEndYears = 0;
                            if (Object_Type == 'Adult') {
                                addyears = -75;
                                addEndYears = -17;
                                $(this).datepicker('option', 'yearRange', "-75:+0");
                            } else if (Object_Type == 'Child') {
                                addyears = -16;
                                addEndYears = -3;
                                $(this).datepicker('option', 'yearRange', "-16:+0");
                            } else if (Object_Type == 'Infant') {
                                addyears = -2;
                            }
                            startDate.setFullYear(startDate.getFullYear() + addyears);
                            $(this).datepicker('option', 'minDate', startDate);
                            $(this).datepicker("option", "maxDate", addEndYears + "y");
                            console.log(selectedObject);
                        }
                    });

                }

            });
        },
        loadAllTravellerDetails: function (count, type) {
            for (var i = 0; i < parseInt(count); i++) {
                var travellerObject = {
                    Object_Label: '',
                    Object_Type: type,
                    title: "",
                    firstName: '',
                    lastName: '',
                    gender: "",
                    dateOfBirth: '',
                    country: "",
                    type: 'P',
                    passportNo: '',
                    otherNo: ''
                };
                if (type == 'Adult') {
                    travellerObject.Object_Label = this.pageObject.Adult_Label;
                } else if (type == 'Child') {
                    travellerObject.Object_Label = this.pageObject.Child_Label;
                } else {
                    travellerObject.Object_Label = this.pageObject.Infant_Label;
                }
                this.traveller_Details.push(travellerObject);
            }

        },
        getPagecontent: function () {
            var self = this;
            var huburl = self.commonStore.hubUrls.hubConnection.cmsUrl;
            var portno = self.commonStore.hubUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            self.dir = langauage == "ar" ? "rtl" : "ltr";
            var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + self.commonAgencyCode + '/Template/Insurance Tune Protect/Insurance Tune Protect/Insurance Tune Protect.ftl';
            console.log(cmsPage);
            axios.get(cmsPage, {
                headers: {
                    'content-type': 'text/html',
                    'Accept': 'text/html',
                    'Accept-Language': langauage
                }
            }).then(function (response) {
                self.content = response.data;
                if (response.data.area_List.length > 1) {
                    var mainComp = response.data.area_List[1].Issuance_Page;
                    self.getAllMapData(mainComp.component);


                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        async getAllMapData(contentArry) {
            var tempDataObject = {};
            contentArry.map(function (item) {
                let allKeys = Object.keys(item)
                for (let j = 0; j < allKeys.length; j++) {
                    let key = allKeys[j];
                    let value = item[key];

                    if (key != 'name' && key != 'type') {
                        if (value == undefined || value == null) {
                            value = "";
                        }
                        tempDataObject[key] = value;
                    }
                }
            });
            this.pageObject = tempDataObject;

        },
        paymentGatewayButton: function(){
            var self = this;
            if (self.commonStore.agencyNode.loginNode.nodetype !="HQ"){
                self.getHQPaymentGateway();
            } else {
                self.creditCardButton(self.commonStore.agencyNode.loginNode.paymentGateways);
            }
        },
        creditCardButton:function(paymetGateways){
            var self = this;
            if(paymetGateways){
            var paymentMethods = paymetGateways.filter(function (item) { return item.id == 2; });
            if (self.commonStore.commonRoles.hasInsuranceTPPaymentGateway && paymentMethods) {
                self.showPaymentgateway = true;
            }
        }
        else{
            self.showPaymentgateway = false;
            self.paymentType = 7;
        }
        },
        getHQPaymentGateway: function () {
            var vm = this;
            var hqNodeId = vm.commonStore.agencyNode.loginNode.solutionId;
            if (hqNodeId){
      
                var huburl = vm.commonStore.hubUrls.hubConnection.baseUrl;
                var portno = vm.commonStore.hubUrls.hubConnection.ipAddress;
                var serviceUrl = vm.commonStore.hubUrls.hubConnection.hubServices.paymentGatewayHQ;
      
                var config = {
                    axiosConfig: {
                        method: "GET",
                        url: huburl + portno + serviceUrl + hqNodeId
                    },
                    successCallback: function (response) {
                        try {
                            
                            if (response.data){
                                console.log("HQ payment gateways : ", response.data);
                                vm.creditCardButton(response.data);
                            }
                           
                        } catch (err) {
                            console.log('no payment gateway found in hqnode');
                        }
                    },
                    errorCallback: function (error) { },
                    showAlert: false
                };
      
                mainAxiosRequest(config);
            }
        }
    },
    created: function () {
        this.getPagecontent();
        window.sessionStorage.setItem("sendIssueMail", false);
        this.Agency_Info = JSON.parse(atob(window.localStorage.getItem("agencyNode")));
        this.Agency_Username = this.Agency_Info.firstName + ' ' + this.Agency_Info.lastName;
        var selectInsuranceFromSession = localStorage.selectedInsurance;
        selectInsuranceFromSession = JSON.parse(selectInsuranceFromSession);
        this.selectedPlan = selectInsuranceFromSession.Plan_Details;
        var paxDetails = selectInsuranceFromSession.Search_Details.Pax_Details;
        this.paxDetails = paxDetails;
        this.Station_Details = selectInsuranceFromSession.Search_Details.Station_Details;
        this.Date_Details = selectInsuranceFromSession.Search_Details.Date_Details;
        this.sealedCode = selectInsuranceFromSession.sealedCode;
        this.selectCredential = selectInsuranceFromSession.credential;
        window.sessionStorage.setItem("selectCredential", JSON.stringify(this.selectCredential));
        this.loadAllTravellerDetails(paxDetails.adult, 'Adult');
        this.loadAllTravellerDetails(paxDetails.children, 'Child');
        this.loadAllTravellerDetails(paxDetails.infant, 'Infant');
        this.setCalendar();
        setTimeout(() => {
            this.paymentGatewayButton()
        }, 1000);
    }
});