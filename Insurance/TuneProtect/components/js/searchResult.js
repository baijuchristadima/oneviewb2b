Vue.component('tune-protect-result', {
    //for websocket
    mixins: [websocketMixin],
    data() {
        return {
            commonStore: vueCommonStore.state,
            checked: false,
            commonAgencyCode: 'AGY75',
            pageObject: {
                Banner_Title: 'Tune Protect',
                Banner_Image: '/Insurance/InsuranceComponents/Images/tune-protect-banner.jpg',
                Quotation_Title: 'Quotations',
                Remove_Quotation_Title: 'Remove',
                Send_Quotation_Title: 'Send',
                Add_Quotation_Title: 'Add Quote',
                Select_Policy_Title: 'Select',
                Available_Plan_Title: 'Available Plans',
                Available_Upsell_Plan_Title: 'Available Upsell Plans',
                Cancel_Policy_Title: 'Cancel',
                Issue_Policy_Title: 'Issue Policy',
                Remove_Quotation_Success: 'Selected quotation removed successfully.',
                Remove_Quotation_Warning: 'Please choose atleast one quotation.',
                Send_Quotation_Warning: 'Please choose atleast one quotation.',
                Email_Subject_Alert: 'Please enter subject to proceed.',
                Email_To_Alert: 'Please enter valid receiver email to proceed.',

            },
            Quotation_List: [],
            Available_Plan_List: [],
            Available_Upsel_Plan_List: [],
            Search_Details: {},
            quotation_Content: '',
            quotationMailObject: { subject: '', toEmail: '', ccEmail: '' },
            selectedPlan: {},
            showLoaders: true,
            sealedCode: "",
            selectCredential: {},
            sortValues: [
                { isActive: true, sortBy: "Price", isAscending: false },
                { isActive: false, sortBy: "Name", isAscending: false },
            ],
        }
    },
    methods: {
        selectPlan: function (selectedPlan) {
            this.selectedPlan = selectedPlan;
            $('#insuranceplan').modal('show');
        },
        issuePolicy: function (selectedPlan) {
            var selectInsuranceRequest = { Search_Details: this.Search_Details, Plan_Details: selectedPlan, sealedCode: this.sealedCode, credential: this.selectCredential};
            localStorage.selectedInsurance = JSON.stringify(selectInsuranceRequest);
            window.location.href = '/Insurance/TuneProtect/IssuePolicy.html'; 
        },
        loadAvailblePlans: function (data) {
            if (data.availablePlans.length > 0 || data.availableUpsellPlans.length > 0) {
                this.showLoaders = false;
                this.Available_Plan_List = data.availablePlans.sort(function(a, b) {
                    return a.totalPremiumAmount - b.totalPremiumAmount;
                });
                this.Available_Upsel_Plan_List = data.availableUpsellPlans.sort(function(a, b) {
                    return a.upsellPlans[0].totalPremiumAmount - b.upsellPlans[0].totalPremiumAmount;
                });
            }
            // var planObject = {
            //     policy_title: data.planAdditionalInfoTitle, 
            //     amount: '700', 
            //     image: '/Insurance/InsuranceComponents/Images/TA.jpg', 
            //     plan_description:
            //         ' <table width="100%" cellspacing="0" cellpadding="3" border="0" style="font-family: Tahoma, Arial, Sans-Serif; font-size: 1em;"><tbody><tr><td colspan="2"><b><u>Travel Assurance is important, dont you think so?</u></b></td></tr><tr>' +
            //         '<td colspan="2" height="5px"></td></tr> <tr> <td colspan="2">Travelling with a peace of mind simply means your entire journey is protected with Tune Protect Travel Assurance - Gold Plan.<br>From USD 21.00 per person, you will be covered with the following:</td>' +
            //         '</tr><tr><td colspan="2" height="5px"></td></tr><tr><td width="3%" align="center">❱</td> <td width="97%"><b>USD 100,000</b> for Accidental and Sickness Medical Reimbursement</td> </tr>' +
            //         '<tr><td width="3%" align="center">❱</td><td width="97%"><b>USD 100,000</b> for Emergency Medical Evacuation (Subject to Accidental and Medical Reimbursement Limit)</td></tr><tr><td width="3%" align="center">❱</td>' +
            //         '<td width="97%"><b>USD 30,000</b> for Accidental Death and Permanent Disablement</td></tr><tr><td colspan="2">and more...</td></tr> <tr><td colspan="2" height="5px"></td></tr><tr><td colspan="2" style="font-size:0.8em">Tune Protect Travel Assurance - Gold Plan is underwritten by a local insurance company. Click <a href="http://www.tune2protect.com/travelassurance/" target="_blank" style="color:blue;text-decoration:underline;">here</a>                                            to view the terms and conditions</td>' +
            //         '</tr> </tbody></table>'
            // };
            // var upselplan = JSON.parse(JSON.stringify(planObject));
            // upselplan.policy_title = "Travel Adventure Assurance";
            // upselplan.amount = "825";
            // upselplan.image = "/Insurance/InsuranceComponents/Images/TAA.jpg";
            // // this.Available_Plan_List.push(planObject);
            // this.Available_Upsel_Plan_List.push(upselplan);
        },
        removeQuotation: function () {
            var selectedQuotation = this.Quotation_List.filter(function (el) {
                return el.checked == true;
            });
            if (selectedQuotation.length > 0) {
                this.Quotation_List = this.Quotation_List.filter(function (el) {
                    return el.checked == false;
                });
                alertify.alert("Success", this.pageObject.Remove_Quotation_Success);
            } else {
                alertify.alert("Warning", this.pageObject.Remove_Quotation_Warning);
            }
        },
        addQuotation: function (Available_Plan) {
            var QuotationObject = JSON.parse(JSON.stringify(Available_Plan));
            QuotationObject.checked = false;
            this.Quotation_List.push(QuotationObject);
        },
        quotationLblClick: function (Quotation) {
            Quotation.checked = Quotation.checked ? false : true;
        },
        sendQuotation: function () {
            var selectedQuotation = this.Quotation_List.filter(function (el) {
                return el.checked == true;
            });
            var vm = this;
            vm.quotation_Content = "";
            vm.quotationMailObject = { subject: '', toEmail: '', ccEmail: '' };
            if (selectedQuotation.length > 0) {
                selectedQuotation.forEach(element => {
                    var plans = '<div class="insurance_card"><div class="coverage_head"><div class="col-lg-8 col-md-8 col-xs-8"><h4>' + 
                    element.planTitle + '</h4></div><div class="col-lg-4 col-md-4 col-xs-4 text-right"> <h6>' + 
                        vm.commonStore.agencyNode.loginNode.currency + ' ' +
                        element.totalPremiumAmount + '</div></div><div class="insuranceSec"><div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 nopadding"><span>' + 
                        element.planContent + '</span></div></div></div><br>';
                    vm.quotation_Content = vm.quotation_Content + plans;
                });
                $('#voucherHtmlEdit').summernote('code', vm.quotation_Content);
                $('#sendQuotationModel').modal('show');
            } else {
                alertify.alert("Warning", this.pageObject.Send_Quotation_Warning);
            }
        },
        sendEmailQuotation: function () {
            var vm = this;
            if (this.quotationMailObject.subject == undefined || this.quotationMailObject.subject == '') {
                alertify.alert("Warning", this.pageObject.Email_Subject_Alert);
                return;
            } else if (this.quotationMailObject.toEmail == undefined || this.quotationMailObject.toEmail == '') {
                alertify.alert("Warning", this.pageObject.Email_To_Alert);
                return;
            }
            var allToEmails = this.quotationMailObject.toEmail.split(';');
            var toEmailStatus = "";
            allToEmails.forEach(element => {
                var status = vm.isValidEmail(element);
                if (!status) {
                    toEmailStatus = vm.pageObject.Email_To_Alert;
                    return;
                }
            });
            if (toEmailStatus != '') {
                alertify.alert("Warning", toEmailStatus);
                return;
            }
            if (this.quotationMailObject.ccEmail != undefined && this.quotationMailObject.ccEmail != '') {
                var allCCEmails = this.quotationMailObject.ccEmail.split(';');
                allCCEmails.forEach(element => {

                    var status = vm.isValidEmail(element);
                    if (!status) {
                        toEmailStatus = vm.pageObject.Email_To_Alert;
                        return;
                    }
                });
                if (toEmailStatus != '') {
                    alertify.alert("Warning", toEmailStatus);
                    return;
                }
            }


        },
        isValidEmail: function (emailID) {
            var status = false;
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = emailID.match(emailPat);
            if (matchArray != null) { status = true; }
            return status;
        },
        loadAllResults: function () {
            var urldata = this.getUrlVars().insurance;
            if (urldata == undefined) {
                window.location.href = 'index.html';
            } else {
                var allDatas = urldata.split('--');
                if (allDatas.length > 2) {
                    var countryDetails = allDatas[0];
                    var dateDetails = allDatas[1];
                    var paxDetails = allDatas[2];
                    var airline = allDatas[3];
                    var departureCountry = countryDetails.split('-')[0];
                    var departureCity = countryDetails.split('-')[1];
                    var arrivalCountry = countryDetails.split('-')[2];
                    var arrivalCity = countryDetails.split('-')[3];
                    var departureDate = dateDetails.split('-')[0];
                    var returnDate = dateDetails.split('-')[1] || "NA";
                    var adult = paxDetails.split('|')[0];
                    var children = paxDetails.split('|')[1];
                    var infant = paxDetails.split('|')[2];

                    var travelpaxDetails = { adult: adult, children: children, infant: infant };
                    var traveldateDetails = { Start_Date: departureDate, Return_Date: returnDate };
                    var stationDetails = { Dept_Country: departureCountry, Dept_City: departureCity, Arrival_Country: arrivalCountry, Arrival_City: arrivalCity, airline: airline };
                    this.Search_Details = { Pax_Details: travelpaxDetails, Date_Details: traveldateDetails, Station_Details: stationDetails };
                    var hubUrls = HubServiceUrls;
                    var hubUrl = hubUrls.hubConnection.baseUrl;
                    var port = hubUrls.hubConnection.ipAddress;
                    var tpGetCurrencyCode = hubUrls.hubConnection.hubServices.insurance.tpGetCurrencyCode;

                    axios.get(hubUrl + port + tpGetCurrencyCode, {
                        headers: { Authorization: "Bearer " + window.localStorage.getItem("accessToken") }
                    }).then(function (response) {
                        var allCountries = response.data;
                        var selectedQuotation = allCountries.filter(function (el) {
                            return el.code == departureCountry;
                        });
                        if (selectedQuotation.length > 0 && selectedQuotation[0].currency.length > 0) {
                            var selectedCountryObj = selectedQuotation[0].currency[0].code;
                            console.log(selectedCountryObj);
                        }

                    }).catch(function (error) {
                        console.log(error);
                    });

                    var searchRQ = {
                        request: {
                            service: "InsuranceRQ",
                            supplierCodes: [24],
                            token: window.localStorage.getItem("accessToken"),
                            content: {
                                command: "TuneprotectSearchRQ",
                                criteria: {
                                    criteriaType: "TuneprotectInsurance",
                                    departCountryCode: departureCountry,
                                    departStationCode: departureCity,
                                    departDateTime: moment(departureDate, "DD|MM|YYYY").format("YYYY-MM-DD 00:00:00"),
                                    arrivalCountryCode: arrivalCountry,
                                    arrivalStationCode: arrivalCity,
                                    returnDateTime: returnDate != "NA" ? moment(returnDate, "DD|MM|YYYY").format("YYYY-MM-DD 00:00:00") : undefined,
                                    departAirlineCode: airline || "",
                                    totalAdults: adult,
                                    totalChild: children,
                                    totalInfants: infant
                                }
                            }
                        }
                    }
                    this.connect(); //open websocket connection           

                    this.sendMessage(JSON.stringify(searchRQ));
                    var vm = this;
                    setTimeout(function () {
                        vm.disconnect();
                    }, 90000);
                } else {
                    window.location.href = 'index.html';
                }
            }
        },
        processInsuranceResult: function (response) {
            this.sealedCode = response.response.content.data.sealed;
            this.selectCredential = response.response.selectCredential;
            this.loadAvailblePlans(response.response.content.data);
        },
        getUrlVars: function () {
            var vars = [],
                hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        },
        getPagecontent: function () {
            var self = this;
            var huburl = self.commonStore.hubUrls.hubConnection.cmsUrl;
            var portno = self.commonStore.hubUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            self.dir = langauage == "ar" ? "rtl" : "ltr";
            var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + self.commonAgencyCode + '/Template/Insurance Tune Protect/Insurance Tune Protect/Insurance Tune Protect.ftl';
            console.log(cmsPage);
            axios.get(cmsPage, {
                headers: { 'content-type': 'text/html', 'Accept': 'text/html', 'Accept-Language': langauage }
            }).then(function (response) {
                self.content = response.data;
                if (response.data.area_List.length > 0) {
                    var mainComp = response.data.area_List[0].Search_Result;
                    self.getAllMapData(mainComp.component);


                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        async getAllMapData(contentArry) {
            var tempDataObject = {};
            contentArry.map(function (item) {
                let allKeys = Object.keys(item)
                for (let j = 0; j < allKeys.length; j++) {
                    let key = allKeys[j];
                    let value = item[key];

                    if (key != 'name' && key != 'type') {
                        if (value == undefined || value == null) {
                            value = "";
                        }
                        tempDataObject[key] = value;
                    }
                }
            });
            this.pageObject = tempDataObject;

        },
        addQuotationToCurrentCart: function (insurance, index) {
            var vm = this;
            var uuid = insurance.planCode + "|" + insurance.ssrFeeCode + "|" + index + "|insurance";
            var insuranceQuotation = this.commonStore.selectedCart.services.insuranceTP;

            if (typeof insuranceQuotation == "undefined") {
                insuranceQuotation = [];
            }
            if (insuranceQuotation.findIndex(function (x) { return x.serialNumber == uuid; }) == -1) {
                if (typeof vm.commonStore.selectedCart.services.insuranceTP == "undefined") {
                    vm.commonStore.selectedCart.services.insuranceTP = [];
                }
                vm.commonStore.selectedCart.services.insuranceTP.push({
                    serialNumber: uuid,
                    planTitle: insurance.planTitle,
                    totalPremiumAmount: insurance.totalPremiumAmount,
                    planContent: insurance.planContent
                });

                for (var k = 0; k < vm.commonStore.carts.length; k++) {
                    if (vm.commonStore.carts[k].id == vm.commonStore.selectedCart.id) {
                        vm.commonStore.carts[k].services.insuranceTP = vm.commonStore.selectedCart.services.insuranceTP;
                        break;
                    }
                }
                var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;
                var serviceUrl = this.commonStore.hubUrls.hubConnection.hubServices.addQuote;

                var config = {
                    axiosConfig: {
                        method: "post",
                        url: hubUrl + serviceUrl,
                        data: {
                            content: JSON.stringify(vm.commonStore.carts)
                        }
                    },
                    successCallback: function (response) {
                        if (response) {
                            vm.getAllquotationsOnServer();
                            // alertify.alert("Success", "Added to quotaion " + vm.commonStore.selectedCart.name);
                        }
                    },
                    errorCallback: function (error) {
                        alertify.alert("Error", "Error unable to add quotation.");
                    },
                    showAlert: true
                };

                mainAxiosRequest(config);
            } else {
                alertify.alert("Please note", "Quotation already added.");
            }


        },
        getAllquotationsOnServer: function () {
            var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = this.commonStore.hubUrls.hubConnection.hubServices.pendingQuoteDetails;
            var vm = this;

            var config = {
                axiosConfig: {
                    method: "get",
                    url: hubUrl + serviceUrl
                },
                successCallback: function (response) {
                    if (response && response.data.data[0].content) {
                        vm.commonStore.carts = JSON.parse(response.data.data[0].content);
                    } else {
                        vm.commonStore.carts = [
                            {
                                id: uuidv4(),
                                name: "Default",
                                services: {
                                    flights: [],
                                    hotels: [],
                                    insuranceTP: [],
                                    insuranceWIS: []
                                }
                            }
                        ];
                    }
                },
                errorCallback: function (error) {
                    vm.commonStore.carts = [
                        {
                            id: uuidv4(),
                            name: "Default",
                            services: {
                                flights: [],
                                hotels: [],
                                insuranceTP: [],
                                insuranceWIS: []
                            }
                        }
                    ];
                },
                showAlert: false
            };

            mainAxiosRequest(config);
        },
        sortResults: function(sortType) {
            var currentId = sortType.sortBy;
            var index = _.findIndex(this.sortValues, function(e) { return e.sortBy == currentId })
            var item = this.sortValues[index];
            this.sortValues = [
                { isActive: false, sortBy: "Price", isAscending: true },
                { isActive: false, sortBy: "Name", isAscending: true },
            ];
            this.$set(this.sortValues, index, { isActive: true, sortBy: item.sortBy, isAscending: !item.isAscending });

            switch (currentId) {
                case 'Price':
                    this.Available_Plan_List.sort(function(a, b) {
                        return  item.isAscending ? parseFloat(a.totalPremiumAmount) - parseFloat(b.totalPremiumAmount) 
                        : parseFloat(b.totalPremiumAmount) - parseFloat(a.totalPremiumAmount);
                    });
                    this.Available_Upsel_Plan_List.sort(function(a, b) {
                        return item.isAscending ? parseFloat(a.upsellPlans[0].totalPremiumAmount) - parseFloat(b.upsellPlans[0].totalPremiumAmount) 
                        : parseFloat(b.upsellPlans[0].totalPremiumAmount) - parseFloat(a.upsellPlans[0].totalPremiumAmount);
                    });
                    break;
                case 'Name':
                    this.Available_Plan_List.sort(function(a, b) {
                        return  item.isAscending ? a.planTitle.localeCompare(b.planTitle) : b.planTitle.localeCompare(a.planTitle);
                    });
                    this.Available_Upsel_Plan_List.sort(function(a, b) {
                        return item.isAscending ? a.upsellPlans[0].planTitle.localeCompare(b.upsellPlans[0].planTitle) : b.upsellPlans[0].planTitle.localeCompare(a.upsellPlans[0].planTitle);
                    });
                    break;
                default:
                    break;
            }
        },
    },
    created: function () {
        this.getPagecontent();
        this.loadAllResults();
        $('#voucherHtmlEdit').summernote({
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontstyle', ['fontsize', 'fontname', 'color']],
                ['para', ['ul', 'ol', 'paragraph', 'height']],
                ['Edit', ['undo', 'redo']],
                ['insert', ['table', 'hr']],
                ['help', ['codeview', 'help']]
            ]
        });
    }
});