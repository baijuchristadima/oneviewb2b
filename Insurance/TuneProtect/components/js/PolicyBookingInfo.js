Vue.component('tune-protect-booking', {
    data() {
        return {
            commonStore: vueCommonStore.state,
            commonAgencyCode: 'AGY75',
            toEmail: '',
            policyNumber: '',
            pageObject: {
                Banner_Title: 'Policy Informations',
                Banner_Image: '/Insurance/InsuranceComponents/Images/tune-protect-banner.jpg',
                Policy_Detail_Title: 'Policy',
                Email_Label: 'Email Insurance',
                Email_Title: 'Email Ticket',
                Download_Label: 'Download Insurance',
                Cancel_Label: 'Cancel Policy',
                Booking_History_Title: 'Booking History',
                Price_Breakup_Title: 'Price Breakup',
                To_Email_Label: 'EMAIL TO',
                Booking_Info_Title: 'BOOKING INFORMATION',
                Ref_Number_Label: 'Our Ref No:',
                Booking_Date_Label: 'Booking Date:',
                Start_Date_Label: 'Start Date:',
                Status_Label: 'Status:',
                Policy_Number_Label: 'Policy Number:',
                Destination_Label: 'Destination:',
                Total_Label: 'Total:',
                Payment_Mode_Label: 'Payment Mode :',
                Policy_Detail_Label: 'POLICY DETAILS',
                Passenger_Detail_Title: 'PASSENGERS',
                Help_Support_Title: 'HELP & SUPPORT',
                Help_Support_Desc: 'FOR ANY QUERIES, REFER OUR FAQS'

            },
            Plan_Object: {},
            Plan_Hub_Response: {
                insuranceItem: {
                    startDate: '',
                    fare: '',
                    schemeName: '',
                    travelerInfoList: []
                }
            },
            sendmailstatus: true,
            Agency_Info: {},
            InsuranceRq: {},

        }
    },
    methods: {
        loadAvailblePlans: function () {
            this.Plan_Object = {
                policy_title: 'Travel Assurance - Gold Plan',
                amount: '700',
                image: '/Insurance/InsuranceComponents/Images/TA.jpg',
                plan_description: ' <table width="100%" cellspacing="0" cellpadding="3" border="0" style="font-family: Tahoma, Arial, Sans-Serif; font-size: 1em;"><tbody><tr><td colspan="2"><b><u>Travel Assurance is important, dont you think so?</u></b></td></tr><tr>' +
                    '<td colspan="2" height="5px"></td></tr> <tr> <td colspan="2">Travelling with a peace of mind simply means your entire journey is protected with Tune Protect Travel Assurance - Gold Plan.<br>From USD 21.00 per person, you will be covered with the following:</td>' +
                    '</tr><tr><td colspan="2" height="5px"></td></tr><tr><td width="3%" align="center">❱</td> <td width="97%"><b>USD 100,000</b> for Accidental and Sickness Medical Reimbursement</td> </tr>' +
                    '<tr><td width="3%" align="center">❱</td><td width="97%"><b>USD 100,000</b> for Emergency Medical Evacuation (Subject to Accidental and Medical Reimbursement Limit)</td></tr><tr><td width="3%" align="center">❱</td>' +
                    '<td width="97%"><b>USD 30,000</b> for Accidental Death and Permanent Disablement</td></tr><tr><td colspan="2">and more...</td></tr> <tr><td colspan="2" height="5px"></td></tr><tr><td colspan="2" style="font-size:0.8em">Tune Protect Travel Assurance - Gold Plan is underwritten by a local insurance company. Click <a href="http://www.tune2protect.com/travelassurance/" target="_blank" style="color:blue;text-decoration:underline;">here</a>                                            to view the terms and conditions</td>' +
                    '</tr> </tbody></table>',
                url: 'http://uat1.tuneprotect.com/ZEUSCONFIG2/ZEUS_Process_GeneratePolicyB2B.aspx?IID=0&PNO=T2P-2019-AEAPI-0000879'
            };


        },
        sendEmailFunction: function () {
            var vm = this;
            if (this.toEmail == undefined || this.toEmail == '') {
                alertify.alert("Warning", "Please enter valid email to proceed.");
                return;
            }
            var status = vm.isValidEmail(this.toEmail);
            if (!status) {
                alertify.alert("Warning", "Please enter valid email to proceed.");
                return;
            }
            this.sendEmailVoucher(vm.toEmail);
        },
        sendEmailVoucher: function (mailid) {
            var vm = this;

            var emailParams = {
                fullName: vm.commonStore.agencyNode.firstName + " " + vm.commonStore.agencyNode.lastName,
                logoUrl: vm.commonStore.agencyNode.loginNode.logo,
                policyName: vm.Plan_Hub_Response.insuranceItem.schemeName,
                ourRefNo: vm.Plan_Hub_Response.bookingRef,
                bookingDate: moment(vm.Plan_Hub_Response.bookDate).format('ddd, DD MMM YY'),
                startDate: moment(vm.Plan_Hub_Response.insuranceItem.startDate).format('ddd, DD MMM YY'),
                endDate: vm.Plan_Hub_Response.insuranceItem.endDate ? moment(vm.Plan_Hub_Response.insuranceItem.endDate).format('ddd, DD MMM YY') : undefined,
                policyNumber: vm.policyNumber,
                destination: vm.Plan_Hub_Response.insuranceItem.airportCode && vm.Plan_Hub_Response.insuranceItem.airportCode.name.split(',')[1],
                total: vm.$n(vm.Plan_Hub_Response.totalFare / vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency),
                addOns: vm.Plan_Hub_Response.insuranceItem.info,
                fullAmount: vm.$n(vm.Plan_Hub_Response.totalFare / vm.commonStore.currencyMultiplier, 'currency', vm.commonStore.selectedCurrency),
                paxDetails: $("#tblBody").html(),
                voucherUrl: vm.Plan_Hub_Response.insuranceItem.travelerInfoList[0].policyUrlLink,
                agencyTitle: vm.commonStore.agencyNode.loginNode.name,
                agencyPhone: vm.commonStore.agencyNode.loginNode.phoneList.filter(function (tel) {
                    return tel.type == "Telephone"
                })[0].number || "",
                agencyEmail: vm.commonStore.agencyNode.loginNode.email || "",
            };

            vm.getTemplate("TPInsuranceVoucherNew").then(function (templateResponse) {
                var data = templateResponse.data.data;
                var emailTemplate = "";
                if (data.length > 0) {
                    for (var x = 0; x < data.length; x++) {
                        if (data[x].enabled == true && data[x].type == "TPInsuranceVoucherNew") {
                            emailTemplate = data[x].content;
                            break;
                        }
                    }
                };

                var fromEmail = [{
                    emailId: ""
                }];
                try {
                    fromEmail = _.filter(vueCommonStore.state.agencyNode.loginNode.emailList,
                        function (o) {
                            return o.emailTypeId == 3 && o.emailType.toLowerCase() == "support";
                        });
                } catch (err) {}

                var htmlGenerate = vm.commonStore.hubUrls.emailServices.htmlGenerate
                var emailData = {
                    template: emailTemplate,
                    content: emailParams
                };
                var ccEmails = null;
                ccEmails = _.filter(vm.commonStore.agencyNode.loginNode.emailList,
                    function (o) {
                        return o.emailId != undefined && o.emailTypeId == 11
                    });
                ccEmails = _.map(ccEmails, function (o) {
                    return o.emailId;
                });
                axios.post(htmlGenerate, emailData)
                    .then(function (htmlResponse) {
                        var emailPostData = {
                            type: "AttachmentRequest",
                            toEmails: Array.isArray(mailid) ? mailid : [mailid],
                            fromEmail: fromEmail[0].emailId || vm.commonStore.fallBackEmail,
                            ccEmails: ccEmails,
                            bccEmails: null,
                            subject: "Your insurance policy - " + vm.policyNumber,
                            attachmentPath: "",
                            html: htmlResponse.data.data
                        };
                        var mailUrl = vm.commonStore.hubUrls.emailServices.emailApiWithAttachment;
                        sendMailService(mailUrl, emailPostData);
                        $('#insuranceMail').modal('hide');

                    }).catch(function (error) {
                        alertify.alert('Error', 'We have found some technical difficulties. Please contact admin!');
                    })
            })

        },
        getTemplate(template) {
            var url = this.commonStore.hubUrls.emailServices.getTemplate + "AGY75/" + template;
            // var url = this.commonStore.hubUrls.emailServices.getTemplate + vueCommonStore.state.agencyNode.loginNode.code + "/" + template;
            return axios.get(url);
        },
        isValidEmail: function (emailID) {
            var status = false;
            var emailPat = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
            var matchArray = emailID.match(emailPat);
            if (matchArray != null) {
                status = true;
            }
            return status;
        },
        getUrlVars: function () {
            var vars = [],
                hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        },
        getPagecontent: function () {
            var self = this;
            var huburl = self.commonStore.hubUrls.hubConnection.cmsUrl;
            var portno = self.commonStore.hubUrls.hubConnection.ipAddress;
            var langauage = (localStorage.Languagecode) ? localStorage.Languagecode : "en";
            self.dir = langauage == "ar" ? "rtl" : "ltr";
            var cmsPage = huburl + portno + '/persons/source?path=/B2B/AdminPanel/CMS/' + self.commonAgencyCode + '/Template/Insurance Tune Protect/Insurance Tune Protect/Insurance Tune Protect.ftl';


            axios.get(cmsPage, {
                headers: {
                    'content-type': 'text/html',
                    'Accept': 'text/html',
                    'Accept-Language': langauage
                }
            }).then(function (response) {
                self.content = response.data;
                if (response.data.area_List.length > 2) {
                    var mainComp = response.data.area_List[2].Policy_Info_Page;
                    self.getAllMapData(mainComp.component);


                }
            }).catch(function (error) {
                console.log(error);
            });
        },
        moment: function (date) {
            return moment(date);
        },
        async getAllMapData(contentArry) {
            var tempDataObject = {};
            contentArry.map(function (item) {
                let allKeys = Object.keys(item)
                for (let j = 0; j < allKeys.length; j++) {
                    let key = allKeys[j];
                    let value = item[key];

                    if (key != 'name' && key != 'type') {
                        if (value == undefined || value == null) {
                            value = "";
                        }
                        tempDataObject[key] = value;
                    }
                }
            });
            this.pageObject = tempDataObject;

        },
        getParameterByName: function (name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, '\\$&');
            var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, ' '));
        },
        getInsuranceDetails: function (bookingRefNumber) {
            var self = this;
            var hubUrl = self.commonStore.hubUrls.hubConnection.baseUrl + self.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = self.commonStore.hubUrls.hubConnection.hubServices.insurance.tuneprotectDetail;

            var dataRequest = {
                request: {
                    service: "InsuranceRQ",
                    content: {
                        command: "TuneprotectDetailsRQ",
                        supplierSpecific: {},
                        policyDetails: {
                            bookingRef: bookingRefNumber
                        }
                    },
                    selectCredential: self.selectCredential,
                    supplierCodes: null
                }
            }
            //Get Insurance Details
            axios({
                method: "POST",
                url: hubUrl + serviceUrl,
                data: dataRequest,
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
                }
            }).then((response) => {
                console.log("RESPONSE RECEIVED DETAILS: ", response);
                var Plan_Hub_Response = response.data.response.content.booking;
                if (Plan_Hub_Response.bookingRef) {
                    self.policyNumber = Plan_Hub_Response.bookingRef;
                }
                if (Plan_Hub_Response.bookingStatusName == 'Requested' && Plan_Hub_Response.paymentMode == 'Payment Gateway') {
                    if (self.getParameterByName("status") == 101 && Plan_Hub_Response.paymentStatus) {
                        self.bookInsurance(Plan_Hub_Response);
                    } else if (self.getParameterByName("status") == 102 || !Plan_Hub_Response.paymentStatus) {
                        alertify.confirm("Error", "Payment is failed.",
                            function () {
                                self.Plan_Hub_Response = response.data.response.content.booking;
                            },
                            function () {});
                    } else {
                        alertify.confirm("Error", "We have found some technical difficulties. Please contact admin!.",
                            function () {
                                self.Plan_Hub_Response = response.data.response.content.booking;
                            },
                            function () {});
                    }
                } else if (Plan_Hub_Response.paymentMode == 'Credit') {
                    self.Plan_Hub_Response = response.data.response.content.booking;
                    if (!self.sendmailstatus) {
                        self.sendEmailVoucher(self.Agency_Info.emailId);
                        window.sessionStorage.setItem("sendIssueMail", true);
                    }
                } else if (Plan_Hub_Response.paymentMode == 'Payment Gateway' && Plan_Hub_Response.bookingStatusName == 'Issued') {
                    self.Plan_Hub_Response = response.data.response.content.booking;
                    if (!self.sendmailstatus) {
                        self.sendEmailVoucher(self.Agency_Info.emailId);
                        window.sessionStorage.setItem("sendIssueMail", true);
                    }
                }

            }).catch(function () {
                alertify.alert('Error', 'We have found some technical difficulties. Please contact admin!');
            })
        },
        bookInsurance: function (Plan_Hub_Response) {
            var self = this;

            //Book Insurance
            var hubUrls = HubServiceUrls;
            var hubUrl = hubUrls.hubConnection.baseUrl;
            var port = hubUrls.hubConnection.ipAddress;
            var tpGetCurrencyCode = hubUrls.hubConnection.hubServices.insurance.purchase;

            self.InsuranceRq.request.content.isHubOnly = false;
            self.InsuranceRq.request.content.bookingRef = Plan_Hub_Response.bookingRef;
            axios.post(hubUrl + port + tpGetCurrencyCode, self.InsuranceRq, {
                headers: {
                    Authorization: "Bearer " + window.localStorage.getItem("accessToken")
                }
            }).then(function (response) {
                console.log("RESPONSE RECEIVED BOOK: ", response);
                var confirmation = response.data.response.content;
                if (confirmation.data.proposalState == 'CONFIRMED') {
                    //Get Insurance Details
                    var bookingRef = confirmation.bookingRef
                    self.getInsuranceDetails(bookingRef);
                } else {
                    alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");
                }
            }).catch(function (error) {
                try {
                    if (error.response.data.code == 402 && error.response.data.message) {
                        alertify.alert("Warning", error.response.data.message);
                    } else {
                        alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");
                    }
                } catch (error) {
                    alertify.alert("Warning", "We have found some technical difficulties. Please contact admin.");
                }
                console.log(error);
            });
        }
    },
    created: function () {
        this.getPagecontent();
        this.Agency_Info = JSON.parse(atob(window.localStorage.getItem("agencyNode")));
        this.sendmailstatus = JSON.parse(window.sessionStorage.getItem("sendIssueMail"));
        this.InsuranceRq = JSON.parse(window.sessionStorage.getItem("Insurance_Rq"));
        this.selectCredential = JSON.parse(window.sessionStorage.getItem('selectCredential'));
        var item = JSON.parse(window.sessionStorage.getItem('Insurance_Info'));
        this.policyNumber = item.referenceNumber;
        this.getInsuranceDetails(item.referenceNumber);


        // this.loadAvailblePlans();
    }
});