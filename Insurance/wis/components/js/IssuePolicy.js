﻿var countryList = [{
        code: "AF",
        name: "Afghanistan"
    },
    {
        code: "AL",
        name: "Albania"
    },
    {
        code: "DZ",
        name: "Algeria"
    },
    {
        code: "AS",
        name: "American Samoa"
    },
    {
        code: "AD",
        name: "Andorra"
    },
    {
        code: "AO",
        name: "Angola"
    },
    {
        code: "AI",
        name: "Anguilla"
    },
    {
        code: "AQ",
        name: "Antartica"
    },
    {
        code: "AG",
        name: "Antigua And Barbuda"
    },
    {
        code: "AR",
        name: "Argentina"
    },
    {
        code: "AM",
        name: "Armenia"
    },
    {
        code: "AW",
        name: "Aruba"
    },
    {
        code: "AU",
        name: "Australia"
    },
    {
        code: "AT",
        name: "Austria"
    },
    {
        code: "AZ",
        name: "Azerbaijan"
    },
    {
        code: "BS",
        name: "Bahamas"
    },
    {
        code: "BH",
        name: "Bahrain"
    },
    {
        code: "BD",
        name: "Bangladesh"
    },
    {
        code: "BB",
        name: "Barbados"
    },
    {
        code: "BY",
        name: "Belarus"
    },
    {
        code: "BE",
        name: "Belgium"
    },
    {
        code: "BZ",
        name: "Belize"
    },
    {
        code: "BJ",
        name: "Benin"
    },
    {
        code: "BM",
        name: "Bermuda"
    },
    {
        code: "BT",
        name: "Bhutan"
    },
    {
        code: "BO",
        name: "Bolivia"
    },
    {
        code: "BQ",
        name: "Bonaire St Eustatius And Saba "
    },
    {
        code: "BA",
        name: "Bosnia-Herzegovina"
    },
    {
        code: "BW",
        name: "Botswana"
    },
    {
        code: "BR",
        name: "Brazil"
    },
    {
        code: "IO",
        name: "British Indian Ocean Territory"
    },
    {
        code: "BN",
        name: "Brunei Darussalam"
    },
    {
        code: "BG",
        name: "Bulgaria"
    },
    {
        code: "BF",
        name: "Burkina Faso"
    },
    {
        code: "BI",
        name: "Burundi"
    },
    {
        code: "KH",
        name: "Cambodia"
    },
    {
        code: "CM",
        name: "Cameroon-Republic Of"
    },
    {
        code: "CB",
        name: "Canada Buffer"
    },
    {
        code: "CA",
        name: "Canada"
    },
    {
        code: "CV",
        name: "Cape Verde-Republic Of"
    },
    {
        code: "KY",
        name: "Cayman Islands"
    },
    {
        code: "CF",
        name: "Central African Republic"
    },
    {
        code: "TD",
        name: "Chad"
    },
    {
        code: "CL",
        name: "Chile"
    },
    {
        code: "CN",
        name: "China"
    },
    {
        code: "CX",
        name: "Christmas Island"
    },
    {
        code: "CC",
        name: "Cocos Islands"
    },
    {
        code: "CO",
        name: "Colombia"
    },
    {
        code: "KM",
        name: "Comoros"
    },
    {
        code: "CG",
        name: "Congo Brazzaville"
    },
    {
        code: "CD",
        name: "Congo The Democratic Rep Of"
    },
    {
        code: "CK",
        name: "Cook Islands"
    },
    {
        code: "CR",
        name: "Costa Rica"
    },
    {
        code: "CI",
        name: "Cote D Ivoire"
    },
    {
        code: "HR",
        name: "Croatia"
    },
    {
        code: "CU",
        name: "Cuba"
    },
    {
        code: "CW",
        name: "Curacao"
    },
    {
        code: "CY",
        name: "Cyprus"
    },
    {
        code: "CZ",
        name: "Czech Republic"
    },
    {
        code: "DK",
        name: "Denmark"
    },
    {
        code: "DJ",
        name: "Djibouti"
    },
    {
        code: "DM",
        name: "Dominica"
    },
    {
        code: "DO",
        name: "Dominican Republic"
    },
    {
        code: "TP",
        name: "East Timor Former Code)"
    },
    {
        code: "EC",
        name: "Ecuador"
    },
    {
        code: "EG",
        name: "Egypt"
    },
    {
        code: "SV",
        name: "El Salvador"
    },
    {
        code: "EU",
        name: "Emu European Monetary Union"
    },
    {
        code: "GQ",
        name: "Equatorial Guinea"
    },
    {
        code: "ER",
        name: "Eritrea"
    },
    {
        code: "EE",
        name: "Estonia"
    },
    {
        code: "ET",
        name: "Ethiopia"
    },
    {
        code: "FK",
        name: "Falkland Islands"
    },
    {
        code: "FO",
        name: "Faroe Islands"
    },
    {
        code: "ZZ",
        name: "Fictitious Points"
    },
    {
        code: "FJ",
        name: "Fiji"
    },
    {
        code: "FI",
        name: "Finland"
    },
    {
        code: "FR",
        name: "France"
    },
    {
        code: "GF",
        name: "French Guiana"
    },
    {
        code: "PF",
        name: "French Polynesia"
    },
    {
        code: "GA",
        name: "Gabon"
    },
    {
        code: "GM",
        name: "Gambia"
    },
    {
        code: "GE",
        name: "Georgia"
    },
    {
        code: "DE",
        name: "Germany"
    },
    {
        code: "GH",
        name: "Ghana"
    },
    {
        code: "GI",
        name: "Gibraltar"
    },
    {
        code: "GR",
        name: "Greece"
    },
    {
        code: "GL",
        name: "Greenland"
    },
    {
        code: "GD",
        name: "Grenada"
    },
    {
        code: "GP",
        name: "Guadeloupe"
    },
    {
        code: "GU",
        name: "Guam"
    },
    {
        code: "GT",
        name: "Guatemala"
    },
    {
        code: "GW",
        name: "Guinea Bissau"
    },
    {
        code: "GN",
        name: "Guinea"
    },
    {
        code: "GY",
        name: "Guyana"
    },
    {
        code: "HT",
        name: "Haiti"
    },
    {
        code: "HN",
        name: "Honduras"
    },
    {
        code: "HK",
        name: "Hong Kong"
    },
    {
        code: "HU",
        name: "Hungary"
    },
    {
        code: "IS",
        name: "Iceland"
    },
    {
        code: "IN",
        name: "India"
    },
    {
        code: "ID",
        name: "Indonesia"
    },
    {
        code: "IR",
        name: "Iran"
    },
    {
        code: "IQ",
        name: "Iraq"
    },
    {
        code: "IE",
        name: "Ireland-Republic Of"
    },
    {
        code: "IL",
        name: "Israel"
    },
    {
        code: "IT",
        name: "Italy"
    },
    {
        code: "JM",
        name: "Jamaica"
    },
    {
        code: "JP",
        name: "Japan"
    },
    {
        code: "JO",
        name: "Jordan"
    },
    {
        code: "KZ",
        name: "Kazakhstan"
    },
    {
        code: "KE",
        name: "Kenya"
    },
    {
        code: "KI",
        name: "Kiribati"
    },
    {
        code: "KP",
        name: "Korea Dem Peoples Rep Of"
    },
    {
        code: "KR",
        name: "Korea Republic Of"
    },
    {
        code: "KW",
        name: "Kuwait"
    },
    {
        code: "KG",
        name: "Kyrgyzstan"
    },
    {
        code: "LA",
        name: "Lao Peoples Dem Republic"
    },
    {
        code: "LV",
        name: "Latvia"
    },
    {
        code: "LB",
        name: "Lebanon"
    },
    {
        code: "LS",
        name: "Lesotho"
    },
    {
        code: "LR",
        name: "Liberia"
    },
    {
        code: "LY",
        name: "Libya"
    },
    {
        code: "LI",
        name: "Liechtenstein"
    },
    {
        code: "LT",
        name: "Lithuania"
    },
    {
        code: "LU",
        name: "Luxembourg"
    },
    {
        code: "MO",
        name: "Macao -Sar Of China-"
    },
    {
        code: "MK",
        name: "Macedonia -Fyrom-"
    },
    {
        code: "MG",
        name: "Madagascar"
    },
    {
        code: "MW",
        name: "Malawi"
    },
    {
        code: "MY",
        name: "Malaysia"
    },
    {
        code: "MV",
        name: "Maldives Island"
    },
    {
        code: "ML",
        name: "Mali"
    },
    {
        code: "MT",
        name: "Malta"
    },
    {
        code: "MH",
        name: "Marshall Islands"
    },
    {
        code: "MQ",
        name: "Martinique"
    },
    {
        code: "MR",
        name: "Mauritania"
    },
    {
        code: "MU",
        name: "Mauritius Island"
    },
    {
        code: "YT",
        name: "Mayotte"
    },
    {
        code: "MB",
        name: "Mexico Buffer"
    },
    {
        code: "MX",
        name: "Mexico"
    },
    {
        code: "FM",
        name: "Micronesia"
    },
    {
        code: "MD",
        name: "Moldova"
    },
    {
        code: "MC",
        name: "Monaco"
    },
    {
        code: "MN",
        name: "Mongolia"
    },
    {
        code: "ME",
        name: "Montenegro"
    },
    {
        code: "MS",
        name: "Montserrat"
    },
    {
        code: "MA",
        name: "Morocco"
    },
    {
        code: "MZ",
        name: "Mozambique"
    },
    {
        code: "MM",
        name: "Myanmar"
    },
    {
        code: "NA",
        name: "Namibia"
    },
    {
        code: "NR",
        name: "Nauru"
    },
    {
        code: "NP",
        name: "Nepal"
    },
    {
        code: "AN",
        name: "Netherlands Antilles"
    },
    {
        code: "NL",
        name: "Netherlands"
    },
    {
        code: "NC",
        name: "New Caledonia"
    },
    {
        code: "NZ",
        name: "New Zealand"
    },
    {
        code: "NI",
        name: "Nicaragua"
    },
    {
        code: "NE",
        name: "Niger"
    },
    {
        code: "NG",
        name: "Nigeria"
    },
    {
        code: "NU",
        name: "Niue"
    },
    {
        code: "NF",
        name: "Norfolk Island"
    },
    {
        code: "MP",
        name: "Northern Mariana Islands"
    },
    {
        code: "NO",
        name: "Norway"
    },
    {
        code: "OM",
        name: "Oman"
    },
    {
        code: "PK",
        name: "Pakistan"
    },
    {
        code: "PW",
        name: "Palau Islands"
    },
    {
        code: "PS",
        name: "Palestine - State Of"
    },
    {
        code: "PA",
        name: "Panama"
    },
    {
        code: "PG",
        name: "Papua New Guinea"
    },
    {
        code: "PY",
        name: "Paraguay"
    },
    {
        code: "PE",
        name: "Peru"
    },
    {
        code: "PH",
        name: "Philippines"
    },
    {
        code: "PL",
        name: "Poland"
    },
    {
        code: "PT",
        name: "Portugal"
    },
    {
        code: "PR",
        name: "Puerto Rico"
    },
    {
        code: "QA",
        name: "Qatar"
    },
    {
        code: "RE",
        name: "Reunion Island"
    },
    {
        code: "RO",
        name: "Romania"
    },
    {
        code: "RU",
        name: "Russia"
    },
    {
        code: "XU",
        name: "Russia"
    },
    {
        code: "RW",
        name: "Rwanda"
    },
    {
        code: "WS",
        name: "Samoa-Independent State Of"
    },
    {
        code: "SM",
        name: "San Marino"
    },
    {
        code: "ST",
        name: "Sao Tome And Principe Islands "
    },
    {
        code: "SA",
        name: "Saudi Arabia"
    },
    {
        code: "SN",
        name: "Senegal"
    },
    {
        code: "RS",
        name: "Serbia"
    },
    {
        code: "SC",
        name: "Seychelles Islands"
    },
    {
        code: "SL",
        name: "Sierra Leone"
    },
    {
        code: "SG",
        name: "Singapore"
    },
    {
        code: "SX",
        name: "Sint Maarten"
    },
    {
        code: "SK",
        name: "Slovakia"
    },
    {
        code: "SI",
        name: "Slovenia"
    },
    {
        code: "SB",
        name: "Solomon Islands"
    },
    {
        code: "SO",
        name: "Somalia"
    },
    {
        code: "ZA",
        name: "South Africa"
    },
    {
        code: "SS",
        name: "South Sudan"
    },
    {
        code: "ES",
        name: "Spain"
    },
    {
        code: "LK",
        name: "Sri Lanka"
    },
    {
        code: "SH",
        name: "St. Helena Island"
    },
    {
        code: "KN",
        name: "St. Kitts"
    },
    {
        code: "LC",
        name: "St. Lucia"
    },
    {
        code: "PM",
        name: "St. Pierre And Miquelon"
    },
    {
        code: "VC",
        name: "St. Vincent"
    },
    {
        code: "SD",
        name: "Sudan"
    },
    {
        code: "SR",
        name: "Suriname"
    },
    {
        code: "SZ",
        name: "Swaziland"
    },
    {
        code: "SE",
        name: "Sweden"
    },
    {
        code: "CH",
        name: "Switzerland"
    },
    {
        code: "SY",
        name: "Syrian Arab Republic"
    },
    {
        code: "TW",
        name: "Taiwan"
    },
    {
        code: "TJ",
        name: "Tajikistan"
    },
    {
        code: "TZ",
        name: "Tanzania-United Republic"
    },
    {
        code: "TH",
        name: "Thailand"
    },
    {
        code: "TL",
        name: "Timor Leste"
    },
    {
        code: "TG",
        name: "Togo"
    },
    {
        code: "TK",
        name: "Tokelau"
    },
    {
        code: "TO",
        name: "Tonga"
    },
    {
        code: "TT",
        name: "Trinidad And Tobago"
    },
    {
        code: "TN",
        name: "Tunisia"
    },
    {
        code: "TR",
        name: "Turkey"
    },
    {
        code: "TM",
        name: "Turkmenistan"
    },
    {
        code: "TC",
        name: "Turks And Caicos Islands"
    },
    {
        code: "TV",
        name: "Tuvalu"
    },
    {
        code: "UM",
        name: "U.S. Minor Outlying Islands"
    },
    {
        code: "UG",
        name: "Uganda"
    },
    {
        code: "UA",
        name: "Ukraine"
    },
    {
        code: "AE",
        name: "United Arab Emirates"
    },
    {
        code: "GB",
        name: "United Kingdom"
    },
    {
        code: "US",
        name: "United States Of America"
    },
    {
        code: "UY",
        name: "Uruguay"
    },
    {
        code: "UZ",
        name: "Uzbekistan"
    },
    {
        code: "VU",
        name: "Vanuatu"
    },
    {
        code: "VA",
        name: "Vatican"
    },
    {
        code: "VE",
        name: "Venezuela"
    },
    {
        code: "VN",
        name: "Vietnam"
    },
    {
        code: "VG",
        name: "Virgin Islands-British"
    },
    {
        code: "VI",
        name: "Virgin Islands-United States"
    },
    {
        code: "WF",
        name: "Wallis And Futuna Islands"
    },
    {
        code: "EH",
        name: "Western Sahara"
    },
    {
        code: "YE",
        name: "Yemen Republic"
    },
    {
        code: "ZM",
        name: "Zambia"
    },
    {
        code: "ZW",
        name: "Zimbabwe"
    },
]
Vue.component('wis-issue', {
    data() {
        return {
            commonStore: vueCommonStore.state,
            selectedPolicy: {},
            selectedOptions: [],
            paxInformation: [],
            paxTitles: [{
                    id: 1,
                    name: 'Mr',
                    paxType: ['ADT', 'SR1', 'SR2'],
                    gender: 'M'
                },
                {
                    id: 2,
                    name: 'Miss',
                    paxType: ['ADT', 'SR1', 'SR2'],
                    gender: 'F'
                },
                {
                    id: 3,
                    name: 'Mrs',
                    paxType: ['ADT', 'SR1', 'SR2'],
                    gender: 'F'
                },
                {
                    id: 4,
                    name: 'Ms',
                    paxType: ['CHD', 'INF'],
                    gender: 'F'
                },
                {
                    id: 5,
                    name: 'Master',
                    paxType: ['CHD', 'INF'],
                    gender: 'M'
                }
            ],
            countryList: countryList,
            finalizing: false
        }
    },
    methods: {
        benefitShowMore: function () {
            var status = $('#benefirMore').html().toLowerCase();
            if (status.includes('show')) {
                $('#divBenefits table tbody tr').each(function () {
                    $(this).show();
                });
                $('#benefirMore').html('Hide <i class="fa fa-minus-square"></i>');
            } else {
                $('#divBenefits table tbody tr').each(function (index) {
                    index < 6 ? $(this).show() : $(this).hide();
                });
                $('#benefirMore').html('Show more <i class="fa fa-plus-square"></i>');
            }
        },
        setDatePicker: function () {
            for (var index = 0; index < this.paxInformation.length; index++) {
                var minAge = parseInt(this.paxInformation[index].ageLimit.split('-')[0]);
                var maxAge = parseInt(this.paxInformation[index].ageLimit.split('-')[1]);

                var maxDate = '';
                var minDate = '';
                if (minAge != 0) {
                    maxDate = moment().subtract(minAge, 'years').diff(moment(), 'days');
                }
                if (maxAge != 0) {
                    minDate = moment().subtract(maxAge, 'years').diff(moment(), 'days');
                }

                $('#txtDob' + this.paxInformation[index].paxTypeCode.toLowerCase() + index).datepicker({
                    yearRange: 'c-70:c+10',
                    numberOfMonths: 1,
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'dd/mm/yy',
                    maxDate: '+0d',
                    onSelect: function (selectedDate) {},
                });

                if (minDate != '') {
                    $("#txtDob" + this.paxInformation[index].paxTypeCode.toLowerCase() + index).datepicker("option", "minDate", minDate);
                }
                if (maxDate != '') {
                    $("#txtDob" + this.paxInformation[index].paxTypeCode.toLowerCase() + index).datepicker("option", "maxDate", maxDate);
                }

            }
        },
        validateOnBook: function () {
            var isSuccess = true;

            for (var index = 0; index < this.paxInformation.length; index++) {
                if (stringIsNullorEmpty($('#ddlTitle' + this.paxInformation[index].paxTypeCode.toLowerCase() + index).val())) {
                    alertify.alert('Warning', 'Please select title', function () {
                        $(".addclass").removeClass("fa fa-circle-o-notch fa-spin");
                    });
                    return false;
                }

                if (stringIsNullorEmpty($('#txtFname' + this.paxInformation[index].paxTypeCode.toLowerCase() + index).val())) {
                    alertify.alert('Warning', 'Please fill first name', function () {
                        $(".addclass").removeClass("fa fa-circle-o-notch fa-spin");
                    });
                    return false;
                }

                if (stringIsNullorEmpty($('#txtLname' + this.paxInformation[index].paxTypeCode.toLowerCase() + index).val())) {
                    alertify.alert('Warning', 'Please fill last name', function () {
                        $(".addclass").removeClass("fa fa-circle-o-notch fa-spin");
                    });
                    return false;
                }

                if (stringIsNullorEmpty($('#txtDob' + this.paxInformation[index].paxTypeCode.toLowerCase() + index).val())) {
                    alertify.alert('Warning', 'Please select date of birth', function () {
                        $(".addclass").removeClass("fa fa-circle-o-notch fa-spin");
                    });
                    return false;
                }

                if (stringIsNullorEmpty($('#txtnationality' + this.paxInformation[index].paxTypeCode.toLowerCase() + index).val())) {
                    alertify.alert('Warning', 'Please select nationality', function () {
                        $(".addclass").removeClass("fa fa-circle-o-notch fa-spin");
                    });
                    return false;
                }

                if (stringIsNullorEmpty($('#txtPassport' + this.paxInformation[index].paxTypeCode.toLowerCase() + index).val())) {
                    alertify.alert('Warning', 'Please fill passport number', function () {
                        $(".addclass").removeClass("fa fa-circle-o-notch fa-spin");
                    });
                    return false;
                }

                if (!isValidPassport($('#txtPassport' + this.paxInformation[index].paxTypeCode.toLowerCase() + index).val())) {
                    alertify.alert("Warning", "Invalid passport number", function () {
                        $(".addclass").removeClass("fa fa-circle-o-notch fa-spin");
                    });
                    return false;
                }
            }

            if ($("#chkAgree").prop('checked') == false) {
                alertify.alert("Terms and Conditions", "Please accept terms and conditions", function () {
                    $(".addclass").removeClass("fa fa-circle-o-notch fa-spin");
                });
                return false;
            }

            return isSuccess;
        },
        purchaseInsurance: function () {
            var isSuccess = this.validateOnBook();
            var vm = this;
            if (isSuccess && !vm.finalizing) {
                var travellers = [];
                for (var index = 0; index < this.paxInformation.length; index++) {
                    var title = $('#ddlTitle' + this.paxInformation[index].paxTypeCode.toLowerCase() + index).val();
                    var fName = $('#txtFname' + this.paxInformation[index].paxTypeCode.toLowerCase() + index).val();
                    var lName = $('#txtLname' + this.paxInformation[index].paxTypeCode.toLowerCase() + index).val();
                    var dob = moment($('#txtDob' + this.paxInformation[index].paxTypeCode.toLowerCase() + index).val(), 'DD/MM/YYYY').format('YYYY-MM-DD');
                    var passportNumber = $('#txtPassport' + this.paxInformation[index].paxTypeCode.toLowerCase() + index).val();
                    var nationality = $('#txtnationality' + this.paxInformation[index].paxTypeCode.toLowerCase() + index).val();

                    var titleId = '1';
                    if (title != '') {
                        if (title == 'Mr') {
                            titleId = 1;
                        }
                        if (title == 'Mrs') {
                            titleId = 2;
                        }
                        if (title == 'Ms') {
                            titleId = 3;
                        }
                        if (title == 'Miss') {
                            titleId = 4;
                        }
                        if (title == 'Master') {
                            titleId = 5;
                        }
                    }
                    var traveller = {
                        title: {
                            titleId: titleId,
                            name: title
                        },
                        firstName: fName,
                        lastName: lName,
                        dob: dob,
                        passportNum: passportNumber,
                        nationality: nationality,
                        paxType: this.paxInformation[index].paxTypeCode
                    }

                    travellers.push(traveller);
                }

                var hubUrls = HubServiceUrls;
                var hubUrl = hubUrls.hubConnection.baseUrl;
                var port = hubUrls.hubConnection.ipAddress;
                var finaliseInsurance = hubUrls.hubConnection.hubServices.wisInsurance.finaliseInsurance;
                // var totalPriceOptions = this.selectedOptions.reduce(function(a,c) {
                //     return a + c.fare;
                // }, 0)
                var data = {
                    request: {
                        service: "InsuranceWisRQ",
                        supplierCodes: [23],
                        content: {
                            command: "WisFinaliseRQ",
                            finaliseInsuranceRQ: {
                                quoteInsurance: {
                                    supplierId: 23,
                                    startDate: this.selectedPolicy.tripDetails.startDate,
                                    endDate: this.selectedPolicy.tripDetails.endDate,
                                    adtNum: parseInt(this.selectedPolicy.pax.adults),
                                    insuranceTypeId: (["single", "annual", "biennial"].indexOf(this.selectedPolicy.tripDetails.tripType.toLowerCase()) + 1) || 1,
                                    insuranceGroupId: parseInt(this.selectedPolicy.tripDetails.familyGroup),
                                    predefinedDestinationsIdList: [(["gulf", "europe", "subcon", "worldwide_ex", "worldwide"].indexOf(this.selectedPolicy.tripDetails.destinationId.toLowerCase()) + 1) || 6]
                                },
                                schemeDetails: {
                                    name: this.selectedPolicy.policy.name,
                                    premium: this.selectedPolicy.policy.premium,
                                    information: this.selectedPolicy.policy.name,
                                    price: this.selectedPolicy.policy.premium,
                                    sealedPrice: this.selectedPolicy.policy.sealedPrice,
                                    benefitList: this.selectedPolicy.policy.benefits.map(function (e) {
                                        return {
                                            section: e.section,
                                            cover: e.cover,
                                            coverageAmt: e.amount,
                                            excess: e.excess
                                        }
                                    }),
                                    optionList: this.selectedOptions
                                },
                                userContactDetails: {
                                    countryCode: this.commonStore.agencyNode.loginNode.country.code,
                                    contactNumber: this.commonStore.agencyNode.contactNumber
                                },
                                insuranceFinaliseRQ: {
                                    quoteId: this.selectedPolicy.quoteId,
                                    schemeId: this.selectedPolicy.schemeId,
                                    titleCustomer: this.commonStore.agencyNode.title.name,
                                    firstNameCustomer: this.commonStore.agencyNode.firstName,
                                    lastNameCustomer: this.commonStore.agencyNode.lastName,
                                    email: this.commonStore.agencyNode.emailId,
                                    mobile: this.commonStore.agencyNode.contactNumber,
                                    address1: "",
                                    address2: "",
                                    address3: "",
                                    address4: "",
                                    options: this.selectedPolicy.options,
                                    titleTraveller: travellers.map(function (e) {
                                        return e.title.name;
                                    }),
                                    firstNameTraveller: travellers.map(function (e) {
                                        return e.firstName;
                                    }),
                                    lastNameTraveller: travellers.map(function (e) {
                                        return e.lastName;
                                    }),
                                    dob: travellers.map(function (e) {
                                        return e.dob;
                                    }),
                                    passportNumber: travellers.map(function (e) {
                                        return e.passportNum;
                                    })
                                },
                                travelerInfoList: travellers,
                                sealed: this.selectedPolicy.sealed,
                                paymentMode: 7,
                                couponUsedIds: []
                            }
                        },
                        token: window.localStorage.getItem("accessToken"),
                    }
                }
                vm.finalizing = true;
                axios.post(hubUrl + port + finaliseInsurance, data, {
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
                    }
                }).then(function (response) {
                    if (response.data.response.content.data.result == "success") {
                        window.sessionStorage.setItem('Insurance_Info', JSON.stringify({
                            referenceNumber: response.data.response.content.bookingRef
                        }));
                        window.location.href = '/Insurance/wis/policyinfo.html';
                    } else {
                        vm.finalizing = false;
                        alertify.alert('Error', 'Error in finalizing insurance.');
                    }
                }).catch(function (error) {
                    vm.finalizing = false;
                    console.log(error);
                    alertify.alert('Error', 'Error in finalizing insurance.');
                });
            }
        }
    },
    mounted: function () {
        var vm = this;

        this.selectedPolicy = JSON.parse(window.localStorage.getItem("selectedPolicy"));

        var selectedOptions = [];
        for (var property in this.selectedPolicy.options) {
            if (this.selectedPolicy.options[property] == 1) {
                selectedOptions.push({
                    id: parseInt(property),
                    name: this.selectedPolicy.policy.options[property].name,
                    description: this.selectedPolicy.policy.options[property].description,
                    fare: this.selectedPolicy.policy.options[property].price,
                    sealedP: this.selectedPolicy.policy.options[property].sealedP
                });
            }
        }

        for (var property in this.selectedPolicy.options) {
            if (this.selectedPolicy.options[property] == 0) {
                delete this.selectedPolicy.options[property];
            }
        }

        this.selectedOptions = selectedOptions;

        var paxInformation = [];
        var adult = this.selectedPolicy.pax.adults;
        var child = this.selectedPolicy.pax.children;
        var senior = this.selectedPolicy.pax.seniors;
        var senior2 = this.selectedPolicy.pax.seniors2;
        for (var i = 1; i <= parseInt(adult) + parseInt(child) + parseInt(senior) + parseInt(senior2); i++) {
            var paxTypeCode = 'ADT';
            var paxType = 'Adult';
            var ageLimit = '17-65';
            if (i > parseInt(adult)) {
                paxTypeCode = 'CHD';
                paxType = 'Child';
                ageLimit = '0-16';
            }
            if (i > (parseInt(child) + parseInt(adult))) {
                paxTypeCode = 'SR1';
                paxType = 'Senior';
                ageLimit = '66-75';
            }
            if (i > (parseInt(child) + parseInt(adult) + parseInt(senior))) {
                paxTypeCode = 'SR2';
                paxType = 'Super Seniors';
                ageLimit = '76-85';
            }
            var paxAvailbleTitles = this.paxTitles.filter(function (title) {
                return jQuery.inArray(paxTypeCode.toUpperCase(), title.paxType) != -1 ? true : false;
            });

            paxInformation.push({
                paxType: paxType,
                ageLimit: ageLimit,
                paxTypeCode: paxTypeCode,
                paxAvailbleTitles: paxAvailbleTitles,
            })

        }
        this.paxInformation = paxInformation;

        setTimeout(() => {
            $('[data-toggle="popover"]').popover({
                html: true
            });
            vm.setDatePicker();
        }, 100);


    }
});