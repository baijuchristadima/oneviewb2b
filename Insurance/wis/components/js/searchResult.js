Vue.component('wis-result', {
    //for websocket
    mixins: [websocketMixin],
    data() {
        return {
            commonStore: vueCommonStore.state,
            results: {
                quotes: null
            },
            fullBenefitsValue: {},
            totalCost: 0,
            selectedOptions: [],
            selectedBenefits: {},
            schemeId: null,
            pax: {},
            tripDetails: {},
            response: null
        }
    },
    methods: {
        searchInurance: function () {
            var query = [];
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                query.push(hash[0]);
                query[hash[0]] = hash[1];
            }

            if (query.length == 0) {
                window.location.href = 'index.html';
            } else {
                var allDatas = query.insurance.split('-');
                if (allDatas.length > 0) {
                    var tripType = allDatas[0];
                    var startDate = allDatas[1];
                    var endDate = allDatas[2];
                    var familyGroup = allDatas[3];
                    var adult = allDatas[4].split("|")[0];
                    var children = allDatas[4].split("|")[1];
                    var seniors = allDatas[4].split("|")[2];
                    var superSeniors = allDatas[4].split("|")[3];
                    var airport = allDatas[5].split("|")[0];
                    var destinationId = allDatas[5].split("|")[1];

                    this.pax = {
                        adults: adult,
                        children: children,
                        seniors: seniors,
                        seniors2: superSeniors,
                    };

                    this.tripDetails = {
                        tripType: tripType,
                        startDate: startDate.replace(/\|/g, "-"),
                        endDate: endDate.replace(/\|/g, "-"),
                        familyGroup: familyGroup,
                        destinationId: destinationId
                    }

                    var searchRQ = {
                        request: {
                            service: "InsuranceWisRQ",
                            supplierCodes: [23],
                            token: window.localStorage.getItem("accessToken"),
                            content: {
                                command: "WisSearchRQ",
                                criteria: {
                                    criteriaType: "WisInsurance",
                                    journeyId: tripType,
                                    startDate: startDate.replace(/\|/g, "-"),
                                    end_date: endDate.replace(/\|/g, "-"),
                                    region: destinationId,
                                    airport: airport == "NA" ? undefined : airport,
                                    family: familyGroup == 2 ? 2 : 1,
                                    group: familyGroup == 3 ? 2 : 1,
                                    ageBands: {
                                        adults: adult,
                                        children: children,
                                        seniors: seniors,
                                        seniors2: superSeniors,
                                    }
                                }
                            }
                        }
                    }
                    
                    this.connect(); //open websocket connection           

                    this.sendMessage(JSON.stringify(searchRQ));
                    // this.processInsuranceResult();
                    var vm = this;
                    setTimeout(function () {
                        vm.disconnect();
                    }, 90000);

                } else {
                    window.location.href = 'index.html';
                }
            }
        },
        processInsuranceResult: function (response) {
            this.results.quotes = response.response.content.data.quotes;
            this.response = response.response.content.data;
        },
        fullBenefits: function (quote) {
            this.fullBenefitsValue = JSON.parse(JSON.stringify(quote));
            this.fullBenefitsValue.benefits = this.fullBenefitsValue.benefits.filter(function(e){
                return e.option == 0 && e.amount != "Not Covered" && e.amount != "";
            });
        },
        clickPolicy: function (event) {
            $('.cover_brder').removeClass("blue_color");
            $(event.currentTarget).addClass('blue_color');
        },
        selectBenefits: function (quote, schemeId){
            this.schemeId = schemeId;
            this.selectedBenefits = JSON.parse(JSON.stringify(quote));
            for (var property in this.selectedBenefits.options) {
                var benefitsNew = [];
                var descriptionNew = this.selectedBenefits.options[property].description;
                benefitsNew = this.selectedBenefits.benefits.filter(function (e) {
                    return e.option == property;
                })
                if (benefitsNew.length > 0) {
                    descriptionNew += '<table><thead><tr><td><strong>Section Cover</strong></td><td><strong>Sum Insured</strong></td><td><strong>Excess</strong></td></tr></thead><tbody>';
                    for (var index = 0; index < benefitsNew.length; index++) {
                        descriptionNew += '<tr><td>' + benefitsNew[index].cover + '</td><td>' + benefitsNew[index].amount + '</td><td>' + benefitsNew[index].excess + '</td></tr>';
                    }
                    descriptionNew += '</tbody></table>';
                }
                this.selectedBenefits.options[property].descriptionNew = descriptionNew;
            }
            this.totalCost = parseFloat(quote.premium);
            setTimeout(() => {
                $('[data-toggle="popover"]').popover({ html: true });
            }, 100);
        },
        addRemoveOptions: function (option, name, event){
            if (event.target.checked) {
                var tempOptions = [];
                for (var index = 0; index < this.selectedBenefits.benefits.length; index++) {
                    if (this.selectedBenefits.benefits[index].option == name) {
                        tempOptions.push({
                            cover: this.selectedBenefits.benefits[index].cover,
                            coverageAmt: this.selectedBenefits.benefits[index].amount,
                            excess: this.selectedBenefits.benefits[index].excess
                        });
                    }
                }
                this.selectedOptions.push({name: name, options: tempOptions});
                this.totalCost += parseFloat(option.price);
            } else {
                var oIndex = this.selectedOptions.findIndex(function(e){
                    return e.name == name;
                })
                this.selectedOptions.splice(oIndex, 1);
                this.totalCost -= parseFloat(option.price);
            }
            $('[data-toggle="popover"]').popover({ html: true });
        },
        addQuotationToCurrentCart: function (insurance, name, index) {
            var vm = this;
            var uuid = vm.results.quote_id + "|" + insurance.name.toLowerCase().split(" ").join("") + "|" + name + "|" + index + "|insurance";
            var insuranceQuotation = this.commonStore.selectedCart.services.insuranceWIS;

            if (typeof insuranceQuotation == "undefined") {
                insuranceQuotation = [];
            }
            if (insuranceQuotation.findIndex(function (x) { return x.serialNumber == uuid; }) == -1) {
                if (typeof vm.commonStore.selectedCart.services.insuranceWIS == "undefined") {
                    vm.commonStore.selectedCart.services.insuranceWIS = [];
                }
                vm.commonStore.selectedCart.services.insuranceWIS.push({
                    serialNumber: uuid,
                    planTitle: insurance.name,
                    totalPremiumAmount: insurance.premium,
                    planContent: insurance,
                    tripDetails: vm.tripDetails,
                    pax: vm.pax
                });

                for (var k = 0; k < vm.commonStore.carts.length; k++) {
                    if (vm.commonStore.carts[k].id == vm.commonStore.selectedCart.id) {
                        vm.commonStore.carts[k].services.insuranceWIS = vm.commonStore.selectedCart.services.insuranceWIS;
                        break;
                    }
                }
                var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;
                var serviceUrl = this.commonStore.hubUrls.hubConnection.hubServices.addQuote;

                var config = {
                    axiosConfig: {
                        method: "post",
                        url: hubUrl + serviceUrl,
                        data: {
                            content: JSON.stringify(vm.commonStore.carts)
                        }
                    },
                    successCallback: function (response) {
                        if (response) {
                            vm.getAllquotationsOnServer();
                            // alertify.alert("Success", "Added to quotaion " + vm.commonStore.selectedCart.name);
                        }
                    },
                    errorCallback: function (error) {
                        alertify.alert("Error", "Error unable to add quotation.");
                    },
                    showAlert: true
                };

                mainAxiosRequest(config);
            } else {
                alertify.alert("Please note", "Quotation already added.");
            }


        },
        getAllquotationsOnServer: function () {
            var hubUrl = this.commonStore.hubUrls.hubConnection.baseUrl + this.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = this.commonStore.hubUrls.hubConnection.hubServices.pendingQuoteDetails;
            var vm = this;

            var config = {
                axiosConfig: {
                    method: "get",
                    url: hubUrl + serviceUrl
                },
                successCallback: function (response) {
                    if (response && response.data.data[0].content) {
                        vm.commonStore.carts = JSON.parse(response.data.data[0].content);
                    } else {
                        vm.commonStore.carts = [
                            {
                                id: uuidv4(),
                                name: "Default",
                                services: {
                                    flights: [],
                                    hotels: [],
                                    insuranceTP: [],
                                    insuranceWIS: []
                                }
                            }
                        ];
                    }
                },
                errorCallback: function (error) {
                    vm.commonStore.carts = [
                        {
                            id: uuidv4(),
                            name: "Default",
                            services: {
                                flights: [],
                                hotels: [],
                                insuranceTP: [],
                                insuranceWIS: []
                            }
                        }
                    ];
                },
                showAlert: false
            };

            mainAxiosRequest(config);
        },
        issuePolicy: function (value){
            var options = {};
            for (var property in this.selectedBenefits.options) {
                var index = this.selectedOptions.findIndex(function(e){
                    return e.name == property;
                })
                if (index != -1) {
                    options[property] = 1
                } else {
                    options[property] = 0
                } 
            }
            console.log(options);
            var request = {
                quoteId: this.response.quoteId,
                schemeId: this.schemeId,
                options: options,
                policy: this.selectedBenefits,
                totalCost: this.totalCost,
                pax: this.pax,
                sealed: this.response.sealed,
                tripDetails: this.tripDetails
            }
            console.log(request);
            window.localStorage.setItem("selectedPolicy", JSON.stringify(request));
            window.location.href = "/Insurance/wis/IssuePolicy.html";

        }
    },
    created: function () {
        this.searchInurance();
    }
});