﻿Vue.component('wis-booking-info', {
    data() {
        return {
            commonStore: vueCommonStore.state,
            // options: { "88": 0, "89": 0, "90": 0, "91": 1, "92": 1, "93": 1 },
            // policy: { "name": "Premier Traveler", "premium": "84.00", "currency": "AED", "benefits": [{ "section": "A", "cover": "Trip Cancellation / Curtailment", "amount": "$5,000", "excess": "$100", "option": 0 }, { "section": "B", "cover": "Emergency Medical and Other Expenses", "amount": "$500,000", "excess": "$100", "option": 0 }, { "section": "", "cover": "Transportation & Accommodation Expenses", "amount": "$50 per day (max $1,500)", "excess": "$100", "option": 0 }, { "section": "", "cover": "Emergency Family Travel", "amount": "1 x Economy Ticket", "excess": "$100", "option": 0 }, { "section": "", "cover": "Repatriation of the Deceased", "amount": "$10,000", "excess": "$100", "option": 0 }, { "section": "", "cover": "Funeral Expenses", "amount": "$1,000", "excess": "$100", "option": 0 }, { "section": "", "cover": "Dental Expenses", "amount": "$200 per tooth (max $1,000)", "excess": "$100", "option": 0 }, { "section": "C", "cover": "Personal Accident", "amount": "", "excess": "", "option": 0 }, { "section": "", "cover": "Accidental Death or Dismemberment", "amount": "$100,000", "excess": "", "option": 0 }, { "section": "", "cover": "Accidental Death (Common Carrier)", "amount": "Included", "excess": "", "option": 0 }, { "section": "", "cover": "Permanent Total Disablement", "amount": "$100,000", "excess": "", "option": 0 }, { "section": "D", "cover": "Missed Departure", "amount": "$1,000", "excess": "$100", "option": 0 }, { "section": "E", "cover": "Travel Delay", "amount": "$50 per hour up to $1,000", "excess": "4 hours", "option": 0 }, { "section": "F", "cover": "Personal Possessions", "amount": "$5,000 (max $750 per bag)", "excess": "$100", "option": 0 }, { "section": "", "cover": "Valuables (including Laptop)", "amount": "$350", "excess": "$100", "option": 0 }, { "section": "", "cover": "Single Item, Pair or Set", "amount": "$75", "excess": "$100", "option": 0 }, { "section": "", "cover": "Tobacco, Alcohol & Fragrances", "amount": "$75", "excess": "$100", "option": 0 }, { "section": "", "cover": "Baggage Delay", "amount": "$50 per hour up to $1,000", "excess": "4 hours", "option": 0 }, { "section": "F2", "cover": "Personal Money", "amount": "$1,000 ($300 cash limit)", "excess": "$100", "option": 0 }, { "section": "F3", "cover": "Loss of Passport / Travel Documents", "amount": "$500", "excess": "", "option": 0 }, { "section": "G", "cover": "Travel Visa Rejection", "amount": "$100", "excess": "", "option": 0 }, { "section": "H", "cover": "Personal Liability", "amount": "$500,000", "excess": "", "option": 0 }, { "section": "I", "cover": "Legal Expenses", "amount": "$10,000", "excess": "", "option": 0 }, { "section": "J", "cover": "Bail Bond", "amount": "$5,000", "excess": "", "option": 0 }, { "section": "K", "cover": "Credit Card Fraud", "amount": "$500", "excess": "$100", "option": 0 }, { "section": "L", "cover": "Mugging", "amount": "$500", "excess": "$100", "option": 0 }, { "section": "", "cover": "Winter Sports Extension", "amount": "", "excess": "", "option": 0 }, { "section": "", "cover": "Ski Pack", "amount": "$300", "excess": "$50", "option": 91 }, { "section": "", "cover": "Delayed Ski Equipment", "amount": "$300", "excess": "$50", "option": 91 }, { "section": "", "cover": "Ski Equipment", "amount": "$300", "excess": "$50", "option": 91 }, { "section": "", "cover": "Piste Closure", "amount": "$300", "excess": "$50", "option": 91 }, { "section": "", "cover": "Avalanche Closure", "amount": "$300", "excess": "$50", "option": 91 }, { "section": "", "cover": "Business Cover", "amount": "", "excess": "", "option": 0 }, { "section": "", "cover": "Business Equipment", "amount": "$500", "excess": "$50", "option": 93 }, { "section": "", "cover": "Replacement of Business Associate", "amount": "$750", "excess": "$50", "option": 93 }, { "section": "", "cover": "Business Samples", "amount": "$150", "excess": "$50", "option": 93 }, { "section": "", "cover": "Golf Cover", "amount": "", "excess": "", "option": 0 }, { "section": "", "cover": "Golf Equipment", "amount": "$1,500 (max $300 per single item)", "excess": "$100", "option": 88 }, { "section": "", "cover": "Delayed Golf Equipment", "amount": "$200 ($20 a day)", "excess": "$50", "option": 88 }, { "section": "", "cover": "Loss of Green Fee", "amount": "$375 ($75 a day)", "excess": "$50", "option": 88 }, { "section": "", "cover": "Hole in One", "amount": "$50", "excess": "$50", "option": 88 }, { "section": "", "cover": "Terrorism Extension", "amount": "$100,000", "excess": "Applicable to benefit claimed", "option": 89 }], "options": { "88": { "name": "Golf Cover", "price": "21.00", "description": "<p>Cover for golfing including loss of green fees, golf equipment and hole-in-one benefit.</p>", "descriptionNew": "<p>Cover for golfing including loss of green fees, golf equipment and hole-in-one benefit.</p><table><thead><tr><td><strong>Section Cover</strong></td><td><strong>Sum Insured</strong></td><td><strong>Excess</strong></td></tr></thead><tbody><tr><td>Golf Equipment</td><td>$1,500 (max $300 per single item)</td><td>$100</td></tr><tr><td>Delayed Golf Equipment</td><td>$200 ($20 a day)</td><td>$50</td></tr><tr><td>Loss of Green Fee</td><td>$375 ($75 a day)</td><td>$50</td></tr><tr><td>Hole in One</td><td>$50</td><td>$50</td></tr></tbody></table>" }, "89": { "name": "Terrorism Extension", "price": "21.00", "description": "<p>Extension of the policy to include injury, loss or damage due to acts of terrorism.</p>", "descriptionNew": "<p>Extension of the policy to include injury, loss or damage due to acts of terrorism.</p><table><thead><tr><td><strong>Section Cover</strong></td><td><strong>Sum Insured</strong></td><td><strong>Excess</strong></td></tr></thead><tbody><tr><td>Terrorism Extension</td><td>$100,000</td><td>Applicable to benefit claimed</td></tr></tbody></table>" }, "90": { "name": "Hazardous Sports", "price": "21.00", "description": "<p>Cover for a number of additional hazardous sports including parachuting.</p>", "descriptionNew": "<p>Cover for a number of additional hazardous sports including parachuting.</p>" }, "91": { "name": "Winter Sports", "price": "84.00", "description": "<p>Cover for Skiing including ski pack costs, damage to ski equipment and closure of the resort.</p>", "descriptionNew": "<p>Cover for Skiing including ski pack costs, damage to ski equipment and closure of the resort.</p><table><thead><tr><td><strong>Section Cover</strong></td><td><strong>Sum Insured</strong></td><td><strong>Excess</strong></td></tr></thead><tbody><tr><td>Ski Pack</td><td>$300</td><td>$50</td></tr><tr><td>Delayed Ski Equipment</td><td>$300</td><td>$50</td></tr><tr><td>Ski Equipment</td><td>$300</td><td>$50</td></tr><tr><td>Piste Closure</td><td>$300</td><td>$50</td></tr><tr><td>Avalanche Closure</td><td>$300</td><td>$50</td></tr></tbody></table>" }, "92": { "name": "Excess Waiver", "price": "42.00", "description": "<p>Paying an additional premium so that no excess applies for the majority of covers.</p>", "descriptionNew": "<p>Paying an additional premium so that no excess applies for the majority of covers.</p>" }, "93": { "name": "Business Cover", "price": "25.00", "description": "<p>Cover to send replacement business associate to complete the business itinerary plus cover for business equipment and samples</p>", "descriptionNew": "<p>Cover to send replacement business associate to complete the business itinerary plus cover for business equipment and samples</p><table><thead><tr><td><strong>Section Cover</strong></td><td><strong>Sum Insured</strong></td><td><strong>Excess</strong></td></tr></thead><tbody><tr><td>Business Equipment</td><td>$500</td><td>$50</td></tr><tr><td>Replacement of Business Associate</td><td>$750</td><td>$50</td></tr><tr><td>Business Samples</td><td>$150</td><td>$50</td></tr></tbody></table>" } } },
            // selectedOptions: [],
            Plan_Hub_Response: {
                insuranceItem: {
                    startDate: '',
                    fare: '',
                    schemeName: '',
                    travelerInfoList: []
                }
            },
            purchasing: false,
            downloading: false,
        }
    },
    methods: {
        getPagecontent: function () {
            var self = this;
            var hubUrl = self.commonStore.hubUrls.hubConnection.baseUrl + self.commonStore.hubUrls.hubConnection.ipAddress;
            var serviceUrl = self.commonStore.hubUrls.hubConnection.hubServices.wisInsurance.insuranceDetails;

            // var selectCredential = JSON.parse(window.sessionStorage.getItem('selectCredential'));
            var selectCredential = null;
            var item = JSON.parse(window.sessionStorage.getItem('Insurance_Info'));
            self.policyNumber = item.referenceNumber;
            // window.sessionStorage.setItem("Insurance_Info", JSON.stringify(item));
            var dataRequest = {
                request: {
                    service: "InsuranceWisRQ",
                    content: {
                        command: "WisDetailsRQ",
                        policyDetails: {
                            bookingRef: item.referenceNumber
                        }
                    },
                    supplierCodes: [23],
                    token: window.localStorage.getItem("accessToken"),
                }
            }

            axios({
                method: "POST",
                url: hubUrl + serviceUrl,
                data: dataRequest,
                headers: {
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
                }
            }).then((response) => {
                console.log("RESPONSE RECEIVED: ", response);
                self.Plan_Hub_Response = response.data.response.content.data;
                setTimeout(() => {
                    $('[data-toggle="popover"]').popover({
                        html: true
                    });
                }, 100);
            }).catch(function () {
                alertify.alert('Error', 'We have found some technical difficulties. Please contact admin!');
            })


        },
        moment: function (date) {
            return moment(date);
        },
        purchase: function () {
            var vm = this;
            if (!vm.purchasing) {
                var hubUrls = HubServiceUrls;
                var hubUrl = hubUrls.hubConnection.baseUrl;
                var port = hubUrls.hubConnection.ipAddress;
                var purchaseInsurance = hubUrls.hubConnection.hubServices.wisInsurance.purchaseInsurance;
                var data = {
                    request: {
                        service: "InsuranceWisRQ",
                        content: {
                            command: "WisPurchaseRQ",
                            purchaseInsuranceRQ: {
                                policyId: this.Plan_Hub_Response.success.policyId,
                                bookingRef: this.Plan_Hub_Response.bookingRef
                            }
                        },
                        token: window.localStorage.getItem("accessToken"),
                        supplierCodes: [23]
                    }
                }
                vm.purchasing = true;
                axios.post(hubUrl + port + purchaseInsurance, data, {
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
                    }
                }).then(function (response) {
                    if (response.data.response.content.data.result == "success") {
                        alertify.alert('Suucess', 'Insurance purchased successfully',
                            function () {
                                window.location.reload();
                            });
                    } else {
                        vm.purchasing = false;
                        alertify.alert('Error', 'Error in purchasing insurance.');
                    }
                }).catch(function (error) {
                    vm.purchasing = false;
                    console.log(error);
                    alertify.alert('Error', 'Error in purchasing insurance.');
                });
            }
        },
        downloadvoucher: function () {
            var vm = this;
            if (!vm.downloading) {
                var hubUrls = HubServiceUrls;
                var hubUrl = hubUrls.hubConnection.baseUrl;
                var port = hubUrls.hubConnection.ipAddress;
                var insuranceDocument = hubUrls.hubConnection.hubServices.wisInsurance.insuranceDocument;
                var data = {
                    request: {
                        service: "InsuranceWisRQ",
                        content: {
                            command: "WisDocumentRQ",
                            insuranceDocumentRQ: {
                                policyId: this.Plan_Hub_Response.success.policyId,
                                bookingRef: this.Plan_Hub_Response.bookingRef,
                                documentType: "certificate"
                            }
                        },
                        token: window.localStorage.getItem("accessToken"),
                        supplierCodes: [23]
                    }
                }
                vm.downloading = true;
                axios.post(hubUrl + port + insuranceDocument, data, {
                    headers: {
                        "Content-Type": "application/json",
                        "Authorization": "Bearer " + window.localStorage.getItem("accessToken")
                    }
                }).then(function (response) {
                    if (response.data) {
                        var binaryString = window.atob(response.data);
                        var binaryLen = binaryString.length;
                        var bytes = new Uint8Array(binaryLen);
                        for (var i = 0; i < binaryLen; i++) {
                            var ascii = binaryString.charCodeAt(i);
                            bytes[i] = ascii;
                        }
                        var blob = new Blob([bytes], {
                            type: "application/pdf"
                        });
                        var link = document.createElement('a');
                        link.href = window.URL.createObjectURL(blob);
                        link.download = "myFileName.pdf";
                        link.click();
                        vm.downloading = false;
                    } else {
                        vm.downloading = false;
                        alertify.alert('Error', 'Error in getting insurance voucher.');
                    }
                }).catch(function (error) {
                    vm.downloading = false;
                    console.log(error);
                    alertify.alert('Error', 'Error in getting insurance voucher.');
                });
            }
        }

    },
    mounted: function () {
        // for (var property in this.options) {
        //     if (this.options[property] == 1) {
        //         this.selectedOptions.push(this.policy.options[property]);
        //     }
        // }
        this.getPagecontent();
    }
});